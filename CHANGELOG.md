# [1.0.0-198](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-197...1.0.0-198) (2024-10-10)


### Bug Fixes

* **job-details:** don't calculate the price to the token amounts ([ffb1dba](https://gitlab.com/jobgrader/jobgrader-app/commit/ffb1dba0ffbd3cd7061445bdadad2aa28eb102ca))



# [1.0.0-197](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-196...1.0.0-197) (2024-10-10)


### Bug Fixes

* **theme:** Inconsisten icon color upon setting theme to system ([89b1470](https://gitlab.com/jobgrader/jobgrader-app/commit/89b1470b8c3e7d410d8adb8b3d3a49627af3a496))
* **theme:** Save system default theme ([275e30a](https://gitlab.com/jobgrader/jobgrader-app/commit/275e30ac0b04567f61d0c8c04ce49c12999be874))



# [1.0.0-196](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-195...1.0.0-196) (2024-10-09)



# [1.0.0-195](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-194...1.0.0-195) (2024-10-09)



# [1.0.0-194](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-193...1.0.0-194) (2024-10-09)


### Bug Fixes

* **wallet-connect:** Eth_sendTransaction request body updated ([8213e45](https://gitlab.com/jobgrader/jobgrader-app/commit/8213e4583a2e661cb8c004392356b9a2caa414fc))
* **wallet-connect:** Signing using wallets imported using private key ([da77ebf](https://gitlab.com/jobgrader/jobgrader-app/commit/da77ebf9fe81c8c4ebdf5a1a11d6d4dc894610bc))


### Features

* **jobs:** remove header & footer vom job relevant pages [#97](https://gitlab.com/jobgrader/jobgrader-app/issues/97) ([f057747](https://gitlab.com/jobgrader/jobgrader-app/commit/f0577470abf3f618251a60c495270d391435b054))



# [1.0.0-193](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-192...1.0.0-193) (2024-10-04)



# [1.0.0-192](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-191...1.0.0-192) (2024-10-01)


### Bug Fixes

* **telegram:** Using the telegramcheckdata endpoint to link telegramId with the userId ([e6554d7](https://gitlab.com/jobgrader/jobgrader-app/commit/e6554d7e1a57f272e97036cbaafa753f090f1dc6))



# [1.0.0-191](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-190...1.0.0-191) (2024-09-27)


### Bug Fixes

* **dropbox:** HTTP interceptor exception added for Dropbox get_currect_account API call ([d316e4a](https://gitlab.com/jobgrader/jobgrader-app/commit/d316e4abe56be75eaec91094300a69063a259a7d))



# [1.0.0-190](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-189...1.0.0-190) (2024-09-24)


### Bug Fixes

* **barcode:** add safe are to the FAB buttons on top and botom in the UI on iOS [#151](https://gitlab.com/jobgrader/jobgrader-app/issues/151) ([4b5cba1](https://gitlab.com/jobgrader/jobgrader-app/commit/4b5cba1d97ead6f1dd340577b15ebc18a1c51799))


### Features

* **jobs:** show empty jobs message [#149](https://gitlab.com/jobgrader/jobgrader-app/issues/149) ([d664d35](https://gitlab.com/jobgrader/jobgrader-app/commit/d664d35fe96086e485dcca1f5f4468b37bca5f72))
* **jobs:** show success mesasage once job is done [#150](https://gitlab.com/jobgrader/jobgrader-app/issues/150) ([ecfdc33](https://gitlab.com/jobgrader/jobgrader-app/commit/ecfdc33b2f0b6e6c714b48ea94a714c40175fe61))



# [1.0.0-189](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-188...1.0.0-189) (2024-09-23)


### Bug Fixes

* **polls-quizzes:** Check for SBT NFT Id presence ([df2daff](https://gitlab.com/jobgrader/jobgrader-app/commit/df2daff158981f1badc37fa00f46852148800c5f))
* **pwa-video-recording:** Prevent echo during the recording session ([6963ea8](https://gitlab.com/jobgrader/jobgrader-app/commit/6963ea86586be2b59eacbbc7d4e338f20ee1e069))
* **video:** force media recorder to release data periodically ([29a5e54](https://gitlab.com/jobgrader/jobgrader-app/commit/29a5e547c190849b338eb3cc5932b83835047e3f))


### Features

* **video:** look for best available ([e6e8952](https://gitlab.com/jobgrader/jobgrader-app/commit/e6e8952f71904d767b3cda10e74fe55325b73a8c))



# [1.0.0-188](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-187...1.0.0-188) (2024-09-20)


### Bug Fixes

* **ai-training:** assuere speakIndex is reset on stop ([f72b437](https://gitlab.com/jobgrader/jobgrader-app/commit/f72b437026dbc2aab14b065927f2aa566c40b4d2))
* **ai-training:** assure speak is stoped on page leaving ([6485140](https://gitlab.com/jobgrader/jobgrader-app/commit/64851405995a0b825d5a1b797bbba5981ad4b8c0))
* **ai-training:** don't show any header before data json file is loaded ([51723bf](https://gitlab.com/jobgrader/jobgrader-app/commit/51723bfd08cad64b6bbb08d7de1f6367130d63a0))



# [1.0.0-187](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-186...1.0.0-187) (2024-09-19)


### Bug Fixes

* **ai-training:** use display:none on hidden slides [#138](https://gitlab.com/jobgrader/jobgrader-app/issues/138) [#139](https://gitlab.com/jobgrader/jobgrader-app/issues/139) ([756ccb3](https://gitlab.com/jobgrader/jobgrader-app/commit/756ccb38c9395c372cbea22293b549aad79ac2cd))
* **prompt:** Audio playback works in device silent mode ([bbf04be](https://gitlab.com/jobgrader/jobgrader-app/commit/bbf04be08461d70d9e1ad40bbe559467ac63b72f))



# [1.0.0-186](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-185...1.0.0-186) (2024-09-18)


### Bug Fixes

* **ai-training:** init empty solutions if data field is not present ([003f86a](https://gitlab.com/jobgrader/jobgrader-app/commit/003f86a7f07f33714afd6a6d780a277026be09aa))
* **ai-training:** stop text speak on leaving prompt ([ee59ba0](https://gitlab.com/jobgrader/jobgrader-app/commit/ee59ba08d1eb56246e93a5fdca239f3716c8c6cc))
* **jobs:** assure min max lenght for audio/video are numbers ([9c337d4](https://gitlab.com/jobgrader/jobgrader-app/commit/9c337d46dc10f3bd296cb23620084a54c1e4979b))
* **jobs:** show text on video and audio job as innerHTML ([fb28d03](https://gitlab.com/jobgrader/jobgrader-app/commit/fb28d0361d30943903d1c88cbf43b7220b0cff78))
* **video-recording:** iOS: Reading the recorded video file before uploading ([7116419](https://gitlab.com/jobgrader/jobgrader-app/commit/7116419b8fa37c9f2e9f168716b93ea0231649da))


### Features

* **ai-training:** add new ai prompt with attributes job ([1cc55fd](https://gitlab.com/jobgrader/jobgrader-app/commit/1cc55fd45dbff7bf5695bf3dd8f95888bf8334c5))
* **audio:** upload media for job solutions ([f37bbdc](https://gitlab.com/jobgrader/jobgrader-app/commit/f37bbdc90431564cee8a9127bdb19c99a6f145ad))
* **jobs:** handle job fully asigned or already done by worker ([921a423](https://gitlab.com/jobgrader/jobgrader-app/commit/921a423da9de6eccecf67a6754574c7fdf8f8346))
* **jobs:** upload media direct to S3 bucket using presigend urls ([4117dfc](https://gitlab.com/jobgrader/jobgrader-app/commit/4117dfca6be38ecc5e436ffe0052e2258cbeb410))
* **jobs:** use new exchange oracle with assignment ([ada5461](https://gitlab.com/jobgrader/jobgrader-app/commit/ada5461886b08a5bc7dc7b32b1952047d1e2b513))
* **video:** convert videourl to base64 & extract file extension for uploadign to the solutions ([869b2b4](https://gitlab.com/jobgrader/jobgrader-app/commit/869b2b471aaa96bbe999a955053d71c5f9511ab8))
* **video:** upload media for job solutions ([5a256b1](https://gitlab.com/jobgrader/jobgrader-app/commit/5a256b1ba22113a25f8fa4a9d83b3a618b6d21b6))



# [1.0.0-185](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-184...1.0.0-185) (2024-08-26)



# [1.0.0-184](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-183...1.0.0-184) (2024-08-26)


### Bug Fixes

* **video:** fix start/pause buttons on too short video or on recording a new one ([c361466](https://gitlab.com/jobgrader/jobgrader-app/commit/c36146611476aa77e5a439f294b0d4970fb29bb2))


### Features

* **wallet-detail:** Celo and Polygon Amoy balances are displayed ([543e178](https://gitlab.com/jobgrader/jobgrader-app/commit/543e178e11f09b7ff3f08369aa771f7e02b80d77))



# [1.0.0-183](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-181...1.0.0-183) (2024-08-21)


### Bug Fixes

* **audio:** fix start/pause behaviour ([5ba5fa2](https://gitlab.com/jobgrader/jobgrader-app/commit/5ba5fa282baac70ddef96217f5109a9043fcda01))
* **audio:** just try stop recording when it's really recording ([e573ce4](https://gitlab.com/jobgrader/jobgrader-app/commit/e573ce4acd5ef98e91e46ba36fcb2778c46e0611))
* **audio:** stop recording on page leave ([b311bd4](https://gitlab.com/jobgrader/jobgrader-app/commit/b311bd4c54f1e3c2aaa27355c89923e0375b8542))
* **jobs:** interrupt multiple request on job list page ([b7467f5](https://gitlab.com/jobgrader/jobgrader-app/commit/b7467f5f675a814dbc08596efd1da90e97d73f6e))
* **video:** don't break app if you leave page without ongoing recording ([ca1c5be](https://gitlab.com/jobgrader/jobgrader-app/commit/ca1c5be6d7d7a601e081fd7ea579c6004d8f168d))
* **video:** get text from exchange ([301415f](https://gitlab.com/jobgrader/jobgrader-app/commit/301415f785ed9a1e40e528d6e3c92736d01ff15d))


### Features

* **audio:** add progress bar for min & max recording time ([84e5942](https://gitlab.com/jobgrader/jobgrader-app/commit/84e59427da9da9515541e18b09fb3f3edc725db2))
* **social-media-task:** Telegram task tiles' user interaction added ([d2e706b](https://gitlab.com/jobgrader/jobgrader-app/commit/d2e706bcc81b6b2a6a84cbf3d0d383728b2359d5))



# [1.0.0-181](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-180...1.0.0-181) (2024-08-19)



# [1.0.0-180](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-179...1.0.0-180) (2024-08-16)


### Bug Fixes

* Catch error fetching Moralis NFTs ([4773e6c](https://gitlab.com/jobgrader/jobgrader-app/commit/4773e6cbef5dc42f7fac45192d27ba71fceb0582))
* Generating firebase token on iOS ([bdccd82](https://gitlab.com/jobgrader/jobgrader-app/commit/bdccd824d902d45b233ed3ac6dc1149ef0fd66c2))
* **sqlite:** update addUpgradeStatement method for @capacitor-community/sqlite@6 ([a0c3ff4](https://gitlab.com/jobgrader/jobgrader-app/commit/a0c3ff4ef70af6b94ae6d39b4c27df694921aee9))
* **sqlite:** update addUpgradeStatement method for @capacitor-community/sqlite@6 ([04c0025](https://gitlab.com/jobgrader/jobgrader-app/commit/04c0025bb321f394f9f24b572ff06758109ea173))


### Features

* **video:** add progress bar for min & max recording time ([24fe487](https://gitlab.com/jobgrader/jobgrader-app/commit/24fe487d15424f57024604c770b86b9e55b2214d))
* **video:** video recorder overlays position is updated on scroll event, applicable on iOS ([fb39a5c](https://gitlab.com/jobgrader/jobgrader-app/commit/fb39a5c50ff4bb6804379e0e0b712d12456d3388))


### Reverts

* **video:** use @teamhive/capacitor-video-recorder on iOS ([4035af3](https://gitlab.com/jobgrader/jobgrader-app/commit/4035af3e0a94a159a2a81b15415d940b418098a0))



# [1.0.0-179](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-178...1.0.0-179) (2024-08-05)


### Bug Fixes

* **walletconnect:** User session_request rejection is handled correctly ([be67797](https://gitlab.com/jobgrader/jobgrader-app/commit/be67797c4df837d3fa2913823dc51ed14ff7a368))



# [1.0.0-178](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-177...1.0.0-178) (2024-08-01)


### Bug Fixes

* **login:** pass backup symmetric key as hex to decrypt function ([d794514](https://gitlab.com/jobgrader/jobgrader-app/commit/d794514f230509911d7c145e2839eb49646143ad))



# [1.0.0-177](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-176...1.0.0-177) (2024-07-29)

### Features

+ **social**: Telegram group link for Jobgrader updated

# [1.0.0-176](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-175...1.0.0-176) (2024-07-26)


### Bug Fixes

* **kyc:** show alert window just once instead one per file [#122](https://gitlab.com/jobgrader/jobgrader-app/issues/122) ([57bfad8](https://gitlab.com/jobgrader/jobgrader-app/commit/57bfad8507cff06a10ecaa14542717a5606bf819))



# [1.0.0-175](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-174...1.0.0-175) (2024-07-24)


### Bug Fixes

* **changepassword:** if biometric is not supported, navigate to the change password form page ([ccf880a](https://gitlab.com/jobgrader/jobgrader-app/commit/ccf880a408357b7aa61a995104a4866b6ad45f4b))
* **epn-nft:** Certificates page EPN NFT is processed and displayed as per the standard ([42a7353](https://gitlab.com/jobgrader/jobgrader-app/commit/42a735369b7fa203dabdaf6935ef09d9ab88ad75))
* **walletconnect:** WalletConnect v2 session deletion workaround, prevent multiple empty session_request modal windows ([b7c0e44](https://gitlab.com/jobgrader/jobgrader-app/commit/b7c0e445b654869de60772c2cdff8716e7366f10))


### Features

* **assessment-nft:** Fetching Assessment NFTs from the EPN network ([0a77119](https://gitlab.com/jobgrader/jobgrader-app/commit/0a7711970fa17386ad22ec501d5e4069bb12c79f))
* **http:** de/encrypt API calls centrilized, retry on a 403 BAD X-API-KEY error [#109](https://gitlab.com/jobgrader/jobgrader-app/issues/109) ([b165e68](https://gitlab.com/jobgrader/jobgrader-app/commit/b165e68ef1eda0dbdeb521936e8bfc12035c1480))



# [1.0.0-174](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-173...1.0.0-174) (2024-07-15)


### Bug Fixes

* **audio-labbeling:** add android.permission.RECORD_AUDIO permission ([7d60807](https://gitlab.com/jobgrader/jobgrader-app/commit/7d608072a88679bde6012f28b219df0e90606eb3))
* **job-details:** wrap information text ([f1b2320](https://gitlab.com/jobgrader/jobgrader-app/commit/f1b232008eab36d5e59ad2a22468613bf5b5000b))
* Multiple video elemnt tags are removed ([994a7ba](https://gitlab.com/jobgrader/jobgrader-app/commit/994a7bac0bd869c633faf88f4b46346eada2033a))
* **ocr:** don't fetch next scan when we are at the end ([c957ef7](https://gitlab.com/jobgrader/jobgrader-app/commit/c957ef7d8c6d5b2e79dff8f353a0a3339d1a5f59))
* **video-labeling:** pwa implementation fix ([fda016c](https://gitlab.com/jobgrader/jobgrader-app/commit/fda016cc4e7c2af613eb0eac65b1f9e8e3bb7b0c))
* **video-labelling:** use pwa implementation on android ([c1db76a](https://gitlab.com/jobgrader/jobgrader-app/commit/c1db76af97e920f6ac254478e4d5fd9eca08e7c4))



# [1.0.0-172](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-171...1.0.0-172) (2024-07-12)


### Bug Fixes

* Converting old unverified wallet to a monitoring wallet ([b6bb65c](https://gitlab.com/jobgrader/jobgrader-app/commit/b6bb65c57fc19d9de4c12625a756121a4adbf095))
* Navigation post verified wallet change ([400f56f](https://gitlab.com/jobgrader/jobgrader-app/commit/400f56fc806b0200b2e4291e50e4a8cfabd309a5))
* Post verified wallet change color update ([41685cd](https://gitlab.com/jobgrader/jobgrader-app/commit/41685cd53577905ecd433378cff620a0f3008335))
* Updating the new verified wallet address ([72ca22a](https://gitlab.com/jobgrader/jobgrader-app/commit/72ca22a5c33764d2a8ebba6ca52f2a497a1c9400))


### Features

* Provide option to reactivate an inactive verified wallet or to generate a new one ([06eea00](https://gitlab.com/jobgrader/jobgrader-app/commit/06eea002a9ed7367aaa2ff1bdd1a9790686cf14a))



# [1.0.0-171](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-169...1.0.0-171) (2024-07-09)


### Bug Fixes

* **nft:** check for a working ipfs gateway ([6865a04](https://gitlab.com/jobgrader/jobgrader-app/commit/6865a04f8a3283273eea9088a24f65a82f3d1867))
* Conversion of Wallet to a Verified Wallet on a second device ([d1b62433](https://gitlab.com/jobgrader/jobgrader-app/-/commit/d1b62433974bd1aa56b2a8cd17ccd6f66ff2b8b2))


# [1.0.0-169](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-168...1.0.0-169) (2024-07-05)


### Bug Fixes

* **android:** some deep/app links didnt work, wrong configuration ([54c76c9](https://gitlab.com/jobgrader/jobgrader-app/commit/54c76c93a2b07ede73635f1d9e7700fc81452e17))
* **api:** handle secrets response as text ([39a6aed](https://gitlab.com/jobgrader/jobgrader-app/commit/39a6aed0e7d39b0fcbe01495d16d42f762d5e891))
* **api:** whitelist endpoints which don't need authentication ([a911b8d](https://gitlab.com/jobgrader/jobgrader-app/commit/a911b8de4f4b0ee85497000e24e229f35cda297d))
* checkjobsolution response: success: false handling ([bff9035](https://gitlab.com/jobgrader/jobgrader-app/commit/bff9035abd67e92445e7037ae5b0b534d670acc8))
* **country-selector** error loading flags ([b72f96e](https://gitlab.com/jobgrader/jobgrader-app/commit/b72f96e75d9203cd3eced109720ebbedf056ba5f))
* **css:** add stepped colors for light and dark theme [#14](https://gitlab.com/jobgrader/jobgrader-app/issues/14) ([e05366d](https://gitlab.com/jobgrader/jobgrader-app/commit/e05366d32200299822b8f5213ce1b4fee56dd133))
* **dashboard:** check if social media task is valid and add a taskId if missing ([73d9f07](https://gitlab.com/jobgrader/jobgrader-app/commit/73d9f0787ce01077c7d1485fc374811fb8dda500))
* Displaying safe NFTs ([b73aa16](https://gitlab.com/jobgrader/jobgrader-app/commit/b73aa1618ca482db52fed3a7e28b5bff9039d665))
* Ensure that the Moralis SDK is initialized before fetching the NFTs ([9d46be9](https://gitlab.com/jobgrader/jobgrader-app/commit/9d46be9bca8eff5d362474616085faf4a4d6782b))
* **http:** wrong check for authorization header, refresh token on 401 and retry api call ([f8fcbbe](https://gitlab.com/jobgrader/jobgrader-app/commit/f8fcbbe4856bb8b57863187cf9c4a4162b496343))
* **jobs:** don't show missing wallet message in demo mode ([dd6dbf5](https://gitlab.com/jobgrader/jobgrader-app/commit/dd6dbf575990b8ab8d9890203c118c41046e6014))
* Navigate to the wallet-list page after renaming the wallet ([295aef7](https://gitlab.com/jobgrader/jobgrader-app/commit/295aef7ba0e722da2f05ee6d877241eb110b8a6e))
* NFT image URL from Gnosis chain is displayed ([bf10436](https://gitlab.com/jobgrader/jobgrader-app/commit/bf10436288cd296a94781b9b43bb6946a92ff75d))
* signout from firebase auth on a factory reset ([42455cf](https://gitlab.com/jobgrader/jobgrader-app/commit/42455cf12aac2564e1af05a1136d53d9361e6ef8))


### Features

* add demo for librechat ([8bae11a](https://gitlab.com/jobgrader/jobgrader-app/commit/8bae11ab5e48020d4bf0e48b338318139e1df507))
* add new universal/app link for web.jobgrader.app ([cadbbf2](https://gitlab.com/jobgrader/jobgrader-app/commit/cadbbf208023b4f75268eb3f3b68242bb0959ac4))
* Add search bar to the language selectors of additional info form ([4820af7](https://gitlab.com/jobgrader/jobgrader-app/commit/4820af7b543ffe55122e397c9d127c232849054f))
* Assessment NFT minted on Gnosis chain and displayed in the NFT listing page and Certificates page ([0d9c18b](https://gitlab.com/jobgrader/jobgrader-app/commit/0d9c18b4a52f14fb1b44e3b74850bf4bca18c5f6))
* Display THXC token and points balances in the designated page ([463211a](https://gitlab.com/jobgrader/jobgrader-app/commit/463211ae5277a840a3c0871fe5ac21c3f20a5e46))
* firebase cloud function checkjobsolution checks if the assessment quiz solution is correct, mintsoulbound checks and mints if necessary ([ac71ef7](https://gitlab.com/jobgrader/jobgrader-app/commit/ac71ef7d5e68a49144e048d9ebfc23400119ff7f))
* ISO-639 languages added as options for proficencies, arranged in ascending order of code ([132bd63](https://gitlab.com/jobgrader/jobgrader-app/commit/132bd633f4cd77847a28eb17b57451abc362e54f))
* **job-details:** add quality check property & improve translations ([129ccd1](https://gitlab.com/jobgrader/jobgrader-app/commit/129ccd17c1208be1b350eb872d7845fcaa7af32f))
* **jobs:** before auto navigating back wait for everything be complete ([ead9629](https://gitlab.com/jobgrader/jobgrader-app/commit/ead96294bc491c7da8cbec1b51302ff1fed4e0c0))
* **jobs:** check for kyc from job detail/how to page and show wallet... ([7b51167](https://gitlab.com/jobgrader/jobgrader-app/commit/7b51167cf6ef151a0a3070ac2788e8ccf24301c7))
* **jobs:** send certificates to checkJobSolutions and just if it is available and not in demo mode ([d84a8b8](https://gitlab.com/jobgrader/jobgrader-app/commit/d84a8b88f0984cb9ed19e4df8236930450e9a8ac))
* **jobs:** skip how to page if missing [#49](https://gitlab.com/jobgrader/jobgrader-app/issues/49) ([b952269](https://gitlab.com/jobgrader/jobgrader-app/commit/b952269d4c0b2aa20ec02728bae288f7fdd1cb7f))
* **referral:** show qr code on desktop & show relative store button per platform ([7988fd5](https://gitlab.com/jobgrader/jobgrader-app/commit/7988fd54590aff460e2464074c4f48eebb16079a))
* Social Media Task section has the referral entry ([46679b9](https://gitlab.com/jobgrader/jobgrader-app/commit/46679b907850b94214adeffe44cce1de499e261d))
* Unverified Wallet option removed, redesigning on the wallet page before initiation ([8955843](https://gitlab.com/jobgrader/jobgrader-app/commit/8955843b0d7a5101d5676401c40842e4f98afe2f))



# [1.0.0-168](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-166...1.0.0-168) (2024-05-17)


### Bug Fixes

* **api:** set responseType to text for getUserWeb3WalletAddress ([5313756](https://gitlab.com/jobgrader/jobgrader-app/commit/5313756aa8c7f6d5a2f334f865a13e0e1f754bca))
* **auth:** check if user exists ([50cd87e](https://gitlab.com/jobgrader/jobgrader-app/commit/50cd87e3e3f5efc2bce05310590a6a906c2d07c2))
* **auth:** prevent user from login with wrong password after a password change ([86577a8](https://gitlab.com/jobgrader/jobgrader-app/commit/86577a836bdc6035ecae7e2a1b542b2be57a2796))
* **auth:** wait for isAuthenticated until user is fired once ([751f033](https://gitlab.com/jobgrader/jobgrader-app/commit/751f03314b015a0656644fdc6f1c796682cc24c5))
* **aut:** set persistence layer on native apps ([6918733](https://gitlab.com/jobgrader/jobgrader-app/commit/691873387026d0d68e08b59b42d9cbfc36d067eb))
* **broadcast:** Missing timestamp parameter in empty array ([ef5dbfc](https://gitlab.com/jobgrader/jobgrader-app/commit/ef5dbfc8639f2a4066c275d64a7810a800c55d45))
* **changepassword:** Spinner gets dismissed after changing the password from the settings page ([3123419](https://gitlab.com/jobgrader/jobgrader-app/commit/31234199342692369b20d7b35e02bc8d46bdee1f))
* close script tag at the right place ([dd56e17](https://gitlab.com/jobgrader/jobgrader-app/commit/dd56e178008703013a55e50880db28e0bd0ce5d0))
* Dismiss loader before displaying toast notification ([e624d55](https://gitlab.com/jobgrader/jobgrader-app/commit/e624d5549e8fed5546cf9af8d2a74a07c4beb5d7))
* **ethereum:** Sepolia is the default test network for Ethereum, Sepolia network added ([68edddd](https://gitlab.com/jobgrader/jobgrader-app/commit/68eddddcc88696e74b17b86e60e56c50a5348d48))
* goerli provider is down use another provider instead ([9468043](https://gitlab.com/jobgrader/jobgrader-app/commit/94680432113743e33dd7b0c6add80ea5d50582c9))
* **http:** clone the original headers in the http interceptor ([f08c8c4](https://gitlab.com/jobgrader/jobgrader-app/commit/f08c8c4cb95490d5ea0a5782c8626410d56dd9c7))
* **job-howto:** show howto from data ([c28c6cd](https://gitlab.com/jobgrader/jobgrader-app/commit/c28c6cda87d42fad9c9a370d998283225c93f294))
* Missing user ID at the time of fetching user group ([c421e81](https://gitlab.com/jobgrader/jobgrader-app/commit/c421e81efe2f8e7ee5b3fe3eff614a9eae4aff5c))


### Features

* **auth:** handle 401 in http interceptor and try to refresh token ([bc27ae3](https://gitlab.com/jobgrader/jobgrader-app/commit/bc27ae3f93e755f7d2c1b7b6c611166616b1316b))
* Edit Profile page: Both basicdata and additional information are clubbed into one form element ([23c9543](https://gitlab.com/jobgrader/jobgrader-app/commit/23c9543adb2a1715ee9fe784dabe1af34b1b3521))
* **jobs:** add demo capability to jobs ([d10dadd](https://gitlab.com/jobgrader/jobgrader-app/commit/d10dadd3bef164d33e25f0abba336653862c8228))
* **jobs:** add iframe game job ([a1509f8](https://gitlab.com/jobgrader/jobgrader-app/commit/a1509f803262c344da3233a34be4d69d3c44e7ff))
* **jobs:** add new description and howto page ([7c5a70c](https://gitlab.com/jobgrader/jobgrader-app/commit/7c5a70c4814ddb2d9e5fc41071ec7893b585cf45))
* **jobs:** choose if after job is done go back to listing page [#3](https://gitlab.com/jobgrader/jobgrader-app/issues/3) ([ff1b953](https://gitlab.com/jobgrader/jobgrader-app/commit/ff1b953869bc7198c12ef283844a9b0202710790))
* Limit on the number of nationalities and languages selected ([5ad52d4](https://gitlab.com/jobgrader/jobgrader-app/commit/5ad52d4dcb27f4e3f4abebd38b7d3e4b3a716713))
* **referral:** add intermediary page for referrals ([df34777](https://gitlab.com/jobgrader/jobgrader-app/commit/df34777ff157195c70a5ee8528e8f0e265f2fa33))
* **sinup:** intermediary page to create a referral after signup process ([2d8af43](https://gitlab.com/jobgrader/jobgrader-app/commit/2d8af43ee7b1e84094dc29ee4b84952b5a66e8d0))
* Tech User Mode bypasses KYC requirement to access Jobs ([985da57](https://gitlab.com/jobgrader/jobgrader-app/commit/985da57260f036222d0e358a1819a20daf08dd77))
* use firebase auth for user authorization ([e45adb5](https://gitlab.com/jobgrader/jobgrader-app/commit/e45adb5a2a896c87bf76e9b384be8d35914434e1))



# [1.0.0-167](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-166...1.0.0-167) (2024-03-28)


### Bug Fixes

* goerli provider is down use another provider instead ([9468043](https://gitlab.com/jobgrader/jobgrader-app/commit/94680432113743e33dd7b0c6add80ea5d50582c9))



# [1.0.0-166](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-165...1.0.0-166) (2024-03-28)


### Bug Fixes

* **fortune:** finalize loading correctly in all possible scenarios ([65b5ff7](https://gitlab.com/jobgrader/jobgrader-app/commit/65b5ff7a6aa433c0a3827c09d6e2f3b5596ff840))
* **hcaptcha-job:** add missing i18 labels ([9411673](https://gitlab.com/jobgrader/jobgrader-app/commit/9411673d9e9b8c164e7f262f5fbe2a2b1891508c))
* **hcaptcha-job:** set app language for job ([b437798](https://gitlab.com/jobgrader/jobgrader-app/commit/b437798d741c6e58f8f4ce1d8ed03b206b5450d1))


### Features

* **video-recording:** PWA can support video recording job, camera stream can be converted to a matroska video format ([139b40e](https://gitlab.com/jobgrader/jobgrader-app/commit/139b40ee7ffbda83e5e7b7161272fb9b148706ea))



# [1.0.0-165](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-164...1.0.0-165) (2024-03-21)


### Bug Fixes

* **ocr-job:** dont overlay modal header with device status bar ([2b92acc](https://gitlab.com/jobgrader/jobgrader-app/commit/2b92acc3c953b41db25b93e024145810a66da2e3))
* **ocr-job:** fix keyboard issue on input focus ([0a19217](https://gitlab.com/jobgrader/jobgrader-app/commit/0a19217eeb55835b52f064b3e76724d6388c29b9))
* **ocr-job:** set modal status after dismissing with esc key ([72b1fc3](https://gitlab.com/jobgrader/jobgrader-app/commit/72b1fc361982bfd59bfcdbf76e83312033ce3361))


### Features

* **hcaptcha:** start first job when page loaded ([786a041](https://gitlab.com/jobgrader/jobgrader-app/commit/786a04164135b47d48ffc4ab41a5b2af1f91994e))
* **ocr-job:** enable pinch to zoom on zoomed ocr scan ([2c4a1ba](https://gitlab.com/jobgrader/jobgrader-app/commit/2c4a1bab9c13e94d700a20815b953615fb65f1ca))



# [1.0.0-164](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-163...1.0.0-164) (2024-03-21)


### Bug Fixes

* **ai-training:** use right i18 label for copy to clipboard ([1a696e4](https://gitlab.com/jobgrader/jobgrader-app/commit/1a696e4c868e1701120166b60112d54f75d7ca74))
* **fortune:** show empty job list on a bug or no available jobs ([852d3f7](https://gitlab.com/jobgrader/jobgrader-app/commit/852d3f7847a1e5e7e2df152e030e5b09cfe0053b))
* **push-notifications:** Tag detection bug fix as per the modified notification structure ([510207c](https://gitlab.com/jobgrader/jobgrader-app/commit/510207c36bb6e654d8c1af3197691f4d4d31302f))
* **push-notifications:** Update the Content-Type of the token update request ([e18615c](https://gitlab.com/jobgrader/jobgrader-app/commit/e18615c5d1404762ce09ec6fc38d1decd095a88a))


### Features

* add new audio laballing job ([d7fda99](https://gitlab.com/jobgrader/jobgrader-app/commit/d7fda997733f3d25c1fea1ddff402ff877f05533))
* add new jobgrader hcaptcha job ([78deaaa](https://gitlab.com/jobgrader/jobgrader-app/commit/78deaaa741e157697b3ca1d24a201215a9022b6a))
* add new video labelling job ([aa24e92](https://gitlab.com/jobgrader/jobgrader-app/commit/aa24e9203f4f047da91482fbd72e3f6d41f25cd4))
* add ocr job type ([9c99452](https://gitlab.com/jobgrader/jobgrader-app/commit/9c99452677f19943cbbd3e35bff8dbba0797987f))
* **ai-training:** add text speak, copy, up and down vote, improve UI ([9f32b99](https://gitlab.com/jobgrader/jobgrader-app/commit/9f32b9976133d83ed18aec0ddec8585ca8092ebe))
* **validation:** initiate the process from the verifier by entering the holders username ([3b100b9](https://gitlab.com/jobgrader/jobgrader-app/commit/3b100b936dfcb512b65554cfbba244f60cc2cbe9))



# [1.0.0-163](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-162...1.0.0-163) (2024-03-08)


### Bug Fixes

* **validate-vc:** Using existing smart agent resolver for validating issuer and holder did ([b486bfc](https://gitlab.com/jobgrader/jobgrader-app/commit/b486bfcad86f21b06a536841179a70d9f7b76bed))


### Features

* **validate-vc:** The global scanner can process a VC and the verifier can obtain information about the holders VC status ([48c6fbf](https://gitlab.com/jobgrader/jobgrader-app/commit/48c6fbfb7e791fc74d04638d5a2dd2838c6083fb))



# [1.0.0-162](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-161...1.0.0-162) (2024-03-06)


### Features

* **discord:** Obtain the latest invite URL of a discord server ([e343019](https://gitlab.com/jobgrader/jobgrader-app/commit/e343019b3f272c502c9bb2793ee43f6c58fa4d7e))
* **fortune:** add ai training job ([0d2ef5a](https://gitlab.com/jobgrader/jobgrader-app/commit/0d2ef5aa0d43dbf58213490756efa05a30f383c0))
* universal and deep link ([cab0714](https://gitlab.com/jobgrader/jobgrader-app/commit/cab07140d6c29a996635ed85e59c2fcdf450d2ac))
* **validate-vc-vp:** Trying vade library, and validation using endpoints in the smart agent ([27f24b0](https://gitlab.com/jobgrader/jobgrader-app/commit/27f24b0b066ea9f45a320849f8cdb9339f67fd55))



# [1.0.0-161](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-160...1.0.0-161) (2024-02-24)


### Bug Fixes

* **assessment-vc:** Check for uniqueness implemented ([17febef](https://gitlab.com/jobgrader/jobgrader-app/commit/17febeffc96920b87cdfcf4a1097888e00a4b92f))
* **assessment-vc:** Deeplink URL check fixed ([1cf72ac](https://gitlab.com/jobgrader/jobgrader-app/commit/1cf72ac226e2d9f9e71ad8330f3da486b243df2c))
* **caridentity:** Date formating issue fixed ([f252c48](https://gitlab.com/jobgrader/jobgrader-app/commit/f252c48299a0362a8c5eb2816ad95f75e66e0290))
* **caridentity:** Post deeplink processing navigation bug fixed ([689302f](https://gitlab.com/jobgrader/jobgrader-app/commit/689302f61222cc421cffbaf01ea63493f0eb90de))
* **clipboard:** Copy wallet address using Capacitor plugin ([ecd6a75](https://gitlab.com/jobgrader/jobgrader-app/commit/ecd6a75d8669f8877da2a2b17d7c980e21fea81d))
* **cloud-backup:** VC and VP backup includes the proof object ([76debdf](https://gitlab.com/jobgrader/jobgrader-app/commit/76debdf7756f12ea974ea9b2131bd80590566332))
* **discord:** filter tasks based on userId ([91ef6ed](https://gitlab.com/jobgrader/jobgrader-app/commit/91ef6ed13809565ba14a09442768a08a33b90cd1))
* **human-ai:** keep navigation on home icon ([66fb86a](https://gitlab.com/jobgrader/jobgrader-app/commit/66fb86a66bf87d959c0c6244727aa2dfdc4ba6b6))
* **legal-documents:** Prevent displaying multiple VCs / VPs in the document modal window ([d5263ab](https://gitlab.com/jobgrader/jobgrader-app/commit/d5263abb799f8eb182fdf6f2261a694105194e16))
* **send-crypto:** Updated the URL to obtain the XDAI gas price ([a4e30fe](https://gitlab.com/jobgrader/jobgrader-app/commit/a4e30fed0a69677e88a8cb2c91dbe25ccf282cf4))
* **social-media-tasks:** Improper imports ([b2441d0](https://gitlab.com/jobgrader/jobgrader-app/commit/b2441d0ecf7544da5a779ae9df777ac8c60e7182))
* **user-activities:** Descending order of user activities are presented ([4adcb62](https://gitlab.com/jobgrader/jobgrader-app/commit/4adcb62a7dd2693bcba72520f1d7cd6223706cca))
* **wallet-card:** Color of the wallet-card after import is set to a constant one ([31b7959](https://gitlab.com/jobgrader/jobgrader-app/commit/31b79599cdfd049ce445fd2be687cc727dba582c))


### Features

* **assessment-vc:** Display Verifiable Presentation / Credential as a QR code ([c460a27](https://gitlab.com/jobgrader/jobgrader-app/commit/c460a275bc0d5397e65ff43744a437a54724b4ca))
* **assessment-vc:** Fetching the Assessment Certificate by deeplink ([5e6868d](https://gitlab.com/jobgrader/jobgrader-app/commit/5e6868da18fffa62db7addc87867487c17944ace))
* **assessment-vc:** Fetching the Assessment Certificateby scanning a QR code ([b79ce11](https://gitlab.com/jobgrader/jobgrader-app/commit/b79ce11b311aeed537a90eec87500a14507a8725))
* **did:** Universal DIF DID Resolver ([199d2a0](https://gitlab.com/jobgrader/jobgrader-app/commit/199d2a09ef0e47ac6a3f70fd7670c41d91140f68))
* **fortune:** add quiz job ([9e42f19](https://gitlab.com/jobgrader/jobgrader-app/commit/9e42f19c74e1e1fc272758220977c86fa289f40f))
* **human-ai:** add ai powered 3d human library ([7b2b799](https://gitlab.com/jobgrader/jobgrader-app/commit/7b2b799e77b2986d81607ebcee26dbd7e9a88857))
* **human-ai:** show info message ([09807c5](https://gitlab.com/jobgrader/jobgrader-app/commit/09807c5f85fd0260e47d0c133383451d3aea22a7))
* **nfts:** Alchemy NFT list contains decentraland wearables, new key translations added ([cada997](https://gitlab.com/jobgrader/jobgrader-app/commit/cada997673aa0d41d03be22ef19bac14f487bd68))
* **nfts:** Using Moralis instead of OpenSea API to fetch all NFTs, includes NFTs not listed under OpenSea ([3b37254](https://gitlab.com/jobgrader/jobgrader-app/commit/3b3725428092c9a619706fc64721b7b99115fdf6))
* **quiz:** Generate assessment VC after the user completes the quiz with correct answers ([e036cee](https://gitlab.com/jobgrader/jobgrader-app/commit/e036cee5f42bbc48b207ce5bf39cd13b2c28c2f7))
* **send-crypto:** Sending tokens, transaction management with wallets imported using private key ([bca7296](https://gitlab.com/jobgrader/jobgrader-app/commit/bca7296cb459ae2630e533f0c01caf886d8edc27))
* **social-media-tasks:** cache tasks with akita ([e096682](https://gitlab.com/jobgrader/jobgrader-app/commit/e096682002f63d8e1056e2bfc85d03df76148a2e))
* **social-media-tasks:** cache tasks with akita ([caeb337](https://gitlab.com/jobgrader/jobgrader-app/commit/caeb33774752ea1213548328a60fa1971f0f1b8f))
* **social-media-tasks:** Display tasks in ion-slides on home page ([1cdecf4](https://gitlab.com/jobgrader/jobgrader-app/commit/1cdecf4e662ea2675177f8766757983977cd8a57))
* **social-media-tasks:** Display THXC balance with social media task earnings ([3f86670](https://gitlab.com/jobgrader/jobgrader-app/commit/3f86670c46f4ee83726609fddb70ffc93ebb018b))
* **social-media-tasks:** Endpoints to obtain the tasks and user status, and update status ([28b5da6](https://gitlab.com/jobgrader/jobgrader-app/commit/28b5da6e09976257aef88b4a8740ab9d7e365500))
* **social-media-tasks:** Posting on Discord channel and following Discord server ([b86b69c](https://gitlab.com/jobgrader/jobgrader-app/commit/b86b69cfb3f8da2c565df13fd8fa30d37410d413))
* **social-media-tasks:** Using SecureStorage for caching until Akita Storage issue is fixed ([54bc802](https://gitlab.com/jobgrader/jobgrader-app/commit/54bc8028b47a76bf07d7c1ee744ed5afe6590def))
* **swiper-job:** add tap gesture to like the the proposal ([916d570](https://gitlab.com/jobgrader/jobgrader-app/commit/916d570bc55cd7f0b5d0e41ae601dcebfb86c879))
* **user-activities:** Following activity trackers are added: Wallet creation, import, follow, KYC data obtained, Backup of VC and VP ([6767c92](https://gitlab.com/jobgrader/jobgrader-app/commit/6767c920ce82f2ed5c436031acbd68351557c01a))
* **validation:** Endpoints to validate VC and VP, barcode scanner can recognize VC and VP ([22eeeee](https://gitlab.com/jobgrader/jobgrader-app/commit/22eeeeef99098dfe49e31e870e74cdafcda75e67))
* **wallet:** Import wallet using private key ([71ea700](https://gitlab.com/jobgrader/jobgrader-app/commit/71ea700bf6f08fff32d214160c7b45003ad8943d))



# [1.0.0-160](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-159...1.0.0-160) (2024-01-18)


### Features

* **fortune:** auto navigte back after job is solved ([ad80a8e](https://gitlab.com/jobgrader/jobgrader-app/commit/ad80a8e798d5574aa47621c8854c5112b370ffbd))



# [1.0.0-159](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-158...1.0.0-159) (2024-01-18)


### Bug Fixes

* **google-drive:** Navigation bug fix post upload ([170bb63](https://gitlab.com/jobgrader/jobgrader-app/commit/170bb63b0bc3f302411665efe5b6bf17970e1651))
* **swiper-job:** disable scroll on card ([bbcba49](https://gitlab.com/jobgrader/jobgrader-app/commit/bbcba495b6b5595197a3a26139f9d725ca1ab3e7))



# [1.0.0-158](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-157...1.0.0-158) (2024-01-17)


### Bug Fixes

* **ab-job:** right size of images in ab-job ([75c9b9e](https://gitlab.com/jobgrader/jobgrader-app/commit/75c9b9e65b53aa849b3849acd7948d0b709e8714))
* **fortune:** show region on light theme ([9df2ad8](https://gitlab.com/jobgrader/jobgrader-app/commit/9df2ad85ce36db7599bb38f4a2b4972cf21c2f4a))
* **registration:** Button text-transform bug fix ([28968c1](https://gitlab.com/jobgrader/jobgrader-app/commit/28968c172c5aaea8eb7472330de83c92f7254f59))


### Features

* **ab-job:** border animation on click ([31ffa48](https://gitlab.com/jobgrader/jobgrader-app/commit/31ffa48dcbd41d5448a2176260b886e40a4ea93d))
* **fortune:** add info button for description to images ([16fd19c](https://gitlab.com/jobgrader/jobgrader-app/commit/16fd19c20bcc364f72d9049ffed60029eb474612))



# [1.0.0-157](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-156...1.0.0-157) (2024-01-16)


### Bug Fixes

* **android:** splash screen fix for android 11 and less ([d676d69](https://gitlab.com/jobgrader/jobgrader-app/commit/d676d697c03563cc3d27bf7dddd6e8aab73fc4ab))
* **fortune:** fix navigate back from price guess page ([c0a1a41](https://gitlab.com/jobgrader/jobgrader-app/commit/c0a1a41b87b0a215d49d8c89d5328f1b55a4a780))
* **fortune:** show right description on price guess job ([2fee74c](https://gitlab.com/jobgrader/jobgrader-app/commit/2fee74cb17c0fb5ae5f2690509b2368510333db6))
* **wallet-connect:** KYC request parsing and info display bug fix ([f51eb29](https://gitlab.com/jobgrader/jobgrader-app/commit/f51eb29beaca3914682c1238084ccc3587d7016f))


### Features

* **fortune:** add ab job ([59c2854](https://gitlab.com/jobgrader/jobgrader-app/commit/59c285481ffeda26fa22b851cd2b787b42738672))
* **fortune:** add rating job ([900aa0c](https://gitlab.com/jobgrader/jobgrader-app/commit/900aa0c582c57285024097c2e71aea1c48e25adc))
* **log:** set loglevel with environment files ([7e5707b](https://gitlab.com/jobgrader/jobgrader-app/commit/7e5707bed118f23252d51497d2a408c0c5ddd593))



# [1.0.0-156](https://gitlab.com/jobgrader/jobgrader-app/compare/1.0.0-64...1.0.0-156) (2024-01-12)


### Bug Fixes

* **android:** enable compatibility mode for splashscreen ([af7daed](https://gitlab.com/jobgrader/jobgrader-app/commit/af7daed353edb3a2a99e0c3e2c107daa1a74935f))
* **api:** get the right basictoken from storage ([b98ba7c](https://gitlab.com/jobgrader/jobgrader-app/commit/b98ba7c0aab5dbb4e743df74e0a7e3e06d1395e2))
* **app-links:** WalletConnect functionality working on iOS and Android, deeplink subscription method is changed to Capacitor App addEventListener ([5560695](https://gitlab.com/jobgrader/jobgrader-app/commit/55606952b3d88479a8b895acb80783ca7eed43c5))
* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([ac7d918](https://gitlab.com/jobgrader/jobgrader-app/commit/ac7d9183a8b80d8a6b960fe816d4e13f6f3be77f))
* **authentication:** bug fix for correct username and incorrect password ([cc8540d](https://gitlab.com/jobgrader/jobgrader-app/commit/cc8540d22159d5f72f2738d146cd0b63271e2da6))
* **barcode-scanner:** hide navigation/show just where it exists ([2779f9d](https://gitlab.com/jobgrader/jobgrader-app/commit/2779f9d269743e0b0df070caa6e55c8bd75c19be))
* **barcode:** open settings app if the user has not consented to camera usage ([6c788e0](https://gitlab.com/jobgrader/jobgrader-app/commit/6c788e0c62f3e8a729a71fdaac0b1fe1ca8bec5a))
* **barcode:** rename and load module ([06bce4d](https://gitlab.com/jobgrader/jobgrader-app/commit/06bce4d962ed9834ba9c13ac20d754d697b1cdfd))
* **broadcast:** issue with timestamp storage fixed ([1b33e9b](https://gitlab.com/jobgrader/jobgrader-app/commit/1b33e9b374dade9457295dc486ec67bcb11a196c))
* **browse-mode:** better UX when the user is in browse mode, correct DP and alert windows are displayed ([2ae5da8](https://gitlab.com/jobgrader/jobgrader-app/commit/2ae5da856fe0e0d8ade73bf7b644be5e28efe0ed))
* **bug:** Wallet Card color stays constant ([aa3e0fe](https://gitlab.com/jobgrader/jobgrader-app/commit/aa3e0fe663c581ef99a6571c070e7dee50921f78))
* **bug:** Wallet Card color stays constant ([e4b40a5](https://gitlab.com/jobgrader/jobgrader-app/commit/e4b40a5e5c0a10579056a0184ec1203d0273e7f0))
* **camera:** add pwa-elements to support camera on browser ([3913c63](https://gitlab.com/jobgrader/jobgrader-app/commit/3913c63c1e0ac5db1b9ec48d2fa6df62fc5cde15))
* **capacitor:** custom origin, permission string localization, new firebase plugin ([d061114](https://gitlab.com/jobgrader/jobgrader-app/commit/d061114a4297db26d9097364b973d7762edffe1b))
* **chart:** use the right dataSet, remove plain js func ([b4c0414](https://gitlab.com/jobgrader/jobgrader-app/commit/b4c0414d5f06a220cef5252ac8925980b698a867))
* **chart:** use the right dataSet, remove plain js func ([f11a5cd](https://gitlab.com/jobgrader/jobgrader-app/commit/f11a5cd216616e8ed13ccfac3a035b89c2a58a77))
* **console:** Logs disabled for production ([0e35eca](https://gitlab.com/jobgrader/jobgrader-app/commit/0e35ecac87deb3452413d69eb2beee1f5e507a0b))
* corporate language for Web2 & Web3 ([f1a4521](https://gitlab.com/jobgrader/jobgrader-app/commit/f1a4521f9e6af270c379ba231db9b850e47d4d8f))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([0eb0aee](https://gitlab.com/jobgrader/jobgrader-app/commit/0eb0aee8121f0a8d248243f654407918b6ba1247))
* **dashboard:** show the right navigation point after scanner is closed ([49329cb](https://gitlab.com/jobgrader/jobgrader-app/commit/49329cbabc89e9e516d6067b2e623996ae45ca7c))
* **deeplink), feature(dropbox:** Deeplink issue with Dropbox fixed, Dropbox available on iOS ([5c85e0d](https://gitlab.com/jobgrader/jobgrader-app/commit/5c85e0d02c3bb02ea99394f116c3452a864200f1))
* **deeplink:** data sharing process: /ageverification: post confirmation navigation fixed ([ac94ced](https://gitlab.com/jobgrader/jobgrader-app/commit/ac94ced9f733d8592e35ccb4aeace7f6db3c84b8))
* **deeplink:** Deprecated subscriptionn handling updated ([26ead75](https://gitlab.com/jobgrader/jobgrader-app/commit/26ead75c32a1e7ce823ea741ccb51a50da0e27c2))
* **deeplink:** format fixed for pretix and ticket methods ([873466f](https://gitlab.com/jobgrader/jobgrader-app/commit/873466f25382de83c57aa6e817a3375f7c62f9c4))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([b3ec773](https://gitlab.com/jobgrader/jobgrader-app/commit/b3ec773c950529902f9b6011159aa942e15923cc))
* **design:** cards width in the swiper is now set to maximum available ([8be8213](https://gitlab.com/jobgrader/jobgrader-app/commit/8be82138beb4a6a6bf35a8d85f8b48d07be1ae0f))
* **design:** SVG icons around the chart ([eadefa0](https://gitlab.com/jobgrader/jobgrader-app/commit/eadefa01a19a6200de9cea97f9a7ae24d71fc002))
* **design:** SVG icons around the chart ([ef812bf](https://gitlab.com/jobgrader/jobgrader-app/commit/ef812bf7a9f09f36ae392fe0f24063645504790f))
* **did-comm:** inject the service global for now ([b960003](https://gitlab.com/jobgrader/jobgrader-app/commit/b960003be1a2bc45c79137e78894a48205cd0612))
* **email:** Disposable mail ID detection during the registration process ([effd8e0](https://gitlab.com/jobgrader/jobgrader-app/commit/effd8e0780b9f818ff549099c5653ff6a9de5786))
* **file-backup:** Use the correct prefix from the environment file for the encrypted Seed Phrase backup ([6127de3](https://gitlab.com/jobgrader/jobgrader-app/commit/6127de3be4fcfcc6f9cebd381971e457e32f56a4))
* **firebase:** new config file added ([d0bb21f](https://gitlab.com/jobgrader/jobgrader-app/commit/d0bb21fcd5619340d141813d86f0a5d0b4885fda))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([1ab4bae](https://gitlab.com/jobgrader/jobgrader-app/commit/1ab4baea2aaae71c3ac4954de18de94d05e77172))
* **google-drive:** Create folder API call bug fixed ([ca72a89](https://gitlab.com/jobgrader/jobgrader-app/commit/ca72a89c9d69a8d329a201c1183eb0ae81362712))
* **google-drive:** Prevent crashing of the iOS app upon initiating Google Auth ([cbb1820](https://gitlab.com/jobgrader/jobgrader-app/commit/cbb18200b6b5bd13a528e8083bc822c62744e2e4))
* **google-drive:** Prevent crashing of the iOS app upon initiating Google Auth ([852c383](https://gitlab.com/jobgrader/jobgrader-app/commit/852c3835f7bf18dc9fb6a59683ceae9ef135b5fd))
* **google-drive:** Using capacitor plugin for authenticating user's Google Account: Tested on Android: Working as expected ([fd574c4](https://gitlab.com/jobgrader/jobgrader-app/commit/fd574c413786862202bf1edb8b5df791e30f7114))
* **hcaptcha:** make hcaptcha type safe ([b66a4b8](https://gitlab.com/jobgrader/jobgrader-app/commit/b66a4b8e42029bc8f56390234fef17ec3c420809))
* **hcaptch:** catch exc like challenge-closed ([d6fe67f](https://gitlab.com/jobgrader/jobgrader-app/commit/d6fe67f2924ebfc348d5bc1fd506646a07640c95))
* **header:** design fixed for hcaptcha and kyc pages ([0cc5688](https://gitlab.com/jobgrader/jobgrader-app/commit/0cc56883803a0a354f7a313fa311b22e78c9d4f0))
* **header:** design fixed for hcaptcha and kyc pages ([046d8ae](https://gitlab.com/jobgrader/jobgrader-app/commit/046d8aef12480d01ad6a6b5655906275f4585817))
* **http-interceptor:** don't send barer token to other services ([ccdb2c4](https://gitlab.com/jobgrader/jobgrader-app/commit/ccdb2c43b9465776a720230d050442deb7c5a170))
* **inappbrowser:** post KYC processes and landing page ([e429dde](https://gitlab.com/jobgrader/jobgrader-app/commit/e429dde8169422b059ed7174ba02b867e3f13a85))
* **ion-segment, wallet-generation:** styling, navigation fixes ([9059ae9](https://gitlab.com/jobgrader/jobgrader-app/commit/9059ae987daf9b83c71f7ddd4cb1fff29cf9ee23))
* **key:** do not stop the lockservice on kyc process ([cc9cd9c](https://gitlab.com/jobgrader/jobgrader-app/commit/cc9cd9c2efe4062ee7714c7b7afc545c7e9d4749))
* **kyc:** force fetching kyc data ([91bc605](https://gitlab.com/jobgrader/jobgrader-app/commit/91bc605c3906cae95c88d17c4815daedd3543b69))
* **kyc:** forward to veriff-thanks page also on pwa app ([377c639](https://gitlab.com/jobgrader/jobgrader-app/commit/377c639e4f4e929a256801848f37e8d6d7bebd62))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([c31a50a](https://gitlab.com/jobgrader/jobgrader-app/commit/c31a50ab2519f65aa9a358b97027c95018066f0b))
* **kyc:** post inappbrowser close event on low end android devices ([f6f8331](https://gitlab.com/jobgrader/jobgrader-app/commit/f6f8331b0288da033e817e276a7a0d99322d625d))
* **kyc:** remove navigation elements from DID_RETRIEVE state handling ([1881d6f](https://gitlab.com/jobgrader/jobgrader-app/commit/1881d6f3d6ebecf45e819f3d6ffd21ad45f6957a))
* **kyc:** wait all new kyc data are retrieved before we emit dataProcessedAfterKyc event ([42fa501](https://gitlab.com/jobgrader/jobgrader-app/commit/42fa501c01014d6493fdd47156533538f80a9c06))
* **linkedin:** Routing the access_token fetch through the MW, display the linkedin registered email ID in connected-accounts page after success ([e0c5f74](https://gitlab.com/jobgrader/jobgrader-app/commit/e0c5f742c37bdc85419dda2f4bad4be3fcbad5cb))
* **linkedin:** Using the correct json property ([5afb170](https://gitlab.com/jobgrader/jobgrader-app/commit/5afb170e3c153de0f8cee1f2de0b16e51cc3d78c))
* **lock-screen, login:** hide CTA description when spinner is loaded ([a9e5f00](https://gitlab.com/jobgrader/jobgrader-app/commit/a9e5f00c583fc6b474ec9094d452edf26a6ac0f6))
* **login:** return to onboarding page when reset device ([2ac3ddd](https://gitlab.com/jobgrader/jobgrader-app/commit/2ac3ddd3358d9be37aa703aa2a6cd544d3f82223))
* **login:** Try catch block in case of akita storage clear error ([fd3b853](https://gitlab.com/jobgrader/jobgrader-app/commit/fd3b85371f5431d8714eba40227e949964286589))
* **login:** wait 2 seconds before page reload on reset device ([38778ee](https://gitlab.com/jobgrader/jobgrader-app/commit/38778ee02d982453c76494e6b2c31bcb294768fa))
* **marketplace, login:** missing PIN handling, new category in marketplace ([72f968d](https://gitlab.com/jobgrader/jobgrader-app/commit/72f968dd47baa17a19f539b1b065ec736789e386))
* **marketplace:** adapt to new capacitor sqlite plugin ([8165da7](https://gitlab.com/jobgrader/jobgrader-app/commit/8165da7a3ee22dbb9bd9799b4dd357c7098ebf26))
* **merchant-details:** force kyc process after timeout ([cb0bb4f](https://gitlab.com/jobgrader/jobgrader-app/commit/cb0bb4f2109f2b9133ea94eec5c48a3fd38cb881))
* **merchant-details:** Job categories pages' CTA button navigation fix ([7041686](https://gitlab.com/jobgrader/jobgrader-app/commit/704168695b7da30dc6ad19638ae25a1fd8e55342))
* **nft:** imported wallet nft calculation: decentraland collectibles ([d8c05c1](https://gitlab.com/jobgrader/jobgrader-app/commit/d8c05c1efbefb850619866604810ac0008b8a9dd))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([da553bc](https://gitlab.com/jobgrader/jobgrader-app/commit/da553bc685e34843bee1f18b8e303f9a5ac41cf4))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([9be08ed](https://gitlab.com/jobgrader/jobgrader-app/commit/9be08eda8357893479e19c580959483bea043e3a))
* **onboarding:** disable swipe animation on iOS before slider unlock ([1dcc058](https://gitlab.com/jobgrader/jobgrader-app/commit/1dcc058bcf3d82b4f984c33890173fb06ae65821))
* **onboarding:** enable swipe back on last slide ([2381fec](https://gitlab.com/jobgrader/jobgrader-app/commit/2381fec912ecf05bc5028bc0268637f27e55afe9))
* **onboarding:** Post wallet creation, navigate to tab-home instead of kyc ([7992689](https://gitlab.com/jobgrader/jobgrader-app/commit/79926895fe2bfea8fbe8eb6feeee26e2863ac02f))
* **payments:** close missing tag ([e70c3ea](https://gitlab.com/jobgrader/jobgrader-app/commit/e70c3ea3a798b3f4812947e772589207dfcaa0ab))
* **payments:** don't add account if no publicKey present ([7a6f27e](https://gitlab.com/jobgrader/jobgrader-app/commit/7a6f27ed5525cd214ad3d15053c342f17e0dd585))
* **payments:** don't pass obsolete params ([949172a](https://gitlab.com/jobgrader/jobgrader-app/commit/949172ad37c1fad9fd6ca984fa524e72e688ae1f))
* **personal-details:** Parse trust values correctly after receiving KYC push notification ([9f5edd5](https://gitlab.com/jobgrader/jobgrader-app/commit/9f5edd5c49ed9fdf1d25737375dc523be31859f2))
* **post-kyc:** Post KYC updates ([ed7cdc9](https://gitlab.com/jobgrader/jobgrader-app/commit/ed7cdc95ccbfe0ae0dec030d29ed8bc5e03d626f))
* **push-notification:** Commented out code snippet which stops processing of the incoming push notification, and duplicates the navigation to the login page ([b6bed6e](https://gitlab.com/jobgrader/jobgrader-app/commit/b6bed6e2557eb55fec23446d9ddcf7c61ec364dd))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([1fb4e18](https://gitlab.com/jobgrader/jobgrader-app/commit/1fb4e18d8d266ce505cdd6ea8676719f87c94c22))
* **push-notification:** process notification data ([479b514](https://gitlab.com/jobgrader/jobgrader-app/commit/479b514d6346c378ade35d68a6b74a81b5650cd4))
* **push-notifications:** Added the token generation on PWA / browser in a try catch block to prevent it from obstructing the user registration process ([1b89bc3](https://gitlab.com/jobgrader/jobgrader-app/commit/1b89bc30b0b4b38f0b258a2916bdfe67727c9f4d))
* **push-notifications:** Prevent double entry of the notifications based on notification ID uniqueness check ([f92b904](https://gitlab.com/jobgrader/jobgrader-app/commit/f92b9040f90d78cb6d1c080282ce59861874606f))
* **qr-code-scanner:** width and padding ([de0ea5d](https://gitlab.com/jobgrader/jobgrader-app/commit/de0ea5de3764a47dbb7b9040e1e7e6924ac52334))
* **registration:** Firebase push notification token push after successful registration ([9251ebe](https://gitlab.com/jobgrader/jobgrader-app/commit/9251ebec106580c86abaa3890e4604f30dd8b48f))
* **reset-device:** Try catch block in case of akita storage clear error ([4bb6f6d](https://gitlab.com/jobgrader/jobgrader-app/commit/4bb6f6d7ce3b15757cf93400459380b728f9130e))
* seam header & title on all settings pages ([a217ca7](https://gitlab.com/jobgrader/jobgrader-app/commit/a217ca7060dba863aaba040ed942ec97253fdbb2))
* **send-crypto:** hide/show modal on barcode scanner ([ecda13f](https://gitlab.com/jobgrader/jobgrader-app/commit/ecda13fcdcef876de4d7245bb73721acaad6f29b))
* **splash-screen:** remove dark and obsolete splash img ([15b9f9c](https://gitlab.com/jobgrader/jobgrader-app/commit/15b9f9c766a851e2a3f313ce229842281db132f5))
* **sqlstorage:** for now don't truncate tables ([947913c](https://gitlab.com/jobgrader/jobgrader-app/commit/947913c26f60cc983f46be547f7b354c8e6e4513))
* **storage:** handle akita storage clear ([1070057](https://gitlab.com/jobgrader/jobgrader-app/commit/1070057ecf24576c411f454d997fa97727fb7d02))
* **storage:** wait on read before storage is ready and updated from localforage ([7284249](https://gitlab.com/jobgrader/jobgrader-app/commit/728424941a98b9ffe77c1e588cf1dc59acfbeed4))
* **storage:** wait until all storages are ready ([6a09867](https://gitlab.com/jobgrader/jobgrader-app/commit/6a098671ab31df37d088d76d3150edf1cc690acd))
* **store:** clear entire store ([435a53e](https://gitlab.com/jobgrader/jobgrader-app/commit/435a53e0d66b4b59217f594700f1b06e3831ce6a))
* **swiper-job:** don't show All done message at startup ([6f4dbaf](https://gitlab.com/jobgrader/jobgrader-app/commit/6f4dbafc31d5d96a256b9d611dbfabd82e4afedb))
* **tab-home:** Correct order of the token balance, and consistent display of the dashboard values ([21b205e](https://gitlab.com/jobgrader/jobgrader-app/commit/21b205ee2714fddc77e067cf007d2a45a32917cc))
* **tab-home:** Do not display the KYC alert window if the user is on browse ode ([eb908d8](https://gitlab.com/jobgrader/jobgrader-app/commit/eb908d8cafe9859e51c5c7e19a94e7b514109c8e))
* **tab-home:** obsolet code which raises an exc ([2ca221b](https://gitlab.com/jobgrader/jobgrader-app/commit/2ca221b4e4db950b4988e493446c1f4fc89b5f6d))
* **tabs:** update active tabs depending on router ([6d36387](https://gitlab.com/jobgrader/jobgrader-app/commit/6d36387e8cc6930cbd15751bee8f1c09313afdac))
* **theme:** use light and dark mode over scss file ([579a947](https://gitlab.com/jobgrader/jobgrader-app/commit/579a947df5dd9e5d6f123902bd9f79c24b9fc85e))
* **thxc:** graph max circumference value updated to 70, show THXC balane in the THXC page ([c9061bf](https://gitlab.com/jobgrader/jobgrader-app/commit/c9061bf8a7f1d8ca6ce6491fa948b5d2efc3b45b))
* **translation:** Added the missing translations for the KYC intermediary page ([4090053](https://gitlab.com/jobgrader/jobgrader-app/commit/40900533a84391c28144c7c4dd99878355ed1cb1))
* us correct cyrillic words wor Web2 & Web3 ([4dc2f79](https://gitlab.com/jobgrader/jobgrader-app/commit/4dc2f796283b4935669e9e04d164804d5a4dd4a1))
* **user-display-picture:** issue with changing uploaded DP in the home page ([9847c14](https://gitlab.com/jobgrader/jobgrader-app/commit/9847c148d3295d4ec50e4dd0d636a1c067a72021))
* **user-mnemonic:** Recovery of userMnemonic ([7e08cf4](https://gitlab.com/jobgrader/jobgrader-app/commit/7e08cf4a6bbc78c20c77b2b3b3e8c305456653be))
* **vc:** don't dupplicate VCs on startup ([77f7443](https://gitlab.com/jobgrader/jobgrader-app/commit/77f7443953af95e849d9cdf16b4bd281b31378d8))
* **vc:** dont't store dupplicated vc's ([7a08983](https://gitlab.com/jobgrader/jobgrader-app/commit/7a0898399520e2809830e02ba3971cb4ade332d8))
* **walkthrough:** last slide drag event is handled ([8f6068b](https://gitlab.com/jobgrader/jobgrader-app/commit/8f6068b7dd90a28c80f214c71554b30975cc754e))
* **wallet-card:** monitoring wallet card color stays constant throughout the app lifecycle ([995411a](https://gitlab.com/jobgrader/jobgrader-app/commit/995411a12813da80ec732311b689dd38fdfdc102))
* **wallet-connect:** MinAgeCheck display in request form ([0deae8b](https://gitlab.com/jobgrader/jobgrader-app/commit/0deae8b26ee37d66cd4b5d53866e567eddaf4157))
* **wallet-connect:** session_proposal if there is no requiredNamespace ([c6af235](https://gitlab.com/jobgrader/jobgrader-app/commit/c6af23564651b7958909251a354de7f3d2c2ff1c))
* **wallet-generation:** Navigation to the correct success / status page when running the app as a PWA / on the browser ([055665c](https://gitlab.com/jobgrader/jobgrader-app/commit/055665c9310829da9cc4867098dab1015322e97f))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([ca86965](https://gitlab.com/jobgrader/jobgrader-app/commit/ca86965a31f3c279bc84004b1c341bf7b9dcd9ba))
* **wallet-generation:** Step-3 navURL is corrected ([2b5e799](https://gitlab.com/jobgrader/jobgrader-app/commit/2b5e7991b52254e3abc7a041f231184a5003a0ef))
* **wallet-generation:** Using the right local folder location on Android for local backup ([d4d819c](https://gitlab.com/jobgrader/jobgrader-app/commit/d4d819cbd223464c9e955960dc17de21af5e1cee))
* **wallet-list:** minor bug fix ([1b613a9](https://gitlab.com/jobgrader/jobgrader-app/commit/1b613a98cc53994a34a901abb35ebb0033bfe667))
* **wallet-recovery:** fix includes working on dataURI instead of filepath ([7e01ea9](https://gitlab.com/jobgrader/jobgrader-app/commit/7e01ea95b0e7ca74c17d87108cebb0a8c86a262a))
* **walletconnect:** eth_sendTransaction handling ([ab8c52c](https://gitlab.com/jobgrader/jobgrader-app/commit/ab8c52ccc0bbe00b4e114cf7bfae7426a638e40c))
* **WalletConnect:** Navigation back to the dApp ([d78c7ef](https://gitlab.com/jobgrader/jobgrader-app/commit/d78c7efa70bd60805037e1437b88c241022c01af))


### Features

* add price guess job with new servers ([9909a5e](https://gitlab.com/jobgrader/jobgrader-app/commit/9909a5e254ac03bcc1868ee259359e29457e8595))
* **app:** migrate to [@capacitor](https://gitlab.com/capacitor): update Info.Plist and entitlement files ([95611df](https://gitlab.com/jobgrader/jobgrader-app/commit/95611dfd94790a77d96619bbb8b67609ad5475be))
* **app:** sent console error logs to sentry ([28e5cbb](https://gitlab.com/jobgrader/jobgrader-app/commit/28e5cbbc8d22b1a5585b05cdaf0cc08a6f7bc345))
* **google-drive-permission:** Permission alert windows are added in Wallet Settings (Cloud Backup), new wallet generation in Web3 page, VC and VP backups in legal document modal window ([5ad394c](https://gitlab.com/jobgrader/jobgrader-app/commit/5ad394cd1329e22bdeae22adb5ea61a4019bdade))
* **hcaptcha:** catch console.error message from plugin ([5784e98](https://gitlab.com/jobgrader/jobgrader-app/commit/5784e983a6406f8fa3d25c90c0490257bb56ec73))
* **hcaptcha:** catch error-callback and send to sentry using console.error ([457db16](https://gitlab.com/jobgrader/jobgrader-app/commit/457db16fbd9215709574cbde7727d1c31d02c51c))
* **hcaptcha:** don't wait for siteverify response to reload new hcaptcha ([4777c15](https://gitlab.com/jobgrader/jobgrader-app/commit/4777c15ee9bc8f79d7fc5ef37f4655b88d7318af))
* **hcaptcha:** init hcaptcha just once & remove it when leaving page ([6009a66](https://gitlab.com/jobgrader/jobgrader-app/commit/6009a667d6c8b0850875f19c15d389977ab61b58))
* **hcaptcha:** send data to sentry if hcaptcha sucsess is false ([12bfc2f](https://gitlab.com/jobgrader/jobgrader-app/commit/12bfc2fedcad7f57209d9e304c1c35b5efc3b43a))
* **kyc-alert:** Display the necessary KYC alert in the merchant-details page ([4214d2d](https://gitlab.com/jobgrader/jobgrader-app/commit/4214d2d075848d8752daf5f70da917a8b385eb9a))
* **kyc-media:** User is prompted with a permission alert window before uploading the KYC Media to the users Google Drive account ([16202ce](https://gitlab.com/jobgrader/jobgrader-app/commit/16202ce5f82b32e8f66ad4a99de8442dbe581f7f))
* **kyc:** use @capacitor/browser for android ([283cf55](https://gitlab.com/jobgrader/jobgrader-app/commit/283cf55b9d6689257dd8fafcb548565bcd1a646d))
* **linkedin:** Added methods to call /me, /delete, /post endpoints ([f9fc642](https://gitlab.com/jobgrader/jobgrader-app/commit/f9fc642a38e959bcf22a7353dbb4146090db9398))
* **linkedin:** Display certificates, educational qualifications, and languages in Certificates page ([290086e](https://gitlab.com/jobgrader/jobgrader-app/commit/290086e5b2d15f4b5a39f6d7d947867638a963de))
* migrate qrcode to angularx-qrcode ([50d9325](https://gitlab.com/jobgrader/jobgrader-app/commit/50d9325da63eacf80da478f4f81a56204592b8d9))
* **push-notifications:** Process the notifications contained in the drawer before removing them ([caff78d](https://gitlab.com/jobgrader/jobgrader-app/commit/caff78d1af138f76c2658ec25462a3a41f2b9908))
* **socials:** follow links on discord, telegram, x, linkedin ([18adbf1](https://gitlab.com/jobgrader/jobgrader-app/commit/18adbf106193f801b2cc0043655f450af3290c27))
* **socials:** linkedin and discord user authentication flow added ([4d73f4b](https://gitlab.com/jobgrader/jobgrader-app/commit/4d73f4ba1ccf8c872c296f502441de43b700bb64))
* **storage:** add kyc media ([d494532](https://gitlab.com/jobgrader/jobgrader-app/commit/d4945321befa202b88386698d6e1d9b3fd4f0ef1))
* **storage:** add user photo ([d9adf2d](https://gitlab.com/jobgrader/jobgrader-app/commit/d9adf2d4fdbec869d0f7b091181c93af9dc82dfa))
* **storage:** add vault secrets ([6fbed35](https://gitlab.com/jobgrader/jobgrader-app/commit/6fbed35f1c9e67e9fef1aced704eedc1d74b48b0))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([b5c9ec6](https://gitlab.com/jobgrader/jobgrader-app/commit/b5c9ec699024837e9ba6cfc42aa1d359deef38cd))
* **swiper-job:** add description at the bottom and style the cards better ([e0d128f](https://gitlab.com/jobgrader/jobgrader-app/commit/e0d128f9d85ef8d13ffc523e79c5cfee9f109baa))
* **tab-home:** Display users THXC balance in the home page ([6ac1299](https://gitlab.com/jobgrader/jobgrader-app/commit/6ac12990b509d31c1ea8722da4a9d6f53be6736d))
* use biometric on login/pin page, show biometric button & remove lottie animation ([866114d](https://gitlab.com/jobgrader/jobgrader-app/commit/866114da2e930c8a090eca4876508715898ddbf9))
* **wallet-generation:** User is prompted with a permission alert window before uploading the encrypted seed-phrases to the users Google Drive account ([dd975c8](https://gitlab.com/jobgrader/jobgrader-app/commit/dd975c86f99f7cb545b1ab1b7b4839b512bc0a4b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([43bb4f5](https://gitlab.com/jobgrader/jobgrader-app/commit/43bb4f5561de88eabd85eebb7f02f76702e942ae))



# [1.0.0-154](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-135...1.0.0-154) (2023-08-07)


### Bug Fixes

* **api:** get the right basictoken from storage ([fe22310](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fe22310cc657b0770214e98b1143d4cf0d618196))
* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **barcode:** open settings app if the user has not consented to camera usage ([6f779be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6f779be00544493664cee6ff074ad0532f50d734))
* **browse-mode:** better UX when the user is in browse mode, correct DP and alert windows are displayed ([380c733](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/380c73314463d1155bef00d99f55d903de7ac2b1))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** forward to veriff-thanks page also on pwa app ([eee67d6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/eee67d6680bc7a151cab878c907b55c590603563))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **kyc:** post inappbrowser close event on low end android devices ([a3a3de6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a3a3de67ac614c97e1a75e338c3a912971996e8a))
* **kyc:** remove navigation elements from DID_RETRIEVE state handling ([555dec6](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/555dec64ba8ddfadf9ace0616a1f6591462da91c))
* **kyc:** wait all new kyc data are retrieved before we emit dataProcessedAfterKyc event ([52081df](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/52081df25f4e0ab876a4344ea165928ca1b31c5e))
* **lock-screen, login:** hide CTA description when spinner is loaded ([4d7aabd](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4d7aabd4bfac11a12dd4305867b8dfec2b695ba2))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **storage:** wait until all storages are ready ([ff5b447](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ff5b447335ec1b0703b23bcdd6901888cf7c5060))
* **store:** clear entire store ([cdd1f9c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cdd1f9c96e1cc0b189041f4094eb9b409d7eadd0))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **tabs:** update active tabs depending on router ([d345586](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d345586dd27e552fce12c28d38cf7f46a53adc18))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **vc:** dont't store dupplicated vc's ([fca3460](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fca34607005d91c671ffd6738ddc3f5d3cac6a03))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** add kyc media ([f4884bb](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f4884bb0adbd86a7cdba981dc213c69282b7904d))
* **storage:** add user photo ([89b0c95](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/89b0c95965363774474cbe8f2e239bfe5a4d0a58))
* **storage:** add vault secrets ([08cadbf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/08cadbf9f4998ca8e885281ebda73b9da5305a56))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-135](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-135) (2023-08-04)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** add kyc media ([f4884bb](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f4884bb0adbd86a7cdba981dc213c69282b7904d))
* **storage:** add user photo ([89b0c95](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/89b0c95965363774474cbe8f2e239bfe5a4d0a58))
* **storage:** add vault secrets ([08cadbf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/08cadbf9f4998ca8e885281ebda73b9da5305a56))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-134](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-134) (2023-08-03)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **authentication:** bug fix for correct username and incorrect password ([8eb0493](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8eb0493a51b6442e19cbcabd3d96bc9a41dc3a09))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **camera:** add pwa-elements to support camera on browser ([0cb3ec9](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0cb3ec90ae9780314aeb7d12e7cdc3718f8ffc40))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **inappbrowser:** post KYC processes and landing page ([21b9387](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/21b93876dce1c83295301efe2bf427cc8066b461))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **key:** do not stop the lockservice on kyc process ([fb9eb92](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fb9eb92a25cf4d048e3caabfec0477d3d14c2510))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **user-display-picture:** issue with changing uploaded DP in the home page ([f3dde30](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f3dde30715dd28f06e46c0cc7965984ed5cd8287))
* **walkthrough:** last slide drag event is handled ([9303fd0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9303fd08c9b73cf71d66d689199591e0b09a860e))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))
* **wallet-list:** minor bug fix ([cd686f5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/cd686f599da4f8ea1d05d1e1df925359133b1cea))


### Features

* **kyc:** use @capacitor/browser for android ([0c21aff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/0c21affea3324ddafbaa5a7490c3761188805c54))
* **storage:** use own branch per storage & wait angular for localForage to be ready ([879334d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/879334d5eb82cf9619680007f901afce05ddd1de))
* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-127](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-102...1.0.0-127) (2023-08-01)


### Bug Fixes

* **app:** FileReader in combination with capacitor and zone.js has a bug, this is a workaround for now ([f5815a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f5815a4c5b133eba2693cac277a0606262a333a5))
* **bug:** Wallet Card color stays constant ([87db118](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/87db1184a86f32d01f68ad1b40b6059a2510e70e))
* **chart:** use the right dataSet, remove plain js func ([a8d6fba](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8d6fba4e458a6824f5294bd5917f32d28b59ecc))
* **crypto:** sending the following tokens: OP, HMT POLYGON / SKALE ([7d0674f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7d0674ff0552fa65d3a6b8b1a87084a932a618a7))
* **dashboard:** show the right navigation point after scanner is closed ([869eeff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/869eeff877db8d8b29b417d8346dee6181ea2a60))
* **deeplink:** scheme and host updated in AndroidManifest.xml ([7960f1d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7960f1d11e2c43653f860eb4149460fc9df17bc9))
* **design:** SVG icons around the chart ([d9f4a7c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d9f4a7c9ba24df6747001f61ac3e7b6b7a05c1cb))
* **did-comm:** inject the service global for now ([7aba5e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7aba5e1a6a58e455803469f116e6af9a1d94ac8f))
* **email:** Disposable mail ID detection during the registration process ([42d1a35](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/42d1a3562d35b62a7167171b90691ea2da3d01b1))
* **firebase:** new config file added ([c9433be](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/c9433be5f67a4fd79b363b957eaa8aaf320d17b0))
* **gas:** Gas prices for the following tokens are introduced: OP, HMT POLYGON / SKALE, XDAI ([9ef1f32](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9ef1f32b001f86e0ee1395de7851397fb4a64f61))
* **header:** design fixed for hcaptcha and kyc pages ([28caf7f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/28caf7f14ad77407c24333effc4ac13774cefe5f))
* **header:** design fixed for hcaptcha and kyc pages ([fbe6d49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/fbe6d490c49983f6ef3a7058618a41495b755ad1))
* **ion-segment, wallet-generation:** styling, navigation fixes ([b10a672](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/b10a672f2f1dd4f225647350c37fe84857cbb0f5))
* **kyc:** PIN entry form is introduced in both KYC page and Documents Modal window ([addd825](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/addd82556a37b0f0f757dc2c9821bd32c8c37fa5))
* **login:** return to onboarding page when reset device ([4f43aa2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4f43aa2f335107f161b078e578690b5cf34b8842))
* **login:** wait 2 seconds before page reload on reset device ([6ac2b9a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6ac2b9a63e0389da7183bad067e915b52b4f1d11))
* **marketplace, login:** missing PIN handling, new category in marketplace ([df9d3a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df9d3a4f95394e2f97ebc96d44770bd836dfd2cb))
* **marketplace:** adapt to new capacitor sqlite plugin ([03d058c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/03d058c685d3500d650f3ad81de87d95eebca3c3))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([92e885d](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/92e885d97f51862573b02939056bcf4c17c80c8c))
* **payments:** close missing tag ([bfbb5d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/bfbb5d36e999c81da9a8ed003c839204a2d058e4))
* **post-kyc:** Post KYC updates ([ddd8cff](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ddd8cff98c9350c056d5746ca18c2e569d7345ea))
* **push-notification:** open KYC session in an inappbrowser on iOS, list notifications under the settings sub page ([d84dde5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d84dde5fa485e334e8fd79466be90d1c8cb81852))
* **push-notification:** process notification data ([748a490](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/748a490f19be0faf0739b1436cff53ab358b6978))
* seam header & title on all settings pages ([85a3bd5](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/85a3bd5c4f4015707140653d133f1c0dc6d8d11d))
* **sqlstorage:** for now don't truncate tables ([7f7cf08](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7f7cf0876a5141840a78bea9b8ab55bda049365e))
* **storage:** wait on read before storage is ready and updated from localforage ([ccc6ff7](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ccc6ff7f07f7df61208d2979a3bd87b6c78a30d4))
* **tab-home:** obsolet code which raises an exc ([91d4f12](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/91d4f12730273b99640400f9e55a1310511244a6))
* **theme:** use light and dark mode over scss file ([e708c6e](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e708c6ea54916b51338bc41cac5e40c870f70cbe))
* **wallet-connect:** MinAgeCheck display in request form ([dc666ad](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/dc666adb56c551532f7ba5e47ba09e59be5dd6b8))
* **wallet-generation:** paper backup - missing translations added, navigation fixes, parsing and decoding errors fixed ([a8f06d3](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a8f06d399865642e9f0da11bb5443fcc5407f29f))
* **wallet-generation:** Step-3 navURL is corrected ([d1e7261](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d1e72617cc89f47939ecc9bf9d6a5cde84864a6d))


### Features

* use biometric on login/pin page, show biometric button & remove lottie animation ([8709c49](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8709c494519067c628f9248ee614e046d4513b0b))


### Reverts

* **hcaptcha:** don't hide I'm human button ([7226b6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7226b6cc35a44a02dc8564ffe214f96b9766faef))



# [1.0.0-100](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-67...1.0.0-100) (2023-07-11)


### Features
* **app:** migrate app from cordova to capacitor@5

* **ui:** improvements on the user journey


### Bug Fixes

* **bug:** Wallet Card color stays constant ([e3cd821](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/e3cd8212a91b6bd078336b58872f239223480448))


# [1.0.0-67](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-66...1.0.0-67) (2023-07-05)


### Bug Fixes

* **chart:** use the right dataSet, remove plain js func ([644f430](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/644f43020302964349f44cac70e71ace019d60ec))
* **design:** SVG icons around the chart ([1e03d29](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/1e03d290ed65d5af494d326461f02f52cb664670))
* **oliver:** edit button removed, verify button replaces chat, alert window when user has no KYC data ([6175006](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6175006d909f16ab9ebfb25c79fc614880bb1236))



# [1.0.0-66](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-64...1.0.0-66) (2023-07-04)


### Bug Fixes

* **hcaptch:** catch exc like challenge-closed ([923c3b8](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/923c3b821031e0b00e27c8bc3dda948d9eff831b))
* **payments:** don't add account if no publicKey present ([5279c96](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5279c966d1ad7a6a5454ed925140968f92d6ebe7))
* **payments:** don't pass obsolete params ([f324811](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f324811ccff875e17c606337f8ae3a39ede0b0a6))



# [1.0.0-66](https://gitlab.com/jobgrader/jobgrader-mobile-app/compare/1.0.0-64...1.0.0-66) (2023-07-04)


### Bug Fixes

* **hcaptch:** catch exc like challenge-closed ([923c3b8](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/923c3b821031e0b00e27c8bc3dda948d9eff831b))
* **payments:** don't add account if no publicKey present ([5279c96](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5279c966d1ad7a6a5454ed925140968f92d6ebe7))
* **payments:** don't pass obsolete params ([f324811](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f324811ccff875e17c606337f8ae3a39ede0b0a6))



# 1.0.0-64 (2023-06-27)


### Bug Fixes

* **all:** remove unused animated header ([7155e3f](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/7155e3f80a2a63eecdf39511ca2655a1a089ad98))
* **chat:** Removal of chat related calls from all pages ([a53ffb0](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/a53ffb01c066923ded406d52c50dd402902d63c0))
* **cordova:** type safe if deeplinks plugin is not available ([5067194](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/5067194aa8e2a9e3ee6cba67b328bd5ac23ec299))
* **crypto-currency:** catch exception on setUserCurrenciesListInStorage to not break app ([9276891](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9276891b9a9bb506bd7273813ec11e05ea56bef6))
* **findings-AM:** fix citiziens on kyc ([f105c3b](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/f105c3b859a6e2f6cf09157de13368b0047dcd55))
* **findings-ON:** fix some bugs on camera and upload picture ([2a906a4](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/2a906a4f6bb0d26a861985a5dd831e51976c03f5))
* **findings-ON:** don't reload KYC media every time ([ae26db2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/ae26db275785085ee90378bd8dd3dc142096d4db))
* **findings-ON:** minor fix on wallet and hcaptch page ([337dc39](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/337dc397d5e633ebb12f6cf8f7b947c8b7b9ab13))
* **findings-ON:** update human protcol with right symbol ([9443e44](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9443e44e41e32219878b94da4f2d846a61a33a16))
* **login:** prevent spinner to disappear before redirect on main page ([aa2b0e1](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/aa2b0e124ec4b516facb187f0c5ccd1ff1003cb0))
* **login:** safe close of OTP modal ([8869e23](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/8869e232063fd87b89ef747401d3281be3763077))
* **payments:** after deleting wallet return back to payments page ([1c783bf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/1c783bf5b235d7ad377446b15ed70d2bbca4e02d))
* **payments:** cleanup curriences and removed wallets ([d41ce70](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d41ce70d76350ac4882a129d9b16fce6a5a4ac78))
* **payments:** don't set click event on chart segement ([d94a86a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/d94a86a46a8516716242d70289f4281f379c71a4))
* **payments:** fix flickering issue on the d3 chart and token list ([6586f6c](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/6586f6ca813a6309ee0dca2547d14285505851f8))
* **payments:** fix flickering with performance boost ([adec0aa](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/adec0aa06bb012e468f03ebc9cfad8f04eb66080))
* **payments:** fix some bugs on the wallet ([9c48971](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/9c48971c74dcc5cb44889dab6f6c558c172ed2fc))
* **sign-up:** check for undefined ([de10276](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/de10276269e098f6c972bb02b443ba7969420eca))
* **sign-up:** set the focus on username field at the right point ([df84480](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/df84480d39bb6881c00e465ab77e1bfcd8ffacf0))
* **tab-home:** obsolet code which raises an exc ([86eadaf](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/86eadafc6dfdd066f62847a472907ebd5f6a8ab9))
* **tab-home:** use ionViewDidEnter instead customized onEnter method from even customized IonPage which was called more then just once ([84d6dc2](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/84d6dc273ed1183d7ae65422efbcf290a5d23fbf))


### Features

* **payments:** add Account model ([4ebb94a](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/4ebb94ae1d59fcaa5aab16e13c86a41700f06292))
* **sentry:** add sentry error log ([513efea](https://gitlab.com/jobgrader/jobgrader-mobile-app/commit/513efea7fa15c4319f2da8bfdb7bdf92d722b77a))



