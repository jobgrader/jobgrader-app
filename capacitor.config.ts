import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'app.jobgrader.crowdworker',
  appName: 'Jobgrader',
  webDir: 'www',
  server: {
    hostname: 'jobgrader.app',
    androidScheme: 'https',
    iosScheme: 'jobgrader'
  },
  plugins: {
    GoogleAuth: {
      scopes: ['https://www.googleapis.com/auth/drive.file']
    },
    SplashScreen: {
      launchShowDuration: 10000,
      launchAutoHide: true,
      launchFadeOutDuration: 300
    },
    FirebaseMessaging: {
      presentationOptions: ["badge", "sound", "alert"],
    },
    CapacitorSQLite: {
      iosDatabaseLocation: 'Library/CapacitorDatabase',
      iosIsEncryption: true,
      iosKeychainPrefix: 'angular-sqlite-app-starter',
      iosBiometric: {
        biometricAuth: false,
        biometricTitle : "Biometric login for capacitor sqlite"
      },
      androidIsEncryption: true,
      androidBiometric: {
        biometricAuth : false,
        biometricTitle : "Biometric login for capacitor sqlite",
        biometricSubTitle : "Log in using your biometric"
      },
      electronIsEncryption: false,
      electronWindowsLocation: "C:\\ProgramData\\CapacitorDatabases",
      electronMacLocation: "/Volumes/Development_Lacie/Development/Databases",
      electronLinuxLocation: "Databases"
    },
  },
  cordova: {
    preferences: {
      hostname: 'jobgrader.app',
      Hostname: 'jobgrader.app'
    }
  }
};

export default config;
