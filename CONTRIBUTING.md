# Contributing Guidelines for Jobgrader

Thank you for considering contributing to our open-source project, Jobgrader, released under the Apache License 2.0! We appreciate the time and effort you put into making Jobgrader better. Before you start contributing, please take a moment to read our guidelines.

## How to Contribute

We welcome contributions in the form of bug reports, feature requests, code improvements, and more. To ensure a smooth collaboration, please follow these steps:

1. **Submit an Issue:**
   - If you find a bug or have a feature request, please submit an issue on our [Jobgrader issue tracker](https://gitlab.com/groups/jobgrader/-/issues). Clearly describe the problem or enhancement you're suggesting.

2. **Fork the Repository:**
   - Fork the Jobgrader repository to your GitLAB account and create a new branch for your contribution.

3. **Code Contribution:**
   - Implement your changes or fixes in the branch you created.
   - Ensure your code adheres to our coding standards.

4. **Submit a Pull Request:**
   - Open a pull request against the `main` branch of the Jobgrader repository.
   - Provide a clear and concise description of your changes.
   - Reference the issue number(s) your pull request addresses.

## Request Form, Contributor Sign-On, Test Environment Access, and Bug Bounty in THXC Token

To streamline the contribution process, we request contributors to fill out a request form and sign on with the following contributor sign-on form before submitting a pull request. Additionally, if you identify and responsibly disclose a security vulnerability, you may be eligible for a bug bounty in THXC tokens. Please follow these steps:

1. **Fill out the Jobgrader Request Form:**
   - [Jobgrader Request Form](https://jobgrader.app/contributor)

2. **Contributor Sign-On:**
   - [Jobgrader Contributor Sign-On](https://jobgrader.app/contributor)
     - By signing this form, you acknowledge that you have read and agree to abide by the Jobgrader [Code of Conduct](link-to-jobgrader-code-of-conduct).
     - You confirm that your contributions are made under the terms of the [Apache License 2.0](https://gitlab.com/jobgrader/jobgrader-app/-/blob/main/LICENSE).

3. **Request Test Environment Access:**
   - [Test Environment Access Request Form](https://jobgrader.app/contributor)
     - If your contribution requires testing in a controlled environment, please fill out this form to request access.

4. **Bug Bounty in THXC Token:**
   - If you identify a security vulnerability, responsibly disclose it by contacting our security team at [security@jobgrader.com](mailto:security@jobgrader.com).
   - We offer bug bounties in THXC tokens for eligible security vulnerabilities. Please include relevant details and a clear description of the vulnerability in your disclosure.

5. **Wait for Approval:**
   - Once you submit the forms, our team will review your request.
   - We may provide feedback or ask for additional information.

6. **Get Confirmation:**
   - After approval, you'll receive confirmation to proceed with your contribution, along with any bug bounty rewards if applicable.

## Code of Conduct

Please note that by contributing to Jobgrader, you agree to abide by our [Jobgrader Code of Conduct](https://gitlab.com/jobgrader/jobgrader-app/-/blob/main/CODE_OF_CONDUCT.md). We expect all contributors to adhere to these guidelines, fostering a positive and inclusive community.

## Thank You

We appreciate your contributions to Jobgrader. Your efforts help make Jobgrader better for everyone. If you have any questions or need further assistance, feel free to reach out to us through the contact details provided in the [Jobgrader Request Form](link-to-jobgrader-request-form).
