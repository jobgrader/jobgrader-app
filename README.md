# Jobgrader - Mobile Application for AI Data Training

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](link-to-apache-license)
[![Jobgrader Issue Tracker](https://img.shields.io/badge/Issue%20Tracker-Report%20a%20Problem-red)](link-to-jobgrader-issue-tracker)

## Overview

Jobgrader is a mobile application designed for crowdworkers to contribute to AI data training. The application provides functionalities such as Digital Identity, WEB3 wallet, and Digital Workspace. The framework used for development is IONIC.

## How to Contribute

We welcome and appreciate contributions! If you would like to contribute to Jobgrader, please follow our [Contributing Guidelines](link-to-contributing-md).

## Features

- **Digital Identity:** Empower crowdworkers with a robust digital identity system.
- **WEB3 Wallet:** Integration of a secure WEB3 wallet for seamless transactions.
- **Digital Workspace:** Create a collaborative and efficient digital workspace for AI data training.

## Getting Started

To get started with Jobgrader, follow these steps:

1. [Installation instructions, if applicable]
2. [Usage instructions]

## Documentation

Detailed documentation can be found [here](link-to-docs).

## Request Form, Contributor Sign-On, Test Environment Access, and Bug Bounty

If you are planning to contribute to Jobgrader, please fill out the [Jobgrader Request Form](link-to-jobgrader-request-form) and sign the [Contributor Sign-On Form](https://jobgrader.app/contributor). Additionally, if you need access to our test environment or want to report a security vulnerability, follow the guidelines in the [Contributing Guidelines](/CONTRIBUTING.md).

## Used Framework

Jobgrader is built using the IONIC framework.

# Development Setup on macOS

1. Install xcode command line tools: `xcode-select --install`
2. Install brew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
3. Change ownership of pkgconfig: `sudo chown -R $(whoami) /usr/local/lib/pkgconfig`
4. Install cocoapods: `sudo gem install cocoapods`
5. Install [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
6. Install gradle: `brew install gradle`
7. Install [Android Studio](https://developer.android.com/studio)
8. Open Android Studio and install the latest Android SDK
9. In *~/.bashrc* OR *~/.zshrc* (whatever) files, add the following, and the run `source ~/.bashrc` OR `source ~/.zshrc`:
```
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_311.jdk/Contents/Home
export ANDROID_SDK_ROOT=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_SDK_ROOT/tools/bin
export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
export PATH=$PATH:$JAVA_HOME/bin
```
10. It is advised that you run the following for a smooth Android build process:
```
cd ~/Library/Android/sdk/build-tools/31.0.0 \
  && mv d8 dx \
  && cd lib  \
  && mv d8.jar dx.jar
```
11. Install [node 18.10.0](https://nodejs.org/download/release/v18.10.0/)
12. Install [nvm](https://github.com/nvm-sh/nvm)
13. Install yarn, ionic and cordova: `npm i -g yarn && yarn install -g @ionic/cli cordova`
14. Clone the project and cd into the project:
```
git clone https://gitlab.com/jobgrader/jobgrader-mobile-app
cd jobgrader-mobile-app
```
15.  Change the ownership of the subfolders: `sudo chown -R $(whoami) .`
16.  Run the bash script: `chmod +x build.sh && ./build.sh`
17.  Run on browser: `ionic serve`
18.  Add platforms: `chmod +x add_platforms.sh && ./add_platforms.sh`
19.  Build iOS project: `ionic cordova build ios --prod`
20.  Build Android project: `ionic cordova build android --prod`


# Sentry

## Login
Run the login command and generate a new token
```
npx sentry-cli login
```
or pass a known token
```
npx sentry-cli login --auth-token .......
```

## Upload
After building your production build for release run the sentry upload script
```
npm run sentry:upload
```

## HOW TO RELEASE A NEW VERSION
1. change in `package.json` the version, like `1.0.0-xx`
2. run `npm i --legacy-peer-deps` which will update version also in the lock file
3. build your production release with source map enabled `ionic build --prod --source-map`
4. upload required files to sentry `npm run sentry:upload`
5. sync native builds `ionic cap sync ios --no-build` or `ionic cap sync android --no-build`
6. if upload to stores was successful go ahead
5. update changelog `npm run changelog`
6. have a look to the `CHANGELOG.md` if everything is good or change it manually if needed
7. add `package.json`, `package-lock.json` and `CHANGELOG.md` to git (`git add .....` or from `VSCode`)
8. do the official release with `npm run git:rel`
9. tag the release `npm run git:tag`

## Code of Conduct

Please note that by contributing to Jobgrader, you agree to abide by our [Code of Conduct](/CODE_OF_CONDUCT.md). We expect all contributors to adhere to these guidelines, fostering a positive and inclusive community.

## License

Jobgrader is licensed under the [Apache License 2.0](/LICENSE).

## Contact

If you have any questions or need further assistance, please reach out to us through the contact details provided in the [Jobgrader Request Form](https://jobgrader.app/contributor).

## Acknowledgments

[Optional: Acknowledge contributors, libraries, or other resources.]