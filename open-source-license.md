# Jobgrader App: Open Souce Licenses

> Version 1.03, 9th October 2024

This document contains licensing information relating to Jobgrader’s use of free and open-source software with or within the Jobgrader App (hereinafter referred to as, the “FOSS”). Any terms, conditions, and restrictions governing the use of distribution of Jobgrader App not contained within the license(s) governing use and distribution of the Jobgrader App are offered and imposed by Jobgarder alone. The authors, licensors, and distributors of the Jobgrader App have disclaimed all warranties relating to any liability arising from the use and distribution of the Jobgrader App.

Certain FOSS licenses require that Jobgarder make the source code corresponding to the distributed FOSS binaries available under the terms of the same FOSS license. 

Recipients of the Jobgrader App who would like to receive a copy of such source code should submit a source-code request to Jobgarder by email at: dpo@humanindata.com

Please identify in submitted source-code requests: the FOSS packages for which you are requesting source code; the Jobgarder application and version number with which the requested FOSS package is distributed.

**MIT License**

The following FOSS are licensed under MIT License; you may not use this file expect in compliance with the License. 

+ *Ionic-framework* Copyright 2015-present Drifty Co.
+ *ngx-translate/core* Copyright (c) 2018 Olivier Combe
+ *Crypto-js* Copyright (c) 2009-2013 Jeff Mott  | Copyright (c) 2013-2016 Evan Vosberg
+ *Elliptic* Copyright Fedor Indutny, 2014.
+ *Ethers* Copyright (c) 2019 Richard Moore
+ *ionic/cli* Copyright (c) 2017 Drifty Co
+ *Hammerjs* Copyright (C) 2011-2017 by Jorik Tangelder (Eight Media)
+ *Lycwed-cordova-plugin-udid* Copyright (c) 2014 Paldom
+ *Lodash* Copyright JS Foundation and other contributors https://js.foundation/
+ *Moment* Copyright (c) JS Foundation and other contributors
+ *Ng-lottie* Copyright (c) 2016 CHEN Qing
+ *Ngx-autosize* Copyright (c) 2019 Chrystian Ruminowicz
+ *Phonegap-plugin-barcodescanner* Copyright 2010 Matt Kane |  Copyright 2011 IBM Corporation | Copyright 2012-2017 Adobe Systems
+ *Zxcvbn* Copyright (c) 2012-2016 Dan Wheeler and Dropbox, Inc.
+ *Human Protocol “@human-protocol/core”* Copyright (c) 2018 HUMAN Protocol
+ *vladmandic/human* Copyright (c) Vladimir Mandic
+ *alchemyplatform/alchemy-sdk-js* Copyright (c) 2022 Alchemy
+ *swiper* Copyright (c) 2019 Vladimir Kharlampidi
+ *moralis* Copyright (c) 2021 Moralis
+ *iso-639-1* Copyright(c) by Mei Qingguang

You may obtain a copy of the License at: [https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**ISC License**

The following FOSS are licensed under ISC License; you may not  use this file except in compliance with the License.

+ *xmpp/client* Copyright (c) 2017, xmpp.js
+ *BIP Encryption* Library Copyright (c) 2014, Wei Lu

You may obtain a copy of the License at: [https://opensource.org/licenses/ISC](https://opensource.org/licenses/ISC)

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH

REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY

AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,

INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

**Apache License**

The following FOSS are licensed under the Apache License, Version 2.0; you may not use this file except in compliance with the License. 

+ *Cordova-cli* Copyright 2014, agrieve
+ *Rxjs* Copyright (c) 2015-2018 Google, Inc., Netflix, Inc., Microsoft Corp. and contributors
+ *hearro/didcomm* Copyright 2020, viccooper142 and sjback
+ *evannetwork/DIDComm-js* Copyright 2019, orspetol
+ *[evannetwork/vade-evan](https://github.com/evannetwork/vade-evan/blob/develop/LICENSE.txt)*

You may obtain a copy of the License at: [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.

**BSD 3-Clause**

The following FOSS are licensed under the BSD 3-Clause License; you may not use this file except in compliance with the License. 

+ *Vc-js* Copyright (c) 2017-2020, Digital Bazaar, Inc.  All rights reserved.
+ *wavesurfer.js* Copyright (c) 2012-2023, katspaugh and contributors. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

+ Redistributions of source code must retain the above copyright notice, this  list of conditions and the following disclaimer.
+ Redistributions in binary form must reproduce the above copyright notice,  this list of conditions and the following disclaimer in the documentation  and/or other materials provided with the distribution.
+ Neither the name of the copyright holder nor the names of its  contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”

AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE

IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

**Creative Commons – CC0 1.0 Universal**

The following FOSS are licensed under the Creative Commons License; you may not use this file except in compliance with the License. 

Eccrypto No Copyright
Copyright and related rights for sample code are waived via CC0. Sample code is defined as all source code displayed within the prose of the documentation.

Licensed under CC0 License; you may not  use this file expect in compliance with the License You may obtain a copy of the License at: http://creativecommons.org/publicdomain/zero/1.0/

**Permissive License**

*libsodium-wrappers* Copyright (c) 2015-2020 Ahmad Ben Mrad batikhsouri@gmail.org |  Frank Denis j@pureftpd.org| Ryan Lester ryan at@cyph.com
Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED “AS IS” AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

**GNU PUBLIC License**

The following FOSS are licensed under the GNU Public License; you may not use this file except in compliance with the License.

+ *Pretix Service* Copyright (C) 2007 Free Software Foundation, Inc.
+ *Base-58* Copyright (C) 1989, 1991 Free Software Foundation, Inc.
+ *Evannetwork/api-blockchain-core* Copyright (C) 2007 Free Software Foundation, Inc.
+ *ChainSafe/web3.js* https://github.com/ChainSafe/web3.js/blob/1.x/LICENSE

This program is free software: you can redistribute it and/or modify  it under the terms of the GNU Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

**GNU License**

The following FOSS are licensed under the GNU License; you may not use this file except in compliance with the License.

Die folgende FOSS sind unter der WooCommerce Lizenz lizenziert; Sie dürfen diese Datei nur in Übereinstimmung mit dieser Lizenz verwenden

Woocommerce Lizenz Copyright 2015 by the contributors, Jigoshop is Copyright (c) 2011 Jigowatt Ltd.
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

**GNU LESSER GENERAL PUBLIC License**

+ *[web3.js](https://github.com/web3/web3.js)* https://www.gnu.org/licenses/lgpl-3.0.en.html#license-text

**Additional libraries**

+ *Meld* (onramping / offramping (partnership permission))
+ *hCaptcha JavaScript module*: [https://docs.hcaptcha.com/](https://docs.hcaptcha.com/)
+ *Discord API*: [https://discord.com/licenses](https://discord.com/licenses)
+ “@aparajita/capacitor-dark-mode”: “^4.0.0″,

**Awesome-cordova-plugins**

+ “@awesome-cordova-plugins/app-version”: “^6.3.0″,
+ “@awesome-cordova-plugins/calendar”: “^6.3.0″,
+ “@awesome-cordova-plugins/chooser”: “^6.3.0″,
+ “@awesome-cordova-plugins/clipboard”: “^6.3.0″,
+ “@awesome-cordova-plugins/core”: “^6.3.0″,
+ “@awesome-cordova-plugins/deeplinks”: “^6.3.0″,
+ “@awesome-cordova-plugins/email-composer”: “^6.3.0″,
+ “@awesome-cordova-plugins/file”: “^6.3.0″,
+ “@awesome-cordova-plugins/file-opener”: “^6.3.0″,
+ “@awesome-cordova-plugins/fingerprint-aio”: “^6.3.0″,
+ “@awesome-cordova-plugins/in-app-browser”: “^6.3.0″,
+ “@awesome-cordova-plugins/native-audio”: “^6.3.0″,
+ “@awesome-cordova-plugins/network”: “^6.3.0″,
+ “@awesome-cordova-plugins/open-native-settings”: “^6.3.0″,
+ “@awesome-cordova-plugins/safari-view-controller”: “^6.3.0″,
+ “@awesome-cordova-plugins/social-sharing”: “^6.3.0″,
+ “@awesome-cordova-plugins/streaming-media”: “^6.3.0″,
+ “@awesome-cordova-plugins/vibration”: “^6.3.0″,

**Capacitor-community**

+ “@capacitor-community/barcode-scanner”: “^4.0.1″,
+ “@capacitor-community/sqlite”: “5.0.5-2″,

**Capacitor-firebase**

+ “@capacitor-firebase/messaging”: “^5.1.0″,
+ “firebase”: “^9.23.0”,

**Capacitor**

+ “@capacitor/android”: “5.2.2″,
+ “@capacitor/app”: “^5.0.6″,
+ “@capacitor/browser”: “^5.0.6″,
+ “@capacitor/camera”: “^5.0.6″,
+ “@capacitor/core”: “5.2.2″,
+ “@capacitor/device”: “^5.0.6″,
+ “@capacitor/haptics”: “^5.0.6″,
+ “@capacitor/ios”: “5.2.2″,
+ “@capacitor/keyboard”: “^5.0.6″,
+ “@capacitor/push-notifications”: “^5.0.6″,
+ “@capacitor/splash-screen”: “^5.0.6″,
+ “@capacitor/status-bar”: “^5.0.6″,
+ “@capacitor/assets”: “^2.0.4″,
+ “@capacitor/cli”: “5.2.2″,

**Akita**

+ “@datorama/akita”: “^8.0.1″,
+ “@datorama/akita-ngdevtools”: “7.0.0″,
+ “@ionic/pwa-elements”: “^3.2.2″,
+ “@ionic/storage-angular”: “^4.0.0″,

**Sentry**

+ “@sentry/angular-ivy”: “7.54.0″,
+ “@sentry/capacitor”: “^0.12.1″,
+ “@sentry/cli”: “^2.19.0″,

**WalletConnect**

+ “@walletconnect/client”: “1.8.0″,
+ “@walletconnect/core”: “^2.8.6″,
+ “@walletconnect/sign-client”: “^2.8.6″,
+ “@walletconnect/types”: “^2.8.6″,
+ “@walletconnect/utils”: “^2.8.6″,
+ “@walletconnect/web3wallet”: “^1.8.5″,
+ “capacitor-data-storage-sqlite”: “^5.0.0-2”,
+ “immer”: “^9.0.19”,
+ “conventional-changelog-cli”: “^3.0.0″,
+ “cordova-set-version”: “^14.0.3″,
+ “cordova.plugins.diagnostic”: “^7.1.2″,
+ “ionic-plugin-deeplinks”: “^1.0.24”,
+ “webpack-bundle-analyzer”: “^4.9.0”,


Jobgrader Innovations GmbH

+ Münchener Strasse 45
+ 60329 Frankfurt am Main
+ Germany
+ Email: contact@jobgrader.app