const q = require('q');
const pjson = require('./../package.json');


async function setVersion(version) {

    const cordovaSetVersion = await import ('cordova-set-version');
    await cordovaSetVersion.default({ version });

    console.log('=============== END BEFORE BUILD =================');
}

module.exports = function(context) {
    console.log('=============== START BEFORE BUILD ===============');
    console.log('Version: ', pjson.version);

    var deferral = q.defer();

    setVersion(pjson.version.substring(0, pjson.version.lastIndexOf('.')+2)).then(
        () => {
            deferral.resolve()
        }
    );

    return deferral.promise;
}

