const prompt = require('prompt-sync')({ sigint: true });
const fs = require('fs');
const target = ['en','bn','de','el','fr','gu','he','hi','id','kn','ko','ml','ms','or','pa','ru','sa','sr','ta','te','th','tl','tr','ur','vi','zh'];

const key = prompt("Enter the key: ");

const folderChoice = prompt("Are you sure? [y/n]: ");

let folderLocation = "src/assets/i18n";

if(folderChoice != "Y" && folderChoice != "y") {
    console.log("Phew!!");
    return;
}

for(let i=0; i<target.length; i++) {

    fs.readdir(folderLocation, (err, files) => {

        var fileName = `${target[i]}.json`;

        const json = fs.readFileSync(folderLocation + "/" + fileName,'utf8');

        var originalJSON = JSON.parse(json);

        const keyPath = key.split('.');

    // Traverse the object based on key path
        let currentObject = originalJSON;

        for (let i = 0; i < keyPath.length - 1; i++) {
            const key = keyPath[i];
            if (!currentObject.hasOwnProperty(key)) {
                console.error(`Error: Key "${keyPath.join('.')}" does not exist.`);
                return;
            }
            currentObject = currentObject[key];
        }

        const keyToDelete = keyPath[keyPath.length - 1];
        delete currentObject[keyToDelete];

        fs.writeFile(folderLocation + "/" + fileName, JSON.stringify(originalJSON, null, 2), (err) => {
            if(err) {
                console.error(err);
            } else {
                console.log("Content written to the file: " + fileName);
            }
        })

    })

}

