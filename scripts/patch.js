const fs = require('fs');

const filesEdit = [
    {
        location: 'platforms/android/app/src/main/java/nl/xservices/plugins/videocaptureplus/VideoCapturePlus.java',
        removeContent: /import com.nimbusds.jose.util.IOUtils;/g,
        replaceContent: ''
    },
    {
        location: 'node_modules/@angular-devkit/build-angular/src/angular-cli-files/models/webpack-configs/browser.js',
        removeContent: /node: false/g,
        replaceContent: 'node: {crypto: true, stream: true}'
    },
    {
        location: 'plugins/cordova-plugin-file-opener2/plugin.xml',
        removeContent: /<uses-permission android:name=\"android.permission.REQUEST_INSTALL_PACKAGES\" \/>/g,
        replaceContent: ''
    }
]
filesEdit.forEach(k => {
    console.log(k)
    fs.readFile(k.location, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        const result = data.replace(k.removeContent, k.replaceContent);
    
        fs.writeFile(k.location, result, 'utf8', function (err) {
            if (err) return console.log(err);
        });
        console.log('Finished patch');
    });
})

fs.copyFile(
'node_modules/@evan.network/api-blockchain-core/libs/vade/pkg-nodejs/vade_evan_bg.js',
'node_modules/@evan.network/api-blockchain-core/libs/vade/pkg-browser/', (s) => {
    console.log('API Blockchain Core repository file patch complete!');
}, (e) => {
    console.log('Error copying file for patching API Blockchain Core repository');
})

// console.log('Executing patching Cordova Firebase');
// const fFireBase = 'plugins/cordova-plugin-firebase/scripts/after_prepare.js';
// fs.readFile(fFireBase, 'utf8', function (err,data) {
//     if (err) {
//         return console.log(err);
//     }
//     const result = data.replace(
//         'fs.readFileSync("platforms/android/res/values/strings.xml").toString();',
//         'fs.readFileSync("platforms/android/app/src/main/res/values/strings.xml").toString();')
//         .replace(
//         'fs.writeFileSync("platforms/android/res/values/strings.xml", strings);',
//         'fs.writeFileSync("platforms/android/app/src/main/res/values/strings.xml", strings);'
//     );
//
//     fs.writeFile(fFireBase, result, 'utf8', function (err) {
//         if (err) return console.log(err);
//     });
//     console.log('Finished executing patching Cordova Firebase');
// });

