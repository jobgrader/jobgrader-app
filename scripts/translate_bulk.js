const prompt = require('prompt-sync')({ sigint: true });
const fs = require('fs');
const lodash = require('lodash');
const source = 'en';
const target = ['bn','de','el','fr','gu','he','hi','id','kn','ko','ml','ms','or','pa','ru','sa','sr','ta','te','th','tl','tr','ur','vi','zh'];

const key = prompt("Enter the key: ");
const text = prompt("Enter the text in en: ");

const folderChoice = prompt("Do you want to update the translation folder content? [y/n]: ");

let folderLocation = "";

if(folderChoice == "Y" || folderChoice == "y") {
    folderLocation = prompt("Enter the folder path (default: src/assets/i18n): ");
    if(folderLocation === "") {
        folderLocation = "src/assets/i18n";
    }
}

let content = '';
// const globalFileName = 'translated.txt';



console.log(`${source}: "${key}": "${text}"`);
content = content + `${source}: "${key}": "${text}"` + '\n';

const main = async () => {
    if(folderChoice == "Y" || folderChoice == "y") {
        fs.readdir(folderLocation, (err, files) => {

            if(!files.includes(`en.json`)) {
                var originalJSON = JSON.parse({});
                var finalJSON = lodash.set(originalJSON, key, text);

                fs.writeFile(folderLocation + "/" + `en.json`, JSON.stringify(finalJSON, null, 2), (err) => {
                    if(err) {
                        console.error(err);
                    } else {
                        console.log("Content written to the file: " + `en.json`);
                    }
                })
            } else {

                var originalContent = fs.readFileSync(folderLocation + "/" + `en.json`,'utf8');
                // console.log(text);
                var originalJSON = JSON.parse(originalContent);

                // var finalJSON = Object.assign(originalJSON, { [key]: text });
                var finalJSON = lodash.set(originalJSON, key, text);

                fs.writeFile(folderLocation + "/" + `en.json`, JSON.stringify(finalJSON, null, 2), (err) => {
                    if(err) {
                        console.error(err);
                    } else {
                        console.log("Content written to the file: " + `en.json`);
                    }
                })
            }
        })
    }

    for(let i=0; i<target.length; i++) {
        var _ = target[i];
        var response = await fetch(`https://translate.googleapis.com/translate_a/single?client=gtx&sl=${source}&tl=${_}&dt=t&q=${encodeURIComponent(text)}`);
        response = await response.json();
        console.log(`${_}: ${key}: ${response[0][0][0]}`);
        content = content + `${_}: "${key}": "${response[0][0][0]}"` + '\n';
        if(folderChoice == "Y" || folderChoice == "y") {
            fs.readdir(folderLocation, (err, files) => {

                var fileName = `${target[i]}.json`;
                var text = response[0][0][0];

                if(!files.includes(fileName)) {
                    var originalJSON = JSON.parse({});
                    var finalJSON = lodash.set(originalJSON, key, text);
                    fs.writeFile(folderLocation + "/" + fileName, JSON.stringify(finalJSON, null, 2), (err) => {
                        if(err) {
                            console.error(err);
                        } else {
                            console.log("Content written to the file: " + fileName);
                        }
                    })
                } else {

                    const json = fs.readFileSync(folderLocation + "/" + fileName,'utf8');
                    // console.log(text);
                    var originalJSON = JSON.parse(json);
                    // var finalJSON = Object.assign(originalJSON, jsonContent);
                    var finalJSON = lodash.set(originalJSON, key, text);

                    fs.writeFile(folderLocation + "/" + fileName, JSON.stringify(finalJSON, null, 2), (err) => {
                        if(err) {
                            console.error(err);
                        } else {
                            console.log("Content written to the file: " + fileName);
                        }
                    })
                }

            })
        }
    }
    // fs.writeFile(globalFileName, content, (err) => {
    //     if(err) {
    //         console.error(err);
    //     } else {
    //         console.log("Content written to the file: " + globalFileName);
    //     }
    // })
}

main();


























