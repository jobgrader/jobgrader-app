import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SettingsAccountPage } from './settings-account.page';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { SharedModule } from '../shared/shared.module';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: SettingsAccountPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  providers: [SafariViewController, EmailComposer, File, SocialSharing],
  declarations: [SettingsAccountPage]
})
export class SettingsAccountPageModule {}
