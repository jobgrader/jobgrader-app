import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SettingsDevicesPage } from './settings-devices.page';

describe('SettingsDevicesPage', () => {
  let component: SettingsDevicesPage;
  let fixture: ComponentFixture<SettingsDevicesPage>;

 beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsDevicesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsDevicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
