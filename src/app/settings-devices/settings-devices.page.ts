import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { User } from '../core/models/User';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { AppleMappingTable } from '../core/providers/device/apple-products.mapping';
import { AndroidSupportedDevices } from '../core/providers/device/android-products.mapping';
import { NetworkService } from '../core/providers/network/network-service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

declare var window: any;

interface DevicePluginInformation {
  cordova?: string;
  model?: string;
  platform?: string;
  uuid?: string;
  version?: string;
  manufacturer?: string;
  isVirtual?: string;
  serial?: string;
}

@Component({
  selector: 'app-settings-devices',
  templateUrl: './settings-devices.page.html',
  styleUrls: ['./settings-devices.page.scss'],
})
export class SettingsDevicesPage implements OnInit {

  public devices = [];
  private user: User;
  profilePictureSrc;
  public deviceId = '';
  devicePluginInformation: DevicePluginInformation = {};

  constructor(
    private api: ApiProviderService,
    private appState: AppStateService,
    private userProviderService: UserProviderService,
    private nav: NavController,
    private loader: LoaderProviderService,
    private translateProviderService: TranslateProviderService,
    private toastController: ToastController,
    private _NetworkService: NetworkService,
    private secureStorage: SecureStorageService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) { }

  async ngOnInit() {

    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    // this.loader.loaderCreate();
    this.user = await this.userProviderService.getUser();
    this.profilePictureSrc = this.userPhotoServiceAkita.getPhoto();
    this.devices = await this.api.getUserDevices(this.user.userid);
    this.deviceId = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
    this.devices = this.devices.filter(x => (x.udid && x.udid != ''));
    var choice = this.devices.findIndex(x => x.deviceId == this.deviceId);
    this.devices.splice(0, 0, this.devices.splice(choice, 1)[0]);
    console.log(this.devices);
    this.devices.forEach(device => {
      if((device.osPlatform == 'iOS') || (device.manufacturer == 'Apple')) {
        device.model = !!AppleMappingTable[device.model] ? AppleMappingTable[device.model] : device.model;
      }
      if(device.osPlatform == 'Android') {
        var model = AndroidSupportedDevices.find(k => k.model == device.model);
        device.model = !!model ? model.marketing_name : device.model;
        device.manufacturer = !!model ? model.retail_branding : device.manufacturer;
      }
    })
    this.loader.loaderDismiss();
  }

  goBackToSettings() {
    this.nav.navigateBack('/dashboard/tab-settings');
  }

  async comingSoon() {
    const toast = await this.toastController.create({
      message: this.translateProviderService.instant('CONTACT.title'),
      position: 'top',
      duration: 3000
    })
    await toast.present()
  }

}
