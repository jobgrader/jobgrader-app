import { Component, ViewChild, NgZone } from '@angular/core';
import { IonContent, AlertController, ModalController, NavController, Platform } from '@ionic/angular';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { ChatService } from '../core/providers/chat/chat.service';
import { Observable } from 'rxjs';
import { ChatMessage, MessageStructure, MessageTypes } from '../core/providers/chat/chat-model';
import { DomSanitizer } from '@angular/platform-browser';
import { EventsList, EventsService } from '../core/providers/events/events.service';
import { Keyboard, KeyboardResize } from '@capacitor/keyboard';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SQLStorageService } from '../core/providers/sql/sqlStorage.service';
import { KycService } from '../kyc/services/kyc.service';
import { IonInfiniteScroll } from '@ionic/angular';
import { LockService } from '../lock-screen/lock.service';
import { CertificatesComponent } from './certificates/certificates.component';
import { DatePipe } from '@angular/common';
import { environment } from 'src/environments/environment';
import { MarketplaceCertificateComponent } from './marketplace-certificate/marketplace-certificate.component';
import { Capacitor } from '@capacitor/core';

declare var window: any;

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage {

  @ViewChild(IonContent) content: IonContent;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  private ngOnInitExecuted = false;

  public enableInfiniteScroll: boolean = false;
  public messageFetchSize: number = 10;
  public messageTypes = MessageTypes;
  public thisChatThreadGuid: string;
  public myUsername: string;
  public theirUsername: string;
  public theirUsernameWithDNS: string;
  public theirPhoto: string;
  public newMsg: string;
  private newMsgStr: MessageStructure;

  public shadowClass: string;
  public isLoading: boolean;
  public canReplyToConversation: boolean;
  private scrollDelay: number = 50;
  private scrollDuration: number = 500;

  private browser: any;
  private browserOptions = 'footer=yes,hideurlbar=no,footercolor=#BF7B54,hidenavigationbuttons=no,presentationstyle=pagesheet';

  constructor(
    private nav: NavController,
    private _TranslateProviderService: TranslateProviderService,
    public _ChatService: ChatService,
    public _DomSanitizer: DomSanitizer,
    public _EventsService: EventsService,
    private iab: InAppBrowser,
    private safariViewController: SafariViewController,
    public _Platform: Platform,
    private _LockService: LockService,
    private _KycService: KycService,
    public _SQLStorageService: SQLStorageService,
    private _ModalController: ModalController,
    private _AlertController: AlertController,
    public _Zone: NgZone
  ) {

  }

  async ionViewWillEnter() {
    this.isLoading = true;

    if (!this._ChatService.chatThreadToView) {
      return this.goBack();
    }

    this.thisChatThreadGuid = this._ChatService.chatThreadToView.guid;
    var myUsername = await this._ChatService.getMyUser_Username();
    this.myUsername = myUsername;
    this.theirUsername = this._ChatService.stripDNS(this._ChatService.chatThreadToView.userWrapper.chatUserName);
    this.theirUsernameWithDNS = this.theirUsername + environment.chatUsernameSuffix + '@' + environment.chatServerUrl;
    this.theirPhoto = this._ChatService.chatThreadToView.userWrapper.displayPicture;

    this._SQLStorageService.getXMessagesInThread(this.thisChatThreadGuid, null, this.messageFetchSize).then(messages => {
      this._ChatService.chatThreadToView.messages = messages;
      this.trySortMessges();
      this.checkCanReplyAndSetupUI();
    });

    this.subscribeToEvents();
  }

  async ionViewWillLeave() {
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();

    this._SQLStorageService.updateDraftMessage(this._ChatService.chatThreadToView.guid, this.newMsg).then(() => {
      this._ChatService.chatThreadToView = null;
      this._ChatService.receiveMessageCallback = null;
    });
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
    this.unsubscribeToEvents();
  }

  subscribeToEvents() {
    this.unsubscribeToEvents();
    this._EventsService.subscribe(EventsList.closeAnyChats, () => {
      if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
      window['skipAutoReconnect'] = true;
      this.goBack();
    });

    this.setResizeModeForiOS(true);
  }
  unsubscribeToEvents() {
    this._EventsService.unsubscribe(EventsList.closeAnyChats);
    this.setResizeModeForiOS(false);
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  private async componentInit() {
    this.isLoading = true;

    if (!this._ChatService.chatThreadToView) {
      return this.goBack();
    }

    this.myUsername = await this._ChatService.getMyUser_Username();
    this.theirUsername = this._ChatService.chatThreadToView.userWrapper.chatUserName;
    this.theirPhoto = this._ChatService.chatThreadToView.userWrapper.displayPicture;
  }

  getPreviousXMessages(event: any) {
    if (!event) { return; }

    var firstId = (this._ChatService.chatThreadToView && this._ChatService.chatThreadToView.messages.length > 0) ?
      this._ChatService.chatThreadToView.messages[0].numericId : null;

      setTimeout(() => {
        this._SQLStorageService.getXMessagesInThread(this.thisChatThreadGuid, firstId, this.messageFetchSize).then(messages => {
          if (messages && messages.length > 0) {
            this._ChatService.chatThreadToView.messages = messages.concat(this._ChatService.chatThreadToView.messages);

            setTimeout(() => {
              event.target.complete();
              // Need to scroll back to the last message before any were added.
              if (firstId != null) {
                var el = document.getElementById(`msg-${firstId}`);
                if (!el) { return; }
                el.scrollIntoView();
              }
            }, 500);
          }
          else {
            setTimeout(() => {
              event.target.complete();
            }, 500);
          }
        });
      }, 250);
  }

  checkToApplyShadow(event: any) {
    if (event && event.detail && event.detail.currentY > 15) {
      return this.shadowClass = "drop-shadow";
    }
    return this.shadowClass = null;
  }

  capsulateNewMsg(newMsg) {
    var typeMsg = typeof(newMsg)
    switch(typeMsg) {
      case 'string': this.newMsgStr = {
        type: MessageTypes.string,
        value: newMsg
      }; break;
      default: this.newMsgStr = {
        type: MessageTypes.string,
        value: newMsg
      }; break;
    }
    // console.log(this.validURL(newMsg))
    if(this.isValidHttpUrl(newMsg)) {
      this.newMsgStr = {
        type: MessageTypes.url,
        value: newMsg
      }
    }
    return JSON.stringify(this.newMsgStr)
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  isValidHttpUrl(str) {
    let url;
    try {
      url = new URL(str);
    } catch (_) {
      return false;
    }
    return url.protocol === "http:" || url.protocol === "https:";
  }

  linkify(text: string) {
    var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    });
  }

  previewURL(link: string) {
    if (this._Platform.is('ios') && this._Platform.is('hybrid')) {
      this.showSafariInstance(link);
    } else if (this._Platform.is('android') && this._Platform.is('hybrid')) {
      this.browser = this.iab.create(link, '_system', this.browserOptions);
    } else if (!this._Platform.is('hybrid')) {
      this.browser = this.iab.create(link, '_system');
    }
  }

  showSafariInstance(url: string) {
    this.safariViewController.isAvailable().then((available: boolean) => {
          if (available) {
            this.safariViewController.show({
              url,
              hidden: false,
              animated: true,
              transition: 'slide',
              enterReaderModeIfAvailable: false,
              tintColor: '#54BF7B'
            })
            .subscribe((result: any) => { },
              (error: any) => console.error(error) );
          } else {
            this.browser = this.iab.create(url, '_self', this.browserOptions);
          }
        }
      );
  }


  checkCanReplyAndSetupUI() {
    var stillFriends: boolean = false;
    var isUserAllowedToUseChatMarketplace: boolean = false;

    var a = this._KycService.isUserAllowedToUseChatMarketplace().then(result => {
      isUserAllowedToUseChatMarketplace = result;
    });

    a.then(() => {
      // Check if the users are still friends to enbale replying
      this._ChatService.isFriend(this.theirUsername).then(result => {
        stillFriends = result;

        this.isLoading = false;
        this.canReplyToConversation = (stillFriends && isUserAllowedToUseChatMarketplace);

        // It's possible that we've left the page before this resolves, therefore it will be null.
        // Also account for having changed the viewed thread whilst this is waiting to resolve.
        if (!this._ChatService.chatThreadToView || (this.thisChatThreadGuid != this._ChatService.chatThreadToView.guid)) {
          if (window.location.href.indexOf('/chat') > -1) {
            // alert(1)
            return this.goBack();
          }
          return; // don't do any navigating
        }

        if (this._ChatService.chatThreadToView.messages &&
          this._ChatService.chatThreadToView.messages.length > 0) {
            this.waitForElementToAppear('msgsLoadedElement').then(() => {
              this.finishSetup();
            });
        }
        else {
          this.finishSetup();
        }

        this._ChatService.receiveMessageCallback = () => {
          setTimeout(() => {
            this.content.scrollToBottom(this.scrollDuration);
          }, this.scrollDelay);
        };
      });
    });
  }

  private getFormattedDay(date: Date): string {
    const datePipe = new DatePipe('en-US');
    return datePipe.transform(date, 'dd MMM yyyy');
  }

  public processDateDisplay(date: Date): string {
    const datePipe = new DatePipe('en-US');
    var dateDisplay = datePipe.transform(date, 'dd.MM.yy');
    var today = new Date();
    if(dateDisplay == datePipe.transform(today, 'dd.MM.yy')) {
      return this._TranslateProviderService.instant('CHAT.today');
    }
    if(date == new Date(today.setDate(today.getDate() - 1))) {
      return this._TranslateProviderService.instant('CHAT.yesterday');
    }
    return dateDisplay;
  }

  displayDateIf(item: any){
    if(item.parsedMessage.type == MessageTypes.keyExchange) {
      return false;
    }
    var index = this._ChatService.chatThreadToView.messages.indexOf(item);
    if(index == 0) {
      return true;
    } else {
      if(this.getFormattedDay(item.date) != this.getFormattedDay(this._ChatService.chatThreadToView.messages[index-1].date)) {
        return true;
      }

      // if(item.parsedMessage.type != this._ChatService.chatThreadToView.messages[index-1].parsedMessage.type) {
      //   return true;
      // }
    }

    return false;
  }

  finishSetup() {
    // only mark thread and messages as read if they've been loaded up.
    var unreadMessages = this._ChatService.chatThreadToView.messages.filter(m => { return m.markedAsUnread; })
    this._SQLStorageService.updateMessagesMarkAsRead(unreadMessages);

    // Update this thread in storage.
    this._ChatService.chatThreadToView.markedAsUnread = false;
    this._ChatService.chatThreadToView.unreadCount = 0;
    this._SQLStorageService.updateThreadMarkedUnread(this._ChatService.chatThreadToView.guid);

    // This could be for friend requests too, hence the check.
    this._ChatService.checkToShowOrHideTabBadge();

    if (this.canReplyToConversation) {
      this._SQLStorageService.getDraftMessageForThread(this._ChatService.chatThreadToView.guid).then(draftMessage => {
        this.newMsg = draftMessage;
      });
    }
    this.content.scrollToBottom(this.scrollDuration).then(() => {
      if (this.canReplyToConversation) {
        try {
          // Bring up the keyboard + scroll down again.
          document.getElementById('txtMsg').focus();
          setTimeout(() => {
            this.content.scrollToBottom(this.scrollDuration).then(() => {
              this.enableInfiniteScroll = true;
            });
          }, 500);

          // Workaround to stop keyboard hiding on send. (source: https://github.com/ionic-team/ionic-plugin-keyboard/issues/81#issuecomment-318948443).
          this.applyKeyboardStayOpenFix();
        }
        catch(e) {}
      }
    });
  }

  sendMessageAsQRCode(messageType: MessageTypes) {
    console.log("sendMessageAsQRCode")
    if (!this.newMsg || this.newMsg.trim().length == 0) { return; }
    this._LockService.restart();

    if (this._ChatService.isConnected()) {
      this.finishSendMessageAsQRCode(messageType);
    }
    else {
      this._ChatService.connect(false).then(() => {
        this.finishSendMessageAsQRCode(messageType);
      })
    }
  }

  finishSendMessageAsQRCode(messageType: MessageTypes) {
    var nm = JSON.stringify({
      type: messageType,
      value: this.newMsg
    });
    var newMessage = <ChatMessage> {
      from: this.myUsername,
      to: this.theirUsername,
      message: nm,
      parsedMessage: JSON.parse(nm),
      markedAsUnread: false,
      date: new Date()
    };

    newMessage.parsedMessage.value = newMessage.parsedMessage.value.trim()

    this._ChatService.sendMessage(newMessage).then(() => {
      this.newMsg = null;
      setTimeout(() => {
        this.content.scrollToBottom(this.scrollDuration);
      }, this.scrollDelay);
    });
  }

  onPress() {
    var buttons = [
      { text: this._TranslateProviderService.instant('CHAT.messageType.text'), role: 'primary', handler: () => { this.sendMessage()} },
      { text: this._TranslateProviderService.instant('CHAT.messageType.qrCode'), handler: () => { this.sendMessageAsQRCode(MessageTypes.qrCode) } }
    ];
    if(!environment.production) {
      buttons.push({ text: this._TranslateProviderService.instant('CHAT.messageType.certificate'), handler: () => { this.sendMessageAsCertificate()} });
      buttons.push({ text: this._TranslateProviderService.instant('CHAT.messageType.healthCertificate'), handler: () => { this.sendMessageAsQRCode(MessageTypes.healthCertificate)} });
    }
    buttons.push({ text: this._TranslateProviderService.instant('BUTTON.CANCEL'), role: 'cancel', handler: () => { } })
    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant('CHAT.messageType.title'),
      message: this._TranslateProviderService.instant('CHAT.messageType.subHeading'),
      buttons: buttons
    }).then(alert => alert.present());
  }

  sendMessageAsCertificate() {
    console.log("sendMessageAsQRCode")
    // if (!this.newMsg || this.newMsg.trim().length == 0) { return; }
    this._LockService.restart();

    if (this._ChatService.isConnected()) {
      this.finishSendMessageAsCertificate();
    }
    else {
      this._ChatService.connect(false).then(() => {
        this.finishSendMessageAsCertificate();
      })
    }
  }

  finishSendMessageAsCertificate() {
    var nm = JSON.stringify({
      type: MessageTypes.certificate,
      value: JSON.stringify(
        {
            "@context": "https://www.w3.org/2018/credentials/v1",
            "id": "vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
            "type": [
                "VerifiableCredential",
                "HelixMarketplace"
            ],
            "issuer": {
                "id": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b"
            },
            "credentialSubject": {
                "id": "did:evan:testcore:0x1ee6bfe7034fdbc5ac5d57fb171b3820297d7711",
                "data": [
                    {
                        "companyName": "Fraunhofer",
                        "header": "",
                        "logo": "https://drive.google.com/uc?export=view&id=1qsRzoWSfAU0bk_h9vA6mx6dLHI43PwnV",
                        "issueDate": "2020-04-28T14:14:34.023Z",
                        "expiryDate": "2022-04-28T14:14:34.023Z",
                        "location": "Blockchain Reallabor, Euronova Campus, Halle 6, An der Hasenkaule 10, 50354 Hürth",
                        "issuer": "Blockchain Reallabor"
                    }
                ]
            },
            "validFrom": "1970-01-19T09:08:03Z",
            "validTo": "1970-01-20T02:39:15Z",
            "credentialStatus": {
                "id": "https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
                "type": "evan:evanCredential"
            },
            "proof": {
                "type": "EcdsaPublicKeySecp256k1",
                "created": "2020-04-28T14:14:34.023Z",
                "jws": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE1ODgwODMyNzMsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJLWUMiXSwiaWQiOiJ2YzpldmFuOnRlc3Rjb3JlOjB4MTM5ZDk2ZTEyN2U1YjI1OGJmZTY2ZTQ2MTZhNGFjMmEyNzFjMGU4NjQ0MmJiMGIxMTljYWFkODVlN2EzYmU1YiIsImlzc3VlciI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4MWVlNmJmZTcwMzRmZGJjNWFjNWQ1N2ZiMTcxYjM4MjAyOTdkNzcxMSIsImRhdGEiOnsidHJ1c3RlZEJ5IjoiQXV0aGFkYSIsInRydXN0TGV2ZWwiOjEsInRydXN0VmFsdWUiOjEsImlkIjpudWxsLCJ1c2VyIjpudWxsLCJsYXN0TW9kaWZlZCI6bnVsbCwiZ2VuZGVyIjoiZGl2ZXJzIiwidGl0bGUiOiJQcm9mLiBEci4iLCJjYWxsbmFtZSI6InRoZSByb2NrIiwiZmlyc3RuYW1lIjoiRXJpa2EiLCJtaWRkbGVuYW1lIjoiTWFyaWEiLCJsYXN0bmFtZSI6Ik11c3Rlcm1hbiIsIm1hcml0YWxzdGF0dXMiOiJ3aWRvd2VkIiwibWFpZGVubmFtZSI6Ik11c3RlcmZyYXUiLCJlbWFpbHR5cGUiOiJ0ZXh0LW9ubHkiLCJlbWFpbCI6ImVyaWthLm11c3Rlcm1hbkBibG9ja2NoYWluLWhlbGl4LmNvbSIsInBob25ldHlwZSI6Im1vYmxpZSIsInBob25lIjoiMDE3NTY3NDUyMyIsImRhdGVvZmJpcnRoIjoiMTkyMC0wNi0yMlQyMzowMDowMFoiLCJjaXR5b2ZiaXJ0aCI6IlN0b2NraG9sbSIsImNvdW50cnlvZmJpcnRoIjoiU2Nod2VkZW4iLCJjaXRpemVuc2hpcCI6Imdlcm1hbiIsInN0cmVldCI6IkxpbmRlbnN0cmFzZSA0MiIsImNpdHkiOiJnZXJtYW4iLCJ6aXAiOiIxMDk2OSIsInN0YXRlIjoiQmVybGluIiwiY291bnRyeSI6IkRldXRzY2hsYW5kIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudHR5cGUiOiJpZC1jYXJkIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudG51bWJlciI6IkM1TlZQQ1o1RSIsImlkZW50aWZpY2F0aW9uaXNzdWVjb3VudHJ5IjoiZ2VybWFueSIsImlkZW50aWZpY2F0aW9uaXNzdWVkYXRlIjoiMjAxNS0wMi0yOFQyMzowMDowMFoiLCJpZGVudGlmaWNhdGlvbmV4cGlyeWRhdGUiOiIyMDI1LTAyLTI4VDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2Vkb2N1bWVudG51bWJlciI6IkExNzE4MTkyMCIsImRyaXZlcmxpY2VuY2Vjb3VudHJ5IjoiZ2VybWFueSIsImRyaXZlcmxpY2VuY2Vpc3N1ZWRhdGUiOiIyMDIwLTAxLTMxVDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2VleHBpcnlkYXRlIjoiMjAzMC0wMS0zMVQyMzowMDowMFoiLCJ0YXhyZXNpZGVuY3kiOiJnZXJtYW55IiwidGF4bnVtYmVyIjoiWDQzN04yMzg2NyIsInRlcm1zb2Z1c2UiOiJUZXJtc29mdXNlIiwidGVybXNvZnVzZXRoaXJkcGFydGllcyI6IlRlcm1zb2Z1c2V0aGlyZHBhcnRpZXMifX0sInZhbGlkRnJvbSI6IjE5NzAtMDEtMTlUMDk6MDg6MDNaIiwidmFsaWRUbyI6IjE5NzAtMDEtMjBUMDI6Mzk6MTVaIiwiY3JlZGVudGlhbFN0YXR1cyI6eyJpZCI6Imh0dHBzOi8vdGVzdGNvcmUuZXZhbi5uZXR3b3JrL3NtYXJ0LWFnZW50cy9zbWFydC1hZ2VudC1kaWQtcmVzb2x2ZXIvdmMvc3RhdHVzL3ZjOmV2YW46dGVzdGNvcmU6MHgxMzlkOTZlMTI3ZTViMjU4YmZlNjZlNDYxNmE0YWMyYTI3MWMwZTg2NDQyYmIwYjExOWNhYWQ4NWU3YTNiZTViIiwidHlwZSI6ImV2YW46ZXZhbkNyZWRlbnRpYWwifX0sImlzcyI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9.ABk8RBlpCAX-gY47_aRxskCCrl_AsluEDbYNEY8BNlY2NS78aoIWXDNNhoJe0J5EpwnWQyR02I9qMn_lqL0qFwE",
                "proofPurpose": "assertionMethod",
                "verificationMethod": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b#key-1"
            }
        })
    });
    var newMessage = <ChatMessage> {
      from: this.myUsername,
      to: this.theirUsername,
      message: nm,
      parsedMessage: JSON.parse(nm),
      markedAsUnread: false,
      date: new Date()
    };

    // newMessage.parsedMessage.value = newMessage.parsedMessage.value.trim()

    this._ChatService.sendMessage(newMessage).then(() => {
      this.newMsg = null;
      setTimeout(() => {
        this.content.scrollToBottom(this.scrollDuration);
      }, this.scrollDelay);
    });
  }

  sendMessage() {
    if (!this.newMsg || this.newMsg.trim().length == 0) { return; }
    this._LockService.restart();

    if (this._ChatService.isConnected()) {
      this.finishSendMessage();
    }
    else {
      this._ChatService.connect(false).then(() => {
        this.finishSendMessage();
      })
    }
  }

  finishSendMessage() {
    var nm = this.capsulateNewMsg(this.newMsg)
    var newMessage = <ChatMessage> {
      from: this.myUsername,
      to: this.theirUsername,
      message: nm,
      parsedMessage: JSON.parse(nm),
      markedAsUnread: false,
      date: new Date()
    };

    newMessage.parsedMessage.value = newMessage.parsedMessage.value.trim()

    this._ChatService.sendMessage(newMessage).then(() => {
      this.newMsg = null;
      setTimeout(() => {
        this.content.scrollToBottom(this.scrollDuration);
      }, this.scrollDelay);
    });
  }

  waitForElementToAppear(elementId): Promise<any> {
    return new Observable(function (observer) {
      var el_ref;
      var f = () => {
        el_ref = document.getElementById(elementId);
        if (el_ref) {
          observer.next(el_ref);
          observer.complete();
          return;
        }
        window.requestAnimationFrame(f);
      };
      f();
    }).toPromise();
  }

  goBack() {
    this._Zone.run(() => {
      this.nav.navigateBack('/dashboard/tab-contact');
    });
  }

  setResizeModeForiOS(val: boolean) {
    if (this._Platform.is('ios')) {
      var resizeMode = (val) ? KeyboardResize.Native : KeyboardResize.None;
      Keyboard.setResizeMode({ mode: resizeMode });
    }
  }

  applyKeyboardStayOpenFix() {
    let el = document.getElementById('btnSend');
    if (!el) { return; }

    el.addEventListener('click', (e) => { this.stopBubble(e); });
    el.addEventListener('mousedown', (e) => { this.stopBubble(e); });
    el.addEventListener('touchdown', (e) => { this.stopBubble(e); });
    el.addEventListener('touchmove', (e) => { this.stopBubble(e); });
    el.addEventListener('touchstart', (e) => { this.stopBubble(e); });

    //Triggered by a phone
    el.addEventListener('touchend', (e) => { this.stopBubble(e); this.sendMessage(); });
    //Triggered by the browser
    el.addEventListener('mouseup', (event) => { this.sendMessage(); });
  }

  stopBubble(event) {
    event.preventDefault();
    event.stopPropagation(); //Stops event bubbling
  }

  copyMessage(msg: string) {
    var parsed = JSON.parse(msg)
    if (parsed.type == MessageTypes.keyExchange) { return; }
    if (!window.cordova || !window.cordova.plugins.clipboard) { return alert('nope'); }
    window.cordova.plugins.clipboard.copy(parsed.value);
    this._ChatService.presentToast(this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'));
  }

  trySortMessges() {
    try {
      this._ChatService.chatThreadToView.messages.forEach(m => {
        if (m.date) {
          m.date = new Date(m.date);
          m.unixDate = m.date.getTime()/1000;
        }
      });
      this._ChatService.chatThreadToView.messages.sort((a, b) => {
        if (a.unixDate === b.unixDate) {
          return 0;
        }
        return (a.unixDate > b.unixDate) ? 1 : -1;
      });
    }
    catch(e) {
      console.log('*** error sorting dates of messages:');
      console.log(e);
    }
  }

  public async showCertificate(input: any, mode: string) {
    const modal = await this._ModalController.create({
      component: (mode == 'use-case') ? MarketplaceCertificateComponent : CertificatesComponent,
      componentProps: {
        data: {
          input: (mode == 'use-case') ? JSON.parse(input) : input,
          // tntUseCaseList: this.tntUseCaseList
        }
      }
    })
    modal.onDidDismiss().then(async () => {

    })
    await modal.present();
  }
}
