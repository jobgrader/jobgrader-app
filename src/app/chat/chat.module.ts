import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { ChatPage } from './chat.page';
import { TranslateModule } from '@ngx-translate/core';
import { AutosizeModule } from 'ngx-autosize';
import { QRCodeModule } from 'angularx-qrcode';
import { CertificatesModule } from './certificates/certificates.module';
import { MarketplaceCertificateModule } from './marketplace-certificate/marketplace-certificate.module';

const routes: Routes = [
  {
    path: '',
    component: ChatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AutosizeModule,
    QRCodeModule,
    CertificatesModule,
    MarketplaceCertificateModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  providers: [SafariViewController],
  declarations: [ChatPage]
})
export class ChatPageModule {}
