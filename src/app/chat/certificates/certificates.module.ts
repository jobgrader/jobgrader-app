import { NgModule } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { CertificatesComponent } from './certificates.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@NgModule({
    imports: [
        CommonModule,
        QRCodeModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        SharedModule,
        TranslateModule.forChild()
    ],
    providers: [
        SafariViewController,
        SocialSharing,
        File,
        FileOpener
    ],
    declarations: [CertificatesComponent],
    exports: [CertificatesComponent]
})
export class CertificatesModule {}
