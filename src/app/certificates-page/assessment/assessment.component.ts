import { Component, Input, OnInit } from '@angular/core';
import { ActionSheetController, ModalController, ToastController, Platform, AlertController } from '@ionic/angular';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Clipboard } from '@capacitor/clipboard';
import { Browser } from '@capacitor/browser';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { DatePipe } from '@angular/common';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { environment } from 'src/environments/environment';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { DropboxService } from 'src/app/core/providers/cloud/dropbox.service';
import { GDriveBackupObject, GdriveService } from 'src/app/core/providers/cloud/gdrive.service';
import { ActivityKeys, UserActivitiesService, UserActivity } from 'src/app/core/providers/user-activities/user-activities.service';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
// import { createPresentation } from '@digitalbazaar/vc';

declare var iCloudDocStorage: any;
@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss'],
})
export class AssessmentComponent implements OnInit {
  @Input() data: any;

  public qrSet = null;
  public displayItems = [];
  public imageUrl: SafeUrl = '';

  browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#BF7B54,hidenavigationbuttons=yes,presentationstyle=pagesheet';
  constructor(
    private _ModalController: ModalController,
    private _SecureStorageService: SecureStorageService,
    private _LoaderProviderService: LoaderProviderService,
    private _TranslateProviderService: TranslateProviderService,
    private _ToastController: ToastController,
    private platform: Platform,
    private _File: File,
    private _FileOpener: FileOpener,
    private _Dropbox: DropboxService,
    private _GDrive: GdriveService,
    private _SocialSharing: SocialSharing,
    private _UserActivity: UserActivitiesService,
    private _AlertController: AlertController,
    private sanitizer: DomSanitizer,
    private browser: InAppBrowser
  ) { }

  public profilePictureSrc = '../../../assets/job-user.svg';
  public emptyCertificateProfile = '../../../assets/job-user.svg';

  async ngOnInit() {
    const urlCreator = (window as any).URL || (window as any).webkitURL;
    console.log(this.data.input);

    // try {
    //   var id = this.data.input.vc.id;
    //   var holder = this.data.input.vc.credentialSubject.id;
    //   var vp = createPresentation({verifiableCredential: this.data.input.vc, id, holder})
    //   this.qrSet = JSON.stringify(vp);
    // } catch(e) {
      // console.log(e);
      delete this.data.input.vc.credentialSubject.data;
      this.qrSet = JSON.stringify(this.data.input.vc);
    // }

    if(!this.data.input.credentialValues) {
      this.data.input = JSON.parse(this.data.input);
    }
    var keys = Object.keys(this.data.input.credentialSubjectRaw)
    console.log(keys);
    this.imageUrl = !!this.data.input.credentialSubjectRaw.badgeUrl ? this.sanitizer.bypassSecurityTrustUrl(this.data.input.credentialSubjectRaw.badgeUrl) : this.emptyCertificateProfile;
    const datePipe = new DatePipe('en-US');
    keys = keys.filter(k => !['badgeUrl', 'imageUrl', 'certificateUrl', 'skills'].includes(k));
    keys.forEach(k => {
      var tem = Object.assign({}, {key: k, value: this.data.input.credentialSubjectRaw[k]});
      if(k == "issueDate" || k == "expiryDate") {
        tem.value = datePipe.transform(tem.value, "dd.MM.yyyy HH:mm");
      }
      this.displayItems.push(tem);
    })

  }

  async saveAsFile(choice: string) {
    console.log(choice);

    var path = null;
    const prefix = (choice == 'vc') ? '' : 'vp-';

    if( this.platform.is('ios') && this.platform.is('hybrid') ) {
      path = this._File.documentsDirectory;
    } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
      path = this._File.dataDirectory;
    }
    var parsed = null;
    try {
      parsed = JSON.parse(this.qrSet);
    } catch(e) {
      parsed = this.qrSet;
    }
    console.log(parsed);

    var fileName = `${prefix}${parsed.id.replaceAll(":","-")}.json`;
    var fullPath = path + fileName;
    const notifText = (choice == 'vc') ? this._TranslateProviderService.instant('CLOUDBACKUP.vc-success') : this._TranslateProviderService.instant('CLOUDBACKUP.vp-success');

    var blobUrl = URL.createObjectURL(new Blob([this.qrSet], {type: "application/json"}));

    if(this.platform.is('hybrid')) {
      if(this.platform.is('ios')) {
        await this._File.writeFile(path, fileName, new Blob([this.qrSet], {type: "application/json"}), { replace: true });

        try {
          var presentToastOpener = () => {
            iCloudDocStorage.fileList("Cloud", (a) => {
              var aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
              var check = aaa.find(aa => aa.includes(fileName) );
              if(check) {
                this.presentToastWithOpen(notifText, check);
                if(choice == 'vc') {
                  this._UserActivity.updateActivity(ActivityKeys.VC_BACKUP, {
                    vcId: parsed.id,
                    vcType: "Assessment",
                    fileName,
                    iCloud: true
                  });
                } else {
                  this._UserActivity.updateActivity(ActivityKeys.VP_BACKUP, {
                    vpId: parsed.id,
                    vcType: "Assessment",
                    fileName,
                    iCloud: true
                  });
                }
              }
            }, (b) => {
              console.log("b", b);
            })
          }

          iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
            iCloudDocStorage.syncToCloud(fullPath, (ss) => {
              presentToastOpener();
            }, (e) => {
              presentToastOpener();
            });
          }, (e) => {
            console.log("initUbiquitousContainer error",e)
          });

        } catch(e) {
          console.log(e);
          await this._SocialSharing.shareWithOptions({
            subject: fileName,
            files: [ path + fileName ]
          });
        }


        // if(this._GDrive.user) {
        //   if(this._GDrive.user.email) {
        //     await this._GDrive.init([{
        //       fileName, contents: qrSet
        //     }], notifText).then(status => {
        //       if(status) {
        //         if(choice == 'vc') {
        //           this._UserActivity.updateActivity(ActivityKeys.VC_BACKUP);
        //         } else {
        //           this._UserActivity.updateActivity(ActivityKeys.VP_BACKUP);
        //         }
        //       }
        //     });
        //   }
        // }

      }
      else if(this.platform.is('android')) {
        await this._File.writeFile(path, fileName, new Blob([this.qrSet], {type: "application/json"}), { replace: true });

        try{

          this.presentAlertForPermission([{
            fileName, contents: this.qrSet
          }], notifText, choice);

        } catch(e) {
          await this._SocialSharing.shareWithOptions({
            subject: fileName,
            files: [ path + fileName ]
          });
        }

      }

    } else {
      this.browser.create(blobUrl, '_blank');

      this.presentAlertForPermission([{
        fileName, contents: this.qrSet
      }], notifText, choice);

      // await this._Dropbox.backupFiles([{
      //   fileName, contents: qrSet
      // }], notifText);
    }

  }

  presentToastWithOpen(message: string, filePath: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
      cssClass: 'toast-notification-open',
      buttons: [
        {
          side: 'end',
          icon: 'folder',
          text: this._TranslateProviderService.instant('APPSTORE.open'),
          handler: async () => {
              this._FileOpener.open(filePath, "application/json").then((aaa) => {
                console.log("aaa", aaa);
              }).catch(bbb => {
                console.log("bbb", bbb);
              })
            }
        }
      ]
    }).then(toast => toast.present())
  }

  presentAlertForPermission(fileContentArray: GDriveBackupObject[], notificationText: string, choice: string) {
    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant("GOOGLEDRIVE_PERMISSION_HEADER"),
      message: (choice == 'vc') ? this._TranslateProviderService.instant("GOOGLEDRIVE_PERMISSION_VC_MESSAGE") : this._TranslateProviderService.instant("GOOGLEDRIVE_PERMISSION_VP_MESSAGE"),
      buttons: [
        {
          text: this._TranslateProviderService.instant('SETTINGS.no'),
          role: "cancel",
          handler: () => {

          }
        },
        {
          text: this._TranslateProviderService.instant('SETTINGS.yes'),
          handler: () => {
            this._GDrive.init(fileContentArray, notificationText).then(status => {
              if(status) {
                for(var i=0; i<fileContentArray.length; i++) {

                  if(choice == 'vc') {
                    this._UserActivity.updateActivity(ActivityKeys.VC_BACKUP, {
                      vcId: JSON.parse(fileContentArray[i].contents).id,
                      fileName: fileContentArray[i].fileName,
                      vcType: "Assessment",
                      gDrive: true
                    });
                  } else {
                    this._UserActivity.updateActivity(ActivityKeys.VP_BACKUP, {
                      vpId: JSON.parse(fileContentArray[i].contents).id,
                      fileName: fileContentArray[i].fileName,
                      vcType: "Assessment",
                      gDrive: true
                    });
                  }

                }
              }
            });
          }
        }
      ]
    }).then(al => al.present())
  }

  close() {
    this._ModalController.dismiss();
  }

  openCertificate(url: string) {
    if(!url) return;

    this.browser.create(url, '_system');

  }

  async copyParameter($event, key: string, value: string) {
      await Clipboard.write({ string: value });
      await this.presentToast(key + this._TranslateProviderService.instant('SETTINGSACCOUNT.copied'))
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 3000, position: 'top' });
    await toast.present();
  }

  async deleteCertificate() {
    await this._LoaderProviderService.loaderCreate();
    var assessmentCertificateList = this.data.assessmentCertificateList.filter(k => k.qrCode != this.data.input.qrCode);
    await this._SecureStorageService.setValue(SecureStorageKey.assessmentCertificate, JSON.stringify(assessmentCertificateList));
    await this.presentToast( this._TranslateProviderService.instant('ASSESSMENT_CERTIFICATE_DELETED'));
    await this._LoaderProviderService.loaderDismiss();
    this._ModalController.dismiss();
  }
}
