import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CertificatesPagePageRoutingModule } from './certificates-page-routing.module';
import { CertificatesPagePage } from './certificates-page.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { CertificatesModule } from '../chat/certificates/certificates.module';
import { JobHeaderModule } from '../job-header/job-header.module';
import { AssessmentModule } from './assessment/assessment.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    CertificatesModule,
    AssessmentModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    CertificatesPagePageRoutingModule
  ],
  declarations: [CertificatesPagePage]
})
export class CertificatesPagePageModule {}
