import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ThemeSwitcherService } from "../core/providers/theme/theme-switcher.service";
import { EventsList, EventsService } from "../core/providers/events/events.service";

@Component({
  selector: "app-user-navigation",
  templateUrl: "./user-navigation.component.html",
  styleUrls: ["./user-navigation.component.scss"],
})
export class UserNavigationComponent implements OnInit {
  @Input("profilePictureSrc") profilePictureSrc: string = "";
  @Input("placeholderImage") placeholderImage: string = "";

  @Output("onNavigation") onNavigation = new EventEmitter<
    | "hcaptcha"
    | "cvat"
    | "fortune"
    | "audino"
    | "ab"
    | "priceGuessing"
    | "formElements"
    // | "polls"
    | "quiz"
  >();

  active_item_index = 6;
  toggle: boolean = false;
  constructor(
    public theme: ThemeSwitcherService,
    private events: EventsService
  ) {}

  ngOnInit() {
    this.handleToggleClick();
    this.events.subscribe(EventsList.changeDp, (photo) => {
      console.log("Event subscription photo", photo);
      this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';
    })
  }

  handleToggleClick() {
    this.toggle = !this.toggle;
  }

  emitNavigationClick(
    type:
    | "hcaptcha"
    | "cvat"
    | "fortune"
    | "audino"
    | "ab"
    | "priceGuessing"
    | "formElements"
    // | "polls"
    | "quiz"
  ) {
    this.onNavigation.emit(type);
  }
}
