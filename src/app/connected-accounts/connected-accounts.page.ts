import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, Platform, ToastController } from '@ionic/angular';
import { DropboxService } from '../core/providers/cloud/dropbox.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { GdriveService } from '../core/providers/cloud/gdrive.service';
import { OnedriveService } from '../core/providers/cloud/onedrive.service';
import { DiscordService } from '../core/providers/cloud/discord.service';
import { LinkedinService } from '../core/providers/cloud/linkedin.service';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { TelegramService } from '@services/cloud/telegram.service';

@Component({
  selector: 'app-connected-accounts',
  templateUrl: './connected-accounts.page.html',
  styleUrls: ['./connected-accounts.page.scss'],
})
export class ConnectedAccountsPage implements OnInit {
  
  googleDriveOSSupport = true;
  iCloudOSSupport = false;
  dropboxOSSupport = false;
  onedriveOSSupport = true;
  discordOSSupport = true;
  linkedinOSSupport = true;
  telegramOSSupport = true;

  onedriveStatus = false;
  dropboxStatus = false;
  discordStatus = false;
  linkedinStatus = false;
  telegramStatus = false;
  
  profilePictureSrc;

  private testMessage = `Testing 101. #jobgrader`;

  constructor(
    private _Platform: Platform,
    private _NavController: NavController,
    private _UserPhotoServiceAkita: UserPhotoServiceAkita,
    private _ToastController: ToastController,
    public _Dropbox: DropboxService,
    public _GDrive: GdriveService,
    public _Onedrive: OnedriveService,
    public _Discord: DiscordService,
    public _Linkedin: LinkedinService,
    private _Social: SocialSharing,
    public _Telegram: TelegramService,
    private _Alert: AlertController
  ) { }

  ngOnInit() {
    this.profilePictureSrc = this._UserPhotoServiceAkita.getPhoto();
    this.dropboxOSSupport = !!this._Platform.is('hybrid');
    this.iCloudOSSupport = (this._Platform.is('hybrid') && this._Platform.is('ios'));
    this.dropboxStatus = !!this._Dropbox.access_token; 
    this.discordStatus = !!this._Discord.user;
    this.linkedinStatus = !!this._Linkedin.user_data;
    this.telegramStatus = !!this._Telegram.user;
  }

  public goBack(): void {
    this._NavController.navigateBack('/dashboard/tab-settings');
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 2000, position: 'top' });
    await toast.present();
  }

  async alterTelegram(value: boolean) {
    if(value) {
      this._Telegram.openTelegramOAuth();
    } else {
      this._Telegram.user = null;
    }
  }

  async alterGoogleDriveAuth(value: boolean) {
    if(value) {
      await this._GDrive.signInToGoogle();
    } else {
      await this._GDrive.handleSignoutClick();
    }
  }

  async alterDropbox(value: boolean) {
    if(value) {
      await this._Dropbox.signInToDropbox((this._Platform.is('hybrid') ? "jobgrader://jobgrader.app/connected-accounts" : window.location.href));
      this.dropboxStatus = true;
    } else {
      await this._Dropbox.logOut();
    }
  }

  async alterDiscord(value: boolean) {
    if(value) {
      await this._Discord.openDiscordOAuth();
      this.discordStatus = true;
    } else {
      await this._Discord.logOut();
    }
  }

  async alterLinkedin(value: boolean) {
    if(value) {
      await this._Linkedin.openLinkedInOAuth();
      this.linkedinStatus = true;
    } else {
      await this._Linkedin.logOut();
    }
  }

  shareOnTwitter() {
    console.log("shareOnTwitter()");
    this._Social.shareViaTwitter(this.testMessage, this.profilePictureSrc).then(response => {
      console.log("response", response);
    }).catch(error => {
      console.log("error", error);
    });
  }

  shareOnFacebook() {
    console.log("shareOnFacebook()");
    this._Social.shareViaFacebook(this.testMessage, this.profilePictureSrc).then(response => {
      console.log("response", response);
    }).catch(error => {
      console.log("error", error);
    });
  }

  shareOnInstagram() {
    console.log("shareOnInstagram()");
    this._Social.shareViaInstagram(this.testMessage, this.profilePictureSrc).then(response => {
      console.log("response", response);
    }).catch(error => {
      console.log("error", error);
    });
  }

  shareOnWhatsApp() {
    console.log("shareOnWhatsApp()");
    this._Social.shareViaWhatsApp(this.testMessage, this.profilePictureSrc).then(response => {
      console.log("response", response);
    }).catch(error => {
      console.log("error", error);
    });
  }

  customShare() {
    console.log("customShare()");
    this._Alert.create({
      header: 'Enter the app name',
      inputs: [{
        type: 'text',
        placeholder: 'com.apple.social.facebook',
        name: 'appName'
      }],
      buttons: [{
        text: 'OK',
        handler: (data) => {
          const appName = data.appName;
          this._Social.shareVia(appName, this.testMessage).then(aa => {
            alert(JSON.stringify(aa));
          }).catch(e => {
            alert(`error: ${JSON.stringify(e)}`);
          })
        }
      }, {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {}
      }]
    }).then(a => {
      a.present();
    }).catch(e => {
      console.log(e);
    })
    
  }


}
