export interface Country {
    country: string;
    code2: string;
    code3: string;
}
