import { Country } from '../interfaces/country.interface';

export const countries: Array<Country> = [
    // {
    //   country: 'Afghanistan',
    //   code2: 'AF',
    //   code3: 'AFG',
    // },
    {
        country: 'Aland Islands',
        code2: 'AX',
        code3: 'ALA',
    }, // Part of Finland
    {
        country: 'Albania',
        code2: 'AL',
        code3: 'ALB',
    },
    {
        country: 'Algeria',
        code2: 'DZ',
        code3: 'DZA',
    },
    {
        country: 'American Samoa',
        code2: 'AS',
        code3: 'ASM',
    },
    {
        country: 'Andorra',
        code2: 'AD',
        code3: 'AND',
    },
    {
        country: 'Angola',
        code2: 'AO',
        code3: 'AGO',
    },
    // {
    //   country: 'Anguilla',
    //   code2: 'AI',
    //   code3: 'AIA',
    // }, // Part of the UK
    // {
    //   country: 'Antarctica',
    //   code2: 'AQ',
    //   code3: 'ATA',
    // },
    {
        country: 'Antigua and Barbuda',
        code2: 'AG',
        code3: 'ATG',
    },
    {
        country: 'Argentina',
        code2: 'AR',
        code3: 'ARG',
    },
    {
        country: 'Armenia',
        code2: 'AM',
        code3: 'ARM',
    },
    {
        country: 'Aruba',
        code2: 'AW',
        code3: 'ABW',
    }, // Part of the Netherlands
    {
        country: 'Australia',
        code2: 'AU',
        code3: 'AUS',
    },
    {
        country: 'Austria',
        code2: 'AT',
        code3: 'AUT',
    },
    {
        country: 'Azerbaijan',
        code2: 'AZ',
        code3: 'AZE',
    },
    {
        country: 'Bahamas (the)',
        code2: 'BS',
        code3: 'BHS',
    },
    {
        country: 'Bahrain',
        code2: 'BH',
        code3: 'BHR',
    },
    {
        country: 'Bangladesh',
        code2: 'BD',
        code3: 'BGD',
    },
    {
        country: 'Barbados',
        code2: 'BB',
        code3: 'BRB',
    },
    {
        country: 'Belarus',
        code2: 'BY',
        code3: 'BLR',
    },
    {
        country: 'Belgium',
        code2: 'BE',
        code3: 'BEL',
    },
    {
        country: 'Belize',
        code2: 'BZ',
        code3: 'BLZ',
    },
    {
        country: 'Benin',
        code2: 'BJ',
        code3: 'BEN',
    },
    // {
    //   country: 'Bermuda',
    //   code2: 'BM',
    //   code3: 'BMU',
    // }, // Part of the UK
    // {
    //   country: 'Bhutan',
    //   code2: 'BT',
    //   code3: 'BTN',
    // },
    {
        country: 'Bolivia (Plurinational State of)',
        code2: 'BO',
        code3: 'BOL',
    },
    {
        country: 'Bonaire, Sint Eustatius and Saba',
        code2: 'BQ',
        code3: 'BES',
    }, // Part of the Netherlands
    {
        country: 'Bosnia and Herzegovina',
        code2: 'BA',
        code3: 'BIH',
    },
    {
        country: 'Botswana',
        code2: 'BW',
        code3: 'BWA',
    },
    // {
    //   country: 'Bouvet Island',
    //   code2: 'BV',
    //   code3: 'BVT',
    // },
    {
        country: 'Brazil',
        code2: 'BR',
        code3: 'BRA',
    },
    // {
    //   country: 'British Indian Ocean Territory (the)',
    //   code2: 'IO',
    //   code3: 'IOT',
    // },
    {
        country: 'Brunei Darussalam',
        code2: 'BN',
        code3: 'BRN',
    },
    {
        country: 'Bulgaria',
        code2: 'BG',
        code3: 'BGR',
    },
    {
        country: 'Burkina Faso',
        code2: 'BF',
        code3: 'BFA',
    },
    // {
    //   country: 'Burundi',
    //   code2: 'BI',
    //   code3: 'BDI',
    // },
    {
        country: 'Cabo Verde',
        code2: 'CV',
        code3: 'CPV',
    },
    {
        country: 'Cambodia',
        code2: 'KH',
        code3: 'KHM',
    },
    {
        country: 'Cameroon',
        code2: 'CM',
        code3: 'CMR',
    },
    {
        country: 'Canada',
        code2: 'CA',
        code3: 'CAN',
    },
    // {
    //   country: 'Cayman Islands (the)',
    //   code2: 'KY',
    //   code3: 'CYM',
    // }, // Part of the UK
    {
        country: 'Central African Republic (the)',
        code2: 'CF',
        code3: 'CAF',
    },
    // {
    //   country: 'Chad',
    //   code2: 'TD',
    //   code3: 'TCD',
    // },
    // {
    //   country: 'Chile',
    //   code2: 'CL',
    //   code3: 'CHL',
    // },
    // {
    //   country: 'China',
    //   code2: 'CN',
    //   code3: 'CHN',
    // },
    {
        country: 'Christmas Island',
        code2: 'CX',
        code3: 'CXR',
    },
    {
        country: 'Cocos (Keeling) Islands (the)',
        code2: 'CC',
        code3: 'CCK',
    },
    {
        country: 'Colombia',
        code2: 'CO',
        code3: 'COL',
    },
    // {
    //   country: 'Comoros (the)',
    //   code2: 'KM',
    //   code3: 'COM',
    // },
    {
        country: 'Congo (the Democratic Republic of the)',
        code2: 'CD',
        code3: 'COD',
    },
    {
        country: 'Congo (the)',
        code2: 'CG',
        code3: 'COG',
    },
    // {
    //   country: 'Cook Islands (the)',
    //   code2: 'CK',
    //   code3: 'COK',
    // },
    {
        country: 'Costa Rica',
        code2: 'CR',
        code3: 'CRI',
    },
    {
        country: 'Cote d`Ivoire',
        code2: 'CI',
        code3: 'CIV',
    },
    {
        country: 'Croatia',
        code2: 'HR',
        code3: 'HRV',
    },
    {
        country: 'Cuba',
        code2: 'CU',
        code3: 'CUB',
    },
    {
        country: 'Curacao',
        code2: 'CW',
        code3: 'CUW',
    }, // Part of the Netherlands
    {
        country: 'Cyprus',
        code2: 'CY',
        code3: 'CYP',
    },
    {
        country: 'Czech Republic (the)',
        code2: 'CZ',
        code3: 'CZE',
    },
    {
        country: 'Denmark',
        code2: 'DK',
        code3: 'DNK',
    },
    // {
    //   country: 'Djibouti',
    //   code2: 'DJ',
    //   code3: 'DJI',
    // },
    // {
    //   country: 'Dominica',
    //   code2: 'DM',
    //   code3: 'DMA',
    // },
    {
        country: 'Dominican Republic (the)',
        code2: 'DO',
        code3: 'DOM',
    },
    {
        country: 'Ecuador',
        code2: 'EC',
        code3: 'ECU',
    },
    // {
    //   country: 'Egypt',
    //   code2: 'EG',
    //   code3: 'EGY',
    // },
    {
        country: 'El Salvador',
        code2: 'SV',
        code3: 'SLV',
    },
    // {
    //   country: 'Equatorial Guinea',
    //   code2: 'GQ',
    //   code3: 'GNQ',
    // },
    // {
    //   country: 'Eritrea',
    //   code2: 'ER',
    //   code3: 'ERI',
    // },
    {
        country: 'Estonia',
        code2: 'EE',
        code3: 'EST',
    },
    // {
    //   country: 'Ethiopia',
    //   code2: 'ET',
    //   code3: 'ETH',
    // },
    // {
    //   country: 'Falkland Islands (the) [Malvinas]',
    //   code2: 'FK',
    //   code3: 'FLK',
    // }, // Part of the UK
    {
        country: 'Faroe Islands (the)',
        code2: 'FO',
        code3: 'FRO',
    },
    {
        country: 'Fiji',
        code2: 'FJ',
        code3: 'FJI',
    },
    {
        country: 'Finland',
        code2: 'FI',
        code3: 'FIN',
    },
    {
        country: 'France',
        code2: 'FR',
        code3: 'FRA',
    },
    {
        country: 'French Guiana',
        code2: 'GF',
        code3: 'GUF',
    },
    {
        country: 'French Polynesia',
        code2: 'PF',
        code3: 'PYF',
    },
    // {
    //   country: 'French Southern Territories (the)',
    //   code2: 'TF',
    //   code3: 'ATF',
    // },
    // {
    //   country: 'Gabon',
    //   code2: 'GA',
    //   code3: 'GAB',
    // },
    {
        country: 'Gambia (the)',
        code2: 'GM',
        code3: 'GMB',
    },
    {
        country: 'Georgia',
        code2: 'GE',
        code3: 'GEO',
    },
    {
        country: 'Germany (after Jan. 2013)',
        code2: 'DE',
        code3: 'DEU',
    },
    {
        country: 'Ghana',
        code2: 'GH',
        code3: 'GHA',
    },
    {
        country: 'Gibraltar',
        code2: 'GI',
        code3: 'GIB',
    }, // Part of the UK
    {
        country: 'Greece',
        code2: 'GR',
        code3: 'GRC',
    },
    {
        country: 'Greenland',
        code2: 'GL',
        code3: 'GRL',
    }, // Part of Denmark
    // {
    //   country: 'Grenada',
    //   code2: 'GD',
    //   code3: 'GRD',
    // },
    {
        country: 'Guadeloupe',
        code2: 'GP',
        code3: 'GLP',
    }, // Part of France
    {
        country: 'Guam',
        code2: 'GU',
        code3: 'GUM',
    },
    {
        country: 'Guatemala',
        code2: 'GT',
        code3: 'GTM',
    },
    {
        country: 'Guernsey',
        code2: 'GG',
        code3: 'GGY',
    }, // Part of the UK
    {
        country: 'Guinea',
        code2: 'GN',
        code3: 'GIN',
    },
    // {
    //   country: 'Guinea-Bissau',
    //   code2: 'GW',
    //   code3: 'GNB',
    // },
    {
        country: 'Guyana',
        code2: 'GY',
        code3: 'GUY',
    },
    {
        country: 'Haiti',
        code2: 'HT',
        code3: 'HTI',
    },
    // {
    //   country: 'Heard Island and McDonald Islands',
    //   code2: 'HM',
    //   code3: 'HMD',
    // },
    // {
    //   country: 'Holy See (the)',
    //   code2: 'VA',
    //   code3: 'VAT',
    // },
    {
        country: 'Honduras',
        code2: 'HN',
        code3: 'HND',
    },
    // {
    //   country: 'Hong Kong',
    //   code2: 'HK',
    //   code3: 'HKG',
    // },
    {
        country: 'Hungary',
        code2: 'HU',
        code3: 'HUN',
    },
    {
        country: 'Iceland',
        code2: 'IS',
        code3: 'ISL',
    },
    {
        country: 'India',
        code2: 'IN',
        code3: 'IND',
    },
    {
        country: 'Indonesia',
        code2: 'ID',
        code3: 'IDN',
    },
    // {
    //   country: 'Iran (Islamic Republic of)',
    //   code2: 'IR',
    //   code3: 'IRN',
    // },
    // {
    //   country: 'Iraq',
    //   code2: 'IQ',
    //   code3: 'IRQ',
    // },
    {
        country: 'Ireland',
        code2: 'IE',
        code3: 'IRL',
    },
    {
        country: 'Isle of Man',
        code2: 'IM',
        code3: 'IMN',
    }, // Part of the UK
    {
        country: 'Israel',
        code2: 'IL',
        code3: 'ISR',
    },
    {
        country: 'Italy',
        code2: 'IT',
        code3: 'ITA',
    },
    {
        country: 'Jamaica',
        code2: 'JM',
        code3: 'JAM',
    },
    // {
    //   country: 'Japan',
    //   code2: 'JP',
    //   code3: 'JPN',
    // },
    {
        country: 'Jersey',
        code2: 'JE',
        code3: 'JEY',
    }, // Part of the UK
    {
        country: 'Jordan',
        code2: 'JO',
        code3: 'JOR',
    },
    {
        country: 'Kazakhstan',
        code2: 'KZ',
        code3: 'KAZ',
    },
    {
        country: 'Kenya',
        code2: 'KE',
        code3: 'KEN',
    },
    {
        country: 'Kiribati',
        code2: 'KI',
        code3: 'KIR',
    },
    // {
    //   country: 'Korea (the Democratic People`s Republic of)',
    //   code2: 'KP',
    //   code3: 'PRK',
    // },
    {
        country: 'Korea (the Republic of)',
        code2: 'KR',
        code3: 'KOR',
    },
    {
        country: 'Kuwait',
        code2: 'KW',
        code3: 'KWT',
    },
    {
        country: 'Kyrgyzstan',
        code2: 'KG',
        code3: 'KGZ',
    },
    // {
    //   country: 'Lao People`s Democratic Republic',
    //   code2: 'LA',
    //   code3: 'LAO',
    // },
    {
        country: 'Latvia',
        code2: 'LV',
        code3: 'LVA',
    },
    {
        country: 'Lebanon',
        code2: 'LB',
        code3: 'LBN',
    },
    // {
    //   country: 'Lesotho',
    //   code2: 'LS',
    //   code3: 'LSO',
    // },
    // {
    //   country: 'Liberia',
    //   code2: 'LR',
    //   code3: 'LBR',
    // },
    // {
    //   country: 'Libya',
    //   code2: 'LY',
    //   code3: 'LBY',
    // },
    {
        country: 'Liechtenstein',
        code2: 'LI',
        code3: 'LIE',
    },
    {
        country: 'Lithuania',
        code2: 'LT',
        code3: 'LTU',
    },
    {
        country: 'Luxembourg',
        code2: 'LU',
        code3: 'LUX',
    },
    // {
    //   country: 'Macao',
    //   code2: 'MO',
    //   code3: 'MAC',
    // },
    // {
    //   country: 'Macedonia (the former Yugoslav Republic of)',
    //   code2: 'MK',
    //   code3: 'MKD',
    // },
    {
        country: 'Madagascar',
        code2: 'MG',
        code3: 'MDG',
    },
    {
        country: 'Malawi',
        code2: 'MW',
        code3: 'MWI',
    },
    {
        country: 'Malaysia',
        code2: 'MY',
        code3: 'MYS',
    },
    {
        country: 'Maldives',
        code2: 'MV',
        code3: 'MDV',
    },
    // {
    //   country: 'Mali',
    //   code2: 'ML',
    //   code3: 'MLI',
    // },
    {
        country: 'Malta',
        code2: 'MT',
        code3: 'MLT',
    },
    // {
    //   country: 'Marshall Islands (the)',
    //   code2: 'MH',
    //   code3: 'MHL',
    // },
    // {
    //   country: 'Martinique',
    //   code2: 'MQ',
    //   code3: 'MTQ',
    // },
    // {
    //   country: 'Mauritania',
    //   code2: 'MR',
    //   code3: 'MRT',
    // },
    // {
    //   country: 'Mauritius',
    //   code2: 'MU',
    //   code3: 'MUS',
    // },
    // {
    //   country: 'Mayotte',
    //   code2: 'YT',
    //   code3: 'MYT',
    // },
    {
        country: 'Mexico',
        code2: 'MX',
        code3: 'MEX',
    },
    // {
    //   country: 'Micronesia (Federated States of)',
    //   code2: 'FM',
    //   code3: 'FSM',
    // },
    {
        country: 'Moldova (the Republic of)',
        code2: 'MD',
        code3: 'MDA',
    },
    {
        country: 'Monaco',
        code2: 'MC',
        code3: 'MCO',
    },
    {
        country: 'Mongolia',
        code2: 'MN',
        code3: 'MNG',
    },
    {
        country: 'Montenegro',
        code2: 'ME',
        code3: 'MNE',
    },
    // {
    //   country: 'Montserrat',
    //   code2: 'MS',
    //   code3: 'MSR',
    // }, // Part of the UK
    {
        country: 'Morocco',
        code2: 'MA',
        code3: 'MAR',
    },
    {
        country: 'Mozambique',
        code2: 'MZ',
        code3: 'MOZ',
    },
    {
        country: 'Myanmar',
        code2: 'MM',
        code3: 'MMR',
    },
    {
        country: 'Namibia',
        code2: 'NA',
        code3: 'NAM',
    },
    // {
    //   country: 'Nauru',
    //   code2: 'NR',
    //   code3: 'NRU',
    // },
    {
        country: 'Nepal',
        code2: 'NP',
        code3: 'NPL',
    },
    {
        country: 'Netherlands (the)',
        code2: 'NL',
        code3: 'NLD',
    },
    // {
    //   country: 'New Caledonia',
    //   code2: 'NC',
    //   code3: 'NCL',
    // },
    {
        country: 'New Zealand',
        code2: 'NZ',
        code3: 'NZL',
    },
    {
        country: 'Nicaragua',
        code2: 'NI',
        code3: 'NIC',
    },
    // {
    //   country: 'Niger (the)',
    //   code2: 'NE',
    //   code3: 'NER',
    // },
    {
        country: 'Nigeria',
        code2: 'NG',
        code3: 'NGA',
    },
    // {
    //   country: 'Niue',
    //   code2: 'NU',
    //   code3: 'NIU',
    // },
    {
        country: 'Norfolk Island',
        code2: 'NF',
        code3: 'NFK',
    }, // Part of Australia
    {
        country: 'Northern Mariana Islands (the)',
        code2: 'MP',
        code3: 'MNP',
    },
    {
        country: 'Norway',
        code2: 'NO',
        code3: 'NOR',
    },
    {
        country: 'Oman',
        code2: 'OM',
        code3: 'OMN',
    },
    {
        country: 'Pakistan',
        code2: 'PK',
        code3: 'PAK',
    },
    // {
    //   country: 'Palau',
    //   code2: 'PW',
    //   code3: 'PLW',
    // },
    {
        country: 'Palestine, State of',
        code2: 'PS',
        code3: 'PSE',
    },
    {
        country: 'Panama',
        code2: 'PA',
        code3: 'PAN',
    },
    {
        country: 'Papua New Guinea',
        code2: 'PG',
        code3: 'PNG',
    },
    {
        country: 'Paraguay',
        code2: 'PY',
        code3: 'PRY',
    },
    {
        country: 'Peru',
        code2: 'PE',
        code3: 'PER',
    },
    {
        country: 'Philippines (the)',
        code2: 'PH',
        code3: 'PHL',
    },
    // {
    //   country: 'Pitcairn',
    //   code2: 'PN',
    //   code3: 'PCN',
    // },
    {
        country: 'Poland',
        code2: 'PL',
        code3: 'POL',
    },
    {
        country: 'Portugal',
        code2: 'PT',
        code3: 'PRT',
    },
    {
        country: 'Puerto Rico',
        code2: 'PR',
        code3: 'PRI',
    },
    {
        country: 'Qatar',
        code2: 'QA',
        code3: 'QAT',
    },
    // {
    //   country: 'Réunion',
    //   code2: 'RE',
    //   code3: 'REU',
    // },
    {
        country: 'Romania',
        code2: 'RO',
        code3: 'ROU',
    },
    {
        country: 'Russian Federation (the)',
        code2: 'RU',
        code3: 'RUS',
    },
    {
        country: 'Rwanda',
        code2: 'RW',
        code3: 'RWA',
    },
    {
        country: 'Saint Barthélemy',
        code2: 'BL',
        code3: 'BLM',
    },
    // {
    //   country: 'Saint Helena, Ascension and Tristan da Cunha',
    //   code2: 'SH',
    //   code3: 'SHN',
    // }, // Part of the UK
    // {
    //   country: 'Saint Kitts and Nevis',
    //   code2: 'KN',
    //   code3: 'KNA',
    // },
    {
        country: 'Saint Lucia',
        code2: 'LC',
        code3: 'LCA',
    },
    {
        country: 'Saint Martin (French part)',
        code2: 'MF',
        code3: 'MAF',
    }, // Part of France
    {
        country: 'Saint Pierre and Miquelon',
        code2: 'PM',
        code3: 'SPM',
    }, // Part of France
    // {
    //   country: 'Saint Vincent and the Grenadines',
    //   code2: 'VC',
    //   code3: 'VCT',
    // },
    // {
    //   country: 'Samoa',
    //   code2: 'WS',
    //   code3: 'WSM',
    // },
    {
        country: 'San Marino',
        code2: 'SM',
        code3: 'SMR',
    },
    // {
    //   country: 'Sao Tome and Principe',
    //   code2: 'ST',
    //   code3: 'STP',
    // },
    {
        country: 'Saudi Arabia',
        code2: 'SA',
        code3: 'SAU',
    },
    {
        country: 'Senegal',
        code2: 'SN',
        code3: 'SEN',
    },
    {
        country: 'Serbia',
        code2: 'RS',
        code3: 'SRB',
    },
    // {
    //   country: 'Seychelles',
    //   code2: 'SC',
    //   code3: 'SYC',
    // },
    {
        country: 'Sierra Leone',
        code2: 'SL',
        code3: 'SLE',
    },
    {
        country: 'Singapore',
        code2: 'SG',
        code3: 'SGP',
    },
    {
        country: 'Sint Maarten (Dutch part)',
        code2: 'SX',
        code3: 'SXM',
    }, // Part of the Netherlands
    {
        country: 'Slovakia',
        code2: 'SK',
        code3: 'SVK',
    },
    {
        country: 'Slovenia',
        code2: 'SI',
        code3: 'SVN',
    },
    // {
    //   country: 'Solomon Islands',
    //   code2: 'SB',
    //   code3: 'SLB',
    // },
    // {
    //   country: 'Somalia',
    //   code2: 'SO',
    //   code3: 'SOM',
    // },
    {
        country: 'South Africa',
        code2: 'ZA',
        code3: 'ZAF',
    },
    // {
    //   country: 'South Georgia and the South Sandwich Islands',
    //   code2: 'GS',
    //   code3: 'SGS',
    // },
    // {
    //   country: 'South Sudan',
    //   code2: 'SS',
    //   code3: 'SSD',
    // },
    {
        country: 'Spain',
        code2: 'ES',
        code3: 'ESP',
    },
    {
        country: 'Sri Lanka',
        code2: 'LK',
        code3: 'LKA',
    },
    // {
    //   country: 'Sudan (the)',
    //   code2: 'SD',
    //   code3: 'SDN',
    // },
    {
        country: 'Suriname',
        code2: 'SR',
        code3: 'SUR',
    },
    // {
    //   country: 'Svalbard and Jan Mayen',
    //   code2: 'SJ',
    //   code3: 'SJM',
    // },
    // {
    //   country: 'Swaziland',
    //   code2: 'SZ',
    //   code3: 'SWZ',
    // },
    {
        country: 'Sweden',
        code2: 'SE',
        code3: 'SWE',
    },
    {
        country: 'Switzerland',
        code2: 'CH',
        code3: 'CHE',
    },
    // {
    //   country: 'Syrian Arab Republic',
    //   code2: 'SY',
    //   code3: 'SYR',
    // },
    // {
    //   country: 'Taiwan (Province of China)',
    //   code2: 'TW',
    //   code3: 'TWN',
    // },
    {
        country: 'Tajikistan',
        code2: 'TJ',
        code3: 'TJK',
    },
    {
        country: 'Tanzania, United Republic of',
        code2: 'TZ',
        code3: 'TZA',
    },
    {
        country: 'Thailand',
        code2: 'TH',
        code3: 'THA',
    },
    // {
    //   country: 'Timor-Leste',
    //   code2: 'TL',
    //   code3: 'TLS',
    // },
    {
        country: 'Togo',
        code2: 'TG',
        code3: 'TGO',
    },
    // {
    //   country: 'Tokelau',
    //   code2: 'TK',
    //   code3: 'TKL',
    // },
    {
        country: 'Tonga',
        code2: 'TO',
        code3: 'TON',
    },
    {
        country: 'Trinidad and Tobago',
        code2: 'TT',
        code3: 'TTO',
    },
    {
        country: 'Tunisia',
        code2: 'TN',
        code3: 'TUN',
    },
    {
        country: 'Turkey',
        code2: 'TR',
        code3: 'TUR',
    },
    {
        country: 'Turkmenistan',
        code2: 'TM',
        code3: 'TKM',
    },
    // {
    //   country: 'Turks and Caicos Islands (the)',
    //   code2: 'TC',
    //   code3: 'TCA',
    // }, // Part of the UK
    // {
    //   country: 'Tuvalu',
    //   code2: 'TV',
    //   code3: 'TUV',
    // },
    {
        country: 'Uganda',
        code2: 'UG',
        code3: 'UGA',
    },
    {
        country: 'Ukraine',
        code2: 'UA',
        code3: 'UKR',
    },
    {
        country: 'United Arab Emirates (the)',
        code2: 'AE',
        code3: 'ARE',
    },
    {
        country: 'United Kingdom of Great Britain and Northern Ireland (the)',
        code2: 'GB',
        code3: 'GBR',
    },
    // {
    //   country: 'United States Minor Outlying Islands (the)',
    //   code2: 'UM',
    //   code3: 'UMI',
    // },
    {
        country: 'United States of America (the)',
        code2: 'US',
        code3: 'USA',
    },
    {
        country: 'Uruguay',
        code2: 'UY',
        code3: 'URY',
    },
    {
        country: 'Uzbekistan',
        code2: 'UZ',
        code3: 'UZB',
    },
    {
        country: 'Vanuatu',
        code2: 'VU',
        code3: 'VUT',
    },
    {
        country: 'Venezuela (Bolivarian Republic of)',
        code2: 'VE',
        code3: 'VEN',
    },
    {
        country: 'Viet Nam',
        code2: 'VN',
        code3: 'VNM',
    },
    {
        country: 'Virgin Islands (British)',
        code2: 'VG',
        code3: 'VGB',
    }, // Part of the UK
    {
        country: 'Virgin Islands (U.S.)',
        code2: 'VI',
        code3: 'VIR',
    },
    // {
    //   country: 'Wallis and Futuna',
    //   code2: 'WF',
    //   code3: 'WLF',
    // }, // Part of France
    // {
    //   country: 'Western Sahara',
    //   code2: 'EH',
    //   code3: 'ESH',
    // },
    {
        country: 'Yemen',
        code2: 'YE',
        code3: 'YEM',
    },
    {
        country: 'Zambia',
        code2: 'ZM',
        code3: 'ZMB',
    }
    // {
    //   country: 'Zimbabwe',
    //   code2: 'ZW',
    //   code3: 'ZWE',
    // }
];

// Number of countries supported for DRIVERS_LICENSE = 173
