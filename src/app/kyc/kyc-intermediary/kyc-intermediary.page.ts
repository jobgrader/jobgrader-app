import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { KycProvider } from '../enums/kyc-provider.enum';
import { KycService } from '../services/kyc.service';
import { KycState } from '../enums/kyc-state.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { AlertsProviderService } from 'src/app/core/providers/alerts/alerts-provider.service';
import { NavController } from '@ionic/angular';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';

@Component({
  selector: 'app-kyc-intermediary',
  templateUrl: './kyc-intermediary.page.html',
  styleUrls: ['./kyc-intermediary.page.scss'],
})
export class KycIntermediaryPage implements OnInit {

  userImage;
  activateButton = false;

  constructor(
    private kycService: KycService,
    private nav: NavController,
    private translateService: TranslateProviderService,
    private alertService: AlertsProviderService,
    private secureStorage: SecureStorageService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    public _Theme: ThemeSwitcherService
  ) { }

  async ngOnInit() {
    let photo = this.userPhotoServiceAkita.getPhoto();
    if(photo) {
      this.userImage = photo;
    }
    // await this.initiateOndato();
    this.activateButton = true;
  }

  public async initiateOndato(): Promise<void> {

    const provider = environment.verificationProd ? KycProvider.ONDATO_PROD : KycProvider.ONDATO_TEST;
    await this.kycService.addInitialState(provider);

    if (this.kycService.getState(provider) === KycState.VC_RETRIEVED) {
      await this.alreadyRetrieved(provider);
    } else {
      this.kycService.patchState(provider, KycState.KYC_WAITING);
      await this.kycService.setState(provider, KycState.KYC_INITIATED, undefined, {});
    }

  }

  private async alreadyRetrieved(provider: KycProvider): Promise<void> {
    await this.alertService.alertCreate(
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.header`),
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.sub-header`),
        this.translateService.instant(`KYC.messages.${provider}_ALREADY_RETRIEVED.message`),
        [
          {
            text: this.translateService.instant('GENERAL.ok'),
            handler: () => {}
          }]
    );
  }

  goBack() {
    this.nav.navigateBack('/kyc');
  }

  next() {
    this.initiateOndato();
  }

  cancel() {
    this.nav.navigateForward('/dashboard/tab-home');
  }

}
