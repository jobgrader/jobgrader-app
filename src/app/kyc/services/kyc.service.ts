import { Injectable, OnDestroy } from '@angular/core';
import { KycStatus } from '../interfaces/kyc-status.interface';
import { SecureStorageService } from '../../core/providers/secure-storage/secure-storage.service';
import { KycProvider } from '../enums/kyc-provider.enum';
import { KycState } from '../enums/kyc-state.enum';
import { SecureStorageKey } from '../../core/providers/secure-storage/secure-storage-key.enum';
import { LoaderProviderService } from '../../core/providers/loader/loader-provider.service';
import {
    ApiProviderService,
    AuthadaInitResponse,
    VerifeyeResponse,
    VeriffInitResponse,
    VeriffPrepareBody,
    VerificationCheckResponse
} from '../../core/providers/api/api-provider.service';
import { ToastController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { AppStateService } from '../../core/providers/app-state/app-state.service';
import { AlertsProviderService } from '../../core/providers/alerts/alerts-provider.service';
import { from } from 'rxjs';
import { take } from 'rxjs/operators';
import { ModalController, Platform, NavController } from '@ionic/angular';
import { AppConfig } from '../../app.config';
import { TranslateProviderService } from '../../core/providers/translate/translate-provider.service';
import { Router } from '@angular/router';
import { UtilityService } from '../../core/providers/utillity/utility.service';
import { CryptoProviderService } from '../../core/providers/crypto/crypto-provider.service';
import { VeriffStatusCodes } from '../enums/veriff-status-codes.enum';
// import { VeriffThanksPage } from '../../veriff-thanks/veriff-thanks.page';
import { differenceWith, isEqual } from 'lodash-es';
import { KycmediaService } from "../../core/providers/kycmedia/kycmedia.service";
import { Settings, SettingsService } from '../../core/providers/settings/settings.service';
// import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { ThemeSwitcherService } from '../../core/providers/theme/theme-switcher.service';
import { EventsList, EventsService } from 'src/app/core/providers/events/events.service';
import { Browser } from '@capacitor/browser';
// import { LockService } from 'src/app/lock-screen/lock.service';

@Injectable({
    providedIn: 'root'
})
export class KycService implements OnDestroy {

    private static errors = {
        UNALLOWED_STATE_CHANGE: (from, to) => `You made an unallowed state change. Change from ${from} to ${to} is forbidden`,
        PROVIDER_NOT_FOUND: `Your requested provider was not found`,
        ERROR_STORING_KYC_TO_SECURE_STORE: 'There was an error storing the kyc status to secure storage'
    };

    private browser: InAppBrowserObject;

    private kycStatus: Array<KycStatus> = [];

    private readonly appConfig: any = AppConfig;

    private browserOptions;

    constructor(
        private nav: NavController,
        private secureStorage: SecureStorageService,
        private loaderProviderService: LoaderProviderService,
        private apiProviderService: ApiProviderService,
        private appStateService: AppStateService,
        private alertService: AlertsProviderService,
        private translate: TranslateProviderService,
        private iab: InAppBrowser,
        private router: Router,
        private platform: Platform,
        private kycmedia: KycmediaService,
        private cryptoService: CryptoProviderService,
        private settingService: SettingsService,
        private _ToastController: ToastController,
        private _EventService: EventsService,
        // private safariViewController: SafariViewController,
        // private lockService: LockService,
        private themeSwitcher: ThemeSwitcherService,
        private modalCtrl: ModalController) {
        this.init();
    }

    public ngOnDestroy(): void {
    }

    /** Setting the corresponding change for a kyc provider */
    public async setState(provider: KycProvider, state: KycState, code?: string, additionalInformation?: VeriffPrepareBody): Promise<void> {
        const status = this.kycStatus.find(k => k.kycProvider === provider);
        const index = this.kycStatus.indexOf(status);
        // let timestamp = +new Date();
        if ( !status ) {
            throw new Error(KycService.errors.PROVIDER_NOT_FOUND);
        }
        switch ( status.kycState ) {
            case KycState.KYC_WAITING:
                if ( state !== KycState.KYC_INITIATED ) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                switch ( provider ) {
                    case KycProvider.VERIFF_PROD:
                    case KycProvider.VERIFF_TEST:
                    case KycProvider.VERIFF_PROD_IDCARD:
                    case KycProvider.VERIFF_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFF_PROD_PASSPORT:
                    case KycProvider.VERIFF_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFF_TEST_IDCARD:
                    case KycProvider.VERIFF_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFF_TEST_PASSPORT:
                    case KycProvider.VERIFF_TEST_RESIDENCEPERMIT:
                        const veriffSessionResponse = await this.initVeriff(additionalInformation);
                        if ( !veriffSessionResponse ) {
                            return;
                        } else {
                            this.kycStatus[index].additionalInfo = {
                                veriffSessionId: veriffSessionResponse.veriffSessionId
                            };
                            this.kycStatus[index].kycInitiatedTimeStamp = veriffSessionResponse.created;
                        }
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                    case KycProvider.VERIFEYE_PROD:
                    case KycProvider.VERIFEYE_TEST:
                    case KycProvider.VERIFEYE_PROD_IDCARD:
                    case KycProvider.VERIFEYE_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_PROD_PASSPORT:
                    case KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFEYE_TEST_IDCARD:
                    case KycProvider.VERIFEYE_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_TEST_PASSPORT:
                    case KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT:
                        const verifeyeResponse = await this.initVerifeye(code);
                        if ( !verifeyeResponse ) {
                            return;
                        } else {
                            this.kycStatus[index].additionalInfo = {
                                verifeyeSessionId: verifeyeResponse.sessionId
                            };
                            this.kycStatus[index].kycInitiatedTimeStamp = verifeyeResponse.created;
                        }
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                    case KycProvider.AUTHADA_PROD:
                    case KycProvider.AUTHADA_TEST:
                        await this.initAuthada(index);
                        break;
                    case KycProvider.ONDATO_TEST:
                    case KycProvider.ONDATO_PROD:
                        const ondatoSessionResponse = await this.initOndato();
                        if ( !ondatoSessionResponse ) {
                            return;
                        } else {
                            this.kycStatus[index].additionalInfo = {
                                ondatoSessionId: ondatoSessionResponse.sessionId
                            };
                            this.kycStatus[index].kycInitiatedTimeStamp = ondatoSessionResponse.created;
                        }
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                }

                break;
            case KycState.KYC_INITIATED:
                if ( state !== KycState.KYC_CHECK) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                UtilityService.setTimeout(600).subscribe(async _ => {
                    // const modal: HTMLIonModalElement = await this.modalCtrl.create({
                    //     component: VeriffThanksPage,
                    //     backdropDismiss: false,
                    //     keyboardClose: true
                    // });
                    // await modal.present();
                    // Trigger next state hook directly
                    await this.setState(provider, KycState.KYC_CHECK);
                });
                break;
            case KycState.KYC_CHECK:
                if ( state !== KycState.KYC_CHECK && state !== KycState.KYC_FAILED && state !== KycState.KYC_SUCCESSFUL ) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                switch ( provider ) {
                    case KycProvider.VERIFF_PROD:
                    case KycProvider.VERIFF_TEST:
                    case KycProvider.VERIFF_PROD_IDCARD:
                    case KycProvider.VERIFF_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFF_PROD_PASSPORT:
                    case KycProvider.VERIFF_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFF_TEST_IDCARD:
                    case KycProvider.VERIFF_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFF_TEST_PASSPORT:
                    case KycProvider.VERIFF_TEST_RESIDENCEPERMIT:
                        await this.checkVeriff(provider, status);
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                    case KycProvider.VERIFEYE_PROD:
                    case KycProvider.VERIFEYE_TEST:
                    case KycProvider.VERIFEYE_PROD_IDCARD:
                    case KycProvider.VERIFEYE_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_PROD_PASSPORT:
                    case KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFEYE_TEST_IDCARD:
                    case KycProvider.VERIFEYE_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_TEST_PASSPORT:
                    case KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT:
                        await this.checkVerifeye(provider, status);
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                    case KycProvider.AUTHADA_PROD:
                    case KycProvider.AUTHADA_TEST:
                        await this.checkAuthada(provider, status);
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                    case KycProvider.ONDATO_TEST:
                    case KycProvider.ONDATO_PROD:
                        await this.checkOndato(provider, status);
                        await this.apiProviderService.postKycStatus({
                            stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                        }).then(async timestamp => {
                            await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                        });
                        break;
                }
                break;
            case KycState.KYC_SUCCESSFUL:
                if ( state !== KycState.DID_RETRIEVE ) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                await this.settingService.set(Settings.userIsAllowedToAccessChatMarketplace, true.toString(), true);
                switch ( provider ) {
                    case KycProvider.VERIFEYE_PROD_IDCARD:
                    case KycProvider.VERIFEYE_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_PROD_PASSPORT:
                    case KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFEYE_TEST_IDCARD:
                    case KycProvider.VERIFEYE_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFEYE_TEST_PASSPORT:
                    case KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT:
                    case KycProvider.VERIFF_PROD:
                    case KycProvider.VERIFF_TEST:
                    case KycProvider.VERIFF_PROD_IDCARD:
                    case KycProvider.VERIFF_PROD_DRIVINGLICENCE:
                    case KycProvider.VERIFF_PROD_PASSPORT:
                    case KycProvider.VERIFF_PROD_RESIDENCEPERMIT:
                    case KycProvider.VERIFF_TEST_IDCARD:
                    case KycProvider.VERIFF_TEST_DRIVINGLICENCE:
                    case KycProvider.VERIFF_TEST_PASSPORT:
                    case KycProvider.VERIFF_TEST_RESIDENCEPERMIT:
                    case KycProvider.AUTHADA_PROD:
                    case KycProvider.AUTHADA_TEST:
                        break;
                    case KycProvider.ONDATO_TEST:
                        var pair = await this.kycmedia.returnProviderDocumentPair();
                        if(this.kycStatus[index].additionalInfo.ondatoSessionId) {
                            this.changeKycProvider(KycProvider.ONDATO_TEST, pair[this.kycStatus[index].additionalInfo.ondatoSessionId])
                        }
                        break;
                    case KycProvider.ONDATO_PROD:
                        var pair = await this.kycmedia.returnProviderDocumentPair();
                        if(this.kycStatus[index].additionalInfo.ondatoSessionId) {
                            this.changeKycProvider(KycProvider.ONDATO_PROD, pair[this.kycStatus[index].additionalInfo.ondatoSessionId])
                        }
                        break;
                }
                await this.alertService.alertCreate(
                    this.translate.instant(`KYC.messages.${provider}_SUCCESS.header`),
                    '', //state, // ToDo: Ansik: Remove this after testing
                    // this.translate.instant(`KYC.messages.${provider}_SUCCESS.sub-header`),
                    this.translate.instant(`KYC.messages.${provider}_SUCCESS.message${code ? '-' + code.toLowerCase() : ''}`),
                    [{
                        text: this.translate.instant('GENERAL.ok'),
                        handler: async () => {
                            this.kycStatus[index].kycState = KycState.DID_RETRIEVE;
                            // try {
                            //     if (document.getElementsByClassName('veriff-thanks-content').length) {
                            //         this.modalCtrl.dismiss();
                            //     }
                            // } catch (e) {
                            //     console.log(e);
                            // }
                            // // UtilityService.setTimeout(1000).subscribe(_ => {
                            // try {
                            //     this.nav.navigateBack('/dashboard/tab-home');
                            // } catch(e) {
                            //     console.log(e);
                            // }
                            // });
                            try {
                                this.setState(provider, KycState.KYC_WAITING);
                            } catch(e) {
                                console.log(e);
                            }
                        }
                    }]
                );
                break;
            case KycState.KYC_FAILED:
                if ( state !== KycState.KYC_WAITING ) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                delete this.kycStatus[index].kycInitiatedTimeStamp;
                delete this.kycStatus[index].additionalInfo;
                await this.alertService.alertCreate(
                    this.translate.instant(`KYC.messages.${provider}_FAILED.header`),
                    '',
                    // this.translate.instant(`KYC.messages.${provider}_FAILED.sub-header`),
                    this.translate.instant(`KYC.messages.${provider}_FAILED.message${code ? '-' + code.toLowerCase() : ''}`),
                    [{
                        text: this.translate.instant('GENERAL.ok'),
                        handler: () => {
                        }
                    }]
                );
                break;
            case KycState.DID_RETRIEVE:
                if ( state !== KycState.KYC_WAITING ) {
                    throw new Error(KycService.errors.UNALLOWED_STATE_CHANGE(status.kycState, state));
                }
                await this.settingService.set(Settings.userIsAllowedToAccessChatMarketplace, true.toString(), true);
                try {
                    // await this.apiProviderService.postKycStatus({
                    //     stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                    // }).then(async timestamp => {
                    //     await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                    // });
                    await this.retrieveVc();
                    await this.alertService.alertCreate(
                        this.translate.instant(`KYC.messages.${provider}_RETRIEVE.header`),
                        // this.translate.instant(`KYC.messages.${provider}_RETRIEVE.sub-header`),
                        '',
                        this.translate.instant(`KYC.messages.${provider}_RETRIEVE.message`),
                        [{
                            text: this.translate.instant('GENERAL.ok'),
                            handler: () => {
                            }
                        }]
                    );
                    this.kycStatus[index].kycState = KycState.VC_RETRIEVED;
                    return;
                } catch (e) {
                    console.error(e);
                    UtilityService.setTimeout(5000).subscribe(_ => this.setState(provider, KycState.KYC_WAITING));
                    return;
                }
        }
        this.kycStatus[index].kycState = state;
        await this.setKycStatusToSecureStorage();
    }

    private iabCreate(link: string) {
        if (this.platform.is('ios') && this.platform.is('hybrid')) {
        //   this.showSafariInstance(link);
            Browser.open({ url: link, toolbarColor: '#54BF7B', presentationStyle: 'popover' });
            Browser.addListener('browserPageLoaded', () => {
                this.nav.navigateForward('/veriff-thanks');
            });
            Browser.addListener('browserFinished', () => {
                Browser.removeAllListeners();
                this._EventService.publish(EventsList.dataReceivedByPushNotification);
                // this.lockService.addResume();
            });
        } else if (this.platform.is('android') && this.platform.is('hybrid')) {

            // this.lockService.removeResume();
            // Browser.open({ url: link, toolbarColor: '#54BF7B' });

            // Browser.addListener('browserPageLoaded', () => {
                // this.nav.navigateForward('/veriff-thanks');
            // });
            Browser.addListener('browserFinished', () => {
                Browser.removeAllListeners();
                this.nav.navigateForward('/veriff-thanks');
                this._EventService.publish(EventsList.dataReceivedByPushNotification);
                // this.lockService.addResume();
            });

            Browser.open({ url: link, toolbarColor: '#54BF7B' });

        } else if (!this.platform.is('hybrid')) {

            this.browser = this.iab.create(link, '_system');
            this.nav.navigateForward('/veriff-thanks');
            this._EventService.publish(EventsList.dataReceivedByPushNotification);

        }
      }


      /* mburger: we use the new capacitor browser so we don't need the SafariViewController anymore
      showSafariInstance(url: string) {
        this.safariViewController.isAvailable().then((available: boolean) => {
              if (available) {
                this.safariViewController.show({
                  url,
                  hidden: false,
                  animated: true,
                  transition: 'slide',
                  enterReaderModeIfAvailable: false,
                  tintColor: '#54BF7B'
                }).subscribe((result: any) => {
                    if(result.event === 'closed') {
                        console.log('Closed');
                        this._EventService.publish(EventsList.dataReceivedByPushNotification);
                        this.nav.navigateForward('/veriff-thanks');
                        // this.apiProviderService.getUserImageAndData(true, true).then(({ photo, userData, userTrust}) => {
                        //     this.secureStorage.setValue(SecureStorageKey.userData, JSON.stringify(userData)).then(() => {
                        //         this.secureStorage.setValue(SecureStorageKey.userTrustData, JSON.stringify(userTrust)).then(() => {
                        //         })
                        //     })
                        // })
                    };
                },
                  (error: any) => console.error(error) );
              } else {
                this.browser = this.iab.create(url, '_self', this.browserOptions);
              }
            }
          );
      }
    */

    /** Getter for the state of a specific kyc provider */
    public getState(provider: KycProvider): KycState {
        if(this.kycStatus == null) { this.kycStatus = [] }
        const status = this.kycStatus.find(k => k.kycProvider === provider);
        return status ? status.kycState : undefined;
    }

    public getKycInitiatedTimestamp(provider: KycProvider) {
        if(this.kycStatus == null) { this.kycStatus = [] }
        const status = this.kycStatus.find(k => k.kycProvider === provider);
        return status ? status.kycInitiatedTimeStamp : undefined;
    }

    /** Callback for adding a kyc provider with initial state */
    public async addInitialState(provider: KycProvider): Promise<void> {
        if (!this.getState(provider)) {
            this.kycStatus.push({
                kycProvider: provider,
                kycInitiatedTimeStamp: undefined,
                kycState: KycState.KYC_WAITING,
                additionalInfo: undefined
            });
            await this.setKycStatusToSecureStorage();
        }
    }

    /** Patchingfor the state of a specific kyc provider */
    public patchState(provider: KycProvider, state: KycState): void {
        const index = this.kycStatus.findIndex(k => k.kycProvider === provider);
        this.kycStatus[index].kycState = state;
    }

    /** Syncs Kyc status with MW */
    public syncKyc(): void {
        this.secureStorage.getValue(SecureStorageKey.kycStatusTimestamp, false).then(kycStatusTimestamp => {
            if( !!kycStatusTimestamp ) {
                this.apiProviderService.getKycStatus(kycStatusTimestamp).then(async res => {
                    if(!!res){
                        this.kycStatus = JSON.parse(decodeURIComponent(res.stringifiedKycStatus));
                        await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, res.timestamp.toString())
                        await this.setKycStatusToSecureStorage();
                        await this.kycmedia.init();
                    }
                })
            }
            else {
                this.apiProviderService.getKycStatus().then(async res => {
                if(!!res){
                    this.kycStatus = JSON.parse(decodeURIComponent(res.stringifiedKycStatus));
                    await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, res.timestamp.toString());
                    await this.setKycStatusToSecureStorage();
                    await this.kycmedia.init();
                    }
                })
            }
        });


    }

    /** Checks wheter or not a kyc was initiated */
    public checkForActions(): void {
        Object.keys(KycProvider).forEach(async provider => {
            if (this.kycStatus == null) { this.kycStatus = []}
            const status = this.kycStatus.find(k => k.kycProvider === provider);
            if (!status) {
                return;
            }
            if ( status.kycState === KycState.KYC_INITIATED || status.kycState === KycState.KYC_CHECK ) {
                await this.setState(KycProvider[provider], KycState.KYC_CHECK);
                return;
            }
        });
    }

    /** Reset the kyc status on the device */
    public async reset(): Promise<void> {
        this.kycStatus = Object.keys(KycProvider).map(kycProvider => {
            return {
                kycProvider,
                kycInitiatedTimeStamp: null,
                kycState: KycState.KYC_WAITING
            };
        }) as any;
        await this.setKycStatusToSecureStorage();
    }

    private init(): void {
        this.secureStorage.getValue(SecureStorageKey.kycStatus, false).then(async kycStatus => {
            this.kycStatus = kycStatus ? JSON.parse(kycStatus) : this.kycStatus;
        });
        this.browserOptions = 'footer=yes,hideurlbar=no,hidenavigationbuttons=no,presentationstyle=pagesheet,location=yes';

        let currentTheme = this.themeSwitcher.getCurrentTheme();

        if(currentTheme === 'dark'){
        this.browserOptions = this.browserOptions
            + 'footercolor=#54BF7B'
            + 'navigationbuttoncolor=#d1d2d1'
            + 'toolbarcolor=#54BF7B'
            + 'closebuttoncolor=#d1d2d1';
        }
        else {
        this.browserOptions = this.browserOptions
        + 'footercolor=#54BF7B'
        + 'navigationbuttoncolor=#000000'
        + 'toolbarcolor=#f7f7f7'
        + 'closebuttoncolor=#000000';
        }
    }

    private async setKycStatusToSecureStorage(): Promise<void> {
        try {
            await this.secureStorage.setValue(SecureStorageKey.kycStatus, JSON.stringify(this.kycStatus));
        } catch (e) {
            throw new Error(KycService.errors.ERROR_STORING_KYC_TO_SECURE_STORE);
        }
    }

    private initOndato(): Promise<any> {
        return new Promise(async (resolve, reject) => {
            await this.loaderProviderService.loaderCreate(this.translate.instant('KYC.KYCLOADER_ONDATO_1'));
            from(this.apiProviderService.generateOndatoSession())
                .pipe(take(1))
                .subscribe(async (ondatoSessionResponse: VeriffInitResponse) => {
                // .subscribe(async (ondatoSessionResponse ) => {
                    await this.loaderProviderService.loaderDismiss();
                    if ( ondatoSessionResponse ) {
                        resolve(ondatoSessionResponse);
                        this.iabCreate(ondatoSessionResponse.url);
                        // this.browser = this.iab.create(ondatoSessionResponse.url, '_system');
                    } else {
                        resolve(undefined);
                        await this.alertService.alertCreate('', '', this.translate.instant('LOADER.somethingWrong'), ['Ok']);
                    }
                }, this.handleVerificationErrorResponse.bind(this));
        })
    }

    private initVeriff(body: VeriffPrepareBody): Promise<VeriffInitResponse> {
        return new Promise(async resolve => {
            await this.loaderProviderService.loaderCreate(this.translate.instant('LOADER.pleaseWait') + " " + this.translate.instant('KYC.KYCLOADER_VERIFF'));
            from(this.apiProviderService.prepareVeriffSession(body))
                .pipe(take(1))
                .subscribe(async (veriffSessionResponse: VeriffInitResponse) => {
                    await this.loaderProviderService.loaderDismiss();
                    if ( veriffSessionResponse ) {
                        resolve(veriffSessionResponse);
                        // this.iabCreate(veriffSessionResponse.url);
                        this.browser = this.iab.create(veriffSessionResponse.url, '_system');
                    } else {
                        resolve(undefined);
                        await this.alertService.alertCreate('', '', this.translate.instant('LOADER.somethingWrong'), ['Ok']);
                    }
                }, this.handleVerificationErrorResponse.bind(this));
        });
    }

    private async initVerifeye(code: string): Promise<VerifeyeResponse> {
        return new Promise(async resolve => {
            await this.loaderProviderService.loaderCreate(this.translate.instant('LOADER.pleaseWait') + " " + this.translate.instant('KYC.KYCLOADER_VERIFEYE'));
            from(this.apiProviderService.getVerifeyeSession(code))
                .pipe(take(1))
                .subscribe(async (verifeyeResponse: VerifeyeResponse) => {
                    await this.loaderProviderService.loaderDismiss();
                    if ( verifeyeResponse ) {
                        resolve(verifeyeResponse);
                        this.browser = this.iab.create(verifeyeResponse.url, '_system');
                    } else {
                        resolve(undefined);
                        await this.alertService.alertCreate('', '', this.translate.instant('LOADER.somethingWrong'), ['Ok']);
                    }
                })
        })
    }

    private async initAuthada(index: any) {
        const authada = {
            market: { android: { production: 'market://details?id=' },
                ios: { production: `https://apps.apple.com/de/app/authada/id1479685801` }
            },
            start: { android: { production: `deeplink://authada.de/code?start=` },
                web: { staging: 'https://demo.staging.authada.de/eservice/v3/web/?ident' }
            }
        };

        const appName = this.platform.is('ios') ? `${this.appConfig.authadaiOS}` : this.platform.is('android') ? `${this.appConfig.authadaAndroid}` : undefined;
        // console.log(appName)
        await this.alertService.alertCreate(this.translate.instant('KYC.AUTHADA'),
        undefined,  this.translate.instant('APPSTORE.message'),
        [
            {
                text: this.translate.instant('APPSTORE.open'),
                handler: async () => {
                    const link = document.createElement('a');
                    document.body.appendChild(link);
                    link.setAttribute('style', 'display: none');
                    const response = await this.apiProviderService.authadaInit();
                    if ( !response ) {
                        return;
                    }
                    const TOKEN_VALUE = response.mobileToken;
                    // console.log('Token Value: ' + TOKEN_VALUE);
                    // console.log('Response from Authada Init: ' + JSON.stringify(response));


                    this.kycStatus[index].kycInitiatedTimeStamp = response.timestamp;
                        this.kycStatus[index].additionalInfo = {
                            sessionToken: response.sessionToken,
                            mobileToken: response.mobileToken
                        };
                    // console.log(this.kycStatus[index]);
                    const timestamp = await this.apiProviderService.postKycStatus({
                        stringifiedKycStatus: encodeURIComponent(JSON.stringify(this.kycStatus))
                    });

                    if(timestamp){
                        await this.secureStorage.setValue(SecureStorageKey.kycStatusTimestamp, timestamp.toString());
                    }

                    if ( this.platform.is('ios') ) {
                        link.href = appName + `ident?code=${TOKEN_VALUE}`;
                    } else if ( this.platform.is('android') ) {
                        link.href = `${authada.start.android.production}${TOKEN_VALUE}`;
                    } else {
                        link.href = authada.start.web.staging;
                    }
                    // console.log(link.href);
                    link.click();
                    link.remove();
                }
            },
            {
            text: this.translate.instant('APPSTORE.install'),
            handler: () => {
                const link = document.createElement('a');
                document.body.appendChild(link);
                link.setAttribute('style', 'display: none');
                if ( this.platform.is('ios') ) {
                    link.href = authada.market.ios.production;
                } else if ( this.platform.is('android') ) {
                    link.href = authada.market.android.production + appName;
                } else {
                    link.href = authada.start.web.staging;
                }
                // console.log(link.href);
                link.click();
                link.remove();
            }
        },
            {
                text: this.translate.instant('APPSTORE.cancel'),
                role: 'cancel',
                handler: () => {
                    const testIndex = this.kycStatus.findIndex(k => k.kycProvider === KycProvider.AUTHADA_TEST);
                    if(testIndex) {
                        delete this.kycStatus[testIndex].kycInitiatedTimeStamp;
                        delete this.kycStatus[testIndex].additionalInfo;
                        this.kycStatus[testIndex].kycState = KycState.KYC_WAITING;
                        const prodIndex = this.kycStatus.findIndex(k => k.kycProvider === KycProvider.AUTHADA_PROD);
                        delete this.kycStatus[prodIndex].kycInitiatedTimeStamp;
                        delete this.kycStatus[prodIndex].additionalInfo;
                        this.kycStatus[prodIndex].kycState = KycState.KYC_WAITING;
                    }
                }
            }
        ]);
    }

    private checkOndato(provider: KycProvider, status: KycStatus): void {
        this.apiProviderService.ondatoCheckPromise(status.additionalInfo.ondatoSessionId)
            .then(this.handleVerificationCheck(provider, status));
    }

    private checkVeriff(provider: KycProvider, status: KycStatus): void {
        this.apiProviderService.veriffCheckPromise(status.additionalInfo.veriffSessionId)
            .then(this.handleVerificationCheck(provider, status));
    }

    private async checkAuthada(provider: KycProvider, status: KycStatus): Promise<void> {
        this.apiProviderService.authadaCheckPromise(status.additionalInfo.sessionToken)
            .then(this.handleVerificationCheck(provider, status));
    }

    private async checkVerifeye(provider: KycProvider, status: KycStatus): Promise<void> {
        this.apiProviderService.verifeyeCheckPromise(status.additionalInfo.verifeyeSessionId)
            .then(this.handleVerificationCheck(provider, status));
    }

    /**
     * retrieveVc() handles the process of fetching the DID and VCs associated
     * with a given user based on the kycProcess secure-storage key values.
     */
    async retrieveVc(): Promise<{}> {
        return new Promise(resolve => {
            Promise.all([
                this.apiProviderService.getVcIds(),
                this.secureStorage.getValue(SecureStorageKey.verifiableCredentialEncrypted, false),
                this.secureStorage.getValue(SecureStorageKey.did, false)
            ]).then(async ([vcIds, vcEncrypted, did]) => {
                await this.secureStorage.setValue(SecureStorageKey.vcId, JSON.stringify(vcIds));
                const getDecryptedId = async el => {
                    const objectResponse = await this.cryptoService.decryptVerifiableCredential(el);
                    if ( typeof objectResponse === 'object' ) {
                        const vc = JSON.parse(objectResponse.vc);
                        return vc.id;
                    }
                    return undefined;
                };
                const pushList = differenceWith(vcIds, (vcEncrypted ? JSON.parse(vcEncrypted) : []).map(getDecryptedId), isEqual);
                for ( const push of pushList ) {
                    try {
                        const temp =  await this.apiProviderService.getVc(push);
                        await this.pushVc(temp);
                    } catch  (e) { // Why is that?
                        if ( e.status === 200 ) {
                            if ( !!e.error && !!e.error.text ) {
                                await this.pushVc(e.error.text);
                            }
                        }
                    }
                    // this.presentToast(this.translate.instant('CERTIFICATES.vcActive'));
                }
                if ( !did ) {
                    const objectResponse = await this.cryptoService.decryptVerifiableCredential();
                    if ( typeof objectResponse === 'object' ) {
                        const vc = JSON.parse(objectResponse.vc);
                        if ( !!vc.credentialSubject.id ) {
                            await this.secureStorage.setValue(SecureStorageKey.did, vc.credentialSubject.id);
                            const didDocument = await this.apiProviderService.getDid(vc.credentialSubject.id).catch(e => console.log(e));
                            if(didDocument) {
                                const didDocumentEncrypted = await this.cryptoService.encryptGenericData(JSON.stringify(didDocument));
                                await this.secureStorage.setValue(SecureStorageKey.didDocument, didDocumentEncrypted);
                            }
                        }
                    } else {

                        const existingDidDocument = await this.secureStorage.getValue(SecureStorageKey.didDocument, false);

                        if(!existingDidDocument) {
                            const didDocument = await this.apiProviderService.putDid();
                            if(didDocument) {
                                const encryptedDid = await this.cryptoService.encryptGenericData(JSON.stringify(didDocument));
                                await this.secureStorage.setValue(SecureStorageKey.didDocument, encryptedDid);
                                if(didDocument.id) {
                                    await this.secureStorage.setValue(SecureStorageKey.did, didDocument.id);
                                }
                            }
                        }


                    }
                } else {
                    let didDocumentEncrypted = await this.secureStorage.getValue(SecureStorageKey.didDocument, false);
                    if (!didDocumentEncrypted) {
                        const didDocument = await this.apiProviderService.getDid(did).catch(e => console.log(e));
                        if(didDocument) {
                            didDocumentEncrypted = await this.cryptoService.encryptGenericData(JSON.stringify(didDocument));
                            await this.secureStorage.setValue(SecureStorageKey.didDocument, didDocumentEncrypted);
                        }
                    }
                }
                resolve(true);
            }, error => {
                console.error(error);
            });
        });
    }

    presentToast(message: string) {
        this._ToastController.create({ message, duration: 2000, position: 'top' }).then((toast) => {
            toast.present();
        });
    }

    private async pushVc(value: string) {
        const val = await this.secureStorage.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);
        // console.log("value of verifiableCredentialEncrypted: " + val);
        if ( !val || val === '' || val === '[]' || !Array.isArray(JSON.parse(val)) ) {
            const emptyArray = [];
            emptyArray.push(value);
            const vcStringArray = JSON.stringify(emptyArray);
            // console.log("complete array string in verifiableCredentialEncrypted: " + vcStringArray);
            await this.secureStorage.setValue(SecureStorageKey.verifiableCredentialEncrypted, vcStringArray);
        } else {
            const listVcParse = JSON.parse(val);
            listVcParse.push(value);
            await this.secureStorage.setValue(SecureStorageKey.verifiableCredentialEncrypted, JSON.stringify(listVcParse));
            // console.log("Line 1 - listVc: " + value + " listVcParse: " + JSON.stringify(listVcParse));
        }
    }

    private handleVerificationCheck(provider: KycProvider, status: KycStatus): (response: VerificationCheckResponse) => Promise<void> {
        const index = this.kycStatus.findIndex(s => s.kycProvider === provider);
        return async (response: VerificationCheckResponse) => {
            if ( response.kycProcessStatus === 4 ) {
                if ( (status.kycInitiatedTimeStamp + 72 * 60 * 60 * 1000) < +Date.now() ) {
                    this.kycStatus[index].kycState = KycState.KYC_FAILED;
                    await this.setState(provider, KycState.KYC_WAITING);
                } // else do nothing
            } else if ( response.kycProcessStatus === 8192 ) {
                this.kycStatus[index].kycState = KycState.KYC_FAILED;
                await this.setState(provider, KycState.KYC_WAITING, response.kycProcessErrorCode);
            } else if ( response.kycProcessStatus === 128 ) {
                const checkSuccess =
                    ((provider === KycProvider.VERIFF_TEST_DRIVINGLICENCE ||
                        provider === KycProvider.VERIFF_TEST_IDCARD ||
                        provider === KycProvider.VERIFF_TEST_PASSPORT ||
                        provider === KycProvider.VERIFF_TEST_RESIDENCEPERMIT ||
                        provider === KycProvider.VERIFF_PROD_DRIVINGLICENCE ||
                        provider === KycProvider.VERIFF_PROD_IDCARD ||
                        provider === KycProvider.VERIFF_PROD ||
                        provider === KycProvider.VERIFF_TEST ||
                        provider === KycProvider.VERIFF_PROD_PASSPORT ||
                        provider === KycProvider.VERIFF_PROD_RESIDENCEPERMIT) &&
                        response.kycProcessResultCode.toLowerCase() === VeriffStatusCodes.VERIFF_9001.toLowerCase()) || (
                            (provider === KycProvider.VERIFEYE_PROD_DRIVINGLICENCE ||
                            provider === KycProvider.VERIFEYE_PROD_IDCARD ||
                            provider === KycProvider.VERIFEYE_PROD_PASSPORT ||
                            provider === KycProvider.VERIFEYE_TEST_DRIVINGLICENCE ||
                            provider === KycProvider.VERIFEYE_TEST_IDCARD ||
                            provider === KycProvider.VERIFEYE_TEST_PASSPORT ||
                            provider === KycProvider.VERIFEYE_PROD_RESIDENCEPERMIT ||
                            provider === KycProvider.VERIFEYE_TEST_RESIDENCEPERMIT ||
                            provider === KycProvider.VERIFEYE_PROD ||
                            provider === KycProvider.VERIFEYE_TEST
                            ) && !response.kycProcessErrorCode // ANSIK: Check if this is correct
                        )
                        || ((provider === KycProvider.AUTHADA_TEST || provider === KycProvider.AUTHADA_PROD) && !response.kycProcessErrorCode)
                        || ((provider === KycProvider.ONDATO_TEST || provider === KycProvider.ONDATO_PROD) && !response.kycProcessErrorCode);
                const checkVeriffOtherStatusCodes =
                    ((provider === KycProvider.VERIFF_TEST_DRIVINGLICENCE ||
                        provider === KycProvider.VERIFF_TEST_IDCARD ||
                        provider === KycProvider.VERIFF_TEST_PASSPORT ||
                        provider === KycProvider.VERIFF_TEST_RESIDENCEPERMIT ||
                        provider === KycProvider.VERIFF_PROD_DRIVINGLICENCE ||
                        provider === KycProvider.VERIFF_PROD_IDCARD ||
                        provider === KycProvider.VERIFF_PROD ||
                        provider === KycProvider.VERIFF_TEST ||
                        provider === KycProvider.VERIFF_PROD_PASSPORT ||
                        provider === KycProvider.VERIFF_PROD_RESIDENCEPERMIT
                        ) &&
                        response.kycProcessResultCode.toLowerCase() !== VeriffStatusCodes.VERIFF_9001.toLowerCase() &&
                        !response.kycProcessErrorCode);
                // console.log("checkVeriffOtherStatusCodes: " + checkVeriffOtherStatusCodes)
                if ( checkSuccess ) {
                    this.kycStatus[index].kycState = KycState.KYC_SUCCESSFUL;
                    await this.setState(provider, KycState.DID_RETRIEVE, response.kycProcessResultCode);
                } else if ( checkVeriffOtherStatusCodes ) {
                    this.kycStatus[index].kycState = KycState.KYC_FAILED;
                    await this.setState(provider, KycState.KYC_WAITING, response.kycProcessResultCode);
                } else if ( response.kycProcessErrorCode ) { // In case of error code always use that
                    this.kycStatus[index].kycState = KycState.KYC_FAILED;
                    await this.setState(provider, KycState.KYC_WAITING, response.kycProcessErrorCode);
                }
            }
        };
    }

    private async handleVerificationErrorResponse(error): Promise<void> {
        await this.loaderProviderService.loaderDismiss();
        if ( error.status === 429 ) {
            if ( !!error.headers.get('Retry-After') ) {
                const waitMessage = this.translate.instant('LOADER.authadaWaitwithTimer.part1')
                    + error.headers.get('Retry-After') + this.translate.instant('LOADER.authadaWaitwithTimer.part2');
                await this.alertService.alertCreate('', '', waitMessage, ['Ok']);
            }
            await this.alertService.alertCreate('', '', this.translate.instant('LOADER.authadaWait'), ['Ok']);
        } else {
            await this.alertService.alertCreate(
                this.translate.instant('GENERAL.error'),
                this.translate.instant('KYC.something-wrong.sub-header'),
                this.translate.instant('KYC.something-wrong.message'),
                [
                    {
                        text: this.translate.instant('GENERAL.ok'),
                        handler: () => {
                        }
                    }]
            );
        }
    }

    public async isUserAllowedToUseChatMarketplace(): Promise<boolean> {
        var t = await this.secureStorage.getValue(SecureStorageKey.userTrustData, false);
        var a = await this.secureStorage.getValue(SecureStorageKey.kycStatus, false);
        var b = await this.secureStorage.getValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false);
        var result1, result2, result3 = false;

        var trustStorage = !!t ? JSON.parse(t): {};
        var trustKeys = Object.keys(trustStorage);
        if(trustKeys.length > 0) {
            var ttt = [];
            trustKeys.forEach(tt => {
                ttt.push(trustStorage[tt].trustActive && (trustStorage[tt].trustValue == 1));
            })
            if(ttt.find(l => l)) {
                result1 = true;
            }
        }

        var parsedKycStatus = !!a ? JSON.parse(a) : []
        if(parsedKycStatus.length > 0) {
            result2 = !!parsedKycStatus.find(k => [KycState.DID_RETRIEVE, KycState.VC_RETRIEVED, KycState.KYC_SUCCESSFUL].includes(k.kycState))
        }

        result3 = !!b ? (b == "true") : false;

        // Add firstname, lastname, dateofbirth

        return (result1 || result2 || result3)
    }

    public checkIfUserHasDoneKYCthroughOndato(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.secureStorage.getValue(SecureStorageKey.kycUsingOndatoCompleted, false).then((kyc) => {
                resolve((kyc == true.toString()))
            })
        })
    }

    public checkIfUserHasGoneThroughKYCOnce(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.secureStorage.getValue(SecureStorageKey.userIsAllowedToAccessChatMarketplace, false).then((kyc) => {
                resolve((kyc == true.toString()))
            })
        })
    }

    public changeKycProvider(source: KycProvider, documentType: string) {
        this.kycStatus.find(k => k.kycProvider === source).kycProvider = KycProvider[`${source.toString()}_${documentType}`];
    }

}
