import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DiscordService } from '../core/providers/cloud/discord.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { SocialMediaService } from '../core/providers/social-media/social-media.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { SocialMediaTaskServiceAkita } from '../core/providers/state/social-media-task/social-media-task.service';
import { SocialMediaTask, TargetApp, TaskCode } from '../core/providers/state/social-media-task/social-media-task.model';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';
import { TranslateProviderService } from '@services/translate/translate-provider.service';
import { TelegramService } from '@services/cloud/telegram.service';


@Component({
  selector: 'app-social-media-tasks',
  templateUrl: './social-media-tasks.component.html',
  styleUrls: ['./social-media-tasks.component.scss'],
})
export class SocialMediaTasksComponent implements OnInit {

  _SocialMediaTasks: SocialMediaTask[] = [];

  referralGenerated = false;

  option = {
    slidesPerView: 2.5,
    centeredSlides: true,
    loop: false,
    spaceBetween: 10
  }

  points;
  lang: string;

  constructor(
    private _ModalController: ModalController,
    private nav: NavController,
    private _Api: SocialMediaService,
    private _Loader: LoaderProviderService,
    private _Discord: DiscordService,
    private _Telegram: TelegramService,
    private _User: UserProviderService,
    public _Theme: ThemeSwitcherService,
    private _Social: SocialSharing,
    private socialMediaTaskServiceAkita: SocialMediaTaskServiceAkita,
    private socialMediaService: SocialMediaService,
    private translate: TranslateProviderService,
    private firecloud: FirestoreCloudFunctionsService
  ) { }

  async ngOnInit() {
    await this.loadTasks();
  }

  async loadTasks() {
    this._SocialMediaTasks = this.socialMediaTaskServiceAkita.getAllSocialMediaTasks();
    this.lang = await this.translate.getLangFromStorage();

    const points = await this.firecloud.returnPoints();
    if (points) {
      this.points = points.pointsToReferrer;
    } else {
      this.points = 0;
    }

    const taskList = await this._Api.fetchSocialMediaTasks();
    const userStatus = await this._Api.fetchSocialMediaUserStatus();

    let _SocialMediaTasks: SocialMediaTask[] = Array.from(taskList, (k: any) => Object.assign(k, { done: !!userStatus.find(us => (us.taskId == k.taskId)) }));
    _SocialMediaTasks = _SocialMediaTasks.filter(s => (s.language == "all" || s.language.split(",").includes(this.lang)));
    // IMPORTANT: assign taskId to each task if it doesn't exist, this is required for the Akita store
    _SocialMediaTasks.forEach((task, i) => { if (!task.taskId) task.taskId = i.toString() })
    this._SocialMediaTasks = _SocialMediaTasks;

    this.socialMediaTaskServiceAkita.setSocialMediaTasks(this._SocialMediaTasks);
  }

  async openTask(task: SocialMediaTask) {

    if (task.done) {
      return;
    }

    // await this._Loader.loaderCreate();

    let response;

    if (task.targetApp == TargetApp.telegram) {

      if (task.taskCode == TaskCode.follow) {
        this._Telegram.followOnTelegram(task.inviteUrl);
      }
      if (task.taskCode == TaskCode.post) {
        this._Telegram.postOnTelegram(task.inviteUrl, task.shareContent);
      }
    }

    if (task.targetApp == TargetApp.mailchimp && task.taskCode == TaskCode.subscribe) {
      try {
        await this._Loader.loaderCreate();
        response = await this.socialMediaService.subscribeMailChimp(task.taskId);
        await this._Loader.loaderDismiss();
        await this.loadTasks();
      } catch (error) {
        console.log(error);
        await this._Loader.loaderDismiss();
      }

      return;
    }

    if (task.targetApp == TargetApp.discord) {
      if (task.taskCode == TaskCode.follow) {
        this._Discord.followOnDiscord(task.targetChannel, task.shareContent);
      }
      if (task.taskCode == TaskCode.post) {
        this._Discord.postOnDiscord(task.targetChannel, task.shareContent);
      }
    }

    if (task.targetApp == TargetApp.facebook) {
      if (task.taskCode == TaskCode.follow) {
        await this._Loader.loaderDismiss();
        alert('Functionality not yet implemented');
        return;
      }
      if (task.taskCode == TaskCode.share) {
        try {
          response = await this._Social.shareViaFacebook('', '', task.shareContent);
          console.info("try await this._Social.shareViaFacebook('','',task.shareContent);", response);
        } catch (e) {
          console.info("catch await this._Social.shareViaFacebook('','',task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
      if (task.taskCode == TaskCode.post) {
        try {
          response = await this._Social.shareViaFacebook(task.shareContent);
          console.info("try await this._Social.shareViaFacebook(task.shareContent);", response);
        } catch (error) {
          console.info("catch await this._Social.shareViaFacebook(task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
    }

    if (task.targetApp == TargetApp.linkedin) {
      if (task.taskCode == TaskCode.follow) {
        await this._Loader.loaderDismiss();
        alert('Functionality not yet implemented');
        return;
      }
      if (task.taskCode == TaskCode.post) {
        await this._Loader.loaderDismiss();
        alert('Functionality not yet implemented');
        return;
      }
    }

    if (task.targetApp == TargetApp.twitter) {
      if (task.taskCode == TaskCode.follow) {
        await this._Loader.loaderDismiss();
        alert('Functionality not yet implemented');
        return;
      }
      if (task.taskCode == TaskCode.share) {
        try {
          response = await this._Social.shareViaTwitter('', '', task.shareContent);
          console.info("try await this._Social.shareViaTwitter('','',task.shareContent);", response);
        } catch (error) {
          console.info("catch await this._Social.shareViaTwitter('','',task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
      if (task.taskCode == TaskCode.post) {
        try {
          response = await this._Social.shareViaTwitter(task.shareContent);
          console.info("try await this._Social.shareViaTwitter(task.shareContent);", response);
        } catch (error) {
          console.info("catch await this._Social.shareViaTwitter(task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
    }

    if (task.targetApp == TargetApp.whatsapp) {
      if (task.taskCode == TaskCode.share) {
        try {
          response = await this._Social.shareViaWhatsApp('', '', task.shareContent);
          console.info("try await this._Social.shareViaWhatsApp('','',task.shareContent);", response);
        } catch (error) {
          console.info("catch await this._Social.shareViaWhatsApp('','',task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
      if (task.taskCode == TaskCode.post) {
        try {
          response = await this._Social.shareViaWhatsApp(task.shareContent);
          console.info("try await this._Social.shareViaWhatsApp(task.shareContent);", response);
        } catch (error) {
          console.info("catch await this._Social.shareViaWhatsApp(task.shareContent);", response);
          await this._Loader.loaderDismiss();
          return;
        }
      }
    }

    if (task.targetApp == TargetApp.mailchimp) {
      if (task.taskCode == TaskCode.subscribe) {
        await this._Loader.loaderDismiss();
        alert('Functionality not yet implemented');
        return;
      }
    }

    if (!response) {
      await this._Loader.loaderDismiss();
      return;
    }

    // const { taskId, earnings, earningsToken } = task;
    // const timestamp = +new Date();
    // const status = 2;

    // try {
    //   let request = await this._Api.sendSocialMediaUserStatus({ taskId, status, timestamp, earnings, earningsToken });

    //   if(request.status == "success") {
    //     this._SocialMediaTasks.find(k => k.taskId == taskId).done = true;
    //   }
    // } catch(e) {
    //   this._SocialMediaTasks.find(k => k.taskId == taskId).done = true;
    // }

    // await this._Loader.loaderDismiss();
  }

  goToReferral() {
    this.nav.navigateForward('/sign-up/create-referral');
  }

  close() {
    this._ModalController.dismiss();
  }

}
