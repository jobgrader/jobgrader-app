import { Component, NgZone, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavController, AlertController, ActionSheetController, ModalController, ToastController, PopoverController } from '@ionic/angular';
import { StatusBar, Style } from '@capacitor/status-bar';
import { AlertsProviderService } from './core/providers/alerts/alerts-provider.service';
import { DeeplinkProviderService } from './core/providers/deeplink/deeplink-provider.service';
import { TranslateProviderService } from './core/providers/translate/translate-provider.service';
import { AuthenticationProviderService } from './core/providers/authentication/authentication-provider.service';
import { ApiProviderService } from './core/providers/api/api-provider.service';
import { Subject } from 'rxjs';
import { AppStateService } from './core/providers/app-state/app-state.service';
import { AppInitializerService } from './app-initializer.service';
// import { delay, take, tap } from 'rxjs/operators';
import { PushService } from './core/providers/push/push.service';
import { LockService } from './lock-screen/lock.service';
import { CryptoProviderService } from './core/providers/crypto/crypto-provider.service';
import { SecureStorageService } from './core/providers/secure-storage/secure-storage.service';
import { ThemeSwitcherService } from './core/providers/theme/theme-switcher.service';
import { SecureStorageKey } from './core/providers/secure-storage/secure-storage-key.enum';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { environment } from '../environments/environment';
import { EventsList, EventsService } from './core/providers/events/events.service';
// import { ContactAddModalComponent } from './contact/shared/contact-add-modal/contact-add-modal.component';
// import { ChatService } from './core/providers/chat/chat.service';
// import { AppUpdatesService } from './core/providers/app-updates/app-updates.service';
import { SQLStorageService } from 'src/app/core/providers/sql/sqlStorage.service';
import { NetworkService } from './core/providers/network/network-service';
// import { AdjustService } from './core/providers/adjust/adjust.service';
import { UDIDNonce } from './core/providers/device/udid.enum';
import * as CryptoJS from 'crypto-js';
import { RootJailbreakService } from './core/providers/root-jailbreak/root-jailbreak.service';
import { WalletConnectService } from './core/providers/wallet-connect/wallet-connect.service';
import { File } from '@awesome-cordova-plugins/file/ngx';
// import { HumanService } from './core/providers/human/human.service';
import { Camera } from '@capacitor/camera';
import { Capacitor } from '@capacitor/core';
import { SplashScreen } from '@capacitor/splash-screen';
import { initializeApp } from "firebase/app";
import { SQLiteService } from './services/sqlite.service';
import { KycmediaService } from './core/providers/kycmedia/kycmedia.service';
import { KycService } from './kyc/services/kyc.service';
import { DropboxService } from './core/providers/cloud/dropbox.service';
import { BroadcastService } from './core/providers/broadcast/broadcast.service';
import { App, URLOpenListenerEvent } from '@capacitor/app';
import { CryptojsProviderService } from './core/providers/cryptojs/cryptojs-provider.service';
import { GoogleAuth, GoogleAuthPluginOptions } from '@codetrix-studio/capacitor-google-auth';
import { MoralisService } from './core/providers/moralis/moralis.service';
import { DidVcService } from './core/providers/did-vc/did-vc.service';
import { register } from 'swiper/element/bundle';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';

// register swiper element (WebComponents)
register();


declare var navigator: any;    // for cordova 10 incompat with ionic native wrapper.
declare var window: any;
declare var document: any;
// declare var cordova: any;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent implements OnDestroy {

    private destroy$: Subject<any> = new Subject<any>();
    public testDisp = !environment.production;
    public powerUserMode = false;
    public toggleChatEncryption = true;
    public toggleSecurePIN = true;

    isWeb = false;

    constructor(
        public lockService: LockService,
        private router: Router,
        private platform: Platform,
        private alertsProviderService: AlertsProviderService,
        private deeplinkProviderService: DeeplinkProviderService,
        private translateProviderService: TranslateProviderService,
        private authenticationProviderService: AuthenticationProviderService,
        public apiProviderService: ApiProviderService,
        public secureStorageService: SecureStorageService,
        public themeSwitcherService: ThemeSwitcherService,
        public _AppStateService: AppStateService,
        private appInitializerService: AppInitializerService,
        private pushService: PushService,
        private cryptoService: CryptoProviderService,
        private nav: NavController,
        private _AlertController: AlertController,
        private _ActionSheetController: ActionSheetController,
        private audio: NativeAudio,
        private _ModalController: ModalController,
        public _PopoverController: PopoverController,
        private _KycmediaService: KycmediaService,
        private _KycService: KycService,
        // public _ChatService: ChatService,
        // private _AppUpdatesService: AppUpdatesService,
        public _EventsService: EventsService,
        private _NetworkService: NetworkService,
        // private _AdjustService: AdjustService,
        private _Moralis: MoralisService,
        private _FireCloud: FirestoreCloudFunctionsService,
        private _ToastController: ToastController,
        private _CryptojsProviderService: CryptojsProviderService,
        private _Broadcast: BroadcastService,
        private rootJail: RootJailbreakService,
        private _WalletConnectService: WalletConnectService,
        private _Zone: NgZone,
        private _File: File,
        private _Dropbox: DropboxService,
        public _SQLStorageService: SQLStorageService,
        private _DidVcService: DidVcService,
        private sqliteService: SQLiteService) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(async () => {
            // document.querySelector('meta[name=viewport]').setAttribute('content', 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no');
            console.log("this.rootJail.init();"); this.rootJail.init();
            console.log("this.rootJail.init();"); this.initializeGogoleAuth();
            console.log("this.initializeSqlite;"); this.initializeSqlite();
            console.log("this.initializeFirebase()"); this.initializeFirebase();
            console.log("this.themeSwitcherService.themeEventSubscription();"); this.themeSwitcherService.themeEventSubscription();
            console.log("this.themeSwitcherService.setSavedTheme();"); this.themeSwitcherService.setSavedTheme();
            console.log("this.setupGlobalEvents();"); this.setupGlobalEvents();
            console.log("this.cryptoService.init();"); this.cryptoService.init();
            console.log("this.setupStatusBar();"); this.setupStatusBar();
            console.log("this.setupCustomBackButtonListener();"); this.setupCustomBackButtonListener();
            // console.log("this.setupAudioFiles();"); this.setupAudioFiles();
            console.log("this.setupHelixPublicKeyAndGifSettings();"); this.setupHelixPublicKeyAndGifSettings();
            console.log("this.setupPushNotifications();"); this.setupPushNotifications();
            console.log("this.setupAppPermissions();"); this.setupAppPermissions();
            console.log("this.setupDeepLinks();"); this.setupDeepLinks();
            console.log("this._FireCloud.init();"); this._FireCloud.init();
            console.log("this._Moralis.init();"); this._Moralis.init();
            // console.log("this._AdjustService.init();"); this._AdjustService.init();
            // this.setupVideoFiles();
            if (this._AppStateService.basicAuthToken) {
                console.log("this.manageWalletConnectConnections();"); this._WalletConnectService.manageWalletConnectConnections();
                console.log("this.manageWalletConnectConnections2();"); this._WalletConnectService.manageWalletConnectConnections2();
            }
            // console.log("this._Human.fortuneInit();"); this._Human.fortuneInit();
        });
    }

    public initializeGogoleAuth(): Promise<void> {

        return new Promise(resolveAll => {

            const config: GoogleAuthPluginOptions = {
                scopes: environment.googleAuth.scopes,
                serverClientId: environment.googleAuth.serverClientId,
                clientId: environment.googleAuth.clientId,
                iosClientId: environment.googleAuth.iosClientId,
                androidClientId: environment.googleAuth.androidClientId,
                forceCodeForRefreshToken: environment.googleAuth.forceCodeForRefreshToken
            }

            GoogleAuth.initialize(config);

            resolveAll();

        });

    }

    public initializeSqlite(): Promise<void> {

        return new Promise(resolveAll => {


            this.sqliteService.initializePlugin()
                .then(
                    async (ret) => {
                        console.log('AppComponent#initializeSqlite; ret ' + ret);

                        if (this.sqliteService.platform === "web") {
                            this.isWeb = true;

                            await customElements.whenDefined('jeep-sqlite');
                            const jeepSqliteEl = document.querySelector('jeep-sqlite');

                            if (jeepSqliteEl === null) {
                                console.log('AppComponent#initializeSqlite; jeepSqliteEl is null');

                                resolveAll();
                            }

                            await this.sqliteService.initWebStore();
                            console.log(`AppComponent#initializeSqlite; isStoreOpen: ${await jeepSqliteEl.isStoreOpen()}`);

                        }

                        const result: any = await this.sqliteService.echo('Hello World');
                        console.log('AppComponent#initializeSqlite; result.value:', result.value);

                        console.log("this._SQLStorageService.init();"); this._SQLStorageService.init();
                        console.log("this.appInitializerService.init();"); this.appInitializerService.initSecureStorage();

                        resolveAll();
                    }
                )
                .catch(
                    (error) => {
                        console.log('AppComponent#initializeSqlite; error ' + error);
                        resolveAll();
                    }
                );
        });
    }

    public initializeFirebase(): Promise<void> {
        if (Capacitor.isNativePlatform()) {
            return;
        }
        initializeApp(environment.firebase);
    }

    // refer to other ionic overlays if required: https://ionicframework.com/docs/developing/hardware-back-button
    customBackButtonProcess(nav: NavController): Promise<boolean> {
        return new Promise(resolveAll => {
            var modalObjectWasDismissed: boolean = false;

            var a = this._ModalController.getTop().then(modal => {
                if (modal) {
                    modalObjectWasDismissed = true;
                    // ONLY dismiss if it's not the lock screen.
                    if (modal.id != null && modal.id != 'LockScreenPage') {
                        modal.dismiss();
                    }
                }
            });

            var b = a.then(() => {
                return new Promise<void>(resolve => {
                    if (modalObjectWasDismissed) { resolve(); }
                    else {
                        this._ActionSheetController.getTop().then(actionSheet => {
                            if (actionSheet) {
                                modalObjectWasDismissed = true;
                                actionSheet.dismiss();
                            }
                            resolve();
                        });
                    }
                });
            });

            var c = b.then(() => {
                return new Promise<void>(resolve => {
                    if (modalObjectWasDismissed) { resolve(); }
                    else {
                        this._AlertController.getTop().then(alert => {
                            if (alert) {
                                modalObjectWasDismissed = true;
                                alert.dismiss();
                            }
                            resolve();
                        });
                    }
                });
            });

            c.then(() => {
                if (!modalObjectWasDismissed && nav != null) {
                    nav.back();
                }
                resolveAll(modalObjectWasDismissed);
            });
        });
    }

    /**
     * NG2 hook for component destroyment
     */
    public ngOnDestroy(): void {
        this.destroy$.next(undefined);
    }

    private setAppState(basicAuthToken: string, isAuthorized: boolean): void {
        this._AppStateService.isAuthorized = isAuthorized;
    }

    setupStatusBar() {
        if (this.platform.is('android')) {
            // TODO mburger: no Translucent on capacitorjs
            StatusBar.setStyle({ style: Style.Dark });
            // styleBlackTranslucent();

            return;
        }
        if (this.platform.is('ios')) {
            // TODO mburger: mybe need to set hex on capacitorjs
            StatusBar.setBackgroundColor({ color: 'white' });
            //backgroundColorByName('white');

            // TODO mburger: doesnt exists on capacitorjs
            // StatusBar.styleDefault();

            return;
        }
    }

    setupHelixPublicKeyAndGifSettings() {
        // Getting the helix public key
        this.apiProviderService.getHelixPublicKeyPromise()
            .then(helixPublicKey => {
                this.secureStorageService.setValue(SecureStorageKey.helixPublicKey, helixPublicKey);
                this.secureStorageService.setValue(SecureStorageKey.gifSetting, JSON.stringify({
                    noOfFrames: 15,
                    numFrames: 15,
                    frameFloor: false,
                    frameDuration: 0.05,
                    interval: 0.05,
                    gifWidth: 150,
                    gifHeight: 150,
                    videoDuration: 2,
                    gifInterval: 0.0001,
                    reverse: true
                }))
            });
    }

    setupDeepLinks() {

        App.addListener('appUrlOpen', (event: URLOpenListenerEvent) => {
            this._Zone.run(async () => {

                console.log("AppComponent#setupDeepLinks; New Deeplink Listener event:", event);

                if (event.url.startsWith('https://pwa-test.jobgrader.app') || event.url.startsWith('https://web.jobgrader.app')) {
                    // Example url: https://pwa-test.jobgrader.app/tabs/tab2
                    // slug = /tabs/tab2
                    const slug = event.url.split(".app").pop();
                    if (slug) {
                        console.log("AppComponent#setupDeepLinks; slug:", slug);
                        // alert('Slug: ' + slug);
                        // this.router.navigateByUrl(slug);
                    }

                    // return;
                }

                var url = new URL(event.url);

                const Pathnames = {
                    Signature: '/signature',
                    Verification: '/verification',
                    VeriffThanks: '/veriff-thanks',
                    KycSuccess: '/kyc-success',
                    Eudgc: '/eudgc',
                    Pretix: '/pretix',
                    Ticket: '/ticket',
                    AgeVerification: '/ageverification',
                    CarIdentity: '/caridentity',
                    ConnectedAccounts: '/connected-accounts',
                    WalletConnect: '/wc',
                    AssessmentCertificate: '/assessment-certificate',
                    Referral: '/referral',
                }

                const Origins = {
                    WalletConnect: "wc://"
                }

                if (url.origin == Origins.WalletConnect) {
                    this.lockService.removeResume();
                    this._WalletConnectService.setNavigationViaDeeplink();
                    this._WalletConnectService.processDeeplink(url.href, true);
                    this.lockService.addResume();
                }

                if (url.pathname.includes(Pathnames.WalletConnect)) {
                    this.lockService.removeResume();
                    try {
                        var receivedUri = decodeURIComponent(url.searchParams.get('uri'));
                    } catch (e) {
                        receivedUri = url.searchParams.get('uri');
                    }
                    console.log("receivedUri", receivedUri);
                    this._WalletConnectService.setNavigationViaDeeplink();
                    this._WalletConnectService.processDeeplink(receivedUri, true);
                    this.lockService.addResume();
                }

                if (url.pathname.includes(Pathnames.VeriffThanks) || url.pathname.includes(Pathnames.KycSuccess)) {
                    this.nav.navigateForward('/veriff-thanks');
                }

                if (url.pathname.includes(Pathnames.ConnectedAccounts)) {

                    this.lockService.removeResume();

                    var code = url.searchParams.get('code');

                    try {
                        code = decodeURIComponent(code);
                    } catch (e) {
                        console.log(e);
                    }
                    this._Dropbox.processDropboxAuthToken("jobgrader://jobgrader.app/connected-accounts", code);
                    this.lockService.addResume();
                }

                if (url.pathname.includes(Pathnames.Eudgc)) {

                    // Examples on iOS: helix-id://helix-id.com/eudgc?string=U2FsdGVkX1%2BXPQ2r1mSBnm31px%2B%2FRh%2FZ1rjmaLwkzF3Cs5VLJAjk0hD5OrOmGx2zlMnfgxIEWRwvaJmIN22EYGiFXKDSmQpp2j6sg0mMsbHxOpPVstwXWhbh1sgUaKqrwdRyF6Pwm%2FWPJSEpL8lhzRnW66ttITTI6leBONxTzlWaCN9npulXwGAStc7k82eU7C5ZdcPU5oFrmV9%2FC8%2FjcRxmY4FDfjLep9raLIg82QWhuVtrdzBzN25vDZm2Rxy%2BekhVrjTQEFMjRicvZmavZq2B3tCIh4q418U7HL7NyqXgoXHECNuceTk0oRDIY2kd6jRtNbpbGc5CpflbDTcHnCoIEkrq8MfB%2FthMgGr5%2FAOr%2BGCkV4ftfpvu6G9mdF4mwz2Q3bE5S%2ByLcKLS3xrAPcTOTOqcXZuvwaq1ncsL9HFR%2BukJ%2Fu8k2q4K3DgB6bQlw2oKXb%2FLB9G3rnXW7iNSsFMfD%2Bu4E9fvx%2BI0t1WIH%2BBtrnW6odxVySSasMumpp8kGtRuG9UQGtpth8dqA6vAzCIogGAGZiOtojRiKrn2pCQwCnNmlAyBOK8SKjwoZPzeWcEuAjA4Yu8BZCu5VLqUP%2FFEvIDDYKARsshw%2F4rs4RUEtHDlj8iRZFrKDHZOa7cSWoQyLQngn7doCU%2Btc%2FQavCGv5p1BavRh3utOfkFboqDEab8%2FsLIXzI2wnSAuOFCos40fvk67O8lMuLzEIRpUqkphnIGeq9jwthvrRZqHNKwnGZBx7oMWQ74tKlsPW2bmISLBKNtJ3DnpYAv3d5XbJoZznyxzEVB8xmVmIPcimpU1jTQEkQXywnbCKpJLE5uXTcFQJfx%2FvTXOhUZDkgo%2B1SMU1sc11EAV8vsz6Q6L0W%2FyBNxteSVrU7VSLfnJlrH9AA4eF51v025GCpG%2FSloRwhMVyP9eUH5AAaw2mmDBJuM%3D

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    if (receivedResponse) {

                        await this.deeplinkProviderService.processEudgcString(receivedResponse);

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward('/health-certificates');
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward('/health-certificates');
                        }
                    }
                }

                if (url.pathname.includes(Pathnames.CarIdentity)) {

                    // Example on iOS: helix-id://helix-id.com/caridentity?string=U2FsdGVkX18sS3rWN6x1yHIWkdoVfg7b%2Bp7Z0o%2BXz0anZENgNFR0cxzjLT8MmigrszgPwtgJTb9IiunnWLkVaenjLtalqxxaif1EtSYoLg9081WaRf055CFsGih4CyArfKX5qpmfaH8TTpXpOg1fiiPV2FUS8%2FCwutUMGhqNTZefEsESwYBh8T4e3Hlh90cksqjRlcJ41RWsF%2Ff9lJMJv5JvLTGStR%2BEj4f%2FL8evFAaZbKapcMTlO%2F%2Fbl8HcTV5aTgGolNC6tunfvU2UzPm0wunrzz0m6B6jhLHT2wDezzBt1lHcuMO8t3gywJUEMgCdjfOMX1%2Fx3FrEchKiPnHEGTj0VvzVhZAlc3u%2F2XvLIbSHIj5WFGD4LQk9OIxIp3cfiNbfCS%2FjzHj%2F8AaMJfwJr42PxETn4yJeAJu4no39BJFhmag28vSLSA4LcelvwBdKCBmyVuFD11E7if9E8rq8ORcK4OJJZhHgyZy2BAEIF7%2FuJHvlGcNB2G8VpnZwOk%2FbAGU2IAHFYE9c%2BUBWNcEf6fIHl9n4TLFWq6kEJci%2BJf%2FugBYYVWLcqLdvmMkFOjBDQ0%2B%2BmFQutGPjCOheGVycm%2ByFNEpQ2X1s%2FqcLeWI%2BefX5kz9Df32mFREYhy0t%2BS9aUCgIYvSpORVxtPND7fyxQq7HNPABZFJcsA16W5uYaZiiaayqor%2FiczTTyJn%2FzxNGmyjKv3uTGhUxiF042vVKhg%3D%3D

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    const extracted = this._CryptojsProviderService.decrypt(receivedResponse, UDIDNonce.energy);

                    this._DidVcService.generateCarVC(extracted, encodedString).then(r => {
                        if (r) {
                            this.nav.navigateForward(`/dashboard/certificates-page`);
                        }
                    });



                }

                if (url.pathname.includes(Pathnames.AssessmentCertificate)) {

                    // Example on iOS: helix-id://helix-id.com/caridentity?string=U2FsdGVkX18sS3rWN6x1yHIWkdoVfg7b%2Bp7Z0o%2BXz0anZENgNFR0cxzjLT8MmigrszgPwtgJTb9IiunnWLkVaenjLtalqxxaif1EtSYoLg9081WaRf055CFsGih4CyArfKX5qpmfaH8TTpXpOg1fiiPV2FUS8%2FCwutUMGhqNTZefEsESwYBh8T4e3Hlh90cksqjRlcJ41RWsF%2Ff9lJMJv5JvLTGStR%2BEj4f%2FL8evFAaZbKapcMTlO%2F%2Fbl8HcTV5aTgGolNC6tunfvU2UzPm0wunrzz0m6B6jhLHT2wDezzBt1lHcuMO8t3gywJUEMgCdjfOMX1%2Fx3FrEchKiPnHEGTj0VvzVhZAlc3u%2F2XvLIbSHIj5WFGD4LQk9OIxIp3cfiNbfCS%2FjzHj%2F8AaMJfwJr42PxETn4yJeAJu4no39BJFhmag28vSLSA4LcelvwBdKCBmyVuFD11E7if9E8rq8ORcK4OJJZhHgyZy2BAEIF7%2FuJHvlGcNB2G8VpnZwOk%2FbAGU2IAHFYE9c%2BUBWNcEf6fIHl9n4TLFWq6kEJci%2BJf%2FugBYYVWLcqLdvmMkFOjBDQ0%2B%2BmFQutGPjCOheGVycm%2ByFNEpQ2X1s%2FqcLeWI%2BefX5kz9Df32mFREYhy0t%2BS9aUCgIYvSpORVxtPND7fyxQq7HNPABZFJcsA16W5uYaZiiaayqor%2FiczTTyJn%2FzxNGmyjKv3uTGhUxiF042vVKhg%3D%3D

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    var extracted = this._CryptojsProviderService.decrypt(receivedResponse, UDIDNonce.energy);

                    this._DidVcService.generateAssessmentCertificate(extracted, encodedString).then(r => {
                        if (r) {
                            this.nav.navigateForward(`/dashboard/certificates-page`);
                        }
                    });

                }

                if (url.pathname.includes(Pathnames.Pretix)) {

                    // Example on iOS: helix-id://helix-id.com/pretix?string=helix-id://helix-id.com/pretix?string=U2FsdGVkX18YEyNjmu6vK9Z22wtqdXv%2F1QP2ehBGoHoYXix0jSF6ustGMimzVQDdSnUr4n%2F2yQHGLTqJW4Wjf93qOag6yhEO19Sip6Lxd5CbWvGXK2v3BPOlNCrrRvAnTUjWuP5HOhIO6y4qD4b%2FnXOhMU8zA9rCALUfSBaoMszSVWVLAviv1UgZcpZzSl1LviojdJ46URxZI4kSvJ%2B2w5NPk79Oa8tdN3baka4WPfwDSoWMYWvJqhcgDtPDcOGHaz%2FJ1WbILIN%2FWQu%2FNjHbFA%3D%3D


                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);


                    if (receivedResponse) {

                        await this.deeplinkProviderService.processPretixTicket(receivedResponse);

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward('/event-tickets');
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward('/event-tickets');
                        }
                    }
                }

                if (url.pathname.includes(Pathnames.Ticket)) {

                    // Example on iOS: helix-id://helix-id.com/ticket?string=U2FsdGVkX1%2FU9Hbmz3K5sjX%2BqQ5vU7POdOLeoiiCYsrWrKzEfigFpIcXbn9vKqJy

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    if (receivedResponse) {

                        var decoded = this.deeplinkProviderService.decodeDeeplinkStringValue(receivedResponse);
                        console.log(decoded);
                        const url = `/merchant-details/event/${(decoded as any).merchantId}`

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward(url);
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward(url);
                        }


                    }
                }

                if (url.pathname.includes(Pathnames.Signature)) {

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    if (receivedResponse) {
                        this.deeplinkProviderService.signatureDeeplinkStringValue = receivedResponse;

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward('/signature');
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward('/signature');
                        }
                    }
                }

                if (url.pathname.includes(Pathnames.AgeVerification)) {

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    if (receivedResponse) {
                        this.deeplinkProviderService.signatureDeeplinkStringValue = receivedResponse;
                        var extracted = this.deeplinkProviderService.decodeDeeplinkStringValue(decodeURIComponent(receivedResponse), UDIDNonce.energy);
                        var ob2 = {
                            dataAccessProcessId: extracted.dataAccessProcessId,
                            username: extracted.username,
                            publicKey: extracted.publicKey,
                            fields: extracted.fields,
                            checks: extracted.checks,
                            status: extracted.status
                        };

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(ob2))}`);
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(ob2))}`);
                        }

                    }
                }

                if (url.pathname.includes(Pathnames.Verification)) {

                    var encodedString = url.searchParams.get('string');

                    console.log(url.pathname, encodedString);

                    try {
                        var receivedResponse = decodeURIComponent(encodedString);
                    } catch (e) {
                        receivedResponse = encodedString;
                    }

                    console.log("receivedResponse", receivedResponse);

                    if (receivedResponse) {
                        this.deeplinkProviderService.resetSignatureDeeplinkStringValue();
                        this.deeplinkProviderService.verificationDeeplinkStringValue = receivedResponse;

                        if (this._AppStateService.isAuthorized) {
                            this.setAppState(this._AppStateService.basicAuthToken, true);

                            this.nav.navigateRoot('/dashboard').then(() => {
                                this.nav.navigateForward('/verification');
                            });
                        } else {
                            this.setAppState(undefined, false);
                            this.nav.navigateForward('/verification');
                        }
                    }
                }

                if (url.pathname.includes(Pathnames.Referral)) {

                    const code = url.searchParams.get('code');

                    if (code) {
                        await this.secureStorageService.setValue(SecureStorageKey.referralCode, code);
                    }

                    console.log('AppComponent#setupDeepLinks; pathname:', url.pathname);
                    console.log('AppComponent#setupDeepLinks; code:', code);


                    this.presentToast(`You're using the referral code: ${code}`);

                }

            });
        });

    }

    private generateRandomString() {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < 25) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }

    private async setupAppPermissions() {
        if (Capacitor.isPluginAvailable('Camera') && Capacitor.getPlatform() !== 'web') {
            let permStatus = await Camera.checkPermissions();

            if (permStatus.camera === 'prompt') {
                permStatus = await Camera.requestPermissions();
            }
        }
    }

    setupPushNotifications() {
        this.pushService.initPush().then((result: any) => {
            console.log('initPush successfull', result);
        }).catch((e: any) => {
            console.warn('Failed to init push', e);
        });
    }

    setupAudioFiles() {
        // Initialize Audio Files
        this.audio.preloadSimple('drop_start_app', './assets/audio/drop_start_app.mp3')
            .then(() => "drop_start_app set!")
            .catch((err) => { console.log("Preload 1 Failed"); console.log(err); })

    }

    setupCustomBackButtonListener() {
        if (!this.platform.is('android')) { return; }
        this.platform.backButton.subscribeWithPriority(999, () => {
            var currentUrl = window.location.href;
            let scannerCancelled = localStorage.getItem('scannerCancelled');

            var toHomePages = [
                // 'dashboard/tab-profile?source=smart',
                // 'dashboard/payments',
                // 'dashboard/tab-qrcode',
                // 'dashboard/tab-settings',
                // 'dashboard/tab-home',
            ];

            var toLoginPages = [
                'reset-password/step-1',
                'forgot-username'
            ];

            var disabledPages = [
                'dashboard/personal-details',
                'dashboard/payments',
                'dashboard/tab-qrcode',
                'dashboard/tab-settings',
                'dashboard/tab-home',
                'reset-password/step-4',
                'sign-up/step-5',
                'sign-up/step-7',
                'sign-up/step-10',
                'veriff-thanks',
                'feedback-thanks',
                'onboarding',
                'wallet-generation/step-5',
                'forgot-username/step-2'
            ];

            if (scannerCancelled === 'true') {
                localStorage.setItem('scannerCancelled', 'false');
                return;
            }

            // if (scannerCancelled === 'true' || disabledPages.some(x => currentUrl.indexOf(x) > -1)) {
            //     localStorage.setItem('scannerCancelled', 'false');
            //     return;
            // }

            // if (this.router.isActive('/login', true) && this.router.url === '/login') {
            //     /* tslint:disable */
            //     App.exitApp();
            //     /* tslint:enable */
            // }
            // else if (toHomePages.some(x => currentUrl.indexOf(x) > -1)) {
            //     this.customBackButtonProcess(null).then(modalObjectWasDismissed => {
            //         if (!modalObjectWasDismissed) {
            //             this.nav.navigateRoot('/dashboard', { animated: false });
            //         }
            //     });
            // }
            // else if (toLoginPages.some(x => currentUrl.indexOf(x) > -1)) {
            //     this.nav.navigateRoot('/login');
            // }
            // else {
            //     this.customBackButtonProcess(this.nav);
            // }
        });
    }

    encryptBasicAuthToken(basicAuthToken: string, udid: string, nonce: string): Promise<void> {
        return new Promise((resolve, reject) => {
            var string1 = udid.substr(4, 9);
            var string2 = nonce.substr(2, 11);
            var key = CryptoJS.SHA256(string1 + string2, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
            this.cryptoService.symmetricEncrypt(basicAuthToken, key).then((encrypted) => {
                this.secureStorageService.setValue(SecureStorageKey.encryptedBasicAuthToken, encrypted).then(() => {
                    resolve()
                }).catch(() => reject())
            }).catch(() => reject())
        })
    }

    setupGlobalEvents() {

        this.platform.ready().then(() => {



        })

        this.platform.pause.subscribe(() => {
            // this._ChatService.disconnect();
            // console.log('******************* paused! ******************* ')

            const currentUrl = this.router.url;
            if (['/verification', '/signature'].indexOf(currentUrl) !== -1) {
                this.nav.navigateRoot('/dashboard');
            }

            if (['/veriff', '/veriff-thanks'].indexOf(currentUrl) !== -1) {
                this.nav.navigateRoot('/dashboard/personal-details');
            }



        });

        this.platform.resume.subscribe(() => {

            this.rootJail.init();


            // console.log("this.setupDeepLinks();"); this.setupDeepLinks();

            this.pushService.processDeliveredNotifications();

            this._Broadcast.init();

            this.secureStorageService.getValue(SecureStorageKey.lockScreenToggle, false).then(toggle => {
                console.log('toggle value: ' + toggle);
                if (toggle == 'true') {
                    setTimeout(() => {
                        if (this._NetworkService.checkConnection()) {
                            // this._ChatService.connect(true).then((r) => {
                            //     console.log('*** Reconnected in platform.resume event:');
                            if (this._AppStateService.isAuthorized) {
                                this.authenticationProviderService.fetchRemainingVcs().then(() => {
                                    console.log('VC check done!');
                                    this.apiProviderService.getUserImageAndData(true, true);
                                })
                            }
                            // }).catch(e => { console.log('Promise rejected') })
                        }
                    }, 500)
                }
            }).catch(e => {
                console.log('unable to getValue')
            })

            this.secureStorageService.getValue(SecureStorageKey.toggleChatEncryption, false).then((toggleChatEncryption) => {
                this.toggleChatEncryption = environment.production ? true : (toggleChatEncryption == 'true')
            }).catch(e => {
                console.log('unable to getValue')
            })

            this.secureStorageService.getValue(SecureStorageKey.toggleSecurePIN, false).then((toggleSecurePIN) => {
                this.toggleSecurePIN = environment.production ? true : (toggleSecurePIN == 'true');
            }).catch(e => {
                console.log('unable to getValue')
            })

            this.secureStorageService.getValue(SecureStorageKey.powerUserMode, false).then((powerUserMode) => {
                this.powerUserMode = (powerUserMode == 'true')
            }).catch(e => {
                console.log('unable to getValue')
            })

            this.secureStorageService.getValue(SecureStorageKey.language, false).then((language) => {
                console.log("From app.component: language");
                console.log(language);
                if (language) {
                    this.translateProviderService.changeLanguage(language);
                }
            }).catch(e => {
                console.log('unable to getValue')
            })

            this.secureStorageService.getValue(SecureStorageKey.themeMode, false).then((themeMode) => {
                console.log("From app.component: theme");
                console.log(themeMode);
                this.themeSwitcherService.setTheme(themeMode);
            }).catch(e => {
                console.log('unable to getValue')
            })

            // this._AppUpdatesService.checkForUpdate().then(value => {
            //     console.log(value)
            //     if(value > 1) {
            //         this._AlertController.create({
            //             mode: 'ios',
            //             header: '',
            //             message: this.translateProviderService.instant('SETTINGSABOUT.case-plus-one'),
            //             buttons: [{
            //                 text: this.translateProviderService.instant('APPSTORE.update'),
            //                 role: 'primary',
            //                 handler: () => {
            //                     const link = document.createElement('a');
            //                     document.body.appendChild(link);
            //                     link.setAttribute('style', 'display: none');
            //                     link.href = this.platform.is('ios') ? 'https://apps.apple.com/de/app/helixid/id1469238013' : 'market://details?id=app.jobgrader.crowdworker';
            //                     link.click();
            //                     link.remove();
            //                 }
            //             }]
            //         }).then(alert => alert.present())
            //     }
            // })
        });

        this._EventsService.subscribe(EventsList.dataReceivedByPushNotification, () => {
            console.log("Push Service: ", "const { photo, userData, userTrust} = await this.apiProviderService.getUserImageAndData(true, true);");
            this.apiProviderService.getUserImageAndData(true, true).then(async ({ photo, userData, userTrust }) => {
                console.log("Push Service: ", "this.secureStorageService.setValue(SecureStorageKey.userData, JSON.stringify(userData));");
                await this.secureStorageService.setValue(SecureStorageKey.userData, JSON.stringify(userData));
                console.log("Push Service: ", "this.secureStorageService.setValue(SecureStorageKey.userTrustData, JSON.stringify(userTrust));");
                await this.secureStorageService.setValue(SecureStorageKey.userTrustData, JSON.stringify(userTrust));
                // await this._KycmediaService.fetchRemainingMedia();
                await this.authenticationProviderService.fetchRemainingVcs();
                this._EventsService.publish(EventsList.dataProcessedAfterKyc);
            }).catch(async e => {
                await this.authenticationProviderService.fetchRemainingVcs();
                this._EventsService.publish(EventsList.dataProcessedAfterKyc);
            })

        })

        // this._EventsService.subscribe(EventsList.showConfirmFriendRequest, (data) => {
        //     this._PopoverController.create({
        //         component: ContactAddModalComponent,
        //         cssClass: 'proto-custom-popover',
        //         componentProps: {
        //             isIncomingFriendRequest: true,
        //             newThread: data.newThread
        //         },
        //         translucent: false
        //     }).then(popover => {
        //         popover.present();
        //     });
        // });

        this._EventsService.subscribe(EventsList.powerUserMode, (data) => {
            console.log(data);
            this.powerUserMode = data
        })

        this._EventsService.subscribe(EventsList.toggleChatEncryption, (data) => {
            console.log(data);
            this.toggleChatEncryption = data
        })

        this._EventsService.subscribe(EventsList.toggleSecurePIN, (data) => {
            console.log(data);
            this.toggleSecurePIN = data
        })

        this._EventsService.subscribe(EventsList.themeChange, (theme) => {
            this.themeSwitcherService.setTheme(theme);
        })

        this._EventsService.subscribe(EventsList.languageChange, (language) => {
            this.translateProviderService.changeLanguage(language);
        })

        this._EventsService.subscribe(EventsList.hideSplash, () => {
            console.log('*** hiding splash screen ***');

            // mburger TODO: why we wrap this with a timeout?
            setTimeout(() => {
                SplashScreen.hide();
            }, 300);
        });
    }

    presentToast(message: string) {
        this._ToastController.create({
            message,
            position: 'top',
            duration: 2000
        }).then(toast => {
            toast.present();
        })
    }

}
