import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { SignUpPage } from './sign-up.page';

import { SignProviderGuard } from '../core/guards/sign/sign-provider.guard';

const routes: Routes = [
  {
    path: '',
    component: SignUpPage,
    children: [
      {
        path: 'step-1',
        loadChildren: () => import('../sign-up/components/step-1/step-1.module').then(m => m.SignUpStepOnePageModule)
      },
      {
        path: 'step-2',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-2/step-2.module').then(m => m.SignUpStepTwoPageModule)
      },
      {
        path: 'step-3',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-3/step-3.module').then(m => m.SignUpStepThreePageModule)
      },
      {
        path: 'step-4',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-4/step-4.module').then(m => m.SignUpStepFourPageModule)
      },
      {
        path: 'step-5',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-5/step-5.module').then(m => m.SignUpStepFivePageModule)
      },
      {
        path: 'step-6',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-6/step-6.module').then(m => m.SignUpStepSixPageModule)
      },
      {
        path: 'step-7',
        //canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-7/step-7.module').then(m => m.SignUpStepSevenPageModule)
      },
      {
        path: 'step-8',
        // canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-8/step-8.module').then(m =>  m.SignUpStepEightPageModule)
      },
      {
        path: 'step-9',
        // canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/step-9/step-9.module').then(m => m.SignUpStepNinePageModule)
      },
      /*
      {
        path: 'step-10',
        canActivate: [SignProviderGuard],
        loadChildren: '../sign-up/components/step-10/step-10.module#SignUpStepTenPageModule'
      },*/
      {
        path: 'create-referral',
        // canActivate: [SignProviderGuard],
        loadChildren: () => import('../sign-up/components/create-referral/create-referral.module').then(m => m.SignUpCreateReferralPageModule)
      },
      {
        path: '',
        redirectTo: '/sign-up/step-9',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, SharedModule, TranslateModule.forChild(), RouterModule.forChild(routes)],
  declarations: [SignUpPage]
})
export class SignUpPageModule {}
