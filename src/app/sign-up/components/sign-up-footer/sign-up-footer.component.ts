import { Platform } from '@ionic/angular';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'proto-sign-up-footer',
  templateUrl: './sign-up-footer.component.html',
  styleUrls: ['./sign-up-footer.component.scss']
})
export class SignUpFooterComponent implements OnInit {

  constructor(private platform: Platform) {}
  /** EventEmitter to tell the outer component that primary action was fired */
  @Output() public primaryAction = new EventEmitter<void>();
  /** EventEmitter to tell the outer component that secondary action was fired */
  @Output() public secondaryAction = new EventEmitter<void>();
  /** EventEmitter to tell the outer component that tertiary action was fired */
  @Output() public tertiaryAction = new EventEmitter<void>();
  /** The caption of the secondary button */
  @Input() public secondaryCaption = 'sign-up';
  @Input() public tertiaryCaption = '';
  /** If the secondary caption should be visible */
  @Input() public tertiaryActionVisible = false;
  @Input() public secondaryActionVisible = true;
  @Input() public primaryActionVisible = true;

  @Input() public currentFormIsValid: boolean;

  public keyboardIsOpened = false;
  public keyboardHeight = 0;

  public isAndroid: boolean;

  public onSecondaryAction = () => void this.secondaryAction.emit();

  ngOnInit() {
    if (!this.platform.is('android')) {
      this.isAndroid = false;

      window.addEventListener('keyboardWillShow', (event: any) => {
        this.keyboardIsOpened = true;
        this.keyboardHeight = event.keyboardHeight;
      });

      window.addEventListener('keyboardWillHide', () => {
        this.keyboardIsOpened = false;
        this.keyboardHeight = 0;
      });
    } else {
      this.isAndroid = true;
      this.keyboardHeight = 0;

      window.addEventListener('keyboardWillShow', (event: any) => {
        this.keyboardIsOpened = true;
      });

      window.addEventListener('keyboardWillHide', () => {
        this.keyboardIsOpened = false;
      });
    }
  }

  onPrimaryAction() {
    if (!this.currentFormIsValid) {
      return console.log('submit is disabled until form valid');
    }
    return this.primaryAction.emit();
  }

  onTertiaryAction() {
    if(this.tertiaryActionVisible) {
      return this.tertiaryAction.emit();
    }
  }


  getSubmitButtonClass() {
    return (this.currentFormIsValid) ? 'button': 'button disabled-submit';
  }

  
  getSubmitButtonStyle() {
    if (this.currentFormIsValid) {
      // console.log('Unfreeze submit button');
      return {
        'bottom': (this.keyboardIsOpened ? this.keyboardHeight + 84/2 : 84) + 'px'
      }
    }

    // if keyboard is open, hide it behind the keyboard otherwise, put it at the bottom.
    return (this.keyboardIsOpened) ? { 'bottom': '-84px' } : { 'bottom': '84px' }
  }
  
}
