import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SignUpFooterComponent } from './sign-up-footer.component';

describe('SignUpFooterComponent', () => {
  let component: SignUpFooterComponent;
  let fixture: ComponentFixture<SignUpFooterComponent>;

 beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpFooterComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
