import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Platform, ToastController, NavController, IonContent, AlertController } from '@ionic/angular';

import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { PersonalInformation } from '../../../core/models/SignUp';
import { Keyboard } from '@capacitor/keyboard';
import { Capacitor } from '@capacitor/core';
import { Router } from '@angular/router';
import { FirestoreCloudFunctionsService } from 'src/app/core/providers/firestore-cloud-functions/firestore-cloud-functions.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { SecureStorageService } from '@services/secure-storage/secure-storage.service';
import { SecureStorageKey } from '@services/secure-storage/secure-storage-key.enum';

@Component({
    selector: 'app-step-9',
    templateUrl: './step-9.page.html',
    styleUrls: ['./step-9.page.scss']
})
export class SignUpStepNinePage implements OnInit {
    @ViewChild(IonContent, {}) content: IonContent;

    
    referralForm: UntypedFormGroup;
    signProcessing = false;

    deactivateConfirmButton = true;

    private ngOnInitExecuted: any;
    private browser: InAppBrowserObject;

    public step = 1;
    public progress = this.step/9;
    
    constructor(
        // private camera: Camera,
        private nav: NavController,
        private cdr: ChangeDetectorRef,
        private toastController: ToastController,
        private translateProviderService: TranslateProviderService,
        public signService: SignProviderService,
        private router: Router,
        private iab: InAppBrowser,
        private safariViewController: SafariViewController,
        private firestoreCloudFunctionsService: FirestoreCloudFunctionsService,
        private alertController: AlertController,
        private secureStorage: SecureStorageService,
        private platform: Platform,
        private loader: LoaderProviderService
    ) {

    }


    async ionViewWillEnter() {
        this.content.scrollToTop();
        if (!this.ngOnInitExecuted) {
            await this.componentInit();
        }
    }

    onCodeChange(event) {
        this.deactivateConfirmButton = (event.length == 0);
    }

    async ngOnInit() {
        this.ngOnInitExecuted = true;
        await this.componentInit();
    }

    async ionViewDidLeave() {
        this.ngOnInitExecuted = false;
    }

    skipStep() {
        this.nav.navigateForward('/sign-up/step-1');
    }

    async nextStep() {
        if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
        this.referralForm.markAsDirty();
        this.signProcessing = true;   
        console.log(this.referralForm.value);

        await this.loader.loaderCreate();

        if(this.referralForm.value['usedReferralCode'] !== '') {

            const referralCodeValidityCheck = await this.firestoreCloudFunctionsService.checkReferralCodeValidity(this.referralForm.value['usedReferralCode']);
            console.log({ referralCodeValidityCheck });

            if(!referralCodeValidityCheck.success) {
                await this.loader.loaderDismiss();
                this.presentToast(referralCodeValidityCheck.error);


                const alerto = await this.alertController.create({
                    header: this.translateProviderService.instant('REFERRALS.INVALID'), // referralCodeValidityCheck.error,
                    message: this.translateProviderService.instant('REFERRALS.INVALIDBODY'),
                    buttons: [{
                        text: this.translateProviderService.instant('REFERRALS.BUTTONS.TRYAGAIN'),
                        handler: () => {}
                    // }, {
                    //     text: this.translateProviderService.instant('REFERRALS.BUTTONS.NOCODE'),
                    //     handler: () => {
                    //         this.nav.navigateForward('/sign-up/step-1');   
                    //     }
                    }]
                });
    
                await alerto.present();

                return;
            }
        
        } else {

            await this.loader.loaderDismiss();

            const alerto = await this.alertController.create({
                header: this.translateProviderService.instant('REFERRALS.MISSING'),
                message: this.translateProviderService.instant('REFERRALS.PROVIDE'),
                buttons: [{
                    text: this.translateProviderService.instant('REFERRALS.BUTTONS.TRYAGAIN'),
                    handler: () => {}
                // }, {
                //     text: this.translateProviderService.instant('REFERRALS.BUTTONS.NOCODE'),
                //     handler: () => {
                //         this.nav.navigateForward('/sign-up/step-1');   
                //     }
                }]
            });

            await alerto.present();

            return;

        }

        await this.loader.loaderDismiss();

        await this.secureStorage.setValue(SecureStorageKey.referralCode, this.referralForm.value['usedReferralCode']);

        this.nav.navigateForward('/sign-up/step-1');

    }

    async goToPreviousStep() {

        try {
            this.signService.signUpForm = {};
            this.signService.personalInformation = {};
            await this.secureStorage.removeValue(SecureStorageKey.sign, false); // registration form 1
            await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false); // registration form 2
            await this.secureStorage.removeValue(SecureStorageKey.loginCheck, false); // avoid disappearance of registration button
            await this.secureStorage.removeValue(SecureStorageKey.passwordHash, false); // avoid disappearance of registration button
            await this.secureStorage.removeValue(SecureStorageKey.nonce, false); // avoid disappearance of registration button
          } catch (e) {
              console.log(e);
          }
          this.nav.navigateBack('/login?from=registration');

        
    }

    async skipReg() {
        await this.signService.skipReg(9);
    }

    private async componentInit() {

        this.referralForm = new UntypedFormGroup({
            usedReferralCode: new UntypedFormControl('', []),
        });
        
        let code = await this.secureStorage.getValue(SecureStorageKey.referralCode, false);

        if(!code) {
            const url = new URL(window.location.href);
            code = url.searchParams.get('code');
        }

        if(code) {
            this.referralForm.controls.usedReferralCode.setValue(code);
        }
        
        setTimeout(() => this.detectChanges());
    }

    async presentToast(message: string) {

        const toast = await this.toastController.create({
            message, //: this.translateProviderService.instant('SIGNSTEPNINE.checkToast'),
            duration: 2000,
            position: 'top',
        });
        toast.present();
    }

    private detectChanges() {
        if (!this.cdr['destroyed']) {
            this.cdr.detectChanges();
        }
    }

}
