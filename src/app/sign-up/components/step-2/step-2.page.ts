import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Platform, ToastController, NavController, IonContent, IonInput } from '@ionic/angular';
import { SignUp } from '../../../core/models/SignUp';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
// import { Keyboard } from '@capacitor/keyboard';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { UserProviderService } from './../../../core/providers/user/user-provider.service';
import { ApiProviderService } from './../../../core/providers/api/api-provider.service';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';


@Component({
  selector: 'app-step-2',
  templateUrl: './step-2.page.html',
  styleUrls: ['./step-2.page.scss']
})
export class SignUpStepTwoPage implements OnInit {
  @ViewChild(IonContent, {}) content: IonContent;
  @ViewChild('usernameField') usernameField: IonInput;
  public displayName = 'Maxim';
  signForm: UntypedFormGroup;
  signProcessing = false;
  notUniqueUsername: boolean;
  private ngOnInitExecuted: any;
  public step = 3;
  public progress = this.step/9;

  public displayRequirements = {
    username: false
  };

  availableTitles: any[] = [
    {label: 'Dr', value: 'Dr.'},
    {label: 'Prof', value: 'Prof.'},
    {label: 'ProfDr', value: 'ProfDr.'},
    {label: 'none', value: ''},
  ];

  selectedLanguage = 'en';

  private browser: InAppBrowserObject;
  legalResponse = {
      termsandconditions: {
        en: 'https://jobgrader.app/app-termsofuse', // at settings and onboarding
        // de: 'https://jobgrader.app/app-nutzungsbedingungen'
      },
      dataprivacypolicy: {
        en: 'https://jobgrader.app/app-privacy-policy', // at settings and onboarding
        // de: 'https://jobgrader.app/app-datenschutzerklaerung'
      },
      gdpr:  'https://jobgrader.app/user-gdpr-compliance', // at onboarding
      thirdparty: {
        en: 'https://jobgrader.app/app-datause', // at onboarding
        // de: 'https://jobgrader.app/app-datennutzung'
      },
      imprint: {
        en: 'https://jobgrader.app/app-imprint',
        // de: 'https://jobgrader.app/app-impressum'
      }
  };
  browserOptions = 'zoom=no,footer=no,hideurlbar=yes,footercolor=#0021FF,hidenavigationbuttons=yes,presentationstyle=pagesheet';
  // browserOptions = 'footer=yes,hideurlbar=no,footercolor=#0021FF,hidenavigationbuttons=no,usewkwebview=yes,presentationstyle=pagesheet'
  // formsheet or fullscreen

  iabTermsConditions() {
  //   console.log(this.selectedLanguage);
    const url = !!this.legalResponse.termsandconditions[`${this.selectedLanguage}`] ? this.legalResponse.termsandconditions[`${this.selectedLanguage}`] : this.legalResponse.termsandconditions[`en`];
  //   console.log(url);
    if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
          this.showSafariInstance(url);
      } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system', this.browserOptions);
      } else if ( !this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system');
      }
  }

  iabDataPrivacy() {
    const url = !!this.legalResponse.dataprivacypolicy[`${this.selectedLanguage}`] ? this.legalResponse.dataprivacypolicy[`${this.selectedLanguage}`] : this.legalResponse.dataprivacypolicy[`en`];
    console.log(url);
    if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
          this.showSafariInstance(url);
      } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system', this.browserOptions);
      } else if ( !this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system');
      }
  }

  iabGdpr() {
      if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
          this.showSafariInstance(this.legalResponse.gdpr);
      } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
          this.browser = this.iab.create(this.legalResponse.gdpr, '_system', this.browserOptions);
      } else if ( !this.platform.is('hybrid') ) {
          this.browser = this.iab.create(this.legalResponse.gdpr, '_system', this.browserOptions);
      }
  }

  iabThirdParty() {
    const url = !!this.legalResponse.thirdparty[`${this.selectedLanguage}`] ? this.legalResponse.thirdparty[`${this.selectedLanguage}`] : this.legalResponse.thirdparty[`en`];
    console.log(url);
    if ( this.platform.is('ios') && this.platform.is('hybrid') ) {
          this.showSafariInstance(url);
      } else if ( this.platform.is('android') && this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system', this.browserOptions);
      } else if ( !this.platform.is('hybrid') ) {
          this.browser = this.iab.create(url, '_system');
      }
  }

  showSafariInstance(url: string) {
      this.safariViewController.isAvailable().then((available: boolean) => {
              if ( available ) {
                  this.safariViewController.show({
                      url,
                      hidden: false,
                      animated: true,
                      transition: 'slide',
                      enterReaderModeIfAvailable: false,
                      tintColor: '#0021FF'
                  })
                      .subscribe((result: any) => {
                          },
                          (error: any) => console.error(error));
              } else {
                  this.browser = this.iab.create(url, '_self', this.browserOptions);
              }
          }
      );
  }

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private signService: SignProviderService,
    private userProviderService: UserProviderService,
    private api: ApiProviderService,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private loader: LoaderProviderService,
    private platform: Platform,
    private iab: InAppBrowser,
    private safariViewController: SafariViewController
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  public onFocus() {
    console.log('onFocus');
  }

  ionViewDidEnter() {
    this.usernameField?.setFocus();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async nextStep() {
    // if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    this.signForm.markAsDirty();
    this.signProcessing = true;
    this.notUniqueUsername = false;

    if ((this.signForm.value.username).length < 8 || (this.signForm.value.username).length > 25) {
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.USERNAMELENGTH'));
      await this.loader.loaderDismiss();
      return false;
    }

    const nameIsValid = this.signForm.get('username').valid;
    if (!nameIsValid) {
      console.error('User name does not comply to our policies.');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.USERNAME'));
      await this.loader.loaderDismiss();
      return false;
    }

    this.notUniqueUsername = await this.api.checkUserName(this.signForm.value.username).then(res => res);
    if (!!this.notUniqueUsername) {
      console.error('Username in not unique.');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.NOT_UNIQUE'));
      await this.loader.loaderDismiss();
      return false;
    }

    if ( !this.signForm.valid ) {
      console.error('Terms not accepted!');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPNINE.checkToast'));
      await this.loader.loaderDismiss();
      return false;
    }


    await this.signService.saveSignData(this.signForm.value, 2);
    await this.userProviderService.storeUsername(this.signForm.value.username);
    this.nav.navigateForward('/sign-up/step-3');
    this.signProcessing = false;
  }

  goToPreviousStep(): void {
    this.nav.navigateBack('/sign-up/step-1');
  }

  async skipReg() {
    this.signForm.reset();
    await this.signService.skipReg(2);
  }

  private async componentInit() {
    this.signForm = new UntypedFormGroup({
      username: new UntypedFormControl('', [
        Validators.required,
        this.validateUserName
      ]),
      termsConditions: new UntypedFormControl(null, [
        Validators.requiredTrue
      ]),
      dataPrivacy: new UntypedFormControl(null, [
          Validators.requiredTrue
      ])
    });

    // mburger: at this point usernameField will be always undefined
    // so we should remove this, I set the focuson the ioniViewDidEnter
    // live cycle
    this.usernameField?.setFocus();

    this.selectedLanguage = await this.translateProviderService.getLangFromStorage();

    const dataFromStorage: SignUp = await this.signService.getSignData();

    this.setFormDataFromStorage(dataFromStorage);
    setTimeout(() => this.detectChanges());
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  private setFormDataFromStorage(data: SignUp): void {
    if (data && data.username) {
      this.signForm.controls.username.setValue(data.username);
    }
  }

  compareWith(o1, o2){
    return o1 == o2;
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  private validateUserName(formControl: UntypedFormControl) {

    const userNameString = formControl.value;
    const hasEmptySpaces = userNameString.match(/[\s]/);
    const hasSpecialCharacters = userNameString.match(/[\W\s]/);
    const hasBetweenMinAndMaxCharacters = userNameString.match(/^[\w]{8,25}$/);
    const hasBlockLetters = userNameString.match(/[A-Z]/);
    const hasSmallLetter = userNameString.match(/[a-z]/);
    const hasUnderscore = userNameString.match(/[_]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = userNameString.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasSpecialCharacters === null &&
      hasUnderscore === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters == null
      );

    return (isValid ? null : {
        validateUsername: {
          valid: false
        }
      }
    );
  }


}
