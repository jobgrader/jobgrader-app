import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { SignProviderService } from '../../../../core/providers/sign/sign-provider.service';
import { TimesService } from '../../../../core/providers/times/times.service';
import { TranslateProviderService } from '../../../../core/providers/translate/translate-provider.service';
import { PersonalInformation } from '../../../../core/models/SignUp';
import { DatePipe } from '@angular/common';

interface FormElements {
  number?: string;
  issuedate?: string;
  expirydate?: string;
  issuecountry?: string;
}

@Component({
  selector: 'app-document-selector',
  templateUrl: './document-selector.component.html',
  styleUrls: ['./document-selector.component.scss'],
})

export class DocumentSelectorComponent implements OnInit {
  @Input() data: any;
  constructor(
    private _ModalController: ModalController,
    private _SignProviderService: SignProviderService,
    private _TranslateProviderService: TranslateProviderService,
    private _ToastController: ToastController,
    public timesService: TimesService,
    ) {
      this.editForm = new UntypedFormGroup({
        number: new UntypedFormControl('', [Validators.required]),
        issuedate: new UntypedFormControl('', [Validators.required]),
        expirydate: new UntypedFormControl('', [Validators.required]),
        issuecountry: new UntypedFormControl('' ,[Validators.required]),
      })
     }

  public tooltipEvent: 'click' | 'press' | 'hover' = 'click';
  public duration: number = 3000;
  public topPosition: string = "top";

  public editForm: UntypedFormGroup;
  public saveObject: FormElements = {};
  public countryList = this._SignProviderService.countries;

  ngOnInit() {}

  close() {
    this._ModalController.dismiss()
  }

  edit(){
    // console.log(this.data);
    this.data.showMode = false;
    this.setFormDataFromStorage(this.fillModel((this.data.additionalData as FormElements)));
    // console.log(this.editForm);
  }

  cancel() {
    console.log('cancel clicked')
  }

  processCountryDisplay(countryCode: string) {
    let temp_country = this._SignProviderService.countries.find(d => d.code3 === countryCode);
    return (!!temp_country && !!countryCode) ? this._TranslateProviderService.instant(temp_country["countryTranslateKey"]) : countryCode;
  }

  private getFormattedDayForRequest(date: string): number {
    return +new Date(date);
  }
  private getFormattedDay(date: string): string {
    const datePipe = new DatePipe('en-US');

    return datePipe.transform(new Date(date), 'dd MMM yyyy');
  }

  async save(saveBool: boolean) {
    if ( saveBool && !this.editForm.valid ) {
      this.presentToast(this._TranslateProviderService.instant('SIGNSTEPNINE.fillToast'));
      return false;
    }
    
    if(saveBool) {
      if( (this.editForm.controls.number.value).length >=50 ) {
        await this.presentToast(this._TranslateProviderService.instant('PERSONALDETAILS.lengthError')
        + this._TranslateProviderService.instant('SIGNSTEPEIGHT.documentNumber')
        );
        // await this.loader.loaderDismiss();
        return false;
      }
      if (this.editForm.controls.issuedate.value && this.editForm.controls.expirydate.value &&
        this.getFormattedDayForRequest(this.getFormattedDay(this.editForm.controls.issuedate.value))
       > this.getFormattedDayForRequest(this.getFormattedDay(this.editForm.controls.expirydate.value))) {
      await this.presentToast(this._TranslateProviderService.instant('SIGNSTEPEIGHT.dateError'));
      // await this.loader.loaderDismiss();
      return false;
    }
    }
    
    var formData: any = {}
    var existingData = await this._SignProviderService.getPersonalData()
    if( this.data.documentType == 'PASSPORT' ) {
      formData.passportNumber = saveBool ? this.editForm.controls.number.value : ''
      formData.passportIssueDate = saveBool ? this.editForm.controls.issuedate.value : null
      formData.passportExpiryDate = saveBool ? this.editForm.controls.expirydate.value : null
      formData.passportIssueCountry = saveBool ? this.editForm.controls.issuecountry.value : ''
      
      formData.residencePermitNumber = existingData.residence_permit_number
      formData.residencePermitIssueDate = existingData.residence_permit_issue_date
      formData.residencePermitExpiryDate = existingData.residence_permit_expiry_date
      formData.residencePermitIssueCountry = existingData.residence_permit_issue_country
      
      formData.idCardNumber = existingData.idcard_number
      formData.idCardIssueDate = existingData.idcard_issue_date
      formData.idCardExpiryDate = existingData.idcard_expiry_date
      formData.idCardIssueCountry = existingData.idcard_issue_country

      formData.dlNumber = existingData.dl_number
      formData.dlIssueDate = existingData.dl_issue_date
      formData.dlExpiryDate = existingData.dl_expiry_date
      formData.dlIssueCountry = existingData.dl_issue_country
    }

    if( this.data.documentType == 'RESIDENCE_PERMIT') {
      formData.residencePermitNumber = saveBool ? this.editForm.controls.number.value : ''
      formData.residencePermitIssueDate = saveBool ? this.editForm.controls.issuedate.value : null
      formData.residencePermitExpiryDate = saveBool ? this.editForm.controls.expirydate.value : null
      formData.residencePermitIssueCountry = saveBool ? this.editForm.controls.issuecountry.value : ''

      formData.idCardNumber = existingData.idcard_number
      formData.idCardIssueDate = existingData.idcard_issue_date
      formData.idCardExpiryDate = existingData.idcard_expiry_date
      formData.idCardIssueCountry = existingData.idcard_issue_country

      formData.dlNumber = existingData.dl_number
      formData.dlIssueDate = existingData.dl_issue_date
      formData.dlExpiryDate = existingData.dl_expiry_date
      formData.dlIssueCountry = existingData.dl_issue_country

      formData.passportNumber = existingData.passport_number
      formData.passportIssueDate = existingData.passport_issue_date
      formData.passportExpiryDate = existingData.passport_expiry_date
      formData.passportIssueCountry = existingData.passport_issue_country
    }

    if( this.data.documentType == 'ID_CARD' ) {
      formData.idCardNumber = saveBool ? this.editForm.controls.number.value : ''
      formData.idCardIssueDate = saveBool ? this.editForm.controls.issuedate.value : null
      formData.idCardExpiryDate = saveBool ? this.editForm.controls.expirydate.value : null
      formData.idCardIssueCountry = saveBool ? this.editForm.controls.issuecountry.value : ''

      formData.dlNumber = existingData.dl_number
      formData.dlIssueDate = existingData.dl_issue_date
      formData.dlExpiryDate = existingData.dl_expiry_date
      formData.dlIssueCountry = existingData.dl_issue_country

      formData.passportNumber = existingData.passport_number
      formData.passportIssueDate = existingData.passport_issue_date
      formData.passportExpiryDate = existingData.passport_expiry_date
      formData.passportIssueCountry = existingData.passport_issue_country

      formData.residencePermitNumber = existingData.residence_permit_number
      formData.residencePermitIssueDate = existingData.residence_permit_issue_date
      formData.residencePermitExpiryDate = existingData.residence_permit_expiry_date
      formData.residencePermitIssueCountry = existingData.residence_permit_issue_country
    }
    
    if( this.data.documentType == 'DRIVERS_LICENSE' ) {
      formData.dlNumber = saveBool ? this.editForm.controls.number.value : ''
      formData.dlIssueDate = saveBool ? this.editForm.controls.issuedate.value : null
      formData.dlExpiryDate = saveBool ? this.editForm.controls.expirydate.value : null
      formData.dlIssueCountry = saveBool ? this.editForm.controls.issuecountry.value : ''
      
      formData.passportNumber = existingData.passport_number
      formData.passportIssueDate = existingData.passport_issue_date
      formData.passportExpiryDate = existingData.passport_expiry_date
      formData.passportIssueCountry = existingData.passport_issue_country

      formData.passportNumber = existingData.passport_number
      formData.passportIssueDate = existingData.passport_issue_date
      formData.passportExpiryDate = existingData.passport_expiry_date
      formData.passportIssueCountry = existingData.passport_issue_country

      formData.residencePermitNumber = existingData.residence_permit_number
      formData.residencePermitIssueDate = existingData.residence_permit_issue_date
      formData.residencePermitExpiryDate = existingData.residence_permit_expiry_date
      formData.residencePermitIssueCountry = existingData.residence_permit_issue_country
    }

    await this._SignProviderService.savePersonalData(formData, 8)
    this._ModalController.dismiss()
    
  }
  async presentToast(message: string) {
    const toast = await this._ToastController.create({
        message,
        duration: 2000,
        position: 'top',
    });
    toast.present();
}

  fillModel(model: FormElements) {
    try {
        model.number = this.data.number ? this.data.number : '';
        model.issuedate = this.data.issuedate ? (new Date(this.data.issuedate)).toISOString() : null;
        model.expirydate = this.data.expirydate ? (new Date(this.data.expirydate)).toISOString() : null;
        model.issuecountry = this.data.issuecountry ? this.data.issuecountry : '';
    } catch (e) {
        console.log(e);
    }
    return model
  }

  private setFormDataFromStorage(model: FormElements): void {
    if ( model && model.number ) { this.editForm.controls.number.setValue(model.number); }
    if ( model && model.issuedate ) { this.editForm.controls.issuedate.setValue(model.issuedate); }
    if ( model && model.expirydate ) { this.editForm.controls.expirydate.setValue(model.expirydate); }
    if ( model && model.issuecountry ) { this.editForm.controls.issuecountry.setValue(model.issuecountry); }
  }

  dateTimeManager1(event: any) {
    this.saveObject.issuedate = event.detail.value;
    this.editForm.controls.issuedate.setValue(this.saveObject.issuedate);
  }

  dateTimeManager2(event: any) {
    this.saveObject.expirydate = event.detail.value;
    this.editForm.controls.expirydate.setValue(this.saveObject.expirydate);
  }

  @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
        if ( !targetElement ) {
            return;
        }
        const ids = ['select-flag-6'];
        if ( ids.indexOf(targetElement.id) !== -1 ) {
            this._SignProviderService.addSelectBoxAdditionalElements();
        }
    }

}
