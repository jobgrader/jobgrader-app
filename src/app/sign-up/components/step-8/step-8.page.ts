import { ChangeDetectorRef, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController, ModalController, NavController, IonContent } from '@ionic/angular';
import { SignProviderService } from '../../../core/providers/sign/sign-provider.service';
import { PersonalInformation } from '../../../core/models/SignUp';
import { AuthenticationProviderService } from '../../../core/providers/authentication/authentication-provider.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { Keyboard } from '@capacitor/keyboard';
import { SecureStorageService } from '../../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../../core/providers/secure-storage/secure-storage-key.enum';
import { User } from '../../../core/models/User';
import { TimesService } from '../../../core/providers/times/times.service';
import { LoaderProviderService } from '../../../core/providers/loader/loader-provider.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { AlertsProviderService } from '../../../core/providers/alerts/alerts-provider.service';
import { DocumentSelectorComponent } from './document-selector/document-selector.component';
import { Capacitor } from '@capacitor/core';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { FirestoreCloudFunctionsService } from 'src/app/core/providers/firestore-cloud-functions/firestore-cloud-functions.service';
import { LanguageSelectorService } from '@services/language-selector/language-selector.service';

@Component({
  selector: 'app-step-8',
  templateUrl: './step-8.page.html',
  styleUrls: ['./step-8.page.scss']
})
export class SignUpStepEightPage implements OnInit {
  @ViewChild(IonContent, {}) content: IonContent;

  public source;

  public step = 8;
  public progress = this.step/9;

  signProcessing = false;
  private ngOnInitExecuted: any;

  public countrySelector = this.signService.countries.sort((a, b) => (this.translateProviderService.instant(a.countryTranslateKey) > this.translateProviderService.instant(b.countryTranslateKey) ? 1 : -1));
  
  public languageHeader = this.translateProviderService.instant('SIGNSTEPEIGHT.languageHeader');
  public nationalitiesHeader = this.translateProviderService.instant('SIGNSTEPEIGHT.nationalitiesHeader');
  
  additionalInformationForm: UntypedFormGroup;
  
  availableGenders = [
    'male',
    'female',
    'others'
  ];

  placeholders = {
    gender: this.translateProviderService.instant('SIGNSTEPEIGHT.PLACEHOLDER_GENDER'),
    nationalities: this.translateProviderService.instant('SIGNSTEPEIGHT.PLACEHOLDER_NATIONALITIES'),
    languages: this.translateProviderService.instant('SIGNSTEPEIGHT.PLACEHOLDER_LANGUAGES'),
    choose: this.translateProviderService.instant('SIGNSTEPEIGHT.CHOOSE')
  }

  public proficiencyLanguages = this.translateProviderService.returnAllLanguagesWithNativeNames();

  constructor(
    private nav: NavController,
    public timesService: TimesService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    public signService: SignProviderService,
    public authenticationProviderService: AuthenticationProviderService,
    public secureStore: SecureStorageService,
    private translateProviderService: TranslateProviderService,
    public apiProviderService: ApiProviderService,
    private toastController: ToastController,
    private loader: LoaderProviderService,
    private alertService: AlertsProviderService,
    private _ModalController: ModalController,
    private _NetworkService: NetworkService,
    private _FirestoreCloudFunctionsService: FirestoreCloudFunctionsService,
    private _LanguageSelector: LanguageSelectorService
  ) { }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }


  async nextStep() {
    await this.loader.loaderCreate();
    if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.hide();
    this.additionalInformationForm.markAsDirty();
    this.signProcessing = true;
    
    if (!this._NetworkService.checkConnection()) {
      await this.loader.loaderDismiss();
      this._NetworkService.addOfflineFooter('ion-footer');
      return; 
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    if(this.additionalInformationForm.controls.languageProficiencies.value.length > 10) {
      await this.loader.loaderDismiss();
      return this.presentToast(this.translateProviderService.instant('PERSONALDETAILS.languageLimit'));
    }

    if(this.additionalInformationForm.controls.nationalities.value.length > 3) {
      await this.loader.loaderDismiss();
      return this.presentToast(this.translateProviderService.instant('PERSONALDETAILS.nationalityLimit'));
    }
    
    
    this.signProcessing = false;
    console.log(this.additionalInformationForm.value);

    await this.secureStore.setValue(SecureStorageKey.additionalInfoReferral, JSON.stringify(this.additionalInformationForm.value));

    await this.loader.loaderDismiss();
    
    this.nav.navigateForward(`/sign-up/step-7`);

  }

  prevStep() {
    this.nav.navigateBack('/sign-up/step-6')    
  }

  async skipReg() {
    await this.signService.skipReg(8);
  }

  private async componentInit() {

    let checker = this.router.parseUrl(this.router.url).queryParams;
    this.source = checker.source;

    this.additionalInformationForm = new UntypedFormGroup({
      gender: new UntypedFormControl('', []),
      nationalities: new UntypedFormControl('', []),
      languageProficiencies: new UntypedFormControl('', []),
    });
    
    setTimeout(() => this.detectChanges());
  }

  async presentToast(message: string) {

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  @HostListener('document:click', ['$event', '$event.target'])
    public onClick(event: MouseEvent, targetElement: HTMLElement): void {
      console.log("Click registered");
        if ( !targetElement ) {
            return;
        }
        const ids = ['language-selection', 'language-selection-1', 'country-selection', 'country-selection-1'];
        console.log(targetElement.id);
        if ( ids.indexOf(targetElement.id) !== -1 ) {
            this._LanguageSelector.addSelectBoxAdditionalElements();
        }
    }


}
