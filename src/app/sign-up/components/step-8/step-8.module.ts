import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../../shared/shared.module';
import { SignUpStepEightPage } from './step-8.page';
import { DocumentSelectorPageModule } from './document-selector/document-selector.module';

const routes: Routes = [
  {
    path: '',
    component: SignUpStepEightPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    DocumentSelectorPageModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule,
  ],
  declarations: [SignUpStepEightPage]
})
export class SignUpStepEightPageModule {}
