import { Component, inject } from '@angular/core';
import { NavController } from '@ionic/angular';


// custom imports
import { LockService } from '../../../lock-screen/lock.service';


@Component({
  selector: 'app-create-refferal',
  templateUrl: './create-referral.page.html',
  styleUrls: ['./create-referral.page.scss']
})
export class SignUpCreateReferralPage {

  private nav = inject(NavController);
  private lockService = inject(LockService);


  constructor() { }


  async goToDashboard() {
    this.lockService.addResume();
    this.nav.navigateBack('/dashboard').then(() => {
      this.lockService.restart();
    });
  }

  async goToReferralsPage() {
    this.lockService.addResume();
    this.nav.navigateForward('/referrals?from=home').then(() => {
      this.lockService.restart();
    });
  }

}
