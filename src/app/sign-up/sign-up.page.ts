import { Component } from '@angular/core';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html'
})
export class SignUpPage {
  constructor(
    public _TranslateProviderService: TranslateProviderService
  ) {

  }

  async ionViewWillEnter() {
    this._TranslateProviderService.getLangFromStorage().then(lang => {
      this._TranslateProviderService.changeLanguage(lang);
    });
  }
}
