import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserActivitiesService, UserActivity } from '../core/providers/user-activities/user-activities.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-user-activities',
  templateUrl: './user-activities.page.html',
  styleUrls: ['./user-activities.page.scss'],
})
export class UserActivitiesPage implements OnInit {

  public activities: Array<UserActivity> = [];
  userImage;

  constructor(
    private nav: NavController,
    private _UserActivity: UserActivitiesService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();
    const activities = await this._UserActivity.getActivities();
    this.activities = activities.sort(function(a, b) {
      var keyA = new Date(a.timestamp),
        keyB = new Date(b.timestamp);
      
      if (keyA < keyB) return 1;
      if (keyA > keyB) return -1;
      return 0;
    });
  }

  goBackToSettings() {
    this.nav.navigateBack('/dashboard/tab-settings');
  }

}
