
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { WalletGenerationStepTwoOnePage } from './step-2-1.page';
import { KeyExportModule } from 'src/app/sign-up/components/step-5/key-export/key-export.module';

const routes: Routes = [
  {
    path: '',
    component: WalletGenerationStepTwoOnePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    KeyExportModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [WalletGenerationStepTwoOnePage]
})
export class WalletGenerationStepTwoOnePageModule {}
