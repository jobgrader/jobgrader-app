import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';
import { ethers } from 'ethers';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { SettingsService, Settings } from 'src/app/core/providers/settings/settings.service';
import { ActivityKeys, UserActivitiesService } from 'src/app/core/providers/user-activities/user-activities.service';

@Component({
    selector: 'app-wallet-generation-step-4-2',
    templateUrl: './step-4-2.page.html',
    styleUrls: ['./step-4-2.page.scss']
})

export class WalletGenerationStepFourTwoPage {

    constructor(
        private _NavController: NavController,
        private _Router: Router,
        private _ApiProviderService: ApiProviderService,
        private _SecureStorageService: SecureStorageService,
        private _ToastController: ToastController,
        private _SettingService: SettingsService,
        private _UserActivity: UserActivitiesService,
        private _Translate: TranslateProviderService
    ) {
    }

    private lang = 'en';
    private seedPhrase: string;
    private stage: string;
    public displayItems = [];
    public chosenItems = [];
    public heading: string = '';
    public data;

    ionViewWillEnter() {
        let checker = this._Router.parseUrl(this._Router.url).queryParams;
        this.stage = checker.stage;
        console.log(this.stage);

        this.heading = (this.stage == "1" || this.stage == "4") ? this._Translate.instant('WEB3WALLET.STEP4.verify-heading-1') : ((this.stage == "2" || this.stage == "5") ? this._Translate.instant('WEB3WALLET.STEP4.verify-heading-2') : this._Translate.instant('WEB3WALLET.STEP4.verify-heading'));

        this.data = checker.data;

        console.log(this.data);

        var check = CryptoJS.AES.decrypt(this.data, UDIDNonce.energy).toString(CryptoJS.enc.Utf8) ;

        if(this.stage == "4") {
            this.seedPhrase = JSON.parse(check as any).verified;
        } else if(this.stage == "5") {
            this.seedPhrase = JSON.parse(check as any).unverified;
        } else {
            this.seedPhrase = check;
        }

        this.displayItems = [];
        this.chosenItems = [];

        var f = this.seedPhrase.split(" ");
        var sh = [];

        for(let i=0; i<f.length; i++) {
            sh.push(f[i])
        }

        this.displayItems = this.shuffle(sh);
        console.log(sh);
    }

    shuffle(array) {
        let currentIndex = array.length,  randomIndex;
        while (currentIndex != 0) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex--;
          [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
        }
        return array;
      }

    deny() {
        // this._NavController.navigateBack('/wallet-generation/step-4-1');
        // if(this.stage != "4") {
            console.log("this.seedPhrase", this.seedPhrase);
            console.log("this.stage", this.stage);
            this._NavController.navigateBack(`/wallet-generation/step-4-1?stage=${this.stage}&data=${encodeURIComponent(this.data)}`);
        // } else {
        //     this._NavController.navigateBack(`/wallet-generation/step-4-1?stage=${this.stage}&data=${encodeURIComponent(CryptoJS.AES.encrypt(JSON.stringify({
        //         verified: generateMnemonic(),
        //         unverified: generateMnemonic()
        //     }), UDIDNonce.energy).toString())}`);
        // }
    }

    async confirm() {
        console.log(this.seedPhrase);
        var fff = this.chosenItems.join(' ');
        console.log(fff);
        var check = (fff == this.seedPhrase);
        console.log(check);
        if(check) {
            const wallet = ethers.Wallet.fromMnemonic(this.seedPhrase);

            const im = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
            const imW = !!im ? JSON.parse(im): [];

            if(this.stage == "1" || this.stage == "4") {    
                const existingVerifiedWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, this.seedPhrase);
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address);
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
                await this._ApiProviderService.saveUserconsent({
                    web3wallet_lostinfo: true,
                    web3wallet_exposeinfo: true,
                    web3wallet_secretinfo: true,
                    web3wallet_activationdate: +new Date()
                }).catch(e => {
                    console.log('Error with the saveUserconsent endpoint');
                    console.log(e);
                })
                if(!existingVerifiedWalletAddress){
                    await this._ApiProviderService.saveUserWeb3WalletAddress(wallet.address).catch(e => {
                        console.log('Error with the saveUserWeb3WalletAddress endpoint');
                        console.log(e);
                    });    
                } else {
                    await this._ApiProviderService.savePostUserWeb3WalletAddress(wallet.address).catch(e => {
                        console.log('Error with the saveUserWeb3WalletAddress endpoint');
                        console.log(e);
                    });
                }

                if(existingVerifiedWalletAddress) {
                    const colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
                    const random = Math.floor(Math.random() * colorschemes.length);
                    const color = colorschemes[random];
                    imW.push({
                        heading: 'Unverified Wallet',
                        mnemonic: null,
                        publicKey: existingVerifiedWalletAddress,
                        creationDate: +new Date(),
                        color
                    })
                    await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(imW));
                }
                
                await this._SettingService.set(Settings.paperBackupTimestamp, new Date().toISOString(), true);
                await this._UserActivity.updateActivity(ActivityKeys.VERIFIED_BACKUP_PAPER);

                this._NavController.navigateForward(`/wallet-generation/step-5?mode=paper&stage=${this.stage}`);
            
            } else if(this.stage == "2" || this.stage == "3" || this.stage == "5") {
                
                
                await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(imW));
                // if(this.stage == "2" || this.stage == "5"){
                    // await this._UserActivity.updateActivity(ActivityKeys.UNVERIFIED_BACKUP_PAPER, { address: this._ApiProviderService.obtainDisplayableAddress(wallet.address) });
                // }
                this._NavController.navigateForward(`/wallet-generation/step-5?mode=paper&stage=${this.stage}`); // status not required
            }


        } else {
            this.presentToast(this._Translate.instant('WEB3WALLET.STEP4.error-toast'));
            var f = this.seedPhrase.split(" ");
            var sh = [];

            for(let i=0; i<f.length; i++) {
                sh.push(f[i])
            }

            this.chosenItems = [];
            this.displayItems = this.shuffle(sh);
        }

    }

    presentToast(message: string) {
        this._ToastController.create({
            duration: 2000,
            position: 'top',
            message
        }).then(toast => {
            toast.present();
        })
    }

    setWord(gg: string) {
        this.displayItems.splice(this.displayItems.indexOf(gg), 1);
        this.chosenItems.push(gg);
    }

    deleteWord(gg: string) {
        this.chosenItems.splice(this.chosenItems.indexOf(gg), 1);
        this.displayItems.push(gg);
    }

}