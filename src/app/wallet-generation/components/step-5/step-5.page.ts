import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';


@Component({
    selector: 'app-wallet-generation-step-5',
    templateUrl: './step-5.page.html',
    styleUrls: ['./step-5.page.scss']
})

export class WalletGenerationStepFivePage {

    public icon = "../../../../assets/icon/sign-success.svg";

    public customContent1 = '';
    public customContent2 = '';
    public customContent3 = '';
    public customContent4 = '';

    public stage;
    public displayEmoji = false;

    constructor(
        private _NavController: NavController,
        private _Router: Router,
        private _Translate: TranslateProviderService,
    ) {
    }

    public ionViewWillEnter() {
        let checker = this._Router.parseUrl(this._Router.url).queryParams;
        console.log(checker);
        this.stage = checker.stage;

        if(checker.mode) {
            if(checker.status) {
                if(checker.status == "local") {
                    this.displayEmoji = true;
                    this.customContent1 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-1-local`);
                    this.customContent2 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-2-local`);
                    this.customContent3 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-3-local`);
                } else {
                    this.customContent1 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-1`);
                    this.customContent2 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-2`);
                    this.customContent3 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-3`);
                }
            } else {
                this.customContent1 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-1`);
                this.customContent2 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-2`);
                this.customContent3 = this._Translate.instant(`WEB3WALLET.STEP5.${checker.mode}-${this.stage}-3`);
            }
        }

    }

    deny() {

    }

    confirm() {
        if(this.stage == "1") {
            this._NavController.navigateForward('/wallet-generation/step-3?stage=2');
        } if(this.stage == "3") {
            this._NavController.navigateForward('/dashboard/payments');
        } else {
            this._NavController.navigateForward('/dashboard/tab-home');
        }

    }

}