import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { FilebackupDisclaimerComponent } from './filebackup-disclaimer.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
    ],
  declarations: [ FilebackupDisclaimerComponent ],
//   entryComponents: [ FilebackupDisclaimerComponent ],
  exports: [ FilebackupDisclaimerComponent ]
})
export class FilebackupDisclaimerModule {}
