import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wallet-generation',
  templateUrl: './wallet-generation.page.html',
})
export class WalletGenerationPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  cancel() {
    console.log('cancel');
  }

  deny() {
    console.log('deny');
  }

  confirm() {
    console.log('confirm');
  }

}
