import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { JobHeaderComponent } from './job-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule.forChild(),
  ],
  providers: [

  ],
  declarations: [JobHeaderComponent],
//   entryComponents: [DocumentModalPageComponent],
  exports: [JobHeaderComponent]
})
export class JobHeaderModule {}
