import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { RecoveryPhraseHelperComponent } from './recovery-phrase-helper.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CodeInputModule } from 'angular-code-input';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';

@NgModule({
    imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    CodeInputModule,
    TranslateModule.forChild()
  ],
  providers: [SocialSharing, File],
  declarations: [RecoveryPhraseHelperComponent],
//   entryComponents: [RecoveryPhraseHelperComponent],
  exports: [RecoveryPhraseHelperComponent]
})
export class RecoveryPhraseHelperModule {}
