import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AlertController, Platform, ToastController, NavController, ModalController } from '@ionic/angular';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { FingerprintAIO, FingerprintOptions } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { InAppBrowser, InAppBrowserObject } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { LockService } from '../lock-screen/lock.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { Settings, SettingsService } from '../core/providers/settings/settings.service';
// import { ChatService } from '../core/providers/chat/chat.service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { EventsService, EventsList } from '../core/providers/events/events.service';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { RecoveryPhraseHelperComponent } from './recovery-phrase-helper/recovery-phrase-helper.component';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from '../core/providers/device/udid.enum';
import { fiatCurrencies } from '../core/providers/nft/fiat-currencies';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Browser } from '@capacitor/browser';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { LoaderProviderService } from '@services/loader/loader-provider.service';
import { BiometricService } from '@services/biometric/biometric.service';

@Component({
  selector: 'app-tab-settings',
  templateUrl: './tab-settings.page.html',
  styleUrls: ['./tab-settings.page.scss'],
})
export class TabSettingsPage implements OnInit {

  selectedLanguage: string;
  selectedCurrency: string;
  avaliableLangs: string[] = [];
  currencyList: any[] = fiatCurrencies;

  public selectedTheme: string;

  private ngOnInitExecuted: any;
  public lastActionsAvailable = false;

  // fingerprintOptions: FingerprintOptions = environment.fingerprintOptions;
  private browser: InAppBrowserObject;
  legalResponse = {
    termsandconditions: {
      en: 'https://jobgrader.app/app-termsofuse',
      // de: 'https://jobgrader.app/app-nutzungsbedingungen'
    },
    dataprivacypolicy: {
      en: 'https://jobgrader.app/app-privacy-policy',
      // de: 'https://jobgrader.app/app-datenschutzerklaerung'
    },
    gdpr: 'https://jobgrader.app/user-gdpr-compliance',
    thirdparty: {
      en: 'https://jobgrader.app/app-datause',
      // de: 'https://jobgrader.app/app-datennutzung'
    },
    imprint: {
      en: 'https://jobgrader.app/app-imprint',
      // de: 'https://jobgrader.app/app-impressum'
    }
  };
  helpPages = {
    en: 'https://jobgrader.app/app-faq',
    // de: 'https://jobgrader.app/app-faq'
  }
  openSourceLibraries = {
    en: 'https://jobgrader.app/app-open-source-license',
    // de: 'https://jobgrader.app/app-open-source-lizenz'
  }
  web3Links = {
    en: 'https://jobgrader.app/agb-web3-wallet-en',
    // de: 'https://jobgrader.app/agb-web3-wallet-de'
  }
  userImage;
  browserOptions = '';

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private authenticationProviderService: AuthenticationProviderService,
    private translateProviderService: TranslateProviderService,
    private alertController: AlertController,
    private apiProviderService: ApiProviderService,
    public appStateService: AppStateService,
    private toastController: ToastController,
    private faio: FingerprintAIO,
    private safariViewController: SafariViewController,
    private iab: InAppBrowser,
    private platform: Platform,
    private lockService: LockService,
    private secureStorage: SecureStorageService,
    private cryptoProviderService: CryptoProviderService,
    private settingsService: SettingsService,
    public themeSwitcher: ThemeSwitcherService,
    // public _ChatService: ChatService,
    private _EventsService: EventsService,
    private _ModalController: ModalController,
    private _Chooser: Chooser,
    private _File: File,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private loader: LoaderProviderService,
    private biometric: BiometricService,
    private http: HttpClient
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
    if (this.appStateService.basicAuthToken) {
      this.userImage = this.userPhotoServiceAkita.getPhoto();
    }
    this.browserOptions = 'footer=yes,hideurlbar=no,hidenavigationbuttons=no,presentationstyle=pagesheet,location=yes';

    let currentTheme = this.themeSwitcher.getCurrentTheme();

    if (currentTheme === 'dark') {
      this.browserOptions = this.browserOptions
        + 'footercolor=#222528'
        + 'navigationbuttoncolor=#d1d2d1'
        + 'toolbarcolor=#222528'
        + 'closebuttoncolor=#d1d2d1';
    }
    else {
      this.browserOptions = this.browserOptions
        + 'footercolor=#BF7B54'
        + 'navigationbuttoncolor=#000000'
        + 'toolbarcolor=#f7f7f7'
        + 'closebuttoncolor=#000000';
    }
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async languageChanged() {
    await this.translateProviderService.changeLanguage(this.selectedLanguage);
    await this.settingsService.set(Settings.language, this.selectedLanguage, true);
    this._EventsService.publish(EventsList.languageChange, this.selectedLanguage);
    // this._ChatService.setLocale();
  }

  async themeChanged() {
    console.info(this.selectedTheme);
    this.themeSwitcher.setTheme(this.selectedTheme);
    this._EventsService.publish(EventsList.themeChange, this.selectedTheme);
  }

  async currencyChanged() {
    this.secureStorage.setValue(SecureStorageKey.currency, this.selectedCurrency);
  }

  async logout() {
    await this.authenticationProviderService.logout();
    await this.authenticationProviderService.cleanForLogout(this.cryptoProviderService);
    this.nav.navigateRoot('/login');
  }

  private async componentInit() {
    this.selectedLanguage = await this.translateProviderService.getLangFromStorage();
    this.selectedTheme = await this.themeSwitcher.getSavedTheme();
    this.avaliableLangs = this.translateProviderService.returnAppSupportedLanguages();

    var selectedCurrency = await this.secureStorage.getValue(SecureStorageKey.currency, false);
    this.selectedCurrency = !!selectedCurrency ? selectedCurrency : "EUR";

    if (!this.appStateService.basicAuthToken) {
      this.apiProviderService.noUserLoggedInAlert();
      // this._AuthenticationProviderService.displayLoginOptionIfNecessary();
    }

    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  async confirmLogout() {
    const alert = await this.alertController.create({
      mode: 'ios',
      header: this.translateProviderService.instant('SETTINGS.logout'),
      message: this.translateProviderService.instant('SETTINGS.confirm'),
      buttons: [
        {
          text: this.translateProviderService.instant('SETTINGS.no'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Logout cancelled');
          }
        }, {
          text: this.translateProviderService.instant('SETTINGS.yes'),
          cssClass: 'primary',
          handler: () => {
            console.log('Logout confirmed');
            this.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  goToImprint() {
    this.iabCreate(!!this.legalResponse.imprint[`${this.selectedLanguage}`] ?
      this.legalResponse.imprint[`${this.selectedLanguage}`] :
      this.legalResponse.imprint[`en`]
    );
  }
  goToTerms() {
    this.iabCreate(!!this.legalResponse.termsandconditions[`${this.selectedLanguage}`] ?
      this.legalResponse.termsandconditions[`${this.selectedLanguage}`] :
      this.legalResponse.termsandconditions[`en`]
    );
  }
  goToPrivacy() {
    this.iabCreate(!!this.legalResponse.dataprivacypolicy[`${this.selectedLanguage}`] ?
      this.legalResponse.dataprivacypolicy[`${this.selectedLanguage}`] :
      this.legalResponse.dataprivacypolicy[`en`]
    );
  }
  goToHelpPage() {
    this.iabCreate(!!this.helpPages[`${this.selectedLanguage}`] ?
      this.helpPages[`${this.selectedLanguage}`] :
      this.helpPages[`en`]
    );
  }
  goToOpenSourceLibraries() {
    this.iabCreate("https://jobgrader.gitlab.io/jobgrader-app/open-source-license.html");
  }
  goToWEB3() {
    this.iabCreate(!!this.web3Links[`${this.selectedLanguage}`] ?
      this.web3Links[`${this.selectedLanguage}`] :
      this.web3Links[`en`]
    );
  }

  /** Callback for changing the page to security */
  public goToSecurity(): void {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/security') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }

  public goToConnectedAccounts() {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/connected-accounts') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }

  goToNotifications() {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/notifications') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }
  goToDevices() {
    console.log("Open Devices");
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/settings-devices') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }
  goToAccount() {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/settings-account') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }
  goToActivities() {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/user-activities') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }
  goToReferrals() {
    (this.appStateService.isAuthorized ?
      this.nav.navigateForward('/referrals') :
      this.apiProviderService.noUserLoggedInAlert()
    );
  }
  goToAbout() {
    this.nav.navigateForward('/app-overview');
  }

  iabCreate(link: string) {
    if (this.platform.is('ios') && this.platform.is('hybrid')) {
      this.showSafariInstance(link);
      // Browser.open({ url: link, toolbarColor: '#54BF7B', presentationStyle: 'popover' });

    } else if (this.platform.is('android') && this.platform.is('hybrid')) {
      this.browser = this.iab.create(link, '_system', this.browserOptions);
      // Browser.open({ url: link, toolbarColor: '#54BF7B' });

    } else if (!this.platform.is('hybrid')) {
      this.browser = this.iab.create(link, '_system');
    }
  }

  showSafariInstance(url: string) {
    this.safariViewController.isAvailable().then((available: boolean) => {
      if (available) {
        this.safariViewController.show({
          url,
          hidden: false,
          animated: true,
          transition: 'slide',
          enterReaderModeIfAvailable: false,
          tintColor: '#54BF7B'
        })
          .subscribe((result: any) => { },
            (error: any) => console.error(error));
      } else {
        this.browser = this.iab.create(url, '_self', this.browserOptions);
      }
    }
    );
  }

  public goToFeedback(): void {
    this.nav.navigateForward('/feedback');
  }


  goToChangePassword() {

    const url = '/changepassword';
    (this.appStateService.isAuthorized ?
      this.biometric.biometricAvailable$().subscribe({
        next: (value) => {
          if (value) {
            this.biometric.biometricShow().subscribe({
              next: (v) => {
                console.log({ v });
                this.nav.navigateForward(url);
              }
            })
          } else {
            this.nav.navigateForward(url);
          }
        },
        error: (e) => {
          console.log({ e });
          this.nav.navigateForward(url);
        }
      })
      :
      this.apiProviderService.noUserLoggedInAlert());
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  walletRecoveryPhraseHelper() {
    console.log("walletRecoveryPhraseHelper");
    this.cryptoProviderService.returnBackupSymmetricKey().then(backupSymmetricKey => {

      console.log(backupSymmetricKey);

      const modalPresent = (mnemonic: string) => {

        var d1 = CryptoJS.AES.decrypt(JSON.parse(mnemonic), UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);

        console.log(d1);

        try {
          var d3 = JSON.parse(d1);
        } catch (e) {
          d3 = d1;
        }

        try {
          var d2 = CryptoJS.AES.decrypt(d3, backupSymmetricKey).toString(CryptoJS.enc.Utf8);
          console.log(d2);
        } catch (e) {
          d2 = d3;
        }

        try {
          var d4 = JSON.parse(d2);
        } catch (e) {
          d4 = d2;
        }

        console.log(d4.length);

        if (d4.length == 0) {
          this.presentToast(this.translateProviderService.instant('IMPORTKEY.invalid-file'));
        } else {
          this._ModalController.create({
            component: RecoveryPhraseHelperComponent,
            componentProps: {
              data: d4,
              isMnemonic: (d4.split(" ").length > 1)
            }
          }).then(modal => {
            modal.onDidDismiss().then(() => {

            })
            modal.present();
          })
        }


      }

      this.alertController.create({
        mode: 'ios',
        message: this.translateProviderService.instant('EXPORTKEY.password-alert'),
        inputs: [{
          type: 'password',
          placeholder: "...",
          name: 'password'
        }],
        buttons: [
          {
            text: this.translateProviderService.instant('BUTTON.CANCEL'),
            role: 'cancel',
            handler: () => {

            }
          },
          {
            text: this.translateProviderService.instant('GENERAL.ok'),
            handler: async (data) => {
              var plaintextPassword = data.password;
              var passwordHash = await this.secureStorage.getValue(SecureStorageKey.passwordHash, false);
              var nonce = await this.secureStorage.getValue(SecureStorageKey.nonce, false);
              if (this.cryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {


                this.alertController.create({
                  mode: 'ios',
                  header: this.translateProviderService.instant('WALLETSETTINGS.file-backup'),
                  message: this.translateProviderService.instant('WALLETSETTINGS.upload-file'),
                  buttons: [{
                    text: this.translateProviderService.instant('WALLETSETTINGS.select-file'),
                    cssClass: 'recovery-blue',
                    handler: () => {
                      if (this.platform.is('hybrid')) {

                        this.lockService.removeResume();

                        this._Chooser.getFile({ mimeTypes: "application/json" }).then(async (file) => {
                          console.log(file);
                          if (!file) {
                            this.presentToast(this.translateProviderService.instant('IMPORTKEY.nofile'));
                            return;
                          }

                          if (!!(file as any).dataURI) {
                            var data = (file as any).dataURI.toString().replace("data:application/json;base64,", "");
                            var d = window.atob(data);
                          } else {
                            try {
                              data = await this._File.readAsArrayBuffer(file.path, file.name);
                              d = new TextDecoder().decode(data);
                            } catch (e) {
                              data = await this._File.readAsArrayBuffer((file as any).uri.replace(file.name, ""), file.name);
                              d = new TextDecoder().decode(data);
                            }
                          }

                          modalPresent(d);

                        }).catch(e => {
                          console.log(e);
                        })
                      } else {
                        var a = document.createElement("input");
                        a.type = "file";
                        a.style.display = "none";

                        a.onchange = ((fileData) => {
                          console.log(a.files);
                          console.log(fileData);
                          var reader = new FileReader();
                          reader.onload = async (e) => {
                            var t = e.target.result;

                            if (t.toString().indexOf("data:application/json;base64,") == -1) {
                              this.presentToast(this.translateProviderService.instant('IMPORTKEY.invalid-format'));
                              return;
                            }

                            var tt = window.atob(t.toString().replace("data:application/json;base64,", ""));

                            console.log(tt);

                            modalPresent(tt);

                          }

                          reader.readAsDataURL(a.files[0]);

                        })
                        a.click();
                        a.remove();

                      }
                    }
                  }]
                }).then(alertoo => {
                  alertoo.present();
                })

              } else {
                this.presentToast(this.translateProviderService.instant('EXPORTKEY.incorrect-password'));
              }
            }
          }
        ]
      }).then(alerti => alerti.present())



    })
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  async goToInAppBrowser() {
    this.nav.navigateForward('/veriff-thanks');
    return;
    var alerto = await this.alertController.create({
      mode: 'ios',
      header: 'In-App Browser Testing',
      message: 'Please enter the URL to open',
      inputs: [{ type: 'url', name: 'url', placeholder: 'https://...' }],
      buttons: [
        {
          text: 'Ok',
          handler: (data) => {
            var link = data.url;
            if (this.platform.is('ios') && this.platform.is('hybrid')) {
              Browser.open({ url: link, toolbarColor: '#54BF7B', presentationStyle: 'popover' });
            } else if (this.platform.is('android') && this.platform.is('hybrid')) {
              Browser.open({ url: link, toolbarColor: '#54BF7B' });
            } else if (!this.platform.is('hybrid')) {
              this.browser = this.iab.create(link, '_system');
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        }
      ]
    })
    await alerto.present();
  }

  socialsX() {
    this.iab.create(environment.socials.x, '_system');
  }

  socialsLinkedin() {
    this.iab.create(environment.socials.linkedin, '_system');
  }

  socialsTelegram() {
    this.iab.create(environment.socials.telegram, '_system');
  }

  async socialsDiscord() {
    await this.loader.loaderCreate();
    this.http.get(environment.firestore.endpoints.jobgraderInviteUrl).subscribe({
      next: async (d: any) => {
        await this.loader.loaderDismiss();
        this.iab.create(d.inviteUrl, '_system');
      },
      error: async (e) => {
        console.log(e);
        await this.loader.loaderDismiss();
        this.iab.create(environment.socials.discord, '_system');
      }
    })
  }

}
