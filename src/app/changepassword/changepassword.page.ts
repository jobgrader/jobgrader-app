import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { NetworkService } from '../core/providers/network/network-service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { PasswordStrengthInfo } from '../core/providers/user';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { zxcvbn } from '@utilities/zxcvbn';


@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  public displayRequirements = {
    username: false,
    password1: false,
    password2: false,
  };
  userImage;
  signForm: UntypedFormGroup;
  verificationStatus: number;
  notValidForm = false;
  signProcessing = false;
  ngOnInitExecuted: any;

  public passwordStrengthInfo: PasswordStrengthInfo = {
    password0Type: "password",
    password1Strength: 0,
    password1StrengthText: "",
    password1Type: "password",
    password2Strength: 0,
    password2StrengthText: "",
    password2Type: "password",
    okButtonEnabled: false,
    okButtonClass: 'save-button-disabled'
  };

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private toastController: ToastController,
    private translateProviderService: TranslateProviderService,
    private loader: LoaderProviderService,
    private secureStorage: SecureStorageService,
    private _NetworkService: NetworkService,
    private cryptoProviderService: CryptoProviderService,
    private apiProviderService: ApiProviderService,
    private appStateService: AppStateService,
    private authenticationProviderService: AuthenticationProviderService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** Go back callback */
  public goBack(): void {
    this.nav.navigateBack('/dashboard/tab-settings');
  }

  async componentInit() {
    this.signForm = new UntypedFormGroup({
      currentPassword: new UntypedFormControl('', [
        Validators.required
      ]),
      password: new UntypedFormControl('', [
        Validators.required,
        this.validatePassword
      ]),
      passwordConfirm: new UntypedFormControl('', [
        Validators.required,
        this.validatePassword
      ])
    }, {
      validators: this.passwordConfirming
    });

    this.userImage = this.userPhotoServiceAkita.getPhoto();

    this.checkPassword1Strength();
    this.checkPassword2Strength();
    setTimeout(() => this.detectChanges());
  }

  private validatePassword(formControl: UntypedFormControl) {
    const password = formControl.value;
    const hasEmptySpaces = password.match(/[\s]/);
    const hasBetweenMinAndMaxCharacters = password.match(/^.{8,25}$/);
    const hasBlockLetters = password.match(/[A-Z]/);
    const hasSmallLetter = password.match(/[a-z]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = password.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters == null
    );

    return (
      isValid
      ? null : {
        validatePassword: {
          valid: false
        }
      }
    );
  }

  checkPassword1Strength() {
    var control = this.signForm.controls.password;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password1Strength = score;
    this.getPassword1StrengthClass();
  }

  checkPassword2Strength() {
    var control = this.signForm.controls.passwordConfirm;
    var score = zxcvbn(control.value).score;
    // if we don't confirm to our own validation, override the score to zero.
    score = (control.valid) ? score : 0;
    this.passwordStrengthInfo.password2Strength = score;
    this.getPassword2StrengthClass();
  }

  getPassword1StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password1Strength,
      lbl: this.passwordStrengthInfo.password1StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, false);
    this.passwordStrengthInfo.password1StrengthText = obj.lbl;
    return cssClass;
  }

  getPassword2StrengthClass() {
    var obj = {
      val: this.passwordStrengthInfo.password2Strength,
      lbl: this.passwordStrengthInfo.password2StrengthText
    };

    var cssClass = this.getPasswordStrengthClass(obj, true);
    this.passwordStrengthInfo.password2StrengthText = obj.lbl;
    return cssClass;
  }

  getPasswordStrengthClass(obj: any, overrideLabels: boolean) {
    var className: string;
    var prefix: string = this.translateProviderService.instant('PASSWORDSTRENGTH.strength') + ": ";
    switch(obj.val) {
      case 0: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.na'); className = "red"; break;
      case 1: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.weak'); className = "red"; break;
      case 2: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.good'); className = "green"; break;
      case 3: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.great'); className = "green"; break;
      case 4: obj.lbl = prefix + this.translateProviderService.instant('PASSWORDSTRENGTH.superb'); className = "green"; break;
    }

    var pw2Empty = (this.signForm.value.passwordConfirm.trim().length == 0);
    var matching = (this.signForm.value.password === this.signForm.value.passwordConfirm);

    if (overrideLabels) {
      className = (matching) ? "green" : "red";
      if (this.signForm.value.passwordConfirm.trim().length == 0) {
        obj.lbl = null;
      }
      else {
        obj.lbl = (matching) ?
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_match') :
          this.translateProviderService.instant('PASSWORDSTRENGTH.passwords_do_not_match');
      }
    }

    this.passwordStrengthInfo.okButtonEnabled = (!pw2Empty && matching);
    this.passwordStrengthInfo.okButtonClass = (this.passwordStrengthInfo.okButtonEnabled) ? '' : 'save-button-disabled';

    return className;
  }

  togglePassword0Visibility() {
    this.passwordStrengthInfo.password0Type = (this.passwordStrengthInfo.password0Type == 'text') ? 'password' : 'text';
  }

  togglePassword1Visibility() {
    this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
  }

  togglePassword2Visibility() {
    this.passwordStrengthInfo.password2Type = (this.passwordStrengthInfo.password2Type == 'text') ? 'password' : 'text';
  }

  passwordConfirming() {
    return (formGroup: UntypedFormGroup) => {
      const control = formGroup.controls.password;
      const matchingControl = formGroup.controls.passwordConfirm;

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          console.log('passwordConfirmation failed');
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }

      console.log('passwordConfirmation succeeded');
    };
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

  async presentToast(message: string) {
    var toast = await this.toastController.create({ message, duration: 3000, position: 'top' });
    toast.present();
  }

  async save() {
    if (!this.passwordStrengthInfo.okButtonEnabled) {
      return;
    }

    if (!this._NetworkService.checkConnection()) {
      await this.loader.loaderDismiss();
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.signForm.markAsDirty();
    this.signProcessing = true;

    if (this.signForm.value.password === '' || !this.signForm.value.password) {
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.REQUIREMENTS.FILLALL'));
      await this.loader.loaderDismiss();
      return false;
    }

    const passwordIsValid = this.signForm.get('password').valid;
    if (!passwordIsValid) {
      console.error('Password does not comply to our policies.');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.PASSWORD'));
      await this.loader.loaderDismiss();
      return false;
    }

    if (this.signForm.value.password !== this.signForm.value.passwordConfirm) {
      console.error('Entered passwords do not match.');
      await this.presentToast(this.translateProviderService.instant('SIGNSTEPONE.ERROR.MISMATCH'));
      await this.loader.loaderDismiss();
      return false;
    }

    if (this.passwordStrengthInfo.password1Strength < 2) {
      console.error('Password is too weak.');
      await this.presentToast(this.translateProviderService.instant('PASSWORDSTRENGTH.error_too_weak'));
      await this.loader.loaderDismiss();
      return false;
    }

    // If we're here - awesome, save.
    this.secureStorage.getValue(SecureStorageKey.userName, false).then(username => {
      this.finishChangePassword(username);
    });
  }

  async finishChangePassword(username: string) {
    var currentPw = this.signForm.value.currentPassword;
    var newPw = this.signForm.value.password;

    const requestBody = {
      username,
      password: await this.cryptoProviderService.encryptPasswordAsBase64(currentPw),
      newPassword: await this.cryptoProviderService.encryptPasswordAsBase64(newPw)
    };

    this.apiProviderService.changePassword(requestBody).then(async (response) => {
      if (!response.errorFound) {
        await this.loader.loaderCreate();
        this.presentToast(this.translateProviderService.instant('LOADER.passwordChangeSuccessful'));
        await this.authenticationProviderService.resetBasicAuthToken().then(async (res) => {
          if (res) {
            await this.authenticationProviderService.setBasicAuthToken(username, newPw).then(async (res) => {
              if (res) {
                this.signProcessing = false;
                await this.loader.loaderDismiss();
                await this.authenticationProviderService.logout();
                await this.authenticationProviderService.cleanForLogout(this.cryptoProviderService);
                this.nav.navigateRoot('/login');
              }
            });
          }
        });
      }
      else {
        this.showFailed();
      }
    }).catch(e => {
      this.showFailed(e);
    });
  }

  showFailed(e?: any) {
    if (e) { console.log(e); }
    this.signProcessing = false;
    this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
  }

}
