import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { DocumentModalPageComponent } from './document-modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';

@NgModule({
  imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  providers: [
    SafariViewController,
    SocialSharing,
    File,
    FileOpener
  ],
  declarations: [DocumentModalPageComponent],
//   entryComponents: [DocumentModalPageComponent],
  exports: [DocumentModalPageComponent]
})
export class DocumentModalPageModule {}
