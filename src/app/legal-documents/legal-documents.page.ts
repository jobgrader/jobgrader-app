import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { take } from 'rxjs/operators';
import { User } from '../core/models/User';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { SignProviderService } from '../core/providers/sign/sign-provider.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { StorageKeys, StorageProviderService } from '../core/providers/storage/storage-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { KycmediaService } from '../core/providers/kycmedia/kycmedia.service';
import { KycService } from '../kyc/services/kyc.service';
import { LockService } from '../lock-screen/lock.service';
import { DocumentModalPageComponent } from './document-modal/document-modal.component';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { NetworkService } from '../core/providers/network/network-service';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { KycMedia } from '../core/providers/state/kyc-media/kyc-media.model';
import { KycMediaServiceAkita } from '../core/providers/state/kyc-media/kyc-media.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

interface DocumentDisplay {
  passport?: boolean;
  residencepermit?: boolean;
  idcard?: boolean;
  driverlicence?: boolean;
}

interface VcDisplay {
  passport?: Array<any>;
  residencepermit?: Array<any>;
  idcard?: Array<any>;
  driverlicence?: Array<any>;
}

interface DocumentModalObject {
  header?: string;
  documentType?: string;
  media?: any;
  number?: string;
  numberStatus?: number;
  issuedate?: number;
  issuedateStatus?: number;
  expirydate?: number;
  expirydateStatus?: number;
  issuecountry?: string;
  issuecountryStatus?: string;
  vc?: Array<any>;
}

@Component({
  selector: 'app-legal-documents',
  templateUrl: './legal-documents.page.html',
  styleUrls: ['./legal-documents.page.scss'],
})
export class LegalDocumentsPage {

  public profilePictureSrc = '../../assets/job-user.svg';
  public isInnovator = false;
  public user: User;
  public duration: number = 3000;
  public countryList = this.signService.countries;
  public displayVerifiedTick = false;

  displayLoadingDots = false;

  public documentDisplay: DocumentDisplay = {};
  public vcDisplay: VcDisplay = {
    passport: [],
    idcard: [],
    driverlicence: [],
    residencepermit: []
  };
  public tooltipEvent: 'click' | 'press' | 'hover' = 'click';

  private passportMedia: Array<any> = [];
  private idCardMedia: Array<any> = [];
  private residencePermitMedia: Array<any> = [];
  private dlMedia: Array<any> = [];

  public allVerified = {
    idcard: false,
    driverlicence: false,
    passport: false,
    residencepermit: false
  }

  constructor(
    private nav: NavController,
    private appStateService: AppStateService,
    private apiProviderService: ApiProviderService,
    private userProviderService: UserProviderService,
    private translateProviderService: TranslateProviderService,
    private alertService: AlertsProviderService,
    private signService: SignProviderService,
    private storage: StorageProviderService,
    private lockService: LockService,
    private kycService: KycService,
    private _ModalController: ModalController,
    private _CryptoProviderService: CryptoProviderService,
    private _SecureStorageService: SecureStorageService,
    private kycmedia: KycmediaService,
    private _NetworkService: NetworkService,

    private _ImageSelectionService: ImageSelectionService,
    private loader: LoaderProviderService,
    private kycMediaServiceAktia: KycMediaServiceAkita,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
   }


  /** @inheritDoc */
  public async ngOnInit() {
    // await this.updateDataAfterEdit();
}

public async ionViewWillEnter() {
  console.log("Legal Documents: loaderCreate()");
  // await this.loader.loaderCreate();
  this.displayLoadingDots = true;

  var photo = this.userPhotoServiceAkita.getPhoto();
  this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';

  this.displayVerifiedTick = await this.kycService.isUserAllowedToUseChatMarketplace();

  if(!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
  } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
      // if(this.appStateService.basicAuthToken) {
      //   await this.kycmedia.fetchRemainingMedia();
      // }
  }

  var userData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userData, false));
  var trustData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userTrustData, false));
  this.user = Object.assign({}, userData, this.userProviderService.mapTrustData(trustData));

  console.log("Legal Documents: this.user", this.user);

  this.allVerified = {
    passport: ( this.user.passportnumberStatus == 1 && this.user.passportissuecountryStatus == 1 && this.user.passportexpirydateStatus == 1),
    idcard: ( this.user.identificationdocumentnumberStatus == 1 && this.user.identificationissuecountryStatus == 1 && this.user.identificationexpirydateStatus == 1),
    driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 && this.user.driverlicencecountryStatus == 1 && this.user.driverlicenceexpirydateStatus == 1),
    residencepermit: ( this.user.residencepermitnumberStatus == 1 && this.user.residencepermitissuecountryStatus == 1 && this.user.residencepermitexpirydateStatus == 1),
  }

  // console.log(this.allVerified)

  if ( this.user.title === 'Dr.eh.Dr.' ) {
    this.user.title = 'Dr.';
  }

  // console.log(this.user)
  console.log("Legal Documents: await this.processDocumentDisplay();");
  await this.processDocumentDisplay();

  console.log("Legal Documents: await this.populateDocumentMedia();");
  await this.populateDocumentMedia();



  // await this.loader.loaderDismiss();
  this.displayLoadingDots = false;
  console.log("Legal Documents: loaderDismiss");
}

async updateDataAfterEdit() {
  var userData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userData, false));
  var trustData = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userTrustData, false));
  this.user = Object.assign({}, userData, this.userProviderService.mapTrustData(trustData));
  this.allVerified = {
    passport: ( this.user.passportnumberStatus == 1 && this.user.passportissuecountryStatus == 1 && this.user.passportexpirydateStatus == 1),
    idcard: ( this.user.identificationdocumentnumberStatus == 1 && this.user.identificationissuecountryStatus == 1 && this.user.identificationexpirydateStatus == 1),
    driverlicence: ( this.user.driverlicencedocumentnumberStatus == 1 && this.user.driverlicencecountryStatus == 1 && this.user.driverlicenceexpirydateStatus == 1),
    residencepermit: ( this.user.residencepermitnumberStatus == 1 && this.user.residencepermitissuecountryStatus == 1 && this.user.residencepermitexpirydateStatus == 1),
  }
    await this.processDocumentDisplay();
    await this.populateDocumentMedia();
}

async populateDocumentMedia()  {
  // var datePipe = new DatePipe('en-US');
  this.passportMedia = [];
  this.idCardMedia = [];
  this.residencePermitMedia = [];
  this.dlMedia = [];
  const kycMediaArray: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();
  // console.log(kycMediaArray)
  for (var i of kycMediaArray) {
    if (!!i.image) {
        

        if(i.documentType == 'PASSPORT') {
          if(this.passportMedia.length < 1) {
            this.passportMedia.push(i.image);
          }
        }
        if(i.documentType == 'RESIDENCEPERMIT') {
          if(this.residencePermitMedia.length < 2) {
            this.residencePermitMedia.push(i.image);
          }
        }
        if(i.documentType == 'IDCARD') {
          if(this.idCardMedia.length < 2) {
            this.idCardMedia.push(i.image);
          }
        }
        if(i.documentType == 'DRIVINGLICENCE') {
          if(this.dlMedia.length < 2) {
            this.dlMedia.push(i.image);
          }
        }
    }
    // console.log("Residence Permit Media: ");
    // console.log(this.residencePermitMedia);
    // console.log("ID Card Media: ");
    // console.log(this.idCardMedia);
    // console.log("Passport Media: ");
    // console.log(this.passportMedia);
    // console.log("DL Media: ");
    // console.log(this.dlMedia);
  }
}

/** Lifecycle hook for view will enter */
public ionViewDidLeave(): void {
}



goBack(){
  this.nav.navigateBack('/dashboard/personal-details');
  }

async processDocumentDisplay() {
  this.documentDisplay.driverlicence = (await this._SecureStorageService.getValue(SecureStorageKey.driverLicenseSet) == "true" ) ? true : false ;
  this.documentDisplay.idcard = (await this._SecureStorageService.getValue(SecureStorageKey.idCardSet) == "true" ) ? true : false ;
  this.documentDisplay.passport = (await this._SecureStorageService.getValue(SecureStorageKey.passportSet) == "true" ) ? true : false ;
  this.documentDisplay.residencepermit = (await this._SecureStorageService.getValue(SecureStorageKey.residencePermitSet) == "true" ) ? true : false ;
}



  async addDocument() {
    let buttonArray = [];
    for(let i of [{
      bool: this.documentDisplay.passport,
      methodArg: 'PASSPORT',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioPassport')
    },{
      bool: this.documentDisplay.idcard,
      methodArg: 'ID_CARD',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard')
    },{
      bool: this.documentDisplay.driverlicence,
      methodArg: 'DRIVERS_LICENSE',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioDL')
    },{
      bool: this.documentDisplay.residencepermit,
      methodArg: 'RESIDENCE_PERMIT',
      document: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit')
    }]){
      if(!i.bool){
        buttonArray.push(
          {
            text: i.document,
            handler: () => {
              console.log(i.methodArg + "selected");
              this.showModal(i.methodArg);
            }
          }
        )
      }
    }
    if(buttonArray.length > 0) {
      buttonArray.push({
        text: this.translateProviderService.instant('BUTTON.CANCEL'),
        role: 'cancel',
        handler: () => {
          // console.log("cancelled");
        }
      })
      await this.alertService.alertCreate(this.translateProviderService.instant('SIGNSTEPEIGHT.documentType'),'','',buttonArray);
    } else {
      buttonArray.push({
        text: this.translateProviderService.instant('GENERAL.ok'),
        role: 'cancel',
        handler: () => {
          // console.log("cancelled");
        }
      })
      await this.alertService.alertCreate(this.translateProviderService.instant('CONTACT.title'),'',this.translateProviderService.instant('LEGALDOCUMENTS.noMoreDocs'),buttonArray);
    }

  }


  async returnRelevantVCs(docType: string) {
    var vcs = await this._SecureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);
    var vcsArray = !!vcs ? JSON.parse(vcs) : [];

    console.log("Legal Documents: vcsArray", vcsArray);

    if(vcsArray.length > 0) {

      var passportVcs = [];
      var dlVcs = [];
      var residencePermitVcs = [];
      var idcardVcs = [];

      for(let ijk = 0; ijk < vcsArray.length; ijk ++) {
        var k = vcsArray[ijk];
        var decrypt = await this._CryptoProviderService.decryptVerifiableCredential(k);

        console.log("Legal Documents: decrypting VC #", ijk);

        if(!!decrypt.vc) {
          var vc = JSON.parse(decrypt.vc);
          // console.log("vc");
          // console.log(vc);
          if(!!vc.credentialSubject && !!vc.credentialSubject.data) {
            var keys = !!vc.credentialSubject.data[0] ? Object.keys(vc.credentialSubject.data[0]) : Object.keys(vc.credentialSubject.data);

            if(keys.includes('passportnumber') || keys.includes('passportissuecountry') || keys.includes('passportissuedate') || keys.includes('passportexpirydate')){
              passportVcs.push(vc);
            }
            if(keys.includes('driverlicencedocumentnumber') || keys.includes('driverlicenceissuecountry') || keys.includes('driverlicenceissuedate') || keys.includes('driverlicenceexpirycountry')){
              dlVcs.push(vc);
            }
            if(keys.includes('identificationdocumentnumber') || keys.includes('identificationissuecountry') || keys.includes('identificationissuedate') || keys.includes('identificationexpirydate')){
              idcardVcs.push(vc);
            }
            if(keys.includes('residencepermitnumber') || keys.includes('residencepermitissuecountry') || keys.includes('residencepermitissuedate') || keys.includes('residencepermitexpirydate')){
              residencePermitVcs.push(vc);
            }
          }
        }
      }

      switch(docType) {
        case 'PASSPORT': var returnVc = [passportVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(passportVcs, bb => +new Date(bb.proof.created))))] ; break;
        case 'ID_CARD': returnVc = [idcardVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(idcardVcs, bb => +new Date(bb.proof.created))))]; break;
        case 'RESIDENCE_PERMIT': returnVc = [residencePermitVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(residencePermitVcs, bb => +new Date(bb.proof.created))))]; break;
        case 'DRIVERS_LICENSE': returnVc = [dlVcs.find(hh => +new Date(hh.proof.created) == Math.max(...Array.from(dlVcs, bb => +new Date(bb.proof.created))))]; break;
        default: returnVc = []; break;
      }

      console.log("Legal Documents: returnVc", returnVc);

      return returnVc;

    }


  }


  async showModal(header: string) {
    let documentObject: DocumentModalObject = {};
    switch(header) {
      case 'PASSPORT':
        documentObject = {
          header: this.translateProviderService.instant('PERSONALDETAILS.radioPassport'),
          media: this.passportMedia,
          number: this.user.passportnumber,
          numberStatus: this.user.passportnumberStatus,
          issuedate: this.user.passportissuedate,
          issuedateStatus: this.user.passportissuedateStatus,
          expirydate: this.user.passportexpirydate,
          expirydateStatus: this.user.passportexpirydateStatus,
          issuecountry: this.user.passportissuecountry,
          issuecountryStatus: this.user.passportissuecountryStatus,
          vc: await this.returnRelevantVCs('PASSPORT')
        }
        break;
      case 'ID_CARD':
        documentObject = {
          header: this.translateProviderService.instant('PERSONALDETAILS.radioIDCard'),
          media: this.idCardMedia,
          number: this.user.identificationdocumentnumber,
          numberStatus: this.user.identificationdocumentnumberStatus,
          issuedate: this.user.identificationissuedate,
          issuedateStatus: this.user.identificationissuedateStatus,
          expirydate: this.user.identificationexpirydate,
          expirydateStatus: this.user.identificationexpirydateStatus,
          issuecountry: this.user.identificationissuecountry,
          issuecountryStatus: this.user.identificationissuecountryStatus,
          vc: await this.returnRelevantVCs('ID_CARD')
        }
        break;
      case 'RESIDENCE_PERMIT':
        documentObject = {
          header: this.translateProviderService.instant('PERSONALDETAILS.radioResidencePermit'),
          media: this.residencePermitMedia,
          number: this.user.residencepermitnumber,
          numberStatus: this.user.residencepermitnumberStatus,
          issuedate: this.user.residencepermitissuedate,
          issuedateStatus: this.user.residencepermitissuedateStatus,
          expirydate: this.user.residencepermitexpirydate,
          expirydateStatus: this.user.residencepermitexpirydateStatus,
          issuecountry: this.user.residencepermitissuecountry,
          issuecountryStatus: this.user.residencepermitissuecountryStatus,
          vc: await this.returnRelevantVCs('RESIDENCE_PERMIT')
        }
        break;
      case 'DRIVERS_LICENSE':
        documentObject = {
          header: this.translateProviderService.instant('PERSONALDETAILS.radioDL'),
          media: this.dlMedia,
          number: this.user.driverlicencedocumentnumber,
          numberStatus: this.user.driverlicencedocumentnumberStatus,
          issuedate: this.user.driverlicenceissuedate,
          issuedateStatus: this.user.driverlicenceissuedateStatus,
          expirydate: this.user.driverlicenceexpirydate,
          expirydateStatus: this.user.driverlicenceexpirydateStatus,
          issuecountry: this.user.driverlicencecountry,
          issuecountryStatus: this.user.driverlicencecountryStatus,
          vc: await this.returnRelevantVCs('DRIVERS_LICENSE')
        }
        break;
      default:
        documentObject = {
          header: this.translateProviderService.instant('PERSONALDETAILS.radioPassport'),
          media: this.passportMedia,
          number: this.user.passportnumber,
          numberStatus: this.user.passportnumberStatus,
          issuedate: this.user.passportissuedate,
          issuedateStatus: this.user.passportissuedateStatus,
          expirydate: this.user.passportexpirydate,
          expirydateStatus: this.user.passportexpirydateStatus,
          issuecountry: this.user.passportissuecountry,
          issuecountryStatus: this.user.passportissuecountryStatus,
          vc: await this.returnRelevantVCs('PASSPORT')
        }
        break;
    }
    const modal = await this._ModalController.create({
      component: DocumentModalPageComponent,
      componentProps: {
        data: {
          header: documentObject.header,
          documentType: header,
          close: this.translateProviderService.instant('LEGALDOCUMENTS.closeModal'),
          media: documentObject.media,
          number: documentObject.number,
          numberStatus: documentObject.numberStatus,
          issuedate: documentObject.issuedate,
          issuedateStatus: documentObject.issuedateStatus,
          expirydate: documentObject.expirydate,
          expirydateStatus: documentObject.expirydateStatus,
          issuecountry: documentObject.issuecountry,
          issuecountryStatus: documentObject.issuecountryStatus,
          vc: documentObject.vc
        }
      }
    })
    modal.onDidDismiss().then(() => {
      // await this.loader.loaderCreate();
      this.updateDataAfterEdit();
      // await this.loader.loaderDismiss();
    })
    await modal.present();
  }

  async displayPictureOptions() {
    // console.log('Should work!')
    this._ImageSelectionService.showChangePicture().then(photo => {
      this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg'
    });
  }
}
