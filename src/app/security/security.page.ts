import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { NavController, Platform, ToastController, ModalController } from '@ionic/angular';
import { noop } from 'lodash-es';
import { FingerprintAIO } from '@awesome-cordova-plugins/fingerprint-aio/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AlertsProviderService } from '../core/providers/alerts/alerts-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../core/providers/crypto/crypto-provider.service';
import { SettingsService, Settings } from '../core/providers/settings/settings.service';
import { EventsService, EventsList } from '../core/providers/events/events.service';
import { Subject, fromEvent } from 'rxjs';
import { bufferWhen, debounceTime, filter, map, take, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { PinPadComponent } from './pin-pad/pin-pad.component';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit, OnDestroy {

  private destroy$ = new Subject();

  /** Model value for the toggle */
  public toggleValue = false;
  /** View model if faio is available */
  public faioIsAvailable = false;
  public isEnvProduction = environment.production;
  profilePictureSrc;

  // Lock Screen Stuff

  public toggleLockScreen = false;
  public powerUserMode = false;
  public tapToActivate = false;
  public toggleChatEncryption = false;
  public toggleSecurePIN = false;

  constructor(
      public platform: Platform,
      public fingerprintAIO: FingerprintAIO,
      private secureStorageService: SecureStorageService,
      private navCtrl: NavController,
      private translateService: TranslateService,
      private alertService: AlertsProviderService,
      private setting: SettingsService,
      private _EventsService: EventsService,
      private _ToastController: ToastController,
      private crypto: CryptoProviderService,
      private _ModalController: ModalController,
      private elem: ElementRef,
      private _Router: Router,
      private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
  }

  /** @inheritDoc */
  public ngOnInit(): void {
    this.secureStorageService.getValue(SecureStorageKey.faioEnabled, false).then((value: string) => {
      this.toggleValue = (value === 'true');
    }, noop);
    this.secureStorageService.getValue(SecureStorageKey.lockScreenToggle, false).then(value => {
      this.toggleLockScreen = (value == 'true');
    })
    this.secureStorageService.getValue(SecureStorageKey.powerUserMode, false).then(value => {
      this.powerUserMode = (value == 'true');
    })
    this.secureStorageService.getValue(SecureStorageKey.toggleChatEncryption, false).then(value => {
      this.toggleChatEncryption = this.isEnvProduction ? true : (value == 'true');
    })
    this.secureStorageService.getValue(SecureStorageKey.toggleSecurePIN, false).then(value => {
      this.toggleSecurePIN = (value == 'true');
    })
    
    this.profilePictureSrc = this.userPhotoServiceAkita.getPhoto();

    const element = this.elem.nativeElement.querySelectorAll('.page-title')
    const single$ = fromEvent(element[0], 'click');
    single$.pipe(
        bufferWhen(() => single$.pipe(debounceTime(500))),
        map(list => {
            return list.length;
        }),
        tap(count => console.log(count)),
        filter(length => length >= 5),
        take(1)
    ).subscribe(_ => {
        this.tapToActivate = true;
    });
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    this.tapToActivate = false;
    this.destroy$.next(0);
    this.destroy$.complete();
  }

  public ionViewWillEnter() {
    this.fingerprintAIO.isAvailable().then((result) => {
      var biometricOptions = ['biometric', 'face', 'finger'];
      var matchingBio = biometricOptions.find(opt => { return opt == result; });
      this.faioIsAvailable = (matchingBio != null);
    }, (reject) => {
      this.faioIsAvailable = false;
    });
  }

  /** Callback for storing the button */
  public async storeFaio(value: boolean): Promise<void> {
    // if (this.platform.is('android')) {
      // mburger: idk why we need to check for the permissions first on android
      // await this.getPermission();
    // }
    try {
      await this.secureStorageService.setValue(SecureStorageKey.faioEnabled, value.toString());
    } catch (e) {
      try {
        await this.alertService.alertCreate(
            this.translateService.instant('GENERAL.error'),
            null,
            this.translateService.instant(
                'SECURITY.error-storing-message',
                {faio: this.translateService.instant('SECURITY.faio_' + (this.platform.is('android') ? 'android' : 'ios'))}),
            [{
              text: this.translateService.instant('GENERAL.ok'),
              role: 'Cancel'
            }]
        );
      } catch (e) {
        throw new Error('There was an error with opening the alert message in the security page');
      }
    }
  }

  public async storeLockScreenToggle(value: boolean) {
    var message = value ? this.translateService.instant('SECURITY.lock-screen-deactivated'): this.translateService.instant('SECURITY.lock-screen-activated');
    await this.presentToast(message);
    await this.setting.set(Settings.lockScreenToggle, value.toString(), true);
    // await this.secureStorageService.setValue(SecureStorageKey.lockScreenToggle, value.toString());
  }

  public async storeDebugToggle(value: boolean) {
    var message = value ? this.translateService.instant('SECURITY.power-user-activated'): this.translateService.instant('SECURITY.power-user-deactivated');
    await this.presentToast(message);
    await this._EventsService.publish(EventsList.powerUserMode, value);
    await this.secureStorageService.setValue(SecureStorageKey.powerUserMode, value.toString());
  }

  public async storeChatEncryptionToggle(value: boolean) {
    var message = value ? this.translateService.instant('SECURITY.chat-encryption-enabled'): this.translateService.instant('SECURITY.chat-encryption-disabled');
    await this.presentToast(message);
    await this._EventsService.publish(EventsList.toggleChatEncryption, value);
    await this.secureStorageService.setValue(SecureStorageKey.toggleChatEncryption, value.toString());
  }

  public async storeToggleSecurePIN(value: boolean) {
    var message = value ? this.translateService.instant('SECURITY.secure-pin-activated'): this.translateService.instant('SECURITY.secure-pin-deactivated');
    await this.presentToast(message);
    await this._EventsService.publish(EventsList.toggleSecurePIN, value);
    await this.secureStorageService.setValue(SecureStorageKey.toggleSecurePIN, value.toString());
  }

  

  /** Go back callback */
  public goBack(): void {
    this.navCtrl.navigateBack('/dashboard/tab-settings');
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 2000, position: 'top' });
    await toast.present();
  }

  // private getPermission(): Promise<any> {
  //   return this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BIOMETRIC).then(
  //       noop,
  //       err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BIOMETRIC)
  //   );
  // }

  public async changePIN() {
    console.log('Ok! Changing! wait!');
    const modal = await this._ModalController.create({
      component: PinPadComponent,
      componentProps: {
        data: {

        }
      }
    })
    modal.onDidDismiss().then(() => {
      console.log('Dismissed!')
    })
    await modal.present();
  }

}
