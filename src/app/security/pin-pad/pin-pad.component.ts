import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { SecurePinService } from 'src/app/core/providers/secure-pin/secure-pin.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { CodeInputComponent } from 'angular-code-input';

interface FormElements {
  existingPin?: string;
  newPin?: string;
  confirmPin?: string;
}

enum Steps {
  step1 = 'step1',
  step2 = 'step2',
  step3 = 'step3'
}

@Component({
  selector: 'app-pin-pad',
  templateUrl: './pin-pad.component.html',
  styleUrls: ['./pin-pad.component.scss'],
})
export class PinPadComponent implements OnInit {
  @ViewChild('codeInput1') codeInput1: CodeInputComponent;
  @ViewChild('codeInput2') codeInput2: CodeInputComponent;
  @ViewChild('codeInput3') codeInput3: CodeInputComponent;

  // public saveObject: FormElements = {};
  // public changePinForm: FormGroup;
  public steps = Steps;
  public step: string = Steps.step1;
  public ifPinExists: boolean = false;
  private tempPin: string = '';
  public visibility = {
    input1: true,
    input2: true,
    input3: true,
  }

  constructor(
    private _ModalController: ModalController,
    private _SecurePinService: SecurePinService,
    private _SecureStorageService: SecureStorageService,
    private _ToastController: ToastController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    this._SecureStorageService.getValue(SecureStorageKey.securePINEncrypted, false).then((pin) => {
      if(pin) {
        this.ifPinExists = true;
        this.step = this.steps.step1;
        
      } else {
        this.step = this.steps.step2;
        
      }
      // console.log(this.changePinForm);
    })
  }

  onCodeChanged1(code: string) { console.log(code) }
  onCodeChanged2(code: string) { console.log(code) }
  onCodeChanged3(code: string) { console.log(code) }

  onCodeCompleted1(code: string) {
    this._SecureStorageService.getValue(SecureStorageKey.securePINEncrypted, false).then((encryptedPin) => {
      if(encryptedPin) {
        this._SecurePinService.checkPIN(code, encryptedPin).then((val) => {
          if(val) {
            this.codeInput1.reset();
            this.step = this.steps.step2;
          } else {
            console.log("Case 5")
            this.codeInput1.reset();
            return this.presentToast(this._Translate.instant('PIN.incorrectPIN'));
          }
        });
      }
    });
  }

  onCodeCompleted2(code: string) {
    this.tempPin = code;
    console.log(this.tempPin);
    this.codeInput2.reset();
    this.step = this.steps.step3;
  }

  onCodeCompleted3(code: string) {
    console.log(this.tempPin);
    console.log(code);
    if(code != this.tempPin) {
      console.log("Case 1")
      this.codeInput3.reset();
      return this.presentToast(this._Translate.instant('PIN.noMatch'));
    }
    this._SecurePinService.pushEncryptedPIN(this.tempPin).then((c) => {
      console.log(c);
      this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, c).then(() => {})
      console.log("Case 4")
      this.presentToast(this._Translate.instant('PIN.success'));
      this.codeInput3.reset();
      this._ModalController.dismiss();
    })
  }

  public close() {
    this._ModalController.dismiss();
  }

  private presentToast(message) {
    this._ToastController.create({
      message: message,
      duration: 2000,
      position: 'top'
    }).then((toast) => {
      toast.present();
    })
  }

  public togglePINVisibility(input) {
    this.visibility[input] = !this.visibility[input];
  }

  private setFormDataFromStorage(model: FormElements): void {
    // if ( model && model.confirmPin ) { this.changePinForm.controls.confirmPin.setValue(model.confirmPin); }
    // if ( model && model.newPin ) { this.changePinForm.controls.newPin.setValue(model.newPin); }
    // if ( model && model.existingPin ) { this.changePinForm.controls.existingPin.setValue(model.existingPin); }
  }
}
