import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReferralsPageRoutingModule } from './referrals-routing.module';
import { JobHeaderModule } from '../job-header/job-header.module';
import { ReferralsPage } from './referrals.page';
import { TranslateModule } from '@ngx-translate/core';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    ReferralsPageRoutingModule
  ],
  providers: [
    SocialSharing
  ],
  declarations: [ReferralsPage]
})
export class ReferralsPageModule {}
