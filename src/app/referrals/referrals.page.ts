import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { NavController, ToastController } from '@ionic/angular';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Clipboard } from '@capacitor/clipboard';
import { Referrals, FirestoreCloudFunctionsService } from '../core/providers/firestore-cloud-functions/firestore-cloud-functions.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { Router } from '@angular/router';
import { TranslateProviderService } from '@services/translate/translate-provider.service';


@Component({
  selector: 'app-referrals',
  templateUrl: './referrals.page.html',
  styleUrls: ['./referrals.page.scss'],
})
export class ReferralsPage implements OnInit {
  userImage: any;
  source;
  referrals: Array<Referrals> = [
    // {
    //   generatedReferralCode: "KJA123",
    //   referralCategory: "category_a",
    //   title: "German Campaign",
    //   description: "Earn 50 EUR by joining the campaign",
    //   active: true
    // },
    // {
    //   generatedReferralCode: null,
    //   referralCategory: "category_b",
    //   title: "Indian Campaign",
    //   description: "Earn 50 INR by joining the campaign",
    //   active: true
    // },
    // {
    //   generatedReferralCode: "JAS412",
    //   referralCategory: "category_c",
    //   title: "Pakistani Campaign",
    //   description: "Earn 25 PKR by joining the campaign",
    //   active: false
    // },
    // {
    //   generatedReferralCode: null,
    //   referralCategory: "category_d",
    //   title: "Italian Campaign",
    //   description: "Earn 25 EUR by joining the campaign",
    //   active: false
    // }
  ];
  user;
  language;

  constructor(
    private appStateService: AppStateService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private nav: NavController,
    private social: SocialSharing,
    private _Router: Router,
    private toast: ToastController,
    private firestoreCloudFunctionsService: FirestoreCloudFunctionsService,
    private userService: UserProviderService,
    private loader: LoaderProviderService,
    private translate: TranslateProviderService
  ) { }

  async ngOnInit() {
    if(this.appStateService.basicAuthToken) {
      this.userImage = this.userPhotoServiceAkita.getPhoto();
    }
    this.language = await this.translate.getLangFromStorage();
    this.user = await this.userService.getUser();
    let checker = this._Router.parseUrl(this._Router.url).queryParams;
    this.source = checker.from;

    await this.updateReferralPrograms();

  }

  goBackToSettings() {
    if(this.source) {
      this.nav.navigateBack('/dashboard/tab-home');
    } else {
      this.nav.navigateBack('/dashboard/tab-settings');
    }
  }

  share(code: string) {
    const url=`https://web.jobgrader.app/referral?code=${code}`;
    // Clipboard.write({ string: url }).then(() => {
      // this.presentToast(`Referal Link has been copied.`);
      const shareText = `GM, GM ✌️\nCheck out this awesome app, Jobgrader! It's perfect for anyone who wants to earn extra income on-the-go. Use my referral code ${code} when you sign up.\nHere’s the link: ${url}`
      this.social.share(
        shareText,
        null,
        null,
        url);
    // })
  }

  async updateReferralPrograms() {
    this.referrals = await this.firestoreCloudFunctionsService.fetchReferralPrograms();
    // this.referrals = Array.from(referrals, r => Object.assign(r, { active:  (r.active && (new Date() < new Date(this.processedTimestamp(r.expiryDate._seconds, r.expiryDate._nanoseconds))))}) );
  }

  processedTimestamp(seconds: number, nanoseconds: number) {
    return (seconds * 1000) + (nanoseconds / 1000000);
  }

  async generate(referralCategory: string) {
    await this.loader.loaderCreate();
    const r = await this.firestoreCloudFunctionsService.generateReferralCode({
      referralCategory,
    }).catch(async e => {
      await this.loader.loaderDismiss();
      await this.presentToast(this.translate.instant('REFERRALS.ERROR'));
      this.nav.navigateBack(`/dashboard/personal-details`);
    });
    if(!!r) {
      await this.updateReferralPrograms();
      await this.loader.loaderDismiss();
    }
  }

  presentToast(message: string) {
    this.toast.create({
      message,
      position: "top",
      duration: 2000
    }).then(t => {
      t.present();
    })
  }

}
