import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LibrechatPage } from './librechat.page';

const routes: Routes = [
  {
    path: '',
    component: LibrechatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LibrechatPageRoutingModule {}
