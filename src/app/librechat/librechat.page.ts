import { Component, inject } from '@angular/core'
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-librechat',
  templateUrl: './librechat.page.html',
  styleUrls: ['./librechat.page.scss'],
})
export class LibrechatPage {

  private nav = inject(NavController);

  constructor() { }


  goBack() {
    this.nav.back();
  }
}
