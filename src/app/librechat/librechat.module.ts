import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LibrechatPageRoutingModule } from './librechat-routing.module';
import { LibrechatPage } from './librechat.page';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    JobHeaderModule,
    SharedModule,
    LibrechatPageRoutingModule
  ],
  providers: [
    SocialSharing
  ],
  declarations: [LibrechatPage]
})
export class LibrechatPageModule {}
