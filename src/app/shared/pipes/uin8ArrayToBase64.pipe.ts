
import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash-es';
import { CryptoProviderService } from '../../core/providers/crypto/crypto-provider.service';

@Pipe({ name: 'uint82Base64' })
export class UInt8ArrayToBase64Pipe implements PipeTransform {

    constructor(private cryptoProviderService: CryptoProviderService) {}

    /** Transforms a u int 8 array to base 64 */
    transform(value: any): string {
       if (value instanceof Uint8Array) {
           return this.cryptoProviderService.uInt8ArrayToBase64(value);
       } else {
           return value;
       }
    }
}


