import { TranslateModule } from '@ngx-translate/core';
// import { ContactAddModalComponent } from './../contact/shared/contact-add-modal/contact-add-modal.component';
import { SignUpFooterComponent } from './../sign-up/components/sign-up-footer/sign-up-footer.component';
import { SignUpHeaderComponent } from './../sign-up/components/sign-up-header/sign-up-header.component';
import { FieldLockComponent } from './../_sharedComponents/field-lock.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsComponent } from './components/tabs/tabs.component';
import { TabDirective } from './directives/tab-index.directive';
import { TabService } from './directives/tab.service';
import { SortByPipe } from './pipes/sort-by.pipe';
import { UInt8ArrayToBase64Pipe } from './pipes/uin8ArrayToBase64.pipe';
import { CountrySelectionItemComponent } from './components/country-selection-item/country-selection-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ImageSelectionComponent } from './components/image-selection/image-selection.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { MandatoryFieldIndicatorComponent } from './components/mandatory-field-indicator/mandatory-field-indicator.component';
import { BackButtonComponent } from './components/back-button/back-button.component';
// import { TooltipsModule } from 'ionic-tooltips';

@NgModule({
    providers: [
        TabService
    ],
    declarations: [
        TabsComponent,
        SignUpHeaderComponent,
        SignUpFooterComponent,
        FieldLockComponent,
        // ContactAddModalComponent,
        TabDirective,
        SortByPipe,
        UInt8ArrayToBase64Pipe,
        CountrySelectionItemComponent,
        ImageSelectionComponent,
        ErrorPageComponent,
        MandatoryFieldIndicatorComponent,
        BackButtonComponent
    ],
    imports: [
        TranslateModule.forChild(),
        RouterModule,
        CommonModule,
        IonicModule,
        ReactiveFormsModule,
        // TooltipsModule,
    ],
    exports: [
        TabsComponent,
        SignUpHeaderComponent,
        SignUpFooterComponent,
        FieldLockComponent,
        // ContactAddModalComponent,
        TabDirective,
        SortByPipe,
        UInt8ArrayToBase64Pipe,
        CountrySelectionItemComponent,
        ImageSelectionComponent,
        ErrorPageComponent,
        MandatoryFieldIndicatorComponent,
        BackButtonComponent
    ]
})
export class SharedModule {
}
