var sqlitePlugin;
var cordova;

import { Capacitor } from '@capacitor/core';

export function getOpenDatabasePromise () {
    // console.log('getOpenDatabasePromise; Capacitor', Capacitor);

    return new Promise(
        (resolve) => {
            if (Capacitor &&
                Capacitor.isPluginAvailable('CapacitorDataStorageSqlite') &&
                Capacitor.isNativePlatform()
            ) {

                resolve(true);

            } else {

                throw new Error('CapacitorDataStorageSqlite plugin is not present.');

            }

        }
    );
}