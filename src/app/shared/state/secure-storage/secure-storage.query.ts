import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';


// custom imports
import { SecureStorageState } from './secure-storage.model';
import { SecureStorageStore } from './secure-storage.store';


@Injectable({ providedIn: 'root' })
export class SecureStorageQuery extends Query<SecureStorageState> {

  constructor(protected store: SecureStorageStore) {
    super(store);
  }

}
