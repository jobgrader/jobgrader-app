import { Injectable } from '@angular/core';


// custom imports
import { SecureStorageStore } from './secure-storage.store';


@Injectable({ providedIn: 'root' })
export class SecureStorageServiceAkita {

  constructor(private store: SecureStorageStore) {
  }

  public updateValue(key: string, value: string): any {

    const obj = {};
    obj[key] = value;

    return this.store.update(obj);
  }

  public clear() {
    let ob = this.store.getValue();

    let clear = {};

    Object.entries(ob).forEach(([key, value]) => {
      clear[key] = null;
    });

    this.store.update(clear);
  }

}
