import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-mandatory-field-indicator',
  templateUrl: './mandatory-field-indicator.component.html',
  styleUrls: ['./mandatory-field-indicator.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MandatoryFieldIndicatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
