import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MandatoryFieldIndicatorComponent } from './mandatory-field-indicator.component';

describe('MandatoryFieldIndicatorComponent', () => {
  let component: MandatoryFieldIndicatorComponent;
  let fixture: ComponentFixture<MandatoryFieldIndicatorComponent>;

 beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatoryFieldIndicatorComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatoryFieldIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
