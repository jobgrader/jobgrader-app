import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Capacitor } from '@capacitor/core';
import { Keyboard, KeyboardInfo } from '@capacitor/keyboard';
import { NavController } from '@ionic/angular';
import { Subject, filter, takeUntil } from 'rxjs';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AppStateService } from 'src/app/core/providers/app-state/app-state.service';
import { BarcodeService } from 'src/app/core/providers/barcode/barcode.service';
// import { ChatService } from 'src/app/core/providers/chat/chat.service';
import { EventsList, EventsService } from 'src/app/core/providers/events/events.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnDestroy, OnInit {
  @Input('active_tab') active_tab = -1;
  new_message = 2;
  new_activities=  2;
  public readonly tabs = {
    smartProfile: '/dashboard/tab-profile?source=smart',
    web3Profile: '/dashboard/tab-profile?source=web3',
    contact: '/dashboard/tab-contact',
    home: '/dashboard/tab-home',
    qrcode: '/dashboard/tab-qrcode',
    settings: '/dashboard/tab-settings',
    marketplace: '/dashboard/marketplace',
    merchantDetails: '/merchant-details',
    signature: '/signature',
    verification: '/verification',
    personalDetails: '/dashboard/personal-details',
    legalDocuments: '/dashboard/legal-documents',
    certificatesPage: '/certificates-page',
    // healthDocuments: '/dashboard/health-certificates',
    // financialDocuments: '/dashboard/financial-documents',
    veriffThanks: '/veriff-thanks',
    veriffFailed: '/veriff-failed',
    requests: '/dashboard/requests',
    // eventTickets: '/event-tickets',
    // grace: '/grace',
    payments: '/dashboard/payments',
    // kyc: '/kyc',
    cryptoAccount: '/dashboard/payments/crypto-account-page',
    cryptoActivities: '/dashboard/crypto-activities',
    walletSettings: '/dashboard/payments/wallet-settings',
    networkAccountDetails: '/dashboard/payments/network-account-details',
    hcaptcha: '/dashboard/hcaptcha',
    fortune: '/jobs',
    humanAi: '/dashboard/human',
    // celebrityNfts: '/celebrity-nfts',
    // celebrityNftList: '/celebrity-nft-list',
    // celebrityNftDetail: '/celebrity-nft-detail',
    // kassenBelege: '/kassen-belege',
    nft: '/dashboard/nft'
  };

  public showContactsBadge: boolean = false;
  public showProfileBadge: boolean = false;
  public myGuid: string;
  public light: boolean;

  private destroyed = new Subject();

  keyboardOpen = false;

  constructor(
    public route: Router,
    // public _ChatService: ChatService,
    private _Theme: ThemeSwitcherService,
    private _AppState: AppStateService,
    private _Api: ApiProviderService,
    private _Barcode: BarcodeService,
    private _Event: EventsService,
    private nav: NavController) {

      route.events
        .pipe(
          takeUntil(this.destroyed),
          filter(event => event instanceof NavigationEnd)
        )
        .subscribe(
          (event: NavigationEnd) => {
            this.active_tab = this.updateActiveTab();
          }
        );

      if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.addListener('keyboardWillShow', (info: KeyboardInfo) => this.keyboardOpen = true);
      if (Capacitor.isPluginAvailable('Keyboard')) Keyboard.addListener('keyboardWillHide', () => this.keyboardOpen = false);
  }

  ngOnInit() {
    var theme = this._Theme.getCurrentTheme();
    this.light = (theme == 'light');
    this._Event.subscribe(EventsList.themeChange, (theme) => {
      console.log(theme);
      this.light = (theme == 'light');
    });
  }

  ngOnDestroy(): void {
    this.destroyed.next(true);
    this.destroyed.complete();
  }

  handleTabClick(e: any) {
    console.log(e);
    let anchor;
    if (e.target.matches("a")) {
      anchor = e.target
    } else {
      anchor = e.target.closest("a")
    }
    if (anchor != null) {
      const allAnchors = [];

      let anchors  = document.getElementsByTagName("a");
      for (let i = 0; i < anchors.length; i++) {
        allAnchors.push(anchors.item(i));

      }
      const index = allAnchors.indexOf(anchor)
      document.getElementById('indicator').style.setProperty("--position", index.toString())
      document.querySelectorAll("a").forEach(elem => {
        elem.classList.remove("active")
      })
      anchor.classList.add("active")
    }
  }


  goToPage(routePath: string) {
    this.nav.navigateRoot(routePath);
  }

  updateActiveTab() {
    console.log('===> TabsComponent#updateActiveTab; this.route.url:', this.route.url);

    if (this.isHomeTab()) {
      return 2;
    } else if (this.isProfileTab()) {
      return 0;
    }
    else if (this.isWeb3Tab()) {
      return 1;
    } else if (this.isSettingsTab()) {
      return 4;
    } else {
      return 3;
    }
  }
  isProfileTab() {
    return (
      this.route.url.indexOf(this.tabs.smartProfile) > -1 ||
      this.route.url.indexOf(this.tabs.personalDetails) > -1 ||
      this.route.url.indexOf(this.tabs.legalDocuments) > -1 ||
      this.route.url.indexOf(this.tabs.certificatesPage) > -1 ||
      // this.route.url.indexOf(this.tabs.healthDocuments) > -1 ||
      // this.route.url.indexOf(this.tabs.financialDocuments) > -1  ||
      this.route.url.indexOf(this.tabs.requests) > -1 ||
      // this.route.url.indexOf(this.tabs.eventTickets) > -1 ||
      // this.route.url.indexOf(this.tabs.kassenBelege) > -1 ||
      this.route.url.indexOf(this.tabs.contact) > -1
      // this.route.url.indexOf(this.tabs.kyc) > -1
    );
  }

  isWeb3Tab() {
    return (
      this.route.url.indexOf(this.tabs.web3Profile) > -1 ||
      this.route.url.indexOf(this.tabs.payments) > -1 ||
      this.route.url.indexOf(this.tabs.cryptoAccount) > -1 ||
      this.route.url.indexOf(this.tabs.cryptoActivities) > -1 ||
      this.route.url.indexOf(this.tabs.walletSettings) > -1 ||
      this.route.url.indexOf(this.tabs.nft) > -1 ||
      // this.route.url.indexOf(this.tabs.grace) > -1 ||
      // this.route.url.indexOf(this.tabs.celebrityNfts) > -1 ||
      // this.route.url.indexOf(this.tabs.celebrityNftList) > -1 ||
      // this.route.url.indexOf(this.tabs.celebrityNftDetail) > -1 ||
      this.route.url.indexOf(this.tabs.networkAccountDetails) > -1
    );
  }

  openScanner() {
    if (this._AppState.isAuthorized) {
      this._Barcode.shortcutsPageModification()
                      .then(
                        (value) => this.active_tab = this.updateActiveTab()
                      );
    } else {
      this._Api.noUserLoggedInAlert();
    }
  }

  isHomeTab() {
    return (
      this.route.url.indexOf(this.tabs.home) > -1 ||
      this.route.url.indexOf(this.tabs.marketplace) > -1 ||
      this.route.url.indexOf(this.tabs.merchantDetails) > -1 ||
      this.route.url.indexOf(this.tabs.hcaptcha) > -1 ||
      this.route.url.indexOf(this.tabs.fortune) > -1 ||
      this.route.url.indexOf(this.tabs.humanAi) > -1
    );
  }

  isQrTab() {
    return (
      this.route.url.indexOf(this.tabs.qrcode) > -1
    );
  }

  isSettingsTab() {
    return (
      this.route.url.indexOf(this.tabs.settings) > -1
    );
  }

}
