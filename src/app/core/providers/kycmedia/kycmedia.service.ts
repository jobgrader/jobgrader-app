import { Injectable } from '@angular/core';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { ApiProviderService } from '../api/api-provider.service';
import { AppStateService } from '../app-state/app-state.service';
import { KycStatus } from 'src/app/kyc/interfaces/kyc-status.interface';
import { environment } from 'src/environments/environment';
import { KycProvider } from '../../models/Kyc';
import { KycState } from 'src/app/kyc/enums/kyc-state.enum';
import { KycMediaServiceAkita } from '../state/kyc-media/kyc-media.service';
import { KycMedia } from '../state/kyc-media/kyc-media.model';
import { AlertController, Platform, ToastController } from '@ionic/angular';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { FileOpener } from '@awesome-cordova-plugins/file-opener/ngx';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { GDriveBackupObject, GdriveService } from '../cloud/gdrive.service';
import { ActivityKeys, UserActivitiesService } from '../user-activities/user-activities.service';
declare var iCloudDocStorage: any;
declare var window: any;

interface MediaContent {
  image?: any;
  imageIdentifier?: string;
  documentType?: string;
  sessionId?: string;
  lastmodified?: string;
  verifiedBy?: string;
}

@Injectable({
  providedIn: 'root'
})

export class KycmediaService {

  private mediaContentArray: Array<KycMedia> = [];

  constructor(
    private secureStorage: SecureStorageService,
    private apiProviderService: ApiProviderService,
    private appStateService: AppStateService,
    private kycMediaServiceAktia: KycMediaServiceAkita,
    private _Plaform: Platform,
    private _File: File,
    private _FileOpener: FileOpener,
    private _Translate: TranslateProviderService,
    private _GDrive: GdriveService,
    private _ToastController: ToastController,
    private _AlertController: AlertController,
    private _UserActivity: UserActivitiesService
  ) { }

  public async init() {
    var kycMediaTimestamp = await this.secureStorage.getValue(SecureStorageKey.kycMediaTimestamp, false);
        if(!kycMediaTimestamp) {
          await this.populateMediaArray(false);
        }
        else {
          var latestTimestamp = await this.apiProviderService.getKycMediaTimestamp();
          if(Number(latestTimestamp) > Number(kycMediaTimestamp)) {
            await this.secureStorage.setValue(SecureStorageKey.kycMediaTimestamp, latestTimestamp.toString());
            await this.populateMediaArray(true);
          }
        }
    }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
    // kycStatus: 75a8c9fe-2062-4dea-8b8c-6c2ffd5ee217
    // kycMedia: 75a8c9fe-2062-4dea-8b8c-6c2ffd5ee217
  public async fetchRemainingMedia() {
    // console.log('KycmediaService#fetchRemainingMedia');

    const kycStatusString = await this.secureStorage.getValue(SecureStorageKey.kycStatus, false);
    let kycMedia: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();
    let kycStatus = !!kycStatusString ? JSON.parse(kycStatusString) : [];
    // console.log(kycStatus);
    // console.log(kycMedia);

    var uniqueSessionIdKycStatus = Array.from(kycStatus, k => {
      if(!!(k as any).additionalInfo){
        if(!!(k as any).additionalInfo.veriffSessionId) {
          return (k as any).additionalInfo.veriffSessionId
        }
      }
      }).filter(this.onlyUnique);
    // console.log(uniqueSessionIdKycStatus);
    let uniqueSessionIdKycMedia = Array.from(kycMedia, km => (km as any).sessionId).filter(this.onlyUnique);
    let remainingSessionIds = uniqueSessionIdKycStatus.filter(n => !uniqueSessionIdKycMedia.includes(n)).filter(nn => nn);
    // console.log(remainingSessionIds);
    if(remainingSessionIds.length > 0) {
      for(let i=0; i<remainingSessionIds.length; i++) {
        const documentTypeObject = await this.apiProviderService.veriffCheckPromise(remainingSessionIds[i]);
        const mediaContent = await this.apiProviderService.getKycMediaIds(remainingSessionIds[i]);
        // console.log("mediaContent: "+ JSON.stringify(mediaContent))
        if(mediaContent) {
          const backupArray: Array<GDriveBackupObject> = [];

          for (let k of mediaContent["media"] ) {
            const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
            const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.veriff);
            const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());
            if(!!k.kycMediaId && ['document-front', 'document-back'].includes(k.mediaIdentifier)){
              const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
              if(mediafile) {
                const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, false);
                this.mediaContentArray.push({
                  id: watermarked.src.length,
                  image: watermarked.src,
                  imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                  documentType: (documentTypeObject as any).kycDocumentType,
                  sessionId: remainingSessionIds[i],
                  lastmodified: (k.lastModified ? k.lastModified : null),
                  verifiedBy
                })
                this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
              }
            }
          }

          this.uploadMediaOnCloud(backupArray);
        }
      }
    }
    uniqueSessionIdKycStatus = Array.from(kycStatus, k => {
      if(!!(k as any).additionalInfo) {
        if(!!(k as any).additionalInfo.ondatoSessionId) {
          return (k as any).additionalInfo.ondatoSessionId
        }
      }
    }).filter(this.onlyUnique);
    uniqueSessionIdKycMedia = Array.from(kycMedia, km => (km as any).sessionId).filter(this.onlyUnique);
    remainingSessionIds = uniqueSessionIdKycStatus.filter(n => !uniqueSessionIdKycMedia.includes(n)).filter(nn => nn);
    if(remainingSessionIds.length > 0) {
      for(let i=0; i<remainingSessionIds.length; i++) {
        const documentTypeObject = await this.apiProviderService.ondatoCheckPromise(remainingSessionIds[i]);
        const mediaContent = await this.apiProviderService.getKycMediaIds(remainingSessionIds[i]);
        // console.log("mediaContent: "+ JSON.stringify(mediaContent))
        if(mediaContent) {
          const backupArray: Array<GDriveBackupObject> = [];

          for (let k of mediaContent["media"] ) {
            if(!!k.kycMediaId && ['front', 'back', 'document'].includes(k.mediaIdentifier)){
              const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.ondato);
              const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());

              if(!kycMedia.find(km => (km.id == k.kycMediaId))) {
                const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                if(mediafile) {
                  const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, false);
                  this.mediaContentArray.push({
                    id: k.kycMediaId,
                    image: watermarked.src,
                    imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                    documentType: (documentTypeObject as any).kycDocumentType,
                    sessionId: remainingSessionIds[i],
                    lastmodified: (k.lastModified ? k.lastModified : null),
                    verifiedBy
                  })
                  this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                  backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
                }
              }

            }
          }

          this.uploadMediaOnCloud(backupArray);
        }
      }
    }

    uniqueSessionIdKycStatus = Array.from(kycStatus, k => {
      if(!!(k as any).additionalInfo) {
        if(!!(k as any).additionalInfo.verifeyeSessionId) {
          return (k as any).additionalInfo.verifeyeSessionId
        }
      }
    }).filter(this.onlyUnique);
    uniqueSessionIdKycMedia = Array.from(kycMedia, km => (km as any).sessionId).filter(this.onlyUnique);
    remainingSessionIds = uniqueSessionIdKycStatus.filter(n => !uniqueSessionIdKycMedia.includes(n)).filter(nn => nn);
    if(remainingSessionIds.length > 0) {
      for(let i=0; i<remainingSessionIds.length; i++) {
        const documentTypeObject = await this.apiProviderService.verifeyeCheckPromise(remainingSessionIds[i]);
        const mediaContent = await this.apiProviderService.getKycMediaIds(remainingSessionIds[i]);
        // console.log("mediaContent: "+ JSON.stringify(mediaContent))
        if(mediaContent) {
          const backupArray: Array<GDriveBackupObject> = [];

          for (var k of mediaContent["media"] ) {
            const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
            const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.verifeye);
            const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());
            if(!!k.kycMediaId && ['front', 'back'].includes(k.mediaIdentifier)){
              const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
              if(mediafile) {
                const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, true);
                this.mediaContentArray.push({
                  id: watermarked.src.length,
                  image: watermarked.src,
                  imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                  documentType: (documentTypeObject as any).kycDocumentType,
                  sessionId: remainingSessionIds[i],
                  lastmodified: (k.lastModified ? k.lastModified : null),
                  verifiedBy
                })
                this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
              }
            }
          }
          this.uploadMediaOnCloud(backupArray);
        }
      }
    }
  }

  public async uploadMediaOnCloud(backupArray: Array<GDriveBackupObject>) {
    // console.log('KycmediaService#uploadMediaOnCloud; backupArray.length', backupArray.length);

    if (!backupArray || backupArray.length === 0) {
      return;
    }

    if(this._Plaform.is('ios') && this._Plaform.is('hybrid')) {
      const path = this._File.documentsDirectory;


      const promises = [];

      backupArray.forEach((backupObject) => {
        promises.push(this.storeToiCloud(path, backupObject.fileName, backupObject.contents));
      });


      Promise.all(promises)
        .then(
          () => {
            this.presentToastOpener();
          }
        )
        .catch(
          (error) => {
            this.presentToastOpener();
          }
        )
    } else {
      this.presentKYCAlertForPermission(backupArray);
    }
  }


  private storeToiCloud(path: string, fileName: string, contents: string): Promise<void> {
    return new Promise(
      async(resolve, reject) => {
        const fullPath = path + fileName;

        const b64Data = contents.replace("data:image/png;base64,", "");
        const byteCharacters = window.atob(b64Data);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const fileBlob = new Blob([byteArray], {type: "image/png"});

        await this._File.writeFile(path, fileName, fileBlob, { replace: true });

        iCloudDocStorage.initUbiquitousContainer(environment.icloud, (s) => {
          iCloudDocStorage.syncToCloud(fullPath, (ss) => {
            resolve();
          }, (e) => {
            reject();
          });
        });
      }
    );
  }

  private presentToastOpener() {
    iCloudDocStorage.fileList("Cloud", (a) => {
      const aaa = Array.from(a, aaaa => Object.keys(aaaa)[0]);
      // const check = aaa.find(aa => aa.includes(fileName) );
      this._UserActivity.updateActivity(ActivityKeys.KYC_DATA_OBTAINED);
      // if(check) {
      //   this.presentToastWithOpen(this._Translate.instant('KYCMEDIABACKUP.notification-iCloud'), check);
      // }
    }, (b) => {
      console.log("b", b);
    })
  }

  private presentKYCAlertForPermission(backupArray: Array<GDriveBackupObject>) {
    // console.log('KycmediaService#presentKYCAlertForPermission; backupArray.length', backupArray.length);

    this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant("GOOGLEDRIVE_PERMISSION_HEADER"),
      message: this._Translate.instant("GOOGLEDRIVE_PERMISSION_KYC_MESSAGE"),
      buttons: [
        {
          text: this._Translate.instant('SETTINGS.no'),
          role: "cancel",
          handler: () => {}
        },
        {
          text: this._Translate.instant('SETTINGS.yes'),
          handler: () => {
            this._GDrive.init(backupArray, this._Translate.instant('KYCMEDIABACKUP.notification-GoogleDrive'))
                  .then(
                    (status) => {
                      if(status) {
                        this._UserActivity.updateActivity(ActivityKeys.KYC_DATA_OBTAINED);
                      }
                    }
                  );
          }
        }
      ]
    }).then(al => al.present())
  }

  private presentToastWithOpen(message: string, filePath: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
      cssClass: 'toast-notification-open',
      buttons: [
        {
          side: 'end',
          icon: 'folder',
          text: this._Translate.instant('APPSTORE.open'),
          handler: () => {
            this._FileOpener.open(filePath, "image/png").then((aaa) => {
              console.log("aaa", aaa);
            }).catch(bbb => {
              console.log("bbb", bbb);
            })
          },
        }
      ]
    }).then(toast => toast.present())
  }

  public async returnProviderDocumentPair() {

    var kycStatusString = await this.secureStorage.getValue(SecureStorageKey.kycStatus, false);
    var kycStatus: KycStatus[] = !!kycStatusString ? JSON.parse(kycStatusString): [];
    var ondatoStatuses = Array.from(kycStatus, k => {
      if(!!k.additionalInfo) {
        if(!!(k as any).additionalInfo.ondatoSessionId) {
          return (k as any).additionalInfo.ondatoSessionId;
        }
      }
    }).filter(_ => !!_);
    let mediaItem: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();

    var ob = {};

    for(let i=0; i<ondatoStatuses.length; i++) {
      var j = mediaItem.find(mi => mi.sessionId == ondatoStatuses[i]);
      if(j) {
        ob = Object.assign(ob, { [ondatoStatuses[i]]: j.documentType })
      }
    }

    return ob;

  }

  public async populateMediaArray(exist: boolean) {
    // console.log('KycmediaService#populateMediaArray; exist', exist);

    var kycStatus = await this.secureStorage.getValue(SecureStorageKey.kycStatus, false);
    let kycMedia: KycMedia[] = this.kycMediaServiceAktia.getAllKycMedia();
      if(!!kycStatus && kycStatus !== "[]"){
        var kycStatusArray: Array<KycStatus> = !!kycStatus ? JSON.parse(kycStatus) : [];
        var filteredKycStatusArray: Array<KycStatus> = []; // Change this

        var kc = Object.keys(KycProvider);

        // console.log("Object.keys(KycProvider)");
        // console.log(kc);
        // console.log(kycStatusArray);
        for(let kcc = 0; kcc < kc.length; kcc ++) {
          var ck = kycStatusArray.filter(fl => (fl.kycProvider == kc[kcc] && ![KycState.KYC_INITIATED, KycState.KYC_FAILED].includes(fl.kycState) ));
          // console.log(ck);
          if(ck.length > 0) {
            filteredKycStatusArray.push(ck.find(ggg => ggg.kycInitiatedTimeStamp == Math.max(...Array.from(ck, gggg => gggg.kycInitiatedTimeStamp))));
            // console.log(filteredKycStatusArray);
          }
        }

        if(filteredKycStatusArray.length > 0) {
          for(let elem of filteredKycStatusArray) {
            if(elem.hasOwnProperty("additionalInfo")){
              if(elem.additionalInfo.hasOwnProperty("veriffSessionId")){
                // console.log("veriffSessionId: " + elem.additionalInfo.veriffSessionId);
                const documentTypeObject = await this.apiProviderService.veriffCheckPromise(elem.additionalInfo.veriffSessionId);
                const mediaContent = await this.apiProviderService.getKycMediaIds(elem.additionalInfo.veriffSessionId);
                // console.log("mediaContent: "+ JSON.stringify(mediaContent))
                if(mediaContent) {
                  const backupArray: Array<GDriveBackupObject> = [];

                  for (let k of mediaContent["media"] ) {
                    const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                    const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.veriff);
                    const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());
                    // console.log("k: " + k);
                    if(!!k.kycMediaId && ['document-front', 'document-back'].includes(k.mediaIdentifier)){
                      const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                      if(mediafile) {
                        const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, false);
                        this.mediaContentArray.push({
                          id: watermarked.src.length,
                          image: watermarked.src,
                          imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                          // documentType: (elem.kycProvider).split('_')[2],
                          documentType: (!!(documentTypeObject as any).kycDocumentType ? (documentTypeObject as any).kycDocumentType : (elem.kycProvider).split('_')[2]),
                          sessionId: elem.additionalInfo.veriffSessionId,
                          lastmodified: (k.lastModified ? k.lastModified : null),
                          verifiedBy
                        })
                        // console.log(this.mediaContentArray);
                        this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                        backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
                        if(! exist ) {
                          const timestamp = (Math.max.apply(null, (this.mediaContentArray.map(e => Number(e.lastmodified))).filter(function (x) { return isFinite(x); })))
                          await this.secureStorage.setValue(SecureStorageKey.kycMediaTimestamp, timestamp.toString());
                        }
                      }
                    }
                  }

                  this.uploadMediaOnCloud
                }
              }
              if(elem.additionalInfo.hasOwnProperty("verifeyeSessionId")){
                // console.log("verifeyeSessionId: " + elem.additionalInfo.verifeyeSessionId);
                var documentTypeObject = await this.apiProviderService.verifeyeCheckPromise(elem.additionalInfo.verifeyeSessionId);
                var mediaContent = await this.apiProviderService.getKycMediaIds(elem.additionalInfo.verifeyeSessionId);
                // console.log("mediaContent: "+ JSON.stringify(mediaContent))
                if(mediaContent) {
                  const backupArray: Array<GDriveBackupObject> = [];

                  for (let k of mediaContent["media"] ) {
                    const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                    const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.verifeye);
                    const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());
                    // console.log("k: " + k);
                    if(!!k.kycMediaId && ['front', 'back'].includes(k.mediaIdentifier)){
                      const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                      if(mediafile) {
                        const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, true);
                        this.mediaContentArray.push({
                          id: watermarked.src.length,
                          image: watermarked.src,
                          imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                          documentType: (!!(documentTypeObject as any).kycDocumentType ? (documentTypeObject as any).kycDocumentType : (elem.kycProvider).split('_')[2]),
                          // documentType: (elem.kycProvider).split('_')[2],
                          sessionId: elem.additionalInfo.verifeyeSessionId,
                          lastmodified: (k.lastModified ? k.lastModified : null),
                          verifiedBy
                        })
                        // console.log(this.mediaContentArray);
                        this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                        backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
                        if(! exist ) {
                          const timestamp = (Math.max.apply(null, (this.mediaContentArray.map(e => Number(e.lastmodified))).filter(function (x) { return isFinite(x); })))
                          await this.secureStorage.setValue(SecureStorageKey.kycMediaTimestamp, timestamp.toString());
                        }
                      }
                    }
                  }

                  this.uploadMediaOnCloud(backupArray);
                }
              }
              if(elem.additionalInfo.hasOwnProperty("ondatoSessionId")){
                const documentTypeObject = await this.apiProviderService.ondatoCheckPromise(elem.additionalInfo.ondatoSessionId);
                // console.log("ondatoSessionId: " + elem.additionalInfo.ondatoSessionId);
                // console.log("ondatoCheck: " + JSON.stringify(documentTypeObject));
                const mediaContent = await this.apiProviderService.getKycMediaIds(elem.additionalInfo.ondatoSessionId);
                // console.log("mediaContent: "+ JSON.stringify(mediaContent))
                if(mediaContent) {
                  const backupArray: Array<GDriveBackupObject> = [];

                  for (let k of mediaContent["media"] ) {

                    const verifiedBy = (!!mediaContent["trustProviderProcess"]["trustProviderClearName"] ? mediaContent["trustProviderProcess"]["trustProviderClearName"] : environment.did.ondato);
                    const displayDate = (!!k.lastModified ? new Date(k.lastModified).toISOString() : new Date().toISOString());
                    // console.log("k: " + k);
                    if(!!k.kycMediaId && ['front', 'back', 'document'].includes(k.mediaIdentifier)){

                      if(!kycMedia.find(km => (km.id == k.kycMediaId))) {
                        const mediafile = await this.apiProviderService.getKycMediaFiles(k.kycMediaId);
                        if(mediafile) {
                          const watermarked = await this.apiProviderService.addWaterMarkToImage(mediafile, '../../assets/IconJ-scaled.png', `${verifiedBy}`, `${displayDate}`, false);
                          this.mediaContentArray.push({
                            id: k.kycMediaId,
                            image: watermarked.src,
                            imageIdentifier: (k.mediaIdentifier ? k.mediaIdentifier : null),
                            documentType: (documentTypeObject as any).kycDocumentType,
                            sessionId: elem.additionalInfo.ondatoSessionId,
                            lastmodified: (k.lastModified ? k.lastModified : null),
                            verifiedBy
                          })
                          // console.log(this.mediaContentArray);
                          this.kycMediaServiceAktia.setKycMedia(this.mediaContentArray);
                          backupArray.push({ fileName: `${(documentTypeObject as any).kycDocumentType}-${k.kycMediaId}.png`, contents: watermarked.src});
                      }
                        if(! exist ) {
                          const timestamp = (Math.max.apply(null, (this.mediaContentArray.map(e => Number(e.lastmodified))).filter(function (x) { return isFinite(x); })))
                          await this.secureStorage.setValue(SecureStorageKey.kycMediaTimestamp, timestamp.toString());
                        }
                      }
                    }
                  }

                  this.uploadMediaOnCloud(backupArray);
                }
              }
            }
          }
        }
      }
    }

}
