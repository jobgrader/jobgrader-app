import { HostListener, Injectable } from '@angular/core';
import { SubscriptionLike, delay, filter, interval, of, take } from 'rxjs';
import { UtilityService } from '@services/utillity/utility.service';
import { TranslateProviderService } from '@services/translate/translate-provider.service';

@Injectable({
  providedIn: 'root'
})
export class LanguageSelectorService {

  private stylesheetId = 'select-hide';

  constructor(
    private _Translate: TranslateProviderService
  ) { }

  public addSelectBoxAdditionalElements(): void {
    if (!document.querySelector(`#${this.stylesheetId}`)) {
        this.addStyleSelectToHead();
    }
    interval(1)
        .pipe(
            filter(_ => !!document.querySelector('.alert-checkbox-group')),
            take(1)
        ).toPromise().then(_ => {
        return this.createLoadingSpinner();
    }).then(_ => {
        return Promise.all([
            this.addSearchBar(),
        ]);
    }).then(_ => {
        UtilityService.setTimeout(100)
            .subscribe(_ => {
                this.removeLoadingSpinner();
                UtilityService.removeStyleFromHead(this.stylesheetId);
            });
    });
}

private removeLoadingSpinner(): void {
  document.querySelector('.loading-overlay-select').remove();
}

private createLoadingSpinner(): void {
  const div = document.createElement('DIV');
  const img: any = document.createElement('IMG');
  img.src = '../../../../assets/spinner.gif';
  div.appendChild(img);
  div.style.position = 'absolute';
  div.style.top = '0';
  div.style.bottom = '0';
  div.style.left = '0';
  div.style.right = '0';
  div.style.background = 'var(--background)';
  div.classList.add('loading-overlay-select');
  img.style.width = '40px';
  img.style.marginLeft = 'calc(50% - 20px)';
  img.style.marginTop = 'calc(50% - 20px)';
  const group: any = document.querySelector('.alert-checkbox-group');
  group.style.position = 'relative';
  group.appendChild(div);
}

public addStyleSelectToHead(): void {
  const styles = `
      ion-alert .alert-checkbox-group .alert-checkbox-button {
          opacity: 0
      }
  `;
  UtilityService.addStyleToHead(styles, this.stylesheetId);
}

  addSearchBar() {
    return new Promise<void>(resolve => {
        const addSearchBarTimeout: SubscriptionLike = of(true).pipe(delay(50)).subscribe(() => {
            const bar: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-head');
            const text: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-title');
            const message: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-message');
            const itemsContainer: NodeListOf<HTMLElement> = document.querySelectorAll('.alert-checkbox-group');
            if ( bar && text ) {
                bar.forEach((i, index) => {
                    if ( document.querySelector('.alert-wrapper ion-searchbar') ) {
                        return;
                    }
                    const searchBar = document.createElement('ion-searchbar');
                    searchBar.placeholder = this._Translate.instant('BUTTON.SEARCH');
                    i.appendChild(searchBar);
                    // if (text && text.length > 0 && text[index]) {
                    //     text[index].style.display = 'none';
                    // }
                    // message[index].style.display = 'none';

                    const itemsDummy = Array.from(itemsContainer);
                    const items = Array.from(itemsDummy[0].childNodes);

                    searchBar.addEventListener('ionInput', handleInput);

                    function handleInput(event) {
                        const query = event.target.value.toLowerCase();
                        requestAnimationFrame(() => {
                            items.forEach((item) => {
                                const myNode = (item.childNodes[0].childNodes[1] as HTMLScriptElement);
                                const elem = item as HTMLElement;
                                const shouldShow = myNode ? myNode.innerText.toLowerCase().indexOf(query) > -1 : null;
                                elem.style.display = shouldShow ? 'block' : 'none';
                            });
                        });
                    }
                });
            }
            resolve();
            addSearchBarTimeout.unsubscribe();
        });
    });
}


}
