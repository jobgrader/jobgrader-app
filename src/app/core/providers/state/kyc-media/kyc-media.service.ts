import { Injectable } from '@angular/core';


// custom imports
import { KycMediaStore } from './kyc-media.store';
import { KycMedia } from './kyc-media.model';
import { KycMediaQuery } from './kyc-media.query';


@Injectable({ providedIn: 'root' })
export class KycMediaServiceAkita {

  constructor(
    private query: KycMediaQuery,
    private store: KycMediaStore,
  ) {
  }

  public getAllKycMedia(): KycMedia[] {
    return this.query.getAll()
  }

  public setKycMedia(kycMedia: KycMedia[]) {
    this.store.set(kycMedia);
  }

  public clear() {
    this.setKycMedia([]);
  }
}
