import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { produce } from 'immer';


// custom imports
import { createKycMedia, KycMedia } from './kyc-media.model';


export interface KycMediaState extends EntityState<KycMedia> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'kyc-media', idKey: 'id', producerFn: produce })
export class KycMediaStore extends EntityStore<KycMediaState> {

  constructor() {
    super(createKycMedia());
  }

}
