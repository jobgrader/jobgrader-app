import { Injectable } from '@angular/core';


// custom imports
import { SocialMediaTaskStore } from './social-media-task.store';
import { SocialMediaTask } from './social-media-task.model';
import { SocialMediaTaskQuery } from './social-media-task.query';


@Injectable({ providedIn: 'root' })
export class SocialMediaTaskServiceAkita {

  constructor(
    private query: SocialMediaTaskQuery,
    private store: SocialMediaTaskStore,
  ) {
  }

  public getAllSocialMediaTasks(): SocialMediaTask[] {
    return this.query.getAll()
  }

  public setSocialMediaTasks(socialMediaTasks: SocialMediaTask[]) {
    this.store.set(socialMediaTasks);
  }

  public clear() {
    this.setSocialMediaTasks([]);
  }
}
