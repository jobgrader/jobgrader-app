import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { produce } from 'immer';


// custom imports
import { createSocialMediaTask, SocialMediaTask } from './social-media-task.model';


export interface SocialMediaTaskState extends EntityState<SocialMediaTask> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'social-media-task', idKey: 'taskId', producerFn: produce })
export class SocialMediaTaskStore extends EntityStore<SocialMediaTaskState> {

  constructor() {
    super(createSocialMediaTask());
  }

}
