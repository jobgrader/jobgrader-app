export enum TargetApp {
  discord = 'discord',
  whatsapp = 'whatsapp',
  facebook = 'facebook',
  twitter = 'twitter',
  linkedin = 'linkedin',
  mailchimp = 'mailchimp',
  telegram = 'telegram'
}

export enum TaskCode {
  follow = 'follow',
  post = 'post',
  subscribe = 'subscribe',
  share = 'share',
}

export interface SocialMediaTask {
  taskId: string;
  targetApp: TargetApp;
  targetChannel: string;
  taskCode: TaskCode;
  taskDescription: string;
  earnings: number;
  earningsToken: string;
  shareContent: string;
  active: boolean;
  done: boolean;
  language: string;
  inviteUrl?: string;
}

export function createSocialMediaTask(): SocialMediaTask {
  return {

  } as SocialMediaTask;
}