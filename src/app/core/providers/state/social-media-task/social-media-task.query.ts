import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';


// custom imports
import { SocialMediaTaskState, SocialMediaTaskStore } from './social-media-task.store';


@Injectable({ providedIn: 'root' })
export class SocialMediaTaskQuery extends QueryEntity<SocialMediaTaskState> {

  constructor(protected store: SocialMediaTaskStore) {
    super(store);
  }

}
