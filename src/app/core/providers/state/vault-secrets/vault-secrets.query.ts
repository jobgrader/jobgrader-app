import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';


// custom imports
import { VaultSecrets } from './vault-secrets.model';
import { VaultSecretsStore } from './vault-secrets.store';


@Injectable({ providedIn: 'root' })
export class VaultSecretsQuery extends Query<VaultSecrets> {

  constructor(protected store: VaultSecretsStore) {
    super(store);
  }

}
