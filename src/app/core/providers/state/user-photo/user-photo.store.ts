import { Injectable } from '@angular/core';
import { Store, StoreConfig, UpdateStateCallback } from '@datorama/akita';


// custom imports
import { createUserPhoto, UserPhoto,  } from './user-photo.model';


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'user-photo' })
export class UserPhotoStore extends Store<UserPhoto> {

  constructor() {
    super(createUserPhoto());
  }


}
