import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import * as crypto from 'crypto';
import { AlertController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';
import { TranslateProviderService } from '@services/translate/translate-provider.service';
import { Clipboard } from '@capacitor/clipboard';
import { LoaderProviderService } from '@services/loader/loader-provider.service';

interface TelegramUser {
  id: number;
  first_name: string;
  last_name: string;
  username: string;
  photo_url: string;
  auth_date: number;
  hash: string;
}

interface CheckTelegramDataResponse {
  success: boolean;
  error?: string;
  data?: any;
}

@Injectable({
  providedIn: 'root'
})
export class TelegramService {

  public user: TelegramUser;

  private botToken: string = environment.telegram.bot.token;
  private botUsername: string = environment.telegram.bot.token.split(":")[0];
  private redirectUri: string = environment.telegram.redirectUri;

  constructor(
    private inAppBrowser: InAppBrowser,
    private toast: ToastController,
    private http: HttpClient,
    private translate: TranslateProviderService,
    private alertController: AlertController,
    private firebase: FirestoreCloudFunctionsService,
    private loader: LoaderProviderService
  ) { }

  private generateAuthUrl(): string {
    const telegramAuthUrl = `https://oauth.telegram.org/auth?bot_id=${this.botUsername}&origin=${encodeURIComponent(this.redirectUri)}&request_access=write`;
    return telegramAuthUrl;
  }

  private presentToast(message: string) {
    this.toast.create({
      message,
      position: "top",
      duration: 2000
    }).then(t => t.present());
  }

  openTelegramOAuth(): Promise<boolean> {
    return new Promise((resolve) => {

      if (!this.user) {
        const url = this.generateAuthUrl();

        const browser = this.inAppBrowser.create(url, '_blank', { location: 'no', zoom: 'no' });

        browser.on('loadstart').subscribe(async (event) => {
          if (event.url.startsWith(this.redirectUri)) {

            browser.close();

            this.loader.loaderCreate(this.translate.instant('LOADER.telegram')).then(() => {
              // alert(event.url);

              const url = new URL(event.url);
              const hashValue = url.hash.split("#tgAuthResult=")[1];
              const buffer = Buffer.from(hashValue, 'base64');
              const decodedResponse = buffer.toString('utf-8');
              const userData: TelegramUser = JSON.parse(decodedResponse);

              const isValid = this.verifyAuthData(userData);

              // alert(decodedResponse + ": " + isValid);

              if (isValid) {
                this.checkTelegramData(userData.id.toString()).then(v => {
                  // alert(JSON.stringify(v));

                  if (v) {
                    this.user = userData;
                    alert(this.translate.instant('TELEGRAM_ACCOUNT_LINKED'));
                    // this.presentToast(`Logged in as ${this.user.username}`);
                    this.loader.loaderDismiss();
                    resolve(true);
                  } else {
                    // this.presentToast(v.error);
                    this.loader.loaderDismiss();
                    resolve(false);
                  }
                }).catch(e => {
                  this.loader.loaderDismiss();
                  resolve(false);
                })

              } else {
                this.loader.loaderDismiss();
                resolve(false);
              }
            })


          }
        });
      } else {
        resolve(true);
      }


    })
  }

  async checkTelegramData(id: string): Promise<CheckTelegramDataResponse> {
    const body = await this.firebase.encryptBody({ id });

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.telegramDatachecker}`, body).subscribe({
        next: (response: CheckTelegramDataResponse) => {
          resolve(response);
        },
        error: (error) => reject(error)
      })
    })
  }

  verifyAuthData(authData: TelegramUser): boolean {
    const secretKey = crypto.createHash('sha256').update(this.botToken).digest();
    const dataCheckString = Object.keys(authData)
      .filter(key => key !== 'hash')
      .sort()
      .map(key => `${key}=${authData[key as keyof TelegramUser]}`)
      .join('\n');

    const hmac = crypto.createHmac('sha256', secretKey)
      .update(dataCheckString)
      .digest('hex');

    return hmac === authData.hash;
  }


  postOnTelegram(inviteUrl: string, content: string) {

    const openInstance = () => {
      this.alertController.create({
        header: "Telegram",
        message: this.translate.instant('TELEGRAM_OPEN_DISCLAIMER'),
        buttons: [
          {
            text: this.translate.instant('SETTINGS.yes'),
            handler: () => {
              Clipboard.write({ string: content }).then(() => {
                this.inAppBrowser.create(inviteUrl, '_system');
              })
            }
          },
          {
            text: this.translate.instant('SETTINGS.no'),
            role: "cancel",
            handler: () => {

            }
          }
        ]
      }).then(a => {
        a.present();
      })
    }

    if (!!this.user) {
      openInstance();
    } else {
      this.openTelegramOAuth().then(r => {
        // if (r) {
        openInstance();
        // }
      })
    }
  }

  followOnTelegram(inviteUrl: string) {

    const openInstance = (inviteLink: string) => {
      this.inAppBrowser.create(inviteLink, '_system');
    }

    const processApiResponse = (inviteLink: string) => {
      if (!!this.user) {
        openInstance(inviteLink);
      } else {
        this.openTelegramOAuth().then(r => {
          // if (r) {
          openInstance(inviteLink);
          // }
        })
      }
    }

    processApiResponse(inviteUrl);

  }
}
