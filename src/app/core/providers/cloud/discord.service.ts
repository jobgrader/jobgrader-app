import { Injectable } from '@angular/core';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { AlertController, ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { Clipboard } from '@capacitor/clipboard';
import { SocialMediaService } from '../social-media/social-media.service';

@Injectable({
  providedIn: 'root'
})
export class DiscordService {

  public user = null;
  private state = null;

  constructor(
    private inAppBrowser: InAppBrowser,
    private toast: ToastController,
    private socialMediaService: SocialMediaService,
    private alertController: AlertController,
    private translate: TranslateProviderService
    ) {}

  openDiscordOAuth(): Promise<boolean> {

    return new Promise((resolve) => {
      this.state = this.generateRandomString();
      const discordAuthUrl = `${environment.discord.oauth_base_url}?state=${this.state}&client_id=${environment.discord.client_id}&response_type=code&redirect_uri=${encodeURIComponent(environment.discord.redirect_uri)}&scope=guilds+guilds.join+email+connections+identify`; 
      const browser = this.inAppBrowser.create(discordAuthUrl, '_blank', { location: 'no', zoom: 'no' });
      
      browser.on('loadstart').subscribe((event) => {
        if (event.url.startsWith(environment.discord.redirect_uri)) {
          browser.close();
          setTimeout(() => {
              this.socialMediaService.obtainDiscordPersonalInfo(this.state).then((meResponse) => {
                this.user = meResponse;
                console.info(this.user);
                this.socialMediaService.updateDiscordId(this.user.me.id).then((r) => {
                  resolve(true);
                }).catch(e => {
                  resolve(false);
                })
              })
          }, 2000)
        }
      });
    })

  }

  postOnDiscord(channelId: string, content: string) {

      const openInstance = () => {
        this.alertController.create({
          header: "Discord",
          message: this.translate.instant('DISCORD_OPEN_DISCLAIMER'),
          buttons: [
            {
              text: this.translate.instant('SETTINGS.yes'),
              handler: () => {
                Clipboard.write({ string: content }).then(() => {
                  this.inAppBrowser.create(channelId, '_blank');
                })
              }
            },
            {
              text: this.translate.instant('SETTINGS.no'),
              role: "cancel",
              handler: () => {

              }
            }
          ]
        }).then(a => {
          a.present();
        })
      }
  
      if(!!this.user) {
        openInstance();
      } else {
        this.openDiscordOAuth().then(r => {
          if(r) {
            openInstance()
          }
        })
      }
  }

  followOnDiscord(guildId: string, inviteUrl: string) {

    const openInstance = (inviteLink: string) => {
      this.inAppBrowser.create(inviteLink, '_blank');
    }

    const processApiResponse = (inviteLink: string) => {
      if(!!this.user) {
        openInstance(inviteLink);
      } else {
        this.openDiscordOAuth().then(r => {
          if(r) {
            openInstance(inviteLink);
          }
        })
      }
    }

    // this.socialMediaService.fetchDiscordInviteLink(guildId).subscribe({
    //   next: (url) => {
    //     console.info("Discord Invite URL: ", url);
    //     if(!!url) {
    //       processApiResponse(url);
    //     } else {
    processApiResponse(inviteUrl);
    //     }
    //   }, 
    //   error: (error) => {
    //     console.error("Discord Invite Error: ", error);
    //     processApiResponse(inviteUrl);
    //   }
    // })

      
  
      

  }

  private generateRandomString() {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < 25) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }

  logOut() {
    this.user = null;
  }

  private presentToast(message: string) {
    this.toast.create({
      message,
      duration: 2000,
      position: 'top'
    }).then((t) => {
      t.present();
    })
  }
   
}
