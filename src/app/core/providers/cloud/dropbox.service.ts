import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import * as Dropbox from 'dropbox';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { HttpClient } from '@angular/common/http';

declare var window: any;

var app_key = "1j8mwo3m4p2spku";

var dbxAuth = new Dropbox.DropboxAuth({
  clientId: app_key,
});

export interface DropboxBackupObject {
  fileName: string;
  contents: string;
}

@Injectable({
  providedIn: 'root'
})

export class DropboxService {

  public access_token = null;
  public user = null;

  constructor(
    private _HttpClient: HttpClient,
    private _InAppBrowser: InAppBrowser,
    private _Translate: TranslateProviderService,
    private _ToastController: ToastController
  ) {

  }

  async backupFiles(dropboxBackupObjects: Array<DropboxBackupObject>, notification: string) {

    if (this.access_token) {
      const dbx = new Dropbox.Dropbox({ accessToken: this.access_token, fetch });
      // var folderContent = await dbx.filesListFolder({ path: '' });

      // console.log(folderContent.result);

      var parsedUploadResponse = null;

      for (let i = 0; i < dropboxBackupObjects.length; i++) {
        var fileName = dropboxBackupObjects[i].fileName;
        var content = dropboxBackupObjects[i].contents;

        const file = new Blob([content], { type: "text/plain" });
        const form = new FormData();
        // form.append('content', file);
        form.append('file', file);

        parsedUploadResponse = await dbx.filesUpload({
          path: `/${fileName}`,
          contents: file
        })

        console.log(parsedUploadResponse);

      }

      if (!!parsedUploadResponse) {
        this.presentToast(notification);
      }

    }

  }

  requestCall(url: string, body: any): Promise<any> {

    return new Promise((resolve, reject) => {
      this._HttpClient.post(url, body, {
        headers: {
          'Authorization': `Bearer ${this.access_token}`,
          'Content-Type': 'application/json'
        }
      }).subscribe({
        next: (response) => {
          resolve(response);
        },
        error: (err) => {
          reject(err);
        }
      })
    })

  }

  async getUser() {
    this.user = await this.requestCall("https://api.dropboxapi.com/2/users/get_current_account", null);
    return this.user;
  }


  async signInToDropbox(REDIRECT_URI: string) {

    if (!this.access_token) {

      var code = new URL(window.location.href).searchParams.get('code');

      if (!code) {
        var authorizationURL = await dbxAuth.getAuthenticationUrl(REDIRECT_URI, undefined, 'code', 'offline', undefined, undefined, true);
        var codeVerifier = dbxAuth.getCodeVerifier();
        window.sessionStorage.setItem("codeVerifier", codeVerifier);

        this._InAppBrowser.create(authorizationURL.toString(), "_system");

      } else {
        this.processDropboxAuthToken(REDIRECT_URI, code);
      }

    } else {

      const dbx = new Dropbox.Dropbox({ accessToken: this.access_token, fetch });
      var folderContent = await dbx.filesListFolder({ path: '' });

      console.log(folderContent.result);

    }

  }

  async processDropboxAuthToken(REDIRECT_URI: string, code: string) {

    if (code) {

      var codeVerifier = window.sessionStorage.getItem("codeVerifier");
      dbxAuth.setCodeVerifier(codeVerifier);
      var response = await dbxAuth.getAccessTokenFromCode(REDIRECT_URI, code);
      this.access_token = (response.result as any).access_token;

      dbxAuth.setAccessToken(this.access_token);
      this.user = await this.getUser();

      this.presentToast(`Dropbox: ${this.user.email}: ${this._Translate.instant('CHAT.connected')}`);
    }

  }

  logOut() {
    this.access_token = null;
    this.user = null;
    this.presentToast(`Dropbox: ${this._Translate.instant('WALLETCONNECT_V2.disconnection_successful')}`);
  }


  presentToast(message: string) {
    this._ToastController.create({
      position: 'top',
      duration: 3000,
      message,
    }).then(toast => toast.present())
  }

}
