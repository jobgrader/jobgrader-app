import { Injectable } from '@angular/core';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';

import { environment } from 'src/environments/environment';
import { ApiProviderService } from '../api/api-provider.service';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LinkedinService {

  public user_data = null;
  public user_certificates = [

  ];
  public user_languages = [

  ];
  public user_education = [

  ];
  private permissions = ["openid", "profile", "email", "w_member_social", "r_basicprofile", "r_organization_social"];

  constructor(
    private inAppBrowser: InAppBrowser,
    private api: ApiProviderService,
    private toast: ToastController
  ) {}

  openLinkedInOAuth() {

    const state = this.generateRandomString();
    const redirect_uri = environment.apiUrl + "/webhook/linkedin";
    const oauth_url = `https://www.linkedin.com/oauth/v2/authorization?state=${state}&response_type=code&client_id=${environment.linkedin.client_id}&redirect_uri=${encodeURIComponent(redirect_uri)}&scope=${encodeURIComponent(this.permissions.join(" "))}`;

    const browser = this.inAppBrowser.create(oauth_url, '_blank', { location: 'no', zoom: 'no' });
    browser.on('loadstart').subscribe((event) => {
      if (event.url.startsWith(redirect_uri)) {
        browser.close();
        try {
          setTimeout(() => {
            this.api.obtainLinkedinUserData(state).then(response => {
                  this.user_data = response;
                  console.log(this.user_data);
                  this.api.obtainLinkedinMeData(state).then(me_data => {
                    this.user_certificates = !!me_data.certifications ? me_data.certifications : [];
                    this.user_languages = !!me_data.languages ? me_data.languages : [];
                    this.user_education = !!me_data.education ? me_data.education : [];

                    this.api.checkIfUserFollowsLinkedinCompany(state, "urn:li:organization:100391862")
                    .then((company_follow_response) => {
                      if(!!company_follow_response){
                        if(!!company_follow_response.elements){
                          if(company_follow_response.elements.length > 0) {
                            if(!!company_follow_response.elements[0].following) {
                              this.presentToast('Great! You are following the Jobgrader company page');
                            } else {
                              this.presentToast('You are not following the Jobgrader company page!');
                            }
                          }
                        }
                      }
                      this.deleteLinkedinData(state);
                    }).catch(ee => {
                      console.log(ee);
                      this.deleteLinkedinData(state);
                    })

                  }).catch(e => {
                    console.log(e);
                  })

            }).catch(eee => {
              console.log(eee);
            })
          }, 1000)
        } catch(e) {
          console.log(e);
        }
      }
    });
  }

  private deleteLinkedinData(state: string) {
    this.api.deleteLinkedinData(state).then(() => {
      console.log("Linkedin Process complete!");
    }).catch((eee) => {
      console.log(eee);
    });
  }

  private generateRandomString() {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < 25) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
      counter += 1;
    }
    return result;
  }

  private presentToast(message: string) {
    this.toast.create({
      message,
      position: 'top',
      duration: 2000
    }).then(toast => {
      toast.present();
    })
  }

  logOut() {
    this.user_data = null;
    this.user_certificates = [];
    this.user_education = [];
    this.user_languages = [];
  }
}
