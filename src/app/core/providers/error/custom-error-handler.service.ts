import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import * as SentryAngular from "@sentry/angular-ivy";


// custom imports
import { ApiProviderService } from '../api/api-provider.service';
import { AppStateService } from '../app-state/app-state.service';
import { environment } from '../../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class CustomErrorHandler implements ErrorHandler {

    constructor(
        public _ApiProviderService: ApiProviderService,
        public _AppStateService: AppStateService) {
    }

    /** @inheritDoc */
    public handleError(error: any): void {
        if (
            error.toString().indexOf('cordova_not_available') > -1
            // ||
            // error.rejection === 20 ||
            // error.toString().indexOf('No Image Selected') > -1 ||
            // error.toString().indexOf('No camera available') > -1 ||
            // error.toString().indexOf('overlay does not exist') > -1 ||
            // error.toString().indexOf('TypeError') > -1 ||
            // error.toString().indexOf('ExpressionChangedAfterItHasBeenCheckedError') > -1
        ) {
            return;
        }
        if (
            // error instanceof HttpErrorResponse && error.status !== 500 ||
            // error instanceof HttpErrorResponse && error.status !== 429 ||
            error instanceof HttpErrorResponse && error.status !== 401
        ) {
            return;
        }
        // if (!!error.code) {
        //     return;
        // }


        console.error(error);

        if(environment.production) {
            // log error to sentry
            try {
                SentryAngular.captureException(error.originalError || error);
            } catch (e) {
                // do nothing
            }
        }

    }

}
