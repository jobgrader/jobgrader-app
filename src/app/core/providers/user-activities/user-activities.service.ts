import { Injectable } from '@angular/core';
import { ApiProviderService } from '../api/api-provider.service';
import { DeviceService } from '../device/device.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../secure-storage/secure-storage.service';

export enum ActivityKeys {
  USER_CREATED = 'USER_CREATED',
  WALLET_CREATED = 'WALLET_CREATED',
  WALLET_IMPORTED = 'WALLET_IMPORTED',
  WALLET_FOLLOWED = 'WALLET_FOLLOWED',
  VERIFIED_BACKUP_CLOUD = 'VERIFIED_BACKUP_CLOUD',
  UNVERIFIED_BACKUP_CLOUD = 'UNVERIFIED_BACKUP_CLOUD',
  VERIFIED_BACKUP_ICLOUD = 'VERIFIED_BACKUP_ICLOUD',
  UNVERIFIED_BACKUP_ICLOUD = 'UNVERIFIED_BACKUP_ICLOUD',
  VERIFIED_BACKUP_GOOGLEDRIVE = 'VERIFIED_BACKUP_GOOGLEDRIVE',
  UNVERIFIED_BACKUP_GOOGLEDRIVE = 'UNVERIFIED_BACKUP_GOOGLEDRIVE',
  VERIFIED_BACKUP_DROPBOX = 'VERIFIED_BACKUP_DROPBOX',
  UNVERIFIED_BACKUP_DROPBOX = 'UNVERIFIED_BACKUP_DROPBOX',
  VERIFIED_BACKUP_ONEDRIVE = 'VERIFIED_BACKUP_ONEDRIVE',
  UNVERIFIED_BACKUP_ONEDRIVE = 'UNVERIFIED_BACKUP_ONEDRIVE',
  VERIFIED_BACKUP_LOCAL = 'VERIFIED_BACKUP_LOCAL',
  UNVERIFIED_BACKUP_LOCAL = 'UNVERIFIED_BACKUP_LOCAL',
  VERIFIED_BACKUP_PAPER = 'VERIFIED_BACKUP_PAPER',
  UNVERIFIED_BACKUP_PAPER = 'UNVERIFIED_BACKUP_PAPER',
  KYC_DATA_OBTAINED = 'KYC_DATA_OBTAINED',
  VC_BACKUP = 'VC_BACKUP',
  VP_BACKUP = 'VP_BACKUP'
}

export interface UserActivity {
  timestamp: any;
  activity: ActivityKeys;
  deviceId?: string;
  additionalInformation?: any;
}

@Injectable({
  providedIn: 'root'
})
export class UserActivitiesService {

  constructor(
    private _SecureStorage: SecureStorageService,
    private _ApiProvider: ApiProviderService,
    private _Device: DeviceService
  ) { }

  getActivityFromStorage(activity: ActivityKeys): Promise<UserActivity> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((userActivities) => {
        var v: Array<UserActivity> = !!userActivities ? JSON.parse(userActivities): [];
        var check = v.find(vv => vv.activity == activity);
        resolve(check);
      })
    })
  }

  updateActivity(activity: ActivityKeys, additionalInformation?: any): Promise<void> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((activities) => {
        var v: Array<UserActivity> = !!activities ? JSON.parse(activities): [];

        this._Device.getDeviceId().then((deviceId) => {
          var ob = !!additionalInformation ? { 
            timestamp: +new Date(), 
            activity,
            deviceId,
            additionalInformation
          } : { 
            timestamp: +new Date(), 
            activity,
            deviceId 
          };
          v.push(ob);
          this._ApiProvider.updateUserActivities(ob).then(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          }).catch(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          })
          
        }).catch(e => {
          var ob = { 
            timestamp: +new Date(), 
            activity
          };
          v.push(ob);
          this._ApiProvider.updateUserActivities(ob).then(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          }).catch(() => {
            this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(v)).then(() => {
              resolve();
            }).catch(e => {
              resolve();
            })
          })
        })
      })
    })
  }

  fetchActivities(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._ApiProvider.fetchUserActivities().then((response) => {
        var parsedResponse = <Array<UserActivity>>response.content.map(r => {
          const { activity, timestamp, deviceId, additionalInformation } = r;
          return { activity, timestamp, deviceId, additionalInformation };
        })
        this._SecureStorage.setValue(SecureStorageKey.userActivities, JSON.stringify(parsedResponse)).then(() => {
          resolve();
        }).catch(e => {
          resolve();
        })
      }).catch(e => {
        resolve();
      })
    })
  }

  getActivities(): Promise<Array<UserActivity>> {
    return new Promise((resolve, reject) => {
      this._SecureStorage.getValue(SecureStorageKey.userActivities, false).then((activities) => {
        var v: Array<UserActivity> = !!activities ? JSON.parse(activities): [];
        resolve(v);
      })
    })
  }

  getTest(): Promise<Array<UserActivity>> {
    return new Promise((resolve, reject) => {
      
        var v: Array<UserActivity> = [{
          timestamp: +new Date(),
          activity: ActivityKeys.USER_CREATED
        },{
          timestamp: +new Date(),
          activity: ActivityKeys.VERIFIED_BACKUP_ICLOUD
        },{
          timestamp: +new Date(),
          activity: ActivityKeys.UNVERIFIED_BACKUP_ICLOUD
        }]
        resolve(v);
      
    })
  }

}
