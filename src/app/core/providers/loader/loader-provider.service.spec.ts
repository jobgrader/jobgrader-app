import { TestBed } from '@angular/core/testing';

import { LoaderProviderService } from './loader-provider.service';

describe('LoaderProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoaderProviderService = TestBed.get(LoaderProviderService);
    expect(service).toBeTruthy();
  });
});
