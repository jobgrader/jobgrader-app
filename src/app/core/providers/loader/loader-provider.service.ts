import { Injectable } from '@angular/core';

import { LoadingController } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';

@Injectable({
  providedIn: 'root'
})
export class LoaderProviderService {

  public loader: any;

  constructor(private readonly loadingController: LoadingController, private translateProviderService: TranslateProviderService) { }

  async loaderCreate(message: string = this.translateProviderService.instant('LOADER.pleaseWait'), duration: number = 15000) {
    await this.loaderDismiss();

    this.loader = await this.loadingController.create({
      message,
      duration
    });
    await this.loader.present();
  }

  async loaderDismiss() {
    if (this.loader) {
      await this.loader.dismiss();
    }
  }

}
