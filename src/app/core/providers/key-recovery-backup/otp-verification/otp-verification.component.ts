import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateProviderService } from '../../translate/translate-provider.service';


@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.component.html',
  styleUrls: ['./otp-verification.component.scss'],
})
export class OtpVerificationComponent implements OnInit {
  @Input() user: any;

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
  ) { }

  ngOnInit() {
    
  }

  close() {
    this._ModalController.dismiss();
  }

  onCodeChanged(code: string) {

  }

  onCodeCompleted(code: string) {

    this._ModalController.dismiss({ value: code });
      
  }


}
