import { Injectable } from '@angular/core';
import { ModalController, NavController, Platform } from '@ionic/angular';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { DeviceService } from '../device/device.service';
import { SignProviderService } from '../sign/sign-provider.service';
import { ApiProviderService, OtpTypesForDeviceRestoration } from '../api/api-provider.service';
import { ethers, Wallet } from 'ethers';
import { AppStateService } from '../app-state/app-state.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { AlertController, ToastController } from '@ionic/angular';
import { DeeplinkProviderService } from '../deeplink/deeplink-provider.service';
import { StorageKeys, StorageProviderService } from '../storage/storage-provider.service';
import { environment } from '../../../../environments/environment';
// import { ChatService } from '../chat/chat.service';
import { KeyExchangeService } from '../chat/key-exchange.service';
import { SecurePinService } from '../secure-pin/secure-pin.service';
import { NetworkService } from '../network/network-service';
import { ImportUserkeyComponent } from 'src/app/login/import-userkey/import-userkey.component';
import { CryptoCurrency, EthereumNetworks } from '../wallet-connect/constants';
import { File } from '@awesome-cordova-plugins/file/ngx';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from '../device/udid.enum';
import { UserActivitiesService } from '../user-activities/user-activities.service';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { OtpVerificationComponent } from './otp-verification/otp-verification.component';
import { UserService } from '@services/user';

import { decrypt, generatePrivate, getPublic } from '@toruslabs/eccrypto';

declare var window: any;
declare var iCloudDocStorage: any;

@Injectable({
  providedIn: 'root'
})
export class KeyRecoveryBackupService {

  constructor(
    private _SecureStorageService: SecureStorageService,
    private _CryptoProviderService: CryptoProviderService,
    private _DeviceService: DeviceService,
    private _ApiProviderService: ApiProviderService,
    private _SignProviderService: SignProviderService,
    private deeplinkProviderService: DeeplinkProviderService,
    private storageService: StorageProviderService,
    private nav: NavController,
    private _AppStateService: AppStateService,
    private _AlertController: AlertController,
    private _ToastController: ToastController,
    private _LoaderProviderService: LoaderProviderService,
    private _TranslateProviderService: TranslateProviderService,
    private _KeyExchangeService: KeyExchangeService,
    private _SecurePinService: SecurePinService,
    // public _ChatService: ChatService,
    private _File: File,
    private _Vault: VaultService,
    private _Platform: Platform,
    private _UserActivity: UserActivitiesService,
    private _ModalController: ModalController,
    private _NetworkService: NetworkService,
    private userService: UserService,
  ) { }

  async init(otp?: string){
    const devicePrivateKeyEncryption = await this._SecureStorageService.getValue(SecureStorageKey.devicePrivateKeyEncryption, false);
      if(!devicePrivateKeyEncryption) {
        await this._CryptoProviderService.generateDeviceEncryptionKeyPair();
        await this._CryptoProviderService.generateDeviceKeyPair();
        return await this._DeviceService.pushDeviceInfo(otp);
    }
  }

  async pushKeys() {
    let deviceId = await this._SecureStorageService.getValue(SecureStorageKey.deviceInfo, false);

    if(!deviceId) {
      return;
    }

    var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();
    if(!backupSymmetricKey){
      return;
    }

    var body = [];
    // var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.deviceMnemonic, false)
    var devicePrivateKeyEncryption = await this._SecureStorageService.getValue(SecureStorageKey.devicePrivateKeyEncryption, false)

    // if(mnemonic) {
    //   var deviceDelegateMnemonic = await this._CryptoProviderService.symmetricEncrypt(mnemonic, backupSymmetricKey).catch(e => { console.log(e); return null; })
    //   if(deviceDelegateMnemonic) {
    //     body.push({
    //       keyName: 'deviceDelegateMnemonic',
    //       encryptedValue: deviceDelegateMnemonic
    //     })
    //   }
    // }

    if(devicePrivateKeyEncryption) {
      var devicePrivateKeyEncryptionEncryption = await this._CryptoProviderService.symmetricEncrypt(devicePrivateKeyEncryption, backupSymmetricKey).catch(e => { console.log(e); return null; })
      if(devicePrivateKeyEncryptionEncryption) {
        body.push({
          keyName: 'devicePrivateKeyEncryption',
          encryptedValue: devicePrivateKeyEncryptionEncryption
        })
      }
    }

    if(body.length > 0) {
      await this._ApiProviderService.postKeyBackup(deviceId, body);
    }

  }

  private async syncVC() {
    // console.log('KeyRecoveryBackup#syncVC');

    const vcIds = await this._ApiProviderService.getVcIds();
    // console.log('KeyRecoveryBackup#syncVC; vcIds:', vcIds);
    // console.log('KeyRecoveryBackup#syncVC; vcIds.length:', vcIds.length);

    if(vcIds.length > 0) {
      await this._SecureStorageService.setValue(SecureStorageKey.vcId, JSON.stringify(vcIds));

      let listVCs = [];

      for(var vcId of vcIds) {
        try {
              const temp = await this._ApiProviderService.getVc(vcId);;
              // await this.pushVc(temp);
              listVCs.push(temp);
          } catch  (e) {
              if ( e.status === 200 ) {
                  if ( !!e.error && !!e.error.text ) {
                      // await this.pushVc(e.error.text);
                      listVCs.push(e.error.text);
                  }
              }
          }
      }
      // console.log('KeyRecoveryBackup#syncVC; listVCs:', listVCs);
      // console.log('KeyRecoveryBackup#syncVC; listVCs.length:', listVCs.length);
      await this._SecureStorageService.setValue(SecureStorageKey.verifiableCredentialEncrypted, JSON.stringify(listVCs));


      var objectResponse = await this._CryptoProviderService.decryptVerifiableCredential();
      if ( typeof objectResponse === 'object' ) {
          var vc = JSON.parse(objectResponse.vc);
          if ( !!vc.credentialSubject.id ) {
              await this._SecureStorageService.setValue(SecureStorageKey.did, vc.credentialSubject.id);
              var didDocument = await this._ApiProviderService.getDid(vc.credentialSubject.id).catch(e => console.log(e));
              if(didDocument) {
                var didDocumentEncrypted = await this._CryptoProviderService.encryptGenericData(JSON.stringify(didDocument));
                await this._SecureStorageService.setValue(SecureStorageKey.didDocument, didDocumentEncrypted);
              }
          }
      }
    } else {

      const existingDidDocument = await this._SecureStorageService.getValue(SecureStorageKey.didDocument, false);

      if(!existingDidDocument) {
        const didDocument = await this._ApiProviderService.putDid();
        if(didDocument) {
          var encryptedDIDDocument = await this._CryptoProviderService.encryptGenericData(JSON.stringify(didDocument));
          await this._SecureStorageService.setValue(SecureStorageKey.didDocument, encryptedDIDDocument);
          if(didDocument.id) {
            await this._SecureStorageService.setValue(SecureStorageKey.did, didDocument.id);
          }
        }
      }


    }

    // mburger: the loader should not be dismissed at this point
    // await this._LoaderProviderService.loaderDismiss();
  }

  // mburger: we don't need this here because we doubled everytime our credentials, so we handle this now
  // directly in the syncVc function
  // private async pushVc(value: string) {
  //   let val = await this._SecureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false);

  //   if ( !val || val === '' || val === '[]' || !Array.isArray(JSON.parse(val)) ) {
  //         var emptyArray = [];
  //         emptyArray.push(value);
  //         var vcStringArray = JSON.stringify(emptyArray);
  //         await this._SecureStorageService.setValue(SecureStorageKey.verifiableCredentialEncrypted, vcStringArray);
  //     } else {
  //         var listVcParse = JSON.parse(val);
  //         listVcParse.push(value);
  //         await this._SecureStorageService.setValue(SecureStorageKey.verifiableCredentialEncrypted, JSON.stringify(listVcParse));
  //     }
  // }

  generateDecryptableObject(input: any) {
    const base64 = this._CryptoProviderService.base64urlTobase64(input);
    const encryptedByte = this._CryptoProviderService.base64ToUint8Array(base64);
    const arrayByteObject = {
        iv : encryptedByte.slice(0, 16),
        v: encryptedByte.slice(16, 16 + 65),
        cipherText: encryptedByte.slice(16 + 65, encryptedByte.length - 32),
        t: encryptedByte.slice(encryptedByte.length - 32)
    };

    const hexByteObjectEncrypted = {
        iv: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(arrayByteObject.iv)), 'hex'),
        ephemPublicKey: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(arrayByteObject.v)), 'hex'),
        ciphertext: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(arrayByteObject.cipherText)), 'hex'),
        mac: Buffer.from(this._CryptoProviderService.base64ToHex(this._CryptoProviderService.uInt8ArrayToBase64(arrayByteObject.t)), 'hex')
    };

    return hexByteObjectEncrypted;
  }

  async recoveryAlert(loginMode: boolean) {
    const user = JSON.parse(await this._SecureStorageService.getValue(SecureStorageKey.userData, false));

    if(this._NetworkService.checkConnection()) {
      var deviceId = await this._DeviceService.getDeviceId();
    }

    if(loginMode) {
      if(!this._NetworkService.checkConnection()) {
        await this.navigationPostRecovery();
        return;
      }

      await this.importUserKey();
      return;
    }

    if(!deviceId) {

      deviceId = await this._DeviceService.getDeviceId();

    }

    if(deviceId == 'newDevice') {
      await this._ApiProviderService.getRecoveryOTP(user.userid, OtpTypesForDeviceRestoration.REGISTER_DEVICE);
    }
    else {
      await this._ApiProviderService.getRecoveryOTP(user.userid, OtpTypesForDeviceRestoration.DEVICE_KEY_RECOVERY);
    }

    const loginModal = await this._ModalController.create({
      component: OtpVerificationComponent,
      componentProps: {
        user
      }
    });

    await loginModal.present();

    const modalData = await loginModal.onDidDismiss();

    console.log("modalData", modalData);

    // mburger: this has to be tested
    if (!modalData?.data) {
      return;
    }

    var code =  environment.production ? modalData.data?.value : '12893478979';

    if(!code || code == "") {
      await this.presentToast(this._TranslateProviderService.instant('RESPONSECODE.OTP_WRONG'));
      await this._SignProviderService.unbrandDeviceFromUser();
      return;
    }

    if(deviceId == 'newDevice') {
      const response = await this.init(code);

      if(response) {

        await this.importUserKey();
        return;
      } else {
        await this.presentToast(this._TranslateProviderService.instant('RESPONSECODE.OTP_WRONG'));
        await this._SignProviderService.unbrandDeviceFromUser();
        return;
      }
    }
    else {
      const temporaryPrivateKeyEncryptionUInt: any = generatePrivate();
      const temporaryPublicKeyEncryptionUInt: any = getPublic(temporaryPrivateKeyEncryptionUInt);
      const temporaryPrivateKeyEncryption = this._CryptoProviderService.uInt8ArrayToBase64(temporaryPrivateKeyEncryptionUInt);
      const temporaryPublicKeyEncryption =  this._CryptoProviderService.uInt8ArrayToBase64(temporaryPublicKeyEncryptionUInt);

      const response = await this.userService.getDeviceRegistrationStatus(deviceId, user.userid, Array.from(temporaryPublicKeyEncryptionUInt).map(this._CryptoProviderService.i2hex).join(''), code, 'backupKey').catch(async (e) => {
        console.log(e);

      })
      console.log(response);

      if(!response){
        await this.presentToast(this._TranslateProviderService.instant('RESPONSECODE.OTP_WRONG'));
        await this._SignProviderService.unbrandDeviceFromUser();
        await this._LoaderProviderService.loaderDismiss();
        return;
      }

      if(response.errorFound == true) {
        await this.presentToast(this._TranslateProviderService.instant('RESPONSECODE.OTP_WRONG'));
        await this._SignProviderService.unbrandDeviceFromUser();
        await this._LoaderProviderService.loaderDismiss();
        return;
      }

      const backupDecryptableObject = this.generateDecryptableObject((!!response.backupKey ? response.backupKey : response.keys.backupKey));

      const backupSymmetricKey = await decrypt(
        this._CryptoProviderService.toBuffer(this._CryptoProviderService.base64ToUint8Array(temporaryPrivateKeyEncryption).buffer),
        backupDecryptableObject).catch(async e => {
          const bool = await this.init();
          if(bool) {
            await this.importUserKey();
            return;
          }
          else {

            await this._SignProviderService.unbrandDeviceFromUser();
            await this._LoaderProviderService.loaderDismiss();
            return;
          }
        });

      if(!backupSymmetricKey) { return; }

      const body = await this._ApiProviderService.getKeyBackup(deviceId);

      if(Object.keys(body).length == 0) {

          const bool = await this.init();

          if(bool) {
            await this.importUserKey();
            return;
          } else {

            await this._SignProviderService.unbrandDeviceFromUser();
            await this._LoaderProviderService.loaderDismiss();
            return;
          }
      }

      // const mnemonicObject = body.find(_ => _.keyName == 'deviceDelegateMnemonic');
      const devicePrivateKeyEncryptionObject = body.find(_ => _.keyName == 'devicePrivateKeyEncryption');

      // if(!mnemonicObject || !devicePrivateKeyEncryptionObject) { return; }
      if(!devicePrivateKeyEncryptionObject) { return; }

      // const mnemonic = await this._CryptoProviderService.symmetricDecrypt(mnemonicObject.encryptedValue, backupSymmetricKey);
      const devicePrivateKeyEncryption = await this._CryptoProviderService.symmetricDecrypt(devicePrivateKeyEncryptionObject.encryptedValue, backupSymmetricKey.toString('hex'));
      const devicePublicKeyEncryption = this._CryptoProviderService.uInt8ArrayToBase64(getPublic(Buffer.from(this._CryptoProviderService.base64ToUint8Array(devicePrivateKeyEncryption))));


      await this._SecureStorageService.setValue(SecureStorageKey.devicePublicKeyEncryption, devicePublicKeyEncryption);
      await this._SecureStorageService.setValue(SecureStorageKey.devicePrivateKeyEncryption, devicePrivateKeyEncryption);

      const encryptedSymmetricKeyActual = await this.userService.getDeviceRegistrationStatus(deviceId);
      await this._SecureStorageService.setValue(SecureStorageKey.userKeyEncryptedBackup, (!!encryptedSymmetricKeyActual.backupKey ? encryptedSymmetricKeyActual.backupKey : encryptedSymmetricKeyActual.keys.backupKey));
      await this._SecureStorageService.setValue(SecureStorageKey.userKeyEncryptedVC, (!!encryptedSymmetricKeyActual.vcKey ? encryptedSymmetricKeyActual.vcKey : encryptedSymmetricKeyActual.keys.vcKey));
      await this._SecureStorageService.setValue(SecureStorageKey.userKeyEncryptedUser, (!!encryptedSymmetricKeyActual.userKey ? encryptedSymmetricKeyActual.userKey : encryptedSymmetricKeyActual.keys.userKey));

      await this.importUserKey();

    }

  }

  async presentToast(message: string) {
    const toast = await this._ToastController.create({
      message,
      duration: 3000,
      position: 'top',
    });
    toast.present();
  }

  async decryptEncryptedChatPrivateKey() {
    const encryptedChatPrivateKey = await this._SecureStorageService.getValue(SecureStorageKey.encryptedChatPrivateKey, false);
    if(!!encryptedChatPrivateKey)
    {
      const symmetricKey = await this._CryptoProviderService.returnUserSymmetricKey();
      var chatPrivateKey = await this._CryptoProviderService.symmetricDecrypt(encryptedChatPrivateKey, symmetricKey)

      await this._SecureStorageService.setValue(SecureStorageKey.chatPrivateKey, chatPrivateKey)
      await this._SecureStorageService.removeValue(SecureStorageKey.encryptedChatPrivateKey)
    }
  }

  async navigationPostRecovery() {
    // await this._LoaderProviderService.loaderCreate();
    if (
      this.deeplinkProviderService.verificationDeeplinkStringValue &&
      !this.deeplinkProviderService.signatureDeeplinkStringValue
    ) {
      await this._LoaderProviderService.loaderDismiss();
      this.nav.navigateForward('/verification');
    } else if (
      this.deeplinkProviderService.signatureDeeplinkStringValue &&
      this.deeplinkProviderService.signatureDeeplinkStringValue
    ) {
      await this._LoaderProviderService.loaderDismiss();
      this.nav.navigateForward('/signature');
    } else {
      var check = await this._SecureStorageService.getValue(SecureStorageKey.wizardNotRun);
      if (check === 'true') {
        const alert = await this._AlertController.create({
          mode: 'ios',
          header: this._TranslateProviderService.instant('LOGIN.data-missing-title'),
          message: this._TranslateProviderService.instant('LOGIN.data-missing-message'),
          buttons: [
            {
              text: this._TranslateProviderService.instant('LOGIN.data-missing-skip'),
              handler: async () => {
                this.goToDashboard();
              }
            },
            {
              text: this._TranslateProviderService.instant('KYC.ALERTVERIFICATION.goToVerification'),
              cssClass: 'primary',
              handler: async () => {
                await this.nav.navigateForward('/kyc?from=onboarding');
              }
            },
            // {
            //   text: this._TranslateProviderService.instant('KYC.ALERTVERIFICATION.enterDataManually'),
            //   cssClass: 'primary',
            //   handler: async () => {
            //     await this.nav.navigateForward('/sign-up/step-6');
            //   }
            // }
          ]
        });
        await this._LoaderProviderService.loaderDismiss();
        await alert.present();
      } else {
        await this._LoaderProviderService.loaderDismiss();
        this.goToDashboard();
      }
    }
  }


public sendUserMnemonic(): Promise<void> {
  return new Promise((resolve, reject) => {
    this._SecureStorageService.getValue(SecureStorageKey.userMnemonic, false).then((userMnemonic) => {
      this._CryptoProviderService.returnBackupSymmetricKey().then((backupSymmetricKey) => {
        this._CryptoProviderService.symmetricEncrypt(JSON.stringify(userMnemonic), backupSymmetricKey).then((encryptedMnemonic) => {
          this._ApiProviderService.postUserKeyBackup([{
            [SecureStorageKey.userMnemonic]: encryptedMnemonic
          }]).then(() => resolve()).catch((e) => reject(e))
        }).catch(e => reject(e))
      }).catch(e => reject(e))
    }).catch(e => reject(e))
  })
}

public sendUserPublicEthereumAddress(): Promise<void> {
  return new Promise((resolve, reject) => {
    this._SecureStorageService.getValue(SecureStorageKey.userPublicKey, false).then((userPublicKey) => {
          this._ApiProviderService.saveUserEthereumAddress(userPublicKey).then(() => resolve()).catch((e) => reject(e))
    }).catch(e => reject(e))
  })
}

private async importUserKey() {
  // var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.userMnemonic, false);

  console.log("Login Page: importUserKey: ");

  var web3WalletMnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);
  var web3WalletPublicKey = await this._ApiProviderService.getUserWeb3WalletAddress().catch(e => {
    console.log(e);
    web3WalletPublicKey = null;
  });

  if( web3WalletPublicKey ) {
    this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, web3WalletPublicKey);
  }

  // var remainingBackup = async () => {
    // await this._LoaderProviderService.loaderCreate(this._TranslateProviderService.instant('LOADER.keyRestoration'));
    console.log("Login Page: importUserKey: this.pushKeys()");
    this.pushKeys();
    console.log("Login Page: importUserKey: this.syncVC()");
    this.syncVC();
    // await this.decryptEncryptedChatPrivateKey().catch(e => console.log(e));
    this._KeyExchangeService.restoreUserChatKeyBackup().catch(e => console.log(e));
    console.log("Login Page: importUserKey: fetchEncryptedPIN");
    this._SecurePinService.fetchEncryptedPIN().then(pin => {
      console.log("Login Page: importUserKey: pin", pin);
      if(pin) {
        this._SecureStorageService.setValue(SecureStorageKey.securePINEncrypted, pin);
      }
    }).catch(e => {
      console.log(e);
    });


    console.log("Login Page: importUserKey: sendUserPublicEthereumAddress");
    this._KeyExchangeService.sendUserPublicEthereumAddress();

    console.log("Login Page: importUserKey: fetchActivities");
    this._UserActivity.fetchActivities();
    // await this._LoaderProviderService.loaderDismiss();
  // }

    try{
      var username = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);

      if(!web3WalletMnemonic && username && web3WalletPublicKey && this._Platform.is('ios') && this._Platform.is('hybrid')) {
        const folder = this._File.syncedDataDirectory;
        const file = `${environment.filePrefix.userkey}-${username}-${web3WalletPublicKey}.json`;
        console.log("folder", folder);
        console.log("file", file);
        console.log("username", username);

        const processContent = async (content) => {
          if(content.toString().indexOf("data:application/json;base64,") > -1) {
            var ttt = content.toString().replace("data:application/json;base64,","");
          } else {
            ttt = content.toString();
          }
          var tt = window.atob(ttt);
          var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();
          var d1 = CryptoJS.AES.decrypt(JSON.parse(tt), UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
          try {
            var d3 = JSON.parse(d1);
          } catch(e) {
            d3 = d1;
          }
          try {
            var d2 = CryptoJS.AES.decrypt(d3, backupSymmetricKey).toString(CryptoJS.enc.Utf8);
            console.log(d2);
          } catch(e) {
            d2 = d3;
          }
          try {
            var d4 = JSON.parse(d2);
          } catch(e) {
            d4 = d2;
          }
          if(ethers.utils.isValidMnemonic(d4)) {
            // Success Handling
            var walid = ethers.Wallet.fromMnemonic(d4);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, walid.mnemonic.phrase);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, walid.privateKey);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletName, `${username}'s Wallet`);
          }

        }


        try{
          iCloudDocStorage.initUbiquitousContainer(environment.icloud, (r) => {
            iCloudDocStorage.fileList("Cloud",async (s) => {
              var keys = Array.from(s, ss => Object.keys(ss)[0] );
              var fileExists = keys.findIndex(k => (k.indexOf(file) > -1));
              if (fileExists > -1) {
                await this.presentToast(this._TranslateProviderService.instant('WALLET.cloud-backup-file-found'));
                var content = s[fileExists][Object.keys(s[fileExists])[0]];
                // console.log("iCloud file content", content);
                await processContent(content);
              // } else {
              //   await this.presentToast(this._TranslateProviderService.instant('WALLET.no-backup-file-found'));
              }
            }, (e) => console.log("e", e));
          }, (ee) => {
            console.log(ee);
          });

        } catch(eee) {
          console.log(eee);
          try {
            var checkIfFileExists = await this._File.checkFile(folder, file);
            console.log("checkIfFileExists", checkIfFileExists);
            if(checkIfFileExists) {
              var content = await this._File.readAsDataURL(folder, file);
              console.log("cloud file content", content);
              await processContent(content);
            }
          } catch(eeee) {
            console.log(eeee);
          }
        }

      }

    } catch(e) {
      console.log(e);
    }



  this.navigationPostRecovery();
}

  async goToDashboard() {
    await this.nav.navigateRoot('/dashboard');
  }

}

// helix id folder finally displayed in the user’s iCloud Drive. The following combination works:
// Make sure the project uses the updated BCHX cordova icloud document project
// iOS Build target 14.0
// iCloud container check on Xcode prior to building
// NSUbiquitousContainers dictionary entry in Info.plist file: https://developer.apple.com/library/archive/documentation/General/Conceptual/ExtensibilityPG/FileProvider.html
// UIFileSharingEnabled set to YES in Info.plist file
// LSSupportsOpeningDocumentsInPlace set to YES Info.plist file
// CFBundle number should 1 more than the build number when the CloudKit was initialized
