import { Injectable } from '@angular/core';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { User } from '../../models/User';
import { take } from 'rxjs/operators';
import { DeeplinkProviderService } from '../deeplink/deeplink-provider.service';
import { UserProviderService } from '../user/user-provider.service';
import { ApiProviderService } from '../api/api-provider.service';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { AppStateService } from '../app-state/app-state.service';
import { SignProviderService } from '../sign/sign-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { StorageKeys, StorageProviderService } from '../storage/storage-provider.service';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
// import { ChatService } from '../chat/chat.service';
import { differenceWith, isEqual } from 'lodash-es';
import { KycProvider } from '../../../kyc/enums/kyc-provider.enum';
import { KycState } from '../../../kyc/enums/kyc-state.enum';
import { KycService } from '../../../kyc/services/kyc.service';
import { KycmediaService } from '../../../core/providers/kycmedia/kycmedia.service';
import { UDIDNonce } from '../device/udid.enum';
import * as CryptoJS from 'crypto-js';
import { VaultService } from '../vault/vault.service';
import { Capacitor } from '@capacitor/core';
import { Device, DeviceId } from '@capacitor/device';
import { Platform } from '@ionic/angular';
import { UserService } from '@services/user';


declare var window: any;

@Injectable({
    providedIn: 'root'
})
export class AuthenticationProviderService {

    animationController = true;

    constructor(
        private secureStorage: SecureStorageService,
        private storageProviderService: StorageProviderService,
        private deeplinkProviderService: DeeplinkProviderService,
        private userProviderService: UserProviderService,
        private apiProviderService: ApiProviderService,
        private signProvider: SignProviderService,
        private translateProviderService: TranslateProviderService,
        private toastController: ToastController,
        private appStateService: AppStateService,
        // private _ChatService: ChatService,
        private _Vault: VaultService,
        private crypto: CryptoProviderService,
        private _KycService: KycService,
        private _KycmediaService: KycmediaService,
        private _AlertController: AlertController,
        private nav: NavController,
        private _Platform: Platform,
        private userService: UserService,
    ) {
    }

    async setBasicAuthToken(login: string, password: string): Promise<string> {
        const basicAuthToken: string = await this.crypto.generateBasicAuthToken(login, password);
        return basicAuthToken;
    }

    getBasicAuthToken(): Promise<string> {
        return new Promise((resolve: (value: string) => void) => {
            this.secureStorage.getValue(SecureStorageKey.basicAuthToken)
                .then((value: string) => resolve(value), () => resolve(undefined));
        });
    }

    async resetBasicAuthToken(): Promise<boolean> {
        await this.secureStorage.removeValue(SecureStorageKey.basicAuthToken);
        return true;
    }

    async isAuthorized(): Promise<boolean> {
        let isAuthorized = false;
        try {
            if ( await this.userProviderService.getUser() && await this.getBasicAuthToken() ) {
                isAuthorized = true;
            }
        } catch (e) {
            console.log(e);
        }

        return isAuthorized;
    }

    public async loginUsernameAndPassword(username: string, password: string, viaLockScreen: boolean): Promise<boolean> {
        const token = await this.setBasicAuthToken(username, password);
        if (!token) {
            await this.presentToast(this.translateProviderService.instant('LOGIN.wrongPassword'));
            return false;
        }
        return this.login(viaLockScreen);
    }



    public async login(viaLockScreen: boolean) {

            var response = await this.loginStart();
            if (response) {
                this.apiProviderService.userId = response.principal.id;
                if(!!response.principal.username) {
                    this.secureStorage.setValue(SecureStorageKey.userName, response.principal.username).then(() => {});
                }
                // if(!!response.principal.chatPublicKey) {
                //     var chatPublicKey = response.principal.chatPublicKey
                //     this.secureStorage.setValue(SecureStorageKey.chatPublicKey, chatPublicKey).then(() => {})
                // }
                // if(!!response.principal.encryptedChatPrivateKey) {
                //     var encryptedChatPrivateKey = response.principal.encryptedChatPrivateKey
                //     var deviceId = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
                //         if(deviceId) {
                //             console.log('This gets executed')
                //             var symmetricKey = await this.crypto.returnUserSymmetricKey();
                //                 if(symmetricKey) {
                //                     var chatPrivateKey = await this.crypto.symmetricDecrypt(encryptedChatPrivateKey, symmetricKey);
                //                     this.secureStorage.setValue(SecureStorageKey.chatPrivateKey, chatPrivateKey).then(() => {})

                //                 }

                //         } else {
                //             // console.log('Else this one')
                //             this.secureStorage.setValue(SecureStorageKey.encryptedChatPrivateKey, encryptedChatPrivateKey).then(() => {})
                //         }


                // }
                // this.loginGetChatUser().then(result => {
                //     if (!result) {
                //         this.showLoginFailed_CouldNotCreateChatUser();
                //     }
                    var success = await this.finishLoginSetTrustedUser(response.principal.id);
                    if (success) {
                        this._Vault.getAllSecrets();
                        if(viaLockScreen) {
                            this.fetchRemainingVcs();
                        }
                        return true;

                    }
                    else {
                        this.showLoginFailed();
                        return false;
                    }
                // });
            }
            else {
                this.showLoginFailed();
                return false;
            }


    }

    fetchRemainingVcs(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.secureStorage.getValue(SecureStorageKey.vcId, false).then(vcId => {

                this.apiProviderService.getVcIds().then(fullList => {

                    if( fullList.length > 0 ) {
                        var vcIdList = !!vcId ? JSON.parse(vcId) : [];

                        var remainingList = differenceWith(fullList, vcIdList, isEqual);

                        if( remainingList.length > 0 ) {
                            Object.keys(KycProvider).forEach(async provider => {
                                if (this._KycService.getState(KycProvider[provider]) === KycState.KYC_INITIATED) {
                                    await this._KycService.setState(KycProvider[provider], KycState.KYC_CHECK);
                                }
                                if (Object.keys(KycProvider).indexOf(provider) == Object.keys(KycProvider).length - 1) {
                                    await this._KycmediaService.fetchRemainingMedia();
                                    resolve();
                                }
                            });
                        } else {
                            resolve();
                        }
                    } else {
                        resolve();
                    }
                }).catch(e => { console.log('Error fetching vcIds from MW'); reject(e) })
            }).catch(e => { console.log('Error accessing vcId secure-storage'); reject(e) })
        })
    }


    showLoginFailed() {
        console.log('Login failed!');
        this.presentToast(this.translateProviderService.instant('LOGIN.wrongPassword'));
        this.animationController = false;
        this.secureStorage.setValue(SecureStorageKey.loginStatusAfterSignup, false.toString());
    }

    showLoginFailed_CouldNotCreateChatUser() {
        // this.presentToast(this.translateProviderService.instant('LOGIN.wrongPassword')).then(() => {
            this.presentToast(this.translateProviderService.instant('LOGIN.errorGettingChatUser'));
        // });
        this.animationController = false;
        this.secureStorage.setValue(SecureStorageKey.loginStatusAfterSignup, false.toString());
    }

    private async loginStart() {
        var response = await this.userService.login().catch(e => {
            return false;
        });
        if (response) {
            var userId = response.principal.id;
            this.apiProviderService.userId = userId;
            return response;
        }
        else {
            return false;
        }
    }

    loginGetChatUser() {
        return new Promise(resolve => {
            this.apiProviderService.getSelfChatUser().then(chatUser => {
                if (chatUser) {
                    // console.log('*** chatUser: ***');
                    // console.log(chatUser);
                    this.secureStorage.setValue(SecureStorageKey.chatUserData, JSON.stringify(chatUser), false).then(() => {
                        resolve(chatUser);
                    });
                }
                else {
                    resolve(false);
                }
            }).catch(err => {
                console.log(err);
                resolve(false);
            });
        });
    }

    async finishLoginSetTrustedUser(userId: string) {
        var user = await this.apiProviderService.getUserById(userId);
        if (user) {
            await this.secureStorage.setValue(SecureStorageKey.userData, JSON.stringify(user))
            var trustResponse = await this.apiProviderService.getTrustData(userId);
            this.secureStorage.setValue(SecureStorageKey.userTrustData, JSON.stringify(trustResponse));
            var { photo, userData, userTrust } = await this.apiProviderService.getUserImageAndData(false, false);
            this.signProvider.documentSetter(user);
            await this.secureStorage.setValue(SecureStorageKey.loginStatusAfterSignup, "true")
            await this.signProvider.setOnboardingHasRun(true);
            this.appStateService.isAuthorized = true;
            return true;
        }
        else {
            return false;
        }
    }


    async presentToast(message: string) {
        const toast = await this.toastController.create({
            message,
            duration: 2000,
            position: 'top',
        });
        await toast.present();
    }

    async logout(): Promise<any> {
        try {
            const user: User = await this.userProviderService.getUser();
            const userFirstname = (user || {}).firstname;
            const onboardingHasRun = await this.signProvider.getOnboardingHasRun();
            await this.secureStorage.setValue(SecureStorageKey.onboardingHasRun, (onboardingHasRun ? "true" : "false"));
            this.deeplinkProviderService.resetVerificationDeeplinkStringValue();
            this.deeplinkProviderService.resetSignatureDeeplinkStringValue();
            this.deleteAllCookies();
            await this.apiProviderService.logout();
            // this._ChatService.disconnect();
            this.appStateService.isAuthorized = false;
            return true;
        } catch (e) {
            console.log(e);
        }

        return false;
    }

    deleteAllCookies() {
        const cookies = document.cookie.split(';');

        for ( let i = 0; i < cookies.length; i++ ) {
            const cookie = cookies[i];
            const eqPos = cookie.indexOf('=');
            const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
        }
    }

    async cleanForLogout(cryptoProviderService: CryptoProviderService) {
        // Ansik: 2. (H) Remove { User Auth Info } from secure-storage when the user logs out,
        // await this.secureStorage.removeValue(SecureStorageKey.userName, false); // User Auth Info
        // await this.secureStorage.removeValue(SecureStorageKey.password, false); // User Auth Info


        // await this.secureStorage.removeValue(SecureStorageKey.userKeyEncryptedVC, false); // Remove userKeyEncrypted
        // await this.secureStorage.removeValue(SecureStorageKey.userKeyEncryptedBackup, false); // Remove userKeyEncrypted
        // Ansik: 3. Encrypt { User Personal Info } when the user logs out
        await cryptoProviderService.encryptGenericData(await this.secureStorage.getValue(SecureStorageKey.userData)); // User Personal Info

        // Removing sign and personalInfo keys from secure-storage
        await this.secureStorage.removeValue(SecureStorageKey.sign, false); // registration form 1
        await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false); // // registration form 2

        var basicAuthToken = await this.secureStorage.getValue(SecureStorageKey.basicAuthToken, false);


        if (this._Platform.is('hybrid') && Capacitor.isPluginAvailable('Device')) {
            Device.getId().then((deviceId: DeviceId) => {
              console.info('AuthenticationProvider#cleanForLogout; deviceId', deviceId);

              this.encryptBasicAuthToken(basicAuthToken, deviceId.identifier, UDIDNonce.helix as any).then(() => {
                this.secureStorage.removeValue(SecureStorageKey.basicAuthToken, false).then(() => {
                    console.log('Mode 1: BasicAuthToken completely removed from the secure-storage');
                }); // User Auth Info complete
              });
            },
            (err) => {
              console.log('err: ' + JSON.stringify(err));
            });
          }
          else {
              console.log('Could not obtain UDID.');
              var udid = window["navigator"].vendor + window["navigator"].appName + window["navigator"].appCodeName;
              udid = udid.replaceAll(" ","").replaceAll(".","");
              this.encryptBasicAuthToken(basicAuthToken, udid, UDIDNonce.helix as any).then(() => {
                this.secureStorage.removeValue(SecureStorageKey.basicAuthToken, false).then(() => {
                    console.log('Mode 2: BasicAuthToken completely removed from the secure-storage');
                }); // User Auth Info complete
              })
          }
    }

    displayLoginOptionIfNecessary() {
        if(!this.appStateService.basicAuthToken) {
            // this.secureStorage.getValue(SecureStorageKey.lockScreenToggle, false).then((lockScreenToggle) => {
                // if(lockScreenToggle == 'true') {
                    this.secureStorage.getValue(SecureStorageKey.loginCheck, false).then((loginCheck) => {
                        if(loginCheck) {
                            this._AlertController.create({
                                mode: 'ios',
                                header: this.translateProviderService.instant('FORCELOGOUT.heading'),
                                message: this.translateProviderService.instant('FORCELOGOUT.message'),
                                buttons: [{
                                    text: this.translateProviderService.instant('GENERAL.ok'),
                                    role: 'primary',
                                    handler: () => {
                                        this.nav.navigateRoot('/login?from=browsemode')
                                    }
                                }]
                            }).then(alert => alert.present())
                        }
                    })
                // }
            // })
        }
    }

    encryptBasicAuthToken(basicAuthToken: string, udid: string, nonce: string): Promise<void> {
        return new Promise((resolve, reject) => {
            var string1 = udid.substr(4,9);
            var string2 = nonce.substr(2,11);
            var key = CryptoJS.SHA256(string1 + string2, UDIDNonce.helix as any).toString(CryptoJS.enc.Hex);
            // console.log("string1: " + string1);
            // console.log("string2: " + string2);
            // console.log("key: " + key);
            // console.log("basicAuthToken: " + basicAuthToken);
            this.crypto.symmetricEncrypt(basicAuthToken, key).then((encrypted) => {
                // console.log("encrypted: " + encrypted);
                this.secureStorage.setValue(SecureStorageKey.encryptedBasicAuthToken, encrypted).then(() => {
                    resolve()
                }).catch(() => reject())
            }).catch(() => reject())
        })
    }
}
