import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Platform, NavController, ToastController, AlertController } from '@ionic/angular';
import { AppConfig } from '../../../app.config';
import { combineLatest, from, interval, Observable, of, Subject, SubscriptionLike } from 'rxjs';
import { UserProviderService } from '../user/user-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { take, tap } from 'rxjs/operators';
import { flatMap, map, switchMap } from 'rxjs/operators';
import { Basicdatadata, UserKeyResponse, UserService } from '@services/user';
import { User } from '../../models/User';
import { AppStateService } from '../app-state/app-state.service';
import { Trust } from '../../models/Trust';
import { DomSanitizer } from '@angular/platform-browser';
import { LockService } from '../../../lock-screen/lock.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { ComboUserWrapper, RosterItem, ChatKeyPairs } from '../chat/chat-model';
import { resolve } from 'dns';
import watermark from 'watermarkjs';
import { AlertsProviderService } from '../alerts/alerts-provider.service';
import * as CryptoJS from 'crypto-js';
import { CryptoCurrency } from '../wallet-connect/constants';
import { ethers } from 'ethers';
import { UserActivity } from '../user-activities/user-activities.service';
import { UserPhotoServiceAkita } from '../state/user-photo/user-photo.service';

import { encrypt } from '@toruslabs/eccrypto';
import { createCipheriv, createDecipheriv, randomBytes } from 'crypto';

interface ExtendedBasicData extends Basicdatadata {
    lastModified: number;
}

export interface RemoteLog {
    subject: string;
    message: string;
    category: string;
    deviceId: string;
    level: RemoteLogLevel;
}

export enum RemoteLogLevel {
    warning = 'warning',
    info = 'info',
    error = 'error',
    debug = 'debug'
}

export interface VeriffInitResponse {
    answered: boolean; // ???
    answeredString: string;
    created: number;
    createdString: string;
    message: string;
    sessionToken: string;
    status: number;
    url: string;
    userId: string;
    veriffSessionId: string;
}

export interface VeriffPrepareBody {
    type?: string;
    country?: string;
}

export interface AuthadaInitResponse {
    handling: boolean;
    mobileToken: string;
    sessionToken: string;
    timestamp: number;
}

export interface VerifeyeResponse {
    message: string;
    sessionId: string;
    created: number;
    answered: any;
    userId: string;
    url: string;
}

export interface VerificationCheckResponse {
    verificationprocessid: string;
    fk_trustproviderprocess: string;
    fk_user: string;
    created: number;
    documentType?: string;
    kycProcessStatus: number;
    kycProvider: string;
    kycTimestamp: number;
    kycSessionToken: string;
    kycNextCheckTimer: string;
    kycProcessResultCode: string;
    kycProcessErrorCode: string;
}

export enum OtpTypesForDeviceRestoration {
    DEVICE_KEY_RECOVERY = "DEVICE_KEY_RECOVERY",
    REGISTER_DEVICE = "REGISTER_DEVICE"
}

export interface BroadcastMessages {
    timestamp: number;
    title: string;
    message: string;
}

export interface BroadcastMessagesResponse {
    identifier: string;
    templateId: string;
    language: string;
    sentMessage: string;
    lastModified: number;
    notificationId: string;
}

interface MailValidationError {
    fieldName: string;
    fieldValue: string;
    message: string;
    errorCode: number
}

export interface ValidationResponse {
    errorFound: boolean,
    errors: Array<MailValidationError>
}

@Injectable({
    providedIn: 'root'
})
export class ApiProviderService {

    private readonly appConfig: any = AppConfig;
    public userId: any;
    public interval: SubscriptionLike;
    public userSubject: Subject<any>;

    // public globalProfilePicture: string;

    constructor(private http: HttpClient,
        public userProviderService: UserProviderService,
        private secureStorageService: SecureStorageService,
        private platform: Platform,
        private userService: UserService,
        private appStateService: AppStateService,
        private sanitizer: DomSanitizer,
        private loaderService: LoaderProviderService,
        private translateService: TranslateProviderService,
        private _AlertServiceProvider: AlertsProviderService,
        private _AlertController: AlertController,
        private nav: NavController,
        private toastController: ToastController,
        private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
    }

    private uInt8ArrayToBase64(bytes: Uint8Array): string {
        return Buffer.from(bytes).toString('base64');
    }

    private base64ToHex(str) {
        const raw = atob(str);
        let result = '';
        for (let i = 0; i < raw.length; i++) {
            const hex = raw.charCodeAt(i).toString(16);
            result += (hex.length === 2 ? hex : '0' + hex);
        }
        return result.toUpperCase();
    }

    private i2hex(i) {
        return ('0' + i.toString(16)).slice(-2);
    }

    private hex2i(hexString) {
        return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
    }

    private base64urlTobase64(input) {
        if (input) {
            input = input.replace(/-/g, '+').replace(/_/g, '/');
            const pad = input.length % 4;
            if (pad) {
                if (pad === 1) {
                    throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
                }
                input += new Array(5 - pad).join('=');
            }
        }
        return input;
    }

    private symmetricDecrypt(text: string, symmetricKey: string) {
        var hexwith0C = this.base64ToHex(this.base64urlTobase64(text));
        var byteArrayResult = this.hex2i(hexwith0C.slice(2));

        var byteArrayObject = {
            iv: byteArrayResult.slice(0, 12),
            ciphertext: byteArrayResult.slice(12, byteArrayResult.length - 16),
            authTag: byteArrayResult.slice(byteArrayResult.length - 16)
        };
        var byteArrayObjectHex = {
            iv: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.iv)), 'hex'),
            ciphertext: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.ciphertext)), 'hex'),
            authTag: Buffer.from(this.base64ToHex(this.uInt8ArrayToBase64(byteArrayObject.authTag)), 'hex')
        };
        var key = Buffer.from(symmetricKey, 'hex');
        var decipher = createDecipheriv('aes-256-gcm', key, byteArrayObjectHex.iv);
        decipher.setAuthTag(byteArrayObjectHex.authTag);
        let decrypted = decipher.update(byteArrayObjectHex.ciphertext, undefined, 'utf8');
        decrypted += decipher.final();
        return decrypted;
    }

    private symmetricEncrypt(text: any, symmetricKey: any): string {
        // return new Promise((resolve, reject) => {
        //     if(!symmetricKey || symmetricKey === "") {
        //         console.error('Empty userSymmetricKey');
        //         reject();
        //     }
        //     try {
        var key = Buffer.from(symmetricKey, 'hex');
        var iv = randomBytes(12);
        var cipher = createCipheriv('aes-256-gcm', key, iv);
        var ciphered = cipher.update(text, 'utf8');
        ciphered = Buffer.concat([ciphered, cipher.final()]);
        var authTag = cipher.getAuthTag();
        var ivHex = iv.toString('hex');
        var encryptedHex = ciphered.toString('hex');
        var authTagHex = authTag.toString('hex');
        var chiperMsgHex = "0c" + ivHex + encryptedHex + authTagHex;
        var chiperMsgHextoBase64 = Buffer.from(chiperMsgHex, 'hex').toString('base64');
        var chiperMsgHextoBase64URL = this.base64Tobase64url(chiperMsgHextoBase64);
        // console.log("Text to Encrypt: "+ text);
        // console.log("chiperMsgHextoBase64URL: "+ chiperMsgHextoBase64URL);
        return (chiperMsgHextoBase64URL);
        // } catch (e) {
        //     // console.log(e)
        //     reject()
        // }
        // })
    }

    private generateBlueTokens(): Promise<{
        key: string;
        encrypted: string;
    }> {
        return new Promise((resolve) => {
            const r = CryptoJS.lib.WordArray.random(32).toString();
            const rm = new Uint8Array(r.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
            const m = this.appConfig.blue;
            const mm = Buffer.from(m, 'hex');
            encrypt(mm, Buffer.from(rm)).then(encrypted => {
                const encryptedUnion = Buffer.concat([encrypted.iv, encrypted.ephemPublicKey, encrypted.ciphertext, encrypted.mac]);
                const header = this.base64Tobase64url(this.uInt8ArrayToBase64(encryptedUnion));

                resolve({
                    key: r,
                    encrypted: header
                })
            });
        })
    }

    private base64Tobase64url(input) {
        if (input) {
            input = input
                .replace(/\+/g, '-')
                .replace(/\//g, '_');
        }
        return input;
    }

    HCaptchaSiteVerify(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/hcaptcha/siteverify`, body, {
                headers: {

                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
                .subscribe({
                    next: (data) => {
                        console.log(data);
                        resolve(data);
                    },
                    error: (error) => {
                        reject(error);
                    }
                });
        })
    }


    ObtainSiteKeys(): Promise<string> {
        return new Promise((resolve, reject) => {
            this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then(eth_addr => {
                this.http.post(`${this.getApiUrl()}/hcaptcha/register-labeler`, eth_addr, {
                    headers: {

                        "Content-Type": "text/plain"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            console.log(data);
                            resolve(data)
                        },
                        error: (error) => {
                            reject(error);
                        }
                    });
            });
        });
    }

    ObtainhCaptchaUserDashboardData(): Promise<any> {
        return new Promise((resolve, reject) => {
            // this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then(eth_addr => {
            this.http.get(`${this.getApiUrl()}/hcaptcha/dashboard`, {
                headers: {

                }
            })
                .subscribe({
                    next: (data) => {
                        // console.log(data);
                        resolve(data);
                    },
                    error: (error) => {
                        reject(error);
                    }
                });
        })
        // })
    }


    forgotUsername(email: string, encryptedPw: string): Promise<ValidationResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var url = `${this.getApiUrl()}/api/v1/user/usernamereminder`;
                var body = {
                    "email": email,
                    "password": encryptedPw
                };
                // console.log(body);
                var encryptedBody = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(url, encryptedBody, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const rr = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                resolve(JSON.parse(rr));
                            } catch (error) {
                                resolve({
                                    errors: [],
                                    errorFound: false
                                });
                            }
                        },
                        error: (err) => {
                            reject(err);
                        }
                    });
            })
        });
    }

    transhmailDetection(domain: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`https://api.mailcheck.ai/domain/${domain}`)
                .subscribe({
                    next: (result) => {
                        resolve(result);
                    },
                    error: (e) => {
                        reject(e);
                    }
                })
        })

    }

    getSecret(key: any): Promise<string> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v3b/config/get/value/${key}`, {
                    headers: {

                        "x-api-key": blueToken.encrypted
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                resolve(JSON.parse(response));
                            } catch (e) {
                                resolve(data);
                            }
                        }
                    });
            })
        })
    }

    getAllSecrets(): Promise<{ [key: string]: string }> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v3b/config/get/values`, {
                    headers: {
                        "x-api-key": blueToken.encrypted
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                resolve(JSON.parse(response));
                            } catch (e) {
                                resolve(data);
                            }
                        }
                    });
            })
        })
    }

    deleteUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.getApiUrl()}/user/delete`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    checkUserName(userName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/user/checkusername/${userName}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getPublicImage(userId): Observable<any> {
        return this.http.get(`${this.getApiUrl()}/user/${userId}/publicimage`, {
            responseType: 'blob'
        });
    }

    checkEmail(email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/checkemail/${email}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getUserSymmetricKey(deviceId: string, keyName?: string, kek?: string): Promise<UserKeyResponse> {
        let url = `${this.getApiUrl()}/api/v2/user/${deviceId}/userkey`;
        if (!!keyName) {
            url = `${this.getApiUrl()}/api/v2/user/${deviceId}/userkey?keyName=${keyName}`
        }
        if (!!kek) {
            url = url + '&kek=' + kek
        }
        // console.log(url);
        return new Promise((resolve, reject) => {
            this.http.get(url)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getRecoveryOTP(userId: string, type: OtpTypesForDeviceRestoration): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var body = {
                    type: type,
                    identifier: userId
                };
                var encryptedBody = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/otp`, encryptedBody, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                var decrypted = this.symmetricDecrypt(data, blueToken.key);
                                resolve(decrypted);
                            } catch (e) {
                                resolve(data)
                            }
                        },
                        error: (err) => reject(err)
                    });
            });
        })
    }

    getKeyBackup(deviceId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/device/${deviceId}/keybackup`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    postKeyBackup(deviceId: string, body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/device/${deviceId}/keybackup`, body, {
                headers: {

                    "Content-Type": "application/json"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getTargetChatPublicKey(targetUsername: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/chat/${targetUsername}`)
                .subscribe({
                    next: (data) => {
                        // console.log(data);
                        resolve(data);
                    },
                    error: (err) => reject(err)
                });
        });
    }

    // IMPORTANT: if we need this we need to whitelist the endpoint in the http interceptor
    // /api/v1/user/*/qrcode/*
    // getActionsPostQRCodeScan(username: string, targetUsername: string): Promise<any> {
    //     return new Promise((resolve, reject) => {
    //         this.http.get(`${this.getApiUrl()}/api/v1/user/${username}/qrcode/${targetUsername}`)
    //                 .subscribe({
    //                     next: (data) => {
    //                         // console.log(data);
    //                         resolve(data);
    //                     },
    //                     error: (err) => reject(err)
    //                 });
    //     });
    // }

    getUserDevices(userid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueKeys => {
                this.http.get(`${this.getApiUrl()}/api/v3b/devices/get/user/${userid}`, {
                    headers: {
                        "x-api-key": blueKeys.encrypted,
                        "Content-Type": "application/json"
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueKeys.key);
                                resolve(JSON.parse(response));
                            } catch (e) {
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    sendUserDeviceInfo(userid: string, body: any, otp?: string): Promise<any> {
        let url = `${this.getApiUrl()}/api/v3b/device/add/user/${userid}`;
        if (!!otp) {
            url = `${this.getApiUrl()}/api/v3b/device/add/user/${userid}?otp=${otp}`;
        }
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encryptedBody = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(url, encryptedBody, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "text/plain",
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                resolve(JSON.parse(response));
                            } catch (e) {
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    sendUserSettings(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/settings`, encrypted, {
                    // this.http.post(`${this.getApiUrl()}/api/v1/user/settings`, body, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/settings response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/settings response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/settings response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        })
    }

    getUserSettings(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/user/settings`, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/settings response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/settings response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/user/settings response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    deleteUserDeviceInfo(userid: string, deviceid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.getApiUrl()}/api/v1/device/${deviceid}/delete/user/${userid}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getLegals(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/legals`)
                .subscribe({
                    next: (data) => {
                        // console.log(data);
                        resolve(data);
                    },
                    error: (err) => reject(err)
                });
        });
    }

    createUser(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encryptedBody = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                // console.log(`blueToken.encrypted: ${blueToken.encrypted}`);
                // console.log("encryptedBody: "); console.log(encryptedBody);
                var header = {
                    "Content-Type": "text/plain",
                    "x-api-key": blueToken.encrypted as string
                };
                // console.log(header);
                this.http.post(`${this.getApiUrl()}/api/v3b/user/create`, encryptedBody, {
                    headers: header,
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            // this.http.post(`${this.getApiUrl()}/api/v3b/user/create`, encryptedBody, {
                            //     headers: header,
                            //     responseType: 'text'
                            // }).subscribe(res => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                resolve(JSON.parse(response));
                            } catch (e) {
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    setFriendRequestPermissionsForUser(targetUsername: string): Promise<boolean> {
        return new Promise((resolve) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/chat/permissions/${targetUsername}`)
                .subscribe({
                    next: (data) => {
                        resolve(true);
                    },
                    error: (err) => {
                        console.log('*** setFriendRequestPermissionsForUser err: ***');
                        console.log(err);
                        resolve(false);
                    }
                });
        });
    }

    deleteFriend(targetUsername: string): Promise<boolean> {
        return new Promise((resolve) => {
            this.http.delete(`${this.getApiUrl()}/api/v1/user/chat/roster/${targetUsername}`)
                .subscribe({
                    next: (data) => {
                        resolve(true);
                    },
                    error: (err) => {
                        resolve(false);
                    }
                });
        });
    }

    retrieveFriendRequestPermissionsForUser(targetUsername: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/chat/data/${targetUsername}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    searchByChatUsername(username: string): Promise<ComboUserWrapper[]> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/chat/user/${username}`)
                .subscribe({
                    next: (data) => resolve(<ComboUserWrapper[]>data),
                    error: (err) => reject(err)
                });
        });
    }

    getChatContacts(): Promise<ComboUserWrapper[]> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/chat/contacts`)
                .subscribe({
                    next: (data) => resolve(<ComboUserWrapper[]>data),
                    error: (err) => reject(err)
                });
        });
    }

    getSelfChatUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/user/chat/user`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/chat/user response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/chat/user response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/user/chat/user response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    saveUser(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var bodyEncrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/user/save`, bodyEncrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                if (!this.isProd()) {
                                    console.log("POST /user/save response 2");
                                    console.log(JSON.parse(response));
                                }
                                resolve(JSON.parse(response));
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /user/save response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    checkIfUserFollowsLinkedinCompany(state: string, company: string): Promise<any> {
        const url = `${AppConfig.apiUrl}/api/v1/linkedin/companyfollow`;
        const params = { state, company };
        return new Promise((resolve, reject) => {
            this.http.get(url, {
                params,
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    obtainLinkedinUserData(state: string): Promise<any> {
        const url = `${AppConfig.apiUrl}/api/v1/linkedin/userinfo`;
        const params = { state };
        return new Promise((resolve, reject) => {
            this.http.get(url, {
                params,
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    obtainLinkedinMeData(state: string): Promise<any> {
        const url = `${AppConfig.apiUrl}/api/v1/linkedin/me`;
        const params = { state };
        return new Promise((resolve, reject) => {
            this.http.get(url, {
                params,
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    postOnLinkedin(state: string, content: string, author: string): Promise<any> {
        const body = { state, content, author };
        const url = `${AppConfig.apiUrl}/api/v1/linkedin/post`;
        return new Promise((resolve, reject) => {
            this.http.post(url, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    deleteLinkedinData(state: string): Promise<any> {
        const url = `${AppConfig.apiUrl}/api/v1/linkedin/delete`;
        const params = { state };
        return new Promise((resolve, reject) => {
            this.http.delete(url, {
                params,
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    obtainBroadcastMessages(timestamp: number, identifier: string): Promise<Array<BroadcastMessagesResponse>> {
        const url = `${AppConfig.apiUrl}/api/v1/notification/broadcast`;
        const params = !!timestamp ? (!!identifier ? { timestamp, identifier } : { timestamp }) : { identifier };
        return new Promise((resolve, reject) => {
            this.http.get(url, {
                params,
            })
                .subscribe({
                    next: (data: any) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    updateFirebaseToken(token: any, deviceId: string): Promise<any> {
        const url = `${AppConfig.apiUrl}/api/v2/notification/register/${deviceId}`;
        return new Promise((resolve, reject) => {
            this.http.post(url, token, {
                headers: {
                    "Content-Type": "text/plain"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    public async updatePicture(user: User, image: string, filename: string, format: string): Promise<string> {
        return new Promise<string>(async (resolve, reject) => {
            if (!image || image.length === 0) {
                reject();
                return;
            }
            const presentToast = async (message) => {
                const toast = await this.toastController.create({
                    message,
                    duration: 2000,
                    position: 'top',
                });
                await toast.present();
            };
            await this.loaderService.loaderCreate();
            this.saveImage({
                name: filename,
                content_type: format,
                data: image,
                documentid: user.userid
            }).pipe(
                switchMap(_ => this.getImage(user.userid, filename)),
                take(1)
            ).subscribe(imageUrl => {
                resolve(imageUrl);
                presentToast(this.translateService.instant('LOADER.picsaved'));
                this.loaderService.loaderDismiss();
            }, err => {
                // TODO @Ansik Please add a feedback that image upload didn't work
                reject();
                this.loaderService.loaderDismiss();
            });
        });
    }

    validateUser(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/user/validate`, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    validateEmail(email: string, callname: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = `${this.getApiUrl()}/api/v2/user/mailvalidation`;
            const body = {
                email,
                callname
            };
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                return this.http.post(url, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const encrypted = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    console.log("/api/v2/user/mailvalidation response 1");
                                    console.log(encrypted);
                                    resolve(encrypted);
                                } catch (ee) {
                                    const encrypted = this.symmetricDecrypt(data, blueToken.key);
                                    console.log("/api/v2/user/mailvalidation response 2");
                                    console.log(JSON.parse(encrypted));
                                    resolve(JSON.parse(encrypted));
                                }
                            } catch (e) {
                                console.log("/api/v2/user/mailvalidation response 3");
                                console.log(data);
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        })

        // return this.http.post(url, body)
        //     .toPromise();
    }

    validateOtp(otp: string, email: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const url = `${this.getApiUrl()}/api/v1/user/mailvalidation/otp`;
            const body = {
                identifier: email,
                otp,
            };
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                return this.http.post(url, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const encrypted = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    console.log("/api/v1/user/mailvalidation/otp response 1");
                                    console.log(encrypted);
                                    resolve(encrypted);
                                } catch (ee) {
                                    const encrypted = this.symmetricDecrypt(data, blueToken.key);
                                    console.log("/api/v1/user/mailvalidation/otp response 2");
                                    console.log(JSON.parse(encrypted));
                                    resolve(JSON.parse(encrypted));
                                }
                            } catch (e) {
                                console.log("/api/v1/user/mailvalidation/otp response 3");
                                console.log(data);
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        })
    }

    public saveImage(body: any): Observable<any> {
        return this.http.post(`${this.getApiUrl()}/user/image/save`, body);
    }

    public getImagePromise(userId: string, imageName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/user/${userId}/image?imagename=${imageName}`, {
                responseType: 'blob'
            })
                .subscribe({
                    next: (imageBlob) => {
                        if (!imageBlob) {
                            resolve(undefined);
                        } else {
                            try {
                                var reader = new FileReader();
                                var base64data;
                                reader.readAsDataURL(imageBlob);
                                reader.onloadend = async () => {
                                    base64data = reader.result;
                                    // this.globalProfilePicture = base64data;
                                    this.userPhotoServiceAkita.updatePhoto(base64data);
                                }
                            } catch (e) {
                                console.log(e)
                            }
                            const urlCreator = (window as any).URL || (window as any).webkitURL;
                            resolve(this.sanitizer.bypassSecurityTrustUrl(urlCreator.createObjectURL(imageBlob)));
                        }
                    }
                });
        })
    }

    public getImage(userId: string, imageName: string): Observable<any> {
        return this.http.get(`${this.getApiUrl()}/user/${userId}/image?imagename=${imageName}`, {
            responseType: 'blob'
        }).pipe(map(imageBlob => {
            if (!imageBlob) {
                return undefined;
            }
            // console.log(imageBlob)
            try {
                var reader = new FileReader();
                var base64data;
                reader.readAsDataURL(imageBlob);
                reader.onloadend = async () => {
                    base64data = reader.result;
                    // this.globalProfilePicture = base64data;
                    this.userPhotoServiceAkita.updatePhoto(base64data);
                }
            } catch (e) {
                console.log(e)
            }
            const urlCreator = (window as any).URL || (window as any).webkitURL;
            return this.sanitizer.bypassSecurityTrustUrl(urlCreator.createObjectURL(imageBlob));
        }));
    }

    public getIsInnovator(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.userProviderService.getUser().then(user => {
                this.http.get(`${this.getApiUrl()}/api/v1/service/innovator/${user.userid}`, {
                    headers: {

                    }
                })
                    .subscribe({
                        next: (data) => {
                            const b = (data as any).innovatorBadge === 1;
                            resolve(b);
                        },
                        error: (err) => reject(err)
                    });
            }).catch(e => console.log(e))
        });
    }





    generateDID(deviceId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/tnt/did/${deviceId}`, null)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    changePassword(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.put(`${this.getApiUrl()}/api/v1/user/changepassword`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("PUT /api/v1/user/changepassword response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("PUT /api/v1/user/changepassword response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("PUT /api/v1/user/changepassword response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    getUserEns(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/ens`, {
                responseType: 'text'
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getUserKeyBackup(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/keybackup`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    postUserKeyBackup(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/user/keybackup`, body, {
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    saveUserEthereumAddress(address: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/user/public-user-key`, address)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    saveUserWeb3WalletAddress(address: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/user/public-web3-key`, address)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    savePostUserWeb3WalletAddress(address: string): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log("address", address);
            this.http.post(`${this.getApiUrl()}/api/v1/user/public-web3-key`, address, {
                headers: {
                    "Content-Type": "text/plain"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getUserWeb3WalletAddress(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/public-web3-key`, {
                responseType: 'text'
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    saveUserconsent(body: any) {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/user/wallet-consent`, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getUserconsent() {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/wallet-consent`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    putDid(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/user/did`, null)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getDid(did: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/did/${did}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getVc(vcId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/vc/${vcId}`, {
                responseType: 'text'
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getVaccinationTrustList(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/eudgc/trustlist`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    // /api/v1/user/vc

    getVcIds(timestamp?: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/vc`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getTrustData(userid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/user/${userid}/trust`, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/${userid}/trust response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/user/${userid}/trust response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/user/${userid}/trust response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    logout(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/logout`)
                .subscribe({
                    next: (data) => {
                        this.appStateService.isAuthorized = false;
                        resolve(data);
                    },
                    error: (err) => reject(err)
                });
        });
    }

    getSecurePIN(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/pin`, {
                responseType: 'text'
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    postSecurePIN(encryptedPIN: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/user/pin`, `${encryptedPIN}`, {
                headers: {
                    "Content-Type": "text/plain"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    /**
     * Interface to retrieve user data
     * either store in secure cache
     * or in case of a different modification
     * date from the sever and then written
     * to the secure cache
     */

    addMerchantAsRoster(targetUser: string) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/user/chat/addmerchantasroster/${targetUser}`, {})
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    public getUserById(userId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                // console.log("blueToken.encrypted");
                // console.log(blueToken.encrypted);
                this.http.get(`${this.getApiUrl()}/user/basicdata/getbyuserid/${userId}`, {
                    headers: {
                        "x-api-key": blueToken.encrypted
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /user/basicdata/getbyuserid response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /user/basicdata/getbyuserid response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /user/basicdata/getbyuserid response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    public getAuthadaSessionStatus(sessionToken: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/authada/session/${sessionToken}`, {
                    headers: {
                        "x-api-key": blueToken.encrypted
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/authada/session response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/authada/session response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/serviceprovider/authada/session response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    /** Interface to get the current user and his image */
    public getUserImageAndData(getUser = true, getTrust = true): Promise<{ photo: string, userData: User, userTrust: Trust }> {
        return new Promise((resolve, reject) => {
            this.userProviderService.getUser().then(user => {

                this.getUserPhotoImageNames(user.userid).then(async photos => {
                    console.log("ApiProviderService#getUserImageAndData; photos", photos);
                    var photo = (photos.length == 0) ? undefined : await this.getImagePromise(user.userid, photos[0]);
                    var userData = !!getUser ? await this.getUserByIdSecure(user.userid) : {};
                    console.log("ApiProviderService#getUserImageAndData; userData", userData);
                    var userTrust = !!getTrust ? await this.getTrustData(user.userid) : {};
                    console.log("ApiProviderService#getUserImageAndData; userTrust", userTrust);
                    resolve({ photo, userData, userTrust });
                })
                //         console.log("photos", photos);
                //         var photo = (photos.length == 0) ? undefined : this.getImage(user.userid, photos[0]);
                //         this.getUserByIdSecure(user.userid).then(userData => {
                //             this.getTrustData(user.userid).then(userTrust => {
                //                 resolve({ photo, userData, userTrust });
                //             }).catch(e => {
                //                 resolve({ photo, userData, {} });
                //             })
                //         }).catch(e => {
                //             resolve({ photo, {}, {}})
                //         })
                //     })
                // })
            })
        })
        // return new Promise((resolve, reject) => {
        //     this.userProviderService.getUser().then(user => {
        //         this.getUserPhotoImageNames(user.userid).then(photos => {
        //             console.log("photos", photos);
        //             var photo = (photos.length == 0) ? undefined : this.getImage(user.userid, photos[0]);
        //             this.getUserByIdSecure(user.userid).then(userData => {
        //                 this.getTrustData(user.userid).then(userTrust => {
        //                     resolve({ photo, userData, userTrust });
        //                 }).catch(e => {
        //                     resolve({ photo, userData, {} });
        //                 })
        //             }).catch(e => {
        //                 resolve({ photo, {}, {}})
        //             })
        //         })
        //     })
        // })

        // return from(this.userProviderService.getUser())
        //     .pipe(
        //         switchMap(user => combineLatest(
        //             [from(this.getUserPhotoImageNames(user.userid))
        //                 .pipe(map(photos => {
        //                         return (photos.length ? photos[0] : undefined) as string;
        //                     }),
        //                     switchMap(fileName => fileName ?
        //                         this.getImage(user.userid, fileName) :
        //                         of(undefined)
        //                     )),
        //             getUser ?
        //                 from(this.getUserByIdSecure(user.userid)) :
        //                 of({}),
        //             getTrust ?
        //                 from(this.getTrustData(user.userid)) :
        //                 of({})
        //         ]))
        //     );
    }

    /** Interface to delete the user images */
    public deleteImages(userId: string): Promise<void> {
        return new Promise((resolve, reject) => {
            from(this.getUserPhotoImageNames(userId))
                .pipe(switchMap(imageNames => this.http.post(`${this.getApiUrl()}/user/${userId}/images/delete`,
                    imageNames,
                ))).subscribe(_ => resolve(), err => reject(err));
        });
    }


    public getUserByIdSecure(userId: string, username?: string, password?: string): Promise<any> {
        return new Promise(async (resolve, reject) => {
            let storedData: string;
            try {
                storedData = (await this.secureStorageService.getValue(SecureStorageKey.userData)) as string;
            } catch (e) {
                storedData = undefined;
            }
            const basicData: ExtendedBasicData = storedData ? JSON.parse(storedData) : undefined;
            const getData$ = this.userService.getbasicdata(userId).pipe(
                take(1),
                tap((extendedBasicData: ExtendedBasicData) => {
                    return this.secureStorageService.setValue(
                        SecureStorageKey.userData,
                        JSON.stringify(extendedBasicData)
                    );
                    // TODO Seza Clarify what should happen on error
                })
            );
            if (basicData) {
                this.http.get(`${this.getApiUrl()}/api/v1/user/lastmodifed`, {
                    headers: {

                    }
                }) // TODO SeZa Use swagger here
                    .pipe(
                        take(1),
                        flatMap((lastModified: number) => {
                            if (lastModified !== basicData.lastModified) {
                                return getData$;
                            } else {
                                return of(basicData);
                            }
                        })
                    ).subscribe((data: ExtendedBasicData) => resolve(data), reject);
            } else {
                getData$.subscribe((data: ExtendedBasicData) => resolve(data), reject);
            }
        });
    }

    getLastmodified() {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/user/lastmodifed`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getUserPhotos(userid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/user/${userid}/images`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    public getUserPhotoImageNames(userid: string): Promise<string[]> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/user/${userid}/imagenames`)
                .subscribe({
                    next: (data) => resolve(data as string[]),
                    error: (err) => reject(err)
                });
        });
    }

    getUserPhoto(userid: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(
                `${this.getApiUrl()}/user/${userid}image`
            ).subscribe({
                next: (data) => resolve(data),
                error: (err) => reject(err)
            });
        });
    }

    verifyDeeplinkPayload(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v2/verification`, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    claimDeeplinkPayload(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v2/claim`, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    sendFeedback(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                console.log("Feedback Encypted: " + encrypted + " : symmetricDecrypt: " + blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/service/feedback`, encrypted, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    },
                    responseType: 'text'
                })
                    .subscribe({
                        next: (data) => {
                            try {
                                const response = this.symmetricDecrypt(data, blueToken.key);
                                resolve(response);
                            } catch (e) {
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
                // })
            })
        });
    }

    //  generateVeriffSession(data: string): Promise<any> {
    generateVeriffSession(): Promise<VeriffInitResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/veriff/session`, {
                    //  this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/veriff/session?type=${data}`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/veriff/session Response 1");
                                        console.log(response);
                                    }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/veriff/session Response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/serviceprovider/veriff/session Response 3");
                                    console.log(data);
                                }
                                resolve(data as VeriffInitResponse);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })

        });
    }

    prepareVeriffSession(body: VeriffPrepareBody): Promise<VeriffInitResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.put(`${this.getApiUrl()}/api/v2/serviceprovider/veriff/session`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    // if(!this.isProd()) {
                                    console.log("PUT /api/v2/serviceprovider/veriff/session Response 1");
                                    console.log(response);
                                    // }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    // if(!this.isProd()) {
                                    console.log("PUT /api/v2/serviceprovider/veriff/session Response 2");
                                    console.log(JSON.parse(response));
                                    // }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                }
                            } catch (e) {
                                // if(!this.isProd()) {
                                console.log("PUT /api/v2/serviceprovider/veriff/session Response 3");
                                console.log(data);
                                // }
                                resolve(data as VeriffInitResponse);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    getVeriffSessionStatus(veriffSessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/veriff/session/${veriffSessionId}`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("/api/v1/serviceprovider/veriff/session/${veriffSessionId} response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("/api/v1/serviceprovider/veriff/session/${veriffSessionId} response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("/api/v1/serviceprovider/veriff/session/${veriffSessionId} response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    generateOndatoSession(): Promise<VeriffInitResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/ondato/session`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/ondato/session response 1");
                                        console.log(response);
                                    }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("/api/v1/serviceprovider/ondato/session response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response) as VeriffInitResponse);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("/api/v1/serviceprovider/ondato/session response 3");
                                    console.log(data);
                                }
                                resolve(data as VeriffInitResponse)
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    prepareOndatoSession(): Promise<VeriffInitResponse> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/serviceprovider/ondato/session`, {})
                .subscribe({
                    next: (data) => resolve(data as VeriffInitResponse),
                    error: (err) => reject(err)
                });
        });
    }

    getOndatoSessionStatus(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/ondato/session/${sessionId}`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/ondato/session/ response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/ondato/session/ response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response));
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/serviceprovider/ondato/session/ response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    getKycMedia(veriffSessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/kyc/media/session/${veriffSessionId}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getKycMediaTimestamp(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/kyc/media/lastupdate`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getKycMediaIds(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v2/kyc/media/session/${sessionId}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    initDataaccessprocess(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/public/dataaccessprocess/start`, body)
                .subscribe({
                    next: (data) => {
                        resolve(data);
                    },
                    error: (e) => {
                        reject(e);
                    }
                });
        });
    }

    fetchUserDecisionDataaccessprocess(dataAccessProcessId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/public/dataaccessprocess/${dataAccessProcessId}/getdata`)
                .subscribe({
                    next: (data) => {
                        resolve(data);
                    },
                    error: (e) => {
                        reject(e);
                    }
                });
        })
    }

    sendDataaccessprocess(processId: string, encryptedData: any, signature?: string): Promise<any> {
        return new Promise((resolve, reject) => {
            var headers = (!!signature) ? {
                headers: {

                    "x-signature": signature,
                    "Content-Type": "text/plain"
                }
            } : {
                headers: {

                    "Content-Type": "text/plain"
                }
            }
            this.http.post(`${this.getApiUrl()}/api/v1/public/dataaccessprocess/${processId}/senddata`, encryptedData, headers)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    confirmDataaccessprocess(processId: string, deviceId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/public/dataaccessprocess/${processId}/confirm/device/${deviceId}`, {
                responseType: 'text',
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    denyDataaccessprocess(processId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/public/dataaccessprocess/${processId}/deny`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getLatestEudgcCerts(): Promise<any> {
        return new Promise((resolve, reject) => {
            // curl https://api.undo-app.de/getcerts -H 'Accept: application/json' -H 'Content-Type: application/json;charset=UTF-8' --data-raw '{"includeraw": true}'
            this.http.post(`https://api.undo-app.de/getcerts`, { "includeraw": true }, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json;charset=UTF-8'
                },
                responseType: 'json',
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    deleteKycMedia(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.getApiUrl()}/api/v1/kyc/media/session/${sessionId}`, {

            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getKycMediaFiles(kycMediaId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/kyc/media/${kycMediaId}/file`, {
                responseType: 'blob'
            })
                .subscribe({
                    next: async (data) => {
                        reader.readAsDataURL(data);
                        reader.onloadend = function () {
                            const base64data = reader.result;
                            // console.log(base64data);
                            resolve(base64data)
                        }
                    },
                    error: (err) => reject(err)
                });
            var reader = new FileReader();
        })
    }

    addWaterMarkToImage(blob: any, image: string, text1: string, text2: string, rotateBoolean: boolean): Promise<any> {
        return new Promise((resolve, reject) => {

            var sallback = (dlob) => {
                fetch(dlob).then(resu => resu.blob())
                    .then((blobData) => {
                        // console.log(blobData);
                        watermark([blobData, image])
                            .image(watermark.image.upperLeft(0.8))
                            // .image(watermark.text.upperRight(text1,'30px Arial','#000',0.5))
                            .then((image) => {
                                fetch(image.src).then(resu2 => resu2.blob())
                                    .then((blobData2) => {

                                        var textToWaterMark = this.translateService.instant('LEGALDOCUMENTS.verifiedBy') + text1 + ' - ' + text2 + this.translateService.instant('LEGALDOCUMENTS.creationDate');
                                        var textToWaterMark1 = this.translateService.instant('LEGALDOCUMENTS.verifiedBy');
                                        var textToWaterMark2 = text1;
                                        var textToWaterMark3 = text2 + " UTC" + this.translateService.instant('LEGALDOCUMENTS.creationDate');

                                        var canvas = <HTMLCanvasElement>document.createElement("CANVAS");

                                        canvas.height = 130;
                                        canvas.width = 1060;
                                        canvas.style.border = "2px solid black";
                                        canvas.style['border-radius'] = "4px";
                                        canvas.style.paddingTop = "5px";
                                        canvas.style.paddingBottom = "5px";
                                        canvas.style.paddingLeft = "0px";
                                        canvas.style.paddingRight = "0px";

                                        var ctx = canvas.getContext('2d');

                                        ctx.fillStyle = "#D1D2D3";
                                        ctx.fillRect(0, 0, canvas.width, canvas.height);

                                        ctx.font = "40px Arial";
                                        ctx.fillStyle = "#000";
                                        ctx.textAlign = "left";
                                        ctx.fillText(textToWaterMark1, 10, 40);
                                        ctx.fillText(textToWaterMark2, 10, 80);
                                        ctx.fillText(textToWaterMark3, 10, 120);

                                        // console.log(canvas.style.width);
                                        // console.log(window.innerWidth);

                                        var canvasURL = canvas.toDataURL();

                                        // console.log(ctx);
                                        // console.log(canvas);
                                        // console.log(canvasURL);

                                        watermark([blobData2, canvasURL])
                                            // watermark([blobData, canvasURL])
                                            .image(watermark.image.lowerLeft(0.8))
                                            .then((image2) => {
                                                // console.log('ADD WATERMARK SUCCESS')
                                                // console.log(image2);
                                                resolve(image2);
                                            })
                                    })
                            }).catch((e) => {
                                // console.log('ADD WATERMARK FAILURE')
                                // console.log(e);
                                reject();
                            })
                    })
            }

            var rotate = (slob) => {
                var sanvas = document.createElement("canvas");
                var ctx = sanvas.getContext("2d");

                var simage = new Image();

                simage.onload = function () {
                    sanvas.width = simage.height;
                    sanvas.height = simage.width;

                    ctx.translate(sanvas.width / 2, sanvas.height / 2);
                    ctx.rotate(90 * Math.PI / 180);
                    // ctx.drawImage(simage,  -0.035 * simage.width, -0.35 * simage.height);
                    ctx.drawImage(simage, simage.width / -2, simage.height / -2);
                    // console.log(sanvas.toDataURL());
                    sallback(sanvas.toDataURL());
                };

                simage.src = slob;
            }

            if (rotateBoolean) {
                rotate(blob)
            } else {
                sallback(blob)
            }

        })
    }

    // /api/v1/kyc/media/session/{sessionId}

    // /api/v1/kyc/media/{kycMediaId}

    // /api/v1/kyc/media/{kycMediaId}/file


    getKycStatus(timestamp?: string): Promise<any> {
        var requestUrl = '/api/v1/user/kycstatus';
        if (!!timestamp) {
            requestUrl = '/api/v1/user/kycstatus?timestamp=' + timestamp;
        }
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}${requestUrl}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    tntSendApproval(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/tnt/proof-request`, body)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    getAssetFromFileIdToken(fileId: string, token: string, mimeType?: string): Promise<any> {
        var url = `${this.getApiUrl()}/api/v1/tnt/file/${fileId}?token=${token}`;
        if (mimeType) {
            url = `${url}&mimeType=${mimeType}`;
        }
        return new Promise((resolve, reject) => {
            this.http.get(url, {
                responseType: 'blob',

            })
                .subscribe({
                    next: (data) => {
                        reader.readAsDataURL(data);
                        reader.onloadend = async () => {
                            var base64data = reader.result;
                            // console.log(base64data)

                            var media = await this.secureStorageService.getValue(SecureStorageKey.certificatesMedia, false)
                            var emptyMediaArray = {}
                            if (media) {
                                emptyMediaArray = JSON.parse(media)
                            }
                            emptyMediaArray = Object.assign(emptyMediaArray, {
                                [fileId]: base64data
                            })
                            await this.secureStorageService.setValue(SecureStorageKey.certificatesMedia, JSON.stringify(emptyMediaArray))
                            resolve(base64data)
                        }
                    },
                    error: (err) => reject(err)
                });
            var reader = new FileReader();
        })
    }

    getAssetFromAssetId(assetId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/tnt/asset?assetId=${assetId}`, {
                responseType: 'blob',
            })
                .subscribe({
                    next: (data) => {
                        reader.readAsDataURL(data);
                        reader.onloadend = async () => {
                            var base64data = reader.result;
                            // console.log(base64data)

                            var media = await this.secureStorageService.getValue(SecureStorageKey.certificatesMedia, false)
                            var emptyMediaArray = {}
                            if (media) {
                                emptyMediaArray = JSON.parse(media)
                            }
                            emptyMediaArray = Object.assign(emptyMediaArray, {
                                [assetId]: base64data
                            })
                            await this.secureStorageService.setValue(SecureStorageKey.certificatesMedia, JSON.stringify(emptyMediaArray))
                            resolve(base64data)
                        }
                    },
                    error: (err) => reject(err)
                });
            var reader = new FileReader();
        })
    }

    checkForAppUpdates(os: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/app-version?os=${os}`, {
                responseType: 'text'
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        })
    }

    postKycStatus(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/user/kycstatus`, body, {
                headers: {
                    "Content-Type": "application/json"
                }
            })
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    resetPassword(body: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(body, blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/resetpassword`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword response 2");
                                        console.log(response);
                                    }
                                    resolve(response);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/resetpassword response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    resetPasswordOtp(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/resetpassword/otp`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword/otp response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword/otp response 2");
                                        console.log(response);
                                    }
                                    resolve(response);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/resetpassword/otp response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    resetPasswordReset(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/resetpassword/reset`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword/reset response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/resetpassword/reset response 2");
                                        console.log(response);
                                    }
                                    resolve(response);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/resetpassword/reset response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    authadaInit(): Promise<AuthadaInitResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/authada/init`, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/authada/init response 1");
                                        console.log(response);
                                    }
                                    resolve(JSON.parse(response) as AuthadaInitResponse);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("GET /api/v1/serviceprovider/authada/init response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response) as AuthadaInitResponse);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("GET /api/v1/serviceprovider/authada/init response 3");
                                    console.log(data);
                                }
                                resolve(data as AuthadaInitResponse);
                            }
                        },
                        error: (err) => reject(err)
                    });
            })
        });
    }

    public authadaCheckPromise(sessionId: string): Promise<VerificationCheckResponse> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/authada/verificationprocess/${sessionId}`)
                .subscribe({
                    next: (data) => resolve(data as VerificationCheckResponse),
                    error: (err) => reject(err)
                });
        });
    }

    /** Check for Authada session status */


    public veriffCheckPromise(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/veriff/verificationprocess/${sessionId}`)
                .subscribe({
                    next: (data) => resolve(data as VerificationCheckResponse),
                    error: (err) => reject(err)
                });
        })
    }

    public verifeyeCheckPromise(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/verifeye/verificationprocess/${sessionId}`)
                .subscribe({
                    next: (data) => resolve(data as VerificationCheckResponse),
                    error: (err) => reject(err)
                });
        })
    }

    public ondatoCheckPromise(sessionId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/ondato/verificationprocess/${sessionId}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    public getUsernameFromEthAddress(ethAddress: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/helixidethaddress?eth=${ethAddress}`, {
                // headers: {
                //
                // }
            })
                .subscribe({
                    next: (data: any) => resolve(data),
                    error: (err) => resolve(null)
                });
        })
    }

    public getEthAddressFromUsername(username: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/helixidethaddress?username=${username}`, {
                // headers: {
                //
                // }
            })
                .subscribe({
                    next: (data: any) => resolve(data),
                    error: (err) => resolve(null)
                });
        })
    }

    public getHelixPublicKey(): Observable<string> {
        const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        return this.http.get(`${this.getApiUrl()}/helix/publickey`, { headers, responseType: 'text' });
    }

    public getHelixPublicKeyPromise(): Promise<string> {
        // const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/helix/publickey`, {
                headers: {
                    'Content-Type': 'text/plain; charset=utf-8',
                }, responseType: 'text'
            })
                .subscribe({
                    next: (data) => {
                        try {
                            resolve(JSON.parse(data));
                        } catch (e) {
                            resolve(data);
                        }
                    },
                    error: (err) => reject(err)
                });
        })
    }


    deleteCouponCode(productId: string, coupon: string, pageNumber: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.delete(`${this.getApiUrl()}/api/v1/marketplace/coupon/${productId}/${coupon}/${pageNumber}`)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }



    getVerifeyeSession(country: string): Promise<VerifeyeResponse> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/api/v1/serviceprovider/verifeye/session`, {
                    headers: {

                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    },
                    params: { country }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("/api/v1/serviceprovider/verifeye/session response 1");
                                        console.log(response);
                                    }
                                    resolve(JSON.parse(response) as VerifeyeResponse);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (this.isProd()) {
                                        console.log("/api/v1/serviceprovider/verifeye/session response 2");
                                        console.log(JSON.parse(response));
                                    }
                                    resolve(JSON.parse(response) as VerifeyeResponse);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("/api/v1/serviceprovider/verifeye/session response 3");
                                    console.log(data);
                                }
                                resolve((data as VerifeyeResponse))
                            }
                        },
                        error: async (err) => {
                            this.loaderService.loaderDismiss();
                            this.toastController.create({
                                message: this.translateService.instant('LOADER.somethingWrong'),
                                duration: 2000,
                                position: 'top'
                            }).then((toast) => { toast.present() })
                            reject(err);
                        }
                    });
            })
        })
    }

    sealoneTestRegister(sealoneId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/sealone/testregister/${sealoneId}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    sealoneTest(sealoneId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/sealone/test/${sealoneId}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    sealoneUnregister(sealoneId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/sealone/unregister/${sealoneId}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    sealoneRegister(sealoneId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/sealone/register/${sealoneId}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    cryptoConversion(source: string, target: string): Promise<any> {
        source = source.includes("HMT") ? "HMT" : source;
        source = source.includes("MATIC") ? "MATIC" : source;
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/crypto/conversion?source=${source}&target=${target}`)
                .subscribe({
                    next: (data) => {
                        resolve(data)
                    },
                    error: (err) => {
                        this.secureStorageService.getValue(SecureStorageKey.cryptoConversions, false).then(cc => {
                            var cryptoConversions = !!cc ? JSON.parse(cc) : {};

                            switch (source) {
                                case CryptoCurrency.ETH: var def = 1164.9638686127344; break;
                                case CryptoCurrency.MATIC: def = 0.39742958409304247; break;
                                case CryptoCurrency.GNOSIS: def = 0.9544788340057131; break;
                                case CryptoCurrency.HMT_POLYGON: def = 0.18691347043539436; break;
                                case CryptoCurrency.HMT_SKALE: def = 0.18691347043539436; break;
                                case CryptoCurrency.ARETH: def = 0; break;
                                case CryptoCurrency.BLUR: def = 0; break;
                                case CryptoCurrency.BNB_BEACON: def = 0; break;
                                case CryptoCurrency.BNB_SMART: def = 0; break;
                                case CryptoCurrency.MKR: def = 0; break;
                                case CryptoCurrency.EGLD: def = 0; break;
                                case CryptoCurrency.NEAR: def = 0; break;
                                case CryptoCurrency.OP: def = 0; break;
                                case CryptoCurrency.XTZ: def = 0; break;
                                case CryptoCurrency.UNI: def = 0; break;
                                case CryptoCurrency.AAVE: def = 0; break;
                                case CryptoCurrency.AE: def = 0; break;
                                case CryptoCurrency.AION: def = 0; break;
                                case CryptoCurrency.APE: def = 0; break;
                                case CryptoCurrency.AURORA: def = 0; break;
                                case CryptoCurrency.AVAX: def = 0; break;
                                case CryptoCurrency.AXS: def = 0; break;
                                case CryptoCurrency.BUSD: def = 0; break;
                                case CryptoCurrency.CELO: def = 0; break;
                                case CryptoCurrency.LINK: def = 0; break;
                                case CryptoCurrency.CRO: def = 0; break;
                                case CryptoCurrency.MANA: def = 0; break;
                                case CryptoCurrency.DOGE: def = 0; break;
                                case CryptoCurrency.DYDX: def = 0; break;
                                case CryptoCurrency.ENS: def = 0; break;
                                case CryptoCurrency.FTM: def = 0; break;
                                case CryptoCurrency.FIL: def = 0; break;
                                case CryptoCurrency.FLOW: def = 0; break;
                                case CryptoCurrency.IMX: def = 0; break;
                                case CryptoCurrency.CAKE: def = 0; break;
                                case CryptoCurrency.SAND: def = 0; break;
                                case CryptoCurrency.TRON: def = 0; break;
                                case CryptoCurrency.TWT: def = 0; break;
                                default: def = 0; break;
                            }


                            resolve(!!cryptoConversions[`${source}/${target}`] ? cryptoConversions[`${source}/${target}`] : def);

                        })
                    }
                });
        });
    }

    pretixMwBuyTicket(organizer: string, event: string) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/orders`, {})
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwBuyTicketv2(organizer: string, event: string, itemNumber: any) {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/orders/item/${itemNumber}`, null)
                .subscribe({
                    next: (data) => resolve(data),
                    error: (err) => reject(err)
                });
        });
    }

    pretixMwGetEventInfo(organizer: string, event: string) {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwGetOrderInfo(organizer: string, event: string, order: string) {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/orders/${order}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwGetTicket(organizer: string, event: string, code: string) {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/orders/${code}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwCheckIntoEvent(organizer: string, event: string, code: string) {
        return new Promise((resolve, reject) => {
            this.http.patch(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/orders/${code}`, {})
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwSendPushNotification(organizer: string, event: string, username: string) {
        return new Promise((resolve, reject) => {
            this.http.put(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}/events/${event}/user/${username}`, {})
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixMwGetOrganizerInfo(organizer: string) {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/pretix/organizers/${organizer}`)
                .subscribe(res => resolve(res), err => reject(err));
        });
    }

    pretixFetchOrganizer(token: string, baseUrl: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${baseUrl}/api/v1/organizers`, {
                headers: {
                    Authorization: `Token ${token}`
                }
            }).subscribe(res => resolve(res), err =>
                // reject(err);
                resolve({
                    "count": 1,
                    "next": null,
                    "previous": null,
                    "results": [
                        {
                            "name": "Blockchain HELIX AG",
                            "slug": "BCHX"
                        }
                    ]
                })
            );
        });
    }

    pretixFetchEvent(token: string, baseUrl: string, organizer: string, event: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${baseUrl}/api/v1/organizers/${organizer}/events/${event}/`, {
                headers: {
                    Authorization: `Token ${token}`
                }
            }).subscribe(res => resolve(res), err =>
                // reject(err)
                resolve({
                    "name": {
                        "en": "The Long Island Colosseum"
                    },
                    "slug": "tlisc",
                    "live": false,
                    "testmode": true,
                    "currency": "EUR",
                    "date_from": "2021-10-01T15:00:00Z",
                    "date_to": "2021-10-01T21:00:00Z",
                    "date_admission": null,
                    "is_public": true,
                    "presale_start": null,
                    "presale_end": null,
                    "location": {
                        "en": "Münchener Straße 45HH,\r\n60329 Frankfurt am Main"
                    },
                    "geo_lat": null,
                    "geo_lon": null,
                    "has_subevents": false,
                    "meta_data": {},
                    "seating_plan": null,
                    "plugins": [
                        "pretix.plugins.sendmail",
                        "pretix.plugins.statistics",
                        "pretix.plugins.ticketoutputpdf"
                    ],
                    "seat_category_mapping": {},
                    "timezone": "UTC",
                    "item_meta_properties": {},
                    "valid_keys": {
                        "pretix_sig1": []
                    },
                    "sales_channels": [
                        "web"
                    ]
                })
            );
        });
    }

    pretixFetchOrder(token: string, baseUrl: string, organizer: string, event: string, order: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${baseUrl}/api/v1/organizers/${organizer}/events/${event}/orders/${order}/`, {
                headers: {
                    Authorization: `Token ${token}`
                }
            }).subscribe(res => resolve(res), err =>
                // reject(err)
                resolve({
                    "code": "P0F9X",
                    "status": "p",
                    "testmode": true,
                    "secret": "gcyknfky9lk8kgu2",
                    "email": "ansik91@gmail.com",
                    "phone": "+918763955320",
                    "locale": "en",
                    "datetime": "2021-09-14T10:19:14.747938Z",
                    "expires": "2021-09-23T23:59:59Z",
                    "payment_date": "2021-09-09",
                    "payment_provider": "free",
                    "fees": [],
                    "total": "0.00",
                    "comment": "",
                    "custom_followup_at": null,
                    "invoice_address": {
                        "last_modified": "2021-09-09T10:52:50.528593Z",
                        "is_business": false,
                        "company": "",
                        "name": "Ansik Mahapatra",
                        "name_parts": {
                            "_scheme": "full",
                            "full_name": "Ansik Mahapatra"
                        },
                        "street": "Adam-Opel-Strasse 24",
                        "zipcode": "60386",
                        "city": "Frankfurt am Main",
                        "country": "DE",
                        "state": "",
                        "vat_id": "",
                        "vat_id_validated": false,
                        "internal_reference": ""
                    },
                    "positions": [
                        {
                            "id": 2,
                            "order": "P0F9X",
                            "positionid": 1,
                            "item": 1,
                            "variation": null,
                            "price": "0.00",
                            "attendee_name": "Ansik Mahapatra",
                            "attendee_name_parts": {
                                "_scheme": "full",
                                "full_name": "Ansik Mahapatra"
                            },
                            "company": null,
                            "street": null,
                            "zipcode": null,
                            "city": null,
                            "country": null,
                            "state": null,
                            "attendee_email": "ansik91@gmail.com",
                            "voucher": null,
                            "tax_rate": "0.00",
                            "tax_value": "0.00",
                            "secret": "ns5ccv7vbkypcncz8jtym4d8c2q7p8w9",
                            "addon_to": null,
                            "subevent": null,
                            "checkins": [],
                            "downloads": [
                                {
                                    "output": "pdf",
                                    "url": "http://localhost:8000/api/v1/organizers/BCHX/events/kcukc/orderpositions/2/download/pdf/"
                                }
                            ],
                            "answers": [],
                            "tax_rule": null,
                            "pseudonymization_id": "XJFLQBR973",
                            "seat": null,
                            "canceled": false
                        }
                    ],
                    "downloads": [
                        {
                            "output": "pdf",
                            "url": "http://localhost:8000/api/v1/organizers/BCHX/events/kcukc/orders/P0F9X/download/pdf/"
                        }
                    ],
                    "checkin_attention": false,
                    "last_modified": "2021-09-09T10:52:50.526958Z",
                    "payments": [
                        {
                            "local_id": 1,
                            "state": "confirmed",
                            "amount": "0.00",
                            "created": "2021-09-09T10:19:14.767467Z",
                            "payment_date": "2021-09-09T10:19:14.816328Z",
                            "provider": "free",
                            "payment_url": null,
                            "details": {}
                        }
                    ],
                    "refunds": [],
                    "require_approval": false,
                    "sales_channel": "web",
                    "url": "http://localhost/BCHX/kcukc/order/P0F9X/gcyknfky9lk8kgu2/",
                    "customer": null
                })
            );
        });
    }

    pretixFetchTicket(token: string, baseUrl: string, organizer: string, event: string, order: string, download: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${baseUrl}/api/v1/organizers/${organizer}/events/${event}/orders/${order}/download/${download}/`, {
                headers: {
                    Authorization: `Token ${token}`
                }
            }).subscribe(res => resolve(res), err => reject(err));
        });
    }

    isCountryStateSupportedForQuarantine(country: string, state: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/quarantine/support?country=${country}&state=${state}`, {}).subscribe(res => resolve(res.toString() == "true"), err => reject(err));
        });
    }

    getHelixVersion(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                this.http.get(`${this.getApiUrl()}/helix/version`, {
                    headers: {
                        "x-api-key": blueToken.encrypted
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            const response = this.symmetricDecrypt(data, blueToken.key);
                            // console.log(`Lothar version endpoint: ${response}`)
                            resolve(JSON.parse(response));
                        },
                        error: (err) => reject(err)
                    });
            })

        });
    }

    getQuarantineQuestions(lang: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/quarantine?code=${lang}`, {}).subscribe(res => resolve(res), err => reject(err));
        });
    }

    postQuarantineAnswers(body: any, lang: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/quarantine?code=${lang}`, body, {}).subscribe(res => resolve(res), err => reject(err));
        });
    }

    fetchUserActivities(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${this.getApiUrl()}/api/v1/activities`)
                .subscribe({
                    next: (data: any) => {
                        console.info("Fetch User Activities", data);
                        for (let _ = 0; _ < data.content.length; _++) {
                            if (!!data.content[_].additionalInformation) {
                                data.content[_].additionalInformation = JSON.parse(data.content[_].additionalInformation);
                            }
                        }
                        resolve(data);
                    },
                    error: (err) => reject(err)
                });
        });
    }



    obtainDisplayableAddress(address: string) {
        return address.slice(0, 7) + "..." + address.slice(address.length - 7, address.length);
    }

    updateUserActivities(body: UserActivity): Promise<any> {
        return new Promise((resolve, reject) => {
            if (!!body.additionalInformation) {
                body.additionalInformation = JSON.stringify(body.additionalInformation);
            }
            this.http.post(`${this.getApiUrl()}/api/v1/activities`, body, {
                headers: {

                    "Content-Type": "application/json"
                }
            })
                .subscribe({
                    next: (data) => {
                        resolve(data);
                    },
                    error: (err) => {
                        reject(err);
                    }
                });
        });
    }

    blocknativeTrackTransaction(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(`${this.getApiUrl()}/api/v1/blocknative/transaction`, body, {
                headers: {

                    "Content-Type": "application/json"
                }
            })
                .subscribe({
                    next: (data) => {
                        resolve(data);
                    },
                    error: (err) => {
                        reject(err);
                    }
                });
        })
    }

    checkForIDLinkWalletUpdate(address: string): Promise<boolean> {
        return new Promise((resolve, reject) => {

            const process = (newName: string): Promise<boolean> => {
                return new Promise((resolve, reject) => {
                    console.log("Attempt 1");
                    this.savePostUserWeb3WalletAddress(address).then(result => {

                        console.log("Check 3");
                        // if(result) {
                        this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then(web3WalletPublicKey => {
                            this.secureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false).then(web3WalletMnemonic => {
                                this.secureStorageService.getValue(SecureStorageKey.web3WalletName, false).then(web3WalletName => {
                                    this.userProviderService.getUser().then(userData => {
                                        this.secureStorageService.getValue(SecureStorageKey.importedWallets, false).then(im => {
                                            const imW = !!im ? JSON.parse(im) : [];

                                            const thisWallet = imW.find(k => k.publicKey == address);

                                            const colorschemes = ['lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan'];
                                            const random = Math.floor(Math.random() * colorschemes.length);
                                            const color = colorschemes[random];

                                            imW.push({
                                                heading: !!web3WalletName ? web3WalletName : userData.callname,
                                                publicKey: web3WalletPublicKey,
                                                mnemonic: web3WalletMnemonic,
                                                creationDate: +new Date(),
                                                color
                                            })

                                            const filtered = imW.filter(k => k.publicKey != address);

                                            console.log({ filtered });
                                            console.log({ thisWallet });
                                            console.log({ address });
                                            console.log({ filtered });
                                            console.log({ newName });

                                            if (thisWallet) {

                                                if (thisWallet.mnemonic) {
                                                    console.log("Check 4");

                                                    const wallet = ethers.Wallet.fromMnemonic(thisWallet.mnemonic);

                                                    this.secureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, wallet.mnemonic.phrase).then(() => {
                                                        this.secureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey).then(() => {
                                                            this.secureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address).then(() => {
                                                                this.secureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(filtered)).then(() => {
                                                                    this.secureStorageService.setValue(SecureStorageKey.web3WalletName, newName).then(() => {
                                                                        this.presentToast(this.translateService.instant('WALLET.conversion-done'));
                                                                        resolve(true);
                                                                    })
                                                                })
                                                            })
                                                        })
                                                    })
                                                }


                                            } else {

                                                console.log("Check 5");

                                                this.secureStorageService.removeValue(SecureStorageKey.web3WalletMnemonic, false).then(() => {
                                                    this.secureStorageService.removeValue(SecureStorageKey.web3WalletPrivateKey, false).then(() => {
                                                        this.secureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, address).then(() => {
                                                            this.secureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(filtered)).then(() => {
                                                                this.secureStorageService.setValue(SecureStorageKey.web3WalletName, newName).then(() => {
                                                                    this.presentToast(this.translateService.instant('WALLET.conversion-done'));
                                                                    resolve(true);
                                                                })
                                                            })
                                                        })
                                                    })
                                                })

                                            }
                                        })
                                    })
                                })
                            })
                        })
                        // }

                    }).catch(e => {
                        console.log(e);
                        resolve(false);
                    });
                })

            }


            this._AlertController.create({
                mode: 'ios',
                header: this.translateService.instant('WALLET.renameWallet'),
                message: this.translateService.instant('WALLETSETTINGS.for-verifed'),
                inputs: [{ type: 'text', placeholder: 'Satoshi...', name: 'name' }],
                buttons: [{
                    text: this.translateService.instant('SETTINGS.yes'),
                    handler: async (data) => {
                        const wallets = await this.secureStorageService.getValue(SecureStorageKey.importedWallets, false);
                        const walletsParse = !!wallets ? JSON.parse(wallets) : [];

                        const headings = Array.from(walletsParse, k => (k as any).heading);
                        const check = walletsParse.find(w => w.publicKey == address);

                        console.log("Check 1");
                        // if(check) {

                        if (!data.name || data.name == '') {
                            this.presentToast(this.translateService.instant('WALLETSETTINGS.errors.empty-string'));
                            resolve(false);
                        }

                        if (headings.includes(data.name)) {
                            this.presentToast(this.translateService.instant('WALLETSETTINGS.errors.name-used'));
                            resolve(false);
                        }

                        if (data.name.trim().length > 20) {
                            this.presentToast(this.translateService.instant('WALLETSETTINGS.errors.invalid-length'));
                            resolve(false);
                        }

                        console.log("Check 2");

                        process(data.name).then(res => {
                            if (!res) {
                                this.presentToast(this.translateService.instant('IMPORTKEY.something-wrong'));
                                console.log("resolve(false)");
                                resolve(false);
                            } else {
                                console.log("resolve(true)");
                                resolve(true);
                            }
                        })

                        // }

                    }
                }, {
                    text: this.translateService.instant('SETTINGS.no'),
                    role: 'cancel',
                    handler: () => {
                    }
                }]
            }).then(alertp => alertp.present());

        })

    }

    recoveryPhrase(body: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(body, blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/recoveryphrase`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/recoveryphrase response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/recoveryphrase response 2");
                                        console.log(response);
                                    }
                                    resolve(response);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/recoveryphrase response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => {
                            // reject(err);
                            resolve({
                                errorFound: false,
                                errors: []
                            })
                        }
                    });
            })
        });
    }



    recoveryPhraseOtp(body: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.generateBlueTokens().then(blueToken => {
                var encrypted = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);
                this.http.post(`${this.getApiUrl()}/api/v1/user/recoveryphrase/otp`, encrypted, {
                    headers: {
                        "x-api-key": blueToken.encrypted,
                        "Content-Type": "application/json"
                    }
                })
                    .subscribe({
                        next: (data: any) => {
                            try {
                                try {
                                    const response = this.symmetricDecrypt(JSON.parse(data), blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/recoveryphrase/otp response 1");
                                        console.log(response);
                                    }
                                    resolve(response);
                                } catch (ee) {
                                    const response = this.symmetricDecrypt(data, blueToken.key);
                                    if (!this.isProd()) {
                                        console.log("POST /api/v1/user/recoveryphrase/otp response 2");
                                        console.log(response);
                                    }
                                    resolve(response);
                                }
                            } catch (e) {
                                if (!this.isProd()) {
                                    console.log("POST /api/v1/user/recoveryphrase/otp response 3");
                                    console.log(data);
                                }
                                resolve(data);
                            }
                        },
                        error: (err) => {
                            // reject(err);
                            resolve('ce5fc524-3ee9-4d4b-91f1-f7aa98ced1a9'); // For PunyaR123
                        }
                    });
            })
        });
    }

    async noUserLoggedInAlert() {
        await this._AlertServiceProvider.alertCreate(
            this.translateService.instant('BROWSEMODE.heading'),
            undefined,
            this.translateService.instant('BROWSEMODE.message'),
            [
                {
                    text: this.translateService.instant('BROWSEMODE.buttons.login'),
                    handler: async () => { this.nav.navigateForward('/login?from=browsemode'); }
                },
                {
                    text: this.translateService.instant('BROWSEMODE.buttons.signup'),
                    handler: async () => { this.nav.navigateForward('/sign-up/step-9'); }
                },
                {
                    text: this.translateService.instant('BROWSEMODE.buttons.keep-browsing'),
                    handler: async () => { }
                }
            ]
        );
    }

    private presentToast(message: string) {
        this.toastController.create({
            message,
            duration: 2000,
            position: 'top'
        }).then(toast => toast.present())
    }

    private getApiUrl(): string {
        return `${this.appConfig.apiUrl}`;
    }

    private isProd(): boolean {
        return (this.appConfig.helixEnv == 'production');
    }



    public getAuthada(): string {
        if (this.platform.is('ios')) {
            return `${this.appConfig.authadaiOS}`;
        } else if (this.platform.is('android')) {
            return `${this.appConfig.authadaAndroid}`;
        }
    }

}
