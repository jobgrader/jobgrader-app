import { Injectable } from '@angular/core';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AppUpdatesService {

  private number: string;

  constructor(
    private _ApiProviderService: ApiProviderService,
    private _AppVersion: AppVersion,
    private _Platform: Platform,
  ) { }

  // -3: Endpoint Error
  // -2: Platform not supported
  // -1: Device uses an unreleased version of the app
  // 0: Device uses the same version as that in the app store
  // 1: Update available

  public async checkForUpdate(): Promise<number> {
    if((window as any).cordova) {
      this.number = await this._AppVersion.getVersionNumber();
    } else {
      this.number = "1.2.1" // Only for testing; Please use different values to test this the endpoint
    }
    var os = this._Platform.is('ios') ? 'ios' : ( this._Platform.is('android') ? 'android' : null )
    // console.log(os)
    if(os) {
      var update = await this._ApiProviderService.checkForAppUpdates(os).catch(async (e) => {  return -3 })
      // console.log(update);
      // console.log(this.number);
      var checkStringValue = this.checkStringArray(this.number.split("."), update.split("."))
      // console.log(checkStringValue)
      return checkStringValue
    }
    return -2
  }

  checkStringArray(thisDeviceVersion: Array<String>, appStoreVersion: Array<String>){
    return  Number(appStoreVersion[0]) > Number(thisDeviceVersion[0]) ? 1 :
            Number(appStoreVersion[0]) < Number(thisDeviceVersion[0]) ? -1 :
            Number(appStoreVersion[0]) == Number(thisDeviceVersion[0]) ? (
              Number(appStoreVersion[1]) > Number(thisDeviceVersion[1]) ? 1 :
              Number(appStoreVersion[1]) < Number(thisDeviceVersion[1]) ? -1 :
              Number(appStoreVersion[1]) == Number(thisDeviceVersion[1]) ? (
                Number(appStoreVersion[2]) > Number(thisDeviceVersion[2]) ? 1 :
                Number(appStoreVersion[2]) < Number(thisDeviceVersion[2]) ? -1 :
                Number(appStoreVersion[2]) == Number(thisDeviceVersion[2]) ? 0 : (
                  null
                )
              ) : 0
          ) : 0
  }

}
