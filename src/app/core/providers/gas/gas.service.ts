import { Injectable } from '@angular/core';
import { CryptoCurrency } from '../wallet-connect/constants';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


export interface BlockNativeEstimatedPrice {
  confidence: number;
  price: number;
  maxPriorityFeePerGas: number;
  maxFeePerGas: number;
}

export interface BlockNativeBlockPrice {
  blockNumber: number;
  estimatedTransactionCount: number;
  baseFeePerGas: number;
  estimatedPrices: Array<BlockNativeEstimatedPrice>;
}

export interface BlockNativeGas {
  system: string;
  network: string;
  unit: string;
  maxPrice: number;
  currentBlockNumber: number;
  msSinceLastBlock: number;
  blockPrices: Array<BlockNativeBlockPrice>;
  estimatedBaseFees: Array<any>;
}


export interface HelixGasEstimationObject {
  unit: string;
  normal: string;
  fast: string;
}

export const GAS_USED = {
  [CryptoCurrency.ETH] : 21000,
  [CryptoCurrency.MATIC] : 21000,
  [CryptoCurrency.GNOSIS] : 21000,
  [CryptoCurrency.EVE] : 38100
}

@Injectable({
  providedIn: 'root'
})
export class GasService {

  constructor(
    private _HttpClient: HttpClient,
    private _Vault: VaultService
  ) { }

  returnScanGasPrice(currency: CryptoCurrency): Promise<HelixGasEstimationObject> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY).then((etherscanAPIKey) => {
        this._Vault.getSecret(VaultSecretKeys.POLYGONSCAN_API_KEY).then((POLYGONSCAN_API_KEY) => {
          const url = (currency == CryptoCurrency.ETH) ? `https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=${etherscanAPIKey}` : `https://api.polygonscan.com/api?module=gastracker&action=gasoracle&apikey=${POLYGONSCAN_API_KEY}`;
          this._HttpClient.get(url, {
            headers: {

            }
          }).subscribe({
          next: (response: any) => {
            resolve(<HelixGasEstimationObject>{
              unit: "gwei",
              normal: (GAS_USED[currency] * Number(response.result.ProposeGasPrice)).toString(),
              fast: (GAS_USED[currency] * Number(response.result.FastGasPrice)).toString()
            })
          },
          error: (e) => {
            reject(e);
          }
        })

        })
      })
    })
  }

  getGasPriceEstimationEVE(): Promise<HelixGasEstimationObject> {

    return new Promise((resolve, reject) => {

        resolve(<HelixGasEstimationObject>{
          unit: "gwei",
          normal: (GAS_USED[CryptoCurrency.EVE]*100).toString(),
          fast: (GAS_USED[CryptoCurrency.EVE]*120).toString()
        })

      })

  }

  getGasPriceEstimationXDAI(): Promise<HelixGasEstimationObject> {

    return new Promise((resolve, reject) => {
  // const url = 'https://blockscout.com/xdai/mainnet/api/v1/gas-price-oracle';
      // const url = 'https://ggnosis.blockscan.com/gasapi.ashx?apikey=key&method=gasoracle';
      const url = 'https://api.gnosisscan.io/api?module=proxy&action=eth_gasPrice';

      return new Promise((resolve, reject) => {
        this._HttpClient.get(url,{
          headers: {
            "Content-Type": "application/json"
          }
        }).subscribe({
          next: (response: any) => {
            resolve(<HelixGasEstimationObject>{
              unit: "gwei",
              normal: (GAS_USED[CryptoCurrency.GNOSIS] * response.result.ProposeGasPrice).toString(),
              fast: (GAS_USED[CryptoCurrency.GNOSIS] * response.result.FastGasPrice).toString()
            });
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })

  }

  getGasPriceEstimationBlockNative(currency: CryptoCurrency): Promise<HelixGasEstimationObject> {

    const chainId = ((currency == CryptoCurrency.ETH) ? (1) : (137) );
    const url = !!chainId ? `https://api.blocknative.com/gasprices/blockprices?chainId=${chainId}` : "https://api.blocknative.com/gasprices/blockprices";

    console.log(url);

    return new Promise((resolve, reject) => {

      this._Vault.getSecret(VaultSecretKeys.BLOCKNATIVE_API_KEY).then(blockNativeAPIKey => {
        this._HttpClient.get(url,{
          headers: {
            "Content-Type": "application/json",
            "Authorization": blockNativeAPIKey
          }
        }).subscribe({
          next: (response) => {
            var blockNativeGas = <BlockNativeGas>response;
            var gasArray = Array.from(blockNativeGas.blockPrices[0].estimatedPrices, k => k.maxFeePerGas);

            resolve(<HelixGasEstimationObject>{
              unit: "gwei",
              fast: (GAS_USED[currency] * Math.max(...gasArray)).toString(),
              normal: (GAS_USED[currency] * Math.max(...gasArray.filter(k => k.toString() != Math.max(...gasArray).toString()))).toString()
            });
          },
          error: (e) => {
            reject(e);
          }
        });
      })
    })

  }

}
