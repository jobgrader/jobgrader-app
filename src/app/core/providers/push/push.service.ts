import { Injectable, OnDestroy } from '@angular/core';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { filter, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { AppConfig } from '../../../app.config';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AppStateService } from '../app-state/app-state.service';
import { from, combineLatest, of, Subject, timer } from 'rxjs';
import { KycService } from '../../../kyc/services/kyc.service';
import { KycProvider } from '../../../kyc/enums/kyc-provider.enum';
import { KycState } from '../../../kyc/enums/kyc-state.enum';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { LockService } from '../../../lock-screen/lock.service';
import { KycmediaService } from '../../../core/providers/kycmedia/kycmedia.service';
import { UtilityService } from '../utillity/utility.service';
import { ToastController } from '@ionic/angular';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { DeviceService } from '../../../core/providers/device/device.service';
import { SmartAgentURI } from 'src/app/core/models/api-smart-agent.enum';
import { EventsService, EventsList } from 'src/app/core/providers/events/events.service';
import { SignProviderService } from '../sign/sign-provider.service';
import { DataSharingService, StoredRequest, PaymentRequests } from '../data-sharing/data-sharing.service';
import { Capacitor } from '@capacitor/core';
import { FirebaseMessaging, GetTokenOptions, Notification } from "@capacitor-firebase/messaging";
import { environment } from 'src/environments/environment';

interface NotificationMessage {
    id?: any;
    message?: string;
    title?: string;
    timestamp?: number;
}

@Injectable({
    providedIn: 'root',
})
export class PushService implements OnDestroy {
    public newDataReceived$ = new Subject();
    private initialized = false;
    private destroy$ = new Subject();
    public notificationMessage: NotificationMessage;
    public tag = '';
    constructor(
        private http: HttpClient,
        private platform: Platform,
        private userService: UserProviderService,
        private secureStorageService: SecureStorageService,
        private apiProviderService: ApiProviderService,
        private appStateService: AppStateService,
        private lockService: LockService,
        private kycmedia: KycmediaService,
        private toastController: ToastController,
        private translateProviderService: TranslateProviderService,
        private nativeAudio: NativeAudio,
        private _EventsService: EventsService,
        private nav: NavController,
        private deviceService: DeviceService,
        private signProviderService: SignProviderService,
        private kycService: KycService,
        private _DataSharingService: DataSharingService
    ) {
        this.platform.resume
            .subscribe(async () => {
                await this.clearAllNotifications();
            });
    }

    public async initPush() {
        if (!Capacitor.isPluginAvailable('FirebaseMessaging')) {
            return;
        }

        await this.platform.ready();

        var check = await this.secureStorageService.getValue(SecureStorageKey.firebase, false);

        if ( this.initialized && check ) {
            return;
        }

        // console.log("PushService#initPush; initialize");

        this.initialized = true;

        let permStatus = await FirebaseMessaging.checkPermissions();

        // console.log("PushService#initPush; permStatus", permStatus);

        if (permStatus.receive === 'prompt') {
            // console.log("PushService#initPush; ask for permission");
            permStatus = await FirebaseMessaging.requestPermissions();

            console.log("PushService#initPush; permStatus", permStatus);
        }


        if (permStatus.receive !== 'granted') {
            // throw new Error('User denied permissions!');
            return;
        }

        // await FirebaseMessaging.register();

        const token = await this.getToken();
        // console.log("PushService#initPush; token", token);

        // FirebaseMessaging
        //     .addListener('registration',
        //         (token: Token) => {
        //             console.log('PushService#initPush; registration token:', token);

        this.secureStorageService.setValue(SecureStorageKey.firebase, token);
        this.updatePushToken(token);
            //     }
            // );

        // FirebaseMessaging
        //     .addListener('registrationError',
        //         (error: any) => {
        //             console.warn('PushService#initPush; registrationError error:', error);
        //         }
        // );

        // Sample push notification on iOS
        // {"aps":{"alert":{"title":"Authada-Daten empfangen. // To be confirmed :ghost:","body":"Hallo TestOnboarding37, wir haben die von Authada verarbeiteten Daten erhalten. // To be confirmed :ghost:"},"category":"KYC","thread-id":"KYC"},"google.c.a.e":"1","messageType":"notification","notification_foreground":"true","gcm.message_id":"1594388502876926"}

        FirebaseMessaging
            .addListener('notificationReceived',
                (notification) => {
                    console.log('PushService#initPush; pushNotificationReceived notification:', notification);

                    this.processNotification(notification.notification);

                    // // TODO: we have to check if there is a possibility to override push notification if there is present
                    // LocalNotifications
                    //     .schedule({
                    //         notifications: [{
                    //             id: Number(8489728355 + '' + notification.data.serialNumber),
                    //             title: notification.title,
                    //             body: notification.body
                    //         }]
                    //     });
                }
            );

        FirebaseMessaging
            .addListener('notificationActionPerformed',
                (notification) => {
                    console.log('PushService#initPush; pushNotificationActionPerformed notification:', notification);
                    this.processNotification(notification.notification);
                }
            );
    }

    async processDeliveredNotifications() {
        const deliveredNotifications = await FirebaseMessaging.getDeliveredNotifications();
        const notifications = deliveredNotifications.notifications;
        for(let _ of notifications) {
            this.processNotification(_);
        }
        FirebaseMessaging.removeAllDeliveredNotifications();
    };

    public async getToken(): Promise<string> {
        const options: GetTokenOptions = {
          vapidKey: environment.firebase.vapidKey,
        };
        if (Capacitor.getPlatform() === "web") {
            try {
                options.serviceWorkerRegistration =
            await navigator.serviceWorker.register("firebase-messaging-sw.js");
            } catch(e) {
                return null;
            }
          
        }
        const { token } = await FirebaseMessaging.getToken(options);
        return token;
      }

    subscribeToTopic(topic: string) {
        FirebaseMessaging.subscribeToTopic({ topic });
    }

    unsubscribeFromTopic(topic: string) {
        FirebaseMessaging.unsubscribeFromTopic({ topic });
    }

    async processNotification(d: Notification){

        var data = d;

        // if(!this.appStateService.basicAuthToken) {
        //     // console.log('User is not logged in');
        //     return this.nav.navigateForward('/login');
        // }

        var notifications = await this.secureStorageService.getValue(SecureStorageKey.notifications, false);
        var parsedNotifications = !!notifications ? JSON.parse(notifications) : [];

        this.notificationMessage = {
            id: data.id,
            message: data.body,
            title: data.title,
            timestamp: +new Date()
        };

        var checkForDuplicacy = parsedNotifications.find(pn => pn.id == data.id);

        if(!checkForDuplicacy) {
            parsedNotifications.push(this.notificationMessage);
            await this.secureStorageService.setValue(SecureStorageKey.notifications, JSON.stringify(parsedNotifications));
        }
        
        if(!!data.tag) {
            this.tag = data.tag;
        }

        if(!!data.data) {
            if(!!data.data["category"]) {
                this.tag = data.data["category"];
            }
        }

        if( !['chat-offline','xmpp-offline','xmpp-message','data-access-request','walletconnect', 'public-web3-key-update'].includes(this.tag.toLowerCase()) ) {
            this.presentToast(this.notificationMessage.title, this.notificationMessage.message);
        }

        if (this.tag == 'blocknative-mempool-transaction') {

            console.log(data);

            if(!!(data.data as any).request) {

                try{
                    var statusData = JSON.parse((data.data as any).request);
                } catch(e) {
                    statusData = (data.data as any).request;
                }

                console.log(statusData);

                if(!!statusData.status) {
                    if(["confirmed","pending","failed","cancel","speedup","stuck"].includes(statusData.status)) {
                        if(!!statusData.to) {
                            this.notificationMessage = {
                                title: '',
                                message: this.translateProviderService.instant(`BLOCKNATIVENOTIFICATIONSTATUS.${statusData.status}.part1`) + statusData.to.slice(0,6) + "..." + statusData.to.slice(statusData.to.length - 4, statusData.to.length) + this.translateProviderService.instant(`BLOCKNATIVENOTIFICATIONSTATUS.${statusData.status}.part2`)
                            }
                            console.log(this.notificationMessage);
                        }
                    }
                }

            }

        }

        if ( this.tag == 'data-access-request' ) {
            console.log(data);
            var requestData = JSON.parse((data.data as any).data);
            if(!!requestData.dataAccessProcessId && !!requestData.username && !!requestData.publicKey && !!requestData.fields) {
                var checks = !!requestData.checks ? requestData.checks : "[]";
                var checksParse = JSON.parse(checks);
                var checksParseArray = Array.from(checksParse, k => Object.keys(k)[0]);
                if(checksParseArray.includes("CRYPTO_PAYMENT")) {
                    var py: PaymentRequests = {
                        userPublicKey:  checksParse[0]["CRYPTO_PAYMENT"].userPublicKey,
                        amount: checksParse[0]["CRYPTO_PAYMENT"].amount,
                        currency: checksParse[0]["CRYPTO_PAYMENT"].currency,
                        timestamp: checksParse[0]["CRYPTO_PAYMENT"].timestamp,
                        status: 0,
                        privateKey: null,
                        pendingProcesses: [ requestData.dataAccessProcessId ],
                        paymentRequestId: requestData.dataAccessProcessId
                    }
                    var paymentRequests = await this.secureStorageService.getValue(SecureStorageKey.cryptoPaymentRequestsReceived, false);
                    var paymentRequestsParsed = !!paymentRequests ? JSON.parse(paymentRequests) : [];
                    paymentRequestsParsed.push(py);
                    await this.secureStorageService.setValue(SecureStorageKey.cryptoPaymentRequestsReceived, JSON.stringify(paymentRequestsParsed));
                    this.nav.navigateForward(`/dashboard/payments`);
                } else {
                    var ob: StoredRequest = {
                        publicKey: requestData.publicKey,
                        username: requestData.username,
                        dataAccessProcessId: requestData.dataAccessProcessId,
                        fields: requestData.fields.split(","),
                        checks: checks,
                        timestamp: +new Date(),
                        status: 0
                    }
                    console.log("data-access-process: ob: " + JSON.stringify(ob));
                    this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(ob))}`)
                }

                // await this._DataSharingService.signRequestedData(requestData.dataAccessProcessId, requestData.username, requestData.publicKey, requestData.fields.split(","), checks);
            }
        }

        // if ( this.tag == 'tnt-sign-request' ) {

        //     if ( !!(data.data as any).prover && !!(data.data as any).credentialValues && !!(data.data as any).verifier && !!(data.data as any).revealedAttributes && !!(data.data as any).credentialSubjectRaw && !!(data.data as any).schemaDid  ) {
        //         var dataKeys = Object.keys((data.data as any))
        //         dataKeys = dataKeys.filter(k => ["prover", "credentialValues", "verifier", "revealedAttributes", "credentialSubjectRaw", "schemaDid", "attachments", "threadId"].includes(k))
        //         var cleanObject = {}
        //         var emptyArray = []
        //         dataKeys.forEach(k => cleanObject = Object.assign(cleanObject, { [k]: data[k] }))
        //         cleanObject["credentialValues"] = JSON.parse(cleanObject["credentialValues"])
        //         cleanObject["revealedAttributes"] = JSON.parse(cleanObject["revealedAttributes"])
        //         cleanObject["credentialSubjectRaw"] = JSON.parse(cleanObject["credentialSubjectRaw"])

        //         if(cleanObject["credentialSubjectRaw"]["logo"]) {
        //             var fileId = !!cleanObject["credentialSubjectRaw"]["logo"] ? this.returnFileIdFromAssetURL(cleanObject["credentialSubjectRaw"]["logo"]["url"]) : null;
        //             var token = !!cleanObject["credentialSubjectRaw"]["logo"] ? this.returnTokenFromAssetURL(cleanObject["credentialSubjectRaw"]["logo"]["url"]) : null;
        //             var mimeType = cleanObject["credentialSubjectRaw"]["logo"]["mimeType"] ?? 'image/png';
        //             if(fileId && token && mimeType) {
        //                 this.apiProviderService.getAssetFromFileIdToken(fileId, token, mimeType).then(base64 => {
        //                     cleanObject["imageBlob"] = base64;
        //                 })
        //             }
        //         }

        //         if(cleanObject["attachments"]){

        //             if(typeof(cleanObject["attachments"] == "string")){
        //                 cleanObject["attachments"] = JSON.parse(cleanObject["attachments"]);
        //             }

        //             cleanObject["attachments"].forEach(k => {
        //                 var fileId = this.returnFileIdFromAssetURL(k.uri);
        //                 var token = this.returnTokenFromAssetURL(k.uri);
        //                 var mimeType = JSON.parse(cleanObject["credentialValues"].nda).mimeType ?? 'application/pdf';
        //                 if(!mimeType) {
        //                     mimeType = 'application/pdf';
        //                 }

        //                 if(fileId && token && mimeType) {
        //                     this.apiProviderService.getAssetFromFileIdToken(fileId, token, mimeType).then(base64 => {
        //                         cleanObject["documentBlob"] = base64;
        //                     })
        //                 }
        //             })

        //         }
        //         if(cleanObject["credentialSubjectRaw"]["companyName"] == "Bioscientia Labor Mittelhessen") {
        //             cleanObject["credentialValues"]["medicalDocument"] = cleanObject["credentialValues"]["nda"];
        //             cleanObject["attachments"]["tag"] = 'medicalDocument';
        //             cleanObject["credentialSubjectRaw"]["idNumber"] = "T01000322"; // T01000322
        //             cleanObject["credentialSubjectRaw"]["testIdentification"] = "3CF75K8D0L"; // 3CF75K8D0L
        //             cleanObject["credentialSubjectRaw"]["testIssuer"] = "did:evan:helix892743iughf8"; // did:evan:helix892743iughf8
        //             cleanObject["credentialSubjectRaw"]["dateOfTest"] = "202008261430"; // 202008261430
        //             cleanObject["credentialSubjectRaw"]["expirationDate"] = "202009261429"; // 202009261429
        //             cleanObject["credentialSubjectRaw"]["testMethod"] = "PCR"; // PCR
        //             cleanObject["credentialSubjectRaw"]["testResult"] = "n"; //  (e.g. p = positive, n = negative, u = unknown):
        //             delete cleanObject["credentialValues"]["nda"];
        //             delete cleanObject["credentialSubjectRaw"]["country"];
        //             delete cleanObject["credentialSubjectRaw"]["postalCode"];
        //             delete cleanObject["credentialSubjectRaw"]["city"];
        //             delete cleanObject["credentialSubjectRaw"]["address"];
        //             delete cleanObject["credentialSubjectRaw"]["region"];
        //         }
        //         // console.log(cleanObject)
        //         emptyArray.push(cleanObject);
        //         var tntUseCase = await this.secureStorageService.getValue(SecureStorageKey.certificates, false);
        //         if(!tntUseCase) {
        //             await this.secureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(emptyArray));
        //         }
        //         else {
        //             try {
        //                 var tnt = JSON.parse(tntUseCase)
        //                 if(this.checkDuplicityOfContracts(cleanObject, tnt)){
        //                     tnt.push(cleanObject)
        //                     // console.log(tnt)
        //                     await this.secureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(tnt));
        //                 }
        //             }
        //             catch (e) {
        //                 // console.log(e)
        //                 await this.secureStorageService.setValue(SecureStorageKey.certificates, JSON.stringify(emptyArray));
        //             }
        //         }
        //         this._EventsService.publish(EventsList.showProfileTabBadge);
        //     }

        //     if(data.clickAction) {
        //         // console.log("Message was tapped")
        //         if(this.appStateService.basicAuthToken) {
        //             this.nav.navigateForward('/dashboard/personal-details')
        //         } else {
        //             this.nav.navigateForward('/login')
        //         }

        //     }
        // }

        if ( this.tag.toLowerCase().indexOf('kyc') > -1 ) {
                this._EventsService.publish(EventsList.dataReceivedByPushNotification);
                console.log("Push Service: ", "this._EventsService.publish(EventsList.dataReceivedByPushNotification);");
                Object.keys(KycProvider).forEach(async provider => {
                    if (this.kycService.getState(KycProvider[provider]) === KycState.KYC_INITIATED) {
                        this.kycService.setState(KycProvider[provider], KycState.KYC_CHECK);
                        console.log("Push Service: ", "this.kycService.setState(KycProvider[provider], KycState.KYC_CHECK);");
                        // this.kycmedia.init();
                        // console.log("Push Service: ", "this.kycmedia.init();");
                    }
                });
        }

        

    }



    fetchAssetIdFromPartialURIString(partialUri: string) {
        return new URL(SmartAgentURI.uri + partialUri).searchParams.get('assetId')
    }

    returnFileIdFromAssetURL(uri: string) {
    if(uri) {
        try {
            var u = new URL(uri);
            } catch (e) {
            var u = new URL(SmartAgentURI.uri + uri);
            }
            return uri.replace(u.origin, "").replace(u.search, "").replace("/file/","").replace("/api/v1/tnt", "");
        } else {
            return null;
        }
      }

    returnTokenFromAssetURL(uri: string) {
        if(uri) {
            try {
            var u = new URL(uri).searchParams.get('token');
            } catch (e) {
            var u = new URL(SmartAgentURI.uri + uri).searchParams.get('token');
            }
            return u;
        } else {
            return null;
        }
      }

    async fetchAssetFromPartialURIString(partialUri: string, tag: string, filetype: string) {
        var assetId = this.fetchAssetIdFromPartialURIString(partialUri)
        await this.apiProviderService.getAssetFromAssetId(assetId)
    }

    checkDuplicityOfContracts(incomingCertificate: any, existingCertificateArray: Array<any>) {
        // Removing schemaDID for checks
        // delete incomingCertificate.schemaDid
        // existingCertificateArray.forEach(k => delete k.schemaDid)
        //
        var incomingCertificateString = encodeURIComponent(JSON.stringify(incomingCertificate))
        var existingCertificateArrayString: Array<string> = []
        existingCertificateArray.forEach(k => existingCertificateArrayString.push(encodeURIComponent(JSON.stringify(k))))
        return !existingCertificateArrayString.includes(incomingCertificateString)
    }

    validURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
          '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
          '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
          '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
          '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
          '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(str);
      }

    convertToDataURLviaCanvas(url, outputFormat){
        return new Promise( (resolve, reject) => {
            let img = new Image();
        img.crossOrigin = 'Anonymous';
        img.src = url;
            img.onload = function(){
                let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
                ctx = canvas.getContext('2d'),
                dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                //callback(dataURL);
                canvas = null;
                resolve(dataURL);
            };
        });
    }

    /** @inheritDoc */
    public ngOnDestroy() {
        this.destroy$.next(0);
    }

    private updatePushToken(token: string) {
        from(this.userService.getUser())
            .pipe(
                switchMap(user => combineLatest([of(user), timer(0, 1000)
                    .pipe(
                        filter(_ => this.appStateService.basicAuthToken && this.appStateService.isAuthorized),
                        take(1)
                    )
                ])),
                switchMap(([user]) => {
                    if ( !user ) {
                        setTimeout(() => {
                            this.updatePushToken(token);
                        }, 1000);
                        console.log("At least this is getting executed!")
                        return of(0);
                    } else {
                        this.deviceService.updateFirebaseTokenInDeviceTable(token);
                    }
                }),
                takeUntil(this.destroy$),
            ).subscribe();
    }

    private async clearAllNotifications() {
        let permStatus = await FirebaseMessaging.checkPermissions();

        if (permStatus.receive === 'prompt') {
            permStatus = await FirebaseMessaging.requestPermissions();
        }

        if (permStatus.receive !== 'granted') {
            return;
        }

        // TODO mburger: check how to handle this with capacitor FirebaseMessaging
        // await this.firebase.clearAllNotifications();
        // await this.firebase.setBadgeNumber(0);
    }

    async presentToast(header: string, message: string) {
        let numberOfWords = (header + ' ' + message).split(' ').length;
        let calculatedDuration = Math.round((numberOfWords/2)*1000);
        let acceptedDuration = (calculatedDuration > 5000) ? 6250 : ((calculatedDuration < 2000) ? 2000: calculatedDuration);
        let exception = 4000;
        const toast = await this.toastController.create({
          header: header,
          message: message,
        //   duration: exception,
          duration: acceptedDuration,
          position: 'top',
        });
        await toast.present();
      }
}
