import { TestBed } from '@angular/core/testing';

import { DidVcService } from './did-vc.service';

describe('DidVcService', () => {
  let service: DidVcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DidVcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
