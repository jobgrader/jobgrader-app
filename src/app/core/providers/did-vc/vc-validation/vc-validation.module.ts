import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { TranslateModule } from '@ngx-translate/core';
import { VcValidationComponent } from './vc-validation.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        QRCodeModule,
        SharedModule,
        TranslateModule.forChild()
    ],
    providers: [],
    declarations: [VcValidationComponent],
    exports: [VcValidationComponent]
})
export class VcValidationModule {}
