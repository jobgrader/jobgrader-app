import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { Clipboard } from '@capacitor/clipboard';
import { Browser } from '@capacitor/browser';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { TranslateProviderService } from '../../translate/translate-provider.service';

interface Data {
  bool: boolean;
  value: string;
  real_name?: string;
}

@Component({
  selector: 'app-vc-validation',
  templateUrl: './vc-validation.component.html',
  styleUrls: ['./vc-validation.component.scss'],
})
export class VcValidationComponent  implements OnInit {

  @Input() vcMatchData: Data;
  @Input() issuerData: Data;
  @Input() holderData: Data;
  @Input() expiryData: Data;

  constructor(
    private _ModalController: ModalController,
    private _AlertController: AlertController,
    private _Translate: TranslateProviderService,
    private _Platform: Platform,
    private _InAppBrowser: InAppBrowser,
  ) { }

  ngOnInit() {
  }

  async openDidResolver($event, v: string) {
    const al = await this._AlertController.create({
      mode: 'ios',
      message: this._Translate.instant('VC_VALIDATION.ALERT.HEADING'),
      buttons: [
        {
          text: this._Translate.instant('VC_VALIDATION.ALERT.BUTTON1'),
          cssClass: 'alert-green',
          handler: async () => {
            await Clipboard.write({ string: v });
            if (this._Platform.is('ios') && this._Platform.is('hybrid')) {
              Browser.open({ url: "https://dev.uniresolver.io/", toolbarColor: '#54BF7B', presentationStyle: 'popover' });
            } else if (this._Platform.is('android') && this._Platform.is('hybrid')) {
              Browser.open({ url: "https://dev.uniresolver.io/", toolbarColor: '#54BF7B' });
            } else if (!this._Platform.is('hybrid')) {
              this._InAppBrowser.create("https://dev.uniresolver.io/", '_system');
            }
            
          }
        },
        {
          text: this._Translate.instant('VC_VALIDATION.ALERT.BUTTON2'),
          cssClass: 'alert-green',
          handler: async () => {
            await Clipboard.write({ string: v });
          }
        },
        {
          text: this._Translate.instant('GENERAL.cancel'),
          cssClass: 'alert-orange',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    await al.present();
  }

  close() {
    this._ModalController.dismiss();
  }

  returnDate(v: string) {
    const datePipe = new DatePipe('en-US');
    return datePipe.transform(v, "dd.MM.yyyy HH:mm:ss")
  }

  returnEmoji(v: boolean) {
    return v ? '✅' : '❌';
  }

}
