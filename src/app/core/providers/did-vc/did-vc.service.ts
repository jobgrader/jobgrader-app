import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserProviderService } from '../user/user-provider.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { UDIDNonce } from '../device/udid.enum';
import * as CryptoJS from 'crypto-js';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';
import * as didJwt from "did-jwt";
import { VcValidationComponent } from './vc-validation/vc-validation.component';

interface IssuerTableEntry {
  provider_name: string;
  real_name: string;
  did: string;
  address: string;
  clearname: string;
  class: string;
  allow: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class DidVcService {

  private target = {
    assessment: "assessment",
    car: "car"
  };

  constructor(
    private http: HttpClient,
    private user: UserProviderService,
    private loader: LoaderProviderService,
    private storage: SecureStorageService,
    private toast: ToastController,
    private _ModalController: ModalController,
    private translate: TranslateProviderService
  ) { }


  returnDidValidation(did: string): Promise<boolean> {
    return new Promise((resolve) => {
      // https://dev.uniresolver.io/1.0/identifiers/
      // https://agents.evan.network/api/smart-agents/smart-agent-did-resolver/did/get/
      this.http.get(`${environment.apiUrl}/admin/did/get?did=${did}`, {
        headers: {
          Authorization: `Basic ${environment.adminBasicAuth}`
        }
      }).subscribe({
        next: (response) => {
          console.log(response);
          resolve(true);
        },
        error: (error) => {
          console.log(error);
          resolve(false);
        }
      })
    })
    
  }

  generateCertificateVc(clearName: string, data: any, target: string): Observable<any> {
    const url = `${environment.apiUrl}/vc/generate?target=${target}`;
    return this.http.post(url, {
        clearName,
        data,
        helixSpecifiedVcClassifier: "assessment-certificate",
        validFrom: new Date(data.issueDate).toISOString(),
        validTo: new Date(data.expiryDate).toISOString(),
    })  
  }

  generateGenericDid(clearName: string, target: string): Observable<any>{
    const url = `${environment.apiUrl}/admin/did/generate?target=${target}`;
    return this.http.post(url, { clearName, devices: [] }, {
        headers: {
            Authorization: `Basic ${environment.adminBasicAuth}`
        }
    })
  }

  convertGenericDid(clearName: string, target: string): Observable<any>{
      const url = `${environment.apiUrl}/admin/did/convert?target=${target}&clearName=${clearName}`;
      return this.http.get(url, {
          headers: {
              Authorization: `Basic ${environment.adminBasicAuth}`
          }
      })
  }

  generateGenericVc(clearName:string, data: any, target: string): Observable<any> {
      const url = `${environment.apiUrl}/admin/vc/generate?target=${target}`;
      
      console.log("Car Identity Iteration: " + JSON.stringify(data));

      try {
        var validFrom = new Date(data.creation_date).toISOString()
      } catch(e) {
        validFrom = new Date().toISOString();
      }

      try {
        var validTo = new Date(data.expiration_date).toISOString()
      } catch(e) {
        var now = new Date();
        validTo = new Date(now.setFullYear(now.getFullYear() + 1)).toISOString();
      }
      
      return this.http.post(url, {
          clearName,
          data,
          helixSpecifiedVcClassifier: "car",
          validFrom,
          validTo,
      }, {
          headers: {
              Authorization: `Basic ${environment.adminBasicAuth}`
          }
      })
  }

  async generateAssessmentCertificate(request: any, qrCode: string): Promise<string> {
    return new Promise(async (resolve) => {
      const clearName = `${await this.user.getUsername()}_quiz_1@helixid_evan`;

      const generateVc = (certificateDid: any): Promise<string> => {
        return new Promise(async (resolve) => {

          await this.loader.loaderCreate(this.translate.instant('ASSESSMENT_CERTIFICATE_DID'));

          this.generateCertificateVc(clearName, request, this.target.assessment).subscribe({
            next: async (vc) => {
      
              const acs = await this.storage.getValue(SecureStorageKey.assessmentCertificate, false);
              var ac: Array<any> = !!acs ? JSON.parse(acs) : [];

              if(!qrCode) {
                const secret = UDIDNonce.energy;
                const dataString = JSON.stringify(request);
                
                const encoded = encodeURIComponent(dataString);
                const encrypted = CryptoJS.AES.encrypt(encoded, secret).toString();
      
                qrCode = encodeURIComponent(encrypted);
              }
      
              var check = ac.find(ec => ec.qrCode == qrCode);
      
              if(check) {
                  this.presentToast(this.translate.instant('ASSESSMENT_CERTIFICATE_IMPORTED'));
                  await this.loader.loaderDismiss();
                  resolve(null);
              } else {
                var ob = {
                  qrCode,
                  didDocument: certificateDid,
                  vc,
                  credentialValues:{
                      approved:"true",
                      timestamp: +new Date(),
                      assessmentCertificate:"[]"
                  },
                  credentialSubjectRaw:{
                      id: request.id,
                      badgeUrl: request.badgeUrl,
                      imageUrl: request.imageUrl,
                      name: request.name,
                      description: request.description,
                      certificateUrl: request.certificateUrl,
                      issuedBy: request.issuedBy,
                      skills: request.skills,
                      issueDate: request.issueDate,
                      expiryDate: request.expiryDate,
                      permissions: request.permissions,
                      did: certificateDid.id,
                      vcId: vc.id
                  }
                };
                
                ac.push(ob);
                
                await this.storage.setValue(SecureStorageKey.assessmentCertificate, JSON.stringify(ac));
                await this.loader.loaderDismiss();
                resolve(vc.id);
              }
          
            }
          })
        })
        
      } 
      
      this.convertGenericDid(clearName, this.target.assessment).subscribe({
        next: async (certificateDid) => {
          console.info("certificateDid: ", certificateDid);
  
          if(!certificateDid || certificateDid == "") {
              this.generateGenericDid(clearName, this.target.assessment).subscribe({
                next: (did) => {
                  generateVc(did).then(r => {
                    resolve(r);
                  });
                }
              })
          } else {
            generateVc(certificateDid).then(r => {
              resolve(r);
            });
          }
        }
      })
      
    })
  }  

  async generateCarVC(request: any, qrCode: string): Promise<boolean> {
    return new Promise((resolve) => {
      const clearName = `${request.vin}_car@helixid_evan`;

      const generateVc = (certificateDid: any): Promise<boolean> => {
        return new Promise(async (resolve) => {

          await this.loader.loaderCreate(this.translate.instant('CAR_IDENTITY_DID'));

          this.generateGenericVc(clearName, request, this.target.car).subscribe({
            next: async (vc) => {

              const acs = await this.storage.getValue(SecureStorageKey.carIdentities, false);
              var ac: Array<any> = !!acs ? JSON.parse(acs) : [];

              if(!qrCode) {
                const secret = UDIDNonce.energy;
                const dataString = JSON.stringify(request);
                
                const encoded = encodeURIComponent(dataString);
                const encrypted = CryptoJS.AES.encrypt(encoded, secret).toString();
                qrCode = encodeURIComponent(encrypted);
              }
      
              var check = ac.find(ec => ec.qrCode == qrCode);
      
              if(check) {
                  this.presentToast(this.translate.instant('CARIDENTITIES.imported'));
                  await this.loader.loaderDismiss();
                  resolve(false);
              } else {
                var ob = {
                  qrCode,
                  didDocument: certificateDid,
                  vc,
                  credentialValues:{
                      approved:"true",
                      timestamp: +new Date(),
                      car:"[]"
                  },
                  credentialSubjectRaw:{
                    logo: request.logo,
                    manufacturer: request.manufacturer,
                    model: request.model,
                    creation_date: request.creation_date,
                    expiration_date: request.expiration_date,
                    vin: request.vin,
                    insurance: request.insurance,
                    insurance_id: request.insurance_id,
                    registration_plate: request.registration_plate,
                    did: certificateDid.id,
                    vcId: vc.id,
                    public_key: certificateDid.publicKey[0].ethereumAddress,
                    qr_code_type: request.qr_code_type
                  }
                };
                  
                ac.push(ob);
                
                await this.storage.setValue(SecureStorageKey.carIdentities, JSON.stringify(ac));
                await this.loader.loaderDismiss();
                resolve(true);
              }
          
            }
          })
        })
        
      } 
      
      this.convertGenericDid(clearName, this.target.car).subscribe({
        next: async (certificateDid) => {
          console.info("certificateDid: ", certificateDid);
  
          if(!certificateDid || certificateDid == "") {
              this.generateGenericDid(clearName, this.target.car).subscribe({
                next: (did) => {
                  generateVc(did).then(r => {
                    resolve(r);
                  });
                }
              })
          } else {
            generateVc(certificateDid).then(r => {
              resolve(r);
            });
          }
        }
      })
    })
    
  }  

  async offlineValidateVc(jsonParse: any) {
    const decodedJwt = didJwt.decodeJWT(jsonParse.proof.jws);
    const decodedVc = decodedJwt.payload.vc;

    await this.loader.loaderCreate();

    this.http.get(`${environment.apiUrl}/issuerlist`).subscribe({
      next: async (information: Array<IssuerTableEntry>) => {

        console.log(information);

        const issuerEntry = information.find(ii => ii.did == jsonParse.issuer.id);

        const modal = await this._ModalController.create({
          component: VcValidationComponent,
          componentProps: {
              vcMatchData: {
                bool: (jsonParse.id == decodedVc.id),
                value: jsonParse.id
              },
              issuerData: {
                bool: ((jsonParse.issuer.id == decodedVc.issuer.id) && await this.returnDidValidation(decodedVc.issuer.id) && !!issuerEntry.allow),
                value: jsonParse.issuer.id,
                real_name: !!issuerEntry ? issuerEntry.real_name : null
              },
              holderData: {
                bool: ((jsonParse.credentialSubject.id == decodedVc.credentialSubject.id) && await this.returnDidValidation(decodedVc.credentialSubject.id)),
                value: jsonParse.credentialSubject.id
              },
              expiryData: {
                bool: (((new Date(jsonParse.validFrom) == new Date(decodedVc.validFrom)) && (new Date(jsonParse.validFrom) > new Date())) ),
                value: jsonParse.validFrom
              }
            },
        });
    
        await this.loader.loaderDismiss();
    
        await  modal.present();
      },
      error: async (error) => {
        await this.loader.loaderDismiss();
      }
    })
    
  }

  validateVc(vc: any) {
    return this.http.post(`${environment.apiUrl}/validate/vc`, vc);
  }

  validateVp(vp: any) {
    return this.http.post(`${environment.apiUrl}/validate/vp`, vp);
  }

  presentToast(message: string) {
    this.toast.create({
      message,
      duration: 3000,
      position: 'top',
    }).then(t => {
      t.present();
    })
  }


}
