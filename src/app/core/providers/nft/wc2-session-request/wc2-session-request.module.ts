import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Wc2SessionRequestComponent } from './wc2-session-request.component';
import { DidPopoverModule } from 'src/app/merchant-details/did-popover/did-popover.module';

const routes: Routes = [
  {
    path: '',
    component: Wc2SessionRequestComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    DidPopoverModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  declarations: [Wc2SessionRequestComponent]
})
export class Wc2SessionRequestModule {}
