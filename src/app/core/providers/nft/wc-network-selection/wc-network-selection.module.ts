import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { WcNetworkSelectionComponent } from './wc-network-selection.component';

const routes: Routes = [
  {
    path: '',
    component: WcNetworkSelectionComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ],
  declarations: [WcNetworkSelectionComponent]
})
export class WcNetworkSelectionModule {}
