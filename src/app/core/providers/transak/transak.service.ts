import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { HttpClient } from '@angular/common/http';


const webhookPublicKey = "1d9ffac87de599c61283";

export enum UserStatus {
  ACTIVE = "ACTIVE",
  INACTIVE = "INACTIVE",
  DISABLED = "DISABLED"
}

export enum KycStatus {
  NOT_SUBMITTED = "NOT_SUBMITTED",
  SUBMITTED = "SUBMITTED",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED"
}

export enum OrderStatus {
  AWAITING_PAYMENT_FROM_USER = "AWAITING_PAYMENT_FROM_USER",
  PAYMENT_DONE_MARKED_BY_USER = "PAYMENT_DONE_MARKED_BY_USER",
  PROCESSING = "PROCESSING",
  PENDING_DELIVERY_FROM_TRANSAK = "PENDING_DELIVERY_FROM_TRANSAK",
  ON_HOLD_PENDING_DELIVERY_FROM_TRANSAK = "ON_HOLD_PENDING_DELIVERY_FROM_TRANSAK",
  COMPLETED = "COMPLETED",
  CANCELLED = "CANCELLED",
  FAILED = "FAILED",
  REFUNDED = "REFUNDED",
  EXPIRED = "EXPIRED"
}

export enum PaymentMethod {
  credit_debit_card = "credit_debit_card",
  sepa_bank_transfer = "sepa_bank_transfer",
  gbp_bank_transfer = "gbp_bank_transfer",
  neft_bank_transfer = "neft_bank_transfer",
  upi = "upi"
}

export enum JSEvents {
  TRANSAK_WIDGET_CLOSE = "TRANSAK_WIDGET_CLOSE",
  TRANSAK_WIDGET_INITIALISED = "TRANSAK_WIDGET_INITIALISED",
  TRANSAK_WIDGET_OPEN = "TRANSAK_WIDGET_OPEN",
  TRANSAK_ORDER_CREATED = "TRANSAK_ORDER_CREATED",
  TRANSAK_ORDER_CANCELLED = "TRANSAK_ORDER_CANCELLED",
  TRANSAK_ORDER_FAILED = "TRANSAK_ORDER_FAILED",
  TRANSAK_ORDER_SUCCESSFUL = "TRANSAK_ORDER_SUCCESSFUL"
}

export enum WebhookMethods {
  ORDER_PROCESSING = "ORDER_PROCESSING",
  ORDER_FAILED = "ORDER_FAILED",
  ORDER_EXPIRED = "ORDER_EXPIRED",
  ORDER_CANCELLED = "ORDER_CANCELLED",
  ORDER_COMPLETED = "ORDER_COMPLETED",
  ORDER_PAYMENT_VERIFYING = "ORDER_PAYMENT_VERIFYING",
  ORDER_CREATED = "ORDER_CREATED"
}

@Injectable({
  providedIn: 'root'
})

export class TransakService {

  constructor(
    private _HttpClient: HttpClient,
    private _Translate: TranslateProviderService,
    private _Vault: VaultService
  ) { }

  private getApiURL() {
    return (environment.production ? 'https://api.transak.com' : 'https://staging-api.transak.com');
  }

  private getIpAddress(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.get(`https://api.ipify.org?format=json`, {
        headers: {

        }
      }).subscribe({
        next: (response) => {
          resolve(response);
        },
        error: (e) => {
          reject(e);
        }
      });
    })
  }

  public async openModalWindow(address: string, cryptoCurrencyList: string, defaultCryptoCurrency: string) {
    // https://staging-global.transak.com/?apiKey=[insert your staging API key here]&redirectURL=https://transak.com&cryptoCurrencyList=ETH,DAI,USDT&defaultCryptoCurrency=DAI&walletAddress=0x2dd94DC4b658F08E33272e6563dAb1758c10b1de&disableWalletAddressForm=true&exchangeScreenTitle=My%20dApp%20is%20the%20best&isFeeCalculationHidden=true
    var message = "Jobgrader";
    var transacAPIKey = await this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_KEY);
    var lang = await this._Translate.getLangFromStorage();
    var redirectUrl = (lang == 'de') ? 'https://jobgrader.app/app-transaktion-success' : 'https://jobgrader.app/app-transaction-success';

    const base = (environment.production ? 'https://global.transak.com/' : 'https://staging-global.transak.com/');
    const url = `${base}?apiKey=${transacAPIKey}&cryptoCurrencyList=${cryptoCurrencyList}&defaultCryptoCurrency=${defaultCryptoCurrency}&walletAddress=${address}&disableWalletAddressForm=false&exchangeScreenTitle=${encodeURIComponent(message)}&isFeeCalculationHidden=false&redirectURL=${encodeURIComponent(redirectUrl)}`;
    return url;
  }

  public getCryptoCurrencies(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getIpAddress().then(ip => {
        this._HttpClient.get(`${this.getApiURL()}/api/v2/currencies/crypto-currencies?ipAddress=${ip.ip}`, {
          headers: {

          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        });
      })
    })
  }

  public getFiatCurrencies(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.get(`${this.getApiURL()}/api/v2/currencies/fiat-currencies`, {
        headers: {

        }
      }).subscribe({
        next: (response) => {
          resolve(response);
        },
        error: (e) => {
          reject(e);
        }
      });
    })
  }

  public getPrice(fiatCurrency: string, cryptoCurrency: string, fiatAmount: string, network: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.get(`${this.getApiURL()}/api/v2/currencies/price?fiatCurrency=${fiatCurrency}&cryptoCurrency=${cryptoCurrency}&isBuyOrSell=BUY&paymentMethod=gbp_bank_transfer&fiatAmount=${fiatAmount}&network=${network}`,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e)
          }
        });
      })
    })
  }

  public verifyCryptoWalletAddress(walletAddress: string, cryptoCurrency: string, network: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.get(`${this.getApiURL()}/api/v2/currencies/verify-wallet-address?walletAddress=${walletAddress}&cryptoCurrency=${cryptoCurrency}&network=${network}`, {
        headers: {

        }
      }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        });
    })
  }

  public getCountries(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.get(`${this.getApiURL()}/api/v2/countries`, {
        headers: {

        }
      }).subscribe({
        next: (response) => {
          resolve(response);
        },
        error: (e) => {
          reject(e);
        }
      });
    })
  }

  public getAllPartnerOrders(limit: string, startDate?: string, endDate?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const url = (!!startDate && !!endDate) ? `${this.getApiURL()}/partners/api/v1/orders?limit=${limit}&startDate=${startDate}&endDate=${endDate}` : `${this.getApiURL()}/partners/api/v1/orders?limit=${limit}`;
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.get(url,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        });
      })
    })
  }

  public getPartnerDetailsByAPIKey(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_KEY).then(transacAPIKey => {
        this._HttpClient.get(`${this.getApiURL()}/partners/api/v1/${transacAPIKey}`, {
          headers: {

          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })
  }

  public getPartnerOrderByOrderId(customerOrderId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.get(`${this.getApiURL()}/partners/api/v1/order/${customerOrderId}`,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })
  }

  public getWebhooks(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.get(`${this.getApiURL()}/partners/api/v1/webhooks`,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response)
          },
          error: (e) => {
            reject(e);
          }
        });
      })
    })
  }

  public testWebhook(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.post(`${this.getApiURL()}/partners/api/v1/test-webhook`,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        });
      })
    })
  }

  public getUserData(partnerCustomerId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.get(`${this.getApiURL()}/partners/api/v1/get-user-data?partnerCustomerId=${partnerCustomerId}`,{
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })
  }

  public updateWebhookUrl(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.TRANSAC_API_SECRET).then(transacAPISecret => {
        this._HttpClient.post(`${this.getApiURL()}/partners/api/v1/update-webhook-url`, {
          headers: {
            "api-secret": transacAPISecret
          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })
  }

}
