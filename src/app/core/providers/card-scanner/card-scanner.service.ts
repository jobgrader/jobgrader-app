import { Injectable } from '@angular/core';
// import { CardIO } from '@ionic-native/card-io/ngx';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
@Injectable({
  providedIn: 'root'
})
export class CardScannerService {

  constructor(
    // private _CardIO: CardIO,
    private _SecureStorageService: SecureStorageService,
    private _CryptoProviderService: CryptoProviderService
  ) { }

  public cardInformations = [
    {
      cardType: "MasterCard",
      expiryMonth: 2,
      expiryYear: 2026,
      cardNumber: "1283719827391823",
      cardName: 'N26',
      cardholderName: 'Hellen Weller'
    },
    {
      cardType: "Visa",
      expiryMonth: 3,
      expiryYear: 2025,
      cardNumber: "1723981728937189",
      cardName: 'DeutscheBank',
      cardholderName: 'Monica Geller'
    },
    {
      cardType: "Maestro",
      expiryMonth: 4,
      expiryYear: 2024,
      cardNumber: "8127398123123123",
      cardName: 'HDFC India',
      cardholderName: 'Chandler Bing'
    }
  ]

  public setCardInformations() {
    this._SecureStorageService.getValue(SecureStorageKey.cardInformations, false).then((h) => {
      this._CryptoProviderService.returnUserSymmetricKey().then(key => {
        if(h) {
          this._CryptoProviderService.symmetricDecrypt(h, key).then(hh => {
            this.cardInformations = !!hh ? JSON.parse(hh) : this.cardInformations;
          })
        }
      }).catch(e => {
        
      })
    }).catch(e => {
      
    });
  }

  public storeCardInformation(info: any) {
    this.cardInformations.push(info);
    var store = JSON.stringify(this.cardInformations);
    this._CryptoProviderService.returnUserSymmetricKey().then(key => {
      this._CryptoProviderService.symmetricEncrypt(store, key).then(encrypted => {
        this._SecureStorageService.setValue(SecureStorageKey.cardInformations, encrypted);
      })
    })
  }

  public init(): Promise<any> {
    return new Promise((resolve, reject) => {
      // this._CardIO.canScan().then((res: boolean) => {
      //   if(res) {
      //     let options = {
      //       requireExpiry: true,
      //       requireCVV: false,
      //       requirePostalCode: false,
      //       keepApplicationTheme: true,
      //       requireCardholderName: true,
      //       scanExpiry: true,
      //       hideCardIOLogo: true
      //     };
      //     this._CardIO.scan(options).then((values) => {
      //       // console.log(values);
      //       resolve(values);
      //     }).catch((e) => {
      //       console.log(e);
      //       reject(e);
      //     });
      //   } else {
      //     reject();
      //   }
      // }).catch(e => {
      //   console.log(e);
      //   reject(e);
      // }) 
      resolve({});
    })
  }
}

// {
//   cardType: "MasterCard",
//   expiryMonth: 5,
//   expiryYear: 2024,
//   cardNumber: "1283719827391823"
// }