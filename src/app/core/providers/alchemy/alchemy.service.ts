import { Injectable } from '@angular/core';
import { Network, Alchemy, AlchemySettings, OwnedNftsResponse, OwnedNft } from "alchemy-sdk";
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AlchemyService {

  private networks: Array<Network> = [
    Network.ETH_MAINNET,
    Network.MATIC_MAINNET,
    Network.OPT_MAINNET,
    Network.ARB_MAINNET,
    // Network.ASTAR_MAINNET,
    // Network.POLYGONZKEVM_MAINNET
  ]

  constructor() { }

  async fetchNfts(ownerAddr: string) {
    const promises = [];
    for(let i=0; i<this.networks.length; i++) {
      const settings: AlchemySettings = {
        apiKey: environment.alchemy.api_key,
        network: this.networks[i]
      };
      const alchemy = new Alchemy(settings);
      promises.push(
        new Promise(async resolve => {
          const nftsForOwner = await alchemy.nft.getNftsForOwner(ownerAddr)
          resolve(nftsForOwner.ownedNfts);
        })
      )
    }

    const nfts: Array<Array<OwnedNft>> = await Promise.all(promises);

    return nfts.flat();
  }

}
