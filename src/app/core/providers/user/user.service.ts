import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { UserCredential } from "@angular/fire/auth";
import { Observable } from "rxjs";


// custom imports
import { environment } from "src/environments/environment";
import { generateBlueTokens, symmetricDecrypt } from "@utilities/crypto";
import { FirebaseAuthService } from "@services/firebase-auth";
import { UserKeyResponse } from "@services/user";


@Injectable({
    providedIn: 'root'
})
export class UserService {

    private http = inject(HttpClient);
    private firebaseAuthService = inject(FirebaseAuthService);

    constructor() {}

    loginUsernameAndPassword(username: string, password: string): Promise<UserCredential | any> {
        return new Promise(
            (resolve, reject) => {
                const body = { username, password };
                // const blueToken = await this.generateBlueTokens();
                // const encryptedBody = this.symmetricEncrypt(JSON.stringify(body), blueToken.key);

                this.http.post(`${environment.apiUrl}/api/v1/user/login`, body,
                    {
                        headers: {
                            // "x-api-key": blueToken.encrypted,
                            "Content-Type": "application/json"
                        }
                    })
                    .subscribe({
                        next: async (value: any) => {
                            const user: UserCredential = await this.firebaseAuthService.signInWithCustomToken(value.token);
                            resolve(user);
                        },
                        error: (err) => {
                            // IMPORTANT: for now we return an emypt user object instead rejecting the call
                            // reject(err);
                            resolve(null);
                        }
                    });
            }
        );
    }

    login(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.get(`${environment.apiUrl}/user`, {
                headers: {},
                responseType: 'text'
            })
            .subscribe({
                next: (data => {
                    resolve(JSON.parse(data));
                }),
                error: ((err: HttpErrorResponse) => {
                    // console.error(err);
                    reject(err);
                })
            });
        })
    }

    /**
     * Get basic data for user
     *
     * @param userId CouchDB Id of user
     */
    public getbasicdata(userId: string): Observable<any> {
        return this.http.get<any>(`${environment.apiUrl}/user/basicdata/getbyuserid/${encodeURIComponent(String(userId))}`,
            {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                }
            }
        );
    }

    public getDeviceRegistrationStatus(deviceId: string, userId?: string, kek?: string , otp?: string, keyName?: string): Promise<UserKeyResponse> {
        var ob = {};
        if(!!userId) { Object.assign(ob, { identifier: userId }) }
        if(!!kek) { Object.assign(ob, { kek: kek }) }
        if(!!otp) { Object.assign(ob, { otp: otp }) }
        if(!!keyName) { Object.assign(ob, { keyName: keyName }) }
        return new Promise(async(resolve, reject) => {
            this.http.put(`${ environment.apiUrl }/api/v2/user/${deviceId}/userkey`, ob , {})
                    .subscribe({
                        next: (data) => resolve(data),
                        error: (err) => reject(err)
                    })
        })
    }
}