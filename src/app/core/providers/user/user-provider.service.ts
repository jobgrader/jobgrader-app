import { Injectable } from '@angular/core';
import moment from 'moment';
import { User } from '../../models/User';
import { Trust } from '../../models/Trust';
import { AppConfig } from '../../../app.config';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { UserPhotoServiceAkita } from '../state/user-photo/user-photo.service';
// import { SettingsService, Settings } from '../settings/settings.service';

@Injectable({
  providedIn: 'root'
})
export class UserProviderService {

  private readonly appConfig: any = AppConfig;
  photoExists = false;
  constructor(private secureStorage: SecureStorageService,
    // private _SettingsService: SettingsService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) { }

  async getUserNonParsed(): Promise<any> {
    const userResponse = await this.secureStorage.getValue(SecureStorageKey.userData, false);
    return userResponse;
  }

  async getUser(): Promise<User> {
    const userResponse = await this.secureStorage.getValue(SecureStorageKey.userData, false);
    return userResponse ? (await this.parseUserResponse(userResponse)) : null;
  }

  async getUserPhoto(): Promise<string> {
    const userPhotoResponse = this.userPhotoServiceAkita.getPhoto();
    return userPhotoResponse;
  }

  async getUserTrustData(): Promise<Trust> {
    const userTrustData = await this.secureStorage.getValue(SecureStorageKey.userTrustData, false);
    return JSON.parse(userTrustData);
  }

  async storeUsername(userName: string) {
    await this.secureStorage.setValue(SecureStorageKey.userName, userName);
  }

  async getUsername(): Promise<string> {
    const userName = await this.secureStorage.getValue(SecureStorageKey.userName, false);
    return userName;
  }

  async getDeviceInfo(): Promise<string> {
    const deviceInfo = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
    return deviceInfo;
  }

  async getApprovedPhoneNumber(): Promise<string> {
    const phone = await this.secureStorage.getValue(SecureStorageKey.approvedPhoneNumber, false);
    return phone;
  }

  public getUserFullName(user: User): string {
    let userFullName = `${user.firstname || ''} ${user.middlename || ''} ${user.lastname || ''}`;
    userFullName = userFullName.trim();
    userFullName = userFullName.replace(/  +/g, ' ');
    return userFullName;
  }

  userAgeOver18(date: number): boolean {
    //
    // Some info
    // moment(date).format('YYYY-MM-DD HH:mm:ss') to format the date
    //
    const age = moment.duration((moment().diff(moment(date)))).years();

    return age > 18;
  }

  private async parseUserResponse(userResponse: any): Promise<User> {
    // const { userid, revision, id, identityid } = userResponse;
    // let { data } = userResponse;
    // data = data || {};
    // const emailFromStorage = await this.getUserEmail();
    userResponse = userResponse ? JSON.parse(userResponse) : {};
    // const phoneFromStorage = await this.getApprovedPhoneNumber() || '';
    const photo = await this.getUserPhoto();
    const trustData: Trust = await this.getUserTrustData();
    const parsedPhoto = !!photo ? `${this.appConfig.apiUrl}${photo}` : '../../../../assets/job-user.svg';
    this.photoExists = !!photo;

    // console.log(userResponse);

    const user = Object.assign({}, {
      id: userResponse.id || '',
      userid: userResponse.userid || '',
      confirmed: userResponse.confirmed || '',
      lastModified: userResponse.lastModified || '',
      callname: userResponse.callname || '',
      gender: userResponse.gender || '',
      title: (!!userResponse.title ? ((userResponse.title === 'Dr.eh.Dr.') ? 'Dr.' : userResponse.title) : ''),
      firstname: userResponse.firstname || '',
      middlename: userResponse.middlename || '',
      lastname: userResponse.lastname || '',
      email: userResponse.email || '',
      photo: parsedPhoto,
      emailtype: userResponse.emailtype || '',
      maritalstatus: userResponse.maritalstatus || '',
      dateofbirth: userResponse.dateofbirth || null,
      cityofbirth: userResponse.cityofbirth || '',
      countryofbirth: userResponse.countryofbirth || '',
      citizenship: userResponse.citizenship || '',
      street: userResponse.street || '',
      city: userResponse.city || '',
      zip: userResponse.zip || '',
      state: userResponse.state || '',
      country: userResponse.country || '',
      identificationdocumenttype: userResponse.identificationdocumenttype || '',
      identificationdocumentnumber: userResponse.identificationdocumentnumber || '',
      identificationissuecountry: userResponse.identificationissuecountry || '',
      identificationissuedate: userResponse.identificationissuedate || null,
      identificationexpirydate: userResponse.identificationexpirydate || null,
      driverlicencedocumentnumber: userResponse.driverlicencedocumentnumber || '',
      driverlicencecountry: userResponse.driverlicencecountry || '',
      driverlicenceissuedate: userResponse.driverlicenceissuedate || null,
      driverlicenceexpirydate: userResponse.driverlicenceexpirydate || null,
      passportnumber: userResponse.passportnumber || '',
      passportissuecountry: userResponse.passportissuecountry || '',
      passportissuedate: userResponse.passportissuedate || null,
      passportexpirydate: userResponse.passportexpirydate || null,
      residencepermitnumber: userResponse.residencepermitnumber || '',
      residencepermitissuecountry: userResponse.residencepermitissuecountry || '',
      residencepermitissuedate: userResponse.residencepermitissuedate || null,
      residencepermitexpirydate: userResponse.residencepermitexpirydate || null,
      taxresidency: userResponse.taxresidency || '',
      taxnumber: userResponse.taxnumber || '',
      termsofuse: userResponse.termsofuse || '',
      termsofusethirdparties: userResponse.termsofusethirdparties || '',
      maidenname: userResponse.maidenname || '',
      identityId: userResponse.identityId || '',
      emailStatus: userResponse.email ? 1 : 0,
      phone: userResponse.phone || '',
      // phoneStatus: ((userResponse.phone == '') || (userResponse.phone == null)) ? 0 : 1,
    }, this.mapTrustData(trustData));
    return user;
  }

  /** Maps the trust data to Partail of the User */
  public mapTrustData(trust: Trust): Partial<User> {
    const setTrustData = ((trustData: Trust) => (key: keyof Trust) => {
      return !!trustData[key] ? (trustData[key].trustActive ? trustData[key].trustValue : 0 ) : 0;
    })(trust);
    return {
      emailStatus: 1,
      genderStatus: setTrustData('gender'),
      maritalstatusStatus: setTrustData('maritalstatus'),
      firstnameStatus: setTrustData('firstname'),
      lastnameStatus: setTrustData('lastname'),
      dateofbirthStatus: setTrustData('dateofbirth'),
      countryofbirthStatus: setTrustData('countryofbirth'),
      citizenshipStatus: setTrustData('citizenship'),
      streetStatus: setTrustData('street'),
      cityStatus: setTrustData('city'),
      zipStatus: setTrustData('zip'),
      stateStatus: setTrustData('state'),
      cityofbirthStatus: setTrustData('cityofbirth'),
      countryStatus: setTrustData('country'),
      maidennameStatus: setTrustData('maidenname'),
      middlenameStatus: setTrustData('middlename'),
      identificationdocumentnumberStatus: setTrustData('identificationdocumentnumber'),
      identificationissuecountryStatus: setTrustData('identificationissuecountry'),
      identificationexpirydateStatus: setTrustData('identificationexpirydate'),
      identificationissuedateStatus: setTrustData('identificationissuedate'),
      driverlicencedocumentnumberStatus: setTrustData('driverlicencedocumentnumber'),
      driverlicencecountryStatus: setTrustData('driverlicencecountry'),
      driverlicenceexpirydateStatus: setTrustData('driverlicenceexpirydate'),
      driverlicenceissuedateStatus: setTrustData('driverlicenceissuedate'),
      passportnumberStatus: setTrustData('passportnumber'),
      passportissuecountryStatus: setTrustData('passportissuecountry'),
      passportexpirydateStatus: setTrustData('passportexpirydate'),
      passportissuedateStatus: setTrustData('passportissuedate'),
      residencepermitnumberStatus: setTrustData('residencepermitnumber'),
      residencepermitissuecountryStatus: setTrustData('residencepermitissuecountry'),
      residencepermitexpirydateStatus: setTrustData('residencepermitexpirydate'),
      residencepermitissuedateStatus: setTrustData('residencepermitissuedate'),
      phoneStatus: setTrustData('phone'),
    };
  }
}


// if(user.identificationdocumentnumber !== '' || user.identificationissuecountry !== '' || user.identificationissuedate !== null || user.identificationexpirydate !== null) {
//   await this._SettingsService.set(Settings.idCardSet, true.toString());
// }

// if(user.driverlicencedocumentnumber !== '' || user.driverlicencecountry !== '' || user.driverlicenceissuedate !== null || user.driverlicenceexpirydate !== null) {
//   await this._SettingsService.set(Settings.driverLicenseSet, true.toString());
// }

// if(user.residencepermitnumber !== '' || user.residencepermitissuecountry !== '' || user.residencepermitissuedate !== null || user.residencepermitexpirydate !== null) {
//   await this._SettingsService.set(Settings.residencePermitSet, true.toString());
// }

// if(user.passportnumber !== '' || user.passportissuecountry !== '' || user.passportissuedate !== null || user.passportexpirydate !== null) {
//   await this._SettingsService.set(Settings.passportSet, true.toString());
// }

// if(user.identificationdocumentnumber !== '' || user.identificationissuecountry !== '' || user.identificationissuedate !== null || user.identificationexpirydate !== null ||
//   user.driverlicencedocumentnumber !== '' || user.driverlicencecountry !== '' || user.driverlicenceissuedate !== null || user.driverlicenceexpirydate !== null ||
//   user.residencepermitnumber !== '' || user.residencepermitissuecountry !== '' || user.residencepermitissuedate !== null || user.residencepermitexpirydate !== null ||
//   user.passportnumber !== '' || user.passportissuecountry !== '' || user.passportissuedate !== null || user.passportexpirydate !== null ||
//   user.citizenship !== '' || user.city !== '' || user.cityofbirth !== '' || user.country !== '' || user.countryofbirth !== '' || user.dateofbirth !== null
// ) {
//   await this._SettingsService.set(Settings.wizardNotRun, false.toString())
// }