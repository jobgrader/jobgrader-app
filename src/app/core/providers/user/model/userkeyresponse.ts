export interface UserKeyResponse {
    keys?: {
        backupKey?: string;
        vcKey?: string;
        userKey?: string;
    }
    errors?: [any];
    errorFound?: boolean;
    backupKey?: string;
    vcKey?: string;
    userKey?: string;
}