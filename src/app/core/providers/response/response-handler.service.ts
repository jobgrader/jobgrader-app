import { Injectable } from '@angular/core';
import { ResponseCode } from '../../models/ResponseCode';
import { TranslateProviderService} from '../translate/translate-provider.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseHandlerService {

  constructor(
    private translateProviderService: TranslateProviderService
  ) { }

  public getResponseTranslation(error: any): string {
    let responseText = '';
    for(let i = 0; i < error.error.errors.length; i++)
    {
    let errorKey = Object.keys(ResponseCode).find(key => ResponseCode[key] == error.error.errors[i].errorCode );
    responseText = responseText.concat(this.translateProviderService.instant('RESPONSECODE.' + errorKey)+' ');
    }
    return responseText;
  }

}
