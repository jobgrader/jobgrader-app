import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable, firstValueFrom, timer } from 'rxjs';
import { filter, flatMap, take } from 'rxjs/operators';
import { size } from 'lodash-es';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeEn from '@angular/common/locales/en';
import localeBn from '@angular/common/locales/bn';
import localeRu from '@angular/common/locales/ru';
import localeTr from '@angular/common/locales/tr';
import localeHi from '@angular/common/locales/hi';
import localeFr from '@angular/common/locales/fr';
import localeZh from '@angular/common/locales/zh';
import localeOr from '@angular/common/locales/or';
import localeKn from '@angular/common/locales/kn';
import localeHe from '@angular/common/locales/he';
import localeEl from '@angular/common/locales/el';
import localeKo from '@angular/common/locales/ko';
import localePa from '@angular/common/locales/pa';
import localeUr from '@angular/common/locales/ur';
import localeTa from '@angular/common/locales/ta';
import localeTh from '@angular/common/locales/th';
import localeTe from '@angular/common/locales/te';
import localeVi from '@angular/common/locales/vi';
import localeGu from '@angular/common/locales/gu';
import localeId from '@angular/common/locales/id';
import localeMl from '@angular/common/locales/ml';
import localeMs from '@angular/common/locales/ms';
import localeSr from '@angular/common/locales/sr';
import localeSa from '@angular/common/locales/sa';
import localeFi from '@angular/common/locales/fil';

import ISO6391 from 'iso-639-1';

@Injectable()
export class TranslateProviderService {

    public LANG_EN = 'en';
    public LANG_DE = 'de';
    public LANG_HI = 'hi';
    public LANG_FR = 'fr';
    public LANG_BN = 'bn';
    public LANG_RU = 'ru';
    public LANG_TR = 'tr';
    public LANG_OR = 'or';
    public LANG_TA = 'ta';
    public LANG_SC = 'zh';
    public LANG_UR = 'ur';
    public LANG_TH = 'th';
    public LANG_TE = 'te';
    public LANG_VI = 'vi';
    public LANG_GU = 'gu';
    public LANG_ID = 'id';
    public LANG_ML = 'ml';
    public LANG_MS = 'ms';
    public LANG_SR = 'sr';
    public LANG_TL = 'tl';
    public LANG_KN = 'kn';
    public LANG_HE = 'he';
    public LANG_EL = 'el';
    public LANG_KO = 'ko';
    public LANG_PA = 'pa';
    public LANG_SA = 'sa';
    public availableLanguages = [
        this.LANG_EN,
        this.LANG_DE,
        this.LANG_FR,
        this.LANG_TR,
        this.LANG_EL,
        this.LANG_RU,
        this.LANG_SR,
        this.LANG_SA,
        this.LANG_HI,
        this.LANG_PA,
        this.LANG_GU,
        this.LANG_BN,
        this.LANG_OR,
        this.LANG_TE,
        this.LANG_KN,
        this.LANG_TA,
        this.LANG_ML,
        this.LANG_ID,
        this.LANG_MS,
        this.LANG_TL,
        this.LANG_TH,
        this.LANG_VI,
        this.LANG_UR,
        this.LANG_HE,
        this.LANG_KO,
        this.LANG_SC
    ];

    public DEFAULT_LANG = this.LANG_EN;
    public localStorageLangSet$ = new BehaviorSubject(false);

    constructor(
        private translate: TranslateService,
        private secureStore: SecureStorageService
    ) {}

    public init() {
        return new Promise<void>(
            async (resolve) => {
                await this.setDefaultLanguage();
                resolve();
            }
        );
    }

    public returnAllLanguagesWithNativeNames() {
        const nativeNames = ISO6391.getAllNativeNames();
        const allCodes = ISO6391.getAllCodes();
        const enNames = ISO6391.getAllNames();
        let result = [];
        for(let i=0; i< enNames.length; i++) {
            result.push({
                code: allCodes[i],
                name: nativeNames[i],
                en: enNames[i]
            })
        }

        result.sort((a,b) => a.name > b.name ? 1 : -1 );

        return result;
    }

    public returnAppSupportedLanguages() {
        const nativeNames = ISO6391.getAllNativeNames();
        const allCodes = ISO6391.getAllCodes();
        const enNames = ISO6391.getAllNames();
        const orderedAvailableLanguages = this.availableLanguages.sort();
        let result = [];
        for(let i=0; i< orderedAvailableLanguages.length; i++) {
            result.push({
                code: orderedAvailableLanguages[i],
                name: nativeNames[allCodes.findIndex(a => a == orderedAvailableLanguages[i])],
                en: enNames[allCodes.findIndex(a => a == orderedAvailableLanguages[i])]
            })
        }
        result.sort((a,b) => a.name > b.name ? 1 : -1);
        return result;
    }


    private async setDefaultLanguage(): Promise<void> {
        // retrive language from storage
        let language = await this.getLangFromStorage();

        if (!language) {
            // if there is no language in the storage just use the default value
            // TODO: check if the browser language is available or the device language and use this as default
            language = this.DEFAULT_LANG;
        }

        // await the translation file is loaded and the library ready
        await firstValueFrom(this.translate.use(language));

        this.setLocale(language);
        this.localStorageLangSet$.next(true);
    }

    /** Callback ti changing the language */
    public async changeLanguage(language: string) {
        language = this.checkIsLangAvailable(language);
        this.translate.use(language);
        try {
            await this.secureStore.setValue(SecureStorageKey.language, language);
        } catch (e) {
            console.error('There was an error setting the language to the storage', e);
            throw new Error(e);
        }
    }

    /** Retrieving the language from the storage */
    public async getLangFromStorage(): Promise<string> {
        let language;
        try {
            language = await this.secureStore.getValue(SecureStorageKey.language);
        } catch (e) {
            throw new Error(e);
        }
        return this.checkIsLangAvailable(language);
    }

    /** Mapper for instant translate */
    public instant(input: string, interpolateParams: { [key: string]: string } = {}) {
        return this.translate.instant(input, interpolateParams);
    }

    /** Mapper for asynchronous translate to wait till translate is ready */
    public translateGet(key: string, interpolateParams?: { [key: string]: string }): Promise<string> {
        return this.isReady$.pipe(
            flatMap(() => this.translate.get(key, interpolateParams))
        ).toPromise();
    }

    /** If the translation is ready */
    public get isReady$(): Observable<number> {
        return timer(0, 1000)
            .pipe(
                take(20),
                filter(() => size(this.translate.translations) > 0),
                take(1)
            );
    }

    private checkIsLangAvailable(language: string): string {
        return !language ?
            this.translate.getBrowserLang() :
            this.availableLanguages.indexOf(language) !== -1 ? language : this.LANG_EN;
    }

    private setLocale(language: string): void {
        switch (language) {
            case this.LANG_DE:
                registerLocaleData(localeDe, 'de-De');
                break;
            case this.LANG_BN:
                registerLocaleData(localeBn, 'bn-BN');
                break;
            case this.LANG_RU:
                registerLocaleData(localeRu, 'ru-RU');
                break;
            case this.LANG_TR:
                registerLocaleData(localeTr, 'tr-TR');
                break;
            case this.LANG_HI:
                registerLocaleData(localeHi, 'hi-IN');
                break;
            case this.LANG_FR:
                registerLocaleData(localeFr, 'fr-FR');
                break;
            case this.LANG_SC:
                registerLocaleData(localeZh, 'zh-CN');
                break;
            case this.LANG_OR:
                registerLocaleData(localeOr, 'or-IN');
                break;
            case this.LANG_TA:
                registerLocaleData(localeTa, 'ta-IN');
                break;
            case this.LANG_UR:
                registerLocaleData(localeUr, 'ur-IN');
                break;
            case this.LANG_TE:
                registerLocaleData(localeTe, 'te-IN');
                break;
            case this.LANG_TH:
                registerLocaleData(localeTh, 'th-TH');
                break;
            case this.LANG_VI:
                registerLocaleData(localeVi, 'vi-VI');
                break;
            case this.LANG_GU:
                registerLocaleData(localeGu, 'gu-VI');
                break;
            case this.LANG_ID:
                registerLocaleData(localeId, 'id-ID');
                break;
            case this.LANG_ML:
                registerLocaleData(localeMl, 'ml-IN');
                break;
            case this.LANG_MS:
                registerLocaleData(localeMs, 'ms-MS');
                break;
            case this.LANG_SR:
                registerLocaleData(localeSr, 'sr-SR');
                break;
            case this.LANG_TL:
                registerLocaleData(localeFi, 'tl-FI');
                break;
            case this.LANG_KO:
                registerLocaleData(localeKo, 'ko-KO');
                break;
            case this.LANG_HE:
                registerLocaleData(localeHe, 'he-IL');
                break;
            case this.LANG_EL:
                registerLocaleData(localeEl, 'el-GR');
                break;
            case this.LANG_PA:
                registerLocaleData(localePa, 'pa-IN');
                break;
            case this.LANG_KN:
                registerLocaleData(localeKn, 'kn-IN');
                break;
            case this.LANG_SA:
                registerLocaleData(localeSa, 'sa-IN');
                break;
            default:
                registerLocaleData(localeEn, 'en-Gb');
                break;
        }

    }

}
