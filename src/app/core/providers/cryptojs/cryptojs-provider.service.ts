import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';


@Injectable({
  providedIn: 'root'
})
export class CryptojsProviderService {

  constructor() { }

  public encrypt(payload: any, key: string) {
    return CryptoJS.AES.encrypt(encodeURIComponent(JSON.stringify(payload)), key).toString();
  }

  public decrypt(encrypted: string, key: string) {
    let valueDecrypted: any = encrypted;
    try {
      if (encrypted) {
        var coreDecrypted = CryptoJS.AES.decrypt(encrypted, key).toString(CryptoJS.enc.Utf8);
        // console.log(coreDecrypted);
        var decodedURI = decodeURIComponent(coreDecrypted);
        // console.log(decodedURI);
        valueDecrypted = JSON.parse(decodedURI);
        // console.log(valueDecrypted);
      }
    } catch (e) {
      console.log(e);
    }
    return valueDecrypted;
  }

}
