import { Injectable } from '@angular/core';

import { Storage } from '@ionic/storage-angular';


export enum StorageKeys {

  // onboardingHasRun = 'onboardingHasRun',
  // wizardNotRun = 'wizardNotRun',
  // driverLicenseSet = 'driverLicenseSet',
  // idCardSet = 'idCardSet',
  // passportSet = 'passportSet',
  // residencePermitSet = 'residencePermitSet',

  // onboardingHasRunSettingId = 'onboardingHasRunSettingId',
  // wizardNotRunSettingId = 'wizardNotRunSettingId',
  // driverLicenseSetSettingId = 'driverLicenseSetSettingId',
  // idCardSetSettingId = 'idCardSetSettingId',
  // passportSetSettingId = 'passportSetSettingId',
  // residencePermitSetSettingId = 'residencePermitSetSettingId'

}

@Injectable()
export class StorageProviderService {

  constructor(private storage: Storage) { }

  setItem(itemName: string, value: any): Promise<any> {
    return this.storage.set(itemName, value);
  }

  getItem(itemName: string): Promise<any> {
    return this.storage.get(itemName);
  }

  remove(itemName: string): Promise<any> {
    return this.storage.remove(itemName);
  }

  clear(): Promise<any> {
    return this.storage.clear();
  }

}
