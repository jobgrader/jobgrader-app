import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { delay, take } from 'rxjs/operators';
import moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {

    constructor(
        private platform: Platform,
        private translate: TranslateService
    ) {
    }

    private static baseScreenHeight = 731;
    private static baseCircleDiameter = 900;

    /** Helper to ass a stylesheet to the header */
    public static addStyleToHead(styles: string, id: string): void {
        const css: HTMLStyleElement = document.createElement('style');
        css.type = 'text/css';
        if ( (css as any).styleSheet ) {
            (css as any).styleSheet.cssText = styles;
        } else {
            const textNode: Text = document.createTextNode(styles);
            css.appendChild(textNode);
        }
        css.id = id;
        document.getElementsByTagName('head')[0].appendChild(css);
    }

    /** Helpder to remove a stylesheet from the header */
    public static removeStyleFromHead(id: string): void {
        document.getElementById(id).remove();
    }

    /** Generates a random string of a specific length */
    public static randomStringGenerator(length): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    /** Helper function to set an reactive timeout */
    public static setTimeout(ms: number = 10): Observable<void> {
        return of(void 0).pipe(delay(ms), take(1));
    }

    /** Helper to create a base 64 from image */
    public static getBase64Image(img) {
        img.crossOrigin = 'Anonymous';
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        return canvas.toDataURL();
    }

    /** Converts an image url to base 64 */
    public static convertImgToBase64URL(url, callback, outputFormat = 'image/jpeg', diffuser = 1) {
        const img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = () => {
            let canvas: any = document.createElement('CANVAS');
            const ctx: any = canvas.getContext('2d');
            let dataURL: any;
            canvas.height = img.height / diffuser;
            canvas.width = img.width / diffuser;
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            dataURL = canvas.toDataURL(outputFormat);
            console.log(UtilityService.getByteLen(dataURL));
            callback(dataURL);
            canvas = null;
        };
        img.src = url;
    }

    /** Helper to get byte length */
    public static getByteLen(normalVal: string) {
        // Force string type
        normalVal = String(normalVal);

        let byteLen = 0;
        for (let i = 0; i < normalVal.length; i++) {
            const c = normalVal.charCodeAt(i);
            byteLen += c < (1 <<  7) ? 1 :
                c < (1 << 11) ? 2 :
                    c < (1 << 16) ? 3 :
                        c < (1 << 21) ? 4 :
                            c < (1 << 26) ? 5 :
                                c < (1 << 31) ? 6 : Number.NaN;
        }
        return byteLen;
    }

    /** Handler for registering an image on load */
    public static setImageOnLoad(img, loadHandler, errorHandler = (_img, bool) => {}) {
        const helperImg = new Image();
        img.helperImg = helperImg;
        if ( loadHandler ) {
            helperImg.addEventListener('load',
                () => {
                    img.helperImg = null;
                    loadHandler(img);
                }, false);
        }
        if ( errorHandler ) {
            helperImg.addEventListener('error',
                () => {
                    img.helperImg = null;
                    errorHandler(img, false);
                }, false);
            helperImg.addEventListener('abort',
                () => {
                    img.helperImg = null;
                    errorHandler(img, true);
                }, false);
        }
        helperImg.src = img.src;
    }

    /** Generates a buffer from a string */
    public static str2ab(str: string) {
        const buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
        const bufView = new Uint16Array(buf);
        for ( let i = 0, strLen = str.length; i < strLen; i++ ) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

    // Converts an ArrayBuffer directly to base64, without any intermediate 'convert to string then
    // use window.btoa' step. According to my tests, this appears to be a faster approach:
    // http://jsperf.com/encoding-xhr-image-data/5

    /*
    MIT LICENSE
    Copyright 2011 Jon Leighton
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */
    public static base64ArrayBuffer(arrayBuffer): string {
        let base64    = '';
        const encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        const bytes         = new Uint8Array(arrayBuffer);
        const byteLength    = bytes.byteLength;
        const byteRemainder = byteLength % 3;
        const mainLength    = byteLength - byteRemainder;

        let a, b, c, d;
        let chunk;

        // Main loop deals with bytes in chunks of 3
        for (let i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048)   >> 12; // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032)     >>  6; // 4032     = (2^6 - 1) << 6
            d = chunk & 63;               // 63       = 2^6 - 1

            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
        }

        // Deal with the remaining bytes and padding
        if (byteRemainder === 1) {
            chunk = bytes[mainLength];

            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

            // Set the 4 least significant bits to zero
            b = (chunk & 3)   << 4; // 3   = 2^2 - 1

            base64 += encodings[a] + encodings[b] + '==';
        } else if (byteRemainder === 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008)  >>  4; // 1008  = (2^6 - 1) << 4

            // Set the 2 least significant bits to zero
            c = (chunk & 15)    <<  2; // 15    = 2^4 - 1

            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
        }

        return base64;
    }

    /** Date comparision */
    public dateCompare(d1: Date, d2: Date): boolean {
        return moment(d1).isSame(moment(d2));
    }

    /** Gets a weekday approbation */
    public getWeekDayApprobation(d: Date): string {
        const day = moment(d).isoWeekday();
        if (this.translate.currentLang === 'de') {
            switch (day) {
                case 1: return 'Mo';
                case 2: return 'Di';
                case 3: return 'Mi';
                case 4: return 'Do';
                case 5: return 'Fr';
                case 6: return 'Sa';
                case 7: return 'So';
            }
        } else {
            switch (day) {
                case 1: return 'Mo';
                case 2: return 'Tu';
                case 3: return 'We';
                case 4: return 'Th';
                case 5: return 'Fr';
                case 6: return 'Sa';
                case 7: return 'So';
            }
        }
    }


    /** Method to style the after tag accordingly on some screens */
    public calculateWhiteCircleCssParams(selector: string, baseBottom: number = 70): string {
        const screenHeight = this.platform.height();
        const heightFactor = screenHeight / UtilityService.baseScreenHeight;
        const circleDiameter = UtilityService.baseCircleDiameter * heightFactor;
        const circleBottom = baseBottom * heightFactor;
        const withPx = (v: number) => `${v}px`;
        return `
      ${selector} {
        width: ${withPx(circleDiameter)} !important;
        height: ${withPx(circleDiameter)} !important;
        bottom: ${withPx(circleBottom)} !important;
      }
    `;
    }

}
