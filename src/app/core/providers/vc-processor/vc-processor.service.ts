import { Injectable } from '@angular/core';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';

interface BasciDataValueTrust {
  key: string;
  value: string;
  trustValue: string;
  trustLevel: string;
  trustedBy: string;
  validFrom: string;
  validTo: string;
  vcId: string;
}

@Injectable({
  providedIn: 'root'
})
export class VcProcessorService {

  private ignoreFields = ['trustedBy', 'trustLevel', 'trustValue'];

  private sampleVc = {
    "@context":"https://www.w3.org/2018/credentials/v1",
    "id":"vc:evan:testcore:0x3a81bbc2c6b19b4f851e05da80403a1880bf74c1b898b83780bd327b29cd30b1",
    "type":[
      "VerifiableCredential",
      "KYC"
    ],
    "issuer":"did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b",
    "credentialSubject":{
      "id":"did:evan:testcore:0xe418f52c5357ec5264f1c4607eb4f7793770b367",
      "data":{
          "trustedBy":"Authada",
          "trustLevel":"3",
          "trustValue":"1",
          "residencepermitissuecountry":"DEU",
          "residencepermitexpirydate":"2027-04-05T00:00:00Z",
          "firstname":"ANDRÉ",
          "lastname":"MUSTERMANN",
          "title":"",
          "dateofbirth":"1981-06-17",
          "cityofbirth":"FRANKFURT (ODER)",
          "citizenship":"AZE",
          "maidenname":"",
          "city":"LÜBBENAU/SPREEWALD",
          "country":"DEU",
          "street":"EHM-WELK-STRAßE 33",
          "zip":"03222"
      }
    },
    "validFrom":"2021-03-04T10:54:18Z",
    "validTo":"2023-03-04T10:54:18Z",
    "credentialStatus":{
      "id":"https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x3a81bbc2c6b19b4f851e05da80403a1880bf74c1b898b83780bd327b29cd30b1",
      "type":"evan:evanCredential"
    },
    "proof":{
      "type":"EcdsaPublicKeySecp256k1",
      "created":"2021-03-04T10:54:21.030Z",
      "proofPurpose":"assertionMethod",
      "verificationMethod":"did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b#key-1",
      "jws":"eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE2MTQ4NTUyNjAsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJLWUMiXSwiaWQiOiJ2YzpldmFuOnRlc3Rjb3JlOjB4M2E4MWJiYzJjNmIxOWI0Zjg1MWUwNWRhODA0MDNhMTg4MGJmNzRjMWI4OThiODM3ODBiZDMyN2IyOWNkMzBiMSIsImlzc3VlciI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4ZTQxOGY1MmM1MzU3ZWM1MjY0ZjFjNDYwN2ViNGY3NzkzNzcwYjM2NyIsImRhdGEiOnsidHJ1c3RlZEJ5IjoiQXV0aGFkYSIsInRydXN0TGV2ZWwiOiIzIiwidHJ1c3RWYWx1ZSI6IjEiLCJyZXNpZGVuY2VwZXJtaXRpc3N1ZWNvdW50cnkiOiJERVUiLCJyZXNpZGVuY2VwZXJtaXRleHBpcnlkYXRlIjoiMjAyNy0wNC0wNVQwMDowMDowMFoiLCJmaXJzdG5hbWUiOiJBTkRSw4kiLCJsYXN0bmFtZSI6Ik1VU1RFUk1BTk4iLCJ0aXRsZSI6IiIsImRhdGVvZmJpcnRoIjoiMTk4MS0wNi0xNyIsImNpdHlvZmJpcnRoIjoiRlJBTktGVVJUIChPREVSKSIsImNpdGl6ZW5zaGlwIjoiQVpFIiwibWFpZGVubmFtZSI6IiIsImNpdHkiOiJMw5xCQkVOQVUvU1BSRUVXQUxEIiwiY291bnRyeSI6IkRFVSIsInN0cmVldCI6IkVITS1XRUxLLVNUUkHDn0UgMzMiLCJ6aXAiOiIwMzIyMiJ9fSwidmFsaWRGcm9tIjoiMjAyMS0wMy0wNFQxMDo1NDoxOFoiLCJ2YWxpZFRvIjoiMjAyMy0wMy0wNFQxMDo1NDoxOFoiLCJjcmVkZW50aWFsU3RhdHVzIjp7ImlkIjoiaHR0cHM6Ly90ZXN0Y29yZS5ldmFuLm5ldHdvcmsvc21hcnQtYWdlbnRzL3NtYXJ0LWFnZW50LWRpZC1yZXNvbHZlci92Yy9zdGF0dXMvdmM6ZXZhbjp0ZXN0Y29yZToweDNhODFiYmMyYzZiMTliNGY4NTFlMDVkYTgwNDAzYTE4ODBiZjc0YzFiODk4YjgzNzgwYmQzMjdiMjljZDMwYjEiLCJ0eXBlIjoiZXZhbjpldmFuQ3JlZGVudGlhbCJ9fSwiaXNzIjoiZGlkOmV2YW46dGVzdGNvcmU6MHg3Y2ZlOGVlNDVhYTUxMzhhZGYyN2M4MWIxODkzOTA5NDA5NTYxODZiIn0.4pquyMiJ5Va2Oce4Nmw5IHhGNHbkU8pqZsUTl4fyizHi16SZIp2JgtfeBfqje1p-8_UyAFmGJkLeM1y3lOtndQA"
    }
  }

  private observations = [
    { key: 1, value: 'credentialSubject.data.trustedBy should perhaps be changed to the DID of the kyc provider OR the clearname associated with the DID' },
    { key: 2, value: 'The VC can also contain the necessary media files' },
  ]

  constructor(
    private _SecureStorageService: SecureStorageService,
    private _CryptoProviderService: CryptoProviderService
  ) { }



  public init() {
    this._SecureStorageService.getValue(SecureStorageKey.verifiableCredentialEncrypted, false).then(verifiableCredentialEncrypted => {
      var vcs = !!verifiableCredentialEncrypted ? JSON.parse(verifiableCredentialEncrypted) : [];
      if(vcs.length > 0) {
        this._CryptoProviderService.decryptVerifiableCredential(vcs[0]).then(vc => {
          console.log(vc);
          console.log(vc.vc);
          var rawData = JSON.parse(vc.vc);
          console.log(rawData);
          var keyValuesObject = rawData.credentialSubject.data;
          var mediaValues = null;
          var rawDisplayables: Array<BasciDataValueTrust> = [];
          var trustValue = keyValuesObject.trustValue;
          var trustedBy = keyValuesObject.trustedBy;
          var trustLevel = keyValuesObject.trustLevel;
          var vcId = rawData.id;
          var validFrom = rawData.validFrom;
          var validTo = rawData.validTo;
          Object.keys(keyValuesObject).forEach(kv => {
            if(!this.ignoreFields.includes(kv)){
              rawDisplayables.push({
                key: kv,
                value: keyValuesObject[kv],
                vcId,
                trustValue,
                trustLevel,
                trustedBy,
                validFrom,
                validTo,
              })
            }
          })
          console.log(rawDisplayables);
        }).catch(e => {
          console.log(e);
        })
      }
    }).catch(e => {
      console.log(e);
    })
  }

}

