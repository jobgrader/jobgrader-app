export const UDIDNonce = {
    helix: "e9a2c4e470d50d0f1a04b24e0072493edc5a5776e08c9b8c28d997fa24892446",
    secureStorage: "083950eff9cd771218fdacb12d9258ab183523e7fffd9a54ba082ece29339ba6",
    coupons: "3b23829e5798c6faa40301ae092991270aaaa9bc6ef2a332a1892ce2c3256a42",
    userKey: "3b23829e5798c6faa40301ae092991270aaaa9bc6ef2a332a1892ce2c3256a42",
    energy: "e689cc78-c7e2-43d9-a4bc-8cdee7155986"
}

export const KycPins = {
    authada: "348916",
    veriff: "247629",
    ondato: "120897",
    verifeye: "134865"
}