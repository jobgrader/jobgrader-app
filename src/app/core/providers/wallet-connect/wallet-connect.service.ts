import { Injectable } from '@angular/core';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { environment } from 'src/environments/environment';
import { ethers, providers, Wallet } from 'ethers';
import WalletConnect from "@walletconnect/client";
import { AlertController, ModalController, ToastController } from '@ionic/angular';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { WcSessionRequestComponent } from '../nft/wc-session-request/wc-session-request.component';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { CryptoCurrency, CryptoNetworkNames, EthereumNetworks } from './constants';
import { WcNetworkSelectionComponent } from '../nft/wc-network-selection/wc-network-selection.component';
import { IWalletConnectOptions } from "@walletconnect/client/node_modules/@walletconnect/types";
import { UtilityService } from '../utillity/utility.service';
import { UserProviderService } from '../user/user-provider.service';
import SignClient from "@walletconnect/sign-client";
import { Core } from "@walletconnect/core";
import Client, { Web3Wallet } from "@walletconnect/web3wallet";
import { getSdkError } from "@walletconnect/utils";
import { Wc2SessionRequestComponent } from '../nft/wc2-session-request/wc2-session-request.component';
import { Clipboard } from '@capacitor/clipboard';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { GAS_USED, GasService } from '../gas/gas.service';
import CryptoUtils from '@walletconnect/client';
import { InAppBrowser } from '@awesome-cordova-plugins/in-app-browser/ngx';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';
// const ethSigUtil = require("eth-sig-util");

export const CLIENT_META = {
  description: "Your Jobs. Your Money.",
  url: "https://jobgrader.app",
  icons: ["https://drive.google.com/uc?export=view&id=1XMV6voCh4EEq9lMT1FVib5Jr6zhE0nRz"],
  name: "Jobgrader",
};


@Injectable({
  providedIn: 'root'
})
export class WalletConnectService {

  public activeSessions: Array<WalletConnect> = [];
  public activeSessions2: Array<any> = [];
  private deepLinkNavigation: boolean = false;

  constructor(
    private _SecureStorageService: SecureStorageService,
    private _AlertController: AlertController,
    private _ModalController: ModalController,
    private _LoaderController: LoaderProviderService,
    private _Vault: VaultService,
    private _Translate: TranslateProviderService,
    private cryptoProvider: CryptoProviderService,
    private _UserProviderService: UserProviderService,
    private _Gas: GasService,
    private iab: InAppBrowser,
    private _ToastController: ToastController,
    private _Firecloud: FirestoreCloudFunctionsService
  ) {

  }

  subscribeToEvents(connector: WalletConnect, wallet: Wallet) {

    var chainId = connector.chainId;

    connector.on("session_request", (error, payload) => {

      console.log("Wallet Connect, session_request");
      console.log(JSON.stringify(payload));

      if (error) { throw error; }

      this._ModalController.create({
        component: WcSessionRequestComponent,
        componentProps: {
          data: payload,
          request: 'session_request',
          selfAddress: wallet.address,
          targetMerchantAccount: connector['targetMerchantAccount']
        }
      }).then(modal => {
        modal.onDidDismiss().then((dismissObject) => {
          console.log(dismissObject);
          var check = dismissObject.data.value;
          if (check) {
            try {
              connector.approveSession({ chainId, accounts: [wallet.address] });
              this.activeSessions.push(connector);
              this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions)).then(() => {
                this.navigateViaDeeplink(connector.peerMeta.url);
              })
            } catch (e) {
              // alert("approveSession ERROR");
            }
          } else {
            connector.rejectSession({ message: 'Session Request rejected' });
          }
        })
        modal.present();
      })

    });

    // session_update
    connector.on("session_update", (error, payload) => {

      console.log("Wallet Connect, session_update");
      console.log(JSON.stringify(payload));

      if (error) { throw error; }

      console.log("session_update: " + JSON.stringify(payload));

    });

    // call_request
    connector.on("call_request", async (error, payload) => {

      console.log("Wallet Connect, call_request");
      console.log(JSON.stringify(payload));

      console.log("EVENT: call_request: " + JSON.stringify(payload));
      if (error) { throw error; }


      const { id, jsonrpc } = payload;

      if (payload.method == 'wallet_switchEthereumChain') {

        const newChainId = parseInt(payload.params[0].chainId);

        switch (newChainId) {
          case 508674158: var networkName = CryptoNetworkNames.EVAN_TEST as string; break;
          case 49262: networkName = CryptoNetworkNames.EVAN as string; break;
          case 80001: networkName = CryptoNetworkNames.POLYGON_TEST_MUMBAI as string; break;
          case 137: networkName = CryptoNetworkNames.POLYGON as string; break;
          case 100: networkName = CryptoNetworkNames.GNOSIS as string; break;
          case 4: networkName = CryptoNetworkNames.ETHEREUM_TEST_RINKEBY as string; break;
          case 1: networkName = CryptoNetworkNames.ETHEREUM as string; break;
          default: networkName = ''; break;
        }

        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'wallet_switchEthereumChain',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-3')} ${networkName}`,
              message: `${JSON.stringify(payload.params)}`,
            }
          }
        }).then(modal => {

          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;

            if (check) {

              connector.updateSession({ chainId: newChainId, accounts: [wallet.address] });
              this.presentToast(this._Translate.instant('WALLETCONNECT.chain-update-title'), `${this._Translate.instant('WALLETCONNECT.chain-update-success')} ${networkName} (${payload.params[0].chainId})`);

              var index = this.activeSessions.findIndex(c => c.uri == connector.uri);
              this.activeSessions[index] = connector;
              // this.activeSessions[index]['targetMerchantAccount'] = targetMerchantAccount;
              this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions)).then(() => {
                this.navigateViaDeeplink(connector.peerMeta.url);
              })

            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }

      if (payload.method == 'eth_sign') {

        try {
          var signingString = `${this.cryptoProvider.hex2string(payload.params[1])}`;
        } catch (e) {
          signingString = `${JSON.stringify(payload.params)}`
        }


        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'eth_sign',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${signingString}`,
            }
          }
        }).then(modal => {
          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              wallet.signMessage(`${signingString}`).then(result => {
                console.log(`eth_sign response: ${JSON.stringify(result)}`);
                connector.approveRequest({ id, jsonrpc, result });
                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                });
              }).catch(e => console.log(e));
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }

      if (payload.method == 'personal_sign') {
        console.log(connector);
        console.log(connector.peerMeta);


        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'personal_sign',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${this.cryptoProvider.hex2string(payload.params[0])}`,
            }
          }
        }).then(modal => {
          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              wallet.signMessage(this.cryptoProvider.hex2string(payload.params[0])).then(result => {
                console.log(`personal_sign response: ${JSON.stringify(result)}`);
                connector.approveRequest({ id, jsonrpc, result });
                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                });
              }).catch(e => console.log(e));
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }

      if (payload.method == 'eth_signTypedData') {

        console.log(payload);

        try {
          var signingString = `${this.cryptoProvider.hex2string(payload.params[1])}`;
        } catch (e) {
          signingString = `${JSON.stringify(payload.params)}`
        }

        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'eth_signTypedData',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${signingString}`,
            }
          }
        }).then(modal => {
          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              var signerObject = JSON.parse(payload.params[1]);
              delete signerObject.types['EIP712Domain'];
              // delete signerObject.types['RelayRequest'];
              wallet._signTypedData(signerObject.domain, signerObject.types, signerObject.message).then(result => {
                console.log(`eth_signTypedData response: ${JSON.stringify(result)}`);
                connector.approveRequest({ id, jsonrpc, result });
                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                })
              }).catch(e => console.log(e));
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }

      if (payload.method == 'helix_kyc') {

        console.log(payload);

        try {
          var signingString = `${this.cryptoProvider.hex2string(payload.params[1])}`;
        } catch (e) {
          signingString = `${JSON.stringify(payload.params)}`
        }

        console.log(payload.params[1]);
        var basicdataKeys = payload.params[1]['basicdata'];
        var trustKeys = payload.params[1]['trust'];
        var minAgeCheck = payload.params[1]['minAgeCheck'];
        var transferData = {
          timestamp: +new Date(),
          basicdata: {},
          trust: {},
          minAgeCheck
        };

        var userData = await this._UserProviderService.getUser();

        for (var key1 of basicdataKeys) {
          transferData['basicdata'][key1] = userData[key1];
        }

        for (var key2 of trustKeys) {
          transferData['trust'][key2] = (userData[`${key2}Status`] == 1);
        }

        const modal = await this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'helix_kyc',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${JSON.stringify(transferData)}`,
            }
          }
        })
        modal.onDidDismiss().then((dismissObject) => {
          console.log(dismissObject);
          var check = dismissObject.data.value;
          if (check) {
            // var parsed  = JSON.parse(payload.params[1]);
            // console.log(parsed);
            wallet.signMessage(JSON.stringify(transferData)).then(result => {
              console.log(`helix_kyc response: ${JSON.stringify(result)}`);

              var gg = Object.assign(transferData, { signature: result })
              connector.approveRequest({ id, jsonrpc, result: JSON.stringify(gg) });

              this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                this.navigateViaDeeplink(connector.peerMeta.url);
              })
            }).catch(e => console.log(e));
          } else {
            connector.rejectRequest({
              id, error: {
                code: 0,
                message: 'Request was rejected by the user'
              }
            });
          }
        })
        await modal.present();
      }

      if (payload.method == 'eth_sendTransaction') {

        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'eth_sendTransaction',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${JSON.stringify(payload.params)}`,
            }
          }
        }).then(modal => {
          modal.onDidDismiss().then(async (dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {

              const CRYPTONETWORKS_POLYGON = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON);
              const CRYPTONETWORKS_GNOSIS = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_GNOSIS);

              switch (dismissObject.data.networkName) {
                case CryptoNetworkNames.POLYGON:
                  var network = ethers.getDefaultProvider(CRYPTONETWORKS_POLYGON);
                  var gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC);
                  var currency = CryptoCurrency.MATIC;
                  break;
                case CryptoNetworkNames.GNOSIS:
                  network = ethers.getDefaultProvider(CRYPTONETWORKS_GNOSIS);
                  gasPrice = await this._Gas.getGasPriceEstimationXDAI();
                  currency = CryptoCurrency.GNOSIS;
                  break;
                case CryptoNetworkNames.ETHEREUM:
                  network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                  gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH);
                  currency = CryptoCurrency.ETH;
                  break;
                default:
                  network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                  gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH);
                  currency = CryptoCurrency.ETH;
                  break;
              }

              console.log({ network, gasPrice, currency });

              var normalGasPrice = Number(ethers.utils.formatEther(ethers.utils.parseUnits(gasPrice.normal, gasPrice.unit)._hex));
              var factor = GAS_USED[currency];
              var cleanedGasPrice = (normalGasPrice / factor).toFixed(10);
              var utilsGasPrice = ethers.utils.parseEther(cleanedGasPrice);

              console.log({ normalGasPrice, factor, cleanedGasPrice, utilsGasPrice });

              const walletProvider = wallet.connect(network);

              var { from, to, data } = payload.params[0];
              var txObject = {
                from, to, data,
                nonce: await network.getTransactionCount(from, "latest"),
                // gasLimit: ethers.utils.hexlify(21000),
                gasPrice: await network.getGasPrice() // utilsGasPrice
              }

              console.log("txObject", txObject);

              walletProvider.sendTransaction(txObject).then(result => {
                console.log(`eth_sendTransaction response: ${JSON.stringify(result)}`);
                connector.approveRequest({ id, jsonrpc, result: result.hash });
                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.hash.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                })
              }).catch(err => {
                console.log(err);
                try {
                  var key = err.toString().slice(err.toString().indexOf("code=") + 5).split(",")[0];
                  this.presentToast('', this._Translate.instant(`ETHERRORS.${key}`));
                } catch (ef) {
                  console.log(ef);
                }
              });
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }

      if (payload.method == 'eth_signTransaction') {

        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: { params: [connector] },
            request: 'eth_signTransaction',
            selfAddress: wallet.address,
            targetMerchantAccount: connector['targetMerchantAccount'],
            additionalTexts: {
              header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
              message: `${JSON.stringify(payload.params)}`,
            }
          }
        }).then(modal => {
          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              wallet.signTransaction(payload.params[0]).then(result => {
                console.log(`eth_signTransaction response: ${JSON.stringify(result)}`);
                connector.approveRequest({ id, jsonrpc, result });
                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                })
              }).catch(e => console.log(e));
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          modal.present();
        })

      }
    });

    connector.on("disconnect", (error, payload) => {
      console.log(payload);
      if (error) { throw error; }
      this.presentToast(this._Translate.instant('WALLETCONNECT.session-ended-title'), `${this._Translate.instant('WALLETCONNECT.session-ended-message-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.session-ended-message-2')}`).then(() => {
        connector.killSession(payload.params[0]).then(async () => {
          console.log("connector", connector);
          console.log("this.activeSessions", this.activeSessions);
          this.activeSessions = this.activeSessions.filter(k => (k['backupUri'] != connector['backupUri']));
          await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
        }).catch(async e => {
          console.log("connector", connector);
          console.log("this.activeSessions", this.activeSessions);
          this.activeSessions = this.activeSessions.filter(k => (k['backupUri'] != connector['backupUri']));
          await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
        })

      })
    });
  }

  async subscribeToEvents2(web3wallet: any, wallet: Wallet, optionalParams?: any) {
    let peerMeta = {};
    let namespaces: any = {};
    let targetMerchantAccount: string;

    let topic: string;
    let id: any;
    let session: any;
    let name: string;

    const firebaseToken = await this._SecureStorageService.getValue(SecureStorageKey.firebase, false);
    // Note: To be used for push notification registration in future

    web3wallet.on("session_proposal", async (proposal) => {

      console.info(proposal);
      peerMeta = proposal.params.proposer.metadata;
      targetMerchantAccount = proposal.params.proposer.publicKey;

      this._ModalController.create({
        component: Wc2SessionRequestComponent,
        componentProps: {
          data: proposal.params,
          request: 'session_proposal',
          selfAddress: wallet.address,
          targetMerchantAccount
        }
      }).then(modal => {
        modal.onDidDismiss().then(async (dismissObject) => {
          console.log(dismissObject);
          var check = dismissObject.data.value;
          if (check) {
            try {
              namespaces = (!!proposal.params.requiredNamespaces['eip155'] ? proposal.params.requiredNamespaces : proposal.params.optionalNamespaces);
              namespaces['eip155']['accounts'] = namespaces['eip155']['chains'].map(ff => `${ff}:${wallet.address}`);
              console.log(namespaces);

              session = await web3wallet.approveSession({
                id: proposal.params.id,
                namespaces
              });

              console.info(session);

              id = session.topic;

              this._Firecloud.registerWCTopic(session.topic, firebaseToken);

              if (session) {

                try {
                  if (firebaseToken) {
                    web3wallet.registerDeviceToken(firebaseToken);
                  }
                } catch (error) {
                  console.error(error);
                }

                name = session.peer.metadata.name;
                this.presentToast(this._Translate.instant('WALLETCONNECT_V2.connection_successful'), `Connected to ${name}`);
                this.activeSessions2.push(session);
                console.log(this.activeSessions2);
                try {
                  await this._SecureStorageService.setValue(SecureStorageKey.walletConnect2Sessions, JSON.stringify(this.activeSessions2));
                  this.navigateViaDeeplink(session.peer.metadata.url);
                } catch (ee) {
                  console.log(ee);
                }
              }
            } catch (e) {
              console.log(e);
              // alert("approveSession ERROR");
            }
          } else {
            await web3wallet.rejectSession({
              id: proposal.params.id,
              reason: getSdkError("USER_REJECTED_METHODS")
            })
          }
        })
        modal.present();
      })

    });

    web3wallet.on("session_request", async (event) => {

      this.presentToast("WalletConnect v2", "session_request");
      console.log("WalletConnect v2 session_request", event);

      const { params } = event;
      topic = event.topic;
      id = event.id;
      const { request } = params;

      switch (request.method) {
        case "personal_sign": var requestParamsMessage = this.cryptoProvider.hex2string(request.params[0]); break;
        case "eth_signTransaction": requestParamsMessage = JSON.stringify(request.params); break;
        case "eth_sendTransaction": requestParamsMessage = JSON.stringify(request.params); break
        case "eth_sign": requestParamsMessage = this.cryptoProvider.hex2string(request.params[1]); break;
        case "eth_signTypedData": requestParamsMessage = JSON.stringify(request.params[1]); break;
        case "helix_kyc": {
          var basicdataKeys = request.params[1]['basicdata'];
          var trustKeys = request.params[1]['trust'];
          var minAgeCheck = request.params[1]['minAgeCheck'];
          var transferData = {
            timestamp: +new Date(),
            basicdata: {},
            trust: {},
            minAgeCheck: null
          };

          var userData = await this._UserProviderService.getUser();

          for (var key1 of basicdataKeys) {
            transferData['basicdata'][key1] = !!userData[key1] ? userData[key1] : '';
          }

          for (var key2 of trustKeys) {
            transferData['trust'][key2] = (userData[`${key2}Status`] == 1);
          }

          if (minAgeCheck) {
            var getAge = function (dateString) {
              var today = new Date();
              var birthDate = new Date(dateString);
              var age = today.getFullYear() - birthDate.getFullYear();
              var m = today.getMonth() - birthDate.getMonth();
              if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
              }
              return age;
            }
            var age = getAge(userData.dateofbirth);
            transferData['minAgeCheck'] = (userData.dateofbirthStatus == 1 && age >= 18);
          }

          requestParamsMessage = JSON.stringify(transferData);
          break;
        }
        default: requestParamsMessage = JSON.stringify(request.params); break;
      }

      console.log("🚀 ~ peerMeta:", peerMeta);
      console.log("🚀 ~ web3wallet:", web3wallet);
      console.log("🚀 ~ namespaces:", namespaces);
      console.log("🚀 ~ requestParamsMessage:", requestParamsMessage);

      const componentProps = {
        data: (JSON.stringify(peerMeta) != '{}') ? peerMeta : web3wallet.metadata,
        namespaces: !optionalParams ? namespaces : optionalParams.namespaces,
        request: params.request.method,
        selfAddress: wallet.address,
        targetMerchantAccount: params.request.params[1],
        additionalTexts: {
          header: `There is an incoming request on ${params.chainId} chain to sign the following message:`,
          message: `${requestParamsMessage}`,
        }
      };

      if (Object.keys(componentProps.namespaces).length == 0) {
        return;
      }

      this._ModalController.create({
        component: Wc2SessionRequestComponent,
        componentProps
      }).then(modal => {
        modal.onDidDismiss().then(async (dismissObject) => {
          console.log(dismissObject);
          var check = dismissObject.data.value;
          if (check) {
            console.log(check);

            // ! sign(keccak256("\x19Ethereum Signed Message:\n" + len(message) + message))).
            // ? I don’t understand the last line. Isn’t this supposed to be
            // ! sign(keccak256("\x19\x01" + domainSeparator + hashStruct(message)))

            switch (request.method) {
              case "personal_sign": var signingMessage = this.cryptoProvider.hex2string(request.params[0]); break;
              case "eth_signTransaction": signingMessage = request.params[0]; break;
              case "eth_sendTransaction": signingMessage = request.params[0]; break;
              case "eth_sign": signingMessage = this.cryptoProvider.hex2string(request.params[1]); break;
              case "eth_signTypedData": signingMessage = JSON.parse(request.params[1]); break;
              case "helix_kyc": signingMessage = JSON.stringify(requestParamsMessage); break;
              default: signingMessage = JSON.stringify(request.params); break;
            }

            console.log(request.method);
            console.log("🚀 signingMessage:", signingMessage);

            if (request.method == "eth_signTypedData") {
              delete (signingMessage as any).types['EIP712Domain'];
            }

            switch (request.method) {
              case "personal_sign": var signature = await wallet.signMessage(signingMessage); break;
              case "eth_signTransaction": signature = await wallet.signTransaction(signingMessage as any); break;
              case "eth_sendTransaction": {

                const newChainId = parseInt(params.chainId.split(":")[1]);

                switch (newChainId) {
                  case 508674158: var network = ethers.getDefaultProvider(await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_EVAN_TEST));
                    var currency = CryptoCurrency.EVE;
                    break;
                  case 49262: network = ethers.getDefaultProvider(await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_EVAN));
                    currency = CryptoCurrency.EVE;
                    break;
                  case 80001: network = ethers.getDefaultProvider(await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON_TEST_MUMBAI));
                    currency = CryptoCurrency.MATIC;
                    break;
                  case 137: network = ethers.getDefaultProvider(await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON));
                    currency = CryptoCurrency.MATIC;
                    break;
                  case 1: network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                    currency = CryptoCurrency.ETH;
                    break;
                  default: network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                    currency = CryptoCurrency.ETH;
                    break;
                }

                const walletProvider = wallet.connect(network);

                var { from, to, data } = signingMessage as any;
                var txObject = {
                  from, to, data,
                  nonce: await network.getTransactionCount(from, "latest"),
                  // gasLimit: ethers.utils.hexlify(21000),
                  gasPrice: await network.getGasPrice()
                }
                var rr = await walletProvider.sendTransaction(txObject); // await wallet.signMessage(signingMessage); 
                console.log(`eth_sendTransaction response: ${JSON.stringify(rr)}`);
                // signature = await wallet.signTransaction(signingMessage as any); 
                signature = rr.hash;
                break;
              }
              case "eth_sign": signature = await wallet.signMessage(signingMessage); break;
              case "eth_signTypedData": signature = await wallet._signTypedData(
                (signingMessage as any).domain,
                (signingMessage as any).types,
                (signingMessage as any).message); break;
              case "helix_kyc": signature = await wallet.signMessage(signingMessage); break;
              default: signature = await wallet.signMessage(signingMessage); break;
            }

            var result = (request.method == "helix_kyc") ? JSON.stringify(Object.assign(transferData, { signature })) : signature;
            const response = { id, result, jsonrpc: "2.0" };
            await web3wallet.respondSessionRequest({ topic, response });

            // eth_sendTransaction: unknown
            // eth_signTransaction: YES
            // personal_sign: YES
            // eth_sign: YES
            // eth_signTypedData: YES

            this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`);
            this.navigateViaDeeplink(session.peer.metadata.url);
          } else {
            const response = { id, jsonrpc: "2.0", error: { code: 5000, message: "User rejected" } };
            await web3wallet.respondSessionRequest({ topic, response });
          }
        })
        modal.present();
      })

    });

    web3wallet.on('push', (notification) => {
      // TODO: Handle incoming push notification
      console.info("walletConnect: notification: ", notification);
    });

    web3wallet.on("session_request_expire", (event) => {
      // TODO: Dismiss the modal window when the session is expired.
      console.info("walletConnect: session_request_expire: ", event);
    });

    web3wallet.on("session_update", async (event) => {
      // TODO: Update the session as required
      console.info("walletConnect: session_update: ", event);
      // await web3wallet.updateSession({ topic, namespaces: event });
    })

    web3wallet.on("session_delete", async (event) => {
      console.info({ event });
      console.log(name);
      try {
        await web3wallet.disconnectSession({ topic: event.topic, reason: getSdkError("USER_DISCONNECTED") });
        this._Firecloud.deregisterWCTopic(id);
      } catch (e) {
        console.log(e);
      }
      await this.presentToast(this._Translate.instant('WALLETCONNECT_V2.disconnection_successful'), `Disconnected from ${session.peer.metadata.name}`);
      console.info("this.activeSessions2", this.activeSessions2);
      this.activeSessions2 = this.activeSessions2.filter(aa => aa.topic != event.topic);
      try {
        await this._SecureStorageService.setValue(SecureStorageKey.walletConnect2Sessions, JSON.stringify(this.activeSessions2));
      } catch (ee) {
        console.log(ee);
      }
      try {
        web3wallet.deregisterDeviceToken(firebaseToken);
        // Note: To be used for push notification de-registration in future
      } catch (error) {
        console.error(error);
      }
    })

    web3wallet.on("auth_request", async (event) => {
      this.presentToast("WalletConnect v2", "auth_request");
      console.log("WalletConnect v2 auth_request", event);

      const { params } = event;

      this._ModalController.create({
        component: Wc2SessionRequestComponent,
        componentProps: {
          data: peerMeta,
          namespaces,
          request: "auth_request",
          selfAddress: wallet.address,
          targetMerchantAccount,
          additionalTexts: {
            header: `There is an incoming authorization request on:`,
            message: `${JSON.stringify(params)}`,
          }
        }
      }).then(modal => {
        modal.onDidDismiss().then(async (dismissObject) => {
          console.log(dismissObject);
          var check = dismissObject.data.value;
          if (check) {
            console.log(check);
            // await web3wallet.respondAuthRequest(params, 'iss');
            this.presentToast(`Authorization`, `Done`);
          } else {
            this.presentToast(`Authorization`, `Cancel`);
          }
        })
        modal.present();
      })
    })
  }

  manageWalletConnectConnections2() {
    this._Vault.getSecret(VaultSecretKeys.WALLET_CONNECT_PROJECT_ID).then((projectId) => {
      this._SecureStorageService.getValue(SecureStorageKey.walletConnect2Sessions, false).then((walletConnect2SessionsString) => {
        console.log("walletConnect2SessionsString", walletConnect2SessionsString);
        this.activeSessions2 = !!walletConnect2SessionsString ? JSON.parse(walletConnect2SessionsString) : [];
        console.log(this.activeSessions2);
        if (this.activeSessions2.length > 0) {
          for (let o = 0; o < this.activeSessions2.length; o++) {
            this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then((web3WalletPublicKey) => {
              if (this.activeSessions2[o].namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(web3WalletPublicKey) > -1) > -1) {
                this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false).then(async (web3WalletMnemonic) => {
                  if (web3WalletMnemonic) {
                    var wallet = ethers.Wallet.fromMnemonic(web3WalletMnemonic);
                    const core = new Core({ projectId });
                    const topic = this.activeSessions2[o].topic;
                    const namespaces = this.activeSessions2[o].namespaces;
                    const web3Wallet = await Web3Wallet.init({ core, metadata: CLIENT_META });
                    this.subscribeToEvents2(web3Wallet, wallet, namespaces);
                    // await web3Wallet.core.pairing.activate({ topic });
                  }
                  else {
                    console.log(this._Translate.instant('WALLETCONNECT_V2.seed_not_found'));
                  }
                })
              } else {
                this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false).then(async (importedWallets) => {
                  var parsedImportedWallets = !!importedWallets ? JSON.parse(importedWallets) : [];
                  if (parsedImportedWallets.length > 0) {
                    var check = parsedImportedWallets.find(p => this.activeSessions2[o].namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(p.publicKey) > -1) > -1);
                    if (!check) {
                      console.log(this._Translate.instant('WALLETCONNECT_V2.wallet_not_found'));
                      return;
                    }
                    if (!check.mnemonic) {
                      console.log(this._Translate.instant('WALLETCONNECT_V2.seed_not_found'));
                      return;
                    }
                    var wallet = ethers.Wallet.fromMnemonic(check.mnemonic);
                    const core = new Core({ projectId });
                    const topic = this.activeSessions2[o].topic;
                    const namespaces = this.activeSessions2[o].namespaces;
                    const web3Wallet = await Web3Wallet.init({ core, metadata: CLIENT_META });
                    this.subscribeToEvents2(web3Wallet, wallet, { namespaces, topic });
                    // await web3Wallet.core.pairing.activate({ topic });
                  } else {
                    console.log(this._Translate.instant('WALLETCONNECT_V2.wallet_not_found'));
                  }
                })
              }
            })
          }
        }
      })
    })
  }


  manageWalletConnectConnections() {
    this._SecureStorageService.getValue(SecureStorageKey.walletConnectSessions, false).then((walletConnectSessionsString) => {
      console.log("walletConnectSessionsString", walletConnectSessionsString);
      this.activeSessions = !!walletConnectSessionsString ? JSON.parse(walletConnectSessionsString) : [];
      console.log(this.activeSessions);

      if (this.activeSessions.length > 0) {
        for (let i = 0; i < this.activeSessions.length; i++) {
          this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then((web3WalletPublicKey) => {
            if ([this.activeSessions[i]['connectedAccount']].includes(web3WalletPublicKey)) {
              this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false).then((web3WalletMnemonic) => {
                if (web3WalletMnemonic) {
                  var wallet = ethers.Wallet.fromMnemonic(web3WalletMnemonic);
                  this.activeSessions[i] = new WalletConnect({
                    uri: this.activeSessions[i]['backupUri'],
                    storageId: this.activeSessions[i]['_sessionStorage']['storageId'],
                    clientMeta: CLIENT_META
                  });
                  this.subscribeToEvents(this.activeSessions[i], wallet);
                  console.log(this.activeSessions[i]);
                } else {
                  console.log(this._Translate.instant('WALLETCONNECT_V2.seed_not_found'));
                }
              })
            } else {
              this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false).then((importedWallets) => {
                var parsedImportedWallets = !!importedWallets ? JSON.parse(importedWallets) : [];
                if (parsedImportedWallets.length > 0) {
                  var check = parsedImportedWallets.find(p => [this.activeSessions[i]['connectedAccount']].includes(p.publicKey));
                  if (!check) {
                    console.log(this._Translate.instant('WALLETCONNECT_V2.wallet_not_found'));
                    return;
                  }
                  if (!check.mnemonic) {
                    console.log(this._Translate.instant('WALLETCONNECT_V2.seed_not_found'));
                    return;
                  }
                  var wallet = ethers.Wallet.fromMnemonic(check.mnemonic);
                  this.activeSessions[i] = new WalletConnect({
                    uri: this.activeSessions[i]['backupUri'],
                    storageId: this.activeSessions[i]['_sessionStorage']['storageId'],
                    clientMeta: CLIENT_META
                  });
                  this.subscribeToEvents(this.activeSessions[i], wallet);
                  console.log(this.activeSessions[i]);
                } else {
                  console.log(this._Translate.instant('WALLETCONNECT_V2.wallet_not_found'));
                }
              })
            }
          })
        }
      }
    })
  }

  // wc:e187b8be52082d51e0085050a741c1caf6f23efd7de035a3c9eae213a3ae5b24@2?relay-protocol=irn&symKey=111b7844fc8383c9cbcfa5efed2c1bcac706c1b6c010f11a10c2cba1e4a6b370
  // return;

  async processVersion2(uri: string, wallet: Wallet) {
    const projectId = await this._Vault.getSecret(VaultSecretKeys.WALLET_CONNECT_PROJECT_ID);
    const core = new Core({ projectId });
    const web3wallet = await Web3Wallet.init({ core, metadata: CLIENT_META });

    await this.subscribeToEvents2(web3wallet, wallet);

    await web3wallet.core.pairing.pair({ uri });

  }

  async init(uri: string, requestAddress?: string) {
    var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);
    var verifiedWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    var privateKey = null;
    if (requestAddress) {
      var importedWalletsString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
      var importedWallets = !!importedWalletsString ? JSON.parse(importedWalletsString) : [];
      var checker = importedWallets.find(iop => iop.publicKey == requestAddress);
      if (checker) {
        mnemonic = checker.mnemonic;
        privateKey = checker.privateKey;
        if (!mnemonic && !privateKey) {
          this.presentToast(this._Translate.instant('IMPORTKEY.initiate-heading'), this._Translate.instant('IMPORTKEY.initiate-message'));
          return;
        }
      }
    }

    if (!requestAddress && !mnemonic) {
      this.presentToast(this._Translate.instant('IMPORTKEY.initiate-heading'), this._Translate.instant('IMPORTKEY.initiate-message'));
      return;
    }

    var wallet = !!privateKey ? new ethers.Wallet(privateKey) : ethers.Wallet.fromMnemonic(mnemonic);
    var targetMerchantAccount;
    var firebaseToken = await this._SecureStorageService.getValue(SecureStorageKey.firebase, false);
    var pushNotificationConfig = null;

    if (firebaseToken) {
      pushNotificationConfig = {
        url: `${environment.apiUrl}/api/v1/notifications/walletconnect`,
        type: "fcm",
        token: firebaseToken,
        peerMeta: true,
        language: await this._SecureStorageService.getValue(SecureStorageKey.language, false),
      }
    }

    // var storageValue = await this._SecureStorageService.getValueOfExceptionKey();
    // if(storageValue) {
    //   if(typeof(storageValue) == 'string') {
    //     try {
    //       const connector = new WalletConnect(JSON.parse(storageValue));
    //       await connector.killSession();
    //     } catch(e) {
    //       console.log(e);
    //       await this._SecureStorageService.removeValueOfExceptionKey();
    //     }
    //     this.activeSessions = [];
    //     await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
    //   }
    // }

    console.log("Obtained uri via deeplink: " + uri);

    try {
      var newURI = new URL(uri);
      var handshakeTopic = newURI.pathname;
      if (handshakeTopic[handshakeTopic.length - 1] != '1') {
        // await this.presentToast(this._Translate.instant('WALLETCONNECT.version-error-title'), this._Translate.instant('WALLETCONNECT.version-error-message'));
        return this.processVersion2(uri, wallet);
        // wc:9637a89533e01592256e0afc25fe8973947f08b6c171cf1f7f6d89e38271a0cc@2?relay-protocol=irn&symKey=6139e39c4af2252b0e851d7a5bad990c9cbb7a472448dd1a6b4d7740592a0bb3
      }

    } catch (e) {
      console.log(e);
      // bridge = null;
    }

    const connectorObject: IWalletConnectOptions =
    // (!!bridge && !!handshakeTopic) ? {
    //   uri,
    //   bridge,
    //   // handshakeTopic,
    //   storageId: `walletconnect-${UtilityService.randomStringGenerator(6)}`,
    //   clientMeta: CLIENT_META,
    // } :
    {
      uri,
      storageId: `walletconnect-${UtilityService.randomStringGenerator(6)}`,
      clientMeta: CLIENT_META,
    };

    // const connectorName = UtilityService.randomStringGenerator(6);

    // eval('const ' + connectorName + ' = new WalletConnect(connectorObject, pushNotificationConfig )' );

    const connector = new WalletConnect(connectorObject, pushNotificationConfig);

    console.log("handShake from new WalletConnect object: " + connector.handshakeTopic);

    console.log(connector);
    console.log("Wallet Connect init");

    console.log("ACTION", "subscribeToEvents");

    if (connector) {

      // this.subscribeToEvents(connector, wallet);

      // session_request
      connector.on("session_request", (error, payload) => {

        console.log("Wallet Connect, session_request");
        console.log(JSON.stringify(payload));

        if (error) { throw error; }

        this._ModalController.create({
          component: WcSessionRequestComponent,
          componentProps: {
            data: payload,
            request: 'session_request',
            selfAddress: wallet.address,
            targetMerchantAccount: targetMerchantAccount
          }
        }).then(modal => {
          modal.onDidDismiss().then(async (dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              try {
                const net3 = ([
                  EthereumNetworks.MAINNET,
                  EthereumNetworks.KOVAN,
                  EthereumNetworks.RINKEBY,
                  EthereumNetworks.GOERLI,
                  EthereumNetworks.SEPOLIA,
                  EthereumNetworks.ROPSTEN].includes(dismissObject.data.networkValue)) ? new ethers.providers.EtherscanProvider(dismissObject.data.networkValue, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)) : ethers.getDefaultProvider(dismissObject.data.networkValue);
                const wa3 = wallet.connect(net3);
                var chainId = await wa3.getChainId();
                connector.approveSession({ chainId, accounts: [wallet.address] });
                this.activeSessions.push(connector);
                await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
              } catch (e) {
                console.log(e);
                // alert("approveSession ERROR");
              }
            } else {
              connector.rejectSession({ message: 'Session Request rejected' });
            }
          })
          modal.present();
        })

      });

      // session_update
      connector.on("session_update", (error, payload) => {

        console.log("Wallet Connect, session_update");
        console.log(JSON.stringify(payload));

        if (error) { throw error; }

        console.log("session_update: " + JSON.stringify(payload));

      });

      // call_request
      connector.on("call_request", async (error, payload) => {

        console.log("Wallet Connect, call_request");
        console.log(JSON.stringify(payload));

        console.log("EVENT: call_request: " + JSON.stringify(payload));
        if (error) { throw error; }

        // Example of Chain Change: call_request:
        // {
        //   "id":1653486949407,
        //   "jsonrpc":"2.0",
        //   "method":"wallet_switchEthereumChain",
        //   "params":[
        //     {
        //       "chainId":"0x4"
        //     }
        //   ]
        // }

        const { id, jsonrpc } = payload;

        if (payload.method == 'wallet_switchEthereumChain') {

          const newChainId = parseInt(payload.params[0].chainId);

          switch (newChainId) {
            case 508674158: var networkName = CryptoNetworkNames.EVAN_TEST as string; break;
            case 49262: networkName = CryptoNetworkNames.EVAN as string; break;
            case 80001: networkName = CryptoNetworkNames.POLYGON_TEST_MUMBAI as string; break;
            case 137: networkName = CryptoNetworkNames.POLYGON as string; break;
            case 100: networkName = CryptoNetworkNames.GNOSIS as string; break;
            case 4: networkName = CryptoNetworkNames.ETHEREUM_TEST_RINKEBY as string; break;
            case 1: networkName = CryptoNetworkNames.ETHEREUM as string; break;
            default: networkName = ''; break;
          }

          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'wallet_switchEthereumChain',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-3')} ${networkName}`,
                message: `${JSON.stringify(payload.params)}`,
              }
            }
          }).then(modal => {

            modal.onDidDismiss().then((dismissObject) => {
              console.log(dismissObject);
              var check = dismissObject.data.value;

              if (check) {

                connector.updateSession({ chainId: newChainId, accounts: [wallet.address] });
                this.presentToast(this._Translate.instant('WALLETCONNECT.chain-update-title'), `${this._Translate.instant('WALLETCONNECT.chain-update-success')} ${networkName} (${payload.params[0].chainId})`);

                var index = this.activeSessions.findIndex(c => c.uri == connector.uri);
                this.activeSessions[index] = connector;
                this.activeSessions[index]['targetMerchantAccount'] = targetMerchantAccount;
                this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions)).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                })
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }

        if (payload.method == 'eth_sign') {

          try {
            var signingString = `${this.cryptoProvider.hex2string(payload.params[1])}`;
          } catch (e) {
            signingString = `${JSON.stringify(payload.params)}`
          }


          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'eth_sign',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${signingString}`,
              }
            }
          }).then(modal => {
            modal.onDidDismiss().then((dismissObject) => {
              console.log(dismissObject);
              var check = dismissObject.data.value;
              if (check) {
                wallet.signMessage(`${signingString}`).then(result => {
                  console.log(`eth_sign response: ${JSON.stringify(result)}`);
                  connector.approveRequest({ id, jsonrpc, result });
                  this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                    this.navigateViaDeeplink(connector.peerMeta.url);
                  })
                }).catch(e => console.log(e));
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }

        if (payload.method == 'personal_sign') {
          console.log(connector);
          console.log(connector.peerMeta);


          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'personal_sign',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${this.cryptoProvider.hex2string(payload.params[0])}`,
              }
            }
          }).then(modal => {
            modal.onDidDismiss().then((dismissObject) => {
              console.log(dismissObject);
              var check = dismissObject.data.value;
              if (check) {
                wallet.signMessage(this.cryptoProvider.hex2string(payload.params[0])).then(result => {
                  console.log(`personal_sign response: ${JSON.stringify(result)}`);
                  connector.approveRequest({ id, jsonrpc, result });
                  this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                    this.navigateViaDeeplink(connector.peerMeta.url);
                  })
                }).catch(e => console.log(e));
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }

        if (payload.method == 'eth_signTypedData') {

          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'eth_signTypedData',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${payload.params[1]}`,
              }
            }
          }).then(modal => {
            modal.onDidDismiss().then((dismissObject) => {
              console.log(dismissObject);
              var check = dismissObject.data.value;
              if (check) {
                var signerObject = JSON.parse(payload.params[1]);
                delete signerObject.types['EIP712Domain'];
                // delete signerObject.types['RelayRequest'];
                wallet._signTypedData(signerObject.domain, signerObject.types, signerObject.message).then(result => {
                  console.log(`eth_signTypedData response: ${JSON.stringify(result)}`);
                  connector.approveRequest({ id, jsonrpc, result });
                  this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                    this.navigateViaDeeplink(connector.peerMeta.url);
                  })
                }).catch(e => console.log(e));
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }

        if (payload.method == 'helix_kyc') {

          console.log(payload);

          try {
            var signingString = `${this.cryptoProvider.hex2string(payload.params[1])}`;
          } catch (e) {
            signingString = `${JSON.stringify(payload.params)}`
          }

          console.log(payload.params[1]);

          var basicdataKeys = payload.params[1]['basicdata'];
          var trustKeys = payload.params[1]['trust'];
          var minAgeCheck = payload.params[1]['minAgeCheck'];
          var transferData = {
            timestamp: +new Date(),
            basicdata: {},
            trust: {},
            minAgeCheck
          };

          var userData = await this._UserProviderService.getUser();

          for (var key1 of basicdataKeys) {
            transferData['basicdata'][key1] = userData[key1];
          }

          for (var key2 of trustKeys) {
            transferData['trust'][key2] = (userData[`${key2}Status`] == 1);
          }

          if (minAgeCheck) {
            var getAge = function (dateString) {
              var today = new Date();
              var birthDate = new Date(dateString);
              var age = today.getFullYear() - birthDate.getFullYear();
              var m = today.getMonth() - birthDate.getMonth();
              if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
              }
              return age;
            }
            var age = getAge(userData.dateofbirth);
            transferData['minAgeCheck'] = (userData.dateofbirthStatus == 1 && age >= 18);
          }

          const modal = await this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'helix_kyc',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${JSON.stringify(transferData)}`,
              }
            }
          });
          modal.onDidDismiss().then((dismissObject) => {
            console.log(dismissObject);
            var check = dismissObject.data.value;
            if (check) {
              wallet.signMessage(JSON.stringify(transferData)).then(result => {
                console.log(`helix_kyc response: ${JSON.stringify(result)}`);

                var gg = Object.assign(transferData, { signature: result })
                connector.approveRequest({ id, jsonrpc, result: JSON.stringify(gg) });

                this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                  this.navigateViaDeeplink(connector.peerMeta.url);
                })
              }).catch(e => console.log(e));
            } else {
              connector.rejectRequest({
                id, error: {
                  code: 0,
                  message: 'Request was rejected by the user'
                }
              });
            }
          })
          await modal.present();
        }

        if (payload.method == 'eth_sendTransaction') {

          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'eth_sendTransaction',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${JSON.stringify(payload.params)}`,
              }
            }
          }).then(modal => {
            modal.onDidDismiss().then(async (dismissObject) => {
              console.log(dismissObject);

              const CRYPTONETWORKS_POLYGON = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON);
              const CRYPTONETWORKS_GNOSIS = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_GNOSIS);

              var check = dismissObject.data.value;
              if (check) {

                switch (dismissObject.data.networkName) {
                  case CryptoNetworkNames.POLYGON:
                    var network = ethers.getDefaultProvider(CRYPTONETWORKS_POLYGON);
                    var gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC);
                    var currency = CryptoCurrency.MATIC;
                    break;
                  case CryptoNetworkNames.GNOSIS:
                    network = ethers.getDefaultProvider(CRYPTONETWORKS_GNOSIS);
                    gasPrice = await this._Gas.getGasPriceEstimationXDAI();
                    currency = CryptoCurrency.GNOSIS;
                    break;
                  case CryptoNetworkNames.ETHEREUM:
                    network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                    gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH);
                    currency = CryptoCurrency.ETH;
                    break;
                  default:
                    network = new ethers.providers.EtherscanProvider(EthereumNetworks.MAINNET, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                    gasPrice = await this._Gas.returnScanGasPrice(CryptoCurrency.ETH);
                    currency = CryptoCurrency.ETH;
                    break;
                }

                console.log({ network, gasPrice, currency });
                var normalGasPrice = Number(ethers.utils.formatEther(ethers.utils.parseUnits(gasPrice.normal, gasPrice.unit)._hex));
                var factor = GAS_USED[currency];
                var cleanedGasPrice = (normalGasPrice / factor).toFixed(10);
                var utilsGasPrice = ethers.utils.parseEther(cleanedGasPrice);

                console.log({ normalGasPrice, factor, cleanedGasPrice, utilsGasPrice });

                const walletProvider = wallet.connect(network);

                var { from, to, data } = payload.params[0];
                var txObject = {
                  from, to, data,
                  nonce: await network.getTransactionCount(from, "latest"),
                  // gasLimit: ethers.utils.hexlify(21000),
                  gasPrice: await network.getGasPrice() // utilsGasPrice
                }

                console.log("txObject", txObject);


                walletProvider.sendTransaction(txObject).then(result => {
                  console.log(`eth_sendTransaction response: ${JSON.stringify(result)}`);
                  connector.approveRequest({ id, jsonrpc, result: result.hash });
                  this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.hash.substring(0, 8)}...`).then(() => {
                    this.navigateViaDeeplink(connector.peerMeta.url);
                  })
                }).catch(err => {
                  console.log(err);
                  try {
                    var key = err.toString().slice(err.toString().indexOf("code=") + 5).split(",")[0];
                    this.presentToast('', this._Translate.instant(`ETHERRORS.${key}`));
                  } catch (ef) {
                    console.log(ef);
                  }
                });
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }

        if (payload.method == 'eth_signTransaction') {

          this._ModalController.create({
            component: WcSessionRequestComponent,
            componentProps: {
              data: { params: [connector] },
              request: 'eth_signTransaction',
              selfAddress: wallet.address,
              targetMerchantAccount: targetMerchantAccount,
              additionalTexts: {
                header: `${this._Translate.instant('WALLETCONNECT.modal-sign-request-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.modal-sign-request-2')}`,
                message: `${JSON.stringify(payload.params)}`,
              }
            }
          }).then(modal => {
            modal.onDidDismiss().then((dismissObject) => {
              console.log(dismissObject);
              var check = dismissObject.data.value;
              if (check) {
                wallet.signTransaction(payload.params[0]).then(result => {
                  console.log(`eth_signTransaction response: ${JSON.stringify(result)}`);
                  connector.approveRequest({ id, jsonrpc, result });
                  this.presentToast(this._Translate.instant('WALLETCONNECT.signature'), `${result.substring(0, 8)}...`).then(() => {
                    this.navigateViaDeeplink(connector.peerMeta.url);
                  })
                }).catch(e => console.log(e));
              } else {
                connector.rejectRequest({
                  id, error: {
                    code: 0,
                    message: 'Request was rejected by the user'
                  }
                });
              }
            })
            modal.present();
          })

        }
      });

      connector.on("connect", (error, payload) => {

        console.log("Wallet Connect, connect");
        console.log(JSON.stringify(payload));

        if (error) { throw error; }
        this.presentToast(this._Translate.instant('WALLETCONNECT.success-title'), `${this._Translate.instant('WALLETCONNECT.success-message-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.success-message-2')}`).then(async () => {
          targetMerchantAccount = payload.params[0].accounts[0];
          var index = this.activeSessions.findIndex(k => k.uri = connector.uri);
          this.activeSessions[index]['targetMerchantAccount'] = targetMerchantAccount;
          this.activeSessions[index]['backupUri'] = uri;
          this.activeSessions[index]['connectedAccount'] = !!requestAddress ? requestAddress : verifiedWalletAddress;
          console.log("this.activeSessions", this.activeSessions);
          await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
          this.navigateViaDeeplink(payload.params[0].peerMeta.url);
        })
      });

      connector.on("disconnect", (error, payload) => {
        console.log(payload);
        if (error) { throw error; }
        this.presentToast(this._Translate.instant('WALLETCONNECT.session-ended-title'), `${this._Translate.instant('WALLETCONNECT.session-ended-message-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.session-ended-message-2')}`).then(() => {
          connector.killSession(payload.params[0]).then(async () => {
            var index = this.activeSessions.findIndex(k => k['_sessionStorage']['storageId'] == connector['_sessionStorage']['storageId']);
            this.activeSessions.splice(index, 1);
            await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
          }).catch(async e => {
            var index = this.activeSessions.findIndex(k => k['_sessionStorage']['storageId'] == connector['_sessionStorage']['storageId']);
            this.activeSessions.splice(index, 1);
            await this._SecureStorageService.setValue(SecureStorageKey.walletConnectSessions, JSON.stringify(this.activeSessions));
          })

        })
      });

    }

    console.log(connector.connected);

    if (!connector.connected) {
      await connector.createSession();
      console.log("Wallet Connect session created");
    }

  }

  public async returnWalletAddressesNames(sourceData: string) {
    var verifiedWalletName = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletName, false);
    var verifiedWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    var walletListString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    var walletList = !!walletListString ? JSON.parse(walletListString) : [];
    var buttons = [];
    buttons.push({
      text: verifiedWalletName,
      handler: async () => {
        await Clipboard.write({ string: verifiedWalletAddress });
        this.presentToast("", this._Translate.instant('WALLET.addressCopied'));
        this.iab.create(sourceData, "_system");
      }
    });
    var walletListWithMnemonic = walletList.filter(wl => !!wl.mnemonic);
    for (let i = 0; i < walletListWithMnemonic.length; i++) {
      buttons.push({
        text: walletListWithMnemonic[i].heading,
        handler: async () => {
          await Clipboard.write({ string: walletListWithMnemonic[i].publicKey });
          this.presentToast("", this._Translate.instant('WALLET.addressCopied'));
          this.iab.create(sourceData, "_system");
        }
      })
    }
    return buttons;
  }

  public async processDeeplink(receivedResponse: string, deepLink: boolean) {
    var verifiedWalletName = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletName, false);
    var verifiedWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    var walletListString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    var walletList = !!walletListString ? JSON.parse(walletListString) : [];

    if (deepLink) {
      this.deepLinkNavigation = true;
    }

    if (walletList.length == 0) {
      this.init(receivedResponse);
    } else {
      var buttons: any = Array.from(walletList, (k: any) => {
        return {
          text: k.heading,
          handler: () => {
            this.init(receivedResponse, k.publicKey);
          }
        }
      })
      buttons.push({
        text: verifiedWalletName,
        handler: () => {
          this.init(receivedResponse, verifiedWalletAddress);
        }
      });
      buttons.reverse();
      buttons.push({
        text: this._Translate.instant('BUTTON.CANCEL'),
        role: 'cancel',
        handler: () => { }
      });
      this._AlertController.create({
        mode: 'ios',
        header: 'Wallets',
        message: this._Translate.instant('WALLETSETTINGS.select-wallet'),
        buttons
      }).then(alerto => {
        alerto.present();
      })
    }
  }

  private presentToast(header: string, message: string) {
    return this._ToastController.create({
      header: header,
      message: message,
      duration: 3000,
      position: 'top'
    }).then(toast => toast.present());
  }

  public setNavigationViaDeeplink() {
    this.deepLinkNavigation = true;
  }

  private navigateViaDeeplink(url: string) {
    console.log("this.deepLinkNavigation", this.deepLinkNavigation);
    console.log("this.deepLinkNavigation: url: ", url);
    if (this.deepLinkNavigation) {
      try {
        this.deepLinkNavigation = false;
        this.iab.create(url, '_system');
        // var a = document.createElement("a");
        // a.href = url;
        // a.target = "_blank";
        // a.style.display = "none";
        // a.click();
        // a.remove();
      } catch (e) {
        console.log('cannot go back');
      }
    }
  }

}
