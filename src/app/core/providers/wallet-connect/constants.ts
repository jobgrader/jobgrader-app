export enum CryptoNetworkNames {
    THXC = "Thanks Coin (Polygon Amoy)",
    HMT_POLYGON = "Human Token (Polygon)",
    HMT_SKALE = "Human Token (SKALE)",
    POLYGON_TEST_MUMBAI = "Polygon Mumbai-Testnet",
    POLYGON_TEST_AMOY = "Polygon Amoy-Testnet",
    POLYGON = "Polygon Mainnet",
    EVAN_TEST = "EPN Testnet",
    EVAN = "EPN Mainnet",
    ETHEREUM_TEST_RINKEBY = "Ethereum Testnet - rinkeby",
    ETHEREUM_TEST_KOVAN = "Ethereum Testnet - kovan",
    ETHEREUM_TEST_ROPSTEN = "Ethereum Testnet - ropsten",
    ETHEREUM = "Ethereum Mainnet",
    GNOSIS = "Gnosis Chain",
    // Fixed
    ARBITRUM = "Arbitrum Mainnet", // Arbitrum
    ARBITRUM_GOERLI_TESTNET = "Arbitrum Goerli Testnet", // Arbitrum Goerli Testnet
    BLUE = "Blue Token", // Blue token
    BNB_SMART = "BNB Smart Chain", // BNB Smart Chain and BNB Beacon Chain
    BNB_BEACON = "BNB Beacon Chain", // BNB Smart Chain and BNB Beacon Chain
    MAKER_DAO = "Maker Dao", // MakerDAO
    MULTIVERSX = "MultiversX", // MultiversX
    NEAR = "Near Token", // Near
    OPTIMISM = "Optimism Token", // Optimism
    TEZOS = "Tezos Token", // Tezos
    UNISWAP = "Uniswap", // Uniswap
    // Optional
    AAVE = "Aave",
    AE = "Aeternity",
    AION = "Aion",
    APE = "Apecoin",
    AURORA = "Aurora",
    AVAX = "Avalanche",
    AXS = "Axie Infinity",
    BUSD = "Binance USD",
    CELO = "Celo",
    LINK = "ChainLink",
    CRO = "Cronos",
    MANA = "Decentraland Token",
    DOGE = "Dogecoin",
    DYDX = "dYdX",
    ENS = "Ethereum Name Service",
    FTM = "Fantom Token",
    FIL = "Filecoin",
    FLOW = "Flow",
    IMX = "Immutable X",
    CAKE = "PancakeSwap Token",
    SAND = "Sandbox Token",
    TRON = "Tron Token",
    TWT = "Trust Wallet Token",
}

export enum EthereumNetworks {
    RINKEBY = "rinkeby",
    KOVAN = "kovan",
    ROPSTEN = "ropsten",
    GOERLI = "goerli",
    SEPOLIA = "sepolia",
    MAINNET = "mainnet"
}

export enum CryptoCurrency {
    THXC = "THXC",
    HMT_POLYGON = "HMT_POLYGON",
    HMT_SKALE = "HMT_SKALE",
    MATIC = "POL",
    MATIC_AMOY = "MATIC",
    EVE = "EVE",
    ETH = "ETH",
    GNOSIS = "XDAI",
    // Fixed
    ARETH = "ARETH", // Arbitrum
    BLUR = "BLUR", // Blue token
    BNB_BEACON = "BNB_BEACON", // BNB Smart Chain and BNB Beacon Chain
    BNB_SMART = "BNB_SMART", // BNB Smart Chain and BNB Beacon Chain
    MKR = "MKR", // MakerDAO
    EGLD = "EGLD", // MultiversX
    NEAR = "NEAR", // Near
    OP = "OP", // Optimism
    XTZ = "XTZ", // Tezos
    UNI = "UNI", // Uniswap
    // Optional
    AAVE = "AAVE", // Aave
    AE = "AE", // Aeternity
    AION = "AION", // Aion
    APE = "APE", // Apecoin
    AURORA = "AURORA", // Aurora
    AVAX = "AVAX", // Avalanche
    AXS = "AXS", // Axie Infinity
    BUSD = "BUSD", // Binance USD
    CELO = "CELO", // Celo
    LINK = "LINK", // ChainLink
    CRO = "CRO", // Cronos
    MANA = "MANA", // Decentraland Token
    DOGE = "DOGE", // Dogecoin
    DYDX = "DYDX", // dYdX
    ENS = "ENS", // Ethereum Name Service
    FTM = "FTM", // Fantom Token
    FIL = "FIL", // Filecoin
    FLOW = "FLOW", // Flow
    IMX = "IMX", // Immutable X
    CAKE = "CAKE", // PancakeSwap Token
    SAND = "SAND", // Sandbox Token
    TRON = "TRON", // Tron Token
    TWT = "TWT", // Trust Wallet Token
}

export enum CryptoCurrencyColors {
    THXC = "#FF9C00",
    HMT_POLYGON = "#4B00FF",
    HMT_SKALE = "#4B00FF",
    MATIC = "#8247E5",
    MATIC_AMOY = "#8247E5",
    EVE = "#003399",
    ETH = "#8C8C8C",
    XDAI = "#48A9A6",
    // Fixed
    ARETH = "#269FF0", // Arbitrum
    BLUR = "#D04C18", // Blur token
    BNB = "#F0B90E", // BNB Smart Chain and BNB Beacon Chain
    MKR = "#52AC9E", // MakerDAO
    EGLD = "#22F7DD", // MultiversX
    NEAR = "#D6D6D6", // Near
    OP = "#FE0221", // Optimism
    XTZ = "#0263FF", // Tezos
    UNI = "#EC83B8", // Uniswap
    // Optional
    AAVE = "#35B3C3", // Aave
    AE = "#CF4471", // Aeternity
    AION = "#000000", // Aion
    APE = "#0649D4", // Apecoin
    AURORA = "#6FD24D", // Aurora
    AVAX = "#E84042", // Avalanche
    AXS = "#04A1F0", // Axie Infinity
    BUSD = "#F0B90E", // Binance USD
    CELO = "#FCFE53", // Celo
    LINK = "#325DD1", // ChainLink
    CRO = "#2B3865", // Cronos
    MANA = "#FF6B58", // Decentraland Token
    DOGE = "#B39135", // Dogecoin
    DYDX = "#171523", // dYdX
    ENS = "#6B9DF6", // Ethereum Name Service
    FTM = "#1969FF", // Fantom Token
    FIL = "#0190FF", // Filecoin
    FLOW = "#03EF8A", // Flow
    IMX = "#464849", // Immutable X
    CAKE = "#D1884F", // PancakeSwap Token
    SAND = "#00AEEE", // Sandbox Token
    TRON = "#FF0112", // Tron Token
    TWT = "#3375BB", // Trust Wallet Token
}

export enum Explorers {
    EVAN_TEST = "https://testexplorer.evan.network/tx/",
    EVAN = "https://explorer.evan.network/tx/",
    POLYGON_TEST_MUMBAI = "https://mumbai.polygonscan.com/tx/",
    POLYGON_TEST_AMOY = "https://amoy.polygonscan.com/tx/",
    POLYGON = "https://polygonscan.com/tx/",
    GNOSIS = "https://blockscout.com/xdai/mainnet/tx/",
    ETHEREUM = "https://etherscan.io/tx/",
    ETHEREUM_TEST_RINKEBY = "https://rinkeby.etherscan.io/tx/",
    CELO = "https://explorer.celo.org/mainnet/tx/",
    // Fixed
    ARBITRUM = "Arbitrum Mainnet", // Arbitrum
    ARBITRUM_GOERLI_TESTNET = "Arbitrum Goerli Testnet", // Arbitrum Goerli Testnet
    BLUE = "Blue Token", // Blue token
    BNB_SMART = "BNB Smart Chain", // BNB Smart Chain and BNB Beacon Chain
    BNB_BEACON = "BNB Beacon Chain", // BNB Smart Chain and BNB Beacon Chain
    MAKER_DAO = "Maker Dao", // MakerDAO
    MULTIVERSX = "MultiversX", // MultiversX
    NEAR = "Near Token", // Near
    OPTIMISM = "Optimism Token", // Optimism
    TEZOS = "Tezos Token", // Tezos
    UNISWAP = "Uniswap", // Uniswap
    // Optional
}
