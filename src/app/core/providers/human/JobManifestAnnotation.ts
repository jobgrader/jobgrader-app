export interface JobManifestAnnotations {
    payout: number;
    payoutUnit: string;
    workTime: number;
    workTimeUnit: string;
    workTimeLimit: number;
    workTimeLimitUnit: string;
    payoutDuration: number;
    payoutDurationUnit: string;
    regions: string[];
    age: number;
    gender: 'm' | 'f' | 'd' | 'u';
    targetGroup: number;
    personalData: -1 | 0 | 1;
    publication: -1 | 0 | 1;
    scope: number;
    information: string;
    kyc?: boolean;
    certificates?: string[];
    audio?: {
        min?: number;
        max?: number;
    },
    video?: {
        min?: number;
        max?: number;
    },
}