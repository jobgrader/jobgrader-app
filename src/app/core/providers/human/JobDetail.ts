import { JobManifest } from "./JobManifest";


export interface JobDetail {
    escrowAddress: string;
    chainId: number;
    manifest: JobManifest;
}
