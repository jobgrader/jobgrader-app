import { JobManifestAnnotations } from "./JobManifestAnnotation";

export interface JobManifest {
    chainId: number;
    requesterTitle: string;
    requesterDescription: string;
    imageUrl: string;
    dataUrl: string;
    submissionsRequired: number;
    fundAmount: number;
    type: string;
    requestType: string;
    annotations: JobManifestAnnotations;
}
