import { Injectable } from '@angular/core';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { UserProviderService } from '../user/user-provider.service';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { JobDetail } from './JobDetail';

const HUMAN_ACCOUNTS = "https://foundation-yellow-accounts.hmt.ai";
const HUMAN_EXCHANGE = "https://foundation-yellow-exchange.hmt.ai";

const api_key = ""; // Fetch from Vault

export interface FortuneJob {
  address: string;
  chainId: number;
  title: string;
  description: string;
  token: string;
  estimatedPrice: number;
  solutionsRequired: number;
  type: number;
}

export interface PriceGuessJob {
  escrowAddress: string;
  chainId: number;
  manifest: Manifest;
}

export interface Manifest {
  chainId: number;
  requesterTitle: string,
  requesterDescription: string;
  imageUrl: string;
  dataUrl: string;
  fundAmount: number;
  requestType: string;
}

@Injectable({
  providedIn: 'root'
})
export class HumanService {

  constructor(
    private _UserProviderService: UserProviderService,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private http: HttpClient
  ) { }

  ObtainSiteKeys(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._UserProviderService.getUser().then(user => {
        console.log(user);
        this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then(eth_addr => {
          var email = user.email;
          var country = user.country;
          var userId = user.userid;
          console.log(eth_addr);
          var body = { email, language: 'en', country: "DE", eth_addr, userId };
          console.log(body);
          this.http.post(`${HUMAN_ACCOUNTS}/labeler/register`,
            body ,
            { params: { api_key } }
          )
          .subscribe({
            next: (data) => {
              console.log(data);
              resolve(data);
            },
            error: (error) => {
              reject(error);
            }
          });
        })
      })
    })
  }

  VerifyCaptcha( secret: string, sitekey: string, response: string, remoteip: string ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${HUMAN_EXCHANGE}/siteverify`, null, {
        params: {
          secret, // : 0xF02479585571241EA12996B27F996dAf4f9Bb32A, // secret key from test_enterprise_publisher@test.com acc
          sitekey, // : "30564186-e300-473b-a21d-f3411e7a21c2", // sitekey
          response, // : token, // token from solving captcha
          remoteip // : ip address
        }
      })
      .subscribe({
        next: (data) => {
          console.log(data);
          resolve(data);
        },
        error: (error) => {
          console.log(error);
          reject(error);
        }
      });
    })
  }

  RegisterLebeller( email: string, language: string, country: string, eth_addr: string, userId: string, api_key: string ): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${HUMAN_ACCOUNTS}/labeler/register`, {
        email,
        language, //: 'en',
        country,
        eth_addr,
        userId
      }, {
        params: {
          api_key
        }
      })
      .subscribe({
        next: (data) => {
          console.log(data);
          resolve(data);
        },
        error: (error) => {
          console.log(error);
          reject(error);
        }
      });
    })
  }

  RetrieveLebellerStats(email: string, api_key: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${HUMAN_ACCOUNTS}/support/labeler/${email}`, {
        params: {
          api_key
        }
      })
      .subscribe({
        next: (data) => {
          console.log(data);
          resolve(data);
        },
        error: (error) => {
          console.log(error);
          reject(error);
        }
      });
    })
  }

  ShowCaptcha() {
    var hcaptcha = ((window as any).hcaptcha);
    hcaptcha.render(`h-captcha`, {
        sitekey: '', // Fetch from Vault
        theme: 'dark',
        'error-callback': 'onError',
      }).then(() => {
        hcaptcha.execute(`#h-captcha-execute`, { async: true }).then(({ response, key}) => {
            console.log(response, key);
        })
      }).catch(() => {
        hcaptcha.execute(`#h-captcha-execute`, { async: true }).then(({ response, key}) => {
            console.log(response, key);
        })
      })
  }

  presentToast(message: string) {
    this._ToastController.create({ message, duration: 2000, position: 'top' }).then((toast) => {
      toast.present();
    })
  }

  listJobs(chainId: number, workerAddress: string, type: string[]): Observable<any> {

    const queryString = type.map(type => `&jobType=${encodeURIComponent(type)}`).join('');

    return this.http.get<any>(`${environment.hidExchange.url}/job?page=0&pageSize=10&chainId=${chainId}&status=ACTIVE` + queryString, {
        headers: {

        }
    });
  }

  getJobDetails(chainId: number, escrowAddress: string): Observable<JobDetail> {

    return this.http.get<JobDetail>(`${environment.hidExchange.url}/job/details/${chainId}/${escrowAddress}`, {
        headers: {

        }
    });
  }

  assignment(chainId: number, escrowAddress: string, workerAddress: string): Observable<any> {
    const body = {
      chainId,
      escrowAddress,
      workerAddress
    }
    return this.http.post(`${environment.hidExchange.url}/assignment`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  getAssignment(chainId: number, escrowAddress: string, workerAddress: string): Observable<any> {

    return this.http.get<any>(`${environment.hidExchange.url}/assignment?page=0&pageSize=5&chainId=${chainId}&escrowAddress=${escrowAddress}&status=ACTIVE&workerAddress=${workerAddress}`, {
        headers: {

        }
    });
  }

  downloadJson(url: string): Observable<any> {

    return this.http.get<any>(url, {
        headers: {

        }
    });
  }

  solve(assignmentId: string, solution: string) {
    const body = {
      assignmentId,
      solution
    }
    return this.http.post(`${environment.hidExchange.url}/job/solve`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  createPresignedUrl(assignmentId: string, extension: string, mimeType: string) {
    const body = {
      assignmentId,
      extension,
      mimeType
    }
    return this.http.post(`${environment.hidExchange.url}/job/create-presigned-url`, body, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  uploadMedia(url: string, base64: string) {

    // use the pre-signed URL to upload the file to S3
    const formData = new FormData();
    formData.append('file', base64);


    return this.http.put(url, formData, {
      reportProgress: true,
      observe: 'events',
      headers: {
      }
    });
  }
}
