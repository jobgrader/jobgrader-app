import { Injectable } from '@angular/core';
import { DarkMode, IsDarkModeResult } from '@aparajita/capacitor-dark-mode';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { Settings, SettingsService } from '../settings/settings.service';
import { EventsService, EventsList } from '../events/events.service';
import { AppStateService } from '../app-state/app-state.service';

export enum Themes {
  Dark = 'dark',
  Light = 'light',
  System = 'system'
}

@Injectable({
  providedIn: 'root'
})
export class ThemeSwitcherService {

  private currentTheme: Themes.Dark | Themes.Light;

  public availableThemes = [
    Themes.System,
    Themes.Dark,
    Themes.Light,
  ]

  constructor(
    private secureStorage: SecureStorageService,
    private setting: SettingsService,
    private _EventsService: EventsService,
    private _AppStateService: AppStateService
  ) {


  }

  themeEventSubscription() {
    this._EventsService.subscribe(EventsList.themeChange, (theme) => {
      console.log(theme);
      this.setTheme(theme);
    })
  }

  getCurrentTheme() {

    return this.currentTheme;

  }

  saveTheme(theme: string) {

    if (this._AppStateService.basicAuthToken) {
      try {
        this.setting.set(Settings.themeMode, theme, true);
      } catch (error) {
        this.secureStorage.setValue(SecureStorageKey.themeMode, theme);
      }
    } else {
      this.secureStorage.setValue(SecureStorageKey.themeMode, theme);
    }

  }

  async getSavedTheme(): Promise<string> {
    const themeName = await this.secureStorage.getValue(SecureStorageKey.themeMode, false);
    if (themeName) {
      return themeName;
    }
    else {
      return Themes.System;
    }

  }

  setSavedTheme(): void {
    this.secureStorage.getValue(SecureStorageKey.themeMode, false).then((themeName: string) => {
      if (themeName) {
        this.setTheme(themeName);
      }
      else {
        this.setTheme(Themes.System);
      }
    });
  }

  setTheme(themeName: string): void {
    console.info('ThemeSwitcher#setTheme; themeName:', themeName);

    if (themeName == 'system') {

      this.setSystemTheme();

    } else {

      if (themeName === Themes.Dark) {

        this.setDarkTheme();

      } else if (themeName === Themes.Light) {

        this.setLightTheme();

      } else {

        this.setSystemTheme();

      }

    }

    //Save the newly changed theme
    this.saveTheme(themeName);

  }

  private setSystemTheme() {

    DarkMode.isDarkMode()
      .then(
        (res: IsDarkModeResult) => {
          if (res.dark) {

            this.setDarkTheme();

          } else {

            this.setLightTheme();

          }
        }
      )
      .catch(
        (error) => {
          console.error(error);

          this.setLightTheme();

        }
      );

  }

  private setDarkTheme() {
    DarkMode.update({ dark: true });
    this.currentTheme = Themes.Dark;
  }

  private setLightTheme() {
    DarkMode.update({ dark: false });
    this.currentTheme = Themes.Light;
  }
}


