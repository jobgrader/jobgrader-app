import { Injectable } from '@angular/core';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TimesService {

  constructor(
  ) { }

  /** Getter for the birthday max */
  public get birthdayMax(): string {
    const max = moment().subtract(18, 'year').format('YYYY-MM-DD');
    return max;
  }

  /** Getter for the birthday max */
  public get today(): string {
    const max = moment().format('YYYY-MM-DD');
    return max;
  }

}
