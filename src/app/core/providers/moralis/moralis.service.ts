import { Injectable } from '@angular/core';
import Identicon from 'identicon.js';
// import Moralis from 'moralis';
// import { Core } from '@moralisweb3/common-core';
// import { EvmApi } from '@moralisweb3/evm-api';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { CryptoCurrency } from '../wallet-connect/constants';
import { AlchemyNfts, NftDisplayElements2 } from 'src/app/nft/nft.page';
import { HttpClient } from '@angular/common/http';
import { SecureStorageKey } from '@services/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from '@services/secure-storage/secure-storage.service';
import { environment } from 'src/environments/environment';
import { IpfsService } from '@services/ipfs';

const MORALIS_BASE = "https://deep-index.moralis.io/api/v2";

export interface GnosisNft {
  amount: string;
  block_number: string;
  block_number_minted: any;
  collection_banner_image: any;
  collection_logo: any;
  contract_type: string;
  last_metadata_sync: string;
  last_token_uri_sync: string;
  metadata: any;
  minter_address: string;
  name: string;
  owner_of: string;
  possible_spam: boolean;
  symbol: string;
  token_address: string;
  token_hash: string;
  token_id: string;
  token_uri: string;
  verified_collection: boolean;
}

export interface MoralisMetadataResponse {
  token_address: string;
  token_id: string;
  amount: string;
  owner_of: string;
  token_hash: string;
  block_number_minted: string;
  block_number: string;
  transfer_index: Array<number>;
  contract_type: string;
  name: string;
  symbol: string;
  token_uri: string;
  metadata: string;
  last_token_uri_sync: string;
  last_metadata_sync: string;
}

export interface NFTHistory {
  from: string;
  to: string;
  fromImage: string;
  toImage: string;
  amount: string;
  value: string;
  timestamp: any;
}

export interface CelebrityNft {
  name: string;
  address: string;
  profileImage: string;
}

export interface CelebrityNftDetails {
  name: string;
  displayImage: string;
  network: string;
  descriptionText: string;
  creationDate: number;
  modificationDate: number;
  contractAddress: string;
  tokenId: string;
  rank: number;
  internalInformation: any;
  releaseStatus: boolean;
  visibility: boolean;
}

interface MoralisResponse {
  total: number;
  page: number;
  page_size: number;
  cursor: number;
  result: Array<MoralisResult>;
  status: string;
}

interface MoralisResult {
  token_address: string;
  token_id: string;
  owner_of: string;
  block_number: string;
  block_number_minted: string;
  token_hash: string;
  amount: string;
  contract_type: string;
  name: string;
  symbol: string;
  token_uri: string;
  metadata: string;
  last_token_uri_sync: string;
  last_metadata_sync: string;
}

@Injectable({
  providedIn: 'root'
})
export class MoralisService {


  private supportedChains = [
    { name: "eth"	, chain_hex: 0x1	, chain_int: 1, currency: CryptoCurrency.ETH },
    // { name: "goerli"	, chain_hex: 0x5	, chain_int: 5 },
    { name: "polygon"	, chain_hex: 0x89	, chain_int: 137, currency: CryptoCurrency.MATIC },
    // { name: "mumbai"	, chain_hex: 0x13881	, chain_int: 80001 },
    { name: "bsc"	, chain_hex: 0x38	, chain_int: 56, currency: CryptoCurrency.BNB_SMART  },
    // { name: "bsc testnet"	, chain_hex: 0x61	, chain_int: 97 },
    { name: "avalanche"	, chain_hex: 0xA86A	, chain_int: 43114, currency: CryptoCurrency.AVAX },
    // { name: "avalanche testnet"	, chain_hex: 0xA869	, chain_int: 43113 },
    { name: "fantom"	, chain_hex: 0xFA	, chain_int: 250, currency: CryptoCurrency.FTM },
    { name: "cronos"	, chain_hex: 0x19	, chain_int: 25, currency: CryptoCurrency.CRO },
    // { name: "cronos testnet"	, chain_hex: 0x152	, chain_int: 338 }
  ]

  // private evmApi: EvmApi;

  constructor(
    private _HttpClient: HttpClient,
    private _Vault: VaultService,
    private _SecureStorage: SecureStorageService,
    private ipfsService: IpfsService,
  ) {}

   async init() {
    const MORALIS_API_KEY = await this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY);
    try {
      const Moralis = (await import('moralis')).default;
      await Moralis.start({
        apiKey: MORALIS_API_KEY,
      });
      
    } catch(e) {
      console.log(e);
    }

   }

   async getBalanceMoralis(address: string, currency: string) {
    var ob = this.supportedChains.find(sc => sc.currency == currency);
    const Moralis = (await import('moralis')).default;
    const balance = await Moralis.EvmApi.balance.getNativeBalance({ address, chain: ob.chain_hex });
    console.log(currency, balance.raw.balance);
    return balance.raw.balance;
   }

   httpsRequest(option: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.request(option).subscribe({
        next: (res) => {
          resolve(res);
        },
        error: (err) => {
          reject(err);
        }
      })
    })
   }


  fetchNfts(address: string): Promise<Array<any>> {
    return new Promise(async (resolve, reject) => {
        await this.init();

        const chains = ["0x64", "0x27d8"];

        // layz load moarlis library for smalle bundle size
        const Moralis = (await import('moralis')).default;

        // for performance reason we are using Promise.all to fetch all the NFTs
        // from different chains in parallel
        const promises = [];

        for(const chain of chains) {
          const options: any = {
            "chain": chain,
            "format": "decimal",
            "mediaItems": true,
            "address": address
          }
          promises.push(
            new Promise(async (resolve, reject) => {
              try {
                const response = await Moralis.EvmApi.nft.getWalletNFTs(options);
                resolve(response.raw.result);
              } catch (error) {
                 resolve([]);
              }

            })
          );
        }

        const values: Array<any>[] = await Promise.all(promises);

        // flat the promises responses which is a [][] array to []
        resolve(values.flat());
      })
  }

  

  

  safeParse(o) {
    let oo;
    try {
      oo = JSON.parse(o);
    } catch (error) {
      oo = o;
    }
    return oo;
  }

  async processDisplayElements(ob: any) {

    let image_uri = null;

    if(ob.token_uri) {
      try {
        // we use fetch instead of http module becuase it has
        // les restrictions like for CORS
        const r = await fetch(ob.token_uri);
        const r1 = await r.json();
        if(r1.imageUrl) {
          image_uri = r1.imageUrl;
        } else if(r1.image_uri) {
          image_uri = r1.image_uri;
        }
      } catch (error) {
        console.log(error);
      }
    }

    let metadata = this.safeParse(ob.metadata);

    var s = <AlchemyNfts>{
      is_spam: ob.possible_spam,
      balance: ob.amount,
      token_id: ob.token_id,
      name: !!metadata ? metadata.name : ob.name,
      description: !!ob.metadata ? (!!metadata ? metadata.description : null) : null,
      image_uri: !!ob.image_uri ? ob.image_uri : (!!image_uri ? image_uri : (!!ob.metadata ? (!!metadata ? metadata.image : null) : null)),
      token_uri: ob.token_uri,
      token_standard: ob.contract_type,
      collectionName: ob.symbol,
      contract: ob.token_address,
      created_at: ob.last_metadata_sync,
      updated_at: ob.last_token_uri_sync,
      attributes: !!ob.metadata ? (!!metadata ? metadata.attributes : null) : null,
    }

    // IMPORTANT: we change the ipfs uri which is not supported on browser to our default ipfs gateway uri
    if (s.image_uri?.startsWith('ipfs://')) {

      s.image_uri = s.image_uri.replace('ipfs://', this.ipfsService.ipfsGateway);

    // IMPORTANT: this check for now is a just aworkaround because our old soulbound nft token
    // does not use the standard ipfs uri format
    } else if (s.image_uri?.startsWith('https://ipfs.io/ipfs/')) {

      s.image_uri = s.image_uri.replace('https://ipfs.io/ipfs/', this.ipfsService.ipfsGateway);

    }

    // console.log(s);
    return s;
  }

  fetchNftMetadata(contractAddress: string, tokenId: string): Promise<NftDisplayElements2> {

    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        this.httpsRequest(options).then(response => {
          let rr = this.processMetadataResponse(response);
          resolve(rr);
        }).catch(e => {
          console.log(e);
          resolve(this.processMetadataResponse(null));
        })
      })
    })
  }

  getNFTTransfers(contractAddress: string, tokenId: string): Promise<Array<NFTHistory>> {

    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}/transfers`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        this.httpsRequest(options).then(response => {
          resolve(this.processNFTHistoryResponse(response.result));
        }).catch(e => {
          console.log(e);
        })
      })
    })
  }

  getNFTTokenIdOwners(contractAddress: string, tokenId: string, address: string): Promise<NftDisplayElements2> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.MORALIS_API_KEY).then(MORALIS_API_KEY => {
        const options = {
          method: 'GET',
          url: `${MORALIS_BASE}/nft/${contractAddress}/${tokenId}/owners`,
          params: {chain: 'eth', format: 'decimal'},
          headers: {accept: 'application/json', 'X-API-Key': MORALIS_API_KEY}
        }
        this.httpsRequest(options).then(response => {
          var r = response;
          var rr = Array.from(r.result, k => (k as any).owner_of);
          if(rr.includes(address)) {
            resolve(this.processMetadataResponse(r.result[0]));
          } else {
            resolve(this.processMetadataResponse(null));
          }
        }).catch(e => {
          console.log(e);
          this.fetchNftMetadata(contractAddress, tokenId).then(resolution => {
            console.log(resolution);
            resolve(resolution);
          })
        })
      })
    })
  }

  processNFTHistoryResponse(sample: Array<any>): Array<NFTHistory> {
    var res = [];
    for(let i=0; i<sample.length; i++) {
      res.push(<NFTHistory>{
        from: sample[i].from_address,
        fromImage: `data:image/png;base64,${new Identicon(sample[i].from_address, 420).toString()}`,
        to: sample[i].to_address,
        toImage: `data:image/png;base64,${new Identicon(sample[i].to_address, 420).toString()}`,
        amount: sample[i].amount,
        value: sample[i].value,
        timestamp: sample[i].block_timestamp
      })
    }
    return res;
  }

  processMetadataResponse(sample: MoralisMetadataResponse): NftDisplayElements2 {
    if(!sample) {
      return <NftDisplayElements2>{
        attributes: null,
        identifier: null,
        collection: null,
        contract: null,
        token_standard: null,
        name: null,
        description: null,
        image_url: null,
        metadata_url: null,
        created_at: null,
        updated_at: null,
        is_disabled: false,
        is_nsfw: false,
        permalink: null
      }
    }
    var gg = {
      name: null,
      description: '',
      attributes: null,
      compiler: null,
      image: null
    };
    try {
      gg = Object.assign(gg, JSON.parse(sample.metadata));
    } catch (e) {
      console.log(e);
    }
    var g = <NftDisplayElements2> {

      attributes: !!gg.attributes ? gg.attributes : [],
      identifier: !!sample.token_hash ? sample.token_hash : "",
      collection: !!gg.compiler ? gg.compiler : (!!gg.description ? (gg.description.includes("an ENS name") ? "ens" : "") : ""),
      contract: sample.token_address,
      token_standard: !!sample.contract_type ? sample.contract_type : "",
      name: gg.name,
      description: gg.description,
      image_url: (gg.image.includes("ipfs://ipfs/") ? gg.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/") : (gg.image.includes("ipfs://") ? gg.image.replace("ipfs://","https://ipfs.io/ipfs/") : gg.image)),
      metadata_url: !!sample.token_uri ? sample.token_uri : "",
      created_at: new Date(sample.last_metadata_sync).toISOString(),
      updated_at: new Date(new Date(sample.last_token_uri_sync)).toISOString(),
      is_disabled: false,
      is_nsfw: false,
      permalink: null
    }
    return g;
  }

  processResponse(sample: MoralisResponse): Promise<Array<NftDisplayElements2>> {
    return new Promise((resolve, reject) => {
      var ret = [];

      for(let i = 0; i< sample.result.length; i++) {

          // if(sample.result[i].token_uri) {
          //   const options = {
          //     method: 'GET',
          //     url: `${sample.result[i].token_uri}`,
          //     headers: {accept: 'application/json'}
          //   }

          //   this._HttpClient.request(options)
          //   .then(details => {

          //     var f = <NftDisplayElements2>{
          //       attributes: !!details.data.attributes ? details.data.attributes : [],
          //       identifier: !!sample.result[i].token_hash ? sample.result[i].token_hash : "",
          //       collection: !!details.data.compiler ? details.data.compiler : "",
          //       contract: !!sample.result[i].token_address ? sample.result[i].token_address : "",
          //       token_standard: !!sample.result[i].contract_type ? sample.result[i].contract_type : "",
          //       name: !!details.data.name ? details.data.name : "",
          //       description: !!details.data.description ? details.data.description : "",
          //       image_url: (details.data.image.includes("ipfs://ipfs/")) ? (details.data.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (details.data.image.includes("ipfs://") ? details.data.image.replace("ipfs://","https://ipfs.io/ipfs/") : details.data.image),
          //       metadata_url: !!sample.result[i].token_uri ? sample.result[i].token_uri : "",
          //       created_at: !!sample.result[i].last_metadata_sync ? sample.result[i].last_metadata_sync : "",
          //       updated_at: !!sample.result[i].last_token_uri_sync ? sample.result[i].last_token_uri_sync : "",
          //       is_disabled: false,
          //       is_nsfw: false,
          //       permalink: !!sample.result[i].token_uri ? sample.result[i].token_uri : ""
          //     };
          //     ret.push(f);
          //   }).catch(e => {
          //     if(sample.result[i].metadata) {
          //       var metadata = JSON.parse(sample.result[i].metadata);

          //       var f = <NftDisplayElements2>{
          //         attributes: !!metadata.attributes ? metadata.attributes : [],
          //         identifier: !!sample.result[i].token_hash ? sample.result[i].token_hash : "",
          //         collection: !!metadata.compiler ? metadata.compiler : "",
          //         contract: !!sample.result[i].token_address ? sample.result[i].token_address : "",
          //         token_standard: !!sample.result[i].contract_type ? sample.result[i].contract_type : "",
          //         name: !!metadata.name ? metadata.name : "",
          //         description: !!metadata.description ? metadata.description : "",
          //         image_url: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
          //         metadata_url: !!sample.result[i].token_uri ? sample.result[i].token_uri : "",
          //         created_at: !!sample.result[i].last_metadata_sync ? sample.result[i].last_metadata_sync : "",
          //         updated_at: !!sample.result[i].last_token_uri_sync ? sample.result[i].last_token_uri_sync : "",
          //         is_disabled: false,
          //         is_nsfw: false,
          //         permalink: !!sample.result[i].token_uri ? sample.result[i].token_uri : ""
          //       };
          //       console.log("Moralis: f: catch: " + JSON.stringify(f));
          //       ret.push(f);
          //     }
          //     // reject(e);
          //   })
          // } else {
          //   console.log("No token_uri");
            if(sample.result[i].metadata) {
              var metadata = JSON.parse(sample.result[i].metadata);
              console.log(metadata);
            /*
              {
                "name": "SURI FREY NFT #62",
                "description": "2100 UNIQUE SURI FREI NFT BAGS",
                "image": "ipfs://QmXVffs5HbdaQBPHAc2k4jFKuMbMVm3RVB1oezmJzXtXg9/62.png",
                "edition": 62,
                "date": 1696951668977,
                "attributes": [
                    {
                        "trait_type": "BACKGROUNDS",
                        "value": "V06"
                    },
                    {
                        "trait_type": "BAGS",
                        "value": "PurseRedPaitent"
                    },
                    {
                        "trait_type": "HANDLES",
                        "value": "HandleGold"
                    }
                ],
                "compiler": "HashLips Art Engine"
            }
            */
              var f = <NftDisplayElements2>{
                attributes: !!metadata.attributes ? metadata.attributes : [],
                identifier: !!sample.result[i].token_hash ? sample.result[i].token_hash : "",
                collection: !!metadata.compiler ? metadata.compiler : (!!metadata.description ? (metadata.description.includes("an ENS name") ? "ens" : "") : ""),
                contract: !!sample.result[i].token_address ? sample.result[i].token_address : "",
                token_standard: !!sample.result[i].contract_type ? sample.result[i].contract_type : "",
                name: !!metadata.name ? metadata.name : "",
                description: !!metadata.description ? metadata.description : "",
                image_url: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
                metadata_url: !!sample.result[i].token_uri ? sample.result[i].token_uri : "",
                created_at: !!sample.result[i].last_metadata_sync ? sample.result[i].last_metadata_sync : "",
                updated_at: !!sample.result[i].last_token_uri_sync ? sample.result[i].last_token_uri_sync : "",
                is_disabled: false,
                is_nsfw: false,
                permalink: !!sample.result[i].token_uri ? sample.result[i].token_uri : ""
              };
              ret.push(f);
            }
          // }
        }
      // }
      console.log("Moralis: Resolve ret: " + JSON.stringify(ret));
      resolve(ret);
    })
  }

  processCelebrityMetadataResponse(sample: MoralisMetadataResponse): CelebrityNftDetails {
    if(!sample) {
      return <CelebrityNftDetails>{
        name: null,
        displayImage: null,
        network: null,
        descriptionText: null,
        creationDate: null,
        modificationDate: null,
        contractAddress: null,
        tokenId: null,
        rank: 0,
        internalInformation: {},
        releaseStatus: true,
        visibility: false
      }
    }
    var gg = {
      name: null,
      description: '',
      attributes: null,
      image: null
    };
    try {
      gg = Object.assign(gg, JSON.parse(sample.metadata));
    } catch (e) {
      console.log(e);
    }
    var g = <CelebrityNftDetails> {
      name: !!gg.name ? gg.name : `${sample.name} #${sample.token_id}`,
      displayImage: (gg.image.includes("ipfs://ipfs/") ? gg.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/") : (gg.image.includes("ipfs://") ? gg.image.replace("ipfs://","https://ipfs.io/ipfs/") : gg.image)),
      network: sample.contract_type,
      descriptionText: gg.description,
      contractAddress: sample.token_address,
      tokenId: sample.token_id,
      creationDate: +new Date(sample.last_metadata_sync),
      modificationDate: +new Date(new Date(sample.last_token_uri_sync)),
      rank: 1,
      internalInformation: !!gg.attributes ? { attributes: gg.attributes } : {},
      releaseStatus: true,
      visibility: true
    }
    return g;
  }

  processCelebrityResponse(sample: MoralisResponse): Promise<Array<CelebrityNftDetails>> {
    return new Promise((resolve, reject) => {
      var ret = [];

      for(let i = 0; i< sample.result.length; i++) {

          // if(sample.result[i].token_uri) {
          //   const options = {
          //     method: 'GET',
          //     url: `${sample.result[i].token_uri}`,
          //     headers: {accept: 'application/json'}
          //   }

          //   this._HttpClient.request(options).then(details => {

          //     var f = <CelebrityNftDetails>{
          //       name: !!details.data.name ? details.data.name : sample.result[i].name,
          //       displayImage: !!details.data.image ? (details.data.image.includes("ipfs://ipfs/") ? details.data.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/") : (details.data.image.includes("ipfs://") ? details.data.image.replace("ipfs://","https://ipfs.io/ipfs/") : details.data.image)) : details.data,
          //       network: sample.result[i].contract_type,
          //       descriptionText: !!details.data.description ? details.data.description : sample.result[i].name,
          //       contractAddress: sample.result[i].token_address,
          //       tokenId: sample.result[i].token_id,
          //       creationDate: +new Date(sample.result[i].last_token_uri_sync),
          //       modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
          //       rank: 1,
          //       internalInformation: !!details.data.attributes ? { attributes: details.data.attributes } : {},
          //       releaseStatus: true,
          //       visibility: true
          //     };
          //     ret.push(f);
          //   }).catch(e => {
          //     if(sample.result[i].metadata) {
          //       var metadata = JSON.parse(sample.result[i].metadata);

          //       var f = <CelebrityNftDetails>{
          //         name: metadata.name,
          //         displayImage: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
          //         network: sample.result[i].contract_type,
          //         descriptionText: metadata.description,
          //         contractAddress: sample.result[i].token_address,
          //         tokenId: sample.result[i].token_id,
          //         creationDate: +new Date(sample.result[i].last_token_uri_sync),
          //         modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
          //         rank: 1,
          //         internalInformation: !!metadata.attributes ? { attributes: metadata.attributes } : {},
          //         releaseStatus: true,
          //         visibility: true
          //       };
          //       console.log("Moralis: f: catch: " + JSON.stringify(f));
          //       ret.push(f);
          //     }

          //   })
          // } else {
          //   console.log("No token_uri");
            if(sample.result[i].metadata) {
              var metadata = JSON.parse(sample.result[i].metadata);
              console.log(metadata);
              var f = <CelebrityNftDetails>{
                name: metadata.name,
                displayImage: (metadata.image.includes("ipfs://ipfs/")) ? (metadata.image.replace("ipfs://ipfs/","https://ipfs.io/ipfs/")) : (metadata.image.includes("ipfs://") ? metadata.image.replace("ipfs://","https://ipfs.io/ipfs/") : metadata.image),
                network: sample.result[i].contract_type,
                descriptionText: metadata.description,
                contractAddress: sample.result[i].token_address,
                tokenId: sample.result[i].token_id,
                creationDate: +new Date(sample.result[i].last_token_uri_sync),
                modificationDate: +new Date(new Date(sample.result[i].last_metadata_sync)),
                rank: 1,
                internalInformation: !!metadata.attributes ? { attributes: metadata.attributes } : {},
                releaseStatus: true,
                visibility: true
              };
              ret.push(f);
            }
          // }
        }
      // }
      resolve(ret);
    })
  }

}
