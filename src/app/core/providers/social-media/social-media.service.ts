import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';
import { UserProviderService } from '@services/user/user-provider.service';

interface SendSocialMediaUserStatusBody {
  taskId: string;
  status: number;
  timestamp: number;
  earnings: string;
  earningsToken: string;
}

@Injectable({
  providedIn: 'root'
})
export class SocialMediaService {

  constructor(
    private http: HttpClient,
    private firebase: FirestoreCloudFunctionsService,
    private userProvider: UserProviderService
  ) { }

  async subscribeMailChimp(taskId: string): Promise<any> {

    const body = await this.firebase.encryptBody({ taskId });

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.subscribeMailChimp}`, body).subscribe({
        next: (response: any) => {
          resolve(response);
        },
        error: (error) => reject(error)
      })
    })
  }

  fetchSocialMediaTasks(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(`${environment.firestore.endpoints.fetchSocialMediaTasks}`).subscribe({
        next: (response: any) => {
          try {
            const sorted = response.data.sort((a) => { return a.active ? -1 : 1 });
            resolve(sorted);
          } catch (e) {
            resolve(response.data);
          }
        },
        error: (error) => reject(error)
      })
    })
  }

  async fetchSocialMediaUserStatus(): Promise<any> {

    return new Promise((resolve, reject) => {
      this.http.get(`${environment.firestore.endpoints.fetchSocialUserStatus}`, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
        next: (response: any) => {
          resolve(response.data);
        },
        error: (error) => reject(error)
      })
    })
  }

  async sendSocialMediaUserStatus(b: SendSocialMediaUserStatusBody): Promise<any> {

    const body = await this.firebase.encryptBody(b);

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.updateSocialUserStatus}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
        next: (response: any) => {
          resolve(response.data);
        },
        error: (error) => reject(error)
      })
    })
  }

  async obtainDiscordPersonalInfo(state: string): Promise<any> {

    const body = await this.firebase.encryptBody({ state });

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.obtainDiscordCode}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
        next: (response: any) => {
          resolve(response);
        },
        error: (error) => reject(error)
      })
    })
  }

  obtainDiscordGuilds(state: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/v1/discord/guilds?state=${state}`);
  }

  async updateDiscordId(discordId: string): Promise<any> {

    const body = await this.firebase.encryptBody({ discordId });

    return new Promise((resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.updateSocialAccounts}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
        next: (response: any) => {
          resolve(response);
        },
        error: (error) => reject(error)
      })
    })

  }

  fetchDiscordInviteLink(guildId: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/api/v1/discord/invite?guildId=${guildId}`);
  }


}
