import { Injectable, inject } from "@angular/core";
import { Params } from "@angular/router";
import { AlertController, NavController, ToastController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";

// custom imports
import { ApiProviderService } from "@services/api/api-provider.service";
import { SecureStorageKey } from "@services/secure-storage/secure-storage-key.enum";
import { SecureStorageService } from "@services/secure-storage/secure-storage.service";
import { KycProvider } from "src/app/kyc/enums/kyc-provider.enum";
import { KycState } from "src/app/kyc/enums/kyc-state.enum";
import { KycService } from "src/app/kyc/services/kyc.service";
import { JobDetail } from 'src/app/core/providers/human/JobDetail';
import { HumanService } from "@services/human/human.service";


@Injectable()
export class NavigateToJobService {

  private _ApiProviderService = inject(ApiProviderService);
  private _ToastController = inject(ToastController);
  private nav = inject(NavController);
  private _KycService = inject(KycService);
  private _AlertController = inject(AlertController);
  private translateService = inject(TranslateService);
  private secureStorageService = inject(SecureStorageService);
  private humanService = inject(HumanService);

  private maxKycTime = 15 * 60 * 1000;

  public async goToJob(job: JobDetail, demo: boolean, dataBase64: string) {
    // console.log('NavigateToJobService#goToJob; job', this.j);

    let navigateForward = await this.getNavigateForward(job.manifest.requestType, job.chainId, job.escrowAddress);
    if (!navigateForward) {
      return;
    }

    if (demo && dataBase64) {
      const queryParams: Params = {};
      queryParams.demo = demo;;
      queryParams.data = dataBase64;
      // console.log('NavigateToJobService#goToJob; queryParams', queryParams);

      this.nav.navigateForward(navigateForward, { queryParams });
      return;
    }

    // if user is in power user mode he doesnt neet to have KYC to do the job
    const powerUserMode = await this.secureStorageService.getValue(SecureStorageKey.powerUserMode, false);
    // console.log('NavigateToJobService#goToJob; powerUserMode', powerUserMode);
    if (powerUserMode) {
      this.nav.navigateForward(navigateForward);
      return;
    }

    // if KYC is required for the job check for it
    // console.log('NavigateToJobService#goToJob; kyc', job.manifest.annotations?.kyc);
    if (job.manifest.annotations?.kyc) {
      // console.log('NavigateToJobService#goToJob; check for KYC');
      this.userNeedsToDoKyc(false, navigateForward);
    } else {
      // console.log('NavigateToJobService#goToJob; navigate to job');
      this.nav.navigateForward(navigateForward);
    }
  }

  private async getNavigateForward(requestType: string, chainId: number, escrowAddress: string): Promise<string | void> {
    return new Promise(async (resolve) => {
      const workerAddress = await this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);

      this.humanService.assignment(chainId, escrowAddress, workerAddress)
            .subscribe({
              next: (assignment) => {
                // console.log('NavigateToJobService#getNavigateForward; assignment', assignment);

                resolve(this._getNavigateForward(requestType, escrowAddress, assignment.assignmentId));
              },
              error: (err: any) => {
                // console.log('NavigateToJobService#getNavigateForward; error:', err);

                if (err.error?.message == 'Fully assigned job') {
                  this.presentToast(this.translateService.instant('FORTUNE.ERRORS.FULLY_ASSIGNED_JOB'));
                } else if (err.error?.message == 'Assignment already exists') {
                  this.presentToast(this.translateService.instant('FORTUNE.ERRORS.ALREADY_EXISTS'));
                } else {
                  console.error('NavigateToJobService#getNavigateForward; assignment error:', err);
                  this.presentToast(this.translateService.instant('FORTUNE.ERRORS.GENERIC'));
                }
              }
            });
          });
  }

  private _getNavigateForward(requestType: string, escrowAddress: string, assignmentId: number): string {
    let navigateForward: string;

    switch (requestType) {
      case "PRICE_GUESS_SWIPE":
        navigateForward = `/jobs/swiper/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "FORTUNE_RATING":
        navigateForward = `/jobs/rating/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "FORTUNE_AB":
        navigateForward = `/jobs/ab/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "FORTUNE_QUIZ":
        navigateForward = `/jobs/quiz/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "FORTUNE_PROMPT_TRAINING":
        navigateForward = `/jobs/ai-training/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "JOBGRADER_OCR":
        navigateForward = `/jobs/ocr/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "JOBGRADER_AUDIO":
        navigateForward = `/jobs/audio-labelling/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "JOBGRADER_FACE_AI":
        navigateForward = `/jobs/human/${escrowAddress}?assignmentId=${assignmentId}`;
        break;
      case "JOBGRADER_VIDEO":
        navigateForward = `/jobs/video-recording/${escrowAddress}?assignmentId=${assignmentId}`;
      break;
      case "JOBGRADER_HCAPTCHA":
        navigateForward = `/jobs/hcaptcha/${escrowAddress}?assignmentId=${assignmentId}`;
      break;
    }

    // console.log('NavigateToJobService#_getNavigateForward; navigateForward', navigateForward);

    return navigateForward;
  }

  private userNeedsToDoKyc(hCpatcha: boolean, path: string, force: boolean = false) {

    // @michael: !IMPORTANT: The user MUST NOT have a + in the registered email address or else, the siteKey generation endpoint will return an error.

    this._KycService.isUserAllowedToUseChatMarketplace().then(v => {
        if(!v) {

          const state = this._KycService.getState(KycProvider.ONDATO_PROD);
          // console.log('NavigateToJobService#userNeedsToDoKyc; state', state);

          if (state == KycState.KYC_INITIATED && !force) {

            const time_ = this._KycService.getKycInitiatedTimestamp(KycProvider.ONDATO_PROD);
            // console.log("NavigateToJobService#userNeedsToDoKyc; time_", time_);

            if(time_) {
              const currentTime = +new Date();
            //   console.log("NavigateToJobService#userNeedsToDoKyc; currentTime", currentTime)

              if(currentTime < (time_ + this.maxKycTime)) {
                // let remainingTime = (time_ + this.maxKycTime) - currentTime;

                // let diff = remainingTime;
                // diff = Math.floor(diff / 1000);
                // const secs_diff = diff % 60;
                // diff = Math.floor(diff / 60);
                // const mins_diff = diff % 60;
                // diff = Math.floor(diff / 60);
                // const hours_diff = diff % 24;
                // diff = Math.floor(diff / 24);

                // const displayTime = (mins_diff == 1) ? `${mins_diff} ${this.translateService.instant("MINUTE")} ${secs_diff} ${this.translateService.instant("SECONDS")}` : `${mins_diff} ${this.translateService.instant("MINUTES")} ${secs_diff} ${this.translateService.instant("SECONDS")}`;


                // console.log("remainingTime", remainingTime);

                const alertButtons = [
                  {
                    text: this.translateService.instant('GENERAL.ok'),
                    handler: () => {
                      // if (this.timerSubscription) {
                      //   if (!this.timestampSubscription.closed) {
                      //     this.timestampSubscription.unsubscribe();
                      //   }
                      // }
                    }
                  }
                ]

                // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');


                /* mburger: this does not work as it was thought by Ansik, so Im going to remove the code for now

                const source = timer(1000, 1000);

                this.timestampSubscription = source.subscribe(() => {
                  console.log("NavigateToJobService#userNeedsToDoKyc; source.subscribe");

                  remainingTime = remainingTime - 1000;
                  diff = remainingTime;
                  diff = Math.floor(diff / 1000);
                  const secs_diff = diff % 60;
                  diff = Math.floor(diff / 60);
                  const mins_diff = diff % 60;
                  diff = Math.floor(diff / 60);
                  const hours_diff = diff % 24;
                  diff = Math.floor(diff / 24);

                  this.displayTime = `${hours_diff}:${mins_diff}:${secs_diff}`;
                  // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');
                  // console.log(this.displayTime);

                  if(remainingTime <= 1000) {
                    if (this.timestampSubscription) {
                      if (!this.timestampSubscription.closed) {
                        this.timestampSubscription.unsubscribe();
                      }
                    }
                  }
                })
                */



                this._AlertController.create({
                  mode: 'ios',
                  header: this.translateService.instant('KYCINPROGRESS.header'),
                  // subHeader: `${this.displayTime}`,
                  message: `${this.translateService.instant('KYCINPROGRESS.message-1')} ${this.translateService.instant('KYCINPROGRESS.message-2')}`,
                  buttons: alertButtons
                }).then(alerto => {
                  alerto.present();
                })
              } else {
                this.userNeedsToDoKyc(hCpatcha, path, true);
              }
            }

          } else {
            this._AlertController.create({
              mode: 'ios',
              header: this.translateService.instant('HCAPTCHAKYC.header'),
              message: this.translateService.instant('HCAPTCHAKYC.subheader'),
              buttons: [
                {
                  text: this.translateService.instant('GENERAL.ok'),
                  handler: () => {
                    this.nav.navigateForward('/kyc');
                  }
                },
                {
                  text: this.translateService.instant('BUTTON.CANCEL'),
                  role: "cancel",
                  handler: () => {}
                }
              ]
            }).then(alerto => {
              alerto.present();
            })
          }


        } else {

            if(hCpatcha) {
                this._ApiProviderService.ObtainSiteKeys().then(response => {
                    // @michael: !IMPORTANT: The user MUST NOT have a + in the registered email address or else, the siteKey generation endpoint will return an error.
                    if(response == '') {
                        this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
                    } else {
                    //   setTimeout(() => {
                        this.nav.navigateForward(path);
                    //   }, 333)
                    }
                  }).catch(e => {
                      this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
                  });
            } else {
                this.nav.navigateForward(path);
            }

        }
      })
  }

  private  presentToast(message: string) {
    return this._ToastController.create({
      message: message,
      position: 'top',
      duration: 2000
    }).then(toast => toast.present())
  }
}