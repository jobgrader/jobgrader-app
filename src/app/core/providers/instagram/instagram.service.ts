import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class InstagramService {


  public hashTags = ["helixidappnewsweb3"];

  constructor(
    private _HttpClient: HttpClient,
    private _Vault: VaultService,
  ) {

  }

  returnPosts(startDate: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.INSTAGRAM_API).then((INSTAGRAM_API) => {
        this._HttpClient.get(`https://graph.instagram.com/v11.0/5223591067728288/media?since=${startDate}T00%3A00%3A00.000Z&limit=50&fields=id,username&access_token=${INSTAGRAM_API}`,{
          headers: {

          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })

  }

  returnImages(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.INSTAGRAM_API).then((INSTAGRAM_API) => {
        this._HttpClient.get(`https://graph.instagram.com/${id}?fields=media_url,media_type,caption,timestamp&access_token=${INSTAGRAM_API}`, {
          headers: {

          }
        }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
      })
    })

  }

  returnMedium(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._HttpClient.get(`https://api.medium.com/v1/`, {
        headers: {

        }
      }).subscribe({
        next: (response) => {
          resolve(response);
        },
        error: (e) => {
          reject(e);
        }
      })
    })

  }


  showMore(tracker?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this._Vault.getSecret(VaultSecretKeys.INSTAGRAM_API).then(INSTAGRAM_API => {
        if(!!tracker) {
          this._HttpClient.get(tracker, {
            headers: {

            }
          }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
        } else {
          this._HttpClient.get(`https://graph.instagram.com/v11.0/5223591067728288/media?access_token=${INSTAGRAM_API}&pretty=1&fields=id%2Cusername&since=2022-06-09T00%3A00%3A00.000Z&limit=25&after=QVFIUklfdTRmaVJWcnFYZAGpsTEVNRWtZAeTRaczRhWEs2cVBMY3JlRE9SZAHVJZAGY1NjhXc2lWM2h6RVd6LWZA5bC0wTTgwNGt2dlhYN25lMnFaV0FlMk9tQkhB`, {
            headers: {

            }
          }).subscribe({
          next: (response) => {
            resolve(response);
          },
          error: (e) => {
            reject(e);
          }
        })
        }
      })

    })
  }

}


