import { TestBed } from '@angular/core/testing';

import { FirestoreCloudFunctionsService } from './firestore-cloud-functions.service';

describe('FirestoreCloudFunctionsService', () => {
  let service: FirestoreCloudFunctionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirestoreCloudFunctionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
