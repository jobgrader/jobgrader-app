import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { decrypt, encrypt } from '@toruslabs/eccrypto';
import { environment } from 'src/environments/environment';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { UserProviderService } from '../user/user-provider.service';
import { ToastController } from '@ionic/angular';
import { AnalyticsRemoteConfigService } from '@services/firebase-analytics-remote-config/firebase-analytics-remote-config.service';
import { MoralisService } from '@services/moralis/moralis.service';
import { AlchemyNfts } from 'src/app/nft/nft.page';

interface GeneratereferralcodeResponse {
  success: boolean;
  generatedReferralCode?: string;
  error?: string;
}

interface CheckJobSolutionResponse {
  success: boolean;
  trusted?: boolean;
  solutionsCorrect?: boolean;
  error?: string;
}

interface StoreAssessmentVcsResponse {
  success: boolean;
  error?: string;
}

interface ObtainAssessmentVcsResponse {
  success: boolean;
  vcIds?: Array<string>;
  error?: string
}

export interface Referrals {
  generatedReferralCode?: string;
  referralCategory?: string;
  title?: any;
  description?: any;
  maxReferredEmails?: number;
  expiryDate?: {
    _nanoseconds: number;
    _seconds: number;
  };
  active?: boolean;
}

interface FetchreferralprogramsResponse {
  success: boolean;
  data?: Array<Referrals>
  // referralPrograms?: Array<string>;
  // referralCodes?: Array<string>;
  error?: string;
}

interface ObtainusergroupsResponse {
  success: boolean;
  groups?: Array<string>;
  error?: string;
}

interface ManageuserentryResponse {
  success: boolean;
  payment?: string;
  error?: string;
}

interface ObtainuserdataResponse {
  success: boolean;
  gender: string;
  firebaseToken?: string;
  languageProficiencies: Array<string>;
  nationalities: Array<string>;
  groups?: Array<string>;
  points?: number;
}

interface GeneratereferralcodeBody {
  referralCategory: string
}


interface ObtainusergroupsBody {
  email?: string;
  gender: string;
  deviceId?: string;
  firebaseToken?: string;
  languageProficiencies: Array<string>;
  nationalities: Array<string>;
}

interface ManageuserentryBody {
  email?: string;
  userGroups: Array<string>;
  usedReferralCode: string;
}

interface CheckReferralCodeValidityResponse {
  success: boolean;
  error?: string;
}

interface FetchEpnNftsResponse {
  success: boolean;
  error?: string;
  nfts?: Array<any>
}

interface ReturnPointsResponse {
  success: boolean;
  error?: string;
  pointsToReferred?: number;
  pointsToReferrer?: number;
}

interface ThxcUsdResponse {
  success: boolean;
  error?: string;
  data?: {
    bestAsk: number;
    bestBid: number;
  }
}

@Injectable({
  providedIn: 'root'
})
export class FirestoreCloudFunctionsService {

  additionalInformation;
  assessmentContract;
  jobNft;

  constructor(
    private http: HttpClient,
    private secureStorage: SecureStorageService,
    private analyticsRemoteConfig: AnalyticsRemoteConfigService,
    private moralis: MoralisService,
    private toastController: ToastController,
    private userProvider: UserProviderService
  ) {

   }

   init() {
    this.returnAssessmentEpnContract().then(assessmentContractResponse => {
      this.assessmentContract = assessmentContractResponse.contract;
      this.jobNft = assessmentContractResponse.jobNft;
    }).catch(e => {
      console.log(e);
    })
   }

   async registerWCTopic(topic: string, token: string): Promise<any> {
    const body = await this.encryptBody({topic, token});
    return new Promise(async (resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.walletconnectregistertopic}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
          next: (response: any) => {
            resolve(response);
          },
          error: (error) => {
            reject(error);
          }
        })
    })
  }

  async deregisterWCTopic(topic: string): Promise<any> {
    const body = await this.encryptBody({topic});
    return new Promise(async (resolve, reject) => {
      this.http.post(`${environment.firestore.endpoints.walletconnectderegistertopic}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      }).subscribe({
          next: (response: any) => {
            resolve(response);
          },
          error: (error) => {
            reject(error);
          }
        })
    })
  }

  async obtainThxcUsdConversion(): Promise<number> {

    return new Promise(async (resolve, reject) => {

      this.http.get(`${environment.firestore.endpoints.thxcUsd}`, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: ThxcUsdResponse) => {

            console.log("ThxcUsdResponse: ", response);
            if(!!response.success) {
              resolve(response.data.bestAsk);
            } else {
              reject(response.error);
            }
          },
          error: (error) => {

            reject(error);
          }
        })
    })

  }

  async generateReferralCode(info: GeneratereferralcodeBody): Promise<GeneratereferralcodeResponse> {

    return new Promise(async (resolve, reject) => {
      const body = await this.encryptBody(info);

      this.http.post(`${environment.firestore.endpoints.generateReferralCode}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: GeneratereferralcodeResponse) => {

            console.log("generateReferralCode: ", response);
            if(!!response.success) {
              // this.presentToast(JSON.stringify(response));
              resolve(response);
            } else {
              reject(response);
            }
          },
          error: (error) => {

            reject(error);
          }
        })
    })

  }

  returnAssessmentEpnContract(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.firestore.endpoints.fetchAssessmentContract + "?epn=true").subscribe({
        next: async (contractAddressResponse: any) => {
          resolve(contractAddressResponse);
        },
        error: (error) => {
          reject();
        }
      })
    })
  }

  returnAssessmentGnosisContract(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(environment.firestore.endpoints.fetchAssessmentContract).subscribe({
        next: async (contractAddressResponse: any) => {
          resolve(contractAddressResponse);
        },
        error: (error) => {
          reject();
        }
      })
    })
  }

  async returnAssessmentEpnNfts(): Promise<Array<any>> {
    return new Promise(async (resolve) => {
      const verifiedWalletAddress = await this.secureStorage.getValue(SecureStorageKey.web3WalletPublicKey, false);
      if(verifiedWalletAddress) {
        const epntNfts = await this.fetchEpnNfts(verifiedWalletAddress);
        const epnContractResponse = await this.returnAssessmentEpnContract();
        this.assessmentContract = epnContractResponse.contract;
        this.jobNft = epnContractResponse.jobNft;
        const epnFiltered = epntNfts.filter(aa => !aa.possible_spam);
        const processed: Array<AlchemyNfts> = [];
        for(let i=0; i<epnFiltered.length; i++) {
          processed.push(await this.moralis.processDisplayElements(epnFiltered[i]));
        }
        if(epnContractResponse.contract) {
          resolve(processed.filter(a =>
            (
              a.contract.toLowerCase() == epnContractResponse.contract.toLowerCase()
            ) &&
            (
              !!(epnContractResponse.jobNft.find(jn => jn.nftId == a.attributes.find(aa => aa.trait_type == 'id')?.value ))
            )
          ));
        } else {
          resolve([]);
        }
      }
    })

  }

  async returnAssessmentGnosisNfts(): Promise<Array<any>> {
    return new Promise(async (resolve) => {
      const verifiedWalletAddress = await this.secureStorage.getValue(SecureStorageKey.web3WalletPublicKey, false);
      if(verifiedWalletAddress) {
        const gnosisNfts = await this.moralis.fetchNfts(verifiedWalletAddress);
        const gnosisContractResponse = await this.returnAssessmentGnosisContract();
        const gnosisFiltered = gnosisNfts.filter(aa => !aa.possible_spam);
        const processed: Array<AlchemyNfts> = [];
        for(let i=0; i<gnosisFiltered.length; i++) {
          processed.push(await this.moralis.processDisplayElements(gnosisFiltered[i]));
        }
        if(gnosisContractResponse.contract) {
          resolve(processed.filter(a =>
            (
              a.contract.toLowerCase() == gnosisContractResponse.contract.toLowerCase()
            ) &&
            (
              !!(gnosisContractResponse.jobNft.find(jn => jn.nftId == a.attributes.find(aa => aa.trait_type == 'id')?.value ))
            )
          ));
        } else {
          resolve([]);
        }
      }
    })

  }

  async fetchEpnNfts(address: string): Promise<Array<any>> {

    return new Promise(async (resolve, reject) => {

      const body = await this.encryptBody({ address });

      this.http.post(`${environment.firestore.endpoints.fetchEpnNfts}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: FetchEpnNftsResponse) => {

            console.log("FetchEpnNftsResponse: ", response);
            if(!!response.success) {
              // this.presentToast(JSON.stringify(response));
              resolve(response.nfts);
            } else {
              reject([]);
            }
          },
          error: (error) => {

            reject(error);
          }
        })

    })

  }

  async fetchReferralPrograms(): Promise<Array<Referrals>> {

    return new Promise(async (resolve, reject) => {

      this.http.get(`${environment.firestore.endpoints.fetchReferralPrograms}`, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: FetchreferralprogramsResponse) => {

            console.log("fetchReferralPrograms: ", response);
            if(!!response.success) {
              resolve(response.data);
            } else {
              reject(response);
            }
          },
          error: (error) => {

            reject(error);
          }
        })
    })



  }

  async obtainUserGroups(email: string, info: ObtainusergroupsBody): Promise<ObtainusergroupsResponse> {

    return new Promise(async (resolve, reject) => {
      const deviceId = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
      const firebaseToken = await this.secureStorage.getValue(SecureStorageKey.firebase, false);
      const userData = await this.userProvider.getUser();
      const userId = userData.id;
      const rawBody = Object.assign(info, { deviceId, firebaseToken, email });

      console.log({ rawBody });

      const body = await this.encryptBody(rawBody);

      this.http.post(`${environment.firestore.endpoints.obtainUserGroups}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: ObtainusergroupsResponse) => {

            console.log("obtainUserGroups: ", response);

            if(!!response.success) {
              // this.presentToast(JSON.stringify(response));

              try {
                this.analyticsRemoteConfig.initiatelizeAnalytics(userId, response.groups).then(() => {
                  console.log("Success initiatelizeAnalytics: ");
                  this.analyticsRemoteConfig.initiatlizeRemoteConfig().then(() => {
                    console.log("Success initiatlizeRemoteConfig: ");
                  }).catch(e => {
                    console.log("Error initiatlizeRemoteConfig: ", e);
                  })
                }).catch(e => {
                  console.log("Error initiatelizeAnalytics: ", e);
                })
              } catch (error) {
                console.log("Catch block initiatelizeAnalytics and initiatlizeRemoteConfig: ", error);
              }

              resolve(response);
            } else {
              reject(response);
            }
          },
          error: (error) => {

            reject(error);
          }
        })
    })

  }

  async obtainUserData(): Promise<ObtainuserdataResponse> {

    return new Promise(async (resolve, reject) => {

      this.http.get(`${environment.firestore.endpoints.obtainUserData}`, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: ObtainuserdataResponse) => {

            console.log("obtainUserData: ", response);
            if(!!response.success) {
              this.additionalInformation = response;
              resolve(response);
            } else {
              reject(response);
            }
          },
          error: (error) => {

            reject(error);
          }
        })
    })

  }

  async manageUserEntry(info: ManageuserentryBody): Promise<ManageuserentryResponse> {

    return new Promise(async (resolve, reject) => {

      const body = await this.encryptBody(info);

      this.http.post(`${environment.firestore.endpoints.manageUserEntry}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: ManageuserentryResponse) => {

            console.log("manageUserEntry: ", response);
            if(!!response.success) {
              // this.presentToast(JSON.stringify(response));
              resolve(response);
            } else {
              reject(response);
            }
          },
          error: (error) => {

            reject(error);
          }
        })

    })

  }

  async checkReferralCodeValidity(code: string): Promise<CheckReferralCodeValidityResponse> {

    return new Promise(async (resolve, reject) => {

      const body = await this.encryptBody({ code });

      this.http.post(`${environment.firestore.endpoints.checkReferralCodeValidity}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: CheckReferralCodeValidityResponse) => {

            console.log("checkReferralCodeValidity: ", response);

              resolve(response);

          },
          error: (error) => {

            reject(error);
          }
        })

    })

  }

  async returnPoints(): Promise<ReturnPointsResponse> {

    return new Promise(async (resolve, reject) => {

      this.http.get(`${environment.firestore.endpoints.returnPoints}`)
        .subscribe({
          next: (response: ReturnPointsResponse) => {
            console.log("ReturnPointsResponse: ", response);
            resolve(response);
          },
          error: (error) => {
            reject(error);
          }
        })

    })

  }

  async updateFirebaseToken(): Promise<any> {

    const deviceId = await this.secureStorage.getValue(SecureStorageKey.deviceInfo, false);
    const firebaseToken = await this.secureStorage.getValue(SecureStorageKey.firebase, false);

    const body = await this.encryptBody({ deviceId, firebaseToken });

    return new Promise(async (resolve, reject) => {

      this.http.post(`${environment.firestore.endpoints.updateFirebaseToken}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response) => {
            console.log("UpdateFirebaseToken: ", response);
            resolve(response);
          },
          error: (error) => {
            reject(error);
          }
        })

    })

  }

  public async checkJobSolution(escrowAddress: string, solution: string, address: string, certificates: string[], deviceId: string): Promise<CheckJobSolutionResponse> {

    const ob = { escrowAddress, solution, address, certificates, deviceId, epn: true };
    console.info({ ob });
    const body = await this.encryptBody(ob);

    return new Promise(async (resolve, reject) => {

      this.http.post(`${environment.firestore.endpoints.checkJobSolution}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: CheckJobSolutionResponse) => {
            console.log("checkJobSolution: ", response);
            resolve(response);
          },
          error: (error) => {
            reject(error);
          }
        })

    })
  }


  public async storeAssessmentVcs(vcId: string): Promise<boolean> {

    const body = await this.encryptBody({ vcId });

    return new Promise(async (resolve, reject) => {

      this.http.post(`${environment.firestore.endpoints.storeAssessmentVcs}`, body, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: StoreAssessmentVcsResponse) => {
            console.log("storeAssessmentVcs: ", response);
            resolve(response.success);
          },
          error: (error) => {
            reject(error);
          }
        })

    })
  }

  public async obtainAssessmentVcs(): Promise<Array<string>> {

    return new Promise(async (resolve, reject) => {

      this.http.get(`${environment.firestore.endpoints.obtainAssessmentVcs}`, {
        headers: {
          "Content-Type": "text/plain"
        }
      })
        .subscribe({
          next: (response: ObtainAssessmentVcsResponse) => {
            console.log("storeAssessmentVcs: ", response);
            resolve(response.vcIds);
          },
          error: (error) => {
            reject(error);
          }
        })

    })
  }

  public async encryptBody(body: any): Promise<string> {

    const stringified = JSON.stringify(body);

    const encryptedBuffer = await encrypt(
      Buffer.from(environment.firestore.publicKey, "hex"),
      Buffer.from(stringified)
    )

    const uInt8ArrayToBase64 = (bytes: Uint8Array) => {
      return Buffer.from(bytes).toString('base64');
    }

    const encryptedUnion = Buffer.concat([
        encryptedBuffer.iv,
        encryptedBuffer.ephemPublicKey,
        encryptedBuffer.ciphertext,
        encryptedBuffer.mac,
    ]);

    return uInt8ArrayToBase64(encryptedUnion);

  }

  private async decryptBody(encrypted: string): Promise<string> {

    const uInt8ArrayToBase64 = (bytes: Uint8Array): string => {
      return Buffer.from(bytes).toString('base64');
    }

    const base64ToUint8Array = (base64: string): Uint8Array => {
      const binaryString = window.atob(base64);
      const len = binaryString.length;
      const bytes = new Uint8Array(len);
      for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i);
      }
      return bytes;
    }

    const base64ToHex = (str: string): string => {
      const raw = window.atob(str);
      let result = "";
      for (let i = 0; i < raw.length; i++) {
          const hex = raw.charCodeAt(i).toString(16);
          result += hex.length === 2 ? hex : "0" + hex;
      }
      return result.toUpperCase();
    }

    const uIntToBuffer = (u: Uint8Array): Buffer => {
      return Buffer.from(
        base64ToHex(uInt8ArrayToBase64(u)),
        "hex"
      )
    }

    const decryptedUnion = base64ToUint8Array(encrypted);

    const arrayByteObject = {
        iv: decryptedUnion.slice(0, 16),
        v: decryptedUnion.slice(16, 16 + 65),
        cipherText: decryptedUnion.slice(16 + 65, decryptedUnion.length - 32),
        t: decryptedUnion.slice(decryptedUnion.length - 32),
    };

    var hexByteObjectEncrypted = {
        iv: uIntToBuffer(arrayByteObject.iv),
        ephemPublicKey: uIntToBuffer(arrayByteObject.v),
        ciphertext: uIntToBuffer(arrayByteObject.cipherText),
        mac: uIntToBuffer(arrayByteObject.t),
    };

    const decryptedData = await decrypt(
        Buffer.from(environment.firestore.privateKey, "hex"),
        hexByteObjectEncrypted
    );

    return new TextDecoder().decode(decryptedData);

  }

  private presentToast(message: string) {
    this.toastController.create({
      message,
      position: "top",
      duration: 2000
    }).then(toast => {
      toast.present();
    })
  }

}
