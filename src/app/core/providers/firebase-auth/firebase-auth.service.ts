import { Injectable, inject } from '@angular/core';
import { Auth, User, UserCredential, idToken, signInWithCustomToken, user } from '@angular/fire/auth';


// custom import
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { AppStateService } from '../app-state/app-state.service';
import { map, take } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {

  private appStateService = inject(AppStateService);
  private secureStorageService = inject(SecureStorageService);
  private auth = inject(Auth);
  private user$ = user(this.auth);
  // userSubscription: Subscription;
  private token$ = idToken(this.auth);
  // tokenSubscription: Subscription;


  constructor() {
    // console.log('xxxxxx ####');
    // this.userSubscription = this.user$.subscribe((user: User | null) => {
    //   // handle user state changes here. Note, that user will be null if there is no currently logged in user.
    //  console.log('######', user);
    // });

    this.token$.subscribe((token: string | null) => {
      // handle idToken changes here. Note, that user will be null if there is no currently logged in user.
      // console.log('##### token', token);
      this.secureStorageService.setValue(SecureStorageKey.basicAuthToken, token);
      this.appStateService.basicAuthToken = token;
      // console.log('##### !!token', !!token);
      this.appStateService.isAuthorized = !!token;

    });
  }

  // async createUserAndSignIn(email: string, password: string) {
  //   const createUserResponse = await createUserWithEmailAndPassword(this.auth, email, password);
  //   console.log({ createUserResponse });
  //   const signInUserResponse = await signInWithEmailAndPassword(this.auth, email, password);
  //   console.log({ signInUserResponse });
  // }

  // async changePassword(email: string, password: string) {
  //   // TODO: @Ansik: Change password method
  // }

  signInWithCustomToken(token: string): Promise<UserCredential | any> {
    return new Promise((resolve, reject) => {
      signInWithCustomToken(this.auth, token)
        .then(
          async (user: UserCredential) => {
            // console.info('FirebaseAuthService#signInWithCustomToken; user:', user);

            const token = await user.user?.getIdToken();

            this.appStateService.basicAuthToken = token
            this.appStateService.isAuthorized = !!token
            await this.secureStorageService.setValue(SecureStorageKey.basicAuthToken, token);
            await this.secureStorageService.setValue(SecureStorageKey.token, token, false, false);


            resolve(user);
          }
        )
        .catch(
          (error: any) => {
            // console.info('FirebaseAuthService#signInWithCustomToken; error:', error);
            resolve(error);
          }
        );
      });
  }

  async signOut() {
    return this.auth.signOut();
  }

  async refreshToken(): Promise<string> {
    const newToken = await this.auth.currentUser?.getIdToken(true);

    this.appStateService.basicAuthToken = newToken
    await this.secureStorageService.setValue(SecureStorageKey.basicAuthToken, newToken);
    await this.secureStorageService.setValue(SecureStorageKey.token, newToken, false, false);

    return newToken;
  }


  // signInWithEmailAndPassword(email: string, password): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     signInWithEmailAndPassword(this.auth, email, password)
  //       .then(
  //         (value: UserCredential) =>  {
  //           console.info(value);
  //           resolve(false);
  //         }
  //       )
  //       .catch(
  //         (reason: any) => {
  //           console.error(reason);
  //           resolve(false);
  //         }
  //       )
  // }

  get currentUser(): User {
    return this.auth.currentUser;
  }

  public token(): Promise<string> {
    return new Promise((resolve) => {
      this.token$
        .pipe(
          take(1),
          map((token) => {
            resolve(token);
          })
        ).subscribe()
    });
  }

  public isAuthenticated(): Promise<boolean> {
    return new Promise((resolve) => {
      this.user$
        .pipe(
          take(1),
          map((user) => {
            if (user) {
              resolve(true);
            }

            resolve(false);
          })
        ).subscribe()
    });
  }
}
