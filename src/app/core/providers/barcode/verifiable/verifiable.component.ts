import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { TranslateProviderService } from '../../translate/translate-provider.service';
import { Clipboard } from '@capacitor/clipboard';

@Component({
  selector: 'app-verifiable',
  templateUrl: './verifiable.component.html',
  styleUrls: ['./verifiable.component.scss'],
})
export class VerifiableComponent implements OnInit {
  @Input() input: any;
  type;

  coreKeys = [
      // "@context",
      "id",
      // "type",
      "issuer",
      "holder",
      // "credentialSubject",
      "validFrom",
      "validTo",
      // "credentialStatus"
  ];

  credentialKeys = [
      "trustedBy",
      "trustLevel",
      "trustValue",
      "gender",
      "maritalstatus",
      "firstname",
      "lastname",
      "dateofbirth",
      "countryofbirth",
      "citizenship",
      "street",
      "city",
      "zip",
      "state",
      "cityofbirth",
      "country",
      "maidenname",
      "middlename",
      "identificationdocumentnumber",
      "identificationissuecountry",
      "identificationexpirydate",
      "identificationissuedate",
      "driverlicencedocumentnumber",
      "driverlicencecountry",
      "driverlicenceexpirydate",
      "driverlicenceissuedate",
      "passportnumber",
      "passportissuecountry",
      "passportexpirydate",
      "passportissuedate",
      "residencepermitnumber",
      "residencepermitissuecountry",
      "residencepermitexpirydate",
      "residencepermitissuedate",
  ];

  keyValues;

  constructor(
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
    
    private _ToastController: ToastController
  ) {

  }

  ngOnInit() {
    console.log(this.input);
    if(!!this.input.type) {
      if(this.input.type.includes('VerifiableCredential')) {
        this.type = 'Verifiable Credential'
      } else if(this.input.type.includes('VerifiablePresentation')) {
        this.type = 'Verifiable Presentation'
      }
    }
    var keyValues = [];
    for(let _ of this.coreKeys) {
      if(!!this.input[_]) {
        keyValues.push({
          key: _,
          value: this.input[_]
        })
      }
    }
    if(!!this.input.credentialSubject){
      if(!!this.input.credentialSubject.data) {
        for(let _ of this.credentialKeys) {
          if(!!this.input.credentialSubject.data[_]) {
            keyValues.push({
              key: _,
              value: this.input.credentialSubject.data[_]
            })
          }
        }
      }
    }
    this.keyValues = keyValues;
  }

  close() {
    this._ModalController.dismiss();
  }

  async onPress($event, notif: string, value: any) {
    await Clipboard.write({ string: value.toString() });
    await this.presentToast(notif + this._Translate.instant('SETTINGSACCOUNT.copied'));
  }

  presentToast(message: string) {
    this._ToastController.create({
      message,
      position: 'bottom',
      duration: 2000
    }).then((toast) => {
      toast.present();
    })
  }

}
