import { Injectable } from '@angular/core';
// import { ContactAddModalComponent } from '../../../contact/shared/contact-add-modal/contact-add-modal.component';
import { take } from 'rxjs/operators';
import { LockService } from '../../../lock-screen/lock.service';
import { ContactService } from '../../../contact/shared/contact.service';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { UserProviderService } from '../user/user-provider.service';
import { ModalController, NavController, PopoverController } from '@ionic/angular';
import { ApiProviderService } from '../api/api-provider.service';
import { UtilityService } from '../utillity/utility.service';
import { AlertController, ToastController } from '@ionic/angular';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
// import { ChatService } from '../chat/chat.service';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { UDIDNonce } from '../device/udid.enum';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { EuDgc } from './eudgc';
import * as CryptoJS from 'crypto-js';
import { CryptojsProviderService } from '../cryptojs/cryptojs-provider.service';
import { WalletConnectService } from '../wallet-connect/wallet-connect.service';
import { VerifiableComponent } from './verifiable/verifiable.component';
import { App } from '@capacitor/app';
import { DidVcService } from '../did-vc/did-vc.service';
import { BarcodeScanningModalComponent } from './view/barcode-scanning-modal.component';
import { Barcode, BarcodeFormat, BarcodeScanner, LensFacing } from '@capacitor-mlkit/barcode-scanning';
// import * as didJwt from "did-jwt";

// import { NftService } from '../nft/nft.service';


@Injectable()
export class BarcodeService {

    private toast: HTMLIonToastElement;

    // private TG = tg;

    public energyAustraliaKeys = {
        publicKey: 'BFooR4C8mquyKl04DGJ6qcfDWxIiTB0nGPvXVA/o5VIwto/mqFSDEPHP29AmQOBWw1uT4/hSEiA1Nv2dlCO1VQ0=',
        privateKey: 'pqcsU/8PjredYR3dTeb9ftUstU6HhI4BAct4kTGAA9k='
    }

    // public energyAustraliaQRCode: string = null;
    public energyAustraliaQRCode: string = 'U2FsdGVkX18XyJFh5nOUKAusCZoEhIZGPU31C6+U4jTWKeENNkiVjFu3GFJ0hA/Y+YIiLRRPQXDEC4s4gGJ7JtDngaT5ocdXRg6wr6jHHHr8UPMN1TyY/yCtXKJpHBzWBfsOGc/81Uf+DfJf19iNBlWR8LdkpCD+SfNF3Vj6kvs=';
    public energyAustraliaQRCodeComponent: any = {
        "Account No.": '80919823123',
        "Registered email": 's@energy.com'
    };

    // public energyAustraliaQRCodeComponentArray = [];
    public energyAustraliaQRCodeComponentArray = [{
            key: "Account No.",
            value: "80919823123"
        }, {
            key: "Registered email",
            value: "s@energy.com"
        }
    ];

    public allInOneFraunhoferUseCase = [
        // {
        //     "contactTracing": {
        //         "firstName": {
        //             "value": "", // user.firstname
        //             "trust": 1  // user.firstnameStatus
        //         },
        //         "lastName": {
        //             "value": "", // user.lastname
        //             "trust": 1 // user.firstnameStatus
        //         },
        //         "email": {
        //             "value": "", // user.email
        //             "trust": 1 // user.emailStatus
        //         }
        //     },
        //     "vaccinationCertificates": [{
        //         "v": [
        //             {
        //                 "ci": "URN:UVCI:01DE/A60012262/2QVRGFXVDUBKT0TDJBKVV#R",
        //                 "co": "DE",
        //                 "dn": 1,
        //                 "dt": "2021-05-20",
        //                 "is": "Robert Koch-Institut",
        //                 "ma": "ORG-100001699",
        //                 "mp": "EU/1/21/1529",
        //                 "sd": 2,
        //                 "tg": "840539006",
        //                 "vp": "1119305005"
        //             }
        //         ],
        //         "dob": "1991-12-22",
        //         "nam": {
        //             "fn": "Mahapatra",
        //             "gn": "Ansik",
        //             "fnt": "MAHAPATRA",
        //             "gnt": "ANSIK"
        //         },
        //         "ver": "1.3.0",
        //     }],
        //     "testCertificates": [
        //         "https://verify.govdigital.de/v/gd/#f=Bale;g=Vijay;b=19900323;p=V0881112;i=39053592;d=202108290603;t=PCR;r=n;s=CGadc375dc-dc82-4b4b-a1cf-7ac7b026aa9c"
        //     ],
        //     "ticketInformation": {
        //         "ticketVendorURL": "https://www.eventbriteapi.com/v3/",
        //         "clientId": "", // Private Key: JFQJPVZSCKHEC7D4DCBE
        //         "userId": "333454955979",
        //         "eventId": "128013877955",
        //         "ticketId": "153046823",
        //     }
        // }
    ]

    public healthCertificates = [
        // {
        //   "qrCode": "HC1:6BFOXN%TS3DHPVO13J /G-/2YRVA.Q/R82JD2FCJG96V75DOW%IY17EIHY P8L6IWM$S4U45P84HW6U/4:84LC6 YM::QQHIZC4.OI1RM8ZA.A5:S9MKN4NN3F85QNCY0O%0VZ001HOC9JU0D0HT0HB2PL/IB*09B9LW4T*8+DCMH0LDK2%KI*V AQ2%KYZPQV6YP8722XOE7:8IPC2L4U/6H1D31BLOETI0K/4VMA/.6LOE:/8IL882B+SGK*R3T3+7A.N88J4R$F/MAITHW$P7S3-G9++9-G9+E93ZM$96TV6QRR 1JI7JSTNCA7G6MXYQYYQQKRM64YVQB95326FW4AJOMKMV35U:7-Z7QT499RLHPQ15O+4/Z6E 6U963X7$8Q$HMCP63HU$*GT*Q3-Q4+O7F6E%CN4D74DWZJ$7K+ CZEDB2M$9C1QD7+2K3475J%6VAYCSP0VSUY8WU9SG43A-RALVMO8+-VD2PRPTB7S015SSFW/BE1S1EV*2Q396Q*4TVNAZHJ7N471FPL-CA+2KG-6YPPB7C%40F18N4",
        //   "v": [
        //     {
        //         "ci": "URN:UVCI:01DE/A60012262/2QVRGFXVDUBKT0TDJBKVV#R",
        //         "co": "DE",
        //         "dn": 2,
        //         "dt": "2021-05-20",
        //         "is": "Robert Koch-Institut",
        //         "ma": "ORG-100001699",
        //         "mp": "EU/1/21/1529",
        //         "sd": 2,
        //         "tg": "840539006",
        //         "vp": "1119305005"
        //     }
        // ],
        // "dob": "1991-12-22",
        // "nam": {
        //     "fn": "Mahapatra",
        //     "gn": "Ansik",
        //     "fnt": "MAHAPATRA",
        //     "gnt": "ANSIK"
        // },
        // "ver": "1.3.0",
        // "proof": {
        //     "issuer":"C = IT, O = Ministero della Salute, CN = Italy DGC CSCA 1\nValidity",
        //     "subject":"C = IT, O = Ministero della Salute, CN = Italy DGC DSC 1",
        //     "notbefore":1620800297000,
        //     "notafter":1683871919000,
        //     "pubkey":"-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEnL9+WnIp9fvbcocZSGUFlSw9ffW/\njbMONzcvm1X4c+pXOPEs7C4/83+PxS8Swea2hgm/tKt4PI0z8wgnIehojw==\n-----END PUBLIC KEY-----\n",
        //     "rawX509data":"MIIEDzCCAfegAwIBAgIURldu5rsfrDeZtDBxrJ+SujMr2IswDQYJKoZIhvcNAQELBQAwSTELMAkGA1UEBhMCSVQxHzAdBgNVBAoMFk1pbmlzdGVybyBkZWxsYSBTYWx1dGUxGTAXBgNVBAMMEEl0YWx5IERHQyBDU0NBIDEwHhcNMjEwNTEyMDgxODE3WhcNMjMwNTEyMDgxMTU5WjBIMQswCQYDVQQGEwJJVDEfMB0GA1UECgwWTWluaXN0ZXJvIGRlbGxhIFNhbHV0ZTEYMBYGA1UEAwwPSXRhbHkgREdDIERTQyAxMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEnL9+WnIp9fvbcocZSGUFlSw9ffW/jbMONzcvm1X4c+pXOPEs7C4/83+PxS8Swea2hgm/tKt4PI0z8wgnIehoj6OBujCBtzAfBgNVHSMEGDAWgBS+VOVpXmeSQImXYEEAB/pLRVCw/zBlBgNVHR8EXjBcMFqgWKBWhlRsZGFwOi8vY2Fkcy5kZ2MuZ292Lml0L0NOPUl0YWx5JTIwREdDJTIwQ1NDQSUyMHhcMSxPPU1pbmlzdGVybyUyMGRlbGxhJTIwU2FsdXRlLEM9SVQwHQYDVR0OBBYEFC4bAbCvpArrgZ0E+RrqS8V7TNNIMA4GA1UdDwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAgEAjxTeF7yhKz/3PKZ9+WfgZPaIzZvnO/nmuUartgVd3xuTPNtd5tuYRNS/1B78HNNk7fXiq5hH2q8xHF9yxYxExov2qFrfUMD5HOZzYKHZcjcWFNHvH6jx7qDCtb5PrOgSK5QUQzycR7MgWIFinoWwsWIrA1AJOwfUoi7v1aoWNMK1eHZmR3Y9LQ84qeE2yDk3jqEGjlJVCbgBp7O8emzy2KhWv3JyRZgTmFz7p6eRXDzUYHtJaufveIhkNM/U8p3S7egQegliIFMmufvEyZemD2BMvb97H9PQpuzeMwB8zcFbuZmNl42AFMQ2PhQe27pU0wFsDEqLe0ETb5eR3T9L6zdSrWldw6UuXoYV0/5fvjA55qCjAaLJ0qi16Ca/jt6iKuws/KKh9yr+FqZMnZUH2D2j2i8LBA67Ie0JoZPSojr8cwSTxQBdJFI722uczCj/Rt69Y4sLdV3hNQ2A9hHrXesyQslr0ez3UHHzDRFMVlOXWCayj3LIgvtfTjKrT1J+/3Vu9fvs1+CCJELuC9gtVLxMsdRc/A6/bvW4mAsyY78ROX27Bi8CxPN5IZbtiyjpmdfr2bufDcwhwzdwsdQQDoSiIF1LZqCn7sHBmUhzoPcBJdXFET58EKow0BWcerZzpvsVHcMTE2uuAUr/JUh1SBpoJCiMIRSl+XPoEA2qqYU="
        //  }
        // },
        // {
        //     "qrCode": "HC1:6BFOXN*TS0BI$ZD8UHEANJ:87QB.MCDMH%TA1RO4.S-OP/*IKYLEJP/*I5EQ GD/GPWBILC9FF9 RPR-SKG10EQ928GEQW2D*SL5M8Z.2EO56GQ%01.BV-28ABEUESDFWQAKZM6C7P68IXBG/HS$*S%BKVD9O-OEF82E9GX8$G10QVGB3O1KO-OAGJM*K5B9-NT0 2$$0X4PCY0+-CEV4VCBB70J2HT0HD*213P80P2W4VZ0K1HL$0CNN/%8SY0 +AE3PY3LJCA1W4/GJI+C7*4M:KCY05B9.IPOJ73NVAOVREHMB4O-OLG15JLBY4YW8E.D3HH$LR4+KGZTOD2$RUIVUQOHEH622JB4TR/S+:KND3LJ35AL5:4A933NJIFT D0W46Q3QR$P*NIV1JW$3QMIY1JI3B ZJ83BD 3CQST8TE:2LZIC9J1-IM7JZ0K$PI-2T36JF0JEYI1DLZZL162I52::EJ$9KB75 6EZP%VPQYPW2JHRF8ATQ$VQCV36SRPH7JCDD12.M%/1KQRBMMZQOUER09NG2ED%UMP3N.E3NS2:VIVPXJSH5LE4IV70/YIW4",
        //     "t": [
        //         {
        //             "ci": "URN:UVCI:V1:DE:5UPXHGUN9WFSTOURT6GAXHUPRS",
        //             "co": "DE",
        //             "is": "Robert Koch-Institut",
        //             "tg": "840539006",
        //             "tt": "LP217198-3",
        //             "sc": "2021-09-02T08:23:53Z",
        //             "tr": "260415000",
        //             "tc": "Corona Test Center Frankfurt Bahnhofsviertel",
        //             "ma": "1333"
        //         }
        //     ],
        //     "dob": "1968-11-02",
        //     "nam": {
        //         "fn": "Stevens",
        //         "fnt": "STEVENS",
        //         "gn": "Jan",
        //         "gnt": "JAN"
        //     },
        //     "ver": "1.3.0",
        //     "proof": {
        //         "issuer": "C = DE, O = D-Trust GmbH, CN = D-TRUST CA 2-2 2019, 2.5.4.97 = NTRDE-HRB74346",
        //         "subject": "C = DE, O = Robert Koch-Institut, OU = Elektronischer Impfnachweis, CN = Robert Koch-Institut, L = Berlin, postalCode = 13353, street = Nordufer 20, 2.5.4.97 = DT:DE-3023531445, serialNumber = CSM026523255, ST = Berlin\nSubject Public Key Info:",
        //         "notbefore": 1624003975000,
        //         "notafter": 1687421575000,
        //         "pubkey": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEzIzHRqk76vDGIUAj3dIo3UVev41t\nak5P5oxO+zaOlsKnxUNbo9yqIcpOo7stI8WLrif4RRaiZoQ0tvahSHH2iw==\n-----END PUBLIC KEY-----\n",
        //         "rawX509data": "MIIHMDCCBOigAwIBAgIQetZKpTcV7w1/zH8dDU3sBDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQDBbMQswCQYDVQQGEwJERTEVMBMGA1UEChMMRC1UcnVzdCBHbWJIMRwwGgYDVQQDExNELVRSVVNUIENBIDItMiAyMDE5MRcwFQYDVQRhEw5OVFJERS1IUkI3NDM0NjAeFw0yMTA2MTgxMDEyNTVaFw0yMzA2MjIxMDEyNTVaMIHrMQswCQYDVQQGEwJERTEdMBsGA1UEChMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxJDAiBgNVBAsTG0VsZWt0cm9uaXNjaGVyIEltcGZuYWNod2VpczEdMBsGA1UEAxMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxDzANBgNVBAcTBkJlcmxpbjEOMAwGA1UEEQwFMTMzNTMxFDASBgNVBAkTC05vcmR1ZmVyIDIwMRkwFwYDVQRhExBEVDpERS0zMDIzNTMxNDQ1MRUwEwYDVQQFEwxDU00wMjY1MjMyNTUxDzANBgNVBAgTBkJlcmxpbjBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMyMx0apO+rwxiFAI93SKN1FXr+NbWpOT+aMTvs2jpbCp8VDW6PcqiHKTqO7LSPFi64n+EUWomaENLb2oUhx9oujggLIMIICxDAWBgNVHSUEDzANBgsrBgEEAY43j2UBATAfBgNVHSMEGDAWgBRxEDKudHF7VI7x1qtiVK78PsC7FjAtBggrBgEFBQcBAwQhMB8wCAYGBACORgEBMBMGBgQAjkYBBjAJBgcEAI5GAQYCMIHFBggrBgEFBQcBAQSBuDCBtTBCBggrBgEFBQcwAoY2aHR0cDovL3d3dy5kLXRydXN0Lm5ldC9jZ2ktYmluL0QtVFJVU1RfQ0FfMi0yXzIwMTkuY3J0MG8GCCsGAQUFBzAChmNsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NBQ2VydGlmaWNhdGU/YmFzZT8wcAYDVR0gBGkwZzAJBgcEAIvsQAEBMFoGCysGAQQBpTQCgRYFMEswSQYIKwYBBQUHAgEWPWh0dHA6Ly93d3cuZC10cnVzdC5uZXQvaW50ZXJuZXQvZmlsZXMvRC1UUlVTVF9DU01fUEtJX0NQUy5wZGYwgfAGA1UdHwSB6DCB5TCB4qCB36CB3IYyaHR0cDovL2NybC5kLXRydXN0Lm5ldC9jcmwvZC10cnVzdF9jYV8yLTJfMjAxOS5jcmyGO2h0dHA6Ly9jZG4uZC10cnVzdC1jbG91ZGNybC5uZXQvY3JsL2QtdHJ1c3RfY2FfMi0yXzIwMTkuY3JshmlsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NlcnRpZmljYXRlcmV2b2NhdGlvbmxpc3QwHQYDVR0OBBYEFH9SuXOB5Uv5T5OXoY1iudAzPsWbMA4GA1UdDwEB/wQEAwIGwDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQAOCAgEAy16LPyYw6dpE4YqZoIbKbl5yltnnsXY5JoIT30VnkjBqlG46DpaPYj2Zu0/iMKAe5CFQgRBjg5Vn/i5KRWKQE/HGpFpX4KzPWuPFmd5T9bzVLh7y3SidPUR0/kC3ZfVz2P5BdQepGSRyu72ef47qzgW26gtXiYFPcs9HE5VtoJgEcEqEPbpW6JyWG5zLZ4IV+E8YpZ6E69fhlVyiPiNQCmoF8ukl/JPbgCoCGeWJkmZkTB7/yRnFBsQSPRph4GsgLMaTKbLMzKOA7a5H6ZxRY3Diid11f9UWKgYKyNMZbbCkrn9bD+0scMIPYl0LdRI7HIXxGy2HDe0Rp+0qS8WA8zJ7L0N6EiGDwKBeFyhoyR0OIv7NMPgqdmX5KGJxDxaBAEbNyZtVOo7GqJCOybMlv1xs9iKdSCYtUeJanRPZ7xvncFUuTS39WgCJNAE1tLX6ScP1PBRaiA4wRkOM9sEf5OXFq+LF95TGML8gsUuyaoNUBvxIsN+KrKWQIGZNDDqIbQx72AsEbLpW60a0T4vd9nYmllAY9O3jwl0NColkYYcSjLkR2Fv1/C7fdVAiEz3kf8j3QWN+dajjKg2hxbzThZXW3alInwrKhPW+rtYFdOGxFnJ7cRhKQ4L71MJ1x0q7Z7LY84MTTr3Du+HnigyN7GYpEVcWbdFwiEMoLQ51X00="
        //     }
        // },
        // {
        //     "qrCode": "https://s.coronawarn.app?v=1#eyJmbiI6IkphbiIsImxuIjoiU3RldmVucyIsImRvYiI6IjE5NjgtMTEtMDIiLCJ0aW1lc3RhbXAiOjE2MzA1NzEwMzMsInRlc3RpZCI6IjQzNC0wMDQzOTE2YWNhZWE1NDVkNmYxZDFlYzdkNGJjM2Q4ZC00OTAwMDkxIiwic2FsdCI6IkY3RDEwODRFODc0MTFBNDM5NTk1Q0U2MURGNDUyRTZGIiwiZGdjIjp0cnVlLCJoYXNoIjoiNWM1MGNjNDU2MDUwNjk1MGU4Yzk5YTg2NTg2MDM4MzRlMTdhOWVkMGJjYjc1NTE5ZGI2MDYxNjBiNGUwOTlkMSJ9",
        //     "t": [
        //         {
        //             fn: 'Jan',
        //             ln: 'Stevens',
        //             dob: '1968-11-02',
        //             timestamp: 1630571033,
        //             testid: '434-0043916acaea545d6f1d1ec7d4bc3d8d-4900091',
        //             salt: 'F7D1084E87411A439595CE61DF452E6F',
        //             dgc: true,
        //             hash: '5c50cc4560506950e8c99a8658603834e17a9ed0bcb75519db606160b4e099d1'
        //         }
        //     ],
        // },
        // {
        //     "qrCode": "https://testverify.io/v1#eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ2IjoyLCJuIjoiYjBiM2JjMjkwMzFhZTMxOTRlNWE2MjIyNmUyMmIyOTdiMjQzMjgxNWIxODFiOWVmYzBmNGI4MWYxZWZkMTkyOCIsInQiOjE2MzA1NjQzMzcsImMiOiJmIiwiciI6Im4iLCJsIjoiVGVzdHplbnRydW0gRnJhbmtmdXJ0IEhhdXB0YmFobmhvZiIsImQiOiJEci4gbWVkLiBKYXNjaGFyIE0uIEtlcm1hbnkiLCJlZCI6IjV5dmNscUpcL0V2UmQ1MkJ0WndPYlwvMjJYNUp1M25pUGNLUGZpb2RlTVNhaGZFRmNvbEtxazVBcm1lSWlcL05NbXV1ZGFvMzRkZ3c1dmowYitXNXN1aG5XVFlmSFZ1Wk4rd1MwdGFCanNRTWF0bFNHNXpWV1RrbEFZRjMwQUpyY0FsIn0.J8xs2fvjSPoC-5OIMcenH6jH1JC_OQkECvwXQo6HOIbtq_zDhainLk71845okpW8eXzZwAqS6I4sgPrKdi5PwJiWWhcuEa888qiygnzzrZvUQ2h3luJnFfJD--PN4XIrtFWJf6SFxRkbBNcsA6CeJgqG7D5SHv1Dc9LFYiFmux02TQJuUou5eCDJdeFWVKJ3vZkW3ZeRVqyLcmVXFUe1k-ovQBq7Zh3oyuEdM0WLAVD7xhjhCrA0aAHfxc9CYwob_p9kYMjNpNaid-BCzH_R_wNZXD2QW7ZE4ZOLIEMQJS_LncHwk27H9IOTQiYwcrz0oNNp3FGKLCdpWnksexpwvw",
        //     "t": [
        //         {
        //             v: 2,
        //             n: 'b0b3bc29031ae3194e5a62226e22b297b2432815b181b9efc0f4b81f1efd1928',
        //             t: 1630564337,
        //             c: 'f',
        //             r: 'n',
        //             l: 'Testzentrum Frankfurt Hauptbahnhof',
        //             d: 'Dr. med. Jaschar M. Kermany',
        //             ed: '5yvclqJ/EvRd52BtZwOb/22X5Ju3niPcKPfiodeMSahfEFcolKqk5ArmeIi/NMmuudao34dgw5vj0b+W5suhnWTYfHVuZN+wS0taBjsQMatlSG5zVWTklAYF30AJrcAl'
        //         }
        //     ],
        // },
        // {
        //     "qrCode": "https://verify.govdigital.de/v/gd/#f=Bale;g=Vijay;b=19900323;p=V0881112;i=39053592;d=202108290603;t=PCR;r=n;s=CGadc375dc-dc82-4b4b-a1cf-7ac7b026aa9c",
        //     "t": [
        //         {
        //             f: 'Bale',
        //             g: 'Vijay',
        //             b: '19900323',
        //             p: 'V0881112',
        //             i: '39053592',
        //             d: '202108290603',
        //             t: 'PCR',
        //             r: 'n',
        //             s: 'CGadc375dc-dc82-4b4b-a1cf-7ac7b026aa9c'
        //           }
        //     ],
        // },
        // {
        //     "qrCode": "HC1:6BFOXN*TS0BI$ZD8UHEANJ:87QB.MCMMH%TA1RO4.S-OP/*IKYLEJP/*I5EQ GD/GPWBILC9FF9 RPR-SKG10EQ928GEQW2D*SL5M8Z.2EO56GQ%01.BV-28ABEUESDFWQAKZM6C7P68IXBG/HS$*S%BKVD9O-OEF82E9GX8$G10QVGB3O1KO-OAGJM*K5B9-NT0 2$$0X4PCY0+-CEV4VCBB70J2HT0HD*213P80P2W4VZ0K1HL$0CNN/%8SY0 +AE3PY3LJCA1W4/GJI+C7*4M:KCY05B9.IPOJ73NVAOVREHMB4O-OLG15JLBY4YW8E.D3HH$LR4+KGZTOD2$RUIVUQOHEH622JB4TR/S+:KND3LJ35AL5:4A933NJIFT D0W46Q3QR$P*NIV1JW$3QMIY1JI3B ZJ83BD 3CQST8TE:2LZIC9J1-IM7JZ0K$PI-2T36JF0JEYI1DLZZL162I52::EJ$9KB75 6EZP%VPQYPW2JHRF8ATQ$VQCV36SRPH7JCDD12.M%/1KQRBMMZQOUER09NG2ED%UMP3N.E3NS2:VIVPXJSH5LE4IV70/YIW4",
        //     "r": [
        //         {
        //             "ci": "URN:UVCI:V1:DE:5UPXHGUN9WFSTOURT6GAXHUPRS",
        //             "co": "DE",
        //             "is": "Robert Koch-Institut",
        //             "tg": "840539006",
        //             "fr": "2020-12-19",
        //             "df": "2021-06-01",
        //             "du": "2021-09-01"
        //         }
        //     ],
        //     "dob": "1968-11-02",
        //     "nam": {
        //         "fn": "Stevens",
        //         "fnt": "STEVENS",
        //         "gn": "Jan",
        //         "gnt": "JAN"
        //     },
        //     "ver": "1.3.0",
        //     "proof": {
        //         "issuer": "C = DE, O = D-Trust GmbH, CN = D-TRUST CA 2-2 2019, 2.5.4.97 = NTRDE-HRB74346",
        //         "subject": "C = DE, O = Robert Koch-Institut, OU = Elektronischer Impfnachweis, CN = Robert Koch-Institut, L = Berlin, postalCode = 13353, street = Nordufer 20, 2.5.4.97 = DT:DE-3023531445, serialNumber = CSM026523255, ST = Berlin\nSubject Public Key Info:",
        //         "notbefore": 1624003975000,
        //         "notafter": 1687421575000,
        //         "pubkey": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEzIzHRqk76vDGIUAj3dIo3UVev41t\nak5P5oxO+zaOlsKnxUNbo9yqIcpOo7stI8WLrif4RRaiZoQ0tvahSHH2iw==\n-----END PUBLIC KEY-----\n",
        //         "rawX509data": "MIIHMDCCBOigAwIBAgIQetZKpTcV7w1/zH8dDU3sBDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQDBbMQswCQYDVQQGEwJERTEVMBMGA1UEChMMRC1UcnVzdCBHbWJIMRwwGgYDVQQDExNELVRSVVNUIENBIDItMiAyMDE5MRcwFQYDVQRhEw5OVFJERS1IUkI3NDM0NjAeFw0yMTA2MTgxMDEyNTVaFw0yMzA2MjIxMDEyNTVaMIHrMQswCQYDVQQGEwJERTEdMBsGA1UEChMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxJDAiBgNVBAsTG0VsZWt0cm9uaXNjaGVyIEltcGZuYWNod2VpczEdMBsGA1UEAxMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxDzANBgNVBAcTBkJlcmxpbjEOMAwGA1UEEQwFMTMzNTMxFDASBgNVBAkTC05vcmR1ZmVyIDIwMRkwFwYDVQRhExBEVDpERS0zMDIzNTMxNDQ1MRUwEwYDVQQFEwxDU00wMjY1MjMyNTUxDzANBgNVBAgTBkJlcmxpbjBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMyMx0apO+rwxiFAI93SKN1FXr+NbWpOT+aMTvs2jpbCp8VDW6PcqiHKTqO7LSPFi64n+EUWomaENLb2oUhx9oujggLIMIICxDAWBgNVHSUEDzANBgsrBgEEAY43j2UBATAfBgNVHSMEGDAWgBRxEDKudHF7VI7x1qtiVK78PsC7FjAtBggrBgEFBQcBAwQhMB8wCAYGBACORgEBMBMGBgQAjkYBBjAJBgcEAI5GAQYCMIHFBggrBgEFBQcBAQSBuDCBtTBCBggrBgEFBQcwAoY2aHR0cDovL3d3dy5kLXRydXN0Lm5ldC9jZ2ktYmluL0QtVFJVU1RfQ0FfMi0yXzIwMTkuY3J0MG8GCCsGAQUFBzAChmNsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NBQ2VydGlmaWNhdGU/YmFzZT8wcAYDVR0gBGkwZzAJBgcEAIvsQAEBMFoGCysGAQQBpTQCgRYFMEswSQYIKwYBBQUHAgEWPWh0dHA6Ly93d3cuZC10cnVzdC5uZXQvaW50ZXJuZXQvZmlsZXMvRC1UUlVTVF9DU01fUEtJX0NQUy5wZGYwgfAGA1UdHwSB6DCB5TCB4qCB36CB3IYyaHR0cDovL2NybC5kLXRydXN0Lm5ldC9jcmwvZC10cnVzdF9jYV8yLTJfMjAxOS5jcmyGO2h0dHA6Ly9jZG4uZC10cnVzdC1jbG91ZGNybC5uZXQvY3JsL2QtdHJ1c3RfY2FfMi0yXzIwMTkuY3JshmlsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NlcnRpZmljYXRlcmV2b2NhdGlvbmxpc3QwHQYDVR0OBBYEFH9SuXOB5Uv5T5OXoY1iudAzPsWbMA4GA1UdDwEB/wQEAwIGwDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQAOCAgEAy16LPyYw6dpE4YqZoIbKbl5yltnnsXY5JoIT30VnkjBqlG46DpaPYj2Zu0/iMKAe5CFQgRBjg5Vn/i5KRWKQE/HGpFpX4KzPWuPFmd5T9bzVLh7y3SidPUR0/kC3ZfVz2P5BdQepGSRyu72ef47qzgW26gtXiYFPcs9HE5VtoJgEcEqEPbpW6JyWG5zLZ4IV+E8YpZ6E69fhlVyiPiNQCmoF8ukl/JPbgCoCGeWJkmZkTB7/yRnFBsQSPRph4GsgLMaTKbLMzKOA7a5H6ZxRY3Diid11f9UWKgYKyNMZbbCkrn9bD+0scMIPYl0LdRI7HIXxGy2HDe0Rp+0qS8WA8zJ7L0N6EiGDwKBeFyhoyR0OIv7NMPgqdmX5KGJxDxaBAEbNyZtVOo7GqJCOybMlv1xs9iKdSCYtUeJanRPZ7xvncFUuTS39WgCJNAE1tLX6ScP1PBRaiA4wRkOM9sEf5OXFq+LF95TGML8gsUuyaoNUBvxIsN+KrKWQIGZNDDqIbQx72AsEbLpW60a0T4vd9nYmllAY9O3jwl0NColkYYcSjLkR2Fv1/C7fdVAiEz3kf8j3QWN+dajjKg2hxbzThZXW3alInwrKhPW+rtYFdOGxFnJ7cRhKQ4L71MJ1x0q7Z7LY84MTTr3Du+HnigyN7GYpEVcWbdFwiEMoLQ51X00="
        //     }
        // },
        // {
        //     "qrCode": "HC1:6FOXN*TS0BI$ZD8UHEANJ:87QB.MCMMH%TA1RO4.S-OP/*IKYLEJP/*I5EQ GD/GPWBILC9FF9 RPR-SKG10EQ928GEQW2D*SL5M8Z.2EO56GQ%01.BV-28ABEUESDFWQAKZM6C7P68IXBG/HS$*S%BKVD9O-OEF82E9GX8$G10QVGB3O1KO-OAGJM*K5B9-NT0 2$$0X4PCY0+-CEV4VCBB70J2HT0HD*213P80P2W4VZ0K1HL$0CNN/%8SY0 +AE3PY3LJCA1W4/GJI+C7*4M:KCY05B9.IPOJ73NVAOVREHMB4O-OLG15JLBY4YW8E.D3HH$LR4+KGZTOD2$RUIVUQOHEH622JB4TR/S+:KND3LJ35AL5:4A933NJIFT D0W46Q3QR$P*NIV1JW$3QMIY1JI3B ZJ83BD 3CQST8TE:2LZIC9J1-IM7JZ0K$PI-2T36JF0JEYI1DLZZL162I52::EJ$9KB75 6EZP%VPQYPW2JHRF8ATQ$VQCV36SRPH7JCDD12.M%/1KQRBMMZQOUER09NG2ED%UMP3N.E3NS2:VIVPXJSH5LE4IV70/YIW4",
        //     "r": [
        //         {
        //             "ci": "URN:UVCI:V1:DE:5UPXHGUN9WFSTOURT6GAXHUPRS",
        //             "co": "DE",
        //             "is": "Robert Koch-Institut",
        //             "tg": "840539006",
        //             "fr": "2020-12-19",
        //             "df": "2021-06-01",
        //             "du": "2021-09-01"
        //         }
        //     ],
        //     "dob": "1968-11-02",
        //     "nam": {
        //         "fn": "Stevens",
        //         "fnt": "STEVENS",
        //         "gn": "Jan",
        //         "gnt": "JAN"
        //     },
        //     "ver": "1.3.0",
        //     "proof": {
        //         "issuer": "C = DE, O = D-Trust GmbH, CN = D-TRUST CA 2-2 2019, 2.5.4.97 = NTRDE-HRB74346",
        //         "subject": "C = DE, O = Robert Koch-Institut, OU = Elektronischer Impfnachweis, CN = Robert Koch-Institut, L = Berlin, postalCode = 13353, street = Nordufer 20, 2.5.4.97 = DT:DE-3023531445, serialNumber = CSM026523255, ST = Berlin\nSubject Public Key Info:",
        //         "notbefore": 1624003975000,
        //         "notafter": 1687421575000,
        //         "pubkey": "-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEzIzHRqk76vDGIUAj3dIo3UVev41t\nak5P5oxO+zaOlsKnxUNbo9yqIcpOo7stI8WLrif4RRaiZoQ0tvahSHH2iw==\n-----END PUBLIC KEY-----\n",
        //         "rawX509data": "MIIHMDCCBOigAwIBAgIQetZKpTcV7w1/zH8dDU3sBDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQDBbMQswCQYDVQQGEwJERTEVMBMGA1UEChMMRC1UcnVzdCBHbWJIMRwwGgYDVQQDExNELVRSVVNUIENBIDItMiAyMDE5MRcwFQYDVQRhEw5OVFJERS1IUkI3NDM0NjAeFw0yMTA2MTgxMDEyNTVaFw0yMzA2MjIxMDEyNTVaMIHrMQswCQYDVQQGEwJERTEdMBsGA1UEChMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxJDAiBgNVBAsTG0VsZWt0cm9uaXNjaGVyIEltcGZuYWNod2VpczEdMBsGA1UEAxMUUm9iZXJ0IEtvY2gtSW5zdGl0dXQxDzANBgNVBAcTBkJlcmxpbjEOMAwGA1UEEQwFMTMzNTMxFDASBgNVBAkTC05vcmR1ZmVyIDIwMRkwFwYDVQRhExBEVDpERS0zMDIzNTMxNDQ1MRUwEwYDVQQFEwxDU00wMjY1MjMyNTUxDzANBgNVBAgTBkJlcmxpbjBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMyMx0apO+rwxiFAI93SKN1FXr+NbWpOT+aMTvs2jpbCp8VDW6PcqiHKTqO7LSPFi64n+EUWomaENLb2oUhx9oujggLIMIICxDAWBgNVHSUEDzANBgsrBgEEAY43j2UBATAfBgNVHSMEGDAWgBRxEDKudHF7VI7x1qtiVK78PsC7FjAtBggrBgEFBQcBAwQhMB8wCAYGBACORgEBMBMGBgQAjkYBBjAJBgcEAI5GAQYCMIHFBggrBgEFBQcBAQSBuDCBtTBCBggrBgEFBQcwAoY2aHR0cDovL3d3dy5kLXRydXN0Lm5ldC9jZ2ktYmluL0QtVFJVU1RfQ0FfMi0yXzIwMTkuY3J0MG8GCCsGAQUFBzAChmNsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NBQ2VydGlmaWNhdGU/YmFzZT8wcAYDVR0gBGkwZzAJBgcEAIvsQAEBMFoGCysGAQQBpTQCgRYFMEswSQYIKwYBBQUHAgEWPWh0dHA6Ly93d3cuZC10cnVzdC5uZXQvaW50ZXJuZXQvZmlsZXMvRC1UUlVTVF9DU01fUEtJX0NQUy5wZGYwgfAGA1UdHwSB6DCB5TCB4qCB36CB3IYyaHR0cDovL2NybC5kLXRydXN0Lm5ldC9jcmwvZC10cnVzdF9jYV8yLTJfMjAxOS5jcmyGO2h0dHA6Ly9jZG4uZC10cnVzdC1jbG91ZGNybC5uZXQvY3JsL2QtdHJ1c3RfY2FfMi0yXzIwMTkuY3JshmlsZGFwOi8vZGlyZWN0b3J5LmQtdHJ1c3QubmV0L0NOPUQtVFJVU1QlMjBDQSUyMDItMiUyMDIwMTksTz1ELVRydXN0JTIwR21iSCxDPURFP2NlcnRpZmljYXRlcmV2b2NhdGlvbmxpc3QwHQYDVR0OBBYEFH9SuXOB5Uv5T5OXoY1iudAzPsWbMA4GA1UdDwEB/wQEAwIGwDA9BgkqhkiG9w0BAQowMKANMAsGCWCGSAFlAwQCA6EaMBgGCSqGSIb3DQEBCDALBglghkgBZQMEAgOiAwIBQAOCAgEAy16LPyYw6dpE4YqZoIbKbl5yltnnsXY5JoIT30VnkjBqlG46DpaPYj2Zu0/iMKAe5CFQgRBjg5Vn/i5KRWKQE/HGpFpX4KzPWuPFmd5T9bzVLh7y3SidPUR0/kC3ZfVz2P5BdQepGSRyu72ef47qzgW26gtXiYFPcs9HE5VtoJgEcEqEPbpW6JyWG5zLZ4IV+E8YpZ6E69fhlVyiPiNQCmoF8ukl/JPbgCoCGeWJkmZkTB7/yRnFBsQSPRph4GsgLMaTKbLMzKOA7a5H6ZxRY3Diid11f9UWKgYKyNMZbbCkrn9bD+0scMIPYl0LdRI7HIXxGy2HDe0Rp+0qS8WA8zJ7L0N6EiGDwKBeFyhoyR0OIv7NMPgqdmX5KGJxDxaBAEbNyZtVOo7GqJCOybMlv1xs9iKdSCYtUeJanRPZ7xvncFUuTS39WgCJNAE1tLX6ScP1PBRaiA4wRkOM9sEf5OXFq+LF95TGML8gsUuyaoNUBvxIsN+KrKWQIGZNDDqIbQx72AsEbLpW60a0T4vd9nYmllAY9O3jwl0NColkYYcSjLkR2Fv1/C7fdVAiEz3kf8j3QWN+dajjKg2hxbzThZXW3alInwrKhPW+rtYFdOGxFnJ7cRhKQ4L71MJ1x0q7Z7LY84MTTr3Du+HnigyN7GYpEVcWbdFwiEMoLQ51X00="
        //     }
        // }
    ];

    public scannedCertificate = [
        // {
        //     "@context": "https://www.w3.org/2018/credentials/v1",
        //     "id": "vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
        //     "type": [
        //         "VerifiableCredential",
        //         "HelixMarketplace"
        //     ],
        //     "issuer": {
        //         "id": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b"
        //     },
        //     "credentialSubject": {
        //         "id": "did:evan:testcore:0x1ee6bfe7034fdbc5ac5d57fb171b3820297d7711",
        //         "data": [
        //             {
        //                 "companyName": "Fraunhofer",
        //                 "header": "",
        //                 "logo": "https://drive.google.com/uc?export=view&id=1qsRzoWSfAU0bk_h9vA6mx6dLHI43PwnV",
        //                 "issueDate": "2020-04-28T14:14:34.023Z",
        //                 "expiryDate": "2022-04-28T14:14:34.023Z",
        //                 "location": "Blockchain Reallabor, Euronova Campus, Halle 6, An der Hasenkaule 10, 50354 Hürth",
        //                 "issuer": "Blockchain Reallabor"
        //             }
        //         ]
        //     },
        //     "validFrom": "1970-01-19T09:08:03Z",
        //     "validTo": "1970-01-20T02:39:15Z",
        //     "credentialStatus": {
        //         "id": "https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x139d96e127e5b258bfe66e4616a4ac2a271c0e86442bb0b119caad85e7a3be5b",
        //         "type": "evan:evanCredential"
        //     },
        //     "proof": {
        //         "type": "EcdsaPublicKeySecp256k1",
        //         "created": "2020-04-28T14:14:34.023Z",
        //         "jws": "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NkstUiJ9.eyJpYXQiOjE1ODgwODMyNzMsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJLWUMiXSwiaWQiOiJ2YzpldmFuOnRlc3Rjb3JlOjB4MTM5ZDk2ZTEyN2U1YjI1OGJmZTY2ZTQ2MTZhNGFjMmEyNzFjMGU4NjQ0MmJiMGIxMTljYWFkODVlN2EzYmU1YiIsImlzc3VlciI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4MWVlNmJmZTcwMzRmZGJjNWFjNWQ1N2ZiMTcxYjM4MjAyOTdkNzcxMSIsImRhdGEiOnsidHJ1c3RlZEJ5IjoiQXV0aGFkYSIsInRydXN0TGV2ZWwiOjEsInRydXN0VmFsdWUiOjEsImlkIjpudWxsLCJ1c2VyIjpudWxsLCJsYXN0TW9kaWZlZCI6bnVsbCwiZ2VuZGVyIjoiZGl2ZXJzIiwidGl0bGUiOiJQcm9mLiBEci4iLCJjYWxsbmFtZSI6InRoZSByb2NrIiwiZmlyc3RuYW1lIjoiRXJpa2EiLCJtaWRkbGVuYW1lIjoiTWFyaWEiLCJsYXN0bmFtZSI6Ik11c3Rlcm1hbiIsIm1hcml0YWxzdGF0dXMiOiJ3aWRvd2VkIiwibWFpZGVubmFtZSI6Ik11c3RlcmZyYXUiLCJlbWFpbHR5cGUiOiJ0ZXh0LW9ubHkiLCJlbWFpbCI6ImVyaWthLm11c3Rlcm1hbkBibG9ja2NoYWluLWhlbGl4LmNvbSIsInBob25ldHlwZSI6Im1vYmxpZSIsInBob25lIjoiMDE3NTY3NDUyMyIsImRhdGVvZmJpcnRoIjoiMTkyMC0wNi0yMlQyMzowMDowMFoiLCJjaXR5b2ZiaXJ0aCI6IlN0b2NraG9sbSIsImNvdW50cnlvZmJpcnRoIjoiU2Nod2VkZW4iLCJjaXRpemVuc2hpcCI6Imdlcm1hbiIsInN0cmVldCI6IkxpbmRlbnN0cmFzZSA0MiIsImNpdHkiOiJnZXJtYW4iLCJ6aXAiOiIxMDk2OSIsInN0YXRlIjoiQmVybGluIiwiY291bnRyeSI6IkRldXRzY2hsYW5kIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudHR5cGUiOiJpZC1jYXJkIiwiaWRlbnRpZmljYXRpb25kb2N1bWVudG51bWJlciI6IkM1TlZQQ1o1RSIsImlkZW50aWZpY2F0aW9uaXNzdWVjb3VudHJ5IjoiZ2VybWFueSIsImlkZW50aWZpY2F0aW9uaXNzdWVkYXRlIjoiMjAxNS0wMi0yOFQyMzowMDowMFoiLCJpZGVudGlmaWNhdGlvbmV4cGlyeWRhdGUiOiIyMDI1LTAyLTI4VDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2Vkb2N1bWVudG51bWJlciI6IkExNzE4MTkyMCIsImRyaXZlcmxpY2VuY2Vjb3VudHJ5IjoiZ2VybWFueSIsImRyaXZlcmxpY2VuY2Vpc3N1ZWRhdGUiOiIyMDIwLTAxLTMxVDIzOjAwOjAwWiIsImRyaXZlcmxpY2VuY2VleHBpcnlkYXRlIjoiMjAzMC0wMS0zMVQyMzowMDowMFoiLCJ0YXhyZXNpZGVuY3kiOiJnZXJtYW55IiwidGF4bnVtYmVyIjoiWDQzN04yMzg2NyIsInRlcm1zb2Z1c2UiOiJUZXJtc29mdXNlIiwidGVybXNvZnVzZXRoaXJkcGFydGllcyI6IlRlcm1zb2Z1c2V0aGlyZHBhcnRpZXMifX0sInZhbGlkRnJvbSI6IjE5NzAtMDEtMTlUMDk6MDg6MDNaIiwidmFsaWRUbyI6IjE5NzAtMDEtMjBUMDI6Mzk6MTVaIiwiY3JlZGVudGlhbFN0YXR1cyI6eyJpZCI6Imh0dHBzOi8vdGVzdGNvcmUuZXZhbi5uZXR3b3JrL3NtYXJ0LWFnZW50cy9zbWFydC1hZ2VudC1kaWQtcmVzb2x2ZXIvdmMvc3RhdHVzL3ZjOmV2YW46dGVzdGNvcmU6MHgxMzlkOTZlMTI3ZTViMjU4YmZlNjZlNDYxNmE0YWMyYTI3MWMwZTg2NDQyYmIwYjExOWNhYWQ4NWU3YTNiZTViIiwidHlwZSI6ImV2YW46ZXZhbkNyZWRlbnRpYWwifX0sImlzcyI6ImRpZDpldmFuOnRlc3Rjb3JlOjB4N2NmZThlZTQ1YWE1MTM4YWRmMjdjODFiMTg5MzkwOTQwOTU2MTg2YiJ9.ABk8RBlpCAX-gY47_aRxskCCrl_AsluEDbYNEY8BNlY2NS78aoIWXDNNhoJe0J5EpwnWQyR02I9qMn_lqL0qFwE",
        //         "proofPurpose": "assertionMethod",
        //         "verificationMethod": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b#key-1"
        //     }
        // }
    ];

    constructor(
        private readonly lockService: LockService,
        private readonly contactService: ContactService,
        private readonly userService: UserProviderService,
        private readonly translateProviderService: TranslateProviderService,
        private readonly popoverController: PopoverController,
        private readonly toastController: ToastController,
        private readonly alertController: AlertController,
        private readonly openNativeSettings: OpenNativeSettings,
        private readonly apiService: ApiProviderService,
        private _CryptoProviderService: CryptoProviderService,
        // public _ChatService: ChatService,
        private _ModalController: ModalController,
        private _LoaderService: LoaderProviderService,
        private _SecureStorageService: SecureStorageService,
        private _CryptojsProviderService: CryptojsProviderService,
        private _WalletConnectProvider: WalletConnectService,
        private _DidVcService: DidVcService,
        private nav: NavController
    ) {
    }

    public setHealthCertificates(): Promise<void> {
        return new Promise((resolve) => {
            this._SecureStorageService.getValue(SecureStorageKey.healthCertificates, false).then((h) => {
                this.healthCertificates = !!h ? JSON.parse(h) : this.healthCertificates;
                resolve();
            });
        })
    }

    public setCertificates(): Promise<void> {
        return new Promise((resolve) => {
            this._SecureStorageService.getValue(SecureStorageKey.marketplaceCertificates, false).then((h) => {
                this.scannedCertificate = !!h ? JSON.parse(h) : this.scannedCertificate;
                resolve();
            });
        })
    }

    private startBarcodeScanningModal(): Promise<Barcode | undefined> {

        return new Promise<Barcode | undefined>(async (resolve) => {

            const status = await BarcodeScanner.checkPermissions();
            // console.log('BarcodeService#startBarcodeScanningModal; status:', status);

            if (status.camera === 'prompt' || status.camera === 'prompt-with-rationale') {
                const permissions = await BarcodeScanner.requestPermissions();
                // console.log('BarcodeService#startBarcodeScanningModal; permissions:', permissions);

                if (permissions.camera !== 'granted') {
                    resolve(null);
                    return;
                }
            } else if (status.camera === 'denied') {
                await this.presentAlertForSettingsPage();

                const status = await BarcodeScanner.checkPermissions();
                // console.log('BarcodeService#startBarcodeScanningModal; status:', status);

                if (status.camera !== 'granted') {
                    resolve(null);
                    return;
                }
            }

            const modal = await this._ModalController.create({
                component: BarcodeScanningModalComponent,
                // Set `visibility` to `visible` to show the modal (see `src/theme/variables.scss`)
                cssClass: 'barcode-scanning-modal',
                showBackdrop: false,
                componentProps: {
                    formats: [ BarcodeFormat.QrCode ],
                    lensFacing: LensFacing.Back,
                },
            });

            this.lockService.removeResume();

            await modal.present();

            const result = await modal.onDidDismiss();
            // console.log('BarcodeService#startBarcodeScanningModal; result:', result);

            this.lockService.addResume();

            resolve(result.data?.barcode);
        });


    }


    public async shortcutsPageModification(): Promise<void> {
        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();
                    // console.log('BarcodeServic#shortcutsPageModification; barcodeData', barcodeData);

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }
                    if (barcodeData) {
                        const sourceData = barcodeData?.displayValue;

                        let isVerfiableCredential = false;
                        let isVerfiablePresentation = false;
                        let issuerId = null;

                        try {
                            var jsonParse = JSON.parse(sourceData);
                        } catch(e) {
                            jsonParse = null;
                        }

                        if(jsonParse) {
                            if(Object.keys(jsonParse).includes("type")){
                                isVerfiableCredential = jsonParse["type"].includes("VerifiableCredential");
                                isVerfiablePresentation = jsonParse["type"].includes("VerifiablePresentation");
                            }
                        }

                        if(isVerfiableCredential) {
                            if(!!jsonParse["issuer"]) {
                                if(typeof jsonParse["issuer"] == "string") {
                                    issuerId = jsonParse["issuer"];
                                    jsonParse = Object.assign(jsonParse, { issuer: { id: issuerId } });
                                }
                            }

                            await this._DidVcService.offlineValidateVc(jsonParse);

                            // this._DidVcService.validateVc(jsonParse).subscribe({
                            //     next: (v) => {
                            //         console.info("this._DidVcService.validateVc: ", JSON.stringify(v));
                            //     },
                            //     error: (e) => {
                            //         console.log("Error: this._DidVcService.validateVc: ", JSON.stringify(e));
                            //         this.presentToast(e.error.error);
                            //     }
                            // })
                            return;
                        }

                        if(isVerfiablePresentation) {
                            if(!!jsonParse["issuer"]) {
                                if(typeof jsonParse["issuer"] == "string") {
                                    issuerId = jsonParse["issuer"];
                                    jsonParse = Object.assign(jsonParse, { issuer: { id: issuerId } });
                                }
                            }
                            this._DidVcService.validateVp(jsonParse).subscribe({
                                next: (v) => {
                                    console.info("this._DidVcService.validateVp: ", JSON.stringify(v));
                                },
                                error: (e) => {
                                    console.log("Error: this._DidVcService.validateVp: ", JSON.stringify(e));
                                    this.presentToast(e.error.error);
                                }
                            })
                            return;
                        }

                        if(sourceData.startsWith('wc:')) {
                            await this._WalletConnectProvider.processDeeplink(sourceData, false);
                        }
                        else if(sourceData.startsWith('V0;')) {
                            this.DSFinVK(sourceData);
                        }
                        else if(sourceData.indexOf('/contact?string=') > -1) {
                            const cont = decodeURIComponent(new URL(sourceData).searchParams.get('string'));

                            const res = await this.contactService.decryptContactId(cont);
                            const decryptedQRObject = JSON.parse(res);

                            const displayContactId = decryptedQRObject.id;
                            const displayContactHelixUsername = decryptedQRObject.helixUsername;
                            const displayContactChatUsername = decryptedQRObject.chatUsername;
                            const displayContactPublicKey = decryptedQRObject.chatPublicKey;

                            const checkUserCanBeAdded = await this.checkUserCanBeAdded(displayContactId, displayContactChatUsername);
                            if (!checkUserCanBeAdded) {
                                return this.lockService.addResume();
                            }

                            const hasImage = decryptedQRObject.hasImage;

                            if ( hasImage ) {
                                this.apiService.getPublicImage(displayContactId)
                                    .pipe(take(1))
                                    .subscribe(blob => {
                                        const reader = new FileReader();
                                        reader.readAsDataURL(blob);
                                        reader.onloadend = async () => {

                                            const pictureBase64 = (reader.result as string);

                                            this.openPopover(
                                                displayContactId,
                                                displayContactHelixUsername,
                                                displayContactChatUsername,
                                                displayContactPublicKey,
                                                pictureBase64
                                            );
                                        };
                                    });
                            } else {
                                await this.openPopover(
                                    displayContactId,
                                    displayContactHelixUsername,
                                    displayContactChatUsername,
                                    displayContactPublicKey,
                                    null
                                );
                            }
                        }
                        else if((sourceData.indexOf("/ageverification?string=") > -1)) {
                            this.lockService.addResume();
                            const request = new URL(sourceData).searchParams.get('string');
                            console.info("request: ", request);
                            try {
                                var decoded = decodeURIComponent(request);
                            } catch(e) {
                                decoded = request;
                            }
                            const extracted = this._CryptojsProviderService.decrypt(decoded, UDIDNonce.energy);
                            console.info("extracted: ", extracted);
                            const ob2 = {
                                dataAccessProcessId: extracted.dataAccessProcessId,
                                username: extracted.username,
                                publicKey: extracted.publicKey,
                                fields: extracted.fields,
                                checks: extracted.checks,
                                status: extracted.status
                            };
                            console.info("ob2: ", ob2);
                            this.nav.navigateForward(`/data-sharing-signature?data=${encodeURIComponent(JSON.stringify(ob2))}`);
                            resolve();
                        }
                        else if(sourceData.indexOf("/caridentity?string=") > -1) {
                            this.lockService.addResume();
                            const request = new URL(sourceData).searchParams.get('string');
                            const extracted = this._CryptojsProviderService.decrypt(decodeURIComponent(request), UDIDNonce.energy);
                            const r = await this._DidVcService.generateCarVC(extracted, request);
                            if(r) {
                                this.nav.navigateForward(`/dashboard/certificates-page`);
                            }
                            resolve();
                        }
                        else if(sourceData.indexOf("/assessment-certificate?string=") > -1) {
                            this.lockService.addResume();
                            const request = new URL(sourceData).searchParams.get('string');
                            const extracted = this._CryptojsProviderService.decrypt(decodeURIComponent(request), UDIDNonce.energy);
                            const r = await this._DidVcService.generateAssessmentCertificate(extracted, request);
                            if(r) {
                                this.nav.navigateForward(`/dashboard/certificates-page`);
                            }
                            resolve();
                        }
                        // else if(sourceData.startsWith("https://patient.radiomedicum.de:9001/")) {
                        //     const alertsandra = await this.alertController.create({
                        //         header: 'patient.radiomedicum.de',
                        //         message: 'Please enter your code (without spaces) and date of birth in the format (yyyy-mm-dd)',
                        //         inputs: [{
                        //             type: 'text',
                        //             placeholder: 'XXXX.....',
                        //             name: 'code'
                        //         },{
                        //             type: 'text',
                        //             placeholder: '19XX-YY-ZZ',
                        //             name: 'dob'
                        //         }],
                        //         buttons: [{
                        //             text: 'OK',
                        //             handler: (data) => {
                        //                 const code = data.code;
                        //                 const dob = data.dob;

                        //                 if(!code || !dob) {
                        //                     return;
                        //                 }

                        //                 const docForm = document.createElement("form");
                        //                 docForm.method = "POST";
                        //                 docForm.target = "_blank";
                        //                 docForm.action = "https://patient.radiomedicum.de:9001/";

                        //                 const docInput1 = document.createElement("input");
                        //                 docInput1.type = "text";
                        //                 docInput1.name = "code";
                        //                 docInput1.value = code;
                        //                 docForm.appendChild(docInput1);

                        //                 const docInput2 = document.createElement("input");
                        //                 docInput2.type = "text";
                        //                 docInput2.name = "birth";
                        //                 docInput2.value = dob;
                        //                 docForm.appendChild(docInput2);

                        //                 const docInput3 = document.createElement("input");
                        //                 docInput3.type = "text";
                        //                 docInput3.name = "sub";
                        //                 docInput3.value = "ok";
                        //                 docForm.appendChild(docInput3);

                        //                 document.body.appendChild(docForm);

                        //                 const map = window.open("", "DocForm", "status=0,title=0,height=600,width=800,scrollbars=1");

                        //                 if (map) {
                        //                     docForm.submit();
                        //                 }
                        //             }
                        //         }, {
                        //             text: 'Cancel',
                        //             role: 'cancel',
                        //             handler: () => {

                        //             }
                        //         }]
                        //     });
                        //     await alertsandra.present();
                        // }
                        else if(sourceData.startsWith("https://app.poap.xyz/") || sourceData.startsWith("http://poap.xyz/")) {
                            const buttons = await this._WalletConnectProvider.returnWalletAddressesNames(sourceData);
                            buttons.push({
                                text: this.translateProviderService.instant('GENERAL.cancel'),
                                handler: () => {}
                            })
                            const alertj = await this.alertController.create({
                                header: this.translateProviderService.instant('SCANNER.header'),
                                message: this.translateProviderService.instant('SCANNER.poap'),
                                buttons
                            });
                            await alertj.present();
                        }
                        else {
                            try {
                                const checkIfVcVp = JSON.parse(sourceData);
                                if(!!checkIfVcVp.type) {
                                    if(checkIfVcVp.type.includes("VerifiablePresentation")) {
                                        await this.processVP(checkIfVcVp);
                                    } else if(checkIfVcVp.type.includes("VerifiableCredential")) {
                                        await this.processVC(checkIfVcVp);
                                    }
                                }
                            } catch(e) {
                                let dateOfResult = null;
                                const dateOfResultToastText = '';
                                let netData = {};
                                if(sourceData.includes("HC1:")) {
                                    const parseData = await EuDgc.parse(sourceData);
                                    if(!!parseData["t"]){
                                        dateOfResult = +new Date(parseData["t"][0]["sc"]);
                                    }
                                    try {
                                        const signData = await EuDgc.validate(sourceData,{
                                            certFilter: (certInfo) => { return true; }
                                        });
                                        netData = Object.assign(netData, { proof: signData });
                                    } catch (e) {
                                        netData = Object.assign(netData, { proof: {} });
                                    }
                                    netData = Object.assign(netData, { qrCode: sourceData });
                                    netData = Object.assign(netData, parseData);
                                } else {
                                    try {
                                        const url = new URL(sourceData);
                                        const al = await this.alertController.create({
                                            header: this.translateProviderService.instant('SCANNER.header'),
                                            message: this.translateProviderService.instant('SCANNER.message'),
                                            buttons: [
                                                {
                                                    text: this.translateProviderService.instant('BUTTON.CANCEL'),
                                                    role: 'cancel',
                                                    handler: () => {}
                                                },
                                                {
                                                    text: this.translateProviderService.instant('GENERAL.ok'),
                                                    handler: () => {
                                                        const a = document.createElement("a");
                                                        a.href = url.toString();
                                                        a.target = "_blank";
                                                        a.style.display = "none";
                                                        a.click();
                                                        a.remove();
                                                    }
                                                }
                                            ]
                                        })

                                        await al.present();
                                        this.lockService.addResume();
                                        return;
                                    } catch(e) {
                                        await this.presentToast(this.translateProviderService.instant('HEALTHCERTIFICATE.unable-scan'));
                                        this.lockService.addResume();
                                        return;
                                    }
                                }
                            }

                            this.lockService.addResume();
                        }
                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    async processVC(input: any) {
        // const input = {
        //     "@context": "https://www.w3.org/2018/credentials/v1",
        //     "id": "vc:evan:testcore:0x72e379c527b785f192ba5cec3633a9a319211d9a5f6ce19577b3be676c3fe81e",
        //     "type": [
        //         "VerifiableCredential",
        //         "KYC"
        //     ],
        //     "issuer": "did:evan:testcore:0x7cfe8ee45aa5138adf27c81b189390940956186b",
        //     "credentialSubject": {
        //         "id": "did:evan:testcore:0x27f7febd10fc994770dfc42bb003d63ca2476aea",
        //         "data": {
        //             "trustedBy": "Veriff",
        //             "trustLevel": "2",
        //             "trustValue": "1",
        //             "gender": "Male",
        //             "lastname": "Smith",
        //             "firstname": "John",
        //             "dateofbirth": "1990-01-01",
        //             "identificationdocumentnumber": "DocId1633453284867",
        //             "identificationissuecountry": "DEU",
        //             "identificationissuedate": "2015-11-11T00:00:00Z",
        //             "identificationexpirydate": "2021-12-09T00:00:00Z"
        //         }
        //     },
        //     "validFrom": "2021-10-05T17:01:27Z",
        //     "validTo": "2023-10-05T17:01:27Z",
        //     "credentialStatus": {
        //         "id": "https://testcore.evan.network/smart-agents/smart-agent-did-resolver/vc/status/vc:evan:testcore:0x72e379c527b785f192ba5cec3633a9a319211d9a5f6ce19577b3be676c3fe81e",
        //         "type": "evan:evanCredential"
        //     }
        // }

        const modal = await this._ModalController.create({
            component: VerifiableComponent,
            componentProps: {
              input
            }
          })
        modal.onDidDismiss().then(async () => {

        })
        await modal.present();
    }

    async processVP(input: any) {
        // const input = {
        //     "@context": [
        //         "https://www.w3.org/2018/credentials/v1"
        //     ],
        //     "type": [
        //         "VerifiablePresentation"
        //     ],
        //     "id": "vc:evan:testcore:0x72e379c527b785f192ba5cec3633a9a319211d9a5f6ce19577b3be676c3fe81e",
        //     "holder": "did:evan:testcore:0x27f7febd10fc994770dfc42bb003d63ca2476aea"
        // }

        const modal = await this._ModalController.create({
            component: VerifiableComponent,
            componentProps: {
                input
            }
          })
        modal.onDidDismiss().then(async () => {

        })
        await modal.present();

    }

    public async scanAddress(): Promise<string> {
        this.lockService.removeResume();
        return new Promise(async (resolve, reject) => {
            // UtilityService.setTimeout(500).subscribe(_ => {
            try {
                const barcodeData = await this.startBarcodeScanningModal();

                if (!barcodeData) {
                    localStorage.setItem('scannerCancelled', 'true');
                    this.lockService.addResume();
                }

                if (barcodeData ) {

                    const address = barcodeData.displayValue;

                    // if(address.slice(0,2) != "0x") {
                    //     reject();
                    // }

                    this.lockService.addResume();
                    resolve(address);
                }
            } catch (e) {
                this.presentToast(JSON.stringify(e));
                this.lockService.addResume();
            }
        })
    }

    private generateRandomString() {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < 25) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
          counter += 1;
        }
        return result;
      }

    public async scanCustomToken(): Promise<any> {
        this.lockService.removeResume();
        return new Promise(async (resolve, reject) => {
            // UtilityService.setTimeout(500).subscribe(_ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {

                        const scannedText = barcodeData.displayValue;

                        try {
                            const uri = new URL(scannedText);
                            const network = uri.protocol;
                            const contract = uri.pathname;
                            const name = uri.searchParams.get('name');
                            const symbol = uri.searchParams.get('symbol');
                            const decimals = uri.searchParams.get('decimals');
                            this.lockService.addResume();
                            resolve({network, contract, name, symbol, decimals});
                        } catch(e) {
                            reject();
                        }

                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            // });
        })
    }

    public async scanDataSharingRequest(): Promise<any> {
        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {

                        try {
                            var request = new URL(barcodeData.displayValue).searchParams.get('string');
                        } catch(eee) {
                            request = barcodeData.displayValue;
                        }

                        const extracted = this._CryptojsProviderService.decrypt(request, UDIDNonce.energy);

                        this.lockService.addResume();
                        resolve(extracted);
                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    public scanReceipt(): Promise<void> {
        return new Promise((resolve) => {
            this.lockService.removeResume();
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {
                        const scanData = barcodeData.displayValue;
                        this.DSFinVK(scanData).then(() => {
                            this.lockService.addResume();
                            resolve();
                        })

                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                    resolve();
                }
            });
        })

    }

    public DSFinVK(code?: string): Promise<void> {
        return new Promise((resolve, reject) => {
            code = !code ? "V0;ftf8w5LvO0KSi82AFpwHQ;Kassenbeleg-V1;Beleg^1.25_0.00_0.00_0.00_0.00^1.25:Bar;278320;579565;2022-09-12T10:32:02.000Z;2022-09-12T10:32:22.000Z;ecdsa-plain-SHA384;unixTime;YuvouT4kjv5pwzpmvWTNFktLCP38MzILgWT81nyZGthU8h/V0qbRMkB27yatQ9B2dcW98JudE05Z+iZOToyVhCtrwR8vFafVVcilwcfdx5xxHTKLv9e2EPxBs3JfsXC3;BA1NZnGpqu70d/nOUQg7o4/SAw17DRMaK+HsrfU0FCtrrbIqoV5GlDK6B0mkSWZxuXgiMmDI+K+zBFxTVGJwamunZ+4XcMGlDrFXhI9wMAL2q0tfdLAaAq/Ah9vjLjBy8g==" : code;

            if(!code.startsWith("V0;")) {

                this.alertController.create({
                    header: 'Digitale Schnittstelle der Finanzverwaltung für Kassensysteme',
                    message: this.translateProviderService.instant('SCANNER.dsfinvk'),
                    buttons: [
                        {
                            text: this.translateProviderService.instant('BUTTON.CANCEL'),
                            role: 'cancel',
                            handler: () => {

                            }
                        },
                        {
                            text: this.translateProviderService.instant('GENERAL.ok'),
                            handler: () => {
                                const a = document.createElement("a");
                                a.href = code;
                                a.target = "_blank";
                                a.style.display = "none";
                                a.click();
                                a.remove();
                            }
                        }
                    ]
                }).then((alerto) => {
                    alerto.present().then(() => {
                        resolve();
                    })
                })

            } else {
                const codeArray = code.split(";");

                const qrcodeversion = codeArray[0]; // V0
                const kassenseriennummer = codeArray[1]; // AMA-2642
                const processType  = codeArray[2]; // Kassenbeleg-V1
                const processData  = codeArray[3]; // Beleg^4.05_3.00_0.00_0.00_0.00^7.05:Bar
                const transaktionsnummer = codeArray[4]; // 13
                const signaturzaehler  = codeArray[5]; // 44131
                const startzeit  = codeArray[6]; // 2019-11-22T11:29:48.000Z
                const logtime  = codeArray[7]; // 2019-11-22T11:29:49.000Z
                const sigalg  = codeArray[8]; // ecdsa-plain-SHA384
                const logtimeformat  = codeArray[9]; // unixTime
                const signatur = codeArray[10]; // K8zsZ6NjsBzo/Yd3Hba84aH3oT+c4Og5VcfJ7s6Dxz7UmAw- tcKmzW16OkS/lm8pDE/37JHoRlYofUT- LNF+9bY5Jv7C2P4nuEaHwlVarbiJs3bYlgQmIXQDZnf+8FhfBm
                const publickey  = codeArray[11]; // BBXNYQErM4d9sk9Iy+0T6A4sdT ocijml5X78Gq/At2uX- Tcs/3bZNNpyuHd+dpmY59yLh0xZcr9osHhkDAxsQumgjmtb3d9GIVnTaP CslEki84P1iHPiHKHfcszeQajPk3A==

                const content = {
                    qrcodeversion, kassenseriennummer, processType, processData, transaktionsnummer, signaturzaehler, startzeit, logtime, sigalg, logtimeformat, signatur, publickey
                }

                const contentWithQRCode = Object.assign(content, { code });

                this._SecureStorageService.getValue(SecureStorageKey.kassenbeleg, false).then(kassen => {
                    const kassenbeleg = !!kassen ? JSON.parse(kassen): [];
                    kassenbeleg.push(contentWithQRCode);
                    this._SecureStorageService.setValue(SecureStorageKey.kassenbeleg, JSON.stringify(kassenbeleg)).then(() => {
                        this.alertController.create({
                            header: 'Digitale Schnittstelle der Finanzverwaltung für Kassensysteme',
                            message: JSON.stringify(content),
                            buttons: [{
                                text: 'Ok'
                            }]
                        }).then(al => {
                            al.present();
                            resolve();
                        });
                    })
                })
            }

        })


    }

    public async scanCertificate(): Promise<void> {
        this.lockService.removeResume();
        UtilityService.setTimeout(500).subscribe(async _ => {
            try {
                const barcodeData = await this.startBarcodeScanningModal();

                if (!barcodeData) {
                    localStorage.setItem('scannerCancelled', 'true');
                    this.lockService.addResume();
                }

                if (barcodeData) {
                    // console.log(barcodeData.text);
                    // console.log(barcodeData.text.toString());
                    const resolve = JSON.parse(JSON.parse(barcodeData.displayValue));
                    // console.log(resolve);
                    if(resolve["type"] && resolve["type"].includes("HelixMarketplace")) {
                        // this.scannedCertificate.push(resolve);

                        const now = +new Date();
                        const nowString = new Date(now);

                        if(!resolve["credentialSubject"]["data"][0]["issueDate"]) {
                            resolve["credentialSubject"]["data"][0]["issueDate"] = nowString.toISOString();
                        }

                        if(!resolve["credentialSubject"]["data"][0]["expiryDate"]) {
                            nowString.setHours(23);
                            nowString.setMinutes(59);
                            nowString.setSeconds(59);
                            resolve["credentialSubject"]["data"][0]["expiryDate"] = nowString.toISOString();
                        }

                    const st = await this._SecureStorageService.getValue(SecureStorageKey.marketplaceCertificates, false);
                    const stt = !!st ? JSON.parse(st) : [];
                    stt.push(resolve);
                        // const f = sst.find(k => k.qrCode == resolve);

                    this.scannedCertificate = stt;
                    // console.log(this.scannedCertificate);
                    await this._SecureStorageService.setValue(SecureStorageKey.marketplaceCertificates, JSON.stringify(this.scannedCertificate));

                    } else {
                        this.presentToast(this.translateProviderService.instant('CERTIFICATES.noValidMarketplaceCertificate'));
                    }

                    this.lockService.addResume();

                }
            } catch (e) {
                this.presentToast(JSON.stringify(e));
                this.lockService.addResume();
            }
        });
    }

    public async scanUtilityBill(): Promise<void> {
        this.lockService.removeResume();
        UtilityService.setTimeout(500).subscribe(async _ => {
            try {
                const barcodeData = await this.startBarcodeScanningModal();

                if (!barcodeData) {
                    localStorage.setItem('scannerCancelled', 'true');
                    this.lockService.addResume();
                }

                if (barcodeData) {
                    // console.log(barcodeData.content);
                    const resolve = barcodeData.displayValue;
                    this.energyAustraliaQRCode = resolve;
                    this.energyAustraliaQRCodeComponent = this._CryptojsProviderService.decrypt(resolve, UDIDNonce.helix as any);
                    this.energyAustraliaQRCodeComponentArray = [];
                    Object.keys(this.energyAustraliaQRCodeComponent).forEach(k => {
                        this.energyAustraliaQRCodeComponentArray.push({
                            key: k,
                            value: this.energyAustraliaQRCodeComponent[k]
                        })
                    })
                    this.presentToast('Congratulations. Your data has been successfully transferred from the paper or email bill. By clicking “Log into Energy Management App”, you will be automatically redirected to the Energy Management App. Your verified identity data will be transferred to the provider.')
                    this.lockService.addResume();
                }

            } catch (e) {
                this.presentToast(JSON.stringify(e));
                this.lockService.addResume();
            }
        });
    }

    public async scanWalletConnectQRCode(): Promise<any> {
        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {
                        this.lockService.addResume();
                        resolve(barcodeData.displayValue);
                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    processContent = async (scanned) => {
        if(scanned.indexOf('/importwallet?string=') > -1) {
            var cont = decodeURIComponent(new URL(scanned).searchParams.get('string'));
        } else {
            cont = scanned;
        }

        const res = await this.contactService.decryptContactId(cont);

        try {
            var decryptedQRObject = JSON.parse(res);
        } catch(e) {
            decryptedQRObject = res;
        }

        const mnemonic = CryptoJS.AES.decrypt(decryptedQRObject.mnemonic, UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
        const heading = decryptedQRObject.heading;
        const publicKey = decryptedQRObject.publicKey;

        this.lockService.addResume();

        return {
            mnemonic,
            heading,
            publicKey
        };
    }

    public async scanWalletSeedPhrase(): Promise<any> {

        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {

                        const resolvable = await this.processContent(barcodeData.displayValue);
                        this.lockService.addResume();
                        resolve(resolvable);

                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    public async scanContact(): Promise<void> {
        this.lockService.removeResume();
        UtilityService.setTimeout(500).subscribe( async _ => {
            try {
                const barcodeData = await this.startBarcodeScanningModal();

                if (!barcodeData) {
                    localStorage.setItem('scannerCancelled', 'true');
                    this.lockService.addResume();
                }

                if (barcodeData) {
                if(barcodeData.displayValue?.indexOf('/contact?string=') > -1) {
                    var cont = decodeURIComponent(new URL(barcodeData.displayValue).searchParams.get('string'));
                } else {
                    cont = barcodeData.displayValue;
                }
                const res = await this.contactService.decryptContactId(cont);
                const decryptedQRObject = JSON.parse(res);

                // console.log("*** decryptedQRObject: ");
                // console.log(decryptedQRObject);

                const displayContactId = decryptedQRObject.id;
                const displayContactHelixUsername = decryptedQRObject.helixUsername;
                const displayContactChatUsername = decryptedQRObject.chatUsername;
                const displayContactPublicKey = decryptedQRObject.chatPublicKey;

                const checkUserCanBeAdded = await this.checkUserCanBeAdded(displayContactId, displayContactChatUsername);
                if (!checkUserCanBeAdded) {
                    return this.lockService.addResume();
                }

                const hasImage = decryptedQRObject.hasImage;

                if ( hasImage ) {
                    this.apiService.getPublicImage(displayContactId)
                        .pipe(take(1))
                        .subscribe(blob => {
                            const reader = new FileReader();
                            reader.readAsDataURL(blob);
                            reader.onloadend = async () => {
                                // this could be a jpg or gif.
                                const pictureBase64 = (reader.result as string);

                                this.openPopover(
                                    displayContactId,
                                    displayContactHelixUsername,
                                    displayContactChatUsername,
                                    displayContactPublicKey,
                                    pictureBase64
                                );
                            };
                        });
                } else {
                    await this.openPopover(
                        displayContactId,
                        displayContactHelixUsername,
                        displayContactChatUsername,
                        displayContactPublicKey,
                        null
                    );
                }
                this.lockService.addResume();
            }

            } catch (e) {
                this.presentToast(JSON.stringify(e));
                this.lockService.addResume();
            }
        });
    }


    public async scanUseCaseCertificate(): Promise<any> {
        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe( async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {

                        const coreData = barcodeData.displayValue?.replace("helix-id://helix-id.com/contact?string=", "");

                        console.log(coreData);

                        // const symmetricDecrypt = await this._CryptoProviderService.symmetricDecrypt(barcodeData.text, UDIDNonce.helix as any);
                        const symmetricDecrypt = await this.contactService.decryptContactId( decodeURIComponent(coreData) );
                        // const paritallyDecrypted = await this._CryptoProviderService.symmetricDecrypt(symmetricDecrypt, UDIDNonce.helix as any);
                        const decryptedQRObject = this.parseJSONSafe(symmetricDecrypt);

                        // console.log("*** decryptedQRObject: ");
                        // console.log(decryptedQRObject);

                        this.lockService.addResume();

                        this.presentToast(this.translateProviderService.instant('OTC.TOAST.success'));

                        resolve(decryptedQRObject);
                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    public async scanUserDataQR(): Promise<any> {
        this.lockService.removeResume();
        return new Promise((resolve) => {
            UtilityService.setTimeout(500).subscribe(async _ => {
                try {
                    const barcodeData = await this.startBarcodeScanningModal();

                    if (!barcodeData) {
                        localStorage.setItem('scannerCancelled', 'true');
                        this.lockService.addResume();
                    }

                    if (barcodeData) {
                        const encrypted = await this.contactService.decryptContactId(barcodeData.displayValue);
                        const symmetricDecrypt = await this._CryptoProviderService.symmetricDecrypt(encrypted, UDIDNonce.helix as any);
                        const decryptedQRObject = symmetricDecrypt ? this.parseJSONSafe(symmetricDecrypt) : this.parseJSONSafe(encrypted);

                        // console.log("*** decryptedQRObject: ");
                        // console.log(decryptedQRObject);

                        this.lockService.addResume();

                        this.presentToast(this.translateProviderService.instant('OTC.TOAST.success'));

                        resolve(decryptedQRObject);
                    }

                } catch (e) {
                    this.presentToast(JSON.stringify(e));
                    this.lockService.addResume();
                }
            });
        })
    }

    parseJSONSafe(message) {
        try {
          return JSON.parse(message);
        }
        catch(e) {
        //   console.log('*** parseJSONSafe err: ***');
          console.log(e);
          return null;
        }
      }

    async openPopover(id: string, helixUsername: string, chatUsername: string, chatPublicKey: string, displayImage: string) {
        alert('this function is part of the chat which is not implemented');
        // const popover = await this.popoverController.create({
        //     component: ContactAddModalComponent,
        //     cssClass: 'proto-custom-popover',
        //     componentProps: {
        //         id,
        //         helixUsername,
        //         chatUsername,
        //         chatPublicKey,
        //         displayImage,
        //         isIncomingFriendRequest: false
        //     },
        //     translucent: false
        // });
        // await popover.present();
    };

    async presentToast(message: string) {
        try {
            this.toast.dismiss();
        } catch(e) {
            console.log(e)
        }
        this.toast = await this.toastController.create({
          message,
          duration: 3000,
          position: 'top',
        });
        this.toast.present();
      }

    presentAlertConfirm() {
        this.alertController.create({
            mode: 'ios',
            header: this.translateProviderService.instant('QRSCAN.camera-permission-false.header'),
            message: this.translateProviderService.instant('QRSCAN.camera-permission-false.message'),
            buttons: [
            {
                text: this.translateProviderService.instant('QRSCAN.camera-permission-false.settings'),
                handler: () => {
                    this.openNativeSettings.open('settings').then(_ =>
                        {
                            App.exitApp();
                        }
                    )
                }
            },
            {
                text: this.translateProviderService.instant('QRSCAN.camera-permission-false.cancel'),
                role: 'cancel',
                handler: () => {
                }
            }
            ]
        }).then(alert => alert.present())
    }

    async checkUserCanBeAdded(displayContactId: string, displayContactChatUsername: string) {
        let userCanBeAdded: boolean = true;
        let errorMessage: string;
        let myUserId: string;

        try {
            myUserId = (await this.userService.getUser()).userid;
        } catch (e) {
            userCanBeAdded = false;
            errorMessage = this.translateProviderService.instant('CHAT.error-user-id');
        }

        // 1) Shouldn't be able to add yourself.
        const tryingToAddSelf: boolean = (myUserId == displayContactId);
        if (tryingToAddSelf) {
            userCanBeAdded = false;
            errorMessage = this.translateProviderService.instant('CHAT.error-adding-own');
        }
        else {
            this._LoaderService.loaderCreate();
            // 2) Shouldn't be able to add a contact that already exists.
            // const isExistingContact = await this._ChatService.isFriend(displayContactChatUsername);
            // console.log('*** isExistingContact?: ' + isExistingContact);
            // if (isExistingContact) {
            //     userCanBeAdded = false;
            //     errorMessage = this.translateProviderService.instant('CHAT.error-contact-already-added');
            // }
        }

        await this._LoaderService.loaderDismiss();
        if (errorMessage) {
            this.presentToast(errorMessage);
        }
        return userCanBeAdded;
    }

    private async presentAlertForSettingsPage() {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: this.translateProviderService.instant('QRSCAN.camera-permission-false.header'),
            message: this.translateProviderService.instant('QRSCAN.camera-permission-false.message'),
            buttons: [
                {
                    text: this.translateProviderService.instant('QRSCAN.camera-permission-false.cancel'),
                    handler: () => {}
                },
                {
                    text: this.translateProviderService.instant('QRSCAN.camera-permission-false.settings'),
                    handler: async () => {
                        await BarcodeScanner.openSettings();
                    }
                }
            ]
        });

        await alert.present();

        await alert.onDidDismiss();
    }
}
