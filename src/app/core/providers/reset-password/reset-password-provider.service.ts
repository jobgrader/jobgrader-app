import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ResetPassword } from '../../models/ResetPassword';
import { ApiProviderService } from '../api/api-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordProviderService {

  public otpValid = true;
  public registerSuccess = true;
  public personalInfoSuccess = true;
  public resetPasswordForm: ResetPassword = {
    reset_username: '',
    reset_OTP: '',
    reset_password: '',
    reset_confirm_password: ''
  };

  constructor(
    private nav: NavController,
    private router: Router,
    private api: ApiProviderService,
    private secureStorage: SecureStorageService,
  ) { }

  checkIsOtpValid(): boolean {
    return this.otpValid;
  }

  async skipReset() {
    this.resetPasswordForm = {};
    await this.secureStorage.removeValue(SecureStorageKey.resetPassword, false).then(() => {
      this.nav.navigateBack('/login?from=resetpassword');
    });
  }

  async getResetPasswordData(): Promise<ResetPassword> {
    const userResponse = await this.secureStorage.getValue(SecureStorageKey.resetPassword, false);
    return userResponse ? JSON.parse(userResponse) : null;
  }

  async saveResetPasswordData(data: any, step: number): Promise<any> {
    const dataFromStorage: ResetPassword = (await this.getResetPasswordData()) || null;
    if (!!dataFromStorage) {
      this.resetPasswordForm = dataFromStorage;
    }

    switch (step) {
      case 1:
        this.resetPasswordForm.reset_username = data.resetUsername;

        await this.secureStorage.setValue(SecureStorageKey.resetPassword, JSON.stringify(this.resetPasswordForm));
        break;

      case 2: {
        this.resetPasswordForm.reset_OTP = data.resetOTP;

        await this.secureStorage.setValue(SecureStorageKey.resetPassword, JSON.stringify(this.resetPasswordForm));
        break;
      }

      case 3: {
        this.resetPasswordForm.reset_password = data.resetPassword;
        this.resetPasswordForm.reset_confirm_password = data.resetPasswordConfirm;

        await this.secureStorage.setValue(SecureStorageKey.resetPassword, JSON.stringify(this.resetPasswordForm));
        break;
      }

      default:
        break;
    }
  }

}
