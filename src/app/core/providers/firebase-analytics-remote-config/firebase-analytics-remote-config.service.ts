import { Injectable, inject } from '@angular/core';
import { FirebaseRemoteConfig } from '@capacitor-firebase/remote-config';
import { Analytics, setUserId, setUserProperties } from '@angular/fire/analytics';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsRemoteConfigService {

  private analytics: Analytics = inject(Analytics);

  constructor(
    
  ) { }

  async initiatelizeAnalytics(userId: string, userGroups: Array<string>) {
    console.log("Analytics.setUserId", userId);

    setUserId(this.analytics, userId);

    console.log("Analytics.setUserProperty", JSON.stringify(userGroups));
    setUserProperties(this.analytics, {
      key: 'userGroups',
      value: JSON.stringify(userGroups)
    });
  }

  async initiatlizeRemoteConfig() {
    
    await FirebaseRemoteConfig.fetchAndActivate();

    const callbackId = await FirebaseRemoteConfig.addConfigUpdateListener(
      (event, error) => {
        if (error) {
          console.error("FirebaseRemoteConfig.addConfigUpdateListener error", error);
        } else {
          console.log("FirebaseRemoteConfig.addConfigUpdateListener event", event);
        }
      }
    );
    console.log("Firebase Remote Config, callbackId: ", callbackId);

  }
}
