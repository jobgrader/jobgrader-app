import { TestBed } from '@angular/core/testing';

import { AnalyticsRemoteConfigService } from './firebase-analytics-remote-config.service';

describe('AnalyticsRemoteConfigService', () => {
  let service: AnalyticsRemoteConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnalyticsRemoteConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
