import { Injectable } from '@angular/core';
// import * as vade from './pkg/vade_evan';
import init, * as vade from 'vade-wasm';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VadeService {

  public _WasmModule = null;

  constructor(
    private http: HttpClient
  ) { }

  async moduleInitialize() {

    const NODE_URL = "assets/scripts/vade_evan_bg_nodejs.wasm";
    const WEB_URL = "assets/scripts/vade_evan_bg_web.wasm";
    // const choice = NODE_URL;
    const choice = WEB_URL;
    
    try {
      var importObject = { imports: { imported_func: (arg) => console.log(arg) } };
      var fetchResponse = await fetch(choice);
      this._WasmModule = await fetchResponse.arrayBuffer();
      
      // try {
      //   var instantiation = await WebAssembly.instantiate(arrayBuffer, importObject);
      //   console.log(instantiation);
      //   this._WasmModule = instantiation.module;
      // } catch(ee) {
      //   console.log(ee);
      //   this._WasmModule = arrayBuffer;
      // }

    } catch(ee) {
      console.log(ee);
    }
    
    return this._WasmModule;
  }


  async init() {
    await this.moduleInitialize();

    if(!this._WasmModule) {
      this._WasmModule = await init();
    }

    await vade.default(this._WasmModule); 
  }

  public resolveDidDif(did: string): Observable<any> {
    return this.http.get(`https://resolver.identity.foundation/1.0/identifiers/${encodeURIComponent(did)}`,{
      headers: {
        
      }
    })
  }

  async resolveDid(did: string) {

    await this.init();

    let didDocument = null;
    
    try {
      didDocument = await vade.did_resolve(did, {});
      // didDocument = await vade.execute_vade("did_resolve", did, "", "", "did_universal_resolver", {})
    } catch(e) {
      console.log(e);
    }
    return didDocument;
  }

  async createDid(clearName: string) {

    await this.init();

    let didDocument = null;
  
    try {
      didDocument = await vade.did_create("did:evan","{}","",{ clearName });
    } catch(e) {
      console.log(e);
    }

    return didDocument;
  }

  async createVc(body: any) {

    await this.init();

    const signedVC = await vade.vc_zkp_create_credential_schema(body.didDocument.id,"", JSON.stringify(body.credentialSubjectRaw), {});
    console.log(signedVC);

    // const keyPair = await Ed25519VerificationKey2020.generate();
    // const suite = new Ed25519Signature2020({key: keyPair});

    // const credential = {
    //   "@context": [
    //     "https://www.w3.org/2018/credentials/v1",
    //     "https://www.w3.org/2018/credentials/examples/v1"
    //   ],
    //   "id": "https://example.com/credentials/1872",
    //   "type": ["VerifiableCredential", "HelixCarIdentity"],
    //   "issuer": "https://example.edu/issuers/565049",
    //   "issuanceDate": new Date().toISOString(),
    //   "credentialSubject": body
    // };
    
    // const signedVC = await vc.issue({credential, suite, defaultDocumentLoader});
    return JSON.parse(signedVC);
  }

  async validateVc(vc: any) {
    
    await this.init();

    try {
      const validation = await vade.vc_zkp_request_proof(vc.issuer,"", JSON.stringify(vc.proof), {});
      console.log(validation);
    } catch(e) {
      console.log(e);
    }

    try {
      const validation = await vade.vc_zkp_verify_proof(vc.issuer,"", JSON.stringify(vc.proof), {});
      console.log(validation);
    } catch(e) {
      console.log(e);
    }
    
    // const signedVC = await vc.issue({credential, suite, defaultDocumentLoader});
    // return validation;


  }

  
}
