import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class IpfsService {

  private gateways = [
    'https://ipfs.io/ipfs/',
    'https://gateway.pinata.cloud/ipfs/',
    'https://cloudflare-ipfs.com/ipfs/'
  ];

  private testCID = 'QmYGSxWDKULB3t3L3yG8nCmKURe6Tkbr5UYn24vZBqtp7H'

  public ipfsGateway = this.gateways[0];

  constructor() {

    this.checkIPfsGateway();
  }

  // we check if the ipfs gateway is working and set the first one as default gateway
  private async checkIPfsGateway(): Promise<any> {
    for (const gateway of this.gateways) {
        const works = await this.fetch(gateway + this.testCID);
        // console.log('IpfsServiceres#checkFileExistence; works', works);

        if (works) {
            this.ipfsGateway = gateway;
            return;
        }
    }

    this.ipfsGateway = this.gateways[1];
  }

  private async fetch(url: string): Promise<any> {
    // console.log('IpfsService#fetch; check for', url);

    try {
        const response = await fetch(url, { method: 'HEAD'});
        // console.log('IpfsService#fetch; response.status', response.status);

        if (response.ok) {
            // console.log('IpfsService#fetch; ok');
            return true;
        } else if (response.status === 301 || response.status === 302 || response.status === 307 || response.status === 308) {
            // console.log('IpfsService#fetch; redirect');
            return this.fetch(response.headers.get('Location'));
        } else {
            // throw new Error(`## Failed to fetch: ${response.statusText}`);
            return false;
        }
    } catch (error) {
        return false;
    }
   }

}
