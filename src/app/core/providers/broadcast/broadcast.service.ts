import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { ApiProviderService, BroadcastMessages } from '../api/api-provider.service';

export enum SubscriptionTags {
  AppUpdateENEU = "update-en-eu",
  AppUpdateDEEU = "update-de-eu"
}

@Injectable({
  providedIn: 'root'
})
export class BroadcastService {

  _Counter;

  _Messages: Array<BroadcastMessages> = [
    // {
    //   timestamp: +new Date(),
    //   title: 'Update',
    //   message: 'App Update Available'
    // },
    // {
    //   timestamp: +new Date(),
    //   title: 'New Jobs!!',
    //   message: 'New Jobs Available!!'
    // }
  ];

  constructor(
    // private _SecureStorage: SecureStorageService,
    // private _Api: ApiProviderService
  ) {
    // this._SecureStorage.getValue(SecureStorageKey.broadcasts, false).then(broadcasts => {
    // this._Messages = !!broadcasts ? JSON.parse(broadcasts) : this._Messages;
    this._Counter = this._Messages.length;
    // })
  }

  async init() {
    // const timestamp = await this._SecureStorage.getValue(SecureStorageKey.notificationsTimestamp, false);
    // console.log("timestamp", timestamp);
    // const notificationsTimestamp = !!timestamp ? +new Date(Number(timestamp)) : null;
    // console.log("notificationsTimestamp", notificationsTimestamp);
    // const messages = await this._Api.obtainBroadcastMessages(notificationsTimestamp, SubscriptionTags.AppUpdateENEU).catch(e => {
    //   console.log(e);
    // });
    // if(messages) {
    //   console.log("messages", messages);
    //   var formattedMessages = messages.map(m => <BroadcastMessages> {
    //     timestamp: m.lastModified,
    //     title: m.templateId,
    //     message: m.sentMessage
    //   });
    //   console.log("formattedMessages", formattedMessages);
    //   this._Messages = this._Messages.concat(formattedMessages);
    //   formattedMessages.forEach(_ => this._Counter++);
    //   console.log("this._Messages", this._Messages);
    //   await this._SecureStorage.setValue(SecureStorageKey.broadcasts, JSON.stringify(this._Messages));
    //   const timestampArray = Array.from(this._Messages, mm => +new Date((mm as any).timestamp));
    //   const lastUpdatedItem = this._Messages.find(m => Math.max(...timestampArray) == m.timestamp);
    //   if(lastUpdatedItem) {
    //     const lastUpdated = lastUpdatedItem.timestamp;
    //     console.log("lastUpdated", lastUpdated);
    //     await this._SecureStorage.setValue(SecureStorageKey.notificationsTimestamp, lastUpdated.toString());
    //   }
    // }
  }

  resetCounter() {
    this._Counter = 0;
  }

}
