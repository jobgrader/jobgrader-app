import { Injectable, NgZone } from '@angular/core';
import { XmppUser } from 'xmpp-did-comm/src/client/index.js';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { ActionSheetController, AlertController, NavController, PopoverController, ToastController } from '@ionic/angular';
import { TranslateProviderService } from '../translate/translate-provider.service';
import { ApiProviderService } from '../api/api-provider.service';
import { AppStateService } from '../app-state/app-state.service';
import { EventsList, EventsService } from '../events/events.service';
import { ChatMessage, ChatThread, ComboUserWrapper, MessageStructure, ChatKeyPairs, MessageTypes, ChatThreadRequest } from './chat-model';
import { Contact } from 'src/app/contact/shared/contact.model';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import moment from 'moment';
import { SQLStorageService } from '../sql/sqlStorage.service';
import { Subject } from 'rxjs';
import config from 'xmpp-did-comm/src/base-config';
import { environment } from 'src/environments/environment';
import { KeyExchangeService } from './key-exchange.service';
import { uniq } from 'lodash-es';
import { NetworkService } from '../network/network-service';
import { DeeplinkProviderService } from '../deeplink/deeplink-provider.service';
import { DidCommService } from '../did-comm/did-comm.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public myXmppUser: XmppUser;
  public isFullyLoaded: boolean = false;
  public chatThreads: ChatThread[] = [];
  public chatThreadToView: ChatThread;
  public receiveMessageCallback: any;
  public dateLocal: string;
  public hasUnreadChatsOrFriendRequests: boolean = false;
  public chatThreadRequests: ChatThreadRequest[] = [];
  public chatDbIsLockedChange: Subject<boolean> = new Subject<boolean>();
  public contactCount: number;
  public merchantCount: number;

  private keyPair: any;
  public threadChatKeyPairs: any;

  public isOnline: boolean = true;

  constructor(
    public nav: NavController,
    public _SQLStorageService: SQLStorageService,
    public _SecureStorageService: SecureStorageService,
    public _CryptoProviderService: CryptoProviderService,
    public _ToastController: ToastController,
    public _TranslateProviderService: TranslateProviderService,
    public _PopoverController: PopoverController,
    public _ApiProviderService: ApiProviderService,
    public _AppStateService: AppStateService,
    public _EventsService: EventsService,
    public _AlertController: AlertController,
    public _ActionSheetController: ActionSheetController,
    private _KeyExchangeService: KeyExchangeService,
    public _Zone: NgZone,
    public _LoaderProviderService: LoaderProviderService,
    private _DeeplinkProviderService: DeeplinkProviderService,
    private _NetworkService: NetworkService,
    private didCommService: DidCommService
  ) {}
  isConnected() {
    if (!this.myXmppUser) { return false; }
    try {
      return (this.myXmppUser.xmpp.status == 'online');
    }
    catch(e) {
      return false;
    }
  }

  navigateToChat() {
    var url = '/chat';
    if (this.isConnected()) {
      this.nav.navigateForward(url);
    }
    else {
      if(this._NetworkService.checkConnection()) {
        this.connect(false).then(() => {
          this.nav.navigateForward(url);
        });
      } else {
        this.nav.navigateForward(url);
      }

    }
  }

  async connect(alsoLoadThreadData: boolean, uponResume: boolean = false): Promise<boolean> {

    this.isOnline = this._NetworkService.checkConnection();

    if(!this.isOnline) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    if(!this._AppStateService.isAuthorized) {
      return;
    }

    config.xmpp =  {
      hostName: environment.chatServerUrl,
      port: 7070,
      protocol: 'https',
      url: `https://${environment.chatServerUrl}:7070`,
      ws: `wss://${environment.chatServerUrl}:7443`,
      xmppChatDns: environment.chatServerUrl
    }

    await this.decryptEncryptedChatPrivateKey();
    await this.disconnect();
    await this.setLocale();
    this.setIsFullyLoadedZoneWrapped(false);

    var selfChatData = await this.getSecureStorageChatUser();
    if (!selfChatData) {
      var chatUser = await this._ApiProviderService.getSelfChatUser();
      // console.log("chatUser");
      // console.log(chatUser);
      // console.log(this.isOnline);
      if (chatUser) {
          await this._SecureStorageService.setValue(SecureStorageKey.chatUserData, JSON.stringify(chatUser), false);
          selfChatData = chatUser;
      } else {
        // this.showErrorState(this._TranslateProviderService.instant('LOGIN.errorGettingChatUser'), true);
        // this.isOnline = false;
        this.isFullyLoaded = true;
        return this.isFullyLoaded;
      }
    }

    var selfChatPassword = await this.returnChatPassword(selfChatData);
    var myPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPublicKey, false);

    var myPublicKeyAsArrayResult = this._CryptoProviderService.hex2i(myPublicKey);
    if (myPublicKeyAsArrayResult.error) {
      var cs = await this.generateNewChatKeys();
      if(cs) {
        myPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPublicKey, false);
        myPublicKeyAsArrayResult = this._CryptoProviderService.hex2i(myPublicKey);
      }
      if (!cs || myPublicKeyAsArrayResult.error) {
        this.showErrorState(myPublicKeyAsArrayResult.error);
        return this.isFullyLoaded;
      }
    }

    var myPrivateKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPrivateKey, false);
    var myPrivateKeyAsArrayResult = this._CryptoProviderService.hex2i(myPrivateKey);
    if (myPrivateKeyAsArrayResult.error) {
      var cs = await this.generateNewChatKeys();
      if(cs) {
        myPrivateKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPrivateKey, false);
        myPrivateKeyAsArrayResult = this._CryptoProviderService.hex2i(myPrivateKey);
      }
      if (!cs || myPrivateKeyAsArrayResult.error) {
        this.showErrorState(myPrivateKeyAsArrayResult.error);
        return this.isFullyLoaded;
      }
    }

    this.keyPair = {
      keyType: 'ed25519',
      publicKey: myPublicKeyAsArrayResult.result,
      privateKey: myPrivateKeyAsArrayResult.result
    };

    var c = await this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false)
    this.threadChatKeyPairs = !!c ? JSON.parse(c) : {}

    console.log("*** 9) XMPP Chat service XmppUser.connect() start:");

    return XmppUser.connect(selfChatData.username, selfChatPassword).then(xmppUser => {
      this.listenForMessagesWhenIdle(xmppUser);
      this.listenForFriendRequestsWhenIdle(xmppUser);
      this.myXmppUser = xmppUser;

      // 1) Once we've connected, check to see if we've ever set the local storage.
      // 2) If we've not, then we should do a full refresh from server.
      // 3) If we have, we can just connect and wait for messages as normal.
      if (alsoLoadThreadData) {
        this._SQLStorageService.getChatThreadsWithLastMessage().then(deviceChatThreads => {
          if ((!deviceChatThreads || deviceChatThreads && deviceChatThreads.length == 0) || uponResume) {
            this.doFullThreadRefreshFromServer().then(() => {
              // if this fails, it's not an issue.
              try { this.chatDbIsLockedChange.unsubscribe(); } catch {}
              this.setIsFullyLoadedZoneWrapped(true);
              return this.isFullyLoaded;
            });
          }
          else {
            // Avoid race condition between XMPP server and local DB loading data.
            deviceChatThreads.forEach(dct => {
              var index = this.chatThreads.findIndex(ct => { return ct.guid == dct.guid; })
              if (index > -1) {
                this.chatThreads[index] = dct;
                console.log('*** Updated loaded thread - in connect ***');
              }
              else {
                this.chatThreads.push(dct);
                console.log('*** Added not-yet-loaded thread - in connect ***');
              }
            });
            this.setThreadLastMessageAndDate(this.chatThreads);
            this.sortChatThreads();
            this.checkToShowOrHideTabBadge();
            this.syncProfilePictures();
            this.setIsFullyLoadedZoneWrapped(true);
            return this.isFullyLoaded;
          }
        });
      }
      else {
        // We're just making sure we're connected, no data loading.
        this.setIsFullyLoadedZoneWrapped(true);
        return this.isFullyLoaded;
      }
    });
  }

  generateNewChatKeys(): Promise<boolean> {
    return new Promise((resolve) => {
      this.didCommService.generateChatKeyPair().then(() => {
        resolve(true)
      }).catch(() => {
        resolve(false)
      })
    })
  }










  setIsFullyLoadedZoneWrapped(val: boolean) {
    this._Zone.run(() => {
      this.isFullyLoaded = val;
    });
  }

  syncProfilePictures() {
    if (!this.chatThreads || this.chatThreads && this.chatThreads.length == 0) { return; }

    var matchingThread: ChatThread;
    this._ApiProviderService.getChatContacts().then(comboUserWrappers => {

      console.log('*** syncProfilePictures ***');
      console.log(comboUserWrappers)

      comboUserWrappers.forEach(currentWrapper => {
        matchingThread = this.chatThreads.find(ct => { return ct.userWrapper.chatUserName == currentWrapper.chatUserName; })
        if (matchingThread && currentWrapper.displayPicture) {
          this._SQLStorageService.updateAndSetPhotoByThreadGuid(matchingThread, currentWrapper.displayPicture);
        }
      });
    });
  }

  async decryptEncryptedChatPrivateKey(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.encryptedChatPrivateKey, false).then(encryptedChatPrivateKey => {
        if(!!encryptedChatPrivateKey)
        {
          this._CryptoProviderService.returnUserSymmetricKey().then(symmetricKey => {
            this._CryptoProviderService.symmetricDecrypt(encryptedChatPrivateKey, symmetricKey).then((chatPrivateKey) => {
              console.log(chatPrivateKey)
              this._SecureStorageService.setValue(SecureStorageKey.chatPrivateKey, chatPrivateKey).then(() => {
                this._SecureStorageService.removeValue(SecureStorageKey.encryptedChatPrivateKey).then(() => {
                  resolve()
                }).catch(e => reject(e))
              }).catch(e => reject(e))
            }).catch(e => reject(e))
          }).catch(e => reject(e))
        } else {
          resolve()
        }
      })
    })
  }

  async setLocale() {
    var locale = await this._TranslateProviderService.getLangFromStorage();
    this.dateLocal = locale;

    // update in case we're viewing at the same time.
    if (this.chatThreads && this.chatThreads.length > 0) {
      this.setThreadLastMessageAndDate(this.chatThreads);
    }
  }

  showErrorState(msg: string, presentAsToast?: boolean) {
    if (presentAsToast) {
      this.setIsFullyLoadedZoneWrapped(true);
      // this.presentToast(msg);
      return;
    }

    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant('CHAT.chatError'),
      message: msg,
      buttons: [{
        text: this._TranslateProviderService.instant('GENERAL.ok'),
        cssClass: 'primary',
        handler: () => {}
      }
    ]
    }).then(alert => {
      this.setIsFullyLoadedZoneWrapped(true);
      alert.present();
    });
  }

  pushNewMessageToThread(thread: ChatThread, newMessage: ChatMessage) {
    this._Zone.run(() => {
      if (newMessage && thread) {
        thread.messages.push(newMessage);
      }
    });
  }

  setThreadUnreadCount(thread: ChatThread) {
    this._Zone.run(() => {
      if (!thread) { return; }
      this._SQLStorageService.getThreadUnreadCount(thread.guid).then(unreadCount => {
        thread.unreadCount = unreadCount;
      });
    });
  }

  // This is to avoid repeatedly evaluating inline functions on the contact list page.
  setThreadLastMessageAndDate(threads: ChatThread[]) {
    this._Zone.run(() => {
      threads.forEach(ct => {
        if (ct.messages && ct.messages.length > 0) {
          ct.lastMessage = !!ct.messages[ct.messages.length - 1].parsedMessage ? (['string', 'url'].includes(ct.messages[ct.messages.length - 1].parsedMessage.type )  ?
              ct.messages[ct.messages.length - 1].parsedMessage.value : '') : ct.messages[ct.messages.length - 1].message;
          ct.lastMessageDate = ct.messages[ct.messages.length - 1].date;
          ct.lastMessageDateString = this.getLocaleFullDate(ct.messages[ct.messages.length - 1].date);
        }
      });
    });
  }

  getLocaleFullDate(date: Date) {
    if (!date) { return ''; }
    var str = moment(date).locale(this.dateLocal).format('llll')
    if (this.dateLocal == this._TranslateProviderService.LANG_DE) {
        str = str.replace('.,', '.');
    }
    return str;
  }

  async listenForMessagesWhenIdle(xmppUser: any) {
    xmppUser.onMessage(
      async (xmppMessageInbound) => {
        console.log(`*** ChatService - onMessage ***`);
        // console.log(JSON.stringify(xmppMessageInbound));

        var currentThreadUsername = (this.chatThreadToView) ? this.stripDNS(this.chatThreadToView.userWrapper.chatUserName) : null;
        var newMessage = this.getNewChatMessage_Incoming(xmppMessageInbound);

        // console.log("newMessage.parsedMessage.type: " + newMessage.parsedMessage.type);
        // console.log("this.stripDNS(newMessage.from): " + this.stripDNS(newMessage.from));
        // console.log("this.stripDNS(this.myXmppUser.jid): " + this.stripDNS(this.myXmppUser.jid));
        // console.log(this.stripDNS(newMessage.from) !== this.stripDNS(this.myXmppUser.jid));

        var messageFrom = this.stripDNS(newMessage.from);
        var tk = await this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false);
        this.threadChatKeyPairs = !!tk ? JSON.parse(tk) : {};

        if (newMessage.parsedMessage.type == MessageTypes.certificate) {
          await this.addCertificateToPage(newMessage.parsedMessage.value);
          // await this._DeeplinkProviderService.processPretixTicket();
        }

        if (newMessage.parsedMessage.type == MessageTypes.healthCertificate) {
          await this._DeeplinkProviderService.processEudgcString(newMessage.parsedMessage.value);
        }

        if (newMessage.parsedMessage.type === MessageTypes.keyExchange
          && messageFrom !== this.stripDNS(this.myXmppUser.jid)
          && !!this.threadChatKeyPairs[messageFrom] &&
          !this.threadChatKeyPairs[messageFrom].targetPublicKey
          ) {
          this._KeyExchangeService.updateTargetPublicKey(newMessage, this.threadChatKeyPairs).then(() => {
            this.sendEncryptionMessage(newMessage.from).then(() => {
              console.log("FINISHED SENDING THE RETURN PUBLIC KEY")
            })
          })
        }

        // If we are viewing a thread + we have the callback set + it's from that thread, run the callback.
        if (currentThreadUsername && newMessage.from == currentThreadUsername && this.receiveMessageCallback != null) {
          if (this.chatThreadToView.messages.find(m => { return m.id == newMessage.id; })) {
            console.log(`*** ChatService - message ignored, duplicate id: ${newMessage.id} ***`);
            return;
          }

          newMessage.markedAsUnread = false;
          this._SQLStorageService.addMessage(this.chatThreadToView.guid, newMessage).then(newNumericId => {
            if (newNumericId == null) { return; }
            this.pushNewMessageToThread(this.chatThreadToView, newMessage);
            this.setThreadLastMessageAndDate([this.chatThreadToView]);
            this.receiveMessageCallback();
            this.putThreadAtTop(this.chatThreadToView);
            this.checkToShowOrHideTabBadge();
          });
        }
        // Otherwise, update local threads with the new message & mark thread & message as unread.
        else {
          // Try to find the thread in memory
          var thread = this.chatThreads.find(chatThread => { return this.stripDNS(chatThread.userWrapper.chatUserName) == newMessage.from; })
          if (thread) {
            if (thread.messages.find(m => { return m.id == newMessage.id; })) {
              console.log(`*** ChatService - message ignored, duplicate id: ${newMessage.id} ***`);
              return; // somehow, we're getting a duplicate message ignore it.
            }

            newMessage.markedAsUnread = true;

            this._SQLStorageService.addMessage(thread.guid, newMessage).then(newNumericId => {
              if (newNumericId == null) { return; }
              thread.markedAsUnread = true;
              this.pushNewMessageToThread(thread, newMessage);
              this._SQLStorageService.updateThreadMarkedUnread(thread.guid);
              this.setThreadUnreadCount(thread);
              this.setThreadLastMessageAndDate([thread]);
              this.putThreadAtTop(thread);
              this.checkToShowOrHideTabBadge();
            });
          }
          else {
            // If thread not loaded yet, save these and we'll check for them at load finish.
            this._SQLStorageService.getChatThreadGuidByWithUsernameStripped(newMessage.from).then(threadGuid => {
              if (threadGuid != null) {
                newMessage.markedAsUnread = true;
                this._SQLStorageService.addMessage(threadGuid, newMessage).then(newNumericId => {
                  if (newNumericId == null) { return; }

                  this._SQLStorageService.updateThreadMarkedUnread(threadGuid).then(() => {
                    this.reloadSpecificThread(threadGuid);
                  });
                });
                console.log(`*** ChatService - threads not yet loaded into service, but saved new message to disk. ***`);
              }
              else {
                console.log(`*** ChatService - message ignored, thread not found for: ${newMessage.from}, text: ${newMessage.message} ***`);
              }
            });
          }
        }

        // this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false).then((e) => {
        //   this.threadChatKeyPairs = !!e ? JSON.parse(e) : {};
        //   console.log(this.threadChatKeyPairs);
        // })
      },
      (fromUsername) => {
          var from = this.stripDNS(fromUsername)
          var keyPair = !!this.threadChatKeyPairs[from] ? {
            keyType: 'ed25519',
            publicKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[from].publicKey).result,
            privateKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[from].privateKey).result
          } : null
          return [keyPair, this.keyPair]
      }
    );

    console.log('*** listenForMessagesWhenIdle() callback has been enabled');
  }

  async addCertificateToPage(value: string) {
    var cert = await this._SecureStorageService.getValue(SecureStorageKey.marketplaceCertificates, false);
    var certs = !!cert ? JSON.parse(cert) : [];
    var now = +new Date();
    var nowString = new Date(now);

    var resolve = JSON.parse(value);
    if(!resolve["credentialSubject"]["data"][0]["issueDate"]) {
        resolve["credentialSubject"]["data"][0]["issueDate"] = nowString.toISOString();
    }

    if(!resolve["credentialSubject"]["data"][0]["expiryDate"]) {
        nowString.setHours(23);
        nowString.setMinutes(59);
        nowString.setSeconds(59);
        resolve["credentialSubject"]["data"][0]["expiryDate"] = nowString.toISOString();
    }

    certs.push(resolve);
    await this._SecureStorageService.setValue(SecureStorageKey.marketplaceCertificates, JSON.stringify(certs));
    await this.presentToast(this._TranslateProviderService.instant('CERTIFICATES.certAddSuccess'));
  }


  reloadSpecificThread(threadGuid: string) {
    this._SQLStorageService.getChatThreadsWithLastMessage(threadGuid).then(threads => {

      var thread = (threads && threads.length > 0) ? threads[0] : null;
      if (!thread) { return; }

      var index = this.chatThreads.findIndex(ct => { return ct.guid == thread.guid; })
      if (index > -1) {
        this.chatThreads[index] = thread;
        console.log('*** Updated loaded thread - in reloadSpecificThread ***');
      }
      else {
        this.chatThreads.push(thread);
        console.log('*** Added not-yet-loaded thread - in reloadSpecificThread ***');
      }

    }).then(() => {
      this.setThreadLastMessageAndDate(this.chatThreads);
      this.sortChatThreads();
      this.checkToShowOrHideTabBadge();
      console.log('*** Refreshed thread UI ***');
    });
  }




  sendReturnMessage(cc: any, username: string, targetPublicKey: string) {
    this._KeyExchangeService.updateThreadChatKeyPairs(cc, this.threadChatKeyPairs).then((c) => {
      this.threadChatKeyPairs = c;
      this.threadChatKeyPairs[username].targetPublicKey = targetPublicKey; // Just to be sure
      var returnMessagePackage = this.getNewChatMessage_Outgoing(username, cc);
      this.sendMessage(returnMessagePackage);
    }).catch(e => console.log(e))
  }

  checkToShowOrHideTabBadge() {
    this._Zone.run(() => {
      var hasUnreadChats = (this.chatThreads.find(ct => { return ct.markedAsUnread; }) != null);
      this.chatThreadRequests = this.uniqueArray(this.chatThreadRequests);
      var hasUnreadChatFriendRequests = (this.chatThreadRequests.length > 0);
      this.hasUnreadChatsOrFriendRequests = (hasUnreadChats || hasUnreadChatFriendRequests);
    });
  }

  async getSecureStorageChatUser(): Promise<any> {
    return new Promise(resolve => {
      this._SecureStorageService.getValue(SecureStorageKey.chatUserData).then(val => {
        if (!val) {
          console.log("*** XMPP Dashboard err: no chat user in secure-storage.");
          resolve(false);
        }
        else {
          resolve(JSON.parse(val));
        }
      });
    });
  }

  async returnChatPassword(chatUser: any) {
    var userKey = await this._CryptoProviderService.returnUserSymmetricKey();
    try {
      var selfChatPassword = await this._CryptoProviderService.symmetricDecrypt(chatUser.password, userKey);
    }
    catch (e) {
      userKey = await this._CryptoProviderService.returnVCSymmetricKey();
      selfChatPassword = await this._CryptoProviderService.symmetricDecrypt(chatUser.password, userKey);
    }
    return selfChatPassword;
  }

  doFullThreadRefreshFromServer(): Promise<void> {
    return new Promise(resolve => {
      console.log('*** doFullThreadRefreshFromServer call.');
      this._ApiProviderService.getChatContacts().then(comboUserWrappers => {
        var tempThreads: ChatThread[] = [];
        var thread: ChatThread;

        // Create a wrapper chat thread for the friend.
        comboUserWrappers.forEach(user => {
          thread = this.getNewChatThreadObj(user);
          tempThreads.push(thread);
        });
        if (comboUserWrappers.length == 0) {
          this.chatThreads = [];  // set the threads to empty, to show we've got nothing local.
          this.setContactAndMerchantCount();
          resolve();
        }
        else {
          // Loop each friend and getChatHistory() and populate.
          this.getAllChatHistory(tempThreads).then(() => {
            this.setThreadLastMessageAndDate(tempThreads);
            this.chatThreads = tempThreads;
            this.setContactAndMerchantCount();
            this.sortChatThreads();

            if (this._SQLStorageService.dbIsLocked) {
              this.chatDbIsLockedChange.subscribe(value => {
                console.log('*** dbIsLocked was changed to: ' + value);
                this._SQLStorageService.addNewThreadsWithHistory(this.chatThreads).then(() => {
                  resolve();
                });
              });
            }
            else {
              console.log('*** dbIsLocked already unlocked');
              this._SQLStorageService.addNewThreadsWithHistory(this.chatThreads).then(() => {
                resolve();
              });
            }
          });
        }

      }).catch(err => {
        this.chatThreads = [];  // set the threads to empty, to show we've got nothing local.
        this.setContactAndMerchantCount();
        console.log('*** doFullThreadRefreshFromServer error: ***');
        console.log(err);
        resolve();
      });
    });
  }

  getAllChatHistory(tempThreads: ChatThread[]): Promise<void> {
    return new Promise(resolveAll => {
      var self = this;

      console.log('getAllChatHistory length: ' + tempThreads.length);

      var tasks = function(tempThreads) {
        var p = Promise.resolve(); // Q() in q
        tempThreads.forEach(thread => {
          p = p.then(() => self.getChatHistoryForThread(thread));
        });
        return p;
      };

      tasks(tempThreads).then(() => {
        console.log('Finished tasks in getAllChatHistory')
        resolveAll();
      });
    });
  }

  getChatHistoryForThread(tempThread: ChatThread): Promise<void> {
    return new Promise(resolve => {
      console.log('getChatHistoryForThread: ' + tempThread.userWrapper.chatUserName);

      var threadKeyPair = !!this.threadChatKeyPairs[tempThread.userWrapper.chatUserName] ? {
        keyType: 'ed25519',
        publicKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[tempThread.userWrapper.chatUserName].publicKey).result,
        privateKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[tempThread.userWrapper.chatUserName].privateKey).result
      } : null
      var keyPair = [threadKeyPair , this.keyPair]

      this.myXmppUser.getChatHistory(tempThread.userWrapper.chatUserName, null, null, keyPair).then(response => {
        console.log('getChatHistory response: ' + JSON.stringify(response));

        if (response && response.messages) {

          // 1) for some reason, messages are retrieved in reverse, so reverse the list.
          response.messages.reverse();

          // 2) De dupe - for some reason, sometimes we can get duplicates.
          var deDupeList = [];

          var message: ChatMessage;

          response.messages.forEach(responseMessage => {
            if (!deDupeList.find(keepMessage => { return responseMessage.id == keepMessage.id; })) {
              message = this.getNewChatMessage_FromHistoryItem(responseMessage);
              if (message) {
                deDupeList.push(message);
                var messageFrom = this.stripDNS(message.from);
                if (message.parsedMessage.type == MessageTypes.healthCertificate) {
                  this._DeeplinkProviderService.processEudgcString(message.parsedMessage.value);
                }
                if(message.parsedMessage.type === MessageTypes.keyExchange &&
                  messageFrom !== this.stripDNS(this.myXmppUser.jid) &&
                  !!this.threadChatKeyPairs[messageFrom] &&
                  !this.threadChatKeyPairs[messageFrom].targetPublicKey) {
                  this._KeyExchangeService.updateTargetPublicKey(responseMessage, this.threadChatKeyPairs).then(( ) => {
                    this.sendEncryptionMessage(messageFrom).then(() => {
                      console.log("FINISHED SENDING THE RETURN PUBLIC KEY");
                    })
                  })
                }
              }
            }
          });

          tempThread.messages = deDupeList;

        }

        resolve();
      }).catch(err => {
        console.log('*** getChatHistoryForThread err:');
        console.log(err);
        resolve();
      });
    });
  }

  getNewChatMessage_FromHistoryItem(responseMessage: any) {
    try {
      var newMessage = <ChatMessage> {
        id: responseMessage.id,
        from: this.stripDNS(responseMessage.from),
        to: this.stripDNS(responseMessage.to),
        message: responseMessage.message,
        parsedMessage: JSON.parse(responseMessage.message),
        date: new Date(responseMessage.date),
        markedAsUnread: false
      };

      return newMessage;
    }
    catch(ex) {
      console.log(ex);
      return null;
    }
  }

  sendMessage(newMessage: ChatMessage): Promise<void> {
    var username = this.stripDNS(newMessage.to)
    console.log("sending message: username: " + username)

    var keyPair = !!this.threadChatKeyPairs[username] ? (!!this.threadChatKeyPairs[username].targetPublicKey ? {
      keyType: 'ed25519',
      publicKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[username].publicKey).result,
      privateKey: this._CryptoProviderService.hex2i(this.threadChatKeyPairs[username].privateKey).result
    } : this.keyPair ) : this.keyPair

    var userTargetChatPublicKey = this.returnTargetUserChatPublicKeyFromChatUsername(username)

    console.log("sending message: keyPair: " + JSON.stringify(keyPair))

    var targetPublicKeyUInt8Array = !!this.threadChatKeyPairs[username] ? (!!this.threadChatKeyPairs[username].targetPublicKey ?
    this._CryptoProviderService.hex2i(this.threadChatKeyPairs[username].targetPublicKey).result :
    userTargetChatPublicKey ) : userTargetChatPublicKey

    // if(newMessage.parsedMessage.type == MessageTypes.keyExchange && !userTargetChatPublicKey) {
    //   console.log('--------- The required target users user specific publicKey was not found and hence can not send the keyExchange message ----------')
    //   return;
    // }

    var postMessageSending = () => {
      this.pushNewMessageToThread(this.chatThreadToView, newMessage);
      this.setThreadLastMessageAndDate([this.chatThreadToView]);
      this.putThreadAtTop(this.chatThreadToView);
      this._SQLStorageService.addMessage(this.chatThreadToView.guid, newMessage);
    }

    console.log('*** Target User chatPublicKey: ' + JSON.stringify(targetPublicKeyUInt8Array))
    console.log('*** Send message to: ' + newMessage.to);

    if(newMessage.parsedMessage.type == MessageTypes.keyExchange) {
      // keyPair = this.keyPair;
      // targetPublicKeyUInt8Array = userTargetChatPublicKey;
      return new Promise<void>(resolve => {
        this.myXmppUser.sendMessage(newMessage.to, JSON.stringify(newMessage.message));
          postMessageSending();
          resolve();
      })
    }

    return new Promise<void>(resolve => {
      this._SecureStorageService.getValue(SecureStorageKey.toggleChatEncryption, false).then((toggleChatEncryption) => {
        this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then((selfData) => {
          var thisUser = JSON.parse(selfData) as any;
          // console.log(thisUser);
          var toUser = this.chatThreads.find(k => this.stripDNS(k.userWrapper.chatUserName) == username);
          // console.log(toUser);
          if(!toggleChatEncryption || toggleChatEncryption == "false" || thisUser.merchant || toUser.userWrapper.merchant) {
            this.myXmppUser.sendMessage(newMessage.to, JSON.stringify(newMessage.message));
            postMessageSending();
            resolve();
          } else {
            this.myXmppUser.sendEncryptedMessage(JSON.stringify(newMessage.message), newMessage.to, targetPublicKeyUInt8Array, keyPair);
            postMessageSending();
            resolve();
          }
        })
      })
    });
  }

  sortChatThreads() {
    // 1) Chats with messages + dates at the top.
    // 2) Put empty chats underneath, sorted by name.
    var activeChats = this.chatThreads.filter(ct => { return ct.lastMessageDate != null; })
    var emptyChats = this.chatThreads.filter(ct => { return ct.lastMessageDate == null; })

    activeChats = activeChats.sort((a, b) => { return this.sortByNewestDate(a, b); });
    emptyChats = emptyChats.sort((a, b) => { return this.sortByNameAsc(a, b); });
    this.chatThreads = activeChats.concat(emptyChats);
    this.setContactAndMerchantCount();
  }

  sortByNewestDate(a: any, b: any) {
    if (!a || !a.lastMessageDate || a.lastMessageDate == null) {
      return 0;
    }
    else if (!b || !b.lastMessageDate || b.lastMessageDate == null) {
      return 0;
    }
    else if (a.lastMessageDate.valueOf() < b.lastMessageDate.valueOf()) {
      return 1;
    }
    else if (a.lastMessageDate.valueOf() > b.lastMessageDate.valueOf()) {
      return -1;
    }
    return 0;
  }

  sortByNameAsc(a: any, b: any) {
    if (!a || !a.userWrapper || a.userWrapper.username == null) {
      return 0;
    }
    else if (!b || !b.userWrapper.username || b.userWrapper.username == null) {
      return 0;
    }
    else if (a.userWrapper.username.valueOf() < b.userWrapper.username.valueOf()) {
      return -1;
    }
    else if (a.userWrapper.username.valueOf() > b.userWrapper.username.valueOf()) {
      return 1;
    }
    return 0;
  }

  putThreadAtTop(chatThreadToTop: ChatThread) {
    this._Zone.run(() => {
      var index = this.chatThreads.findIndex(ct => { return ct.guid == chatThreadToTop.guid; })
      if (index != -1) {
        this.chatThreads.splice(index, 1);
        this.chatThreads.unshift(chatThreadToTop);
      }
    });
  }

  returnTargetUserChatPublicKeyFromChatUsername(chatUserName) {
    var user = this.chatThreads.find(k => this.stripDNS(k.userWrapper.chatUserName) == this.stripDNS(chatUserName))
    // console.log(user);
    return !!user ? new Uint8Array(user.userWrapper.chatPublicKey.match(/.{1,2}/g).map(byte => parseInt(byte, 16))) : null
  }

  reloadChatRequests() {
    return this._SQLStorageService.getChatThreadRequests().then(updatedChatThreadRequests => {
      this.chatThreadRequests = updatedChatThreadRequests;
      this.chatThreadRequests = this.uniqueArray(this.chatThreadRequests);
      // console.log(this.chatThreadRequests);
      this.checkToShowOrHideTabBadge();
      this.setContactAndMerchantCount();
      this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false).then((e) => {
        this.threadChatKeyPairs = !!e ? JSON.parse(e) : {};
        // console.log(this.threadChatKeyPairs);
        // console.log(this.chatThreadRequests);
      })
    });
    // ANSIK Check here!!!!!!
  }

  listenForFriendRequestsWhenIdle(xmppUser: any) {
    this.reloadChatRequests();
    // Someone deleted you from their contacts / deleted your friend request.
    xmppUser.on('unsubscribed', (event) => {
      if (event && event.attrs && event.attrs.from) {
        this._SQLStorageService.deleteFriendRequestOnlyByUsername(this.stripDNS(event.attrs.from)).then(() => {
          this.reloadChatRequests();
        });
      }
    });

    // Your friend request was accepted.
    xmppUser.on('subscribed', (event) => {
      if (event && event.attrs && event.attrs.from) {
        var username = this.stripDNS(event.attrs.from);
        this.chatThreadRequests = this.uniqueArray(this.chatThreadRequests);
        var chatThreadRequest = this.chatThreadRequests.find(ctr => { return ctr.to == username; })

        if (!chatThreadRequest || chatThreadRequest.isProcessing) {
          console.log('Received duplicate event that is still processing, so ignore.');
          return;
        }

        // we are passing in the retrieved full username with DNS here, which we didn't have before.
        this.acceptFriendRequest(chatThreadRequest, event.attrs.from).then(() => {
          // if this was from a merchant and we're not currently in a chat, go to that chat.
          if (window.location.href.indexOf('/chat') == -1 && chatThreadRequest.userWrapper.merchant) {
            var chatThread = this.chatThreads.find(ct => { return ct.userWrapper.chatUserName == event.attrs.from; })
            if (chatThread) {
              this.chatThreadToView = chatThread;
              this.navigateToChat();
            }
          }
        });
      }
    });

    // Make this an always on listener:
    xmppUser.subscribeFriendRequests(incoming => {
      var from = (incoming && incoming.attrs && incoming.attrs.from) ? incoming.attrs.from : null;
      if (!from) { return; }

      // 1) Get current db list again.
      this._SQLStorageService.getChatThreadRequests().then(chatThreadRequests => {
        var username = this.stripDNS(incoming.attrs.from);

        var existingRequest = chatThreadRequests.find(ctr => {
          return ctr.from == username;
        });
        if (existingRequest) { return; }

        // 2) If not existing, store in db.
        this._ApiProviderService.retrieveFriendRequestPermissionsForUser(this.stripDNS(from)).then(metaData => {
          var newChatThreadRequest = this.getNewChatThreadRequest_Incoming(from, metaData);

            // 3) When finished, get list again fresh from storage and expose on this.chatThreadRequests array.
            this._SQLStorageService.addChatThreadRequest(newChatThreadRequest).then(() => {
              this.presentToast(this._TranslateProviderService.instant('CHAT.friendRequest'));
              this.reloadChatRequests();
            }).then(() => {
              this.getMyUser_IsMerchant().then(myUserIsMerchant => {
                if (myUserIsMerchant) {
                  // If we're a merchant, auto-accept the friend request.
                  this.autoAcceptFriendRequestAsMerchant(newChatThreadRequest);
                }
              });
            });

        }).catch(err => {
          console.log(err);
          // permissions were not set, so this could be an erroneous event
          // from xmpp, ignore.
        });
      })
    });
  }

  async sendFriendRequest(toJid: string, scannedContactInfo: Contact) {
    console.log(`*** MW calling: sendFriendRequest for: ${this.stripDNS(toJid)}`);
    var permissionsSet = await this._ApiProviderService.setFriendRequestPermissionsForUser(this.stripDNS(toJid));
    console.log('*** MW done: sendFriendRequest.');

    if (!permissionsSet) {
      return this.showErrorState('setFriendRequestPermissionsForUser failed', true);
    }

    console.log(`*** XMPP calling: sendFriendRequest: ${toJid}`);
    await this.myXmppUser.sendFriendRequest(toJid);
    console.log('*** XMPP done: sendFriendRequest.');

    var newChatThreadRequest = this.getNewChatThreadRequest_Outgoing(toJid, scannedContactInfo);

    this._SQLStorageService.addChatThreadRequest(newChatThreadRequest).then(() => {
      this.presentToast(this._TranslateProviderService.instant('CHAT.friendRequestSent'));
      this.reloadChatRequests();
      this._PopoverController.dismiss();
    });
  }

  deleteFriend(chatUsername: string) {
    this.chatThreadRequests = this.uniqueArray(this.chatThreadRequests);
    var chatThreadRequestToDelete = this.chatThreadRequests.find(ctr => { return ctr.userWrapper.chatUserName == chatUsername; })
    this.chatThreadRequests = this.chatThreadRequests.filter(ctr => { return ctr.userWrapper.chatUserName != chatUsername; })

    var threadToDelete = this.chatThreads.find(ct => { return ct.userWrapper.chatUserName == chatUsername; })
    this.chatThreads = this.chatThreads.filter(ct => { return ct.userWrapper.chatUserName != chatUsername; })
    this.setContactAndMerchantCount();

    if (chatThreadRequestToDelete || threadToDelete) {
      var guid = (chatThreadRequestToDelete) ? chatThreadRequestToDelete.guid : threadToDelete.guid;
      this._SQLStorageService.deleteThreadAndAnyFriendRequestsByGuid(guid);
    }

    this.reloadChatRequests();

    this.myXmppUser.acceptRemoveFriend(chatUsername);

    // Call MW, which will handle the xmpp roster calls for both users.
    console.log(`*** MW calling: deleteFriend: ${chatUsername}`);
    this._ApiProviderService.deleteFriend(chatUsername).then(success => {
      console.log(`*** MW done: deleteFriend: ${success}`);
    });
  }

  async acceptFriendRequest(chatThreadRequest: ChatThreadRequest, overrideUsernameVal?: string) {
    chatThreadRequest.isProcessing = true;

    var newThread = this.getNewChatThreadObjFromChatThreadRequest(chatThreadRequest, overrideUsernameVal);
    var username = this.stripDNS(newThread.userWrapper.chatUserName);

    // We only need to set permissions + send accept to XMPP if we're ACCEPTING a request, the sender already has permissions set.
    if (!overrideUsernameVal) {
      console.log(`*** MW calling: setFriendRequestPermissionsForUser: ${username}`);
      var permissionsSet = await this._ApiProviderService.setFriendRequestPermissionsForUser(username);
      console.log('*** MW done: setFriendRequestPermissionsForUser.');

      if (!permissionsSet) {
        this.showErrorState('setFriendRequestPermissionsForUser failed', true);
        return null;
      }

      console.log(`*** XMPP calling: acceptFriendRequest for: ${newThread.userWrapper.chatUserName}`);
      await this.myXmppUser.acceptFriendRequest(newThread.userWrapper.chatUserName);
      console.log('*** XMPP done: acceptFriendRequest.');

      var self = await this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false);

      if(!newThread.userWrapper.merchant || !(JSON.parse(self) as any).merchant) {
        this._KeyExchangeService.initiateKeyExchangeProcess(newThread, this.myXmppUser.jid, this.threadChatKeyPairs).then((newMessage) => {
          if (this.isConnected()) {
            this.sendMessage(newMessage).then(() => {
                this._LoaderProviderService.loaderDismiss().then(() => {})
            })
          }
          else {
            this.connect(false).then(() => {
              this.sendMessage(newMessage).then(() => {
                  this._LoaderProviderService.loaderDismiss().then(() => {
                  })
              })
            })
          }
        })
      }
    }

    // If somehow we've got a friend request for someone we already have in our contacts,
    // Re-set the permissions and then just delete it.
    var existingThread = this.chatThreads.find(ct => { return ct.userWrapper.chatUserName == username; })
    if (existingThread) {
      await this._SQLStorageService.deleteFriendRequestOnlyByUsername(username).then(() => {
        this.reloadChatRequests();
      });
      this.chatThreadToView = newThread;
      return existingThread;
    }
    else {
      // This is to avoid duplicate acceptance events coming in from XMPP.
      var wasAdded = await this._SQLStorageService.addNewThread(newThread);
      if (wasAdded) {
        await this._SQLStorageService.deleteFriendRequestOnlyByUsername(username).then(() => {
          this.reloadChatRequests();
        });

        // Add the new thread to the top of the list, as it's the most recent.
        this.chatThreads.unshift(newThread);
        this.chatThreadToView = newThread;
        return newThread;
      }
      return null;
    }
  }

  async autoAcceptFriendRequestAsMerchant(chatThreadRequest: ChatThreadRequest) {
    this.acceptFriendRequest(chatThreadRequest).then(newThread => {
      if (!newThread && chatThreadRequest) {
        chatThreadRequest.isProcessing = false;
      }
      // if (newThread) {
      //   if (!newThread.userWrapper.merchant) {
      //     this._KeyExchangeService.initiateKeyExchangeProcess(newThread, this.myXmppUser.jid, this.threadChatKeyPairs).then((newMessage) => {
      //       if (this.isConnected()) {
      //         this.sendMessage(newMessage).then(() => {
      //             this._LoaderProviderService.loaderDismiss().then(() => {})
      //         })
      //       }
      //       else {
      //         this.connect(false).then(() => {
      //           this.sendMessage(newMessage).then(() => {
      //               this._LoaderProviderService.loaderDismiss().then(() => {
      //             })
      //           })
      //         })
      //       }
      //     })
      //   }
      // }
    });
  }

  async presentToast(message: string) {
    const toast = await this._ToastController.create({
        message,
        duration: 2000,
        position: 'top',
    });
    await toast.present();
  }

  stripDNS(username: string) {
    var index = username.indexOf('@');
    if (index > -1) {
      return username.substring(0, index);
    }
    return username;
  }

  getMyUser_Username(): Promise<string> {
    return new Promise(resolve => {
      this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then(chatUserData => {
        try {
          var user = <any>JSON.parse(chatUserData);
          resolve(user.username);
        }
        catch(e) {
          resolve(null);
        }
      }).catch(err => {
        resolve(null);
      });
    })
  }

  getMyUser_Name(): Promise<string> {
    return new Promise(resolve => {
      this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then(chatUserData => {
        try {
          var user = <any>JSON.parse(chatUserData);
          resolve(user.name);
        }
        catch(e) {
          resolve(null);
        }
      }).catch(err => {
        resolve(null);
      });
    })
  }

  getMyUser_IsMerchant(): Promise<string> {
    return new Promise(resolve => {
      this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false).then(chatUserData => {
        try {
          var user = <any>JSON.parse(chatUserData);
          resolve(user.merchant);
        }
        catch(e) {
          resolve(null);
        }
      }).catch(err => {
        resolve(null);
      });
    })
  }

  disconnect(): Promise<void> {
    return new Promise(resolve => {
      this.setIsFullyLoadedZoneWrapped(false);
      this.chatThreads = [];
      this.chatThreadRequests = [];
      this.setContactAndMerchantCount();

      if (!this.myXmppUser) { resolve(); }
      else {
        try {
          this.myXmppUser.closeConnection();
          this._EventsService.publish(EventsList.closeAnyChats);
          resolve();
        }
        catch(e) {
          this._EventsService.publish(EventsList.closeAnyChats);
          resolve();
        }
      }
    });
  }

  isFriend(username: string): Promise<boolean> {
    return new Promise(resolve => {
      username = this.stripDNS(username);
      console.log('*** isExistingContact call: ' + username);
      this._ApiProviderService.getChatContacts().then(comboUserWrappers => {
        if (comboUserWrappers.length == 0) {
          resolve(false);
        }
        else {
          var contact = comboUserWrappers.find(item => {
            return this.stripDNS(item.chatUserName) == username;
          });
          resolve(contact != null);
        }
      }).catch(err => {
        console.log('*** isExistingContact error:');
        console.log(err)
        resolve(false);
      })
    })
  }

  showSearchUserByUsername() {
    this._AlertController.create({
      mode: 'ios',
      header: this._TranslateProviderService.instant('CHAT.searchForUser'),
      inputs: [{
        name: 'username',
        type: 'text',
        placeholder: this._TranslateProviderService.instant('LOGIN.userName') + '...'
      }],
      buttons: [
        {
          text: this._TranslateProviderService.instant('BUTTON.CANCEL'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        },
        {
          text: this._TranslateProviderService.instant('BUTTON.SEARCH'),
          handler: (data) => {
            if ((!data || !data.username) || (data.username.trim().length == 0)) { return; }
            this.tryToAddUserByNameSearch(data.username.trim())
          }
        }
      ]
    }).then(alert => {
      alert.present();
    });
  }

  tryToAddUserByNameSearch(username: string, tryToDismissLoader?: boolean) {
    this._SQLStorageService.getChatThreadRequests().then(chatThreadRequests => {
      // We are checking against our NAME not USERNAME property here.
      // Reason: User is not typing in the DNS or any suffix.
      this.getMyUser_Name().then(me => {
        if (!me || (username.toLowerCase() == me.toLowerCase())) {
          if (tryToDismissLoader) {
            this._LoaderProviderService.loaderDismiss();
          }
          return this.presentToast(this._TranslateProviderService.instant('CHAT.error-adding-own'));
        }

        this._ApiProviderService.searchByChatUsername(username).then(users => {
          if (!users || users && users.length == 0) { return this.presentToast(this._TranslateProviderService.instant('CHAT.userNotFound')); }

          this.isFriend(users[0].chatUserName).then(isExistingContact => {
            if (isExistingContact) {
              return this.presentToast(this._TranslateProviderService.instant('CHAT.error-contact-already-added'));
            }

            var scannedContactInfo = this.getContactModelFromComboWrapper(users[0]);

            var existingRequest = chatThreadRequests.find(ctr => {
              return ctr.userWrapper.chatUserName == scannedContactInfo.username
            });
            if (existingRequest) { return; }

            this.sendFriendRequest(users[0].chatUserName, scannedContactInfo).then(() => {
              if(!users[0].merchant) {
                this._KeyExchangeService.generateNewThreadSpecificChatKeyPair(users[0].chatUserName, null, this.threadChatKeyPairs);
              }
              if (tryToDismissLoader) {
                this._LoaderProviderService.loaderDismiss();
              }
            })
          });

        }).catch(err => {
          if (tryToDismissLoader) {
            this._LoaderProviderService.loaderDismiss();
          }
          this.presentToast(this._TranslateProviderService.instant('CHAT.errorSearchingUsers'));
          console.log(err);
        });
      });

    });
  }

  tryToAddMerchantViaName(username: string, tryToDismissLoader?: boolean) {
    // this._SQLStorageService.getChatThreadRequests().then(chatThreadRequests => {
      // We are checking against our NAME not USERNAME property here.
      // Reason: User is not typing in the DNS or any suffix.
      this.getMyUser_Name().then(me => {
        if (!me || (username.toLowerCase() == me.toLowerCase())) {
          if (tryToDismissLoader) {
            this._LoaderProviderService.loaderDismiss();
          }
          return this.presentToast(this._TranslateProviderService.instant('CHAT.error-adding-own'));
        }

        var merchantName = username.toLowerCase() + environment.chatUsernameSuffix + "@" + environment.chatServerUrl;

        this._ApiProviderService.addMerchantAsRoster(merchantName).then(() => {

            this._ApiProviderService.searchByChatUsername(username).then(users => {
              if (!users || users && users.length == 0) { return this.presentToast(this._TranslateProviderService.instant('CHAT.userNotFound')); }

              var newChatThread = this.getNewChatThreadObj(users[0])
              this._SQLStorageService.addNewThread(newChatThread).then(() => {
                this.reloadChatRequests();
                this.connect(true, false).then(() => {
                  this._LoaderProviderService.loaderDismiss();
                  this.chatThreadToView = newChatThread;
                  this.navigateToChat();
                })
              })

            }).catch(err => {
              if (tryToDismissLoader) {
                this._LoaderProviderService.loaderDismiss();
              }
              this.presentToast(this._TranslateProviderService.instant('CHAT.errorSearchingUsers'));
              console.log(err);
            });
          // })
        })
      });
    // });
  }



  setContactAndMerchantCount() {
    this.contactCount = (this.chatThreads) ? this.chatThreads.filter(ct => { return !ct.userWrapper.merchant }).length : 0;
    this.merchantCount = (this.chatThreads) ? this.chatThreads.filter(ct => { return ct.userWrapper.merchant }).length : 0;
  }

  // ****************************** Utilities for building objects ******************************

  getContactModelFromComboWrapper(comboUserWrapper: ComboUserWrapper) {
    return <Contact> {
      username: comboUserWrapper.chatUserName,
      chatPublicKey: comboUserWrapper.chatPublicKey,
      name: this.stripDNS(comboUserWrapper.didClearName),
      photo: comboUserWrapper.displayPicture,
      merchant: comboUserWrapper.merchant
    };
  }

  getNewChatThreadRequest_Outgoing(toJid: string, contact: Contact) {
    return <ChatThreadRequest> {
      guid: this._SQLStorageService.generateUniqueId(),
      userWrapper: <ComboUserWrapper> {
        chatUserName: toJid,
        username: contact.name,
        didClearName: null,
        chatPublicKey: contact.chatPublicKey,
        displayPicture: contact.photo,
        merchant: contact.merchant
      },
      from: null,
      to: this.stripDNS(toJid)
    };
  }

  getNewChatThreadRequest_Incoming(fromJid: string, inputUserItem: ComboUserWrapper) {
    return <ChatThreadRequest> {
      guid: this._SQLStorageService.generateUniqueId(),
      userWrapper: inputUserItem,
      from: this.stripDNS(fromJid),
      to: null
    };
  }

  getNewChatThreadObjFromChatThreadRequest(chatThreadRequest: ChatThreadRequest, overrideUsernameVal?: string) {
    var newThread = <ChatThread> {
      guid: chatThreadRequest.guid,
      userWrapper: chatThreadRequest.userWrapper,
      lastMessageDate: null,
      lastMessageDateString: null,
      messages:[],
      markedAsUnread: false
    };

    if (overrideUsernameVal) {
      newThread.userWrapper.chatUserName = overrideUsernameVal;
    }
    return newThread;
  }

  getNewChatThreadObj(inputUserItem: ComboUserWrapper) {
    return <ChatThread> {
      guid: this._SQLStorageService.generateUniqueId(),
      userWrapper: inputUserItem,
      lastMessageDate: null,
      lastMessageDateString: null,
      messages:[],
      markedAsUnread: false
    };
  }

  getNewChatMessage_Outgoing(toUsername: string, cc: any) {
    var messageStructure = <MessageStructure> {
      type: MessageTypes.keyExchange,
      value: cc[toUsername].publicKey
    };

    return <ChatMessage> {
      from: this.stripDNS(this.myXmppUser.jid),
      to: toUsername,
      message: JSON.stringify(messageStructure),
      parsedMessage: messageStructure,
      date: new Date()
    };
  }

  getNewChatMessage_Incoming(xmppMessageInbound: any) {
    // Get message Id if available.
    var id = (xmppMessageInbound && xmppMessageInbound.stanza
      && xmppMessageInbound.stanza.attrs && xmppMessageInbound.stanza.attrs.id) ?
    xmppMessageInbound.stanza.attrs.id : this._SQLStorageService.generateUniqueId();

    // delayed messages will have a stamp attr, otherwise just add new Date() as it's just been sent.
    var dateOfMessage = new Date();
    if (xmppMessageInbound && xmppMessageInbound.stanza && xmppMessageInbound.stanza.children) {
      var delayNode = xmppMessageInbound.stanza.children.find(c => { return c.name == "delay" });
      if (delayNode && delayNode.attrs && delayNode.attrs.stamp) {
        dateOfMessage = new Date(delayNode.attrs.stamp);
      }
    }

    var newMessge = <ChatMessage> {
      id: id,
      from: this.stripDNS(xmppMessageInbound.from),
      to: this.myXmppUser.jid,
      message: xmppMessageInbound.message,
      parsedMessage: JSON.parse(xmppMessageInbound.message),
      date: dateOfMessage
    };

    newMessge.parsedMessage.value = newMessge.parsedMessage.value.trim()
    return newMessge;
  }

  async sendEncryptionMessage(to: string) {
    var targetChatUsername = this.stripDNS(to);

    var c = await this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false);
    var completeSet = !!c ? JSON.parse(c) : {}

    console.log("SENDING RETURN PUBLIC KEY");
    console.log(completeSet);
    console.log(completeSet[targetChatUsername]);
    console.log(completeSet[targetChatUsername].publicKey);

      var nm: MessageStructure = {
        type: MessageTypes.keyExchange,
        value: completeSet[targetChatUsername].publicKey
      }
      var newMessage = <ChatMessage> {
        from: this.stripDNS(this.myXmppUser.jid),
        to: targetChatUsername,
        message: JSON.stringify(nm),
        parsedMessage: nm,
        date: new Date()
      };
      if (this.isConnected()) {
        this.sendMessage(newMessage).then(() => {
          this._KeyExchangeService.updateThreadChatKeyPairs(completeSet, this.threadChatKeyPairs).then((c) => {
            this.threadChatKeyPairs = c;
            // console.log(' +++++++ 9 threadChatPublicKey shared via chat');
            // console.log(this.threadChatKeyPairs);
          })
        })
      }
      else {
        this.connect(true).then(() => {
          this.sendMessage(newMessage).then(() => {
            this._KeyExchangeService.updateThreadChatKeyPairs(completeSet, this.threadChatKeyPairs).then((c) => {
              this.threadChatKeyPairs = c;
              // console.log(' ++++++ 10 threadChatPublicKey shared via chat');
              // console.log(this.threadChatKeyPairs);
            })
          })
        })
      }
    // }).catch(e => console.log(e))
  }

  private uniqueArray(t: any) {
    var tk = Array.from(t, k => JSON.stringify(k));
    var tt = uniq(tk);
    return Array.from(tt, k => JSON.parse((k as string)));
  }

}
