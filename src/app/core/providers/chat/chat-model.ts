export interface ChatThreadRequest {
    guid: string;
    from: string; // if 'from' is present, show accept/delete.
    to: string; // if 'to' is present, just show delete.
    userWrapper: ComboUserWrapper;
    isProcessing: boolean;
}

export interface ChatThread {
    guid: string;
    userWrapper: ComboUserWrapper;
    lastMessage: string;
    lastMessageDate: Date;
    lastMessageDateString: string;
    messages: ChatMessage[];
    isLocked: boolean;
    markedAsUnread: boolean;
    unreadCount: number;
}

export interface ChatMessage {
    id: string;
    from: string;
    to: string;
    message: string;
    parsedMessage?: MessageStructure;
    date: Date;
    markedAsUnread: boolean;
    unixDate: any;
    numericId: number; // for infinite scroll loading.
}

export interface ComboUserWrapper {
    chatUserName: string;
    username: string;
    didClearName: string;
    chatPublicKey: string;
    displayPictureName: string;
    displayPicture: string;
    merchant: boolean
}

export interface RosterItem {
    jid: string;
    nickname: string;
}

export interface Hex2iResult {
    error: any;
    result: any;
}

export interface MessageStructure {
    mode?: string;
    uuid?: string;
    type: string;
    value: any;
}

export interface ChatKeyPairs {
    [targetUsername: string]: {
    publicKey: string;
    privateKey: string;
    targetPublicKey?: any;}
}

export enum MessageTypes {
    keyExchange = 'keyExchange',
    string = 'string',
    url = 'url',
    image = 'image',
    video = 'video',
    location = 'location',
    certificate = 'certificate',
    healthCertificate = 'healthCertificate',
    verifiableCredential = 'verifiableCredential',
    qrCode = 'qrCode'
}