import { Injectable } from '@angular/core';
import { ChatMessage, MessageStructure, MessageTypes, ChatThread, ChatKeyPairs } from './chat-model';
import { LoaderProviderService } from '../loader/loader-provider.service';
import { SecureStorageService } from '../secure-storage/secure-storage.service';
import { SecureStorageKey } from '../secure-storage/secure-storage-key.enum';
import { CryptoProviderService } from '../crypto/crypto-provider.service';
import { ApiProviderService } from '../api/api-provider.service';
import { ethers, Wallet } from 'ethers';
import { environment } from 'src/environments/environment';
import { EthereumNetworks } from '../wallet-connect/constants';
import { VaultSecretKeys, VaultService } from '../vault/vault.service';
import { DidCommService } from '../did-comm/did-comm.service';

@Injectable({
  providedIn: 'root'
})
export class KeyExchangeService {

  constructor(
    private _LoaderProviderService: LoaderProviderService,
    private _SecureStorageService: SecureStorageService,
    private _ApiProviderService: ApiProviderService,
    private _Vault: VaultService,
    private _CryptoProviderService: CryptoProviderService,
    private didCommService: DidCommService
  ) { }

  stripDNS(username: string) {
    var index = username.indexOf('@');
    if (index > -1) {
      return username.substring(0, index);
    }
    return username;
  }

  

  public updateTargetPublicKey(newMessage: ChatMessage, threadChatKeyPairs: any): Promise<void> {
    var from = this.stripDNS(newMessage.from);
    // console.log("UPDATING TARGET PUBLIC KEY");
    // console.log("threadChatKeyPairs: " + JSON.stringify(threadChatKeyPairs));
    // console.log(!!threadChatKeyPairs[from] && !threadChatKeyPairs[from].targetPublicKey);
    return new Promise((resolve, reject) => {
      if(!!threadChatKeyPairs[from] && !threadChatKeyPairs[from].targetPublicKey) {
        // console.log("Step One")
        this.updateThreadPublicKeyFromOtherUser(from, newMessage.parsedMessage.value, threadChatKeyPairs).then(c => {
          // console.log("Step Two")
            this._LoaderProviderService.loaderDismiss().then(() => {
              // console.log("Step Three")
              this.storeUserChatKeyBackup(c).then(() => {
                // console.log("Step Four")
                resolve()
              }).catch(e => resolve())
            }).catch(e => reject(e))
          }).catch(e => reject(e))
      } else {
        // console.log("Step Five")
        resolve()
      }
    })
  }

  initiateKeyExchangeProcess(newThread: ChatThread, jid: string, threadChatKeyPairs: any): Promise<ChatMessage> {
    return new Promise((resolve, reject) => {
      console.log(' +++++++ 1 UserB: Loader Initiated')
      this._LoaderProviderService.loaderCreate().then(() => {
        var targetChatUsername = newThread.userWrapper.chatUserName
        targetChatUsername = this.stripDNS(targetChatUsername)
        console.log(' +++++++ 2 UserB: Generating New Thread Specific Chat Key Pair for ' + targetChatUsername)
        this.generateNewThreadSpecificChatKeyPair(targetChatUsername, null, threadChatKeyPairs).then(completeSet => {
          var nm: MessageStructure = {
            type: MessageTypes.keyExchange,
            value: completeSet[targetChatUsername].publicKey
          }
          var newMessage = <ChatMessage> {
            from: this.stripDNS(jid), //this.myXmppUser.jid
            to: targetChatUsername,
            message: JSON.stringify(nm),
            parsedMessage: nm,
            date: new Date()
          };
          console.log(' +++++++ 3 UserB: Sending Message to ' + newMessage.to + ' : ' + newMessage.message)
          resolve(newMessage)
        }).catch(e => reject(e))
      }).catch(e => reject(e))
    })
  }

  updateThreadChatKeyPairs(cc : any, threadChatKeyPairs: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.setValue(SecureStorageKey.chatKeyPairs, JSON.stringify(cc)).then(() => {
        console.log("SECURE STORAGE CHATKEYPAIRS SET SUCCESFFULY!!");
        threadChatKeyPairs = cc;
        console.log("PUBLIC VARIABLE SET SUCCESSFULY!!");
        console.log(cc);
        resolve(cc);
      }).catch(() => reject())
    })
  }

  public sendUserMnemonic(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.userMnemonic, false).then((userMnemonic) => {
        this._CryptoProviderService.returnBackupSymmetricKey().then((backupSymmetricKey) => {
          this._CryptoProviderService.symmetricEncrypt(JSON.stringify(userMnemonic), backupSymmetricKey).then((encryptedMnemonic) => {
            this._ApiProviderService.postUserKeyBackup([{
              [SecureStorageKey.userMnemonic]: encryptedMnemonic
            }]).then(() => resolve()).catch((e) => reject(e))
          }).catch(e => reject(e))
        }).catch(e => reject(e))
      }).catch(e => reject(e))
    })
  }

  public sendUserPublicEthereumAddress(): Promise<void> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.userPublicKey, false).then((userPublicKey) => {
        if(userPublicKey) {
          this._ApiProviderService.saveUserEthereumAddress(userPublicKey).then(() => resolve()).catch((e) => reject(e));
        } else {
          resolve();
        }
      }).catch(e => reject(e))
    })
  }

  public restoreUserChatKeyBackup(): Promise<void> {
    return new Promise((resolve, reject) => {
      var cc = {}
      this._ApiProviderService.getUserKeyBackup().then(response => {
        if(response.length > 0) {

          var checkIfUserMnemonicExists = Array.from(response, rk => Object.keys(rk)[0]).includes(SecureStorageKey.userMnemonic);

          if(!checkIfUserMnemonicExists) {
            this._CryptoProviderService.generateUserKeyPair().then(() => {
              this.sendUserMnemonic().then(() => {
                this.sendUserPublicEthereumAddress();
              });
            })
          }

          response.forEach(r => {
            var key = Object.keys(r)[0];

            this._CryptoProviderService.returnBackupSymmetricKey().then(backupSymmetricKey => {
              if(key != SecureStorageKey.userMnemonic) {
                var val = JSON.parse(r[key]);
                this._CryptoProviderService.symmetricDecrypt(val.encryptedPrivateKey, backupSymmetricKey).then(privateKey => {
                  cc = Object.assign(cc, {
                    [key] : {publicKey: val.publicKey,
                    privateKey: privateKey,
                    targetPublicKey: val.targetPublicKey}
                  })
                  if(response.indexOf(r) == response.length - 1) {
                    this._SecureStorageService.setValue(SecureStorageKey.chatKeyPairs, JSON.stringify(cc)).then(() => {
                      resolve();
                    }).catch(e => reject(e))
                  }
                }).catch(e => reject(e))
              } else {
                var val = r[key];
                this._CryptoProviderService.symmetricDecrypt(val, backupSymmetricKey).then(um => {
                  var userMnemonic = JSON.parse(um);
                  const wallet = ethers.Wallet.fromMnemonic(userMnemonic);
                  // try {
                  //   this.connectToNetwork(wallet);
                  // } catch(ee) {
                  //     console.log(ee);
                  // }
                  wallet.getAddress().then(userPublicKey => {
                    const userPrivateKey = wallet.privateKey;
                    try {
                        this._SecureStorageService.setValue(SecureStorageKey.userPublicKey, userPublicKey).then(() => {
                          this._SecureStorageService.setValue(SecureStorageKey.userPrivateKey, userPrivateKey).then(() => {
                            this._SecureStorageService.setValue(SecureStorageKey.userMnemonic, userMnemonic).then(() => {
                              if(response.indexOf(r) == response.length - 1) {
                                this._SecureStorageService.setValue(SecureStorageKey.chatKeyPairs, JSON.stringify(cc)).then(() => {
                                  resolve();
                                }).catch(e => reject(e))
                              }
                            })
                          })
                        });
                    } catch (e) {
                        throw new Error('There was an error storing the device key pair');
                    }
                  });
                }).catch(e => {
                  reject(e);
                })
              }
            }).catch(e => reject(e))
          })
        }
        else {
          this._CryptoProviderService.generateUserKeyPair().then(() => {
            this.sendUserMnemonic().then(() => {
              this.sendUserPublicEthereumAddress().then(() => {
                resolve();
              })
            });
          })
        }
      }).catch(e => reject(e))
    })
}

public storeUserChatKeyBackup(cc: any): Promise<void> {
  var ddd = []
  return new Promise((resolve, reject) => {
    var keys = Object.keys(cc);
    keys.forEach(k => {
      this._CryptoProviderService.returnBackupSymmetricKey().then(backupSymmetricKey => {
        this._CryptoProviderService.symmetricEncrypt(cc[k].privateKey, backupSymmetricKey).then(encryptedPrivateKey => {
          ddd.push({ [k] : JSON.stringify({
            publicKey: cc[k].publicKey,
            encryptedPrivateKey: encryptedPrivateKey,
            targetPublicKey: cc[k].targetPublicKey
          })})
          if(keys.indexOf(k) == keys.length - 1) {
            this._ApiProviderService.postUserKeyBackup(ddd).then(() => resolve()).catch((e) => reject(e))
          }
        }).catch(e => reject(e))
      }).catch(e => reject(e))
    })
  })
}

  generateNewThreadSpecificChatKeyPair(chatUsername: string, optionalPublicKey: any, threadChatKeyPairs: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.didCommService.generateThreadSpecificChatKeyPair().then(keys => {
        this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false).then(c => {
          var cc = !!c ? JSON.parse(c) : {}
          var newChatKeyPair: ChatKeyPairs = {
            [chatUsername]: {
              publicKey: !!cc[chatUsername] ? cc[chatUsername].publicKey : keys.publicKey,
              privateKey: !!cc[chatUsername] ? cc[chatUsername].privateKey : keys.privateKey,
              targetPublicKey: !optionalPublicKey ? null : optionalPublicKey
            }
           }
          if(!cc[chatUsername]){
            cc = Object.assign(cc, newChatKeyPair)
          } else {
            if(!cc[chatUsername].targetPublicKey) {
              cc[chatUsername].targetPublicKey = optionalPublicKey
            }
          }
          this.updateThreadChatKeyPairs(cc, threadChatKeyPairs).then((c) => {
            resolve(cc)
          }).catch(e => reject(e))
        }).catch(e => reject(e))
      }).catch(e => reject(e))
    })
  }

  updateThreadPublicKeyFromOtherUser(chatUsername: string, targetPublicKey: string, threadChatKeyPairs: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this._SecureStorageService.getValue(SecureStorageKey.chatKeyPairs, false).then(c => {
        var cc = !!c ? JSON.parse(c) : {}
        if(!!cc[chatUsername]) {
          cc[chatUsername].targetPublicKey = targetPublicKey
        }
        this.updateThreadChatKeyPairs(cc, threadChatKeyPairs)
          .then((c) => { resolve(cc) })
          .catch(e => reject(e))
        // resolve()
      }).catch(e => reject(e))
    })
  }

}
