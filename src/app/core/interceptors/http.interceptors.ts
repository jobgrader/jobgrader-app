import { HttpContextToken, HttpErrorResponse, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import * as SentryAngular from "@sentry/angular-ivy";
import { Observable, catchError, from, map, mergeMap, switchMap, throwError } from 'rxjs';


// custom imports
import { generateBlueTokens, symmetricDecrypt } from "@utilities/crypto";
import { environment } from '../../../environments/environment';
import { FirebaseAuthService } from '../providers/firebase-auth/firebase-auth.service';


// Create a context token for the decryption key
const DECRYPTION_KEY = new HttpContextToken<string>(() => '');

/**
 * HTTP interceptor that currently
 * only shows error toasts in case
 * an http request failed
 */
@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    private firebaseAuthService = inject(FirebaseAuthService);

    constructor() { }

    /**
     * Request and response interceptor, to do
     * add some loading queue here
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { headers }: { headers: HttpHeaders } = request;

        if (headers.has('Authorization') || request.url.includes('.amazonaws.com/') || request.url.startsWith(environment.firestore.endpoints.checkReferralCodeValidity)
            || request.url.startsWith('https://deep-index.moralis.io/api/v2')
            || request.url.startsWith('https://api.dropboxapi.com/2/users/get_current_account')
        ) {

            return next.handle(request)
                .pipe(
                    catchError(error => this.handleError(error, request, next)),
                );

        } else {

            return from(this.firebaseAuthService.token())
                .pipe(
                    mergeMap((token: string) => {
                        let newHeaders = request.headers;
                        if (token && !this.whiteList.filter(w => request.url.indexOf(w) !== -1).length) {
                            newHeaders = newHeaders.set('Authorization', `Bearer ${token}`);
                        }

                        if (this.apiKeyList.filter(w => request.url === w).length > 0) {

                            return from(generateBlueTokens()).pipe(
                                mergeMap((xApiKey) => {
                                    newHeaders = newHeaders.set('X-Api-Key', xApiKey.encrypted);
                                    request = request.clone({ headers: newHeaders, context: request.context.set(DECRYPTION_KEY, xApiKey.key) });

                                    return next.handle(request)
                                        .pipe(
                                            catchError(error => this.handleError(error, request, next)),
                                            map(event => this.handleResponse(event, request))
                                        );
                                })
                            )
                        } else {
                            // IMPORTANT: for now we have decided to not add withCredentials flag
                            // if this is needed we have to add this just for this service
                            // to add it always is something we should avoid
                            // request = request.clone({headers: newHeaders, withCredentials: true});
                            request = request.clone({ headers: newHeaders });
                            return next.handle(request)
                                .pipe(
                                    catchError(error => this.handleError(error, request, next)),
                                    map(event => this.handleResponse(event, request))
                                );
                        }
                    })
                );
        }
    }

    private get whiteList(): string[] {
        return [
            'hmt.ai',
            '/user/validate',
            '/api/v3b/user/create',
            '/user/checkusername',
            '/api/v1/user/usernamereminder',
            '/api/v1/user/checkemail',
            '/api/v1/user/login',
            '/api/v1/user/mailvalidation/otp',
            '/api/v1/legals',
            '/helix/version',
            '/helix/test/dataaccesswebhook',
            '/api/v1/app-version',
            '/helix/publickey',
            '/api/v1/user/chat/',
            '/api/v1/user/resetpassword',
            '/api/v1/helixidethaddress',
            '/webhook/linkedin',
        ];
    }

    private get dontUseCredentialsFlagList(): string[] {
        return [
            'obtainusergroups',
            'obtainuserdata',
            'manageuserentry',
            'fetchreferralprograms',
            'generatereferralcode'
        ];
    }

    private get apiKeyList(): string[] {
        return [
            `${environment.apiUrl}/user`
        ]
    }

    private handleResponse(event: HttpEvent<any>, request: HttpRequest<any>): HttpEvent<any> {

        if (event instanceof HttpResponse && request.context.has(DECRYPTION_KEY)) {
            const decryptionKey = request.context.get(DECRYPTION_KEY);
            const modifiedBody = this.decryptResponse(event.body, decryptionKey);
            return event.clone({ body: modifiedBody });
        }

        return event;
    }

    private handleError(error: HttpErrorResponse, request: HttpRequest<any>, next: HttpHandler, handle401 = true): Observable<HttpEvent<any>> {
        if (error.status === 401 && request.headers.has('Authorization') && handle401) {

            return this.handle401(request, next);

        } else if (error.status === 403 && request.headers.has('X-Api-Key') && error.error === 'BAD X-API-KEY') {

            SentryAngular.captureException(new Error(error.error), {
                extra: {
                    url: request.url,
                    method: request.method,
                    status: error.status,
                    error: error.message
                }
            });

            return this.handleBadXApiKey(request, next);

        } else {

            return throwError(() => error);

        }
    }

    private handle401(request: HttpRequest<any>, next: HttpHandler) {
        return from(this.firebaseAuthService.refreshToken())
            .pipe(
                switchMap(token => {
                    request = request.clone({
                        headers: request.headers.set(
                            'Authorization',
                            `Bearer ${token}`
                        ),
                    });
                    return next.handle(request)
                        .pipe(
                            catchError(error => this.handleError(error, request, next, false)),
                        );
                })
            );
    }

    private handleBadXApiKey(request: HttpRequest<any>, next: HttpHandler) {
        return from(generateBlueTokens())
            .pipe(
                switchMap((xApiKey) => {
                    let newHeaders = request.headers;
                    newHeaders = newHeaders.set('X-Api-Key', xApiKey.encrypted);

                    request = request.clone({ headers: newHeaders, context: request.context.set(DECRYPTION_KEY, xApiKey.key) });

                    return next.handle(request)
                })
            );
    }


    private decryptResponse(body: any, key: string): any {
        return symmetricDecrypt(body, key);
    }

}
