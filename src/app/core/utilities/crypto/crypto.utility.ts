import { encrypt } from '@toruslabs/eccrypto';
import { createDecipheriv } from 'crypto';
import * as CryptoJS from 'crypto-js';

// custom imports
import { environment } from '../../../../environments/environment';


export function generateBlueTokens(): Promise<{ key: string; encrypted: string; }> {
    return new Promise((resolve) => {
        const r = CryptoJS.lib.WordArray.random(32).toString();
        const rm = new Uint8Array(r.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
        const m = environment.blue;
        const mm = Buffer.from(m, 'hex');
        encrypt(mm, Buffer.from(rm)).then(encrypted => {
            const encryptedUnion = Buffer.concat([ encrypted.iv, encrypted.ephemPublicKey, encrypted.ciphertext, encrypted.mac ]);
            const header = base64Tobase64url(uInt8ArrayToBase64(encryptedUnion));
            resolve({
                key: r,
                encrypted: header
            })
        });
    })
}

export function symmetricDecrypt(text: string, symmetricKey: string) {
    const hexwith0C = base64ToHex(base64urlTobase64(text));
    const byteArrayResult = hex2i(hexwith0C.slice(2));

    const byteArrayObject = {
        iv: byteArrayResult.slice(0, 12),
        ciphertext: byteArrayResult.slice(12, byteArrayResult.length - 16),
        authTag: byteArrayResult.slice(byteArrayResult.length - 16)
    };
    const byteArrayObjectHex = {
        iv: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.iv)), 'hex'),
        ciphertext: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.ciphertext)), 'hex'),
        authTag: Buffer.from(base64ToHex(uInt8ArrayToBase64(byteArrayObject.authTag)), 'hex')
    };
    const key = Buffer.from(symmetricKey, 'hex');
    const decipher = createDecipheriv('aes-256-gcm', key, byteArrayObjectHex.iv);
    decipher.setAuthTag(byteArrayObjectHex.authTag);
    let decrypted = decipher.update(byteArrayObjectHex.ciphertext, undefined, 'utf8');
    decrypted += decipher.final();
    return decrypted;
}

function base64Tobase64url(input) {
    if(input){
        input = input
        .replace(/\+/g, '-')
        .replace(/\//g, '_');
    }
    return input;
}

function uInt8ArrayToBase64(bytes: Uint8Array): string {
    return Buffer.from(bytes).toString('base64');
}

function base64ToHex(str) {
    const raw = atob(str);
    let result = '';
    for (let i = 0; i < raw.length; i++) {
      const hex = raw.charCodeAt(i).toString(16);
      result += (hex.length === 2 ? hex : '0' + hex);
    }
    return result.toUpperCase();
}

function base64urlTobase64(input) {
    if (input) {
      input = input.replace(/-/g, '+').replace(/_/g, '/');
      const pad = input.length % 4;
      if (pad) {
        if (pad === 1) {
          throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
        }
        input += new Array(5 - pad).join('=');
      }
    }
    return input;
}

function i2hex(i) {
    return ('0' + i.toString(16)).slice(-2);
}

function hex2i(hexString) {
    return new Uint8Array(hexString.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
}
