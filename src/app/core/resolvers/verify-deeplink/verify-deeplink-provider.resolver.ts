import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';

import { UserProviderService } from '../../providers/user/user-provider.service';
import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { LoaderProviderService } from '../../providers/loader/loader-provider.service';
import { NavController } from '@ionic/angular';


export const VerifyDeeplinkProviderResolver: ResolveFn<any> =
  async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {

    const nav = inject(NavController);
    const deeplinkProviderService = inject(DeeplinkProviderService);
    const authenticationProviderService = inject(AuthenticationProviderService);
    const userProviderService = inject(UserProviderService);
    const loaderProviderService = inject(LoaderProviderService);

    if (deeplinkProviderService.verificationDeeplinkStringValue) {
      await loaderProviderService.loaderCreate();

      const user = await  userProviderService.getUser();
      const obj = {
            byUsrId: '',
            byLink: '',
        };

      obj.byUsrId = await deeplinkProviderService.verifyByUserId(
          user.id
        );

      obj.byLink = await deeplinkProviderService.verify(
            deeplinkProviderService.verificationDeeplinkStringValue
        );

      const verifiedDeeplinkPayloadResponse = obj;

      if (verifiedDeeplinkPayloadResponse) {
        await loaderProviderService.loaderDismiss();

        return verifiedDeeplinkPayloadResponse;
      }
    }

    await loaderProviderService.loaderDismiss();
    nav.navigateRoot('/dashboard');

    return null;
  }
