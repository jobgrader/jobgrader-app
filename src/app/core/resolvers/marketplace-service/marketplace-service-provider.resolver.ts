import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';

import { MarketplaceService } from '../../models/MarketplaceService';


export const MarketplaceServiceProviderResolver: ResolveFn<any> =
  async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {

    const items: MarketplaceService[] = [
      {
        title: {
          en: '',
          de: ''
        },
        slug: 'stuggi-cars',
        name: {
          en: 'car',
          de: 'car'
        },
        icon: '../../../../assets/icon/marketplace/stuggi-cars-hq.png',
      }
    ];

    const { slug } = route.params;
    const item: MarketplaceService = items.find(i => i.slug === slug);
    if (item) {
      return item;
    }

    return {};
  }
