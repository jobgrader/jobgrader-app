import { TestBed, inject } from '@angular/core/testing';

import { MarketplaceServiceProviderResolver } from './marketplace-service-provider.resolver';

describe('MarketplaceServiceProviderResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [MarketplaceServiceProviderResolver],
  }));

  it('should be created', inject([MarketplaceServiceProviderResolver], (resolver: typeof MarketplaceServiceProviderResolver) => {
    expect(resolver).toBeTruthy();
  }));
});
