import { TestBed, inject } from '@angular/core/testing';

import { UserProviderResolver } from './user-provider.resolver';

describe('UserProviderResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [UserProviderResolver],
  }));

  it('should be created', inject([UserProviderResolver], (resolver: typeof UserProviderResolver) => {
    expect(resolver).toBeTruthy();
  }));
});
