import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot } from '@angular/router';

import { User } from '../../models/User';
import { UserProviderService } from '../../providers/user/user-provider.service';


export const UserProviderResolver: ResolveFn<any> =
  async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {

    const userProviderService = inject(UserProviderService);

    const user: User = await userProviderService.getUser();
    if (user) {
      return user;
    }

    return {};
  }
