import { TestBed, inject } from '@angular/core/testing';

import { ClaimDeeplinkProviderResolver } from './claim-deeplink-provider.resolver';

describe('ClaimDeeplinkProviderResolver', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [ClaimDeeplinkProviderResolver],
  }));

  it('should be created', inject([ClaimDeeplinkProviderResolver], (resolver: typeof ClaimDeeplinkProviderResolver) => {
    expect(resolver).toBeTruthy();
  }));
});
