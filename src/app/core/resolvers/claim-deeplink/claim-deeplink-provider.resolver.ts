import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';

import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { LoaderProviderService } from '../../providers/loader/loader-provider.service';
import { NavController } from '@ionic/angular';


export function ClaimDeeplinkProviderResolver(): CanActivateFn {

  return async () => {
      const nav = inject(NavController);
      const deeplinkProviderService = inject(DeeplinkProviderService);
      const authenticationProviderService = inject(AuthenticationProviderService);
      const loaderProviderService = inject(LoaderProviderService);

      if (deeplinkProviderService.verificationDeeplinkStringValue) {
        await loaderProviderService.loaderCreate();

        const claimedDeeplinkPayloadResponse =
          await deeplinkProviderService.claim(
            deeplinkProviderService.verificationDeeplinkStringValue
          );

        if (claimedDeeplinkPayloadResponse) {
          await loaderProviderService.loaderDismiss();

          return claimedDeeplinkPayloadResponse;
        }
      }

      await loaderProviderService.loaderDismiss();
      nav.navigateRoot('/dashboard');

      return null;
    }

}
