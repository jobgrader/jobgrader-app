import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';

import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { NavController } from '@ionic/angular';


export const VerificationDeeplinkAvailabilityProviderGuard: CanActivateFn =

  (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const nav = inject(NavController);
    const deeplinkProviderService = inject(DeeplinkProviderService);

    if (deeplinkProviderService.verificationDeeplinkStringValue) {
      return true;
    }

    nav.navigateRoot('/dashboard');

    return false;
  }
