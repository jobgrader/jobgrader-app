import { TestBed, inject } from '@angular/core/testing';

import { SignProviderGuard } from './sign-provider.guard';

describe('SignProviderGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [SignProviderGuard],
  }));

  it('should be created', inject([SignProviderGuard], (guard: typeof SignProviderGuard) => {
    expect(guard).toBeTruthy();
  }));
});
