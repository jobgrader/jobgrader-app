import { inject } from '@angular/core';
import { RouterStateSnapshot, CanActivateFn, ActivatedRouteSnapshot } from '@angular/router';

import { SignProviderService } from '../../providers/sign/sign-provider.service';
import { SignUp, PersonalInformation } from '../../../core/models/SignUp';

export const ResetPasswordProviderGuard: CanActivateFn =
  async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const signService = inject(SignProviderService);

    const signUpDataFromStorage: SignUp = await signService.getSignData();
    const personalInformationDataFromStorage: PersonalInformation = await signService.getPersonalData();
    const url = state.url;

    return checkActivateAccess(url, signUpDataFromStorage, personalInformationDataFromStorage);
  }

function checkActivateAccess(
  url: string,
  signUpData: SignUp,
  personalInformationData: PersonalInformation
): boolean {
  return true;
}
