import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';

import { DeeplinkProviderService } from '../../providers/deeplink/deeplink-provider.service';
import { NavController } from '@ionic/angular';


export const SignatureDeeplinkAvailabilityProviderGuard: CanActivateFn =
  (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const nav = inject(NavController);
    const deeplinkProviderService = inject(DeeplinkProviderService);

    if (deeplinkProviderService.signatureDeeplinkStringValue) {
      return true;
    }

    nav.navigateRoot('/dashboard');

    return false;
  }
