import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, RouterStateSnapshot } from '@angular/router';
import { AppStateService } from '../../providers/app-state/app-state.service';
import { NavController } from '@ionic/angular';
import { AuthenticationProviderService } from '../../providers/authentication/authentication-provider.service';
import { UserProviderService } from '../../providers/user/user-provider.service';


export const AuthenticationGuard: CanActivateFn =
  async (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    const nav = inject(NavController);
    const appStateService = inject(AppStateService);
    const authenticationProviderService = inject(AuthenticationProviderService);
    const userProviderService = inject(UserProviderService);

    // console.log('*** authenticating on: ' + window.location.href)

    // We don't need to parse the user each time, just check we've got one in storage.
    var unParsedUser = await userProviderService.getUserNonParsed();

    if (unParsedUser) {
      appStateService.isAuthorized = true;

      // if (!appStateService.basicAuthToken) {
      //   // Only wait for this promise to resolve if we haven't already set an app token on this app load cycle.
      //   var authToken = await authenticationProviderService.getBasicAuthToken();
      //   appStateService.basicAuthToken = authToken;
      // }

      return true;
    }
    else {
      nav.navigateRoot('/login');
    }
    return false;
  }

