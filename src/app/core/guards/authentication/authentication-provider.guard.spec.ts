import { TestBed, inject } from '@angular/core/testing';

import { AuthenticationGuard } from './authentication-guard.service';

describe('AuthenticationProviderGuard', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [AuthenticationGuard],
  }));

  it('should be created', inject([AuthenticationGuard], (guard: typeof AuthenticationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
