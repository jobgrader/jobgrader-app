export interface User {
  id?: any;
  userid?: any;
  callname?: any;
  confirmed?: boolean;
  lastModified?: any;
  photo?: any; // not existed
  gender?: any; // data.gender.value
  title?: any; // data.title.value
  firstname?: any; // data.firstname.value
  middlename?: any; // data.middlename.value
  lastname?: any; // data.lastname.value
  maritalstatus?: any; // data.maritalstatus.value
  maritalstatusStatus?: any; // data.maritalstatus.value
  emailtype?: any; // data.emailtype.value
  email?: any; // data.email.value
  phonetype?: any; // data.phonetype.value
  phone?: any; // data.phone.value
  dateofbirth?: any; // Date from data.dateofbirth.value
  cityofbirth?: any; // data.cityofbirth.value
  countryofbirth?: any; // data.countryofbirth.value
  citizenship?: any; // data.citizenship.value
  city?: any; // data.city.value
  zip?: any; // data.zip.value
  state?: any; // data.state.value
  country?: any; // data.country.value
  identificationdocumenttype?: any; // data.identificationdocumenttype.value
  identificationdocumentnumber?: any; // data.identificationdocumentnumber.value
  identificationissuecountry?: any;
  identificationexpirydate?: any;
  identificationissuedate?: any;
  driverlicencedocumentnumber?: any; // data.driverlicencedocumentnumber.value
  driverlicencecountry?: any; // data.driverlicencecountry.value
  driverlicenceexpirydate?: any;
  driverlicenceissuedate?: any;
  passportnumber?: any; // data.driverlicencedocumentnumber.value
  passportissuecountry?: any; // data.driverlicencecountry.value
  passportexpirydate?: any;
  passportissuedate?: any;
  residencepermitnumber?: any; // data.driverlicencedocumentnumber.value
  residencepermitissuecountry?: any; // data.driverlicencecountry.value
  residencepermitexpirydate?: any;
  residencepermitissuedate?: any;
  taxresidency?: any; // data.taxresidency.value
  taxnumber?: any; // data.taxnumber.value
  termsofuse?: any; // data.termsofuse.value
  termsofusethirdparties?: any; // data.termsofusethirdparties.value
  maidenname?: any; // data.maidenname.value
  basicdataId?: any; // basicdata
  revision?: any; // basicdata
  idData?: any; // basicdata
  firstnameStatus?: any;
  lastnameStatus?: any;
  emailStatus?: any;
  phoneStatus?: any;
  dateofbirthStatus?: any;
  countryofbirthStatus?: any;
  cityStatus?: any;
  identityId?: any;
  // Other missing fields
  zipStatus?: any;
  streetStatus?: any;
  street?: any;
  identificationdocumentnumberStatus?: any;
  identificationissuecountryStatus?: any;
  identificationexpirydateStatus?: any;
  identificationissuedateStatus?: any;
  driverlicencedocumentnumberStatus?: any;
  driverlicencecountryStatus?: any;
  driverlicenceexpirydateStatus?: any;
  driverlicenceissuedateStatus?: any;
  passportnumberStatus?: any; // data.driverlicencedocumentnumber.value
  passportissuecountryStatus?: any; // data.driverlicencecountry.value
  passportexpirydateStatus?: any;
  passportissuedateStatus?: any;
  residencepermitnumberStatus?: any; // data.driverlicencedocumentnumber.value
  residencepermitissuecountryStatus?: any; // data.driverlicencecountry.value
  residencepermitexpirydateStatus?: any;
  residencepermitissuedateStatus?: any;
  citizenshipStatus?: any;
  countryStatus?: any;
  genderStatus?: any;
  stateStatus?: any;
  maidennameStatus?: any;
  middlenameStatus?: any;
  cityofbirthStatus?: any;
}
