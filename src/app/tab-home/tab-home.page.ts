import { Component } from "@angular/core";
// import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { User } from "../core/models/User";
import { LoaderProviderService } from "../core/providers/loader/loader-provider.service";
import { ApiProviderService } from "../core/providers/api/api-provider.service";
import { AppStateService } from "../core/providers/app-state/app-state.service";
import {
  ToastController,
  NavController,
  ModalController,
  AlertController,
} from "@ionic/angular";
import { LockService } from "../lock-screen/lock.service";
import { SecureStorageService } from "../core/providers/secure-storage/secure-storage.service";
import { SecureStorageKey } from "../core/providers/secure-storage/secure-storage-key.enum";
import { UserProviderService } from "../core/providers/user/user-provider.service";
// import { ChatService } from "../core/providers/chat/chat.service";
import { MarketplaceService } from "../core/providers/marketplace/marketplace.service";
import { SQLStorageService } from "../core/providers/sql/sqlStorage.service";
import { DocumentModalPageComponent } from "../legal-documents/document-modal/document-modal.component";
import { NetworkService } from "../core/providers/network/network-service";
import { ImageSelectionService } from "../shared/components/image-selection/image-selection.service";
import { BarcodeService } from "../core/providers/barcode/barcode.service";
import { AuthenticationProviderService } from "../core/providers/authentication/authentication-provider.service";
import { PushService } from "../core/providers/push/push.service";
import { KycService } from "../kyc/services/kyc.service";
// import { VadeService } from '../core/providers/vade/vade.service';
// import { HumanService } from '../core/providers/human/human.service';
import { ThemeSwitcherService } from "../core/providers/theme/theme-switcher.service";
import { uniq } from 'lodash-es';
import { Capacitor } from "@capacitor/core";
import { Device } from "@capacitor/device";
import { Platform } from "@ionic/angular";
import { KycProvider } from "../kyc/enums/kyc-provider.enum";
import { KycState } from "../kyc/enums/kyc-state.enum";
import { Subscription, interval } from "rxjs";
import { DatePipe } from "@angular/common";
import { UserPhotoServiceAkita } from "../core/providers/state/user-photo/user-photo.service";
import { BroadcastService } from "../core/providers/broadcast/broadcast.service";
import { BroadcastsComponent } from "./broadcasts/broadcasts.component";
import { NftService } from "../core/providers/nft/nft.service";
import { SocialMediaService } from "../core/providers/social-media/social-media.service";
import { FirestoreCloudFunctionsService } from "@services/firestore-cloud-functions/firestore-cloud-functions.service";
// import { NftService } from "../core/providers/nft/nft.service";

declare var window: any;

export interface ProductCategory {
  key: string;
  translation: string;
}

export enum ShortcutType {
  WALLETDETAIL = 'WALLETDETAIL',
  WALLETNFT = 'WALLETNFT',
  CELEBRITY = 'CELEBRITY',
  CONTACT = 'CONTACT',
  TICKET = 'TICKET',
  COVIDVACCINATION = 'COVIDVACCINATION',
  COVIDTEST = 'COVIDTEST',
  COVIDRECOVERY = 'COVIDRECOVERY',
}

const VARIABLE_SHORTCUTS = 4;
const zoomTargetMaximumHeight = 844;

@Component({
  selector: "app-tab-home",
  templateUrl: "./tab-home.page.html",
  styleUrls: ["./tab-home.page.scss"],
})
export class TabHomePage {

    public profilePictureSrc = '../../assets/job-user.svg';
    public placeholderImage = '../../assets/job-user.svg';
    public lastActionsAvailable = false;
    public user: User;
    public appleDesignQuickFix = false;
    public androidDesignQuickFix = true;
    public displayVerifiedTick = false;
    public viewCategoryText = false;

  public tapToInitiate = true;
  public tapToActivate = true;

  public loyaltyPoints = 0;
  powerUserMode = false;

  // Default enabled categories, regardless of API response.
  public availableCategories: string[] = ["trustedPartner"];
  keys;
  public dashBoardData = {
    available: '0',
    estimated: '0',
    recent: '0',
    total: '0',
    served: '0',
    solved: '0',
    verified: '0'
  }

  oldJobNumbers;
  newJobNumbers;
  oldPayment;
  newPayment;
  percentageJobNumbers;
  percentagePayment;

  timestampObserver = interval(1000);
  timestampSubscription: Subscription;
  displayTime = '...';
  datePipe = new DatePipe('en-US');
  displayAlert = false;

  public shortcuts = [
    // {
    //     title: 'Vac-Cert',
    //     description: 'Vaccination',
    //     icon: '../../assets/icon/health.svg',
    //     link: 'https://www.google.com'
    // },
    // {
    //     title: 'Test-Cert',
    //     description: 'Test',
    //     icon: '../../assets/icon/health.svg',
    //     link: 'https://www.youtube.com'
    // },
    // {
    //     title: 'Rec-Cert',
    //     description: 'Recovery',
    //     icon: '../../assets/icon/health.svg',
    //     link: 'https://www.reddit.com'
    // },
    // {
    //     title: 'Dummy1',
    //     description: 'Dummy',
    //     icon: '../../assets/icon/health.svg',
    //     link: 'https://www.gmail.com'
    // },
    // {
    //     title: 'Dummy2',
    //     description: 'Dummy',
    //     icon: '../../assets/icon/health.svg',
    //     link: 'https://www.office.com'
    // }
  ];
  alertButtons: { text: any; handler: () => void; }[];

  // public emptyArray = new Array(5-this.shortcuts.length);

  constructor(
    public translateService: TranslateService,
    // private router: Router,
    private loaderProviderService: LoaderProviderService,
    private apiProviderService: ApiProviderService,
    private appStateService: AppStateService,
    private toastController: ToastController,
    private lockService: LockService,
    private secureStorage: SecureStorageService,
    private userProviderService: UserProviderService,
    private _MarketplaceService: MarketplaceService,
    // activatedRoute: ActivatedRoute,
    private nav: NavController,
    public _SQLStorageService: SQLStorageService,
    public _SecureStorageService: SecureStorageService,
    public _ModalController: ModalController,
    private _ImageSelectionService: ImageSelectionService,
    private _NetworkService: NetworkService,
    private _Push: PushService,
    private _AuthenticationProviderService: AuthenticationProviderService,
    private barcode: BarcodeService,
    private _KycService: KycService,
    // private _Human: HumanService,
    private _Theme: ThemeSwitcherService,
    // private _NftService: NftService,
    // private _ChatService: ChatService,
    private _AlertController: AlertController,
    // private _VadeService: VadeService,
    private _Platform: Platform,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private _NftService: NftService,
    public _Broadcast: BroadcastService,
    private firecloud: FirestoreCloudFunctionsService,
    private _SocialMediaService: SocialMediaService
  ) {
  }

  /** @inheritDoc */
  public async ionViewDidEnter() {
    this.appleDesignQuickFixMethod();
    this.androidDesignQuickFixMethod();


    this.lockService.restart();
    this.placeholderImage = '../../assets/job-user.svg'; // : '../../assets/U-unAuthorized.svg';
    this.profilePictureSrc = this.placeholderImage;

    if(this.appStateService.basicAuthToken) {
      var photo = this.userPhotoServiceAkita.getPhoto();

      if (photo) {
        this.profilePictureSrc = photo;
      }
     }

    const powerUserMode = await this.secureStorage.getValue(SecureStorageKey.powerUserMode, false);
    this.powerUserMode = (powerUserMode == "true");

    const viewCategoryText = await this.secureStorage.getValue(SecureStorageKey.viewCategoryText, false);
    this.viewCategoryText = (viewCategoryText == true.toString());

    // var browseMode = await this.secureStorage.getValue(SecureStorageKey.browseMode, false);
    // console.log("browseMode", browseMode);

    if(!this.appStateService.basicAuthToken) {
      this.apiProviderService.noUserLoggedInAlert();
      // this._AuthenticationProviderService.displayLoginOptionIfNecessary();
    }

    if(!this._NetworkService.checkConnection()) {
        console.log('Offline Mode');
        this.loaderProviderService.loaderDismiss();
        this._NetworkService.addOfflineFooter('ion-footer');
        this._SQLStorageService.getMarketPlaceMerchants().then(() => {
            // console.log(this._MarketplaceService.merchants);
            this.setAvailableCategories();
        });
        this.toastController.create({
            message: this.translateService.instant('NETWORK.checkConnection'),
            duration: 3000,
            position: 'top',
        }).then(toast => {
            toast.present();
        });
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
        this.loadMarketplace();
        this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false).then((address) => {
          if(address) {
            this._NftService.returnTHXCBalance(address).then((thxcBalance) => {
              this._SecureStorageService.getValue(SecureStorageKey.cachedEarnings, false).then(async cachedEarningsString => {
                const cachedEarnings = !!cachedEarningsString ? JSON.parse(cachedEarningsString) : { "THXC" : 0 };
                this.loyaltyPoints = Number(thxcBalance) + cachedEarnings["THXC"];
                const taskList = await this._SocialMediaService.fetchSocialMediaTasks();
                const myUserStatus = await this._SocialMediaService.fetchSocialMediaUserStatus();

                const tasksCompleted = taskList.filter((k: any) => !!myUserStatus.find(us => us.taskId == k.taskId));
                const earnings = Array.from(tasksCompleted, (tc: any) => !!tc.earnings ? tc.earnings : 0);
                const add = (a,b) => { return a+b }
                cachedEarnings["THXC"] = earnings.reduce(add, 0);
                this.loyaltyPoints = Number(thxcBalance) + cachedEarnings["THXC"];
                this._SecureStorageService.setValue(SecureStorageKey.cachedEarnings, JSON.stringify(cachedEarnings));
              })
            });
          }
        })

        this.keys = Object.keys(this.dashBoardData);
        this._SecureStorageService.getValue(SecureStorageKey.hCaptchaDashboard, false).then((dashboardString) => {
          this.dashBoardData = !!dashboardString ? JSON.parse(dashboardString) : this.dashBoardData;
          this.oldJobNumbers = (this.dashBoardData.solved == "-") ? '0' : this.dashBoardData.solved;
          this.oldPayment = (this.dashBoardData.total == "-") ? '0' : this.dashBoardData.total.replace("HMT", "").trim();
          if(this.appStateService.basicAuthToken) {
            this.apiProviderService.ObtainhCaptchaUserDashboardData().then((response) => {
              this.dashBoardData = {
                available: `${!!response.balance.available ? response.balance.available : 0}`,
                estimated: `${!!response.balance.estimated ? response.balance.estimated : 0}`,
                recent: `${!!response.balance.recent ? response.balance.recent : 0}`,
                total: `${!!response.balance.total ? Number(Number(response.balance.total).toFixed(6)) : 0}`,
                served: response.served,
                solved: response.solved,
                verified: response.verified
              }

              this.newJobNumbers = this.dashBoardData.solved;
              this.newPayment = this.dashBoardData.total.replace("HMT", "").trim();

              console.log({
                oldJobNumbers: this.oldJobNumbers,
                newJobNumbers: this.newJobNumbers,
                oldPayment: this.oldPayment,
                newPayment: this.newPayment
              })

              this.percentageJobNumbers = (this.oldJobNumbers == this.newJobNumbers) ? "+ 0.00%" : (this.oldJobNumbers.toString() == "0" ? "-" : `+ ${((this.newJobNumbers / this.oldJobNumbers) - 1).toFixed(2)}%`);
              this.percentagePayment = (this.oldPayment == this.newPayment) ? "+ 0.00%" : (this.oldPayment.toString() == "0" ? "-" : `+ ${((this.newPayment / this.oldPayment) - 1).toFixed(2)}%`);

              console.log({
                percentageJobNumbers: this.percentageJobNumbers,
                percentagePayment: this.percentagePayment
              });

              this._SecureStorageService.setValue(SecureStorageKey.hCaptchaDashboard, JSON.stringify(this.dashBoardData)).then(() => {
                console.log('Done!');
              })
            }).catch((e) => {
              console.log(e);
              this.dashBoardData = {
                available: '0',
                estimated: '0',
                recent: '0',
                total: '0',
                served: '0',
                solved: '0',
                verified: '0'
              }

              this.newJobNumbers = this.dashBoardData.solved;
              this.newPayment = this.dashBoardData.total.replace("HMT", "").trim();

              console.info({
                oldJobNumbers: this.oldJobNumbers,
                newJobNumbers: this.newJobNumbers,
                oldPayment: this.oldPayment,
                newPayment: this.newPayment
              })

              this.percentageJobNumbers = (this.oldJobNumbers == this.newJobNumbers) ? "+ 0.00%" : (this.oldJobNumbers.toString() == "0" ? "-" : `+ ${((this.newJobNumbers / this.oldJobNumbers) - 1).toFixed(2)}%`);
              this.percentagePayment = (this.oldPayment == this.newPayment) ? "+ 0.00%" : (this.oldPayment.toString() == "0" ? "-" : `+ ${((this.newPayment / this.oldPayment) - 1).toFixed(2)}%`);

              console.info({
                percentageJobNumbers: this.percentageJobNumbers,
                percentagePayment: this.percentagePayment
              });
            })
          }

        })

        if(this.appStateService.basicAuthToken) {

          this.firecloud.updateFirebaseToken().then(r => {
            console.log(`Firebase cloud functions updated in firestore`);
          }).catch(e => {
            console.log(e);
          });

          this.secureStorage.getValue(SecureStorageKey.firebase, false).then(token => {
            if(token) {
                this.secureStorage.getValue(SecureStorageKey.deviceInfo, false).then(deviceId => {
                    if(this.appStateService.isAuthorized) {
                        if(deviceId && deviceId != "undefined") {
                            this.apiProviderService.updateFirebaseToken(token, deviceId).then(() => {
                                console.log(`Firebase token ${token} updated for the deviceId ${deviceId}`)
                            })
                        }
                    }
                })
            } else {
                this._Push.initPush();
            }
          })
        }


        this.leadershipChart();
        this._Broadcast.init();

        // if(browseMode != "true") {
        //   if(this.appStateService.basicAuthToken) {
        //     this.userNeedsToDoKyc();
        //   }
        // }

    }


  }

  toggleViewCategoryText() {
    this.viewCategoryText = !this.viewCategoryText;
    this.secureStorage.setValue(SecureStorageKey.viewCategoryText, this.viewCategoryText.toString());
  }


  private async appleDesignQuickFixMethod() {
    let model: string;

    if (this._Platform.is('hybrid') && Capacitor.isPluginAvailable('Device')) {
      model = (await Device.getInfo()).model;
    } else {
      model = window["navigator"].appName + " " + window["navigator"].appCodeName;
    }
    var requiredDeviceArray = [
      "iPhone1,1",
      "iPhone1,2",
      "iPhone2,1",
      "iPhone3,1",
      "iPhone3,2",
      "iPhone3,3",
      "iPhone4,1",
      "iPhone5,1",
      "iPhone5,2",
      "iPhone5,3",
      "iPhone5,4",
      "iPhone6,1",
      "iPhone6,2",
      // "iPhone7,1",
      "iPhone7,2",
      "iPhone8,1",
      // "iPhone8,2",
      "iPhone8,4",
      "iPhone9,1",
      // "iPhone9,2",
      "iPhone9,3",
      // "iPhone9,4",
      // "iPhone10,1",
      // "iPhone10,4",
      "iPod1,1",
      "iPod2,1",
      "iPod3,1",
      "iPod4,1",
      "iPod5,1",
      "iPod7,1",
      "iPod9,1",
      "iPad1,1",
      "iPad1,2",
      "iPad2,1",
      "iPad2,2",
      "iPad2,3",
      "iPad2,4",
      "iPad3,1",
      "iPad3,2",
      "iPad3,3",
      "iPad2,5",
      "iPad2,6",
      "iPad2,7",
      "iPad3,4",
      "iPad3,5",
      "iPad3,6",
      // "iPad4,1",
      // "iPad4,2",
      // "iPad4,3",
      // "iPad4,4",
      // "iPad4,5",
      // "iPad4,6",
      // "iPad4,7",
      // "iPad4,8",
      // "iPad4,9",
      // "iPad5,1",
      // "iPad5,2",
      // "iPad5,3",
      // "iPad5,4",
      // "iPad6,3",
      // "iPad6,4",
      // "iPad6,7",
      // "iPad6,8",
      // "iPad6,11",
      // "iPad6,12",
      // "iPad7,1",
      // "iPad7,2",
      // "iPad7,3",
      // "iPad7,4",
      // "iPad7,5",
      // "iPad7,6",
      // "iPad7,11",
      // "iPad7,12",
      // "iPad8,1",
      // "iPad8,2",
      // "iPad8,3",
      // "iPad8,4",
      // "iPad8,5",
      // "iPad8,6",
      // "iPad8,7",
      // "iPad8,8",
      // "iPad8,9",
      // "iPad8,10",
      // "iPad8,11",
      // "iPad8,12",
      // "iPad11,1",
      // "iPad11,2",
      // "iPad11,3",
      // "iPad11,4",
      // "iPad11,6",
      // "iPad11,7",
      // "iPad12,1",
      // "iPad12,2",
      // "iPad14,1",
      // "iPad14,2",
      // "iPad13,1",
      // "iPad13,2",
      // "iPad13,4",
      // "iPad13,5",
      // "iPad13,6",
      // "iPad13,7",
      // "iPad13,8",
      // "iPad13,9",
      // "iPad13,10",
      // "iPad13,11",
    ];
    this.appleDesignQuickFix = !requiredDeviceArray.includes(model);
    if (!this.appleDesignQuickFix) {
      var greeting = document.getElementsByClassName("greeting");
      if (greeting) {
        if (greeting.length > 0) {
          (greeting[0] as any).style.marginBottom = "10px";
        }
      }
    }
    // AppleMappingTable[]
    // margin-bottom: 10px;
  }

  private androidDesignQuickFixMethod() {
    var deviceHeight = window.innerHeight;
    if (deviceHeight < zoomTargetMaximumHeight) {
      var zoom =
        80 + 20 * ((deviceHeight - 568) / (zoomTargetMaximumHeight - 568));
      console.log(zoom);
      this.androidDesignQuickFix = false;
      var shortcuts = document.getElementsByClassName(
        "feedback-button-container"
      );
      var flower = document.getElementsByClassName("nav");
      if (shortcuts.length > 0) {
        (shortcuts[0] as any).style.zoom = zoom.toString() + "%";
      }
      if (flower.length > 0) {
        (flower[0] as any).style.zoom = zoom.toString() + "%";
      }
    }
  }

  /** Callback for service soon available */
  public async getServiceSoon(): Promise<void> {
    const toast = await this.toastController.create({
      message: this.translateService.instant("HOME.serviceSoonAvailable"),
      duration: 3000,
      position: "top",
    });
    toast.present();
  }

  /** Callback for going to feedback */
  public goToFeedback(): void {
    this.nav.navigateForward("/feedback");
  }


  loadMarketplace() {
    // Let's do last known setup whilst we refresh anyway.
    this.setAvailableCategories();

    this.secureStorage
      .getValue(SecureStorageKey.marketplaceUpdate, false)
      .then((marketplaceUpdate) => {
        var newDate = +new Date();
        // console.log(!marketplaceUpdate);
        // console.log(Number(marketplaceUpdate) + 2 * 60 * 60 * 1000 < newDate);
        var check =
          !marketplaceUpdate ||
          Number(marketplaceUpdate) + 2 * 60 * 60 * 1000 < newDate;
        // console.log(check);
        if (check) {
          this._MarketplaceService.init().then(() => {
            this._SQLStorageService.getMarketPlaceMerchants().then(() => {
              console.log(this._MarketplaceService.merchants);
              this.setAvailableCategories();
            });
          });

          if (this._MarketplaceService.merchants.length == 0) {
            this._MarketplaceService.forcePull().then(() => {
              this.setAvailableCategories();
              this.loaderProviderService.loaderDismiss();
            });
          }

          this.secureStorage
            .setValue(SecureStorageKey.marketplaceUpdate, newDate.toString())
            .then(() => {});
        }
      });
  }

  getReadable(loyaltyPoints) {
    return Number(loyaltyPoints).toLocaleString();
  }

  setAvailableCategories() {
    this.availableCategories = [
      // "mobility",
      // "finance",
      // "event",
      // "trustedPartner",
      // "eCommerce",
      // "health",
      "crypto",
      // "communication",
      // "publicSector",
      // "myCity",
    ];

    // only count visible merchants.

    var mm = this._MarketplaceService.merchants.filter(l => l.displayToggle);
    var bb = [];
    mm.forEach(mmm => {
        mmm.productCategories.forEach(pc => {
            bb.push(pc)
        })
        if(mm.indexOf(mmm) == mm.length - 1) {
            // this.availableCategories = ['trustedPartner'];
            var n = uniq(bb);
            this.availableCategories = this.availableCategories.concat(n);
            this.availableCategories = uniq(this.availableCategories);
            console.log(this.availableCategories);
        }
    })

    this._MarketplaceService.merchants.forEach(m => {
        m.productCategories.forEach(c => {
            if (m.displayToggle) {
                this.availableCategories.push(c);
            }
        });
    });
  }

  isEnabledCategory(key: string) {
    return this.availableCategories.find((c) => {
      return c == key;
    });
  }

  getCategoryClass(key: string) {
    return this.isEnabledCategory(key) ? "" : "disabled-link";
  }

  viewMarketPlace(key: string) {
    if (!this.isEnabledCategory(key)) {
      return;
    }

    this.apiProviderService.ObtainSiteKeys().then(response => {
      console.log('response', response);
      if(response == 'Please use a real email address.'){
        this.presentToast(response);
      } else if(response == '') {
        this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
      } else {
        this.checkHasSeenTutorial(key);
      }
    }).catch(e => {
        console.log('e', e);
        if(e == 'Please use a real email address.') {
          this.presentToast(e);
        } else {
          this.presentToast(this.translateService.instant('SITEKEY_ERROR'));
        }
    });


  }

  presentToast(message: string) {
    this.toastController.create({message, position: 'top', duration: 2000}).then((toast) => { toast.present()});
  }

  checkHasSeenTutorial(key: string) {
    this._SecureStorageService
      .getValue(SecureStorageKey.hasSeenMarketplaceTutorial, false)
      .then((hasSeenMarketplaceTutorial) => {
        if (hasSeenMarketplaceTutorial != "true") {
          this._ModalController
            .create({
              component: DocumentModalPageComponent,
              componentProps: {
                data: {
                  showAsInfoPage: true,
                  img: "../../assets/marketplace-tutorial.svg",
                  title: this.translateService.instant(
                    "MARKETPLACE_TUTORIAL.title"
                  ),
                  body: this.translateService.instant(
                    "MARKETPLACE_TUTORIAL.text1"
                  ),
                  buttonText: this.translateService.instant(
                    "MARKETPLACE_TUTORIAL.buttonText"
                  ),
                },
              },
            })
            .then((modal) => {
              modal.onDidDismiss().then(() => {
                this._SecureStorageService.setValue(
                  SecureStorageKey.hasSeenMarketplaceTutorial,
                  "true"
                );
                this.nav.navigateForward(`/marketplace/${key}`);
              });
              modal.present();
            });
        } else {
          this.nav.navigateForward(`/marketplace/${key}`);
        }
      });
  }

  scanHealthCertificate() {
    if (this.appStateService.isAuthorized) {
      this.barcode.shortcutsPageModification();
    } else {
      this.apiProviderService.noUserLoggedInAlert();
    }

    // .then(() => {
    //     this.populateShortcuts();
    // });
  }

  async displayPictureOptions() {
    if (this.appStateService.isAuthorized) {
      this._ImageSelectionService.showChangePicture().then((photo) => {
        this.profilePictureSrc = !!photo ? photo : this.placeholderImage;
      });
    } else {
      await this.apiProviderService.noUserLoggedInAlert();
    }
  }
  logShortCuts() {
    console.log(this.shortcuts);
  }

  async navigationHandler(type:
    | "hcaptcha"
    | "cvat"
    | "fortune"
    | "audino"
    | "ab"
    | "priceGuessing"
    | "formElements"
    // | "polls"
    | "quiz"
  ) {
    /**
     * TODO: here you can do your own navigation according to the type
     */
    console.log(type);

    var browseMode = await this.secureStorage.getValue(SecureStorageKey.browseMode, false);
    console.log("browseMode", browseMode);

    // if(browseMode != "true") {
    //   if(this.appStateService.basicAuthToken) {
    //     this.userNeedsToDoKyc();
    //     return;
    //   }
    // }


    //   case '-1': this.merchant = Object.assign(videoLabellingExample,
    //   case '-2': this.merchant = Object.assign(priceGuessingExample,
    //   case '-3': this.merchant = Object.assign(fortuneExample,
    //   case '-4': this.merchant = Object.assign(formsExample,
    //   case '-5': this.merchant = Object.assign(statisticsExample,
    //   case '-6': this.merchant = Object.assign(audioLabellingExample,
    //   case '-7': this.merchant = Object.assign(abTestingExample,
    //   case '-8': this.merchant = Object.assign(abTestingExample,

    if(type == 'hcaptcha') {

      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-8?source=%2Fdashboard%2Ftab-home`);
      }, 1000)

    }
    else if(type == 'cvat') {
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-1?source=%2Fdashboard%2Ftab-home`);
      }, 1000)

    }
    else if(type == 'fortune') {
      // this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false).then(powerUserMode => {
      //   if(powerUserMode == "true") {
      //     setTimeout(() => {
      //       this.nav.navigateForward(`/dashboard/${type}`);
      //     }, 333)
      //   } else {
      //     this.presentToast(this.translateService.instant('CONTACT.title'));
      //   }
      // })

      // this.presentToast(this.translateService.instant('CONTACT.title'));
      // return;

      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-3?source=%2Fdashboard%2Ftab-home`);
      }, 1000)

    }

    else if(type =="audino"){
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-6?source=%2Fdashboard%2Ftab-home`);
      }, 1000)
    }
    else if(type =="ab"){
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-7?source=%2Fdashboard%2Ftab-home`);
      }, 1000)
    }
    else if(type =="priceGuessing"){
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-2?source=%2Fdashboard%2Ftab-home`);
      }, 1000)
    }
    else if(type =="formElements"){
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-4?source=%2Fdashboard%2Ftab-home`);
      }, 1000)
    }
    else if(type =="quiz"){
      setTimeout(() => {
        this.nav.navigateForward(`/merchant-details/trustedPartners/-5?source=%2Fdashboard%2Ftab-home`);
      }, 1000)
    }

  }

  leadershipChart() {
    var id = "#leaderboard";
    var d3 = window.d3;

    d3.select(id).selectAll("*").remove();
    d3.select("#legend").selectAll("*").remove();
    d3.select("#demo").selectAll("*").remove();

    var getRandomInt = (min, max) => {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min) + min);
    };

    var data = [
      { year: 2016, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2016, name:"Leadership", n: getRandomInt(0, 80) },
      { year: 2017, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2017, name:"Leadership", n: getRandomInt(0, 80) },
      { year: 2018, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2018, name:"Leadership", n: getRandomInt(0, 80) },
      { year: 2019, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2019, name:"Leadership", n: getRandomInt(0, 80) },
      { year: 2020, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2020, name:"Leadership", n: getRandomInt(0, 80) },
      { year: 2021, name:"Reputation", n: getRandomInt(0, 80) },
      { year: 2021, name:"Leadership", n: getRandomInt(0, 80) },
    ]

    var margin = {top: 10, right: 60, bottom: 30, left: 30},
    width = window.innerWidth - 100, // 300
    height = 120;

    var svg = d3.select(id)
      .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform",
              "translate(" + margin.left + "," + margin.top + ")")

    //console.log(svg);


    var sumstat = d3.nest() // nest function allows to group the calculation per level of a factor
    .key(function(d) { return d.name;})
    .entries(data);

    // Add X axis --> it is a date format
    var x = d3.scaleLinear()
      .domain(d3.extent(data, function(d) { return d.year; }))
      .range([ 0, width ]);
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x).ticks(5))
      .attr("stroke", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
      .attr("stroke-width", 0.4);

    // Add Y axis
    var y = d3.scaleLinear()
      .domain([0, d3.max(data, function(d) { return +d.n; })])
      .range([ height, 0 ]);
    svg.append("g")
      .call(d3.axisLeft(y))
      .attr("stroke", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
      .attr("stroke-width", 0.4);

    // color palette
    var res = sumstat.map(function(d){ return d.key }) // list of group names
    var color = d3.scaleOrdinal()
      .domain(res)
      .range(['#54BF7B','#FF9C00'])

    // console.log(sumstat);

    // Draw the line
    svg.selectAll(".line")
        .data(sumstat)
        .enter()
        .append("path")
          .attr("fill", "none")
          .attr("stroke", function(d){ return color(d.key) })
          .attr("stroke-width", 1.5)
          .attr("d", function(d){
            return d3.svg.line().interpolate("basis")
              .x(function(d) { return x(d.year); })
              .y(function(d) { return y(+d.n); })
              (d.values)
          })

    var legend = d3
    .select('#legend')
    .append('svg')
    .attr("height", 50)
              .selectAll('.legendItem')
              .data(sumstat);

            legend
              .enter()
              .append('rect')
              .attr('class', 'legendItem')
              .attr('width', 12)
              .attr('height', 12)
              .style('fill', d => color(d.key))
              .attr('transform',
                           (d, i) => {
                               var x = 10;
                               var y = 10 + (12 + 4) * i;
                               return `translate(${x}, ${y})`;
                           });


    legend
    .enter()
    .append('text')
    .attr('x', 10 + 12 + 5)
    .attr('y', (d, i) => 10 + (12 + 4) * i + 12)
    .text(d => this.translateService.instant('HCAPTCHA_DASHBOARD.'+d.key.toLowerCase()) )
    .attr("stroke",  this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .attr("stroke-width", 0.2)
    .attr("fill", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000");

    d3.select("#demo")
    // .append('svg')
    .append('text')
    .attr("height", 50)
    .attr('x', width - 20)
    .attr('y', 10)
    .attr("stroke",  this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .attr("stroke-width", 0.2)
    .attr("fill", this._Theme.getCurrentTheme() == "dark" ? "#fff" : "#000")
    .text(this.translateService.currentLang == 'de' ? 'Demo Modus' : 'Demo Mode')
  }

  private userNeedsToDoKyc() {

    this._KycService.isUserAllowedToUseChatMarketplace().then(v => {
      if(!v) {

        var state = this._KycService.getState(KycProvider.ONDATO_PROD);
        console.log(state);
        if (state == KycState.KYC_INITIATED) {

          var time_ = this._KycService.getKycInitiatedTimestamp(KycProvider.ONDATO_PROD);
          console.log("time_", time_);
          if(time_) {
            var currentTime = +new Date();
            console.log("currentTime", currentTime)

            if(currentTime < (time_ + (15 * 60 * 1000))) {
              var remainingTime = (time_ + (15 * 60 * 1000)) - currentTime;

              var diff = remainingTime;
              diff = Math.floor(diff / 1000);
              var secs_diff = diff % 60;
              diff = Math.floor(diff / 60);
              var mins_diff = diff % 60;
              diff = Math.floor(diff / 60);
              var hours_diff = diff % 24;
              diff = Math.floor(diff / 24);

              this.displayTime = `${hours_diff}:${mins_diff}:${secs_diff}`;


              // console.log("remainingTime", remainingTime);

              this.alertButtons = [
                {
                  text: this.translateService.instant('GENERAL.ok'),
                  handler: () => {
                    this.displayAlert = false;
                    if (this.timestampSubscription) {
                      if (!this.timestampSubscription.closed) {
                        this.timestampSubscription.unsubscribe();
                      }
                    }
                  }
                }
              ]

              this.displayAlert = true;
              // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');
              // this.timestampSubscription = this.timestampObserver.subscribe(() => {
              //   remainingTime = remainingTime - 1000;
              //   diff = remainingTime;
              //   diff = Math.floor(diff / 1000);
              //   var secs_diff = diff % 60;
              //   diff = Math.floor(diff / 60);
              //   var mins_diff = diff % 60;
              //   diff = Math.floor(diff / 60);
              //   var hours_diff = diff % 24;
              //   diff = Math.floor(diff / 24);

              //   this.displayTime = `${hours_diff}:${mins_diff}:${secs_diff}`;
              //   // this.displayTime = this.datePipe.transform(remainingTime, 'HH:mm:ss');
              //   // console.log(this.displayTime);

              //   if(remainingTime <= 1000) {
              //     if (this.timestampSubscription) {
              //       if (!this.timestampSubscription.closed) {
              //         this.timestampSubscription.unsubscribe();
              //       }
              //     }
              //   }
              // })



              // this._AlertController.create({
              //   header: this.translateService.instant('KYCINPROGRESS.header'),
              //   // subHeader: `${this.displayTime}`,
              //   message: `${this.translateService.instant('KYCINPROGRESS.message-1')} ${this.translateService.instant('KYCINPROGRESS.message-2')}: ${this.displayTime}`,
              //   buttons: this.alertButtons
              // }).then(alerto => {
              //   alerto.present();
              // })
            }
          }

        } else {
          this._AlertController.create({
            mode: 'ios',
            header: this.translateService.instant('HCAPTCHAKYC.header'),
            message: this.translateService.instant('HCAPTCHAKYC.subheader'),
            buttons: [
              {
                text: this.translateService.instant('GENERAL.ok'),
                handler: () => {
                  this.nav.navigateForward('/kyc');
                }
              },
              {
                text: this.translateService.instant('BUTTON.CANCEL'),
                role: "cancel",
                handler: () => {}
              }
            ]
          }).then(alerto => {
            alerto.present();
          })
        }


      }
    })
  }

  openNews() {
    if(this._Broadcast._Messages.length > 0) {
      this._ModalController.create({
        component: BroadcastsComponent,
      }).then(modal => {
        modal.onDidDismiss().then(() => {
          this._Broadcast.resetCounter();
        })
        modal.present();
      })
    }
  }

  goToLibrechat() {
    this.nav.navigateForward("/librechat");
  }
}
