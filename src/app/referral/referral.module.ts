import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ReferralPageRoutingModule } from './referral-routing.module';
import { ReferralPage } from './referral.page';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { JobHeaderModule } from '../job-header/job-header.module';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { QRCodeModule } from 'angularx-qrcode';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    JobHeaderModule,
    SharedModule,
    QRCodeModule,
    ReferralPageRoutingModule
  ],
  providers: [
    SocialSharing
  ],
  declarations: [ReferralPage]
})
export class ReferralPageModule {}
