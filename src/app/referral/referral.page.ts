import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Browser } from '@capacitor/browser';


// custom imports
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { ActivatedRoute } from '@angular/router';
import { Capacitor } from '@capacitor/core';


@Component({
  selector: 'app-referral',
  templateUrl: './referral.page.html',
  styleUrls: ['./referral.page.scss'],
})
export class ReferralPage implements AfterViewInit, OnInit {

  profilePictureSrc: string;
  // the reason why we use properties instead of using getPlatform() from the template
  // is that for performance reason its always a bad thing using functions in the template
  isNative = Capacitor.isNativePlatform();
  isIOS = Capacitor.getPlatform() === 'ios';
  isAndroid = Capacitor.getPlatform() === 'android';

  private code: string;
  qrCodeUrl: string;

  constructor(
    private route: ActivatedRoute,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) { }

  ngOnInit() {
    this.profilePictureSrc = this.userPhotoServiceAkita.getPhoto();
    this.code = this.route.snapshot.queryParamMap.get('code');
    this.qrCodeUrl = 'https://web.jobgrader.app/referral?code=' + encodeURIComponent(this.code);
  }

  ngAfterViewInit() {
    try {
      this.openDeepLink('_self');
    } catch (error) {
      console.error('ReferralPage#ngAfterViewInit error', error);
    }
  }

  async openAppstore(windowName: string) {
    await Browser.open({ url: 'https://testflight.apple.com/join/8GJWfQjC', windowName: windowName });
  }

  async openPlaystore(windowName: string) {
    await Browser.open({ url: 'https://play.google.com/store/apps/details?id=app.jobgrader.crowdworker', windowName: windowName });
  }

  async open(windowName: string) {
    await Browser.open({ url: 'https://web.jobgrader.app/referral?code=' + encodeURIComponent(this.code), windowName: windowName });
  }

  async openDeepLink(windowName: string) {
    await Browser.open({ url: 'jobgrader://jobgrader.app/referral?code=' + encodeURIComponent(this.code), windowName: windowName });
  }

}
