import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { AccountModalComponent } from './account-modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  declarations: [AccountModalComponent],
//   entryComponents: [AccountModalComponent],
  exports: [AccountModalComponent]
})
export class AccountModalModule {}
