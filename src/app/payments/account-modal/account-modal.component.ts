import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UntypedFormGroup, Validators, UntypedFormControl } from '@angular/forms';
import { NftService } from 'src/app/core/providers/nft/nft.service';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { DatePipe } from '@angular/common';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { ethers } from 'ethers';

@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.scss'],
})
export class AccountModalComponent implements OnInit {
@Input() data: any;

  public icon;
  public form = new UntypedFormGroup({
    mnemonic: new UntypedFormControl('',[]),
    name: new UntypedFormControl('',[]),
  });
  private datePipe = new DatePipe('en-US');

  constructor(
    private _ModalController: ModalController,
    private _ToastController: ToastController,
    private _SecureStorageService: SecureStorageService,
    private _NftService: NftService,
    private _LoaderProviderService: LoaderProviderService,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.icon = "../../../assets/job-user.svg";
  }

  close() {
    this._ModalController.dismiss();
  }

  async import() {
    await this._LoaderProviderService.loaderCreate();
    if(!this.form.value.mnemonic || !this.form.value.name) {
      console.log("Fields can not be empty");
      await this._LoaderProviderService.loaderDismiss();
      return this.presentToast(this._Translate.instant('WALLET.fieldEmpty'));
    }
    if(this.form.value.name.trim() == "helix_default") {
      await this._LoaderProviderService.loaderDismiss();
      return this.presentToast(this._Translate.instant('WALLET.nohelixdefault'));
    }

    // console.log(ethers.wordlists); // only en is logged

    // cz: cz,
    // en: en,
    // es: es,
    // fr: fr,
    // it: it,
    // ja: ja,
    // ko: ko,
    // zh: zh_cn,
    // zh_cn: zh_cn,
    // zh_tw: zh_tw
    var clearMnemonic = this.form.value.mnemonic.trim().toLowerCase();
    console.log(clearMnemonic);
    clearMnemonic = clearMnemonic.replaceAll(",", " ").replaceAll(";"," ").replaceAll("."," ").replaceAll("+"," ").replaceAll("-"," ").replaceAll("/"," ").replaceAll("_"," ");
    console.log(clearMnemonic);
    clearMnemonic = clearMnemonic.split(" ").filter(k => k != '').join(" ");
    console.log(clearMnemonic);
    var wallet = this._NftService.returnWallet(clearMnemonic);
    console.log(wallet);
    if(!wallet){
      await this._LoaderProviderService.loaderDismiss();
      return this.presentToast(this._Translate.instant('WALLET.invalidMnemonic'));
    }
    var existingUserMnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);
    var verifiedWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    var wallets = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    var walletsParse = !!wallets ? JSON.parse(wallets) : [];
    var check = walletsParse.find(w => w.publicKey == (wallet as any).address);
    if(check || (existingUserMnemonic == wallet.mnemonic.phrase)) {
      await this._LoaderProviderService.loaderDismiss();
      return this.presentToast(this._Translate.instant('WALLET.walletExists'));
    }

    if(!existingUserMnemonic && verifiedWalletAddress == wallet.address) {

      await this._SecureStorageService.setValue(SecureStorageKey.web3WalletName, this.form.value.name);
      await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, wallet.mnemonic.phrase);

    } else {
      const colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
      const random = Math.floor(Math.random() * colorschemes.length);
      const color = colorschemes[random];
      walletsParse.push({
        heading: this.form.value.name,
        mnemonic: wallet.mnemonic.phrase,
        publicKey: wallet.address,
        creationDate: +new Date(),
        color
      })
      console.log(walletsParse);
      await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(walletsParse));
    }

    this.form.reset();
    await this._LoaderProviderService.loaderDismiss();
    this.close();
  }

  async deleteWallet() {
    var wallets = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    var walletsParse = !!wallets ? JSON.parse(wallets) : [];
    var filtered = walletsParse.filter(w => w.publicKey !== this.data.account.publicKey);
    await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(filtered));
    this.presentToast(this._Translate.instant('WALLET.walletDeleted'));
    this.close();
  }

  presentToast(message: string) {
    this._ToastController.create({
      message: message,
      position: 'top',
      duration: 2000
    }).then(toast => toast.present())
  }

}
