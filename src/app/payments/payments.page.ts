import { Component } from "@angular/core";

import {
  ActionSheetController,
  AlertController,
  ModalController,
  NavController,
  ToastController,
} from "@ionic/angular";
import { SecureStorageService } from "../core/providers/secure-storage/secure-storage.service";
import { SecureStorageKey } from "../core/providers/secure-storage/secure-storage-key.enum";
import { AccountModalComponent } from "./account-modal/account-modal.component";
import {
  NftService,
  OpenSeaMethodTypes,
} from "../core/providers/nft/nft.service";
import { ethers } from "ethers";
import { environment } from "src/environments/environment";
import CryptoJS from "crypto-js";
import { UDIDNonce } from "../core/providers/device/udid.enum";
import { WalletImportDisclaimerComponent } from "./wallet-import-disclaimer/wallet-import-disclaimer.component";
import { KeyExportComponent } from "../sign-up/components/step-5/key-export/key-export.component";
import { ImportUserkeyComponent } from "../login/import-userkey/import-userkey.component";
import { LoaderProviderService } from "../core/providers/loader/loader-provider.service";
import { TranslateProviderService } from "../core/providers/translate/translate-provider.service";
import { ImageSelectionService } from "../shared/components/image-selection/image-selection.service";
import Identicon from "identicon.js";
import { NetworkService } from "../core/providers/network/network-service";
import { UserProviderService } from "../core/providers/user/user-provider.service";
import {
  CryptoCurrency,
  CryptoNetworkNames,
  EthereumNetworks,
} from "../core/providers/wallet-connect/constants";
import { ApiProviderService } from "../core/providers/api/api-provider.service";
import { KycService } from "../kyc/services/kyc.service";

import { generateMnemonic } from "bip39";
import { AuthenticationProviderService } from "../core/providers/authentication/authentication-provider.service";
import { AppStateService } from "../core/providers/app-state/app-state.service";
import { VaultSecretKeys, VaultService } from "../core/providers/vault/vault.service";
import { CryptoCurrencyService } from "./crypto-account-page/services/crypto-currency.service";
import { fiatCurrencies } from "../core/providers/nft/fiat-currencies";
import { BarcodeService } from "../core/providers/barcode/barcode.service";
import { ActivityKeys, UserActivitiesService } from "../core/providers/user-activities/user-activities.service";
import { Account } from "./model/account.interface";
import { UserPhotoServiceAkita } from "../core/providers/state/user-photo/user-photo.service";
import { Clipboard } from "@capacitor/clipboard";
import { AlchemyService } from "../core/providers/alchemy/alchemy.service";
import { MoralisService } from "@services/moralis/moralis.service";
import { WalletActivationDisclaimerComponent } from "./wallet-activation-disclaimer/wallet-activation-disclaimer.component";
import { Capacitor } from "@capacitor/core";
import { LockService } from "../lock-screen/lock.service";
import { Chooser } from "@awesome-cordova-plugins/chooser/ngx";
import { CryptoProviderService } from "@services/crypto/crypto-provider.service";
import { File } from "@awesome-cordova-plugins/file/ngx";
import * as web3 from 'web3';
import { FingerprintAIO, FingerprintOptions } from "@awesome-cordova-plugins/fingerprint-aio/ngx";
import { BiometricService } from "@services/biometric/biometric.service";
import { FirestoreCloudFunctionsService } from "@services/firestore-cloud-functions/firestore-cloud-functions.service";
const abi = require('./crypto-account-page/abi.json');
declare var window: any;

@Component({
  selector: "app-payments",
  templateUrl: "./payments.page.html",
  styleUrls: ["./payments.page.scss"],
})
export class PaymentsPage {
  public profilePictureSrc = "../../assets/job-user.svg";
  public displayVerifiedTick = false;
  public accounts: Account[] = [];
  public trackingAccounts = [];
  public isLoaded = false;

  public tapToInitiate = true;
  public tapToActivate = true;

  private DEFAULT_FIAT = "EUR";
  public FIAT_SYMBOL = "€";

  private cryptoBalance = {};

  displayLoader = false;

  // fingerprintOptions: FingerprintOptions = environment.fingerprintOptions;

  public cards = [
    {
      state: 'ON',
      logo: "assets/img/visa.png",
      a: 1234,
      b: 5522,
      c: 8432,
      d: 2264,
      expires: '7/12',
      bank: 'Bank of America'
    },
    {
      state: 'OFF',
      logo: "assets/img/american.png",
      a: 1234,
      b: 5321,
      c: 8283,
      d: 9271,
      expires: '8/19',
      bank: 'JPMorgan'
    },
    {
      state: 'ON',
      logo: "assets/img/mastercard.png",
      a: 8685,
      b: 2445,
      c: 9143,
      d: 7846,
      expires: '11/23',
      bank: 'CityBank'
    }
  ];

  constructor(
    private _NavController: NavController,
    private _SecureStorageService: SecureStorageService,
    private _AuthenticationProviderService: AuthenticationProviderService,
    private _ToastController: ToastController,
    private _AlertController: AlertController,
    private _ModalController: ModalController,
    private _LoaderProviderService: LoaderProviderService,
    private _Translate: TranslateProviderService,
    private _ImageSelectionService: ImageSelectionService,
    private _NftService: NftService,
    private _ActionSheetController: ActionSheetController,
    private _ApiProviderService: ApiProviderService,
    public _AppStateService: AppStateService,
    private _NetworkService: NetworkService,
    private _UserProviderService: UserProviderService,
    private _KycService: KycService,
    private _Vault: VaultService,
    private _Barcode: BarcodeService,
    private _Alchemy: AlchemyService,
    private _Moralis: MoralisService,
    private _Firecloud: FirestoreCloudFunctionsService,
    private _LockService: LockService,
    private _Chooser: Chooser,
    private _File: File,
    private _CryptoProviderService: CryptoProviderService,
    private _UserActivities: UserActivitiesService,
    private _CryptoCurrencyService: CryptoCurrencyService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private biometric: BiometricService
  ) {
  }

  async ionViewDidEnter() {
    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter("ion-footer");
    } else {
      this._NetworkService.removeOfflineFooter("ion-footer");
    }
    if(this._AppStateService.basicAuthToken) {
      var photo = this.userPhotoServiceAkita.getPhoto();
    }
    var currency = await this._SecureStorageService.getValue(
      SecureStorageKey.currency,
      false
    );
    this.DEFAULT_FIAT = !!currency ? currency : this.DEFAULT_FIAT;
    this.FIAT_SYMBOL = fiatCurrencies.find(fi => fi.currency_code == this.DEFAULT_FIAT).currency_symbol;
    this.displayVerifiedTick =
      await this._KycService.isUserAllowedToUseChatMarketplace();
    this.profilePictureSrc = !!photo
      ? photo
      : "../../assets/job-user.svg";
      this._AuthenticationProviderService.displayLoginOptionIfNecessary();
      this.populateWallets();
  }

  async returnNftNumbers(address: string) {
    var nftNumbersCache = await this._SecureStorageService.getValue(
      SecureStorageKey.nftNumbersCache,
      false
    );
    var nftNumbersCacheParse = !!nftNumbersCache
      ? JSON.parse(nftNumbersCache)
      : {};
    return !!nftNumbersCacheParse[address]
      ? nftNumbersCacheParse[address]
      : {
          "Spams": 0,
          "Safe": 0,
          "POAP": 0,
          // "Decentraland Wearables": 0,
        };
  }

  async updateNftNumbers(address: string, content: any) {
    var nftNumbersCache = await this._SecureStorageService.getValue(
      SecureStorageKey.nftNumbersCache,
      false
    );
    var nftNumbersCacheParse = !!nftNumbersCache
      ? JSON.parse(nftNumbersCache)
      : {};
    nftNumbersCacheParse = Object.assign(nftNumbersCacheParse, {
      [address]: content,
    });
    await this._SecureStorageService.setValue(
      SecureStorageKey.nftNumbersCache,
      JSON.stringify(nftNumbersCacheParse)
    );
  }

  exportKeys(account: any) {
    this._ModalController
      .create({
        component: KeyExportComponent,
        componentProps: {
          mnemonic: account.mnemonic,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {});
        modal.present();
      });
  }

  goToWalletSettings(account: any) {
    this._NavController.navigateForward(
      `/dashboard/payments/wallet-settings?address=${account.publicKey}`
    );
  }

  tapToInitiateMethod() {
    this._NavController.navigateForward('/wallet-generation/step-2');
  }

  async missingVerifiedWallet() {

    const c = (): Promise<boolean> => {
      return new Promise(resolve => {
        this._AlertController.create({
          mode: 'ios',
          message: this._Translate.instant('WALLETACTIVATIONDISCLAIMER.password-entry'),
          inputs: [{
            type: 'password',
            placeholder: "...",
            name: 'password'
          }],
          buttons: [
            {
              text: this._Translate.instant('BUTTON.CANCEL'),
              role: 'cancel',
              handler: () => {
                resolve(false);
              }
            },
            {
              text: this._Translate.instant('GENERAL.ok'),
              handler: async (data) => {
                const plaintextPassword = data.password;
                const passwordHash = await this._SecureStorageService.getValue(SecureStorageKey.passwordHash, false);
                const nonce = await this._SecureStorageService.getValue(SecureStorageKey.nonce, false);
                if(this._CryptoProviderService.generateHash(plaintextPassword, nonce) == passwordHash) {
                  resolve(true);
                } else {
                  this.presentToast(this._Translate.instant('EXPORTKEY.incorrect-password'));
                  resolve(false);
                }
              }
            }
          ]
        }).then(alerti => alerti.present())    
      })
      
    }

    const b = (): Promise<boolean> => {
      return new Promise(resolve => {
        this.biometric.biometricAvailable$().subscribe({
          next: (res) => {
            console.log(res);
            if(res) {
              this.biometric.biometricShow().subscribe({
                next: (value) => {
                  resolve(true);
                },
                error: (ee) => {
                  resolve(false);
                }
              })
            } else {
              c().then(v => resolve(v));
            }
          }, 
          error: (e) => {
            c().then(v => resolve(v));
          }
        })
      })
    }

    const a = await this._AlertController.create({
      mode: "ios",
      header: this._Translate.instant("MISSINGVERIFIEDWALLET.header"), 
      message: `${this._Translate.instant("MISSINGVERIFIEDWALLET.message-0")} ${this._Translate.instant("MISSINGVERIFIEDWALLET.message-1")}`, // "It seems that your verified wallet address is not activated. Please select one of the following options: ",
      buttons: [{
        text: this._Translate.instant("MISSINGVERIFIEDWALLET.options.import"), 
        handler: async () => {
          const modal0 = await this._ModalController.create({
            component: WalletActivationDisclaimerComponent,
          });
          modal0.onDidDismiss().then(async (data) => {
            console.info({ data });
            if(data.data.value) {
              const modal1 = await this._ModalController.create({
                component: ImportUserkeyComponent,
                componentProps: {
                  initial: false,
                },
              });
        
              modal1.onDidDismiss().then(async (data) => {
        
                if (data.data.value) {
                  const modal = await this._ModalController.create({
                    component: AccountModalComponent,
                    componentProps: {
                      data: {
                        new: true,
                      },
                    },
                  });
                  modal.onDidDismiss().then(() => {
                    this.populateWallets();
                  });
                  await modal.present();
                } else {
                  await this.populateWallets();
                }
              });
        
              await modal1.present();
            }
          })
          const auth = await b();
          if(auth) {
            await modal0.present();
          }
        }
      }, {
        text: this._Translate.instant("MISSINGVERIFIEDWALLET.options.generate"), 
        handler: async () => {
          const modal0 = await this._ModalController.create({
            component: WalletActivationDisclaimerComponent,
          });
          modal0.onDidDismiss().then(async (data) => {
            console.info({ data });
            if(data.data.value) {
              this._NavController.navigateForward(`/wallet-generation`);
            }
          })
          const auth = await b();
          if(auth) {
            await modal0.present();
          }
        }
      }, {
        text: this._Translate.instant("WALLET.cancel"),
        handler: () => [

        ]
      }]
    });
    await a.present(); 
  }

  async populateWallets() {
    var selfWalletAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey,false);
    var mnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic,false);

    // console.log(selfWalletAddress);
    // console.log(mnemonic);

    if(selfWalletAddress) {

      if(mnemonic) {
          this.tapToInitiate = false;
          this.tapToActivate = false;
      } else {
          this.tapToInitiate = false;
          this.tapToActivate = true;
      }
    } else {
        this.tapToInitiate = true;
        this.tapToActivate = false;
    }

    console.log(this.tapToInitiate);
    console.log(this.tapToActivate);

    if(this.tapToInitiate)
    {
      this.isLoaded = true;
    }
    else
    {
      var check = await this._ApiProviderService
      .getUserWeb3WalletAddress()
      .catch(async (e) => {
        console.log(e);
        // await this._LoaderProviderService.loaderDismiss();
        this.displayLoader = false;
        return;
      });
      // console.log(selfWalletAddress);
      // console.log(check);
      if (check) {
        if (check != selfWalletAddress) {
          this.presentToast(
            this._Translate.instant("WALLETSETTINGS.errors.address-changed")
          );
          await this._ApiProviderService.checkForIDLinkWalletUpdate(check);
        }
      }

      // await this._LoaderProviderService.loaderCreate();
      this.displayLoader = true;
      this.isLoaded = false;

      this.accounts = [];

      // if (mnemonic) {
        var userData = await this._UserProviderService.getUser();
        var existingDefaultWalletName = await this._SecureStorageService.getValue(
          SecureStorageKey.web3WalletName,
          false
        );
        var web3WalletName = !!existingDefaultWalletName
          ? existingDefaultWalletName
          : `${userData.callname}'s Wallet`;
        await this._SecureStorageService.setValue(
          SecureStorageKey.web3WalletName,
          web3WalletName
        );

        var verifiedWalletCreationDateCloud = await this._UserActivities.getActivityFromStorage(ActivityKeys.VERIFIED_BACKUP_ICLOUD);
        var verifiedWalletCreationDatePaper = await this._UserActivities.getActivityFromStorage(ActivityKeys.VERIFIED_BACKUP_PAPER);
        var verifiedWalletCreationDateLocal = await this._UserActivities.getActivityFromStorage(ActivityKeys.VERIFIED_BACKUP_LOCAL);

        var verifiedWalletCreationDate = (
          (!!verifiedWalletCreationDateCloud ? verifiedWalletCreationDateCloud.timestamp : undefined) ||
          (!!verifiedWalletCreationDatePaper ? verifiedWalletCreationDatePaper.timestamp : undefined) ||
          (!!verifiedWalletCreationDateLocal ? verifiedWalletCreationDateLocal.timestamp : undefined)
        );

        this.accounts.push({
          heading: web3WalletName,
          primary: true,
          publicKey: selfWalletAddress,
          icon:
            this.profilePictureSrc == "../../assets/job-user.svg"
              ? "../../assets/job-user.svg"
              : this.profilePictureSrc,
          creationDate: verifiedWalletCreationDate,
          mnemonic: mnemonic,
          privateKey: null,
          color: !!mnemonic ? 'green' : 'grey',
          nft_balance: this.calculateNFTSum(await this.returnNftNumbers(selfWalletAddress))
        });
      // }

      let importedWallets = await this._SecureStorageService.getValue(
        SecureStorageKey.importedWallets,
        false
      );
      let ima = !!importedWallets ? JSON.parse(importedWallets) : [];
      // console.log('PaymentsPage#populateWallets; importedWallets:', ima);

      var unverifiedWalletCreationDateCloud = await this._UserActivities.getActivityFromStorage(ActivityKeys.UNVERIFIED_BACKUP_ICLOUD);
      var unverifiedWalletCreationDatePaper = await this._UserActivities.getActivityFromStorage(ActivityKeys.UNVERIFIED_BACKUP_PAPER);
      var unverifiedWalletCreationDateLocal = await this._UserActivities.getActivityFromStorage(ActivityKeys.UNVERIFIED_BACKUP_LOCAL);

      var unverifiedWalletCreationDate = (
        (!!unverifiedWalletCreationDateCloud ? unverifiedWalletCreationDateCloud.timestamp : undefined) ||
        (!!unverifiedWalletCreationDatePaper ? unverifiedWalletCreationDatePaper.timestamp : undefined) ||
        (!!unverifiedWalletCreationDateLocal ? unverifiedWalletCreationDateLocal.timestamp : undefined)
      );

      if (ima.length > 0) {
        for (let im = 0; im < ima.length; im++) {
          if (ima[im]?.publicKey) {
            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];
            console.log("color", ima[im].color);
            this.accounts.push({
              heading: ima[im].heading,
              publicKey: ima[im].publicKey,
              icon: `data:image/png;base64,${new Identicon(
                ima[im].publicKey,
                420
              ).toString()}`,
              creationDate: !!ima[im].creationDate ? ima[im].creationDate : unverifiedWalletCreationDate,
              mnemonic: !!ima[im].mnemonic ? ima[im].mnemonic : null,
              privateKey: !!ima[im].privateKey ? ima[im].privateKey : null,
              color: !!ima[im].color ? ima[im].color : color,
              nft_balance: this.calculateNFTSum(await this.returnNftNumbers(ima[im].publicKey))
            });

          } else {

            console.error('PaymentsPage#populateWallets; publicKey undefined:', ima[im]);

          }
        }
      }
      this.isLoaded = true;
      // await this._LoaderProviderService.loaderDismiss();
      this.displayLoader = false;

      if(this.tapToActivate) {
        this.missingVerifiedWallet();
      }

      let cryptoBalanceString = await this._SecureStorageService.getValue(
        SecureStorageKey.cryptoBalance,
        false
      );
      let cryptoConversion = await this._SecureStorageService.getValue(
        SecureStorageKey.cryptoConversions,
        false
      );
      let cBalance = !!cryptoBalanceString ? JSON.parse(cryptoBalanceString) : {};
      let cConversion = !!cryptoConversion ? JSON.parse(cryptoConversion) : {};

      console.log("accounts ===>", this.accounts);

      // mburger: this is more a workaround to remove currencies of removed wallets
      await this.removeCurriencesFromRemovedWallets(this.accounts).then();

      // console.log("this.cryptoBalance 1", this.cryptoBalance);

      for(let _ of this.accounts) {
        this.cryptoBalance = Object.assign(this.cryptoBalance, {
          [_.publicKey]: {}
        })
      }

      // console.log("this.cryptoBalance 2", this.cryptoBalance);

      for(let i = 0; i< this.accounts.length; i++){
        let d = this.accounts[i];

        let wallet_currencies =
        await this._CryptoCurrencyService.getWalletCurrencies(
          d.publicKey
        );
        if(!wallet_currencies) {
          await this._CryptoCurrencyService.setInitialWalletData(
            d.publicKey
            // ,
            // this._CryptoCurrencyService.all_wallets_currencies_data
          );
          wallet_currencies =
          await this._CryptoCurrencyService.getWalletCurrencies(
            d.publicKey
          );
        }

        // console.log('===>', d);
        let active_currencies = wallet_currencies.currencies_data.filter(we => !!we.active);
        console.log("active_currencies", active_currencies);

        for(let _ of active_currencies) {

          this.cryptoBalance[d.publicKey] = Object.assign(this.cryptoBalance[d.publicKey], {
              [_.currency]: {
                core: !!cBalance[d.publicKey]
                ? !!cBalance[d.publicKey][_.currency]
                  ? cBalance[d.publicKey][_.currency]
                  : 0
                : 0,
                converted: !!cBalance[d.publicKey]
                ? !!cBalance[d.publicKey][_.currency]
                  ? !!cConversion[`${_.currency}/${this.DEFAULT_FIAT}`]
                    ? Number(
                        cBalance[d.publicKey][_.currency] *
                          cConversion[`${_.currency}/${this.DEFAULT_FIAT}`]
                      )
                    : 0
                  : 0
                : 0,
              }
            })

        }

        let balance_sum = 0;
        for (let _ of Object.keys(this.cryptoBalance[d.publicKey])) {
          // console.log(this.cryptoBalance[d.publicKey][_]);
          if(!!this.cryptoBalance[d.publicKey][_]) {
            balance_sum += this.cryptoBalance[d.publicKey][_].converted;
          }
        }

        console.log("sum is", balance_sum);
        this.accounts[i] = {
          ...this.accounts[i],
          balance: balance_sum.toFixed(2),
        };


        const res5 = await this._Alchemy.fetchNfts(
          d.publicKey
        );

        const res6 = await this._Moralis.fetchNfts(
          d.publicKey
        );

        const res7 = await this._Firecloud.fetchEpnNfts(
          d.publicKey
        );

        const res3 = await this._NftService.returnPOAPMintedNFT(d.publicKey);
        // var res4 = await this._NftService.obtainDecentralandCollectibles(
        //   d.publicKey
        // );

        // console.log("check", res5, res3);

        await this.updateNftNumbers(d.publicKey, {
          "Spams": (!!res5 && !!res6 && !!res7)? res5.filter(k => !!k.contract.isSpam).length + res6.filter(k => !!k.possible_spam).length + res7.filter(k => !!k.possible_spam).length : 0,
          "Safe": (!!res5 && !!res6 && !!res7) ? res5.filter(k => !k.contract.isSpam).length + res6.filter(k => !k.possible_spam).length + res7.filter(k => !k.possible_spam).length : 0,
          "POAP": !!res3 ? res3.length : 0,
          // "Decentraland Wearables": !!res4 ? res4.data.length : 0,
        });

        let nft_balance = this.calculateNFTSum({
          "Spams": (!!res5 && !!res6 && !!res7)? res5.filter(k => !!k.contract.isSpam).length + res6.filter(k => !!k.possible_spam).length + res7.filter(k => !!k.possible_spam).length : 0,
          "Safe": (!!res5 && !!res6 && !!res7) ? res5.filter(k => !k.contract.isSpam).length + res6.filter(k => !k.possible_spam).length + res7.filter(k => !k.possible_spam).length : 0,
          "POAP": !!res3 ? res3.length : 0,
          // "Decentraland Wearables": !!res4 ? res4.data.length : 0,
        });
        this.accounts[i] = {
          ...this.accounts[i],
          nft_balance
        };

      }


      for(let i = 0; i< this.accounts.length; i++){
        let d = this.accounts[i];

        let wallet_currencies =
        await this._CryptoCurrencyService.getWalletCurrencies(
          d.publicKey
        );
        if(!wallet_currencies) {
          await this._CryptoCurrencyService.setInitialWalletData(
            d.publicKey
            // ,
            // this._CryptoCurrencyService.all_wallets_currencies_data
          );
          wallet_currencies =
          await this._CryptoCurrencyService.getWalletCurrencies(
            d.publicKey
          );
        }

        let active_currencies = wallet_currencies.currencies_data.filter(we => !!we.active);
        // console.log("active_currencies", active_currencies);

        for(let _ of active_currencies) {
          var balanceAndConversion = await this.getBalanceAndConversion(d.publicKey, await this._CryptoCurrencyService.CurrencyToRPCMapper(_.currency), _.currency);
          this.cryptoBalance[d.publicKey] = Object.assign(this.cryptoBalance[d.publicKey], {
              [_.currency]: {
                core: !!cBalance[d.publicKey]
                ? !!cBalance[d.publicKey][_.currency]
                  ? cBalance[d.publicKey][_.currency]
                  : balanceAndConversion.core
                : balanceAndConversion.core,
                converted: !!cBalance[d.publicKey]
                ? !!cBalance[d.publicKey][_.currency]
                  ? !!cConversion[`${_.currency}/${this.DEFAULT_FIAT}`]
                    ? Number(
                        cBalance[d.publicKey][_.currency] *
                          cConversion[`${_.currency}/${this.DEFAULT_FIAT}`]
                      )
                    : balanceAndConversion.converted
                  : balanceAndConversion.converted
                : balanceAndConversion.converted,
              }
            })
        }

        let balance_sum = 0;
        for (let _ of Object.keys(this.cryptoBalance[d.publicKey])) {
          // console.log(this.cryptoBalance[d.publicKey][_]);
          if(!!this.cryptoBalance[d.publicKey][_]) {
            balance_sum += this.cryptoBalance[d.publicKey][_].converted;
          }
        }

        console.log("sum is", balance_sum);
        this.accounts[i] = {
          ...this.accounts[i],
          balance: balance_sum.toFixed(2),
        };
      }
    }

  }

  private async removeCurriencesFromRemovedWallets(accounts: Account[]) {
    const data_from_storage = await this._CryptoCurrencyService.getUserCurrenciesListFromStorage();

    if (data_from_storage) {

      // let parsed_data: any[] = JSON.parse(data_from_storage);
      let parsed_data = data_from_storage;

      parsed_data =
        parsed_data.filter((data) => {

          const d =
            Object.keys(data).filter((_data) => {
              const a = accounts.filter((account) => { return account.publicKey === _data } );

              if (a?.length > 0) {
                return true;
              }
            });

          if (d?.length > 0) {
            return true;
          }

      });

      await this._CryptoCurrencyService.setUserCurrenciesListInStorage(parsed_data);
    }

  }

  

  async getBalanceAndConversion(
    address: string,
    provider: any,
    currency: string
  ) {
    let network;
    let balance;

    const CRYPTONETWORKS_HMT_POLYGON = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_HMT_POLYGON);
    const CRYPTONETWORKS_HMT_SKALE = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_HMT_SKALE);
    const CRYPTONETWORKS_THXC = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_THXC);

    const CONTRACTS_HMT_POLYGON = await this._Vault.getSecret(VaultSecretKeys.CONTRACTS_HMT_POLYGON);
    const CONTRACTS_HMT_SKALE = await this._Vault.getSecret(VaultSecretKeys.CONTRACTS_HMT_SKALE);
    const CONTRACTS_THXC = await this._Vault.getSecret(VaultSecretKeys.CONTRACTS_THXC);

    if (
      [
        EthereumNetworks.MAINNET,
        EthereumNetworks.KOVAN,
        EthereumNetworks.RINKEBY,
        EthereumNetworks.GOERLI,
        EthereumNetworks.SEPOLIA,
        EthereumNetworks.ROPSTEN,
      ].includes(provider)
    ) {
      network = new ethers.providers.EtherscanProvider(
        EthereumNetworks.MAINNET,
        await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
      );
      balance = await network.getBalance(address);
    } else if(currency == CryptoCurrency.HMT_POLYGON) {

      const hmtContract = CONTRACTS_HMT_POLYGON;
      const provider = CRYPTONETWORKS_HMT_POLYGON;
      const contract = new ethers.Contract(hmtContract, abi);
      const hmt = contract.connect(ethers.getDefaultProvider(provider));
      balance = await hmt.balanceOf(address);

    } else if(currency == CryptoCurrency.HMT_SKALE) {

      const hmtContract = CONTRACTS_HMT_SKALE;
      const provider = CRYPTONETWORKS_HMT_SKALE;
      const contract = new ethers.Contract(hmtContract, abi);
      const hmt = contract.connect(ethers.getDefaultProvider(provider));
      balance = await hmt.balanceOf(address);

    } else if(currency == CryptoCurrency.THXC) {

      const thxcContract = CONTRACTS_THXC;
      const provider = CRYPTONETWORKS_THXC;
      const contract = new ethers.Contract(thxcContract, abi);
      const hmt = contract.connect(ethers.getDefaultProvider(provider));
      balance = await hmt.balanceOf(address);

    }
    else {
      network = ethers.getDefaultProvider(provider);
      balance = await network.getBalance(address);
    }


    const val = ethers.BigNumber.from(balance._hex);

    if(currency == CryptoCurrency.THXC) {

      var eth = await this._NftService.returnTHXCBalance(address);
      var ethCon = await this._ApiProviderService.cryptoConversion(
        "USD",
        this.DEFAULT_FIAT
      );

      var con = Number(eth) * await this._Firecloud.obtainThxcUsdConversion() * ethCon;

    } else if(currency == CryptoCurrency.EVE) {
      con = 1;
    }
    else {
      con = await this._ApiProviderService.cryptoConversion(
        currency,
        this.DEFAULT_FIAT
      );
    }

    var getSec = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoConversions,
      false
    );
    var getSecParse = !!getSec ? JSON.parse(getSec) : {};
    getSecParse = Object.assign(getSecParse, { [`${currency}/${this.DEFAULT_FIAT}`]: con });

    await this._SecureStorageService.setValue(
      SecureStorageKey.cryptoConversions,
      JSON.stringify(getSecParse)
    );

    var core =
      Math.round(Number(ethers.utils.formatEther(val)) * 10000000) / 10000000;
    var converted = Math.round(100 * con * core) / 100;

    return { core, converted };
  }

  navigateToPage(page: string) {
    console.log("navigateToPage", page);
    this._NavController.navigateForward(page);
  }

  goBack() {
    this._NavController.navigateBack("/dashboard/tab-profile?source=web3");
  }

  async createNewWallet() {
    const alerty = await this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant("IMPORTKEY.alert-1-header"),
      inputs: [{ type: "text", placeholder: "...", name: "name" }],
      buttons: [
        {
          text: this._Translate.instant("BUTTON.CANCEL"),
          role: "cancel",
          handler: () => {},
        },
        {
          text: this._Translate.instant("GENERAL.ok"),
          handler: async (data) => {
            if (!data.name || data.name.trim() == "") {
              this.presentToast(
                this._Translate.instant("IMPORTKEY.name-empty-error")
              );
              return;
            }
            // if(data.name.trim() == "WEB3 Wallet") {
            //   this.presentToast( this._Translate.instant('IMPORTKEY.name-default-error') );
            //   return;
            // }
            if (data.name.trim().length > 20) {
              this.presentToast(
                this._Translate.instant("IMPORTKEY.name-length-error")
              );
              return;
            }

            var importedWallets = await this._SecureStorageService.getValue(
              SecureStorageKey.importedWallets,
              false
            );
            var ima = !!importedWallets ? JSON.parse(importedWallets) : [];

            var existingNames = Array.from(ima, (m) => (m as any).heading);

            if (existingNames.includes(data.name)) {
              this.presentToast(this._Translate.instant("IMPORTKEY.new-name"));
              return;
            }

            console.log("Before ethers");

            var newMnemonic = generateMnemonic();

            const newWallet = ethers.Wallet.fromMnemonic(newMnemonic);

            console.log("After ethers");

            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];

            ima.push({
              heading: data.name,
              publicKey: newWallet.address,
              mnemonic: newWallet.mnemonic.phrase,
              creationDate: +new Date(),
              color
            });

            await this._SecureStorageService.setValue(
              SecureStorageKey.importedWallets,
              JSON.stringify(ima)
            );

            await this._UserActivities.updateActivity(ActivityKeys.WALLET_CREATED, { address: this._ApiProviderService.obtainDisplayableAddress(newWallet.address) });

            const modal1 = await this._ModalController.create({
              component: KeyExportComponent,
              componentProps: {
                mnemonic: newWallet.mnemonic.phrase,
              },
            });
            modal1.onDidDismiss().then(() => {
              this.populateWallets();
            });
            await modal1.present();
          },
        },
      ],
    });

    await alerty.present();
  }

  presentToast(message: string) {
    this._ToastController
      .create({
        duration: 2000,
        position: "top",
        message,
      })
      .then((toast) => toast.present());
  }

  async importAccount() {
    const modal0 = await this._ModalController.create({
      component: WalletImportDisclaimerComponent,
    });
    modal0.onDidDismiss().then(async () => {
      const modal1 = await this._ModalController.create({
        component: ImportUserkeyComponent,
        componentProps: {
          initial: false,
        },
      });

      modal1.onDidDismiss().then(async (data) => {
        // await this.populateWallets();

        if (data.data.value) {
          const modal = await this._ModalController.create({
            component: AccountModalComponent,
            componentProps: {
              data: {
                new: true,
              },
            },
          });
          modal.onDidDismiss().then(() => {
            this.populateWallets();
          });
          await modal.present();
        } else {
          await this.populateWallets();
        }
      });

      await modal1.present();
    });
    modal0.present();
  }

  showOptions() {
    this._ActionSheetController
      .create({
        mode: "md",
        // header: this._Translate.instant('WALLET.addOptions'),
        buttons: [
          {
            text: this._Translate.instant("WALLET.importExistingWallet"),
            handler: () => {
              this.importAccount();
            },
          },
          {
            text: this._Translate.instant("WALLET.createNewWallet"),
            handler: () => {
              this.createNewWallet();
            },
          },
          {
            text: this._Translate.instant("WALLET.traceAccount"),
            handler: () => {
              this.traceAccount();
            },
          },
          {
            text:this._Translate.instant("WALLET.scanSeedPhrase"),
            handler: () => {
              this.scanWalletSeedPhrase();
            }
          },
          {
            text: this._Translate.instant("GENERAL.cancel"),
            icon: "close",
            role: "cancel",
            handler: () => {
              console.log("Cancel");
            },
          },
        ],
      })
      .then((sheet) => {
        sheet.present();
      });
  }

  scanWalletSeedPhrase() {
    this._Barcode.scanWalletSeedPhrase().then(async ({ heading, mnemonic, publicKey }) => {

      try {
        mnemonic = JSON.parse(mnemonic);
      } catch(e) {
        console.log(e);
      }
      // alert(`${heading}: ${publicKey}`);

      var importedWallets = await this._SecureStorageService.getValue(
        SecureStorageKey.importedWallets,
        false
      );

      var web3WalletMnemonic = await this._SecureStorageService.getValue(
        SecureStorageKey.web3WalletMnemonic,
        false
      );

      var web3WalletPublicKey = await this._SecureStorageService.getValue(
        SecureStorageKey.web3WalletPublicKey,
        false
      );

      var ima = !!importedWallets ? JSON.parse(importedWallets) : [];
      var checker = ima.findIndex(ii => ii.publicKey == publicKey);

      if(web3WalletPublicKey == publicKey) {
        if(web3WalletMnemonic == mnemonic) {
          this.presentToast(this._Translate.instant('IMPORTKEY.wallet-imported'));
        } else {
          if(ethers.utils.isValidMnemonic(mnemonic)) {
            await this._SecureStorageService.setValue(
              SecureStorageKey.web3WalletMnemonic,
              mnemonic
            )
          } else {
            this.presentToast(this._Translate.instant('IMPORTKEY.invalid-mnemonic'));
          }
        }
      } else {
        if(checker > -1) {
          this.presentToast(this._Translate.instant("WALLETSETTINGS.errors.address-exists"));
        } else {
          var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
          var random = Math.floor(Math.random() * colorschemes.length);
          var color = colorschemes[random];
          ima.push({
            heading,
            publicKey,
            mnemonic,
            creationDate: +new Date(),
            color
          });
          await this._SecureStorageService.setValue(
            SecureStorageKey.importedWallets,
            JSON.stringify(ima)
          );
        }
      }

      this.populateWallets();

    })
  }

  async traceAccount() {
    // console.log("traceAccount");
    const alerty = await this._AlertController.create({
      mode: 'ios',
      header:
        this._Translate.instant('WALLET.monitoring-wallet-header'),
      inputs: [
        { type: "text", placeholder: "0x..", name: "publicKey" },
        { type: "text", placeholder: "Name", name: "name" },
      ],
      buttons: [
        {
          text: this._Translate.instant("BUTTON.CANCEL"),
          role: "cancel",
          handler: () => {},
        },
        {
          text: this._Translate.instant("GENERAL.ok"),
          handler: async (data) => {
            var address = data.publicKey;

            var publicKeys = Array.from(this.accounts, (ac) => ac.publicKey);
            var headings = Array.from(this.accounts, (ac) => ac.heading);
            // console.log(publicKeys);

            if (!data.name || data.name == "") {
              this.presentToast(
                this._Translate.instant("WALLETSETTINGS.errors.empty-string")
              );
              return;
            }

            if (headings.includes(data.name)) {
              this.presentToast(
                this._Translate.instant("WALLETSETTINGS.errors.name-used")
              );
              return;
            }

            if (publicKeys.includes(data.publicKey)) {
              this.presentToast(
                this._Translate.instant("WALLETSETTINGS.errors.address-exists")
              );
              return;
            }

            if (!ethers.utils.isAddress(data.publicKey)) {
              if (data.publicKey.indexOf(".eth") > 0) {
                var ether = environment.production ? EthereumNetworks.MAINNET : EthereumNetworks.SEPOLIA;
                var provider = new ethers.providers.EtherscanProvider(ether, await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY));
                address = await provider.resolveName(data.publicKey);
                if(!address) {
                  this.presentToast(
                    this._Translate.instant("WALLETSETTINGS.errors.invalid-address")
                  );
                  return;
                }
                console.log({ name: data.publicKey, address });
                this.presentToast(`ENS: ${data.publicKey}, Address: ${address}`);
                // return;
              } else {
                this.presentToast(
                  this._Translate.instant("WALLETSETTINGS.errors.invalid-address")
                );
                return;
              }

            }

            if (data.name.trim().length > 20) {
              this.presentToast(
                this._Translate.instant("WALLETSETTINGS.errors.invalid-length")
              );
              return;
            }

            var im = await this._SecureStorageService.getValue(
              SecureStorageKey.importedWallets,
              false
            );
            var imW = !!im ? JSON.parse(im) : [];

            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];

            imW.push({
              heading: data.name.trim(),
              publicKey: address.trim(),
              icon: `data:image/png;base64,${new Identicon(
                address.trim(),
                420
              ).toString()}`,
              creationDate: +new Date(),
              mnemonic: null,
              color
            });

            await this._SecureStorageService.setValue(
              SecureStorageKey.importedWallets,
              JSON.stringify(imW)
            );

            await this._UserActivities.updateActivity(ActivityKeys.WALLET_FOLLOWED, { address: this._ApiProviderService.obtainDisplayableAddress(address.trim()) })

            this.populateWallets();
          },
        },
      ],
    });

    await alerty.present();
  }

  async showAccount(account) {
    // await this._LoaderProviderService.loaderCreate();
    // this.displayLoader = true;
    this._NavController.navigateForward(
      `/dashboard/payments/crypto-account-page?data=${encodeURIComponent(
        JSON.stringify({
          account: {
            heading: account.heading,
            primary: !!account.primary,
            publicKey: account.publicKey,
          },
        })
      )}`
    );
    // await this._LoaderProviderService.loaderDismiss();
    // this.
  }

  goToSignUp() {
    this._NavController.navigateRoot('/sign-up/step-9');
  }

  goToLogin() {
    this._NavController.navigateRoot('/login?from=browsemode');
  }

  trackNfts(address: string, name: string) {
    this._NavController.navigateForward(
      `/dashboard/nft?mode=nft&trackAddress=${address}&trackName=${name}`
    );
  }

  encryptMnemonic(mnemonic: string) {
    try {
      var encrypted = CryptoJS.AES.encrypt(
        mnemonic,
        UDIDNonce.energy
      ).toString();
    } catch (e) {
      encrypted = CryptoJS.AES.encrypt(
        (mnemonic as any).phrase,
        UDIDNonce.energy
      ).toString();
    }

    return encrypted;
  }

  displayPictureOptions() {
    this._ImageSelectionService.showChangePicture().then((photo) => {
      this.profilePictureSrc = !!photo
        ? photo
        : "../../assets/job-user.svg";
      this.populateWallets();
    });
  }

  showCryptoBalance() {
    console.log(this.cryptoBalance);
  }

  calculateNFTSum(data) {
    let sum = 0;
    Object.keys(data).forEach((k) => (sum += data[k]));
    return sum;
  }

  copyAddress(data: string) {
    Clipboard.write({ string: data }).then(() => {
      this.presentToast(
        this._Translate.instant("WALLET.addressCopied")
      );
    })
  }

  goToCelebrity() {
    this._NavController.navigateForward('/celebrity-nfts');
  }

  goToNews() {
    this._NavController.navigateForward('/grace');
  }

  goToTHXC() {
    this._NavController.navigateForward('/dashboard/nft?mode=loyalty');
  }

  goToWeb3Apps() {
    this._NavController.navigateForward('/dashboard/marketplace/crypto?source=%2Fdashboard%2Fpayments');
  }


  trackByAccounts(index, item) {
    return item.publicKey;
  }

}
