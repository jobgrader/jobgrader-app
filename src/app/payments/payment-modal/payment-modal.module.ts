import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { PaymentModalComponent } from './payment-modal.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
    ],
  providers: [ ],
  declarations: [PaymentModalComponent],
//   entryComponents: [PaymentModalComponent],
  exports: [PaymentModalComponent]
})
export class PaymentModalModule {}
