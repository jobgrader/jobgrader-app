import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NetworkAccountDetailsPage } from './network-account-details.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { TransactionDetailModule } from '../transaction-detail/transaction-detail.module';
import { TranslateModule } from '@ngx-translate/core';
import { SendCryptoModule } from '../crypto-account-page/send-crypto/send-crypto.module';
import { ReceiveCryptoModule } from '../crypto-account-page/receive-crypto/receive-crypto.module';
import { BuyCryptoModule } from '../crypto-account-page/buy-crypto/buy-crypto.module';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: NetworkAccountDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TransactionDetailModule,
    SendCryptoModule,
    ReceiveCryptoModule,
    BuyCryptoModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  providers: [SafariViewController],
  declarations: [NetworkAccountDetailsPage]
})
export class NetworkAccountDetailsPageModule {}
