import { Component } from "@angular/core";
import { Router } from "@angular/router";
import {
  ModalController,
  NavController,
  Platform,
  ToastController,
} from "@ionic/angular";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import Identicon from "identicon.js";
import { environment } from "src/environments/environment";
import {
  CryptoCurrency,
  EthereumNetworks,
} from "src/app/core/providers/wallet-connect/constants";
import { ethers } from "ethers";
import { TransactionDetailComponent } from "../transaction-detail/transaction-detail.component";
import { TranslateProviderService } from "src/app/core/providers/translate/translate-provider.service";
import { ReceiveCryptoComponent } from "../crypto-account-page/receive-crypto/receive-crypto.component";
import { SendCryptoComponent } from "../crypto-account-page/send-crypto/send-crypto.component";
import { SafariViewController } from "@awesome-cordova-plugins/safari-view-controller/ngx";
import { InAppBrowser } from "@awesome-cordova-plugins/in-app-browser/ngx";
import { Clipboard } from '@capacitor/clipboard';
import { ChartService } from "../crypto-account-page/services/chart.service";
import { ThemeSwitcherService } from "src/app/core/providers/theme/theme-switcher.service";
import { VaultSecretKeys, VaultService } from "src/app/core/providers/vault/vault.service";
import { fiatCurrencies } from "src/app/core/providers/nft/fiat-currencies";
import { MeldService } from "src/app/core/providers/meld/meld.service";
import { UserPhotoServiceAkita } from "../../core/providers/state/user-photo/user-photo.service";
const abi = require('../crypto-account-page/abi.json');

@Component({
  selector: "app-network-account-details",
  templateUrl: "./network-account-details.page.html",
  styleUrls: ["./network-account-details.page.scss"],
})
export class NetworkAccountDetailsPage {
  public data: any;
  public mnemonic: string = null;
  public history = [];
  public chartData = [
    {
      count: 0,
      size: 50,
      sum: 0,
    },
    {
      count: 0,
      size: 40,
      sum: 0,
    },
  ];

  public profilePictureSrc =
    "../../../assets/job-user.svg";
  public icon;
  public subHeading = "";
  private browser;

  private DEFAULT_FIAT = "EUR";
  private FIAT_SYMBOL = "€";

  public currencySet = [
    CryptoCurrency.MATIC,
    CryptoCurrency.EVE,
    CryptoCurrency.ETH,
    CryptoCurrency.GNOSIS,
  ];
  browserOptions =
    "zoom=no,footer=no,hideurlbar=yes,footercolor=#0021FF,hidenavigationbuttons=yes,presentationstyle=pagesheet";

  constructor(

    private _Router: Router,
    private _NavController: NavController,
    private _SecureStorageService: SecureStorageService,
    private _ModalController: ModalController,
    private _Translate: TranslateProviderService,
    private _ToastController: ToastController,
    private _SafariViewController: SafariViewController,
    private _InAppBrowser: InAppBrowser,
    private _Platform: Platform,
    private chartService: ChartService,
    private _Vault: VaultService,
    private _Meld: MeldService,
    public _Theme: ThemeSwitcherService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
    this.data = this._Router.parseUrl(this._Router.url).queryParams;
  }

  async ionViewWillEnter() {
    console.log(this.data);
    this.icon = `../../../assets/crypto/vector/${this.data.currency}.svg`;
    if (this.data.primary == "true") {
      const photo = this.userPhotoServiceAkita.getPhoto();
      if(photo) {
        this.profilePictureSrc = !!photo ? photo : this.profilePictureSrc;
      }

      this._SecureStorageService
        .getValue(SecureStorageKey.web3WalletMnemonic, false)
        .then((web3WalletMnemonic) => {
          this.mnemonic = !!web3WalletMnemonic ? web3WalletMnemonic : null;
        });
    } else {
      this.profilePictureSrc = `data:image/png;base64,${new Identicon(
        this.data.address,
        420
      ).toString()}`;
      this._SecureStorageService
        .getValue(SecureStorageKey.importedWallets, false)
        .then((im) => {
          var imW = !!im ? JSON.parse(im) : [];
          var check = imW.find((k) => k.publicKey == this.data.address);
          this.mnemonic = !!check
            ? !!check.mnemonic
              ? check.mnemonic
              : null
            : null;
        });
    }

    var currency = await this._SecureStorageService.getValue(
      SecureStorageKey.currency,
      false
    );

    this.DEFAULT_FIAT = !!currency ? currency : this.DEFAULT_FIAT;

    this.FIAT_SYMBOL = fiatCurrencies.find(fi => fi.currency_code == this.DEFAULT_FIAT).currency_symbol;

    const CRYPTONETWORKS_EVAN = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_EVAN);
    const CRYPTONETWORKS_EVAN_TEST = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_EVAN_TEST);
    const CRYPTONETWORKS_POLYGON = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON);
    const CRYPTONETWORKS_POLYGON_TEST_MUMBAI = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON_TEST_MUMBAI);
    const CRYPTONETWORKS_GNOSIS = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_GNOSIS);
    const CRYPTONETWORKS_ETHEREUM = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_ETHEREUM);
    const CRYPTONETWORKS_ETHEREUM_TEST_RINKEBY = await this._Vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_ETHEREUM_TEST_RINKEBY);

    switch (this.data.currency) {
      case CryptoCurrency.ETH: {
        var provider: any = EthereumNetworks.MAINNET;
        var network = <ethers.providers.EtherscanProvider>(
          new ethers.providers.EtherscanProvider(
            provider,
            await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
          )
        );
        this.populateEthHistory(network);
        break;
      }
      case CryptoCurrency.EVE: {
        provider = environment.production
          ? CRYPTONETWORKS_EVAN
          : CRYPTONETWORKS_EVAN_TEST;
        var networkAlt = <ethers.providers.BaseProvider>(
          ethers.getDefaultProvider(provider)
        );
        this.populateHistory(networkAlt);
        break;
      }
      case CryptoCurrency.MATIC: {
        provider = environment.production
          ? CRYPTONETWORKS_POLYGON
          : CRYPTONETWORKS_POLYGON_TEST_MUMBAI;
        networkAlt = <ethers.providers.BaseProvider>(
          ethers.getDefaultProvider(provider)
        );
        this.populateHistory(networkAlt);
        break;
      }
      case CryptoCurrency.GNOSIS: {
        provider = environment.production
          ? CRYPTONETWORKS_GNOSIS
          : CRYPTONETWORKS_GNOSIS;
        networkAlt = <ethers.providers.BaseProvider>(
          ethers.getDefaultProvider(provider)
        );
        this.populateHistory(networkAlt);
        break;
      }
      default: {
        provider = environment.production
          ? CRYPTONETWORKS_ETHEREUM
          : CRYPTONETWORKS_ETHEREUM_TEST_RINKEBY;
        network = <ethers.providers.EtherscanProvider>(
          new ethers.providers.EtherscanProvider(
            EthereumNetworks.MAINNET,
            await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
          )
        );
        this.populateEthHistory(network);
        break;
      }
    }
  }

  populateEthHistory(network: ethers.providers.EtherscanProvider) {
    this.history = [];
    network.getHistory(this.data.address).then((history) => {
      this.populate(history);
    });
  }

  populateHistory(network: ethers.providers.BaseProvider) {
    console.log(network);

    // network.getHistory(this.data.address).then(history => {
    //   this.populate(history);
    // })
  }

  // ChartData[0] => buy
  // chartData[1] => sell
  populate(history: any) {
    this.history = history;
    console.log(this.history);

    for (let i = 0; i < this.history.length; i++) {
      var hh = this.history[i];
      hh.timestamp = hh.timestamp * 1000;
      console.log(hh.value._hex);
      if (hh.to == this.data.address) {
        this.chartData[0].count =
          this.chartData[0].count +
          Math.round(
            Number(
              ethers.utils.formatEther(ethers.BigNumber.from(hh.value._hex))
            ) * 10000000
          ) /
            10000000;
      } else if (hh.from == this.data.address) {
        this.chartData[1].count =
          this.chartData[1].count +
          Math.round(
            Number(
              ethers.utils.formatEther(ethers.BigNumber.from(hh.value._hex))
            ) * 10000000
          ) /
            10000000;
      }
    }

    const compare = (a, b) => {
      if (a.timestamp > b.timestamp) {
        return -1;
      }
      if (a.timestamp < b.timestamp) {
        return 1;
      }
      return 0;
    };
    this.history.sort(compare);
    console.log(this.chartData);

    this._Translate.getLangFromStorage().then((lang) => {
      this.subHeading =
        lang == "de"
          ? `Anzeige der letzten ${this.history.length} Transaktion(en)`
          : `Showing details of the last ${this.history.length} transaction(s)`;
    });

    var sum = Array.from(this.chartData, (ss) => ss.count).reduce(
      (a, b) => a + b
    );
    // if (sum > 0) {
    console.log("FINAL BALANCE", this.chartData);
    this.d3Chart(this.chartData);
    // }
  }

  showHistory(h: any) {
    console.log(h);
    // return;
    this._ModalController
      .create({
        component: TransactionDetailComponent,
        componentProps: {
          data: h,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {});
        modal.present();
      });
  }

  close() {
    // this._NavController.back();
    this._NavController.navigateBack(`/dashboard/payments/crypto-account-page?data=${encodeURIComponent(
      JSON.stringify({
        account: {
          heading: this.data.heading,
          primary: this.data.primary,
          publicKey: this.data.address,
        },
      })
    )}`);
  }

  private async d3Chart(dataset: any) {
    let chartData = [
      // {
      //   color: "#8FA2F4",
      //   title: "Stake",
      //   size: 20,
      //   count: 10,
      //   sum: 10,
      //   xfactor: 15,
      //   yfactor: 10,
      //   ethFactor: 3,
      //   eurFactor: 18,
      //   labelXFactor: -40,
      //   ethBalance: 0,
      //   eurBalance: "€0",
      //   shadowStrokeWidth: 2,
      //   shadowColor: "#6F7FC5",
      // },
      {
        color: "#0A1A95",
        title: this._Translate.instant('TRANSACTIONRECEIPT.expense'),
        size: 40,
        count: dataset[1].count,
        sum: dataset[1].sum,
        xfactor: 0,
        yfactor: 20,
        ethFactor: 10,
        eurFactor: 55,
        labelXFactor: 0,
        ethBalance: dataset[1].count.toFixed(10),
        eurBalance: await this.chartService.getConversion(
          "ETH",
          this.DEFAULT_FIAT,
          dataset[1].count
        ),
        shadowStrokeWidth: 3,
        shadowColor: "#0A1A95",
      },
      {
        color: "#0B22D9",
        title: this._Translate.instant('TRANSACTIONRECEIPT.income'),
        size: 45,
        count: dataset[0].count,
        sum: dataset[0].sum,
        xfactor: 70,
        yfactor: 0,
        eurFactor: 0,
        ethFactor: 0,
        labelXFactor: 0,
        ethBalance: dataset[0].count.toFixed(10),
        eurBalance: await this.chartService.getConversion(
          "ETH",
          this.DEFAULT_FIAT,
          dataset[0].count
        ),
        shadowStrokeWidth: 3,
        shadowColor: "#0A1CBA",
      },
      // {
      //   color: "#536AEC",
      //   title: "Lend",
      //   size: 25,
      //   count: 20,
      //   sum: 30,
      //   xfactor: 10,
      //   yfactor: 0,
      //   ethFactor: 0,
      //   eurFactor: -10,
      //   labelXFactor: -7,
      //   ethBalance: 0,
      //   eurBalance: "€0",
      //   shadowStrokeWidth: 2,
      //   shadowColor: "#6774C1",
      // },
    ];
    dataset = [...chartData];

    var d3 = (window as any).d3;
    var color = Array.from(dataset, (k) => (k as any).color);

    d3.select("#chart-detail").selectAll("*").remove();

    let width = d3.select("#chart-detail").node().getBoundingClientRect().width;

    let height = 300;
    let donutWidth = 60;
    let innerRadius = Math.min(width, height) / 2 - donutWidth;

    var shadowWidth=10;

    var outerRadiusArcShadow=innerRadius+1;
    var innerRadiusArcShadow=innerRadius-shadowWidth;

    let svg = d3
      .select("#chart-detail")
      .append("svg")
      .attr("width", width)
      .attr("height", height + 50)
      .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    let arcGen = d3
      .arc()
      .innerRadius(innerRadius)
      .outerRadius((d) => d.data.size + innerRadius);
    let angleGen = d3
      .pie()
      .value((d) => d.count.toString())
      .sortValues((a, b) => (a < b ? 1 : -1));

    let data = angleGen(dataset);



    var createChart=function(svg,outerRadius,innerRadius,fillFunction,className){

      var arc=d3.svg.arc()
              .innerRadius(innerRadius)
              .outerRadius(outerRadius);

      var path= svg
              .selectAll('.'+className)
              .data(data)
              .enter()
              .append("path")
              .attr("class", className)
              .attr("d", arcGen)
              .attr("fill", fillFunction)
              .attr("stroke", (d, i) => d.data.shadowColor)
              .attr("stroke-width", (d, i) => d.data.shadowStrokeWidth);

        path.transition()
                .duration(1000)
                .attrTween('d', function(d) {
                    var interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, d);
                    return function(t) {
                        return arc(interpolate(t));
                    };
                });


      var chart={ path:path, arc:arc };

      return chart;
  };

    var mainChart = createChart(svg,
      // innerRadius + (donutWidth / 2),
      function(d) {
        return innerRadius + d.data.size
      },
      innerRadius,function(d,i){
          return color[i];
      },'path1');

    var shadowChart = createChart(svg,
      outerRadiusArcShadow,
      innerRadiusArcShadow,

    function(d,i){
      var c=d3.hsl(color[i]);
      return d3.hsl((c.h+5), (c.s -.07), (c.l -.15));
    },'path2');

      svg
      .selectAll("labels")
      .data(data)
      .enter()
      .append("text")
      .attr("text-anchor", "middle")
      .text((d) => {
        if(Number(d.data.count) == 0) {
          return '';
        }
        return `${d.data.title}: ${d.data.eurBalance}`;
      })
      .attr("font-size", "12px")
      .attr("fill", "white")
      .attr("transform", (d) => {
        return (
          "translate(" +
          (innerRadius + (d.data.size + d.data.labelXFactor) / 1.5) *
            Math.sin((d.endAngle - d.startAngle) / 2 + d.startAngle) +
          ", " +
          -1 *
            (innerRadius + d.data.yfactor) *
            Math.cos((d.endAngle - d.startAngle) / 2 + d.startAngle) +
          ")"
        );
      });


    // svg
    //   .selectAll("balances-eth")
    //   .data(data)
    //   .enter()
    //   .append("text")
    //   .attr("text-anchor", "middle")
    //   .text((d) => {
    //     return d.data.ethBalance + ` ETH`;
    //   })
    //   .attr("font-size", "12px")
    //   .attr("fill", this._Theme.getCurrentTheme() == "dark" ? "white" : "black")
    //   .attr("transform", (d) => {
    //     return (
    //       "translate(" +
    //       (innerRadius + (d.data.xfactor + 50)) *
    //         Math.sin((d.endAngle - d.startAngle) / 2 + d.startAngle) +
    //       ", " +
    //       -1 *
    //         (innerRadius + 48 + d.data.ethFactor) *
    //         Math.cos((d.endAngle - d.startAngle) / 2 + d.startAngle) +
    //       ")"
    //     );
    //   });


    // svg
    //   .selectAll("balances-eur")
    //   .data(data)
    //   .enter()
    //   .append("text")
    //   .attr("text-anchor", "middle")
    //   .text((d) => {
    //     return d.data.eurBalance;
    //   })
    //   .attr("font-size", "10px")
    //   .attr("fill", this._Theme.getCurrentTheme() == "dark" ? "gray" : "black")
    //   .attr("transform", (d) => {
    //     if (d.data.title === 'Lend') {
    //       return (
    //         "translate(" +
    //         (innerRadius + (d.data.xfactor + 50)) *
    //           Math.sin((d.endAngle - d.startAngle) / 2 + d.startAngle) +
    //         ", " +
    //         11+
    //         ")"
    //       );
    //     } else {
    //       return (
    //         "translate(" +
    //         (innerRadius + (d.data.xfactor + 50)) *
    //           Math.sin((d.endAngle - d.startAngle) / 2 + d.startAngle) +
    //         ", " +
    //         -1 *
    //           (innerRadius + 16 + d.data.eurFactor) *
    //           Math.cos((d.endAngle - d.startAngle) / 2 + d.startAngle) +
    //         ")"
    //       );
    //     }

    //   });


    svg
      .append("text")
      .attr("text-anchor", "middle")
      .attr("x", "0")
      .attr("y", "0")
      .attr("id", "update-text")

      .attr("font-size", "12px")
      .append("tspan")
      .attr("x", "0")
      .attr("y", (-4.2).toString() + "em")
      .attr("fill", this._Theme.getCurrentTheme() == 'dark'  ? "white" : "black")
      .text(`tap to update`);

    svg
      .append("text")
      .attr("text-anchor", "middle")
      .attr("x", "0")
      .attr("y", "0")
      .attr("font-size", "14px")
      .attr("font-weight", 800)
      .append("tspan")
      .attr("x", "0")
      .attr("y", (1.2 * 0).toString() + "em")
      .attr("fill", this._Theme.getCurrentTheme() == 'dark'  ? "white" : "black")
      .text(`+ ${this.chartData[0].count.toFixed(10)} ${this.data.currency}` );

    svg
      .append("text")
      .attr("text-anchor", "middle")
      .attr("x", "0")
      .attr("y", "0")
      .attr("font-size", "14px")
      .attr("font-weight", "400")
      .append("tspan")
      .attr("x", "0")
      .attr("y", (1.2 * 1).toString() + "em")
      .attr("fill", this._Theme.getCurrentTheme() == 'dark'  ? "white" : "black")
      .text(`- ${this.chartData[1].count.toFixed(10)} ${this.data.currency}`);


    document.getElementById(`update-text`).onclick = async (event) => {
      this.populateEthHistory(new ethers.providers.EtherscanProvider(
        EthereumNetworks.MAINNET,
        await this._Vault.getSecret(VaultSecretKeys.ETHERSCAN_API_KEY)
      ));
    };
  }

  async Buy() {
    var url = await this._Meld.getApiURL(
      this.data.address
    );

    if (this._Platform.is("ios") && this._Platform.is("hybrid")) {
      this.showSafariInstance(url);
    } else if (this._Platform.is("android") && this._Platform.is("hybrid")) {
      this.browser = this._InAppBrowser.create(
        url,
        "_system",
        this.browserOptions
      );
    } else if (!this._Platform.is("hybrid")) {
      this.browser = this._InAppBrowser.create(url, "_system");
    }
  }

  Send() {
    this._ModalController
      .create({
        component: SendCryptoComponent,
        componentProps: {
          data: this.data,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {
          // this.setupConnectedAccounts(this.data.account.publicKey);
        });
        modal.present();
      });
  }

  Receive() {
    this._ModalController
      .create({
        component: ReceiveCryptoComponent,
        componentProps: {
          data: this.data.address,
        },
      })
      .then((modal) => {
        modal.onDidDismiss().then(() => {
          // this.setupConnectedAccounts(this.data.account.publicKey);
        });
        modal.present();
      });
  }

  Swap() {
    console.log("swap()");
    this.presentToast(this._Translate.instant("CONTACT.title"));
  }

  presentToast(message: string) {
    this._ToastController
      .create({
        message,
        position: "top",
        duration: 2000,
      })
      .then((toast) => {
        toast.present();
      });
  }

  showSafariInstance(url) {
    this._SafariViewController.isAvailable().then((available: boolean) => {
      if (available) {
        this._SafariViewController
          .show({
            url,
            hidden: false,
            animated: true,
            transition: "slide",
            enterReaderModeIfAvailable: false,
            tintColor: "#0021FF",
          })
          .subscribe(
            (result: any) => {},
            (error: any) => console.error(error)
          );
      } else {
        this._InAppBrowser.create(url, "_self", this.browserOptions);
      }
    });
  }

  displayEth(v) {
    if(!v) {
      return '';
    }
    if(!!v._hex) {
      return `${Number(ethers.utils.formatEther(ethers.BigNumber.from(v._hex))).toFixed(5)} ETH`;
    }
    return `${Number(ethers.utils.formatEther(ethers.BigNumber.from(v.hex))).toFixed(5)} ETH`;
  }

  copyAddress() {
    console.log(this.data.account.publicKey);
    Clipboard.write({ string: this.data.account.publicKey }).then(() => {
      this.presentToast(this._Translate.instant("WALLET.addressCopied"));
    })
  }
}
