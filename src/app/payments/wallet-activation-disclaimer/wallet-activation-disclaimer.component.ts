import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-wallet-activation-disclaimer',
  templateUrl: './wallet-activation-disclaimer.component.html',
  styleUrls: ['./wallet-activation-disclaimer.component.scss'],
})
export class WalletActivationDisclaimerComponent  implements OnInit {

  constructor(
    private _ModalController: ModalController
  ) { }

  ngOnInit() {}

  close(value: boolean) {
    this._ModalController.dismiss({ value });
  }

}
