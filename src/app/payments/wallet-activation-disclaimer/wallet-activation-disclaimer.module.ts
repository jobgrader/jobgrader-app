import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { WalletActivationDisclaimerComponent } from './wallet-activation-disclaimer.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  declarations: [WalletActivationDisclaimerComponent],
//   entryComponents: [WalletImportDisclaimerComponent],
  exports: [WalletActivationDisclaimerComponent]
})
export class WalletActivatioonDisclaimerModule {}
