import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { AlertController, ModalController, NavController, PopoverController, ToastController } from '@ionic/angular';
import { ethers, Wallet } from 'ethers';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { BarcodeService } from 'src/app/core/providers/barcode/barcode.service';
import { LoaderProviderService } from 'src/app/core/providers/loader/loader-provider.service';
import { NftService } from 'src/app/core/providers/nft/nft.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { CryptoCurrency } from 'src/app/core/providers/wallet-connect/constants';
import { DidPopoverComponent } from 'src/app/merchant-details/did-popover/did-popover.component';
import { environment } from 'src/environments/environment';
import { TransactionConfirmationComponent } from './confirmation/confirmation.component';
import { GasPriceComponent } from './gas-price/gas-price.component';
import { GasService } from 'src/app/core/providers/gas/gas.service';
import { fiatCurrencies } from 'src/app/core/providers/nft/fiat-currencies';
import { uniq, uniqBy } from 'lodash-es';
import { CryptoCurrencyService, CurrencyToNameMapper } from '../services/crypto-currency.service';
import { eth, Web3 } from 'web3';

@Component({
  selector: 'app-send-crypto',
  templateUrl: './send-crypto.component.html',
  styleUrls: ['./send-crypto.component.scss'],
})
export class SendCryptoComponent implements OnInit {
  @Input() data: any;

  @HostBinding('class.hidden') hidden: boolean = false;

  private DEFAULT_FIAT = "EUR";
  public FIAT_SYMBOL = "€";

  public form = new UntypedFormGroup({
    to: new UntypedFormControl('',[]),
    amount: new UntypedFormControl('',[]),
    currency: new UntypedFormControl('',[]),
  });

  public currencySet;
  public cryptoBalance;
  public requestsSent = [ ];
  public receiptData = {
    gas: "0",
    gasConvert: "0",
    value: "0",
    valueConvert: "0",
    total: "0",
    totalConvert: "0"
  }

  private gasPrices;
  private gasIndex = "fast";
  private conversionEur;

  private blockDefault = false; // etherscan, polygonscan

  constructor(
    private _ModalController: ModalController,
    private _BarcodeService: BarcodeService,
    private _Translate: TranslateProviderService,
    private _SecureStorageService: SecureStorageService,
    private _ApiProviderService: ApiProviderService,
    private _LoaderProviderService: LoaderProviderService,
    private _AlertController: AlertController,
    private _NftService: NftService,
    private _CryptoCurrencyService: CryptoCurrencyService,
    private _Gas: GasService,
    private _PopoverController: PopoverController,
    private _NavController: NavController,
    private _ToastController: ToastController
  ) { }

  async ngOnInit() {

    var wallet_currencies = await this._CryptoCurrencyService.getWalletCurrencies(this.data.account.publicKey);
    var wallet_currencies_data = wallet_currencies.currencies_data;
    this.currencySet = uniqBy(wallet_currencies_data.filter(we => !!we.active), 'currency') ;

    var cryptoBalanceString = await this._SecureStorageService.getValue(SecureStorageKey.cryptoBalance, false);
    this.cryptoBalance = !!cryptoBalanceString ? JSON.parse(cryptoBalanceString): {};

    var currency = await this._SecureStorageService.getValue(SecureStorageKey.currency, false);
    this.DEFAULT_FIAT = !!currency ? currency : this.DEFAULT_FIAT;
    this.FIAT_SYMBOL = fiatCurrencies.find(fi => fi.currency_code == this.DEFAULT_FIAT).currency_symbol;

    // console.log("await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.ETH)",         await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.ETH)          );
    // console.log("await this._Gas.returnScanGasPrice(CryptoCurrency.ETH)",                       await this._Gas.returnScanGasPrice(CryptoCurrency.ETH)                        );
    // console.log("await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.MATIC)",       await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.MATIC)        );
    // console.log("await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC)",                     await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC)                      );
    // console.log("await this._Gas.getGasPriceEstimationEVE()",                                   await this._Gas.getGasPriceEstimationEVE()                                    );
    // console.log("await this._Gas.getGasPriceEstimationXDAI()",                                  await this._Gas.getGasPriceEstimationXDAI()                                   );
    // console.log("await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.OP)",          await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.OP)           );
    // console.log("await this._Gas.returnScanGasPrice(CryptoCurrency.OP)",                        await this._Gas.returnScanGasPrice(CryptoCurrency.OP)                         );
    // console.log("await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.HMT_POLYGON)", await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.HMT_POLYGON)  );
    // console.log("await this._Gas.returnScanGasPrice(CryptoCurrency.HMT_POLYGON)",               await this._Gas.returnScanGasPrice(CryptoCurrency.HMT_POLYGON)                );
    // console.log("await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.HMT_SKALE)",   await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.HMT_SKALE)    );
    // console.log("await this._Gas.returnScanGasPrice(CryptoCurrency.HMT_SKALE)",                 await this._Gas.returnScanGasPrice(CryptoCurrency.HMT_SKALE)                  );


    this.gasPrices = {
      [CryptoCurrency.ETH] : (this.blockDefault ? await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.ETH) : await this._Gas.returnScanGasPrice(CryptoCurrency.ETH)),
      [CryptoCurrency.MATIC] : (this.blockDefault ? await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.MATIC) : await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC)),
      [CryptoCurrency.EVE] : await this._Gas.getGasPriceEstimationEVE(),
      [CryptoCurrency.GNOSIS] : await this._Gas.getGasPriceEstimationXDAI(),
      [CryptoCurrency.OP]: (this.blockDefault ? await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.ETH) : await this._Gas.returnScanGasPrice(CryptoCurrency.ETH)),
      [CryptoCurrency.HMT_POLYGON]: (this.blockDefault ? await this._Gas.getGasPriceEstimationBlockNative(CryptoCurrency.MATIC) : await this._Gas.returnScanGasPrice(CryptoCurrency.MATIC)),
      [CryptoCurrency.HMT_SKALE]: { unit: "gwei", normal: "0", fast: "0" },
    }
    this.conversionEur = {
      [CryptoCurrency.ETH] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.ETH, this.DEFAULT_FIAT),
      [CryptoCurrency.MATIC] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.MATIC, this.DEFAULT_FIAT),
      [CryptoCurrency.EVE] : 1,
      [CryptoCurrency.GNOSIS] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.GNOSIS, this.DEFAULT_FIAT),
      [CryptoCurrency.OP] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.OP, this.DEFAULT_FIAT),
      [CryptoCurrency.HMT_POLYGON] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.HMT_POLYGON, this.DEFAULT_FIAT),
      [CryptoCurrency.HMT_SKALE] : await this._ApiProviderService.cryptoConversion(CryptoCurrency.HMT_SKALE, this.DEFAULT_FIAT),
    }
    console.log(this.gasPrices);
    console.log(this.conversionEur);
    var cryptoPaymentRequestsSent = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentRequestsSent, false);
    this.requestsSent = !!cryptoPaymentRequestsSent ? JSON.parse(cryptoPaymentRequestsSent) : this.requestsSent;
  }

  processIconURL(icon: string) {
    return icon.replace("../../../../../","../../../../");
  }

  processReceipt() {

    console.log(this.gasPrices);

    if(this.form.value.currency) {
      this.receiptData.value = this.form.value.amount;
      this.receiptData.gas = Number(ethers.utils.formatEther(ethers.utils.parseUnits(this.gasPrices[this.form.value.currency][this.gasIndex], this.gasPrices[this.form.value.currency]["unit"])._hex)).toFixed(10);
      this.receiptData.valueConvert = (this.conversionEur[this.form.value.currency] * Number(this.receiptData.value)).toFixed(2);
      this.receiptData.gasConvert = (this.conversionEur[this.form.value.currency] * Number(this.receiptData.gas)).toFixed(2);
      this.receiptData.total = (Number(this.receiptData.value) + Number(this.receiptData.gas)).toFixed(10);
      this.receiptData.totalConvert = (Number(this.receiptData.valueConvert) + Number(this.receiptData.gasConvert)).toFixed(2);
    }

    console.log(this.receiptData);

  }

  close() {
    this._ModalController.dismiss();
  }

  presentToast(message) {
    this._ToastController.create({
      message,
      duration: 2000,
      position: 'bottom'
    }).then(toast => toast.present())
  }

  scanAddress() {
    this.hidden = true;

    this._BarcodeService.scanAddress().then(r => {
      this.hidden = false;

      // console.log(r);
      if(r.slice(0,2) != "0x") {
        try {
          var uri = new URL(r);
          var to = uri.pathname;
          var amount = ethers.utils.formatEther( ethers.utils.parseUnits(Number(uri.searchParams.get('value')).toString(), "wei") );
          this.form.controls.to.setValue(to);
          this.form.controls.amount.setValue(amount);
        } catch(e) {
          console.log(e);
        }
      } else {
        this.form.controls.to.setValue(r);
      }
    }).catch(e => {
      this.hidden = false;

      // mburger: the catch was not used previously, so now we reject the service
      // if user clicks cancel button on the barcode scanner
      // for this reason we even have not to show the messages here

      // this.presentToast(this._Translate.instant('WALLET.invalidQRCodeScanned'));
      // this.form.reset();

    })
  }

  showContentOptions(currencySelected) {
    // var cont = this.data.account.network.find(k => k.currency == currencySelected);
    var fincont = `${currencySelected.currency} (${this._Translate.instant('WALLET.supply')} ${this.cryptoBalance[this.data.account.publicKey][currencySelected.currency]})`;
    return fincont;
  }


  showGasInfoAlert(){
    this._PopoverController.create({
        component: DidPopoverComponent,
        componentProps: {
            data: this._Translate.instant('WEB3WALLETMODAL.tooltip')
        },
        cssClass: 'did-popover',
        showBackdrop: true,
        translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 5000)
    })
  }

  changeGasPrice() {
    this._PopoverController.create({
      component: GasPriceComponent,
      componentProps: {
          displayInformation: this.receiptData,
          gasArray: [CryptoCurrency.HMT_POLYGON, CryptoCurrency.HMT_SKALE].includes(this.form.value.currency) ? this.gasPrices["HMT"] : this.gasPrices[this.form.value.currency],
          conversion: this.conversionEur[this.form.value.currency],
          currency: this.form.value.currency,
          symbol: this.FIAT_SYMBOL
      },
      cssClass: 'send-popover',
      showBackdrop: true,
      translucent: true
    }).then((popover) => {
        popover.onDidDismiss().then(data => {
          console.log(data);
          if(data.data.value) {
            this.gasIndex = data.data.value;
            this.processReceipt();
          }
        })
        popover.present();
    })
  }

  async processPayment() {

    var finalUsername;

    var selfUsername = await this._SecureStorageService.getValue(SecureStorageKey.userName, false);

    if(this.form.value.to == selfUsername) {
      return this.presentToast(this._Translate.instant('WALLET.useDifferentUsername'));
    }

    if(this.form.value.to.trim().slice(0,2) !="0x") {
      var targetAddress = await this._ApiProviderService.getEthAddressFromUsername(this.form.value.to);
      if(!targetAddress) {
        return this.presentToast(this._Translate.instant('WALLET.userEthereumDoesNotExist'));
      }
      finalUsername = this.form.value.to;
    }

    if(this.form.value.to.trim().slice(0,2) =="0x") {
      var finalUsername = await this._ApiProviderService.getUsernameFromEthAddress(this.form.value.to);
      // if(!finalUsername) {
      //   this.presentToast(this._Translate.instant('WALLET.userDoesNotExist'));
      // }
      targetAddress = this.form.value.to;
    }

    if(this.form.value.to == "" || this.form.value.amount == null || this.form.value.currency == "") {
      return this.presentToast(this._Translate.instant('WALLET.fillForm'));
    }

    var curr = this.data.account.network.find(k => k.currency == this.form.value.currency);
    if(!curr) {
      return this.presentToast(this._Translate.instant('WALLET.currencyNotSupported'));
    }

    if(this.form.value.amount > Number(curr.balance)) {
      return this.presentToast(this._Translate.instant('WALLET.amountGreaterThanBalance'));
    }

    if(this.form.value.amount < 0) {
      return this.presentToast(this._Translate.instant('WALLET.negativeAmount'));
    }

    // await this._LoaderProviderService.loaderCreate();

    var decryptedMnemonic = this.data.account.mnemonic;
    if (!!decryptedMnemonic) {
      var wallet: any = this._NftService.returnWallet(decryptedMnemonic);
    } else {
      wallet = eth.accounts.privateKeyToAccount(this.data.account.privateKey);
    }

      var receiptDataEth = this.receiptData;
      receiptDataEth = Object.assign(receiptDataEth, { to: this.form.value.to });
      receiptDataEth = Object.assign(receiptDataEth, { currency: this.form.value.currency });

      this._PopoverController.create({
        component: TransactionConfirmationComponent,
        componentProps: {
          displayInformation: receiptDataEth,
          symbol: this.FIAT_SYMBOL
        },
        cssClass: 'confirm-popover',
        showBackdrop: true,
        translucent: true
      }).then(popover => {
        popover.onDidDismiss().then(async data => {
          console.log(data);
          if(data.data.value) {

            const currency = this.form.value.currency;
            const amount = this.form.value.amount;

            const carryTransaction = async () => {

              var lang = await this._Translate.getLangFromStorage();
              var toastText = (lang == 'de') ? `Senden von ${amount} ${currency} an ${targetAddress.slice(0,4)}...` : `Sending ${amount} ${currency} to ${targetAddress.slice(0,4)}...`;
              this.presentToast(toastText);

              this.form.reset();

              if (!!decryptedMnemonic) {
                var tx = await this._NftService.sendEtherJSTransaction(wallet as Wallet, this.data.account.publicKey, targetAddress, amount, currency, this.receiptData.gas);
              } else {
                const web3 = new Web3(CurrencyToNameMapper[currency]);

                var signature = await web3.eth.accounts.signTransaction({
                  from: web3.eth.accounts.privateKeyToAccount(wallet.privateKey).address,
                  to: targetAddress,
                  value: web3.utils.toBigInt(web3.utils.toWei(amount.toString(), "ether")),
                  gas: this.receiptData.gas,
                  gasPrice: web3.utils.toBigInt(web3.utils.toWei(this.receiptData.gas, "gwei"))
                }, wallet.privateKey);

                tx = await web3.eth.sendSignedTransaction(signature.rawTransaction)
                  // .on('transactionHash', hash => {
                  //   console.log(`Transaction sent! Hash: ${hash}`);
                  // })
                  // .on('receipt', receipt => {
                  //   console.log(`Transaction confirmed! Block number: ${receipt.blockNumber}`);
                  // })
                  // .on('error', error => {
                  //   console.error('Error sending transaction:', error);
                  // })
                  ;

                // return;
              }



              if(tx) {

                var toastText2 = (lang == 'de') ? `${amount} ${currency} an ${targetAddress.slice(0,4)}... wurde gesendet. Der Transaktionsbeleg wurde gespeichert.` : `${amount} ${currency} to ${targetAddress.slice(0,4)}... has been sent. The transaction receipt has been saved.`;
                this.presentToast(toastText2);

                if(!!tx.hash){
                  console.log(tx.hash);
                  var deviceId = await this._SecureStorageService.getValue(SecureStorageKey.deviceInfo, false);
                  console.log(deviceId);
                  if(deviceId) {
                    if([CryptoCurrency.ETH, CryptoCurrency.MATIC, CryptoCurrency.GNOSIS].includes(currency)) {
                      var ob = {
                        "hash": tx.hash,
                        "deviceId": deviceId,
                        "blockchain": "ethereum",
                        "network": (environment.production) ? (currency == CryptoCurrency.ETH ? "main" : (currency == CryptoCurrency.MATIC ? "matic-main" : "xdai")) : (currency == CryptoCurrency.ETH ? "rinkeby" : (currency == CryptoCurrency.MATIC ? "matic-main" : "xdai"))
                      }
                      console.log(ob);
                      await this._ApiProviderService.blocknativeTrackTransaction(ob);
                    }
                  }
                }

                this.close();

              } else {
                this.close();
                this._NavController.navigateBack('/dashboard/payments');
              }

            }

            const conductSelfKYCAML = async () => {
                const body = {
                  "username": finalUsername,
                  "collectionName": "transaction-confirmation",
                  "webhookUrl": `${environment.apiUrl}/helix/test/dataaccesswebhook`,
                  "check": [
                      {
                          "CRYPTO_PAYMENT": {
                            userPublicKey:  this.data.account.publicKey,
                            amount: amount,
                            currency: currency,
                            timestamp: +new Date()
                          }
                      }
                  ]
                };
                var pendingRequest = await this._ApiProviderService.initDataaccessprocess(body);
                console.log(pendingRequest);

                var lang = await this._Translate.getLangFromStorage();
                var toastText = (lang == 'de') ? `Die Zahlung von ${amount} ${currency} an ${finalUsername} ist in Bearbeitung. Bitte überprüfen Sie den Abschnitt "Gesendete Anfragen" auf der Seite.` : `The payment of ${amount} ${currency} to ${finalUsername} is under processing. Please check the Requests Sent section of the page.`;

                this.presentToast(toastText);

                var cryptoPaymentRequestsSentString = await this._SecureStorageService.getValue(SecureStorageKey.cryptoPaymentRequestsSent, false);
                var cryptoPaymentRequestsSent = !!cryptoPaymentRequestsSentString ? JSON.parse(cryptoPaymentRequestsSentString) : [];
                cryptoPaymentRequestsSent.push({
                  userPublicKey: targetAddress,
                  from: this.data.account.publicKey,
                  amount: amount,
                  currency: currency,
                  timestamp: pendingRequest.dataAccessProcess.created,
                  status: 0,
                  privateKey: pendingRequest.privateKey,
                  pendingProcesses: [ pendingRequest.dataAccessProcess.dataAccessProcessId ],
                  paymentRequestId: pendingRequest.dataAccessProcess.dataAccessProcessId
                })
                this.requestsSent = cryptoPaymentRequestsSent;
                await this._SecureStorageService.setValue(SecureStorageKey.cryptoPaymentRequestsSent, JSON.stringify(this.requestsSent));

                this.close();
            }

            const alerty = await this._AlertController.create({
              mode: 'ios',
              header: this._Translate.instant('WEB3WALLETMODAL.kyc-aml-header'),
              message: this._Translate.instant('WEB3WALLETMODAL.kyc-aml-message'),
              buttons: [{
                text: this._Translate.instant('SETTINGS.no'),
                handler: async () => {
                  await carryTransaction()
                }
              }, {
                text: this._Translate.instant('SETTINGS.yes'),
                handler: async () => {
                  await conductSelfKYCAML();
                  this._NavController.navigateBack('/payments');
                }
              }]
            })

            var powerUserMode = await this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false);

            if(!finalUsername) {
              await carryTransaction();
            } else {
              if(powerUserMode == "true") {
                await alerty.present();
              } else {
                await carryTransaction();
              }
            }

          }
        })
        popover.present();
      })

  }


}
