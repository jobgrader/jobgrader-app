import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { SendCryptoComponent } from './send-crypto.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { GasPriceModule } from './gas-price/gas-price.module';
import { TransactionConfirmationModule } from './confirmation/confirmation.module';

@NgModule({
    imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    GasPriceModule,
    TransactionConfirmationModule,
    TranslateModule.forChild()
  ],
  providers: [SafariViewController],
  declarations: [SendCryptoComponent],
//   entryComponents: [SendCryptoComponent],
  exports: [SendCryptoComponent]
})
export class SendCryptoModule {}
