import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ethers } from 'ethers';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { DidPopoverComponent } from 'src/app/merchant-details/did-popover/did-popover.component';

@Component({
  selector: 'app-gas-price',
  templateUrl: './gas-price.component.html',
  styleUrls: ['./gas-price.component.scss'],
})
export class GasPriceComponent implements OnInit {
  @Input() displayInformation: any;
  @Input() gasArray: any;
  @Input() conversion: any;
  @Input() currency: any;
  @Input() symbol: any;

  public normal;
  public fast;
  public fastConverted;
  public normalConverted;

  constructor(
    private _PopOverController: PopoverController,
    private _Translate: TranslateProviderService
  ) { }

  ngOnInit() {
    console.log(this.displayInformation);
    console.log(this.gasArray);
    console.log(this.conversion);
    console.log(this.currency);

    this.normal = (Number(ethers.utils.formatEther(ethers.utils.parseUnits(this.gasArray["normal"].toString(), this.gasArray["unit"])._hex))).toFixed(10);
    this.fast = (Number(ethers.utils.formatEther(ethers.utils.parseUnits(this.gasArray["fast"].toString(), this.gasArray["unit"])._hex))).toFixed(10);
    this.normalConverted = (Math.floor(Number(this.conversion * this.normal) * 100)/100).toFixed(2);
    this.fastConverted = (Math.floor(Number(this.conversion * this.fast) * 100)/100).toFixed(2);
  }

  close() {
    this._PopOverController.dismiss();
  }

  changeValue(value) {
    this._PopOverController.dismiss({ value });
  }

  showTooltip(){
    this._PopOverController.create({
      component: DidPopoverComponent,
      componentProps: {
          data: this._Translate.instant('WEB3WALLETMODAL.tooltip')
      },
      cssClass: 'did-popover',
      showBackdrop: true,
      translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 5000)
    })
  }

}
