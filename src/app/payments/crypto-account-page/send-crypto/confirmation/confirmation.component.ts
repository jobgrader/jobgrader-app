import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
})
export class TransactionConfirmationComponent implements OnInit {
  @Input() data: any;
  @Input() displayInformation: any;
  @Input() symbol: any;

  constructor(
    private _PopOverController: PopoverController
  ) { }

  ngOnInit() {
    console.log(this.data);
    console.log(this.displayInformation);
  }

  confirm() {
    this._PopOverController.dismiss({ value: true});
  }

  cancel() {
    this._PopOverController.dismiss({ value: false});
  }

}
