import { Component, Input, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { AddCustomTokenComponent } from "../add-custom-token/add-custom-token.component";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { CryptoCurrencyService } from "src/app/payments/crypto-account-page/services/crypto-currency.service";
import { ThemeSwitcherService } from "src/app/core/providers/theme/theme-switcher.service";
import { uniq, uniqBy } from 'lodash-es';

@Component({
  selector: "app-currencies",
  templateUrl: "./currencies.component.html",
  styleUrls: ["./currencies.component.scss"],
})
export class CurrenciesComponent implements OnInit {
  wallet_address_index = -1;
  wallet_currencies: any = [];
  currencies_data = [];
  @Input("wallet_address") wallet_address: string = "";
  constructor(
    public cryptoCurrencyService: CryptoCurrencyService,
    private modalCtrl: ModalController,
    public theme: ThemeSwitcherService
  ) {}

  ngOnInit() {
    if (this.wallet_address) {
      this.prepareData();
    }
  }
  async prepareData() {
    this.wallet_currencies =
      await this.cryptoCurrencyService.getWalletCurrencies(this.wallet_address);
    if (this.wallet_currencies && this.wallet_currencies.currencies_data) {
      this.wallet_address_index = this.wallet_currencies.wallet_address_index;
      this.currencies_data = uniqBy(this.wallet_currencies.currencies_data, 'currency') ;
    } else {
      await this.cryptoCurrencyService.setInitialWalletData(
        this.wallet_address
        // ,
        // this.cryptoCurrencyService.all_wallets_currencies_data
      );
      this.prepareData();
    }
  }

  async close() {
    this.modalCtrl.dismiss();
  }

  async updateData(event) {
    console.log(
      event.detail.checked,
      this.currencies_data,
      this.wallet_address_index
    );
    let updated_data = [
      ...this.cryptoCurrencyService.getUserCurrenciesListFromStorage(),
    ];
    updated_data[this.wallet_address_index][this.wallet_address] =
      this.currencies_data;

    console.log("updated_data ====>>>", updated_data);

    await this.cryptoCurrencyService.setUserCurrenciesListInStorage(updated_data);
  }

  async openCustomTokenModal() {
    console.log("opening add custom modal");
    let modal = await this.modalCtrl.create({
      component: AddCustomTokenComponent,
      breakpoints: [0, 0.8, 1],
      initialBreakpoint: 0.8,
      componentProps: {
        address: this.wallet_address
      },
    });
    await modal.present();

    modal.onDidDismiss().then((resp) => {
      if (resp && resp.data && resp.data.add_custom_token) {
      }
    });
  }

  searchInput(event: any) {
    const query = event.target.value.toLowerCase();
    this.currencies_data = this.wallet_currencies.currencies_data.filter(d => (d.name.toLowerCase().indexOf(query) > -1) || (d.currency.toLowerCase().indexOf(query) > -1));
  }

}
