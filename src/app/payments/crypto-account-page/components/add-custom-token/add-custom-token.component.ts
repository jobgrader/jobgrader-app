import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';
import { VaultSecretKeys, VaultService } from '@services/vault/vault.service';
import { ethers } from 'ethers';
import { BarcodeService } from 'src/app/core/providers/barcode/barcode.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { CryptoNetworkNames } from 'src/app/core/providers/wallet-connect/constants';
const abi = require('../../abi.json');

@Component({
  selector: 'app-add-custom-token',
  templateUrl: './add-custom-token.component.html',
  styleUrls: ['./add-custom-token.component.scss'],
})
export class AddCustomTokenComponent implements OnInit {

  @Input() address: string;

  public networks;

  public form = new UntypedFormGroup({
    network: new UntypedFormControl('', [ Validators.required ]),
    contract: new UntypedFormControl('', [ Validators.required ]),
    name: new UntypedFormControl('', [ Validators.required ]),
    symbol: new UntypedFormControl('', [ Validators.required ]),
    decimals: new UntypedFormControl('', [ Validators.required ]),
  })
  icon: string;

  constructor(
    private modalCtrl: ModalController, 
    public theme : ThemeSwitcherService,
    private toast: ToastController,
    private translate: TranslateProviderService,
    private barcode: BarcodeService,
    private vault: VaultService
    ) {}
  
  async ngOnInit() {
    this.icon = ((this.theme.getCurrentTheme() == 'dark') ? '../../../../../assets/icon/scan.svg' : '../../../../../assets/icon/scan-light.svg');
    this.networks = [
      { label: CryptoNetworkNames.ETHEREUM, value: await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_ETHEREUM) },
      { label: CryptoNetworkNames.POLYGON, value: await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON) },
      { label: CryptoNetworkNames.GNOSIS, value: await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_GNOSIS) },
      { label: 'none', value: ''},
    ];
  }


  close() {
    this.modalCtrl.dismiss();
  }

  async save() {
    console.log(this.form);
    if(!this.form.value.network) {
      this.presentToast('Please choose a valid network for the custom token');
      return;
    }
    if(!this.form.value.contract) {
      this.presentToast('Please enter a valid contract address for the custom token');
      return;
    }
    try {
      const contract = new ethers.Contract(this.form.value.contract, abi);
      const network = await this.vault.getSecret(VaultSecretKeys[`CRYPTONETWORKS_${this.form.value.network}`]);
      const customToken = contract.connect(ethers.getDefaultProvider(network)); // CRYPTONETWORKS_POLYGON
      const customTokenBalance = await customToken.balanceOf(this.address);
      const val = ethers.BigNumber.from(customTokenBalance._hex);
      const readableBalance = ethers.utils.formatEther(val)
      console.log(`${this.form.controls.name}: ${readableBalance}`);
    } catch(e) {
      console.log(e);
      this.presentToast('Something went wrong because of the values entered in the form');
    }
    
  }

  scanContractAddress() {
    this.barcode.scanCustomToken().then((contractObject) => {
      this.form.controls.network.setValue(  contractObject.network.replace(":", "")       );
      this.form.controls.contract.setValue( contractObject.contract.replace("token-", "") );
      this.form.controls.name.setValue(     contractObject.name                           );
      this.form.controls.symbol.setValue(   contractObject.symbol                         );
      this.form.controls.decimals.setValue( contractObject.decimals                       );
      console.log(this.form);
    }).catch(error => {
      this.presentToast(this.translate.instant('WALLET.invalidQRCodeScanned'));
      console.log(this.form);
    })
  }

  presentToast(message: string) {
    this.toast.create({
      message,
      position: 'top',
      duration: 2000
    }).then(t => {
      t.present();
    })
  }



}
