import { Injectable } from "@angular/core";
import { VaultSecretKeys, VaultService } from "@services/vault/vault.service";
// import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
// import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { CryptoCurrency, CryptoNetworkNames } from "src/app/core/providers/wallet-connect/constants";


export const CurrencyToNameMapper = {
  [CryptoCurrency.THXC]: CryptoNetworkNames.THXC,
  [CryptoCurrency.HMT_POLYGON]: CryptoNetworkNames.HMT_POLYGON,
  [CryptoCurrency.HMT_SKALE]: CryptoNetworkNames.HMT_SKALE,
  [CryptoCurrency.EVE]: CryptoNetworkNames.EVAN,
  [CryptoCurrency.ETH]: CryptoNetworkNames.ETHEREUM,
  [CryptoCurrency.MATIC]: CryptoNetworkNames.POLYGON,
  [CryptoCurrency.MATIC_AMOY]: CryptoNetworkNames.POLYGON_TEST_AMOY,
  [CryptoCurrency.GNOSIS]: CryptoNetworkNames.GNOSIS,
  [CryptoCurrency.ARETH]: CryptoNetworkNames.ARBITRUM,    // Arbitrum
  [CryptoCurrency.BLUR]: CryptoNetworkNames.BLUE,        // Blue token
  [CryptoCurrency.BNB_SMART]: CryptoNetworkNames.BNB_SMART,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.BNB_BEACON]: CryptoNetworkNames.BNB_BEACON,   // BNB Smart Chain and BNB Beacon Chain
  [CryptoCurrency.MKR]: CryptoNetworkNames.MAKER_DAO,   // MakerDAO
  [CryptoCurrency.EGLD]: CryptoNetworkNames.MULTIVERSX,  // MultiversX
  [CryptoCurrency.NEAR]: CryptoNetworkNames.NEAR,        // Near
  [CryptoCurrency.OP]: CryptoNetworkNames.OPTIMISM,    // Optimism
  [CryptoCurrency.XTZ]: CryptoNetworkNames.TEZOS,       // Tezos
  [CryptoCurrency.UNI]: CryptoNetworkNames.UNISWAP,     // Uniswap
  [CryptoCurrency.AAVE]: CryptoNetworkNames.AAVE,        // Aave
  [CryptoCurrency.AE]: CryptoNetworkNames.AE,          // Aeternity
  [CryptoCurrency.AION]: CryptoNetworkNames.AION,        // Aion
  [CryptoCurrency.APE]: CryptoNetworkNames.APE,         // Apecoin
  [CryptoCurrency.AURORA]: CryptoNetworkNames.AURORA,      // Aurora
  [CryptoCurrency.AVAX]: CryptoNetworkNames.AVAX,        // Avalanche
  [CryptoCurrency.AXS]: CryptoNetworkNames.AXS,         // Axie Infinity
  [CryptoCurrency.BUSD]: CryptoNetworkNames.BUSD,        // Binance USD
  [CryptoCurrency.CELO]: CryptoNetworkNames.CELO,        // Celo
  [CryptoCurrency.LINK]: CryptoNetworkNames.LINK,        // ChainLink
  [CryptoCurrency.CRO]: CryptoNetworkNames.CRO,         // Cronos
  [CryptoCurrency.MANA]: CryptoNetworkNames.MANA,        // Decentraland Token
  [CryptoCurrency.DOGE]: CryptoNetworkNames.DOGE,        // Dogecoin
  [CryptoCurrency.DYDX]: CryptoNetworkNames.DYDX,        // dYdX
  [CryptoCurrency.ENS]: CryptoNetworkNames.ENS,         // Ethereum Name Service
  [CryptoCurrency.FTM]: CryptoNetworkNames.FTM,         // Fantom Token
  [CryptoCurrency.FIL]: CryptoNetworkNames.FIL,         // Filecoin
  [CryptoCurrency.FLOW]: CryptoNetworkNames.FLOW,        // Flow
  [CryptoCurrency.IMX]: CryptoNetworkNames.IMX,         // Immutable X
  [CryptoCurrency.CAKE]: CryptoNetworkNames.CAKE,        // PancakeSwap Token
  [CryptoCurrency.SAND]: CryptoNetworkNames.SAND,        // Sandbox Token
  [CryptoCurrency.TRON]: CryptoNetworkNames.TRON,        // Tron Token
  [CryptoCurrency.TWT]: CryptoNetworkNames.TWT,         // Trust Wallet Token
}


const default_currencies = [
  {
    name: "Thanks Coin (Polygon Amoy)",
    currency: CryptoCurrency.THXC,
    icon: "../../../../../assets/crypto/THXC.svg",
    active: true,
    disabled: true
  },
  {
    name: "Human Token (Polygon)",
    currency: CryptoCurrency.HMT_POLYGON,
    icon: "../../../../../assets/crypto/HMT_POLYGON.svg",
    active: true,
    disabled: true
  },
  {
    name: "Human Token (SKALE)",
    currency: CryptoCurrency.HMT_SKALE,
    icon: "../../../../../assets/crypto/HMT_SKALE.svg",
    active: true,
    disabled: true
  },
  {
    name: "Ethereum",
    currency: CryptoCurrency.ETH,
    icon: "../../../../../assets/crypto/vector/ETH.svg",
    active: true,
    disabled: true
  },
  {
    name: "Polygon",
    currency: CryptoCurrency.MATIC,
    icon: "../../../../../assets/crypto/vector/POL.svg",
    active: true,
    disabled: true
  },
  {
    name: "Polygon (Amoy Testnet)",
    currency: CryptoCurrency.MATIC_AMOY,
    icon: "../../../../../assets/crypto/vector/MATIC.svg",
    active: true,
    disabled: true
  },
  {
    name: "Gnosis",
    currency: CryptoCurrency.GNOSIS,
    icon: "../../../../../assets/crypto/vector/XDAI.svg",
    active: true,
    disabled: true
  },


  // {
  //   name: "EVE (EPN Testnet)",
  //   currency: CryptoCurrency.EVE,
  //   icon: "../../../../../assets/crypto/EVE_TEST.svg",
  //   active: false,
  //   disabled: true
  // },
  {
    name: "EVE (EPN Mainnet)",
    currency: CryptoCurrency.EVE,
    icon: "../../../../../assets/crypto/vector/EVE.svg",
    active: false,
    disabled: false
  },

  // {
  //   name: "Aurora",
  //   currency: CryptoCurrency.AURORA,
  //   icon: "../../../../../assets/crypto/vector/AURORA.svg",
  //   active: false,
  //   disabled: false
  // },
  // {
  //   name: "Avalanche",
  //   currency: CryptoCurrency.AVAX,
  //   icon: "../../../../../assets/crypto/vector/AVAX.svg",
  //   active: false,
  //   disabled: false
  // },

  {
    name: "Celo",
    currency: CryptoCurrency.CELO,
    icon: "../../../../../assets/crypto/vector/CELO.svg",
    active: true,
    disabled: true
  },

  {
    name: "Optimism",
    currency: CryptoCurrency.OP,
    icon: "../../../../../assets/crypto/vector/OP.svg",
    active: true,
    disabled: true
  },

  // {
  //   name: "Cronos",
  //   currency: CryptoCurrency.CRO,
  //   icon: "../../../../../assets/crypto/vector/CRO.svg",
  //   active: false,
  //   disabled: false
  // },

  // {
  //   name: "Fantom Token",
  //   currency: CryptoCurrency.FTM,
  //   icon: "../../../../../assets/crypto/vector/FTM.svg",
  //   active: false,
  //   disabled: false
  // },
  // {
  //   name: "Filecoin",
  //   currency: CryptoCurrency.FIL,
  //   icon: "../../../../../assets/crypto/vector/FIL.svg",
  //   active: false,
  //   disabled: false
  // },

  // Additional Ones

  //  {
  //   name: "BNB Smart Chain",
  //   currency: CryptoCurrency.BNB_SMART,
  //   icon: "../../../../../assets/crypto/vector/BNB.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "BNB Beacon Chain",
  //   currency: CryptoCurrency.BNB_BEACON,
  //   icon: "../../../../../assets/crypto/vector/BNB.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "MakerDAO",
  //   currency: CryptoCurrency.MKR,
  //   icon: "../../../../../assets/crypto/vector/MKR.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "MultiversX",
  //   currency: CryptoCurrency.EGLD,
  //   icon: "../../../../../assets/crypto/vector/EGLD.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Near Token", // Error RPC Method call
  //   currency: CryptoCurrency.NEAR,
  //   icon: "../../../../../assets/crypto/vector/NEAR.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Tezos",
  //   currency: CryptoCurrency.XTZ,
  //   icon: "../../../../../assets/crypto/vector/XTZ.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Uniswap",
  //   currency: CryptoCurrency.UNI,
  //   icon: "../../../../../assets/crypto/vector/UNI.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "ChainLink",
  //   currency: CryptoCurrency.LINK,
  //   icon: "../../../../../assets/crypto/vector/LINK.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Aave",
  //   currency: CryptoCurrency.AAVE,
  //   icon: "../../../../../assets/crypto/vector/AAVE.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Aeternity",
  //   currency: CryptoCurrency.AE,
  //   icon: "../../../../../assets/crypto/vector/AE.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Aion",
  //   currency: CryptoCurrency.AION,
  //   icon: "../../../../../assets/crypto/vector/AION.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Apecoin",
  //   currency: CryptoCurrency.APE,
  //   icon: "../../../../../assets/crypto/vector/APE.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Axie Infinity",
  //   currency: CryptoCurrency.AXS,
  //   icon: "../../../../../assets/crypto/vector/AXS.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Binance USD",
  //   currency: CryptoCurrency.BUSD,
  //   icon: "../../../../../assets/crypto/vector/BUSD.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Flow",
  //   currency: CryptoCurrency.FLOW,
  //   icon: "../../../../../assets/crypto/vector/FLOW.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Immutable X",
  //   currency: CryptoCurrency.IMX,
  //   icon: "../../../../../assets/crypto/vector/IMX.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "PancakeSwap",
  //   currency: CryptoCurrency.CAKE,
  //   icon: "../../../../../assets/crypto/vector/CAKE.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Sandbox Token",
  //   currency: CryptoCurrency.SAND,
  //   icon: "../../../../../assets/crypto/vector/SAND.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Tron Token",
  //   currency: CryptoCurrency.TRON,
  //   icon: "../../../../../assets/crypto/vector/TRX.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Trust Wallet Token",
  //   currency: CryptoCurrency.TWT,
  //   icon: "../../../../../assets/crypto/vector/TWT.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Decentraland Token",
  //   currency: CryptoCurrency.MANA,
  //   icon: "../../../../../assets/crypto/vector/MANA.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Dogecoin",
  //   currency: CryptoCurrency.DOGE,
  //   icon: "../../../../../assets/crypto/vector/DOGE.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "dYdX",
  //   currency: CryptoCurrency.DYDX,
  //   icon: "../../../../../assets/crypto/vector/DYDX.svg",
  //   active: false,
  //   disabled: true
  // },
  // {
  //   name: "Ethereum Name Service",
  //   currency: CryptoCurrency.ENS,
  //   icon: "../../../../../assets/crypto/vector/ENS.svg",
  //   active: false,
  //   disabled: true
  // },
];
@Injectable({
  providedIn: "root",
})
export class CryptoCurrencyService {
  private all_wallets_currencies_data = [];

  constructor(
    // private secureStorage: SecureStorageService
    private vault: VaultService
  ) {
    // this.initAllWalletsCurrencies();
  }

  public CurrencyToRPCMapper = async (cryptoCurrency: string) => {
    let mapper;
    switch (cryptoCurrency) {
      case CryptoCurrency.THXC: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_THXC)); break;
      case CryptoCurrency.HMT_POLYGON: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_HMT_POLYGON)); break;
      case CryptoCurrency.HMT_SKALE: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_HMT_SKALE)); break;
      case CryptoCurrency.EVE: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_EVAN)); break;
      case CryptoCurrency.ETH: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_ETHEREUM)); break;
      case CryptoCurrency.MATIC: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON)); break;
      case CryptoCurrency.MATIC_AMOY: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_POLYGON_TEST_MUMBAI)); break;
      case CryptoCurrency.GNOSIS: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_GNOSIS)); break;
      case CryptoCurrency.ARETH: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_ARBITRUM)); break;    // Arbitrum
      case CryptoCurrency.BNB_SMART: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_BNB_SMART)); break;   // BNB Smart Chain and BNB Beacon Chain
      case CryptoCurrency.NEAR: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_NEAR)); break;        // Near
      case CryptoCurrency.OP: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_OPTIMISM)); break;    // Optimism
      case CryptoCurrency.AURORA: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_AURORA)); break;      // Aurora
      case CryptoCurrency.AVAX: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_AVAX)); break;        // Avalanche
      case CryptoCurrency.CELO: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_CELO)); break;        // Celo
      case CryptoCurrency.CRO: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_CRO)); break;         // Cronos
      case CryptoCurrency.FTM: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_FTM)); break;         // Fantom Token
      case CryptoCurrency.FIL: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_FIL)); break;
      default: mapper = (await this.vault.getSecret(VaultSecretKeys.CRYPTONETWORKS_THXC)); break;
    }
    return mapper;
  }
  // async initAllWalletsCurrencies() {
  //   let data_from_storage = await this.getUserCurrenciesListFromStorage();
  //   if (data_from_storage) {
  //     let parsed_data = JSON.parse(data_from_storage);
  //     this.all_wallets_currencies_data = parsed_data;
  //   }
  // }

  async setInitialWalletData(wallet_address: string) {
    let data = [
      {
        [wallet_address]: [...default_currencies],
      },
    ];
    // data = [...previous_data, ...data];
    data = [...this.all_wallets_currencies_data, ...data];
    console.info('setting wallet currencies', data);
    await this.setUserCurrenciesListInStorage(data);
  }
  async setUserCurrenciesListInStorage(data: any[]) {
    // try {
    //   await this.secureStorage.setValue(
    //     SecureStorageKey.walletCurrencies,
    //     JSON.stringify(data)
    //   );
    // } catch (err) {
    //   // mburger: TODO we need to resolve the exceeded quote issue
    //   // when stored value is to big
    //   console.info(err);
    // }

    // await this.initAllWalletsCurrencies();
    this.all_wallets_currencies_data = data;
  }

  getUserCurrenciesListFromStorage() {
    // return this.secureStorage.getValue(SecureStorageKey.walletCurrencies, false);
    return this.all_wallets_currencies_data;
  }

  async getWalletCurrencies(wallet_address: string) {
    let wallet_address_index = -1;
    let result = [];
    let data_from_storage = await this.getUserCurrenciesListFromStorage();
    if (data_from_storage) {
      // let parsed_data = JSON.parse(data_from_storage);
      let parsed_data = data_from_storage;
      // console.log("parsed -darta", parsed_data);
      parsed_data.forEach((ele, index) => {
        Object.keys(ele).forEach((addressKey, index2) => {
          if (addressKey == wallet_address) {
            wallet_address_index = index;
            Object.keys(ele[addressKey]).forEach((currencyKey) => {
              result.push(ele[addressKey][currencyKey]);
            });
          }
        });
      });
      if (!result.length) {
        return null;
      }
    } else {
      return null;
    }
    return {
      currencies_data: [...result],
      wallet_address_index: wallet_address_index,
    };
  }
}
