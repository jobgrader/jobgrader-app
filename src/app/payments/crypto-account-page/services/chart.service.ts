
import { Injectable } from "@angular/core";
import { AlertController, NavController } from "@ionic/angular";
import { ThemeSwitcherService } from "src/app/core/providers/theme/theme-switcher.service";
import { CryptoCurrency } from "src/app/core/providers/wallet-connect/constants";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { fiatCurrencies } from "src/app/core/providers/nft/fiat-currencies";
import { TranslateProviderService } from "src/app/core/providers/translate/translate-provider.service";

@Injectable({
  providedIn: "root",
})
export class ChartService {
  public sum: Number = 0;
  private CONVERTED_FIAT: string = "";
  data: any;
  dataSet: any[] = [];
  constructor(
    private _NavController: NavController,
    private _Theme: ThemeSwitcherService,
    private _Alert: AlertController,
    private _Translate: TranslateProviderService,
    private _SecureStorageService: SecureStorageService
  ) {}

  prepareIcons(dataSet: any[]) {
    let new_data = [];
    dataSet.forEach((ele, index) => {
      new_data.push({
        size: ele.size,
        color: ele.color,
        count: ele.count, // + 20,
        icon:
          "../../../../../assets/" +
          ele.icon.substr(ele.icon.indexOf("/assets") + 8),
        // iconXFactor: ele.icon.indexOf('XDAI.svg') > -1  ? 20: 0,
      });
    });
    return new_data;
  }

  d3Chart(convertedFiat: string, dataset: any, chart_id: string, firstTime: boolean) {

    this.CONVERTED_FIAT = convertedFiat;

    console.log("dataset", dataset);

    this.dataSet= this.prepareIcons([...dataset]);

    let d3 = (window as any).d3;
    let coreColor = Array.from(this.dataSet, (k) => (k as any).color);

    console.log("this.dataSet", this.dataSet);

    this.sum = Array.from(this.dataSet, (k) => (k as any).count).reduce(
      (a, b) => a + b
    );

    if (!this.sum) {
      this.sum = 0;
    }

    let color = null;

    if (this.sum == 0) {
      this.dataSet.map(ds => ds.count = 10);
      color = coreColor;
    } else {
      color = coreColor;
    }

    setTimeout(() => {
      d3.select(chart_id).selectAll("*").remove();

      let width = d3.select(chart_id).node().getBoundingClientRect().width;
      let height = 350;

      let donutWidth = 100;
      let innerRadius = Math.min(width, height) / 2 - donutWidth;

      var shadowWidth=10;

      var outerRadiusArcShadow=innerRadius+1;
      var innerRadiusArcShadow=innerRadius-shadowWidth;

      let svg = d3
        .select(chart_id)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        console.log(svg);

        let pie = d3.layout
        .pie()
        .value((d) => {
          return d.count;
        })
        // .sort(null);

        // console.log(pie);

      svg
        .selectAll("path")
        .data(pie(this.dataSet))
        .enter()
        .append("svg:image")
        .attr("xlink:href", (d) => {
          return d.data.count ? d.data.icon : '';
        })
        .attr("width", 20)
        .attr("height", 20)
        .attr("transform", (d) => {
          return (
            "translate(" +
            ( ((innerRadius +
              // d.data.size +
              45 +
              (d.data.iconXFactor ? d.data.iconXFactor : 0)) *
              Math.sin(((d.endAngle - d.startAngle) / 2) + d.startAngle)) - 10) +
            ", " +
            -1 *
              ((innerRadius + 45) *
              Math.cos(((d.endAngle - d.startAngle) / 2) + d.startAngle) + 10) +
            ")"
          );
        });



      var mainChart = this.createChart(svg,
        // innerRadius + (donutWidth / 2),
        function(d) {
          return innerRadius + d.data.size
        },
        innerRadius,function(d,i){
            return color[i];
        },'path1', d3, firstTime, pie);

      var shadowChart = this.createChart(svg,
        outerRadiusArcShadow,
        innerRadiusArcShadow,

      function(d,i){
        var c=d3.hsl(color[i]);
        return d3.hsl((c.h+5), (c.s -.07), (c.l -.15));
      },'path2', d3, firstTime, pie);




      let font_size = 30;
      if (this.CONVERTED_FIAT.length > 8) {
        for (let i = 8; i < this.CONVERTED_FIAT.length; i++) {
          if (font_size > 18) {
            if (font_size > 2) {
              font_size = font_size - 2;
            }
          } else {
            if (font_size > 1) {
              font_size = font_size - 1;
            }
          }
        }
      }

      svg
        .append("text")
        .attr("text-anchor", "middle")
        .attr("x", "0")
        .attr("y", "0")
        .attr("font-size", font_size + "px")
        .attr(
          "fill",
          this._Theme.getCurrentTheme() == "dark" ? "white" : "black"
        )
        .text(this.CONVERTED_FIAT ? this.CONVERTED_FIAT : "...");

      svg
        .append("text")
        .attr("text-anchor", "middle")
        .attr("x", "0")
        .attr("y", "0")
        .attr("font-size", "16px")
        .append("tspan")
        .attr("x", "0")
        .attr("y", (2).toString() + "em")
        .attr("fill", "green")
        .attr("id", `balance-indicator`)
        // .text(`+2.08%`); // TODO: Add conversion values

      // mburger: TODO fix this
      // this.data.account.network.forEach((ll, ii) => {
      //   if (ll.currency === CryptoCurrency.ETH) {
      //     document.getElementById(`arc-` + ii).onclick = (event) => {
      //       this.navigateToCryptoHistory(ll.currency);
      //     }
      //     document.getElementById(`text-chart-${ii}`).onclick = () =>
      //       this.navigateToCryptoHistory(ll.currency);
      //   }
      // });
    }, 500);
  }

  private createChart(svg, outerRadius, innerRadius, fillFunction, className, d3, firstTime, pie) {

    let arc = d3.svg.arc()
                  .innerRadius(innerRadius)
                  .outerRadius(outerRadius);

    let path = svg
                .selectAll('.'+className)
                .data(pie(this.dataSet))
                .enter()
                .append("path")
                .attr("class", className)
                .attr("d", arc)
                .attr("fill", fillFunction);

    if(firstTime) {
      path.transition()
              .duration(1000)
              .attrTween('d', function(d) {
                  var interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, d);
                  return function(t) {
                      return arc(interpolate(t));
                  };
              });
    }

    const chart = { path: path, arc: arc };

    return chart;
  };


  public navigateToCryptoHistory(currency: string) {

    const explorerAlert = (link: string) => {
      this._Alert.create({
        message: this._Translate.instant('OPEN_EXPLORER'),
        buttons: [
          { text: this._Translate.instant('SETTINGS.yes'), handler: () => {
            var a = document.createElement("a");
            a.href = link;
            a.style.display = 'none';
            a.target = '_system';
            a.click();
            a.remove();
          } },
          { text: this._Translate.instant('SETTINGS.no'), role: 'cancel', handler: () => {}}
        ]
      }).then((alertp) => {
        alertp.present();
      })
    }

    if(currency == CryptoCurrency.ETH) {
      let icon = this.data.account.network.find(
        (ele) => ele.currency === currency
      ).icon;

      icon = icon.substr(icon.indexOf("/assets") + 8);
      console.log("navigateToCryptoHistory", this.data);
      this._NavController.navigateForward(
        `/dashboard/payments/network-account-details?address=${
          this.data.account.publicKey
        }&heading=${this.data.account.heading}&primary=${!!this.data.account
          .primary}&currency=${currency}&icon=${icon}`
      );
    } else if(currency == CryptoCurrency.HMT_SKALE) {
      explorerAlert(`https://wan-red-ain.explorer.mainnet.skalenodes.com/address/${this.data.account.publicKey}/token-transfers`);
    } else if(currency == CryptoCurrency.HMT_POLYGON) {
      explorerAlert(`https://polygonscan.com/address/${this.data.account.publicKey}#tokentxns`);
    } else if(currency == CryptoCurrency.MATIC) {
      explorerAlert(`https://polygonscan.com/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.GNOSIS) {
      explorerAlert(`https://gnosisscan.io/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.OP) {
      explorerAlert(`https://optimistic.etherscan.io/address/${this.data.account.publicKey}#tokentxns`);
    } else if(currency == CryptoCurrency.AURORA) {
      explorerAlert(`https://explorer.mainnet.aurora.dev/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.AVAX) {
      explorerAlert(`https://snowtrace.io/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.CELO) {
      explorerAlert(`https://explorer.celo.org/mainnet/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.CRO) {
      explorerAlert(`https://cronoscan.com/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.FTM) {
      explorerAlert(`https://ftmscan.com/address/${this.data.account.publicKey}`);
    } else if(currency == CryptoCurrency.FIL) {
      explorerAlert(`https://filfox.info/en/address/${this.data.account.publicKey}`);
    }    

  }

  async getConversion(currency: string, fiat, value) {

    var symbol = fiatCurrencies.find(fi => fi.currency_code == fiat).currency_symbol;

    let data = await this._SecureStorageService.getValue(
      SecureStorageKey.cryptoConversions
    );
    if (data) {
      data = JSON.parse(data);
    }
    if (data && data[currency + "/" + fiat]) {
      return symbol + ' ' + (data[currency + "/" + fiat] * Number(value)).toFixed(2);
    } else {
      return `${symbol} 0.00`;
    }
  }

  LightenDarkenColor(col, amt) {
    col = parseInt(col, 16);
    return (((col & 0x0000FF) + amt) | ((((col >> 8) & 0x00FF) + amt) << 8) | (((col >> 16) + amt) << 16)).toString(16);
  }

}
