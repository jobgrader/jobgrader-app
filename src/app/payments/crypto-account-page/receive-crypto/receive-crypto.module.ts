import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { ReceiveCryptoComponent } from './receive-crypto.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';

@NgModule({
    imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  providers: [SocialSharing],
  declarations: [ReceiveCryptoComponent],
//   entryComponents: [ReceiveCryptoComponent],
  exports: [ReceiveCryptoComponent]
})
export class ReceiveCryptoModule {}
