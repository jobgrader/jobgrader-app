import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';
import { BuyCryptoComponent } from './buy-crypto.component';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
    CommonModule,
    QRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild()
  ],
  providers: [],
  declarations: [BuyCryptoComponent],
//   entryComponents: [BuyCryptoComponent],
  exports: [BuyCryptoComponent]
})
export class BuyCryptoModule {}
