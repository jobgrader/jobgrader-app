import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PaymentsPage } from './payments.page';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { PaymentModalModule } from './payment-modal/payment-modal.module';
import { AccountModalModule } from './account-modal/account-modal.module';
import { WalletImportDisclaimerModule } from './wallet-import-disclaimer/wallet-import-disclaimer.module';
import { KeyExportModule } from '../sign-up/components/step-5/key-export/key-export.module';
import { ImportUserKeyModule } from '../login/import-userkey/import-userkey.module';
import { ReceiveCryptoModule } from './crypto-account-page/receive-crypto/receive-crypto.module';
import { WalletCardComponent } from './wallet-card/wallet-card.component';
import { Ng2FittextModule } from "ng2-fittext";
import { JobHeaderModule } from '../job-header/job-header.module';
import { WalletActivatioonDisclaimerModule } from './wallet-activation-disclaimer/wallet-activation-disclaimer.module';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
const routes: Routes = [
  {
    path: '',
    component: PaymentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    SharedModule,
    PaymentModalModule,
    AccountModalModule,
    WalletImportDisclaimerModule,
    WalletActivatioonDisclaimerModule,
    ReceiveCryptoModule,
    KeyExportModule,
    ImportUserKeyModule,
    JobHeaderModule,
    RouterModule.forChild(routes),
    Ng2FittextModule
  ],
  providers: [File, Chooser],
  declarations: [PaymentsPage, WalletCardComponent]
})
export class PaymentsPageModule {}
