import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../core/models/User';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { DeeplinkProviderService } from '../core/providers/deeplink/deeplink-provider.service';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { ToastController, NavController } from '@ionic/angular';
import { DatePipe } from '@angular/common';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

const VERIFICATION_REJECT_STATUS = 1;
const VERIFICATION_ACCEPT_STATUS = 2;

@Component({
  selector: 'app-verification',
  templateUrl: './verification.page.html',
  styleUrls: ['./verification.page.scss']
})
export class VerificationPage implements OnInit {

  user: User = {};
  userPhoto: string = '../../assets/job-user.svg';
  verificationStatus: number;
  additionalFields: any;
  verifStatus: any;
  ageValidator = true;
  dlValidator = true;
  idcardValidator = true;

  private verifiedDeeplinkPayload: any;
  private verificationData: any;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private activatedRoute: ActivatedRoute,
    private deeplinkProviderService: DeeplinkProviderService,
    private authenticationProviderService: AuthenticationProviderService,
    private userProviderService: UserProviderService,
    private loaderProviderService: LoaderProviderService,
    private toastController: ToastController,
    private secureStorage: SecureStorageService,
    private cdr: ChangeDetectorRef,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
    this.checkVerifiedDeeplinkPayload();
  }

  checkVerifiedDeeplinkPayload() {
    const etalon = ['publicKey', 'sourceId', 'ageMin', 'ageMax', 'name', 'phoneNumber', 'driverLicence', 'residenceCountry'];
    // While testing with testnet version of fleet2share app, append 'pilotLicence' to etalon
    const keysArr = Object.keys(this.deeplinkProviderService.decodeDeeplinkStringValue(this.deeplinkProviderService.verificationDeeplinkStringValue));
    for (let i = 0; i < keysArr.length; i++) {
      if (etalon.indexOf(keysArr[i]) === -1) {
        this.nav.navigateForward(`/unrecognized-parameters`);

        return;
      }
    }
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  userAgeOver18(user: User = {}) {
    return this.userProviderService.userAgeOver18(user.dateofbirth);
  }

  goBack() {
    this.nav.navigateRoot('/dashboard');
  }



  async reject() {
    this.verificationData =
      this.deeplinkProviderService.temporaryVerifiedDeeplinkResponse(
        this.verifiedDeeplinkPayload.sourceId,
        this.verifiedDeeplinkPayload.accountId,
        this.verifiedDeeplinkPayload.verificationPath,
        this.user.identityId,
        this.userProviderService.getUserFullName(this.user),
        this.user.phone,
        VERIFICATION_REJECT_STATUS
      );

    const link = document.createElement('a');
    document.body.appendChild(link);
    link.setAttribute('style', 'display: none');
    link.href = `daimler-demo://daimler-demo.com/sign?string=${this.deeplinkProviderService.encodePayload(
      this.verificationData
    )}`;
    link.click();
    link.remove();
  }

  async accept() {
    const claimedDeeplinkPayload = await this.claimDeeplink();

    this.verificationData =
      this.deeplinkProviderService.temporaryVerifiedDeeplinkResponse(
        claimedDeeplinkPayload.sourceId,
        claimedDeeplinkPayload.accountId,
        claimedDeeplinkPayload.verificationPath,
        this.user.identityId,
        this.userProviderService.getUserFullName(this.user),
        this.user.phone,
        VERIFICATION_ACCEPT_STATUS
      );
    // Added by Ansik for simulation. The payload should be visible on the console
    console.log('The UUID is:' + this.user.identityId);
    console.log('Payload is:' + JSON.stringify(this.verificationData));
    console.log(this.deeplinkProviderService.encodePayload(this.verificationData));
    //
    const link = document.createElement('a');
    document.body.appendChild(link);
    link.setAttribute('style', 'display: none');
    link.href = `daimler-demo://daimler-demo.com/pseudo?string=${this.deeplinkProviderService.encodePayload(
      this.verificationData
    )}`;
    link.click();
    link.remove();
  }

  private async claimDeeplink() {
    await this.loaderProviderService.loaderCreate();

    const claimedDeeplinkPayloadResponse =
      await this.deeplinkProviderService.claim(
        this.deeplinkProviderService.verificationDeeplinkStringValue
      );

    await this.loaderProviderService.loaderDismiss();

    return claimedDeeplinkPayloadResponse;
  }

  private async componentInit() {
    var photo = this.userPhotoServiceAkita.getPhoto();

    if(photo) {
      this.userPhoto = photo;
    }
    const { user, verifiedDeeplinkPayload } = this.activatedRoute.snapshot.data;
    // console.log(verifiedDeeplinkPayload);
    const {
          firstname,
          lastname,
          driverlicencedocumentnumber,
          driverlicencecountry,
          driverlicenceexpirydate,
          driverlicenceissuedate,
          identificationdocumentnumber,
          identificationissuecountry,
          identificationexpirydate,
          identificationissuedate,
          citizenship,
          phone,
          dateofbirth
        } = verifiedDeeplinkPayload.byUsrId.data;
    this.verifStatus = {
      firstname,
      lastname,
      driverlicencedocumentnumber,
      driverlicencecountry,
      driverlicenceexpirydate,
      driverlicenceissuedate,
      identificationdocumentnumber,
      identificationissuecountry,
      identificationexpirydate,
      identificationissuedate,
      citizenship,
      phone,
      dateofbirth
    };
    this.user = user;
    this.verifiedDeeplinkPayload = verifiedDeeplinkPayload.byLink;
    console.log('verifiedDeeplinkPayload = ' + JSON.stringify(this.verifiedDeeplinkPayload));
    this.verificationStatus = this.verifiedDeeplinkPayload.status;
    this.additionalFields = this.verifiedDeeplinkPayload.verificationResult;
    console.log('Additional Fields: ' + JSON.stringify(this.additionalFields));
    if (this.additionalFields.errors.length != 0) {
      for (let i = 0; i < this.additionalFields.errors.length; i++) {
          if (this.additionalFields.errors[i].fieldName == 'dateofbirth') {
            this.ageValidator = false;
            this.presentToast(this.additionalFields.errors[i].message);
          } else if (this.additionalFields.errors[i].fieldName == 'driverlicencedocumentnumber' ||
            this.additionalFields.errors[i].fieldName == 'driverlicencecountry' ||
            this.additionalFields.errors[i].fieldName == 'driverlicenceissuedate' ||
            this.additionalFields.errors[i].fieldName == 'driverlicenceexpirydate') {
            this.dlValidator = false;
            this.presentToast(this.additionalFields.errors[i].message);
          } else if (this.additionalFields.errors[i].fieldName == 'identificationdocumentnumber' ||
            this.additionalFields.errors[i].fieldName == 'identificationissuecountry' ||
            this.additionalFields.errors[i].fieldName == 'identificationissuedate' ||
            this.additionalFields.errors[i].fieldName == 'identificationexpirydate') {
            this.idcardValidator = false;
            this.presentToast(this.additionalFields.errors[i].message);
          }
      }
    }
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

    private getFormattedDay(date: string): string {
      const datePipe = new DatePipe('en-US');

      return datePipe.transform(new Date(date), 'dd MMM yyyy');
    }

    private getFormattedDayForRequest(date: string): number {
      return +new Date(date);
    }

    private detectChanges() {
      if (!this.cdr['destroyed']) {
        this.cdr.detectChanges();
      }
    }
// Basic data status of 4 means that the document need to be resubmitted or renewed as per the law.
}
// Line 15 of verification page html: The verification value can either come from the middleware /verification API call or from basicdata.firstname.status. It is advisable to use
// Line 56 of verification page html: The logic behind driverLicence and ID card should have the same boolean value since the purpose is to show that DL can be used as an identification document and also as a license for vehicle usage allowance
// additionalFields.errorFound: boolean
// additionalFields.errors.fieldName: string
// additionalFields.errors.fieldValue: string
// additionalFields.errors.message: string
