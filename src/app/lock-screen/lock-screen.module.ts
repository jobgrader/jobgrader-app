import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CodeInputModule } from 'angular-code-input';
import { IonicModule } from '@ionic/angular';

import { LockScreenPage } from './lock-screen.page';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { LockService } from './lock.service';

const routes: Routes = [
  {
    path: '',
    component: LockScreenPage
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    CodeInputModule,
    RouterModule.forChild(routes)
  ],
  providers: [
      LockService
  ],
  declarations: [LockScreenPage],
  // entryComponents: [ LockScreenPage ]
})
export class LockScreenPageModule {}
