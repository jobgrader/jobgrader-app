import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.scss'],
})
export class QrcodeComponent  implements OnInit {
  @Input() qrCode: string;

  constructor(
    private _ModalController: ModalController
  ) { }

  ngOnInit() {}

  close() {
    this._ModalController.dismiss();
  }

}
