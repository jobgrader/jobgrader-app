import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { User } from '../core/models/User';
import { AppStateService } from '../core/providers/app-state/app-state.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { NavController, ToastController } from '@ionic/angular';
import { EventsService, EventsList } from 'src/app/core/providers/events/events.service';
import { environment } from 'src/environments/environment';
import { NetworkService } from '../core/providers/network/network-service';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { AuthenticationProviderService } from '../core/providers/authentication/authentication-provider.service';
import { KycService } from '../kyc/services/kyc.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
// import { ChatService } from '../core/providers/chat/chat.service';

@Component({
    selector: 'app-tab-profile',
    templateUrl: './tab-profile.page.html',
    styleUrls: ['./tab-profile.page.scss'],
})
export class TabProfilePage {

    public user: User;
    public profilePictureSrc = '../../assets/job-user.svg';
    public placeholderImage = '../../assets/job-user.svg';
    public isInnovator = false;
    public badgeLength = [];
    public isProduction = !!environment.production;
    public powerUserMode = false;
    public isIntegration = false;
    public tapToInitiate = true;
    public tapToActivate = false;
    public source: string = '';
    public displayVerifiedTick = false;

    // private inactiveColor = "#D7D7D7";
    private inactiveColor = "#787878";

    constructor(
        private nav: NavController,
        private router: Router,
        private appStateService: AppStateService,
        private apiProviderService: ApiProviderService,
        private _KycService: KycService,
        // public _ChatService: ChatService,
        private _ToastController: ToastController,
        private translateService: TranslateProviderService,
        private secureStorage: SecureStorageService,
        private _EventsService: EventsService,
        private _NetworkService: NetworkService,
        private _ImageSelectionService: ImageSelectionService,
        private _AuthenticationProviderService: AuthenticationProviderService,
        private _Theme: ThemeSwitcherService,
        private userPhotoServiceAkita: UserPhotoServiceAkita,
    ) {
    }

    public ionViewWillEnter() {
        let checker = this.router.parseUrl(this.router.url).queryParams;
        console.log(checker);

        if(checker.source) {
            this.source = checker.source;
        }

        var theme = this._Theme.getCurrentTheme();
        if (theme == 'dark') {
            try {
                var frozen = document.getElementsByClassName("item__title__frozen");
                for(var f = 0; f< frozen.length; f++) {
                    (frozen[f] as any).style.color = this.inactiveColor;
                }
            } catch (e) {
                console.log(e);
            }
        }
        this.placeholderImage = '../../assets/job-user.svg'; // : '../../assets/U-unAuthorized.svg';
        this.profilePictureSrc = this.placeholderImage;
        this._EventsService.publish(EventsList.hideProfileTabBadge);
        this.isProduction = environment.production;
        this.isIntegration = (environment.helixEnv == 'integration');
        this._KycService.isUserAllowedToUseChatMarketplace().then(displayVerifiedTick => {
            this.displayVerifiedTick = displayVerifiedTick;
        })

        if(this.appStateService.basicAuthToken) {
            const photo = this.userPhotoServiceAkita.getPhoto();
            if(photo) {
                this.profilePictureSrc = photo
            }
        }

        this.secureStorage.getValue(SecureStorageKey.powerUserMode, false).then(_ => {
            this.powerUserMode = (_ == 'true')
        })
        this.secureStorage.getValue(SecureStorageKey.web3WalletPublicKey, false).then(web3PublicKey => {
            console.log(web3PublicKey);
            if(web3PublicKey) {
                this.secureStorage.getValue(SecureStorageKey.web3WalletMnemonic, false).then(web3WalletMnemonic => {
                    // console.log(web3WalletMnemonic);
                    if(web3WalletMnemonic) {
                        this.tapToInitiate = false;
                        this.tapToActivate = false;
                    } else {
                        this.tapToInitiate = false;
                        this.tapToActivate = true;
                    }
                })
            } else {
                this.tapToInitiate = true;
                this.tapToActivate = false;
            }
        })

        if(!this._NetworkService.checkConnection()){
            this._NetworkService.addOfflineFooter('ion-footer');
        } else {
            this._NetworkService.removeOfflineFooter('ion-footer');
        }
        this._AuthenticationProviderService.displayLoginOptionIfNecessary();
    }

    /** Lifecycle hook for view will enter */
    public ionViewDidLeave(): void {
        this._EventsService.unsubscribe(EventsList.showProfileTabBadge);
        this._EventsService.unsubscribe(EventsList.hideProfileTabBadge);
    }

    public ionViewDidEnter(): void {
        this.secureStorage.getValue(SecureStorageKey.certificates, false).then(certificates => {
            if(certificates) {
                this.badgeLength = JSON.parse(certificates).filter(k => !k.signatureHash)
            }
        })
    }

    private presentToast(message: string) {
        this._ToastController.create({
            message,
            duration: 2000,
            position: 'top'
        }).then(toast => toast.present());
    }

    comingSoon() {
        this.presentToast(this.translateService.instant('CONTACT.title'));
    }

    /** Callback for the navigation point to personal details */
    public async navigateToPage(page: string): Promise<void> {
        // if(['/financial-documents'].includes(page) && this.isProduction) {
        //     return;
        // }
        // if(!this.appStateService.isAuthorized) {
        //     await this.apiProviderService.noUserLoggedInAlert();
        //     return;
        // }
        var ex = ['/nft?mode=nft','/nft?mode=walletconnect'];
        if(ex.includes(page) && (this.tapToInitiate || this.tapToActivate) ) {
            this.presentToast(this.translateService.instant('IMPORTKEY.initiate-message'));
            return;
        }
        // const wizardNotRun = (await this.storageProviderService.getItem(StorageKeys.wizardNotRun)) === 'true';
        // if (wizardNotRun) {
        //     await this.alertService.alertCreate(
        //         await this.translateService.translateGet('PROFILE.personal-details-missing-title'),
        //         undefined,
        //         await this.translateService.translateGet('PROFILE.personal-details-missing-message'),
        //         [{
        //             text: this.translateService.instant('KYC.ALERTVERIFICATION.goToVerification'),
        //             handler: async () => {
        //                 this.nav.navigateForward('/kyc?from=onboarding');
        //                 }
        //             },
        //             {
        //             text: this.translateService.instant('KYC.ALERTVERIFICATION.enterDataManually'),
        //             handler: async () => {
        //                 this.nav.navigateForward('/sign-up/step-6');
        //             }
        //         }]
        //     );
        // } else {
            this.nav.navigateForward(page);
        // }
    }

    async displayPictureOptions() {
        if(this.appStateService.isAuthorized) {
            console.log('Should work!')
            this._ImageSelectionService.showChangePicture().then((photo) => {
                this.profilePictureSrc = !!photo ? photo : this.placeholderImage;
            });
        } else {
            await this.apiProviderService.noUserLoggedInAlert();
        }
    }

    tapToInitiateMethod() {
        if(!this.appStateService.isAuthorized) {
            this.apiProviderService.noUserLoggedInAlert();
        } else {
            this.nav.navigateForward('/wallet-generation');
        }
        // this.nav.navigateForward('/payments');
    }

    async tapToActivateMethod() {
        console.log("tapToActivateMethod");

        if(!this.appStateService.isAuthorized) {
            await this.apiProviderService.noUserLoggedInAlert();
            return;
        }

        this.nav.navigateForward('/payments');

        // const modal = await this._ModalController.create({
        //     component: ImportUserkeyComponent,
        //     componentProps: {
        //         initial: true
        //     }
        // })
        // modal.onDidDismiss().then(async (data) => {
        //     if(data.data.value) {
        //         this.nav.navigateForward('/payments');
        //     }
        // })
        // await modal.present();
    }

}

