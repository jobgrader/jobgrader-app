import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ImportUserKeyModule } from '../login/import-userkey/import-userkey.module';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { TabProfilePage } from './tab-profile.page';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: TabProfilePage,
    resolve: {
      user: UserProviderResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImportUserKeyModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [TabProfilePage]
})
export class TabProfilePageModule {}
