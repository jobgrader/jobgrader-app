import { SignProviderService } from './../core/providers/sign/sign-provider.service';
import { Router } from '@angular/router';
import { TranslateProviderService } from './../core/providers/translate/translate-provider.service';
import { Component, OnInit, ViewChild, NgZone, ElementRef, SecurityContext } from '@angular/core';
import { IonSlides, NavController, AlertController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SecureStorageService } from './../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from './../core/providers/secure-storage/secure-storage-key.enum';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { environment } from 'src/environments/environment';
import { Vibration } from '@awesome-cordova-plugins/vibration/ngx';
import { Platform } from '@ionic/angular';
import { Haptics, ImpactStyle } from '@capacitor/haptics';
import { rainModule } from "./emoji_rain";
import { StreamingMedia, StreamingVideoOptions } from '@awesome-cordova-plugins/streaming-media/ngx';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

// import { VideoSource } from './videoBase64';
import { NetworkService } from '../core/providers/network/network-service';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
// import { AdjustService } from '../core/providers/adjust/adjust.service';
// import { TrackingPermissionComponent } from './tracking-permission/tracking-permission.component';
import { OpenNativeSettings } from '@awesome-cordova-plugins/open-native-settings/ngx';


declare var window: any;
declare var cordova: any;

interface JobgraderWalkthroughContent {
  image: string;
  header: string;
  content: string;
  background: string;
}

enum Intensity {
  START = 800,
  MID = 40,
  MAX = 100,
  LONG = 200,
  VERYLONG = 400
}

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.page.html',
  styleUrls: ['./onboarding.page.scss'],
})
export class OnboardingPage implements OnInit {
@ViewChild('video') video: ElementRef;
videoElement: HTMLVideoElement;

  public jobgraderSlides: Array<JobgraderWalkthroughContent> = [];

  public doNotShow = false;
  public isLoaded = false;
  public videoShouldStart = false;

  public videoFinished = false;
  public hasStarted = false;

  public isFirstSlide = true;
  public isLastSlide = false;
  public slidesLength = 0;

  public activeSlideIndex = 0;
  public paginationOffset = 1;

  public elementTopMove = false;
  public elementBottomMove = false;

  public elementTopFadeIn = false;
  public elementTopFadeOut = false;

  public elementBlueFadeIn = false;
  public elementBlueFadeOut = false;

  public sliderColor = 0;
  public altWeb3Screenshot: string;
  public leanBackground: string;

  public slides: IonSlides;
  @ViewChild('slides') set content(slides: IonSlides) {
    this.slides = slides;
    this.slides.length()
    .then((length) => {
      this.slidesLength = length;
    }).catch(e => console.log(e));
  }

  locked = true;

  sliderOptions = {
    resistanceRatio: 0
  };


  constructor(
    private nav: NavController,
    private router: Router,
    private translateProviderService: TranslateProviderService,
    private signProvider: SignProviderService,
    public translateService: TranslateService,
    private nativeAudio: NativeAudio,
    private secureStorage: SecureStorageService,
    private vibration: Vibration,
    public platform: Platform,
    private _StreamingMedia: StreamingMedia,
    private _AlertController: AlertController,
    private sanitizer: DomSanitizer,
    private _NetworkService: NetworkService,
    private _ThemeSwitcherService: ThemeSwitcherService,
    // private _Adjust: AdjustService,
    private _OpenNativeSettings: OpenNativeSettings,
    private _ModalController: ModalController
  ) { 
    [1,2,3,4,5].forEach(n => {
      this.jobgraderSlides.push({
        image: `../../assets/jobgrader-walkthrough/headlines/${n}.svg`,
        header: this.translateProviderService.instant(`LEANWALKTHROUGH.header.${n}`),
        content: this.translateProviderService.instant(`LEANWALKTHROUGH.content.${n}`),
        background: `../../assets/jobgrader-walkthrough/backgrounds/${n}.png`,
      })
    })
  }

  async ngOnInit() {
    this.isLoaded = false;
    this.videoShouldStart = false;
    await this.signProvider.unbrandDeviceFromUser();
    await this.secureStorage.removeValue(SecureStorageKey.sign, false);
    await this.secureStorage.removeValue(SecureStorageKey.personalInfo, false);
    if(environment.production) {
      await this.secureStorage.setValue(SecureStorageKey.toggleChatEncryption, true.toString());
    }
    this.signProvider.signUpForm = {};
    this.signProvider.personalInformation = {};


    var theme = this._ThemeSwitcherService.getCurrentTheme();
    var lang = await this.translateProviderService.getLangFromStorage();
    // this.sliderColor = 0 ; //255; // (theme == 'dark') ? 255 : 0; // previous setting 255 in case theme == dark, else 0
    console.log(`theme: ${theme}`);
    console.log(`lang: ${lang}`);
    var folder = (theme == 'dark' ? `${lang}-${theme}` : `${lang}-light`) ;
    this.altWeb3Screenshot = (lang == 'de') ? `../../assets/alt-walkthrough/${folder}/START.png`: `../../assets/alt-walkthrough/${folder}/START.png`;

    await this.slides.lockSwipes(true);
    this.locked = true;
    this.isLoaded = true;
    this.addAppropriatePaddingWeb3Logo3();
  }

  customVibrate(ms) {
    if (this.platform.is('android')) {
      this.vibration.vibrate(0);
      this.vibration.vibrate(ms);
    } else if (this.platform.is('ios')) {
      if (ms < 100) {
        Haptics.selectionStart;
      } else if (ms < 200) {
        Haptics.impact({ style: ImpactStyle.Medium });
      } else if (ms < 300) {
        Haptics.impact({ style: ImpactStyle.Heavy });
      } else {
        this.vibration.vibrate(500);
      }
    }
  }

// Max für die Snare
// Mid für die Bass
// Max Start für den Start
// Long (Max) mit 200 mS für die Buzzer
// Very Long (Max) mit 400 mS für den finalen Buzzer
// Max für den Waterdrop ganz am Schluss (nicht Teil des Videos)

  videoStart() {

    var vibrationSanu = () => {
      setTimeout(() => { this.customVibrate(Intensity.START);  }, 180);
      setTimeout(() => { this.customVibrate(Intensity.MID);  }, 1040);
      setTimeout(() => { this.customVibrate(Intensity.MAX);   }, 2040);
      setTimeout(() => { this.customVibrate(Intensity.MID);  }, 1130);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 2140);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 2230);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 3010);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 3050);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 3160);
      setTimeout(() => { this.customVibrate(Intensity.LONG);  }, 4010);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 4220);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 5120);
      setTimeout(() => { this.customVibrate(Intensity.LONG);  }, 5180);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 6120);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 6240);
      setTimeout(() => { this.customVibrate(Intensity.LONG);  }, 7090);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 8040);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 8200);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 9050);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 9130);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 9160);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 9200);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 10060);
      setTimeout(() => { this.customVibrate(Intensity.LONG);  }, 10170);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 11120);
      setTimeout(() => { this.customVibrate(Intensity.LONG);  }, 12080);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 13030);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 13090);
      setTimeout(() => { this.customVibrate(Intensity.MID);   }, 13140);
      setTimeout(() => { this.customVibrate(Intensity.VERYLONG);  }, 13240);
      setTimeout(() => { this.customVibrate(Intensity.MAX);  }, 14200);
    }

    var vid = document.getElementsByTagName("video");
    console.log(vid.length);

    // for (var i = 0; i< vid.length; i++) {
    //   vid[i].volume=0.15;
    // }

    // if (this.platform.is('android') && this.platform.is('hybrid')) {
    //   vibrationSanu();
    // }
    if(this.platform.is('ios') && this.platform.is('hybrid')) {
      console.log('Initiating full screen request');
      // vibrationSanu();
      for( var i = 0; i < vid.length; i++) {
        vid[i].controls = false;
        vid[i].setMediaKeys(null).catch(e => console.log(e));
        if (vid[i].requestFullscreen) {
          // console.log('Option 1');
          vid[i].requestFullscreen();
        } else if ((vid[i] as any).mozRequestFullScreen) {
          // console.log('Option 2');
          (vid[i] as any).mozRequestFullScreen();
        } else if ((vid[i] as any).webkitRequestFullscreen) {
          // console.log('Option 3');
          (vid[i] as any).webkitRequestFullscreen();
        } else if ((vid[i] as any).msRequestFullscreen) {
          // console.log('Option 4');
          (vid[i] as any).msRequestFullscreen();
        }
      }
    }
  }

  async videoEnd() {
    this.videoFinished = true;
    // this.customVibrate(Intensity.LONG);
    var vid = document.getElementsByTagName("video");
    for( var i =0 ; i< vid.length; i++) {
      vid[i].remove();
    }
    if (this.activeSlideIndex == 1) {
      await this.slides.slideNext();
      await this.nativeAudio.play('drop_start_app');
    }

  }



  addAppropriatePaddingWeb3Logo3() {
    try {
      setTimeout(() => {
        var images = document.getElementsByClassName("web3Logo3");
        // 1.9416666666666667 => initial: 82% => 73%
        console.log(images);
        console.log(images.length);
        if ( images.length > 0 ) {
          var aspectRatio = (window as any).innerHeight / (window as any).innerWidth;
          console.log(aspectRatio);
          if(aspectRatio >= 2.16) {
            for(var i = 0; i < images.length; i++) {
              (images[i] as HTMLImageElement).style.width = `85%`;
            }
          }
          // else if(aspectRatio <= 1.78) {
          //   for(var i = 0; i < images.length; i++) {
          //     (images[i] as HTMLImageElement).style.width = `70%`;
          //   }
          // }
          else {
            var width = Math.floor((21 * (aspectRatio - 1.78) / 0.38) + 64);
            console.log(width);
            for(var i = 0; i < images.length; i++) {
              (images[i] as HTMLImageElement).style.width = `${width}%`;
            }
          }
        }
      }, 200)

    } catch(e) {
      console.log(e)
    }
  }

  addAppropriatePadding() {
    try {
      var images = document.getElementsByClassName("web3slide-headerImage");
      if ( images.length > 0 ) {
        var aspectRatio = (window as any).innerWidth / (window as any).innerHeight;
        // var padding = Math.floor((15 * aspectRatio) + 5);
        var padding = Math.floor((140 * aspectRatio) - 63.4);
        // 0.46 => 1% padding
        // 0.56 => 15% padding
        for(var i = 0; i < images.length; i++) {
          (images[i] as HTMLImageElement).style.paddingLeft = `${padding}%`;
          (images[i] as HTMLImageElement).style.paddingRight = `${padding}%`;
          (images[i] as HTMLImageElement).style.display = `flex`;
          (images[i] as HTMLImageElement).style.zIndex = "3";
        }
      }
    } catch(e) {
      console.log(e)
    }
  }

  changeSliderColour() {
    try {
      setTimeout(() => {
        var theme = this._ThemeSwitcherService.getCurrentTheme();
        // console.log(theme);
        var bullets = document.getElementsByClassName("pagination--bullet");
        // console.log(bullets);
        for (var bullet = 0; bullet < bullets.length; bullet++) {
          (bullets[bullet] as any).style.background = (theme == 'dark') ? "rgb(255,255,255,0.25)" : "rgb(0,0,0,0.25)";
        }
        var activeBullet = document.getElementsByClassName("pagination--bullet__active")[0];
        // console.log(activeBullet);
        (activeBullet as any).style.background = (theme == 'dark') ? 'white' : 'black';
       }, 500)
    } catch(e) {
      console.log(e);
    }
  }

  public async goToNextSlide() {
    if(await this.slides.isEnd()) {
      await this.showPermissionModal();
    } else {
      await this.slides.lockSwipes(false);
      this.locked = false;
      await this.slides.slideNext();
      this.addAppropriatePadding();
      this.changeSliderColour();
    }
  }

  public async animateForward() {
    const currentIndex = await this.slides.getActiveIndex();
    
    this.activeSlideIndex = currentIndex;

    this.isFirstSlide = false;

    // switch (currentIndex) {
    //   case 1:
    //     this.elementTopFadeIn = true;
    //     this.elementTopFadeOut = false;

    //     this.elementBlueFadeIn = true;
    //     this.elementBlueFadeOut = false;
    //     break;
    //   case 2:
    //     this.elementTopMove = !this.elementTopMove;
    //     break;
    //   case 3:
    //     break;
    //   case 4:
    //     this.elementBottomMove = !this.elementBottomMove;
    //     break;
    //   default:
    //     break;
    // }

  }

  // @ansik: this does have some UX problems and doesn't make a lot sense
  // async lastSlideDrag() {
  //   const currentIndex = await this.slides.getActiveIndex();
  //   const length = await this.slides.length();
  //   const swiper = await this.slides.getSwiper();
  //   console.log(swiper.swipeDirection)
  //   if (currentIndex + 1 === length) {
  //     this.goToModNav();
  //   }
  // }

  public async animateBackward() {
    const currentIndex = await this.slides.getActiveIndex();
    this.activeSlideIndex = currentIndex;

    if (currentIndex === 0) { this.isFirstSlide = true; }

    // switch (currentIndex) {
    //   case 0:
    //     this.elementTopFadeIn = false;
    //     this.elementTopFadeOut = true;

    //     this.elementBlueFadeIn = false;
    //     this.elementBlueFadeOut = true;
    //     break;
    //   case 1:
    //     this.elementTopMove = !this.elementTopMove;
    //     break;
    //   case 2:

    //     break;
    //   case 3:
    //     this.elementBottomMove = !this.elementBottomMove;
    //     break;
    //   default:
    //     break;
    // }
  }


  public goToModNav() {
    // IMPORTANT: we set this here instead in the slidesWillChange method
    // this needs to be set just after the click on SIGN UP before navigatint
    // to the sign-up pag
    this.signProvider.setOnboardingHasRun(false);

    this.secureStorage.getValue(SecureStorageKey.browseMode, false).then(browseMode => {
      if(browseMode != true.toString()) {
        this.secureStorage.removeValue(SecureStorageKey.browseMode, false).then(() => {
          this.nav.navigateForward('/sign-up');
        })
      } else {
        this.secureStorage.removeValue(SecureStorageKey.browseMode, false).then(() => {
          this.nav.navigateForward('/dashboard/tab-home');
        })
      }
    })
  }

  public goToSignUp() {
    this.slides.slideNext();
  }

  public goToWeb3Wallet() {
    this.secureStorage.removeValue(SecureStorageKey.browseMode, false).then(() => {
      this.secureStorage.setValue(SecureStorageKey.navChoice, 'web3').then(() => {
        this.nav.navigateForward('/sign-up');
      })
    })
  }

  public async slidesWillChange() {
    this.hasStarted = true;
    // var prevVol = null;
    const length = await this.slides.length();
    const currentIndex = await this.slides.getActiveIndex();
    
    if (currentIndex + 1 === length) {
      // @ansik: does it makes sense to set onboarding has run at this point??
      // this needs to be set just after the click on SIGN UP before navigatint
      // to the sign-up pag so I add this in the goToNavMode method
      // this.signProvider.setOnboardingHasRun(false);
      this.isLastSlide = true;
    } else {
      this.isLastSlide = false;
    }
  }

  public ghostArray(n: number) {
    return Array(n);
  }

  public goToSignIn() {
    // if(this.platform.is('hybrid') && this.platform.is('android')) {
    //   this._Adjust.snippet();
    // }
    this.nav.navigateForward('/login?from=introvideo');
  }

  public async goToHome() {
    await this.secureStorage.setValue(SecureStorageKey.browseMode, "true");
      // await this.slides.lockSwipes(false);
      // this.videoShouldStart = true;
      // this.slides.slideNext();
      // this.addAppropriatePadding();
      // this.changeSliderColour();
    // await this.showPermissionModal();
    this.nav.navigateForward('/dashboard/tab-home');
    
  }

  public goPinEntry() {
    this._AlertController.create({
      mode: 'ios',
      header: this.translateProviderService.instant('CONTACT.title'),
      buttons: [ { text: this.translateProviderService.instant('GENERAL.ok'), handler: () => { } }]
      }).then(alert => alert.present())
  }

  goBackSlide() {
    this.slides.slideTo(this.activeSlideIndex - 1);
  }

  async showPermissionModal() {
    // var check = await this._Adjust.hasUserAllowedTracking();
    // console.log("showPermissionModal: " + check);
    // if(check) {
      // this._Adjust.snippet();
      this.goToModNav();
    // } else {

      // if(await this._Adjust.isUserYetToAllowTracking()){
        // this._Adjust.snippet();
        // this.goToModNav();
      // } else {
      //   this._AlertController.create({
      //     mode: 'ios',
      //     header: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.header'),
      //     message: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.message'),
      //     buttons: [
      //       {
      //         text: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.cancel'),
      //         role: 'cancel',
      //         handler: () => {
      //             this.goToModNav();
      //         }
      //       },
      //       {
      //           text: this.translateProviderService.instant('USERTRACKING.tracking-permission-false.settings'),
      //           handler: () => {
      //               this._OpenNativeSettings.open('tracking')
      //               .then(_ => { App.exitApp(); } )
      //               .catch(e => {
      //                 console.log(e)
      //                 this._OpenNativeSettings.open('privacy')
      //                 .then(_ => { App.exitApp(); } )
      //                 .catch(e => {
      //                   console.log(e)
      //                 })
      //               })
      //           }
      //       }]
      //   }).then(alert => {
      //     alert.present();
      //     // this.goToModNav();
      //   })
      // }
    // }
  }

}
