import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ResetPassword } from '../../../core/models/ResetPassword';
import { ResetPasswordProviderService } from '../../../core/providers/reset-password/reset-password-provider.service';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { ApiProviderService } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { NetworkService } from 'src/app/core/providers/network/network-service';

@Component({
  selector: 'app-password-step-1',
  templateUrl: './step-1.page.html',
  styleUrls: ['./step-1.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResetPasswordStepOnePage implements OnInit, OnDestroy {
  @ViewChild(IonContent, {}) content: IonContent;
  resetPasswordForm: UntypedFormGroup;
  verificationStatus: number;
  notValidForm = false;
  signProcessing = false;
  notUniqueUsername = true;
  private ngOnInitExecuted: any;
  private additionalStyleSheetId: string;

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private resetPasswordProvider: ResetPasswordProviderService,
    private utilityService: UtilityService,
    private elemRef: ElementRef,
    private api: ApiProviderService,
    private translateProviderService: TranslateProviderService,
    private toastController: ToastController,
    private _NetworkService: NetworkService
  ) {
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** @inheritDoc */
  async ngOnInit() {
    this.ngOnInitExecuted = true;
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
    await this.componentInit();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async presentToast(message: string) {
    // const translations = {
    //   en: 'All fields should be checked',
    //   de: 'All fields should be checked',
    //   hi: 'All fields should be checked'
    // };

    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  async nextStep() {
    this.resetPasswordForm.markAsDirty();
    this.signProcessing = true;
    this.notUniqueUsername = true;

    if (!this.resetPasswordForm.valid) {
      return false;
    }

    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }

    this.notUniqueUsername = await this.api.checkUserName(this.resetPasswordForm.value.resetUsername);
    if (!this.notUniqueUsername) {
      this.presentToast(this.translateProviderService.instant('LOGIN.userdoesnotexist'));
      return false;
    }
    await this.resetPasswordProvider.saveResetPasswordData(this.resetPasswordForm.value, 1);
    await this.api.resetPassword(this.resetPasswordForm.value.resetUsername).catch(e => console.log(e));
    this.signProcessing = false;
    this.nav.navigateForward('/reset-password/step-2');
  }

  async skipReg() {
    await this.resetPasswordProvider.skipReset();
  }

  prevStep() {
    this.nav.navigateBack('/login?from=resetpassword');
  }

  private async componentInit() {
    this.resetPasswordForm = new UntypedFormGroup({
      resetUsername: new UntypedFormControl('', [
        Validators.required,
        this.validateUsername
      ])
    });
    const dataFromStorage: ResetPassword =
      await this.resetPasswordProvider.getResetPasswordData();

    this.setFormDataFromStorage(dataFromStorage);
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private setFormDataFromStorage(data: ResetPassword): void {
    if (data && data.reset_username) {
      this.resetPasswordForm.controls.resetUsername.setValue(data.reset_username);
    }
  }

  private validateUsername(formControl: UntypedFormControl) {

    const userNameString = formControl.value;
    const hasEmptySpaces = userNameString.match(/[\s]/);
    const hasSpecialCharacters = userNameString.match(/[\W\s]/);
    const hasBetweenMinAndMaxCharacters = userNameString.match(/^[\w]{8,25}$/);
    const hasBlockLetters = userNameString.match(/[A-Z]/);
    const hasSmallLetter = userNameString.match(/[a-z]/);
    const hasUnderscore = userNameString.match(/[_]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = userNameString.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasSpecialCharacters === null &&
      hasUnderscore === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters == null
      );

    return (isValid ? null : {
        validateUsername: {
          valid: false
        }
      }
    );
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
