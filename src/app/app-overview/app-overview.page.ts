import { Component, OnInit } from '@angular/core';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { AppUpdatesService } from '../core/providers/app-updates/app-updates.service';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { LoaderProviderService } from '../core/providers/loader/loader-provider.service';
import { environment } from '../../environments/environment';
import { NetworkService } from '../core/providers/network/network-service';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';

@Component({
  selector: 'app-app-overview',
  templateUrl: './app-overview.page.html',
  styleUrls: ['./app-overview.page.scss'],
})
export class AppOverviewPage implements OnInit {

  public app: string;
  public name: string;
  public code: any;
  public number: string;
  public helixEnv: string = environment.helixEnv;
  public userImage: string;

  constructor(
    private nav: NavController,
    private appVersion: AppVersion,
    private platform: Platform,
    private translate: TranslateProviderService,
    private alertController: AlertController,
    private loader: LoaderProviderService,
    private appUpdate: AppUpdatesService,
    private _NetworkService: NetworkService,
    private _SecureStorage: SecureStorageService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {

    this.appVersion.getAppName()
    .then((app) => {
      this.app = app;
    });

    this.appVersion.getPackageName()
    .then((name) => {
      this.name = name;
    });

    this.appVersion.getVersionCode()
    .then((code) => {
      this.code = code;
    });

    this.appVersion.getVersionNumber()
    .then((number) => {
      this.number = number;
    });


    const userImage = this.userPhotoServiceAkita.getPhoto();
    this.userImage = !!userImage ? userImage : '../../assets/job-user.svg';

  }

  ngOnInit() {}

  goBackToSettings() {
    console.log("goBackToSettings");
    // this.nav.back();
    this.nav.navigateBack('/dashboard/tab-settings');
  }

  async checkForUpdate() {
    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }
    await this.loader.loaderCreate();
    var updateValue = await this.appUpdate.checkForUpdate().catch(e => {
      this.loader.loaderDismiss();
    });

    if(updateValue) {
      console.log(updateValue);
      switch(updateValue){
        case -3: await this.showAlert(-3, this.translate.instant('APPSTORE.endpoint-error')); break;
        case -2: await this.showAlert(-2, this.translate.instant('APPSTORE.platform-not-supported')); break;
        case -1: await this.showAlert(-1, this.translate.instant('SETTINGSABOUT.case-minus-one')); break;
        case 0: await this.showAlert(0, this.translate.instant('SETTINGSABOUT.case-zero')); break;
        case 1: await this.showAlert(1, this.translate.instant('SETTINGSABOUT.case-plus-one')); break;
        default: await this.showAlert(-2, this.translate.instant('APPSTORE.endpoint-error')); break;
      }
    }

  }

  async showAlert(code: number, message: string) {
    await this.loader.loaderDismiss();
    var buttons = [{
      text: this.translate.instant('SIGNSTEPEIGHT.selectOk'),
      role: 'cancel',
      handler: () => {}
    }]
    if(code > 0) {
      buttons.push({
        text: this.translate.instant('APPSTORE.update'),
        role: 'primary',
        handler: async () => {
            const link = document.createElement('a');
            document.body.appendChild(link);
            link.setAttribute('style', 'display: none');
            link.href = this.platform.is('ios') ? 'https://apps.apple.com/de/app/helixid/id1469238013' : 'market://details?id=app.jobgrader.crowdworker';
            link.click();
            link.remove();
        }
      })
    }
    const alert = await this.alertController.create({
      mode: 'ios',
      header: '',
      message: message,
      buttons: buttons
    });
    await alert.present();
  }

}
