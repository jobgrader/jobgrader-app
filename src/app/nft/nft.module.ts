import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NftPage } from './nft.page';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../shared/shared.module';
import { NftDetailModalModule } from './nft-detail/nft-detail.module';
import { WcSessionModule } from './wc-session/wc-session.module';
import { Wc2SessionModule } from './wc2-session/wc2-session.module';
import { DidPopoverModule } from '../merchant-details/did-popover/did-popover.module';
import { JobHeaderModule } from '../job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    component: NftPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    SharedModule,
    DidPopoverModule,
    NftDetailModalModule,
    JobHeaderModule,
    Wc2SessionModule,
    WcSessionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NftPage]
})
export class NftPageModule {}
