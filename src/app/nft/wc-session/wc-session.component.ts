import { Component, Input, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { WalletConnectService } from 'src/app/core/providers/wallet-connect/wallet-connect.service';

@Component({
  selector: 'app-wc-session',
  templateUrl: './wc-session.component.html',
  styleUrls: ['./wc-session.component.scss'],
})
export class WcSessionComponent implements OnInit {
  @Input() data: any;
  public profileImage;
  public keyValue = [];
  public peerName: string = '';
  public peerAddress: string = '';
  public peerDescription: string = '';

  constructor(
    private _ModalController: ModalController,
    private _AlertController: AlertController,
    private _Translate: TranslateProviderService,
    private _WalletConnect: WalletConnectService
  ) { }

  ngOnInit() {
    console.log(this.data);
    // "protocol"
    // 1 "version"
    // 2 "_bridge"
    // 3 "_key"
    // 4 "_clientId"
    // 5 "_clientMeta"
    // 6 "_peerId"
    // 7 "_peerMeta"
    // 8 "_handshakeId"
    // 9 "_handshakeTopic"
    // 10 "_connected"
    // 11 "_accounts"
    // 12 "_chainId"
    // 13 "_networkId"
    // 14 "_rpcUrl"
    // 15 "_eventManager"
    // 16 "_cryptoLib"
    // 17 "_sessionStorage"
    // 18 "_qrcodeModal"
    // 19 "_qrcodeModalOptions"
    // 20 "_signingMethods"
    // 21 "_transport"

    this.profileImage = !!this.data._peerMeta ? this.data._peerMeta.icons[0] : this.data.peerMeta.icons[0];
    this.peerName = !!this.data._peerMeta ? this.data._peerMeta.name : this.data.peerMeta.name;
    this.peerAddress = this.data.targetMerchantAccount;
    this.peerDescription = !!this.data._peerMeta ? this.data._peerMeta.description : this.data.peerMeta.description;
    
    this.keyValue.push({
      key: 'WalletConnect version',
      value: !!this.data.version ? this.data.version : ''
    },{
      key: 'Bridge',
      value: !!this.data._bridge ? this.data._bridge : this.data.bridge
    },{
      key: 'Chain ID',
      value: !!this.data._chainId ? this.data._chainId : this.data.chainId
    },{
      key: this._Translate.instant('WALLETCONNECT.connected-question'),
      value: Object.keys(this.data).includes('_connected') ? ((this.data._connected.toString() == 'true') ? this._Translate.instant('WALLETCONNECT.yes') : this._Translate.instant('WALLETCONNECT.no')) : ((this.data.connected.toString() == 'true') ? this._Translate.instant('WALLETCONNECT.yes') : this._Translate.instant('WALLETCONNECT.no'))
    })

  }

  endSession() {
    this.dismiss(false);
  }

  close() {
    this.dismiss(true);
  }

  dismiss(value: boolean) {
    this._ModalController.dismiss({ value });
  }

}
