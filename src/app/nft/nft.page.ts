import { Component } from '@angular/core';
import { ActionSheetController, AlertController, ModalController, NavController, PopoverController, ToastController } from '@ionic/angular';
import { SecureStorageService } from '../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../core/providers/secure-storage/secure-storage-key.enum';
import { NftService, OpenSeaMethodTypes } from '../core/providers/nft/nft.service';
import { Router } from '@angular/router';
import { CLIENT_META, WalletConnectService } from '../core/providers/wallet-connect/wallet-connect.service';
import { NftDetailComponent } from './nft-detail/nft-detail.component';
import { WcSessionComponent } from './wc-session/wc-session.component';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { ImageSelectionService } from '../shared/components/image-selection/image-selection.service';
import { DidPopoverComponent } from '../merchant-details/did-popover/did-popover.component';
import { NetworkService } from '../core/providers/network/network-service';
import WalletConnect from '@walletconnect/client';
import { environment } from 'src/environments/environment';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { KycService } from '../kyc/services/kyc.service';
import { Wc2SessionComponent } from './wc2-session/wc2-session.component';
import { interval, Subscription } from 'rxjs';
import { getSdkError } from "@walletconnect/utils";
import { Core } from '@walletconnect/core';
import { Web3Wallet } from '@walletconnect/web3wallet';
import { VaultSecretKeys, VaultService } from '../core/providers/vault/vault.service';
import { UserPhotoServiceAkita } from '../core/providers/state/user-photo/user-photo.service';
import { ApiProviderService } from '../core/providers/api/api-provider.service';
import { UserProviderService } from '../core/providers/user/user-provider.service';
import { SocialMediaService } from '../core/providers/social-media/social-media.service';
import { OwnedNft } from 'alchemy-sdk';
import { AlchemyService } from '../core/providers/alchemy/alchemy.service';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';
import { MoralisService } from '@services/moralis/moralis.service';


export interface NftDisplayElements {
  id: number;
  contract_address: string;
  created_date: number;
  name: string;
  owner: string;
  description: string;
  external_link: string;
  image_url: string;
  permalink: string;
  banner_image_url: string;
  username: string;
  profile_img_url: string;
  owner_address: string;
  creator_address: string;
}

export interface AlchemyNfts {
  is_spam: boolean; // contract: NftContractForNft isSpam;
  balance: string;
  token_id: string; // tokenId: string
  name: string;
  description: string;
  image_uri: string;
  token_uri: string; // use this for permalink
  token_standard: string; // NtokenType: NftTokenType;
  collectionName: string; //  collection: BaseNftCollection;
  contract: string; // contract: NftContractForNft address: string;
  created_at: string; // mint?: NftMint;
  updated_at: string; // timeLastUpdated: string;
  attributes: Array<any>; // NftRawMetadata metadata: Record<string, any>;
}

export interface NftDisplayElements2 {
  attributes: Array<any>;
  identifier: string;
  collection: string;
  contract: string;
  token_standard: string;
  name: string;
  description: any;
  image_url: string;
  metadata_url: string;
  created_at: string;
  updated_at: string;
  is_disabled: boolean;
  is_nsfw: boolean;
  permalink: string;
}

export interface PoapNFT {
  event_id: number;
  event_fancy_id: string;
  event_name: string;
  event_event_url: string;
  event_image_url: string;
  event_country: string;
  event_city: string;
  event_description: string;
  event_year: number,
  event_start_date: string;
  event_end_date: string;
  event_expiry_date: string;
  event_supply: number
  tokenId: string
  owner: string;
  chain: string;
  created: string;
}

export interface DecentralandCollectible {
  name: string;
  image: string;
  owner: string;
  collectibleName: string;
  rarity: string;
  url: string;
  createdAt: number;
}

const DECENTALAND_COLLECTABLES_TESTING_ACCOUNT = "0x04fc3e8853872a6648d74a9c77da6429eda7ab63";

const INITITAL_ONBOARDING_POINTS = 20;
const INITITAL_ONBOARDING_POINTS_WIDTH = 25;

const WEB3_WALLET_POINTS = 15;
const WEB3_WALLET_POINTS_WIDTH = 25;

const POAP_POINTS = 10;
const POAP_POINTS_WIDTH = 25;

const STAGE_1_MAX = 100;

@Component({
  selector: 'app-nft',
  templateUrl: './nft.page.html',
  styleUrls: ['./nft.page.scss'],
})
export class NftPage {

  public profilePictureSrc = '../../assets/job-user.svg';
  private qrcode;
  public mode: string;
  private chartDataset = [];
  public tokenBalance = 0;
  public loyaltyPoints = 0;
  public address: string = null;
  public trackAddress: string = null;

  public pageHeading = "";
  public displayVerifiedTick = false;

  public activeSessions = [];
  public activeSessions2 = [];

  private timestampObserver = interval(1000);
  private timestampSubscription: Subscription;

  // public userNfts: Array<NftDisplayElements> = [

  // ];

  nftChartData = {
    "Spams": 0,
    "Safe": 0,
    "POAP": 0,
    // "Decentraland Wearables": 0
  };

  public userNfts2: Array<AlchemyNfts> = [
//   {
//       "identifier": "427943",
//       "collection": "uptrip-cards",
//       "contract": "0xa59b69e1fad081af56c472c03d9b7f80af847c9c",
//       "token_standard": "erc721",
//       "name": "Welcome",
//       "description": null,
//       "image_url": "https://i.seadn.io/gcs/files/21863bcb8a51ed81ba8a9319257556ef.png?w=500&auto=format",
//       "metadata_url": "https://api.uptrip.app/card_metadata/427943",
//       "created_at": "2023-09-04T21:00:24.144524",
//       "updated_at": "2023-09-04T21:00:26.930094",
//       "is_disabled": false,
//       "is_nsfw": false
//   },
//   {
//       "identifier": "428109",
//       "collection": "uptrip-cards",
//       "contract": "0xa59b69e1fad081af56c472c03d9b7f80af847c9c",
//       "token_standard": "erc721",
//       "name": "Salzburg",
//       "description": "Salzburg, birthplace of Mozart, is known for its beautiful baroque architecture, stunning alpine scenery, and annual Salzburg Festival",
//       "image_url": "https://i.seadn.io/gcs/files/512e460ca490e0756fbf3e2c2f154682.png?w=500&auto=format",
//       "metadata_url": "https://api.uptrip.app/card_metadata/428109",
//       "created_at": "2023-09-04T21:00:24.053741",
//       "updated_at": "2023-09-04T21:00:27.191492",
//       "is_disabled": false,
//       "is_nsfw": false
//   },
//   {
//       "identifier": "428110",
//       "collection": "uptrip-cards",
//       "contract": "0xa59b69e1fad081af56c472c03d9b7f80af847c9c",
//       "token_standard": "erc721",
//       "name": "Frankfurt",
//       "description": "Home to the European Central Bank and the Frankfurt Stock Exchange, Frankfurt is a major financial center in Europe and is sometimes called \"Bankfurt\"",
//       "image_url": "https://i.seadn.io/gcs/files/f091331b5278a76af6f120732f72ac62.png?w=500&auto=format",
//       "metadata_url": "https://api.uptrip.app/card_metadata/428110",
//       "created_at": "2023-09-04T21:00:23.933523",
//       "updated_at": "2023-09-04T21:00:26.557355",
//       "is_disabled": false,
//       "is_nsfw": false
//   },
//   {
//     "identifier": "88935341679913222953419014966026917231465911852332564137032628324535643940977",
//     "collection": "ens",
//     "contract": "0x57f1887a8bf19b14fc0df6fd9b2acc9af147ea85",
//     "token_standard": "erc721",
//     "name": "ansikmahapatra.eth",
//     "description": "This is an unknown ENS name with the hash: 0xc49f9e9bb9e93c3e49a9149538ac962c04549edd701047f3f17e9775b2d82871",
//     "image_url": "https://i.seadn.io/gae/0cOqWoYA7xL9CkUjGlxsjreSYBdrUBE0c6EO1COG4XE8UeP-Z30ckqUNiL872zHQHQU5MUNMNhfDpyXIP17hRSC5HQ?auto=format&dpr=1&w=256",
//     "metadata_url": "https://metadata.ens.domains/mainnet/0x57f1887a8bf19b14fc0df6fd9b2acc9af147ea85/88935341679913222953419014966026917231465911852332564137032628324535643940977",
//     "created_at": "2022-09-29T10:38:54.902841",
//     "updated_at": "2022-09-29T10:38:55.221328",
//     "is_disabled": false,
//     "is_nsfw": false
// },
// {
//     "identifier": "79602780085489226049246813704221582101684204940524068403612998400408480120833",
//     "collection": "ansik-collection",
//     "contract": "0x495f947276749ce646f68ac8c248420045cb7b5e",
//     "token_standard": "erc1155",
//     "name": "Grace of the Goddess",
//     "description": "Acryllic on Canvas painting by the artist Bandita Mahapatra",
//     "image_url": "https://i.seadn.io/gae/hTL3R-wKnaweHX1EJlbR20V9mg9BwrOfffRx_QWOdHPHLjbwhU1e7U08uWsV8FhINc8ocxcDoA67Nx1hFUlVtHJsnn2i61xBh94aqfc?w=500&auto=format",
//     "metadata_url": null,
//     "created_at": "2022-05-02T09:23:10.331843",
//     "updated_at": "2022-05-02T09:23:10.394850",
//     "is_disabled": false,
//     "is_nsfw": false
// },
// {
//     "identifier": "79602780085489226049246813704221582101684204940524068403612998399308968493057",
//     "collection": "ansik-collection",
//     "contract": "0x495f947276749ce646f68ac8c248420045cb7b5e",
//     "token_standard": "erc1155",
//     "name": "Kalingadesa Flag",
//     "description": "A concept flag",
//     "image_url": "https://i.seadn.io/gae/sng9RDx00QyGd--u4TuBs2exYB42diRSb_ydNoPQR3LnEF0oSZ9aElbdElTWMKfgUdoYCbMYOfeSXX--AViSVrLrknFZo8FJfneOUQ?w=500&auto=format",
//     "metadata_url": null,
//     "created_at": "2022-04-27T08:49:45.272597",
//     "updated_at": "2022-04-27T08:49:45.320873",
//     "is_disabled": false,
//     "is_nsfw": false
// },
// {
//     "identifier": "79602780085489226049246813704221582101684204940524068403612998398209456865281",
//     "collection": "ansik-collection",
//     "contract": "0x495f947276749ce646f68ac8c248420045cb7b5e",
//     "token_standard": "erc1155",
//     "name": "prabāsi - Emigrant - ପ୍ରବାସୀ",
//     "description": "Test NFT of my first ever track released on music streaming platforms",
//     "image_url": "https://i.seadn.io/gae/TmD8wclFcH7ztRnOjD9LuGJVf9iB9Nq9azZnxBp89_rpXmkkBaWz4FNv_zfU7kNSi-0gmWF0SlGDpFrFEfQWr_P9_mSDCHJ5Op0BWA?w=500&auto=format",
//     "metadata_url": null,
//     "created_at": "2021-12-07T09:37:22.310033",
//     "updated_at": "2021-12-07T17:04:48.689839",
//     "is_disabled": false,
//     "is_nsfw": false
// },
// {
//     "identifier": "0",
//     "collection": "opensea-paymentassets",
//     "contract": "0x0000000000000000000000000000000000000000",
//     "token_standard": "erc20",
//     "name": "Ether",
//     "description": "",
//     "image_url": "https://openseauserdata.com/files/6f8e2979d428180222796ff4a33ab929.svg",
//     "metadata_url": null,
//     "created_at": "2020-01-16T18:26:09.081925",
//     "updated_at": "2023-09-06T09:30:00.170042",
//     "is_disabled": false,
//     "is_nsfw": false
// }

  ];

  public gnosisNfts: Array<AlchemyNfts> = [

  ];

  public epnNfts: Array<AlchemyNfts> = [

  ];

  public poapNfts: Array<PoapNFT> = [
  //   {
  //     "event_id":37824,
  //     "event_fancy_id":"crypto-assets-conference-cac2022a-2022",
  //     "event_name":"Crypto Assets Conference - #CAC2022A ",
  //     "event_event_url":"https://www.crypto-assets-conference.de/",
  //     "event_image_url":"https://assets.poap.xyz/crypto-assets-conference-cac2022a-2022-logo-1649343353251.png",
  //     "event_country":"Germany",
  //     "event_city":"Frankfurt",
  //     "event_description":"This POAP certifies that the holder participated in the Crypto Assets Conference organized by the Frankfurt School Blockchain Center from April 4 to April 6, 2022, in Frankfurt am Main, Germany. The participant can claim this POAP as a digital certificate of attendance.",
  //     "event_year":2022,
  //     "event_start_date":"04-Apr-2022",100px
  //     "event_end_date":"06-Apr-2022",
  //     "event_expiry_date":"05-Aug-2022",
  //     "event_supply":50,
  //     "tokenId":"5000213",
  //     "owner":"0x3c8c011DeC1903532425478075Bf8d6e85891dB7",
  //     "chain":"xdai",
  //     "created":"2022-05-19 15:53:35"
  //  }
  ];

  // public decentralandCollectible: Array<DecentralandCollectible> = [

  // ]

  public safeNfts: Array<AlchemyNfts> = [];

  constructor(
    private _NavController: NavController,
    private router: Router,
    private _SecureStorageService: SecureStorageService,
    private _NftService: NftService,
    public _WalletConnectProvider: WalletConnectService,
    private _ToastController: ToastController,
    private _Translate: TranslateProviderService,
    private _ActionSheetController: ActionSheetController,
    private _AlertController: AlertController,
    private _ImageSelectionService: ImageSelectionService,
    private _PopoverController: PopoverController,
    private _ModalController: ModalController,
    private _KycService: KycService,
    private _NetworkService: NetworkService,
    private _ThemeSwitcher: ThemeSwitcherService,
    private _Vault: VaultService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private _Api: ApiProviderService,
    private _User: UserProviderService,
    private _Alchemy: AlchemyService,
    private _Moralis: MoralisService,
    private firecloud: FirestoreCloudFunctionsService,
    private _SocialMediaService: SocialMediaService
  ) {
   }

   async returnNftNumbers(address: string) {
    var nftNumbersCache = await this._SecureStorageService.getValue(SecureStorageKey.nftNumbersCache, false);
    var nftNumbersCacheParse = !!nftNumbersCache ? JSON.parse(nftNumbersCache) : {};
    return !!nftNumbersCacheParse[address] ? nftNumbersCacheParse[address] : {
      "Spams": 0,
      "Safe": 0,
      "POAP": 0,
      // "Decentraland Wearables": 0
    }
  }

  async updateNftNumbers(address: string, content: any) {
    var nftNumbersCache = await this._SecureStorageService.getValue(SecureStorageKey.nftNumbersCache, false);
    var nftNumbersCacheParse = !!nftNumbersCache ? JSON.parse(nftNumbersCache) : {};
    nftNumbersCacheParse = Object.assign(nftNumbersCacheParse, { [address] : content });
    await this._SecureStorageService.setValue(SecureStorageKey.nftNumbersCache, JSON.stringify(nftNumbersCacheParse));
  }

  async ionViewWillEnter() {

    if( !this._NetworkService.checkConnection() ){
        this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }

    var photo = this.userPhotoServiceAkita.getPhoto();
    this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg';

    var displayVerifiedTick = await this._KycService.isUserAllowedToUseChatMarketplace();
    this.displayVerifiedTick = displayVerifiedTick;


    let checker = this.router.parseUrl(this.router.url).queryParams;
    // console.log(checker);

    if(checker.mode) {
      // console.log(checker.mode);
      this.mode = checker.mode;
    }

    if(checker.trackAddress) {
      this.trackAddress = checker.trackAddress;
    }

    if(this.mode == 'walletconnect') {

        var checkForActiveSessions = () => {
          if(checker.address) {
            this.address = checker.address;
            this.activeSessions = this._WalletConnectProvider.activeSessions.filter(ss => ss.accounts.includes(this.address));
            this.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(this.address) > -1) > -1);
          } else {
            this.activeSessions = this._WalletConnectProvider.activeSessions;
            this.activeSessions2 = this._WalletConnectProvider.activeSessions2;
          }
        }

        checkForActiveSessions();

        this.timestampSubscription = this.timestampObserver.subscribe(() =>
          checkForActiveSessions()
        );

    }

    else if(this.mode == 'loyalty') {
        this.tokenBalance =  await this._NftService.obtainTHXCtoETHPrice(); // this.loyaltyPoints + INITITAL_ONBOARDING_POINTS;

        const cachedEarningsString = await this._SecureStorageService.getValue(SecureStorageKey.cachedEarnings, false);
        const cachedEarnings = !!cachedEarningsString ? JSON.parse(cachedEarningsString) : { "THXC" : 0 };

        const additionalInfo = await this.firecloud.obtainUserData();

        this.loyaltyPoints = cachedEarnings["THXC"] + additionalInfo.points;

        const taskList = await this._SocialMediaService.fetchSocialMediaTasks();
        const myUserStatus = await this._SocialMediaService.fetchSocialMediaUserStatus();
        
        console.info("myUserStatus", myUserStatus);
        const tasksCompleted = taskList.filter((k: any) => !!myUserStatus.find(us => us.taskId == k.taskId));
        console.info("tasksCompleted", tasksCompleted);

        const earnings = Array.from(tasksCompleted, (tc: any) => !!tc.earnings ? tc.earnings : 0);
        const add = (a,b) => { return a+b }

        cachedEarnings["THXC"] = earnings.reduce(add, 0);

        this.loyaltyPoints = cachedEarnings["THXC"] + additionalInfo.points;

        // this.loyaltyPoints = 60;
        // this.tokenBalance = 90;

        console.log("this.loyaltyPoints: ", this.loyaltyPoints);
        console.log("this.tokenBalance: ", this.tokenBalance);
        
        this.d3LoyaltyChart();
        
        this._SecureStorageService.setValue(SecureStorageKey.cachedEarnings, JSON.stringify(cachedEarnings));

    }

    else if(this.mode == 'nft') {

      if(checker.trackAddress) {

        var cachedNftNumbers = await this.returnNftNumbers(checker.trackAddress);

        // this.userNfts = [];
        this.userNfts2 = [];
        this.gnosisNfts = [];
        this.epnNfts = [];
        this.poapNfts = [];
        // this.decentralandCollectible = [];

        this.pageHeading = (checker.trackName) ? `${checker.trackName} NFTs` : this._Translate.instant('NFT.my-nfts');

        this.nftChartData = cachedNftNumbers;
        // this.d3NftChart(this.nftChartData)

        // var res = await this._NftService.openseaAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${checker.trackAddress}`);

        // if(res.assets.length > 0) {
        //   for(let ii = 0; ii< res.assets.length; ii++) {
        //     this.userNfts.push(this.processDisplayElements(res.assets[ii]));
        //   }

        // }

        const res333 = await this._Alchemy.fetchNfts(checker.trackAddress);

        console.log("Alchemy: NFT page: " + JSON.stringify(res333));

        if(res333.length > 0) {
          for(let ii = 0; ii< res333.length; ii++) {
            this.userNfts2.push(this.processAlchemyNft(res333[ii]));
          }

        }

        const res334 = await this._Moralis.fetchNfts(checker.trackAddress);
   

        if(res334.length > 0) {
          for(let ii = 0; ii< res334.length; ii++) {
            this.gnosisNfts.push(await this._Moralis.processDisplayElements(res334[ii]));
          }

        }

        console.info("Gnosis NFTs: " + JSON.stringify(this.gnosisNfts));

        const res335 = await this.firecloud.fetchEpnNfts(checker.trackAddress);
   

        if(res335.length > 0) {
          for(let ii = 0; ii< res335.length; ii++) {
            this.epnNfts.push(await this._Moralis.processDisplayElements(res335[ii]));
          }

        }

        console.info("EPN NFTs: " + JSON.stringify(this.epnNfts));



        // if(!environment.production) {
        //   var res2 = await this._NftService.openseaTestNetAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${checker.trackAddress}`);
        //   if(res2.assets.length > 0) {
        //     for(let iii = 0; iii < res2.assets.length; iii++) {
        //       this.userNfts.push(this.processDisplayElements(res2.assets[iii]));
        //     }

        //   }
        // }

        var res3 = await this._NftService.returnPOAPMintedNFT(checker.trackAddress);
        if(res3.length > 0) {
          for(let iiii = 0; iiii < res3.length; iiii++ ) {
            this.poapNfts.push(this.processPoapNft(res3[iiii]));
          }

        }


        // var res4 = await this._NftService.obtainDecentralandCollectibles(checker.trackAddress);
        // console.log(res4.data);
        // if(res4.data.length > 0) {
        //   for(let iiiii = 0; iiiii < res4.data.length; iiiii++ ) {
        //     var l = await this.processDecentralandCollectible(res4.data[iiiii]);
        //       this.decentralandCollectible.push(l);


        //   }
        // }

        this.nftChartData = {
          "Spams": this.userNfts2.filter(k => !!k.is_spam).length + this.gnosisNfts.filter(k => !!k.is_spam).length + this.epnNfts.filter(k => !!k.is_spam).length,
          "Safe": this.userNfts2.filter(k => !k.is_spam).length + this.gnosisNfts.filter(k => !k.is_spam).length + this.epnNfts.filter(k => !k.is_spam).length,
          "POAP": this.poapNfts.length,
          // "Decentraland Wearables": this.decentralandCollectible.length
        }

        // this.d3NftChart(this.nftChartData);

      } else {

        this.pageHeading = this._Translate.instant('NFT.my-nfts');

        

        var check1 = this.chartDataset.find(cd => cd.count == INITITAL_ONBOARDING_POINTS);
        if(check1) {
          this.chartDataset.find(cd => cd.count == INITITAL_ONBOARDING_POINTS).size = INITITAL_ONBOARDING_POINTS_WIDTH;
        } else {
          this.chartDataset.push({ count: INITITAL_ONBOARDING_POINTS, size: INITITAL_ONBOARDING_POINTS_WIDTH});
        }

        var userPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
        console.log(`userPublicKey ${userPublicKey}`);
        this.userNfts2 = [];
        this.gnosisNfts = [];
        this.epnNfts = [];
        if(userPublicKey) {
          // this.loyaltyPoints = this.loyaltyPoints + WEB3_WALLET_POINTS;

          var check2 = this.chartDataset.find(cd => cd.count == WEB3_WALLET_POINTS);
          if(check2) {
            this.chartDataset.find(cd => cd.count == WEB3_WALLET_POINTS).size = WEB3_WALLET_POINTS_WIDTH;
          } else {
            this.chartDataset.push({ count: WEB3_WALLET_POINTS, size: WEB3_WALLET_POINTS_WIDTH});
          }

          if(!this._NetworkService.checkConnection()) {
            var nfts = await this._SecureStorageService.getValue(SecureStorageKey.nfts, false);
            this.userNfts2 = !!nfts ? JSON.parse(nfts) : [];

            var gnosisNfts = await this._SecureStorageService.getValue(SecureStorageKey.gnosisNfts, false);
            this.gnosisNfts = !!gnosisNfts ? JSON.parse(gnosisNfts) : [];

            var poaps = await this._SecureStorageService.getValue(SecureStorageKey.poaps, false);
            this.poapNfts = !!poaps ? JSON.parse(poaps) : [];

            // this.loyaltyPoints = this.loyaltyPoints + POAP_POINTS;

            var check3 = this.chartDataset.find(cd => cd.count == POAP_POINTS);
            if(check3) {
              this.chartDataset.find(cd => cd.count == POAP_POINTS).size = POAP_POINTS_WIDTH;
            } else {
              this.chartDataset.push({ count: POAP_POINTS, size: POAP_POINTS_WIDTH});
            }

            // this.d3Chart(this.chartDataset);

          } else {

            // var res = await this._NftService.openseaAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${userPublicKey}`);
            // console.log(res.assets.length);
            // if(res.assets.length > 0) {
            //   for(let ii = 0; ii< res.assets.length; ii++) {
            //     var asset = res.assets[ii];
            //     console.log(asset);
            //     this.userNfts.push(this.processDisplayElements(asset));
            //   }
            //   await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts));
            // }

            const res333 = await this._Alchemy.fetchNfts(userPublicKey);

            if(res333.length > 0) {
              for(let ii = 0; ii< res333.length; ii++) {
                this.userNfts2.push(this.processAlchemyNft(res333[ii]));
              }

              await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts2));
            }

            const res334 = await this._Moralis.fetchNfts(userPublicKey);

            if(res334.length > 0) {
              for(let ii = 0; ii< res334.length; ii++) {
                this.gnosisNfts.push(await this._Moralis.processDisplayElements(res334[ii]));
              }
  
              await this._SecureStorageService.setValue(SecureStorageKey.gnosisNfts, JSON.stringify(this.gnosisNfts));
            }

            const res335 = await this.firecloud.fetchEpnNfts(userPublicKey);

            if(res335.length > 0) {
              for(let ii = 0; ii< res335.length; ii++) {
                this.epnNfts.push(await this._Moralis.processDisplayElements(res335[ii]));
              }
  
            }
              

            // if(!environment.production) {
            //   var res2 = await this._NftService.openseaTestNetAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${userPublicKey}`);
            //   console.log(res2.assets.length);
            //   if(res2.assets.length > 0) {
            //     for(let iii = 0; iii < res2.assets.length; iii++) {
            //       var asset2 = res2.assets[iii];
            //       console.log(asset2);
            //       this.userNfts.push(this.processDisplayElements(asset2));
            //     }
            //     await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts));
            //   }
            // }

            var res3 = await this._NftService.returnPOAPMintedNFT(userPublicKey);
            console.log(res3);
            if(res3.length > 0) {
              // this.loyaltyPoints = this.loyaltyPoints + POAP_POINTS;

              var check4 = this.chartDataset.find(cd => cd.count == POAP_POINTS);
              if(check4) {
                this.chartDataset.find(cd => cd.count == POAP_POINTS).size = POAP_POINTS_WIDTH;
              } else {
                this.chartDataset.push({ count: POAP_POINTS, size: POAP_POINTS_WIDTH});
              }

              // this.d3Chart(this.chartDataset);
              for(let iiii = 0; iiii < res3.length; iiii++ ) {
                this.poapNfts.push(this.processPoapNft(res3[iiii]));
              }
              await this._SecureStorageService.setValue(SecureStorageKey.poaps, JSON.stringify(this.poapNfts));
            // } else {
            //   this.d3Chart(this.chartDataset);
            }


            // var res4 = await this._NftService.obtainDecentralandCollectibles(userPublicKey);

            // if(res4.data.length > 0) {
            //   for(let iiiii = 0; iiiii < res4.data.length; iiiii++ ) {
            //     var l = await this.processDecentralandCollectible(res4.data[iiiii]);
            //     this.decentralandCollectible.push(l);
            //   }
            // }


          }

        // } else {
        //   this.d3Chart(this.chartDataset);
        }

        var importedWallets = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
        var ima = !!importedWallets ? JSON.parse(importedWallets) : [];
        if(ima.length > 0) {
          var existingPublicKeys = Array.from(ima, m => (m as any).publicKey);
          existingPublicKeys.forEach(async ep => {
            // var res22 = await this._NftService.openseaAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${ep}`);
            // console.log(res22.assets.length);
            // if(res22.assets.length > 0) {
            //   for(let ii = 0; ii< res22.assets.length; ii++) {
            //     var asset = res22.assets[ii];
            //     console.log(asset);
            //     this.userNfts.push(this.processDisplayElements(asset));
            //   }

            //   await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts));
            // }

            const res333 = await this._Alchemy.fetchNfts(ep);

            if(res333.length > 0) {
              for(let ii = 0; ii< res333.length; ii++) {
                this.userNfts2.push(this.processAlchemyNft(res333[ii]));
              }

              await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts2));
            }


            const res334 = await this._Moralis.fetchNfts(ep);

            if(res334.length > 0) {
              for(let ii = 0; ii< res334.length; ii++) {
                this.gnosisNfts.push(await this._Moralis.processDisplayElements(res334[ii]));
              }
  
              await this._SecureStorageService.setValue(SecureStorageKey.gnosisNfts, JSON.stringify(this.gnosisNfts));
            }

            const res335 = await this.firecloud.fetchEpnNfts(ep);

            if(res335.length > 0) {
              for(let ii = 0; ii< res335.length; ii++) {
                this.epnNfts.push(await this._Moralis.processDisplayElements(res335[ii]));
              }
  
              await this._SecureStorageService.setValue(SecureStorageKey.gnosisNfts, JSON.stringify(this.epnNfts));
            }


            // if(!environment.production) {
            //   var res23 = await this._NftService.openseaTestNetAPICall(OpenSeaMethodTypes.ASSETS, `?owner=${ep}`);
            //   console.log(res23.assets.length);
            //   if(res23.assets.length > 0) {
            //     for(let iii = 0; iii < res23.assets.length; iii++) {
            //       var asset2 = res23.assets[iii];
            //       console.log(asset2);
            //       this.userNfts.push(this.processDisplayElements(asset2));
            //     }

            //     await this._SecureStorageService.setValue(SecureStorageKey.nfts, JSON.stringify(this.userNfts));
            //   }
            // }

            var res3 = await this._NftService.returnPOAPMintedNFT(ep);
            console.log(res3);
            if(res3.length > 0) {
              for(let iiii = 0; iiii < res3.length; iiii++ ) {
                this.poapNfts.push(this.processPoapNft(res3[iiii]));
              }

              await this._SecureStorageService.setValue(SecureStorageKey.poaps, JSON.stringify(this.poapNfts));
            }

            // var res4 = await this._NftService.obtainDecentralandCollectibles(ep);
            // console.log(res4.data);
            // if(res4.data.length > 0) {
            //   for(let iiiii = 0; iiiii < res4.data.length; iiiii++ ) {
            //     var l = await this.processDecentralandCollectible(res4.data[iiiii]);
            //     this.decentralandCollectible.push(l);

            //   }
            // }

            this.nftChartData = {
              "Spams": this.userNfts2.filter(k => !!k.is_spam).length + this.gnosisNfts.filter(k => !!k.is_spam).length + this.epnNfts.filter(k => !!k.is_spam).length,
              "Safe": this.userNfts2.filter(k => !k.is_spam).length + this.gnosisNfts.filter(k => !k.is_spam).length + this.epnNfts.filter(k => !k.is_spam).length,
              "POAP": this.poapNfts.length,
              // "Decentraland Wearables": this.decentralandCollectible.length
            }

            // this.d3NftChart(this.nftChartData);

          })
        }

        this.d3Chart(this.chartDataset);

      }
      this.safeNfts = this.userNfts2.filter(k => !k.is_spam).concat(this.gnosisNfts.filter(k => !k.is_spam)).concat(this.epnNfts.filter(k => !k.is_spam));
      this.updateNftNumbers(this.address, this.nftChartData);
    }

  }

  getReadableLoyaltyPoints(loyaltyPoints) {
    return Number(loyaltyPoints).toLocaleString();
  }

  getReadableTokenBalance(loyaltyPoints) {
    return Number(loyaltyPoints).toFixed(4);
  }

  public ngOnDestroy(): void {
    if (this.timestampSubscription) {
      if (!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }
  }

  private d3LoyaltyChart() {

    const d3 = (window as any).d3;
    const id = "#chart";

    d3.select(id).selectAll("*").remove();
    d3.select(`${id}-legend`).selectAll("*").remove();

    const width = (window as any).innerWidth;
    const height = (window as any).innerWidth;
    const donutWidth = 50;
    const innerRadius = (Math.min(width, height) / 2) - donutWidth;

    const color = [
      '#54BF7B',
      '#FF9C00',
      '#878787',
      'white'];

    const dataset1 = [{
      count: this.tokenBalance,
      size: 40
      }
    ];

    const dataset2 = [{
      count: this.loyaltyPoints,
      size: 30
      }
    ];

  if(this.tokenBalance < STAGE_1_MAX) {
    dataset1.push({ count: STAGE_1_MAX - this.tokenBalance, size: 0 })
  }

  if(this.loyaltyPoints < STAGE_1_MAX) {
    dataset2.push({ count: STAGE_1_MAX - this.loyaltyPoints, size: 0 })
  }

    var svg = d3.select(id)
    .append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

    var pie = d3.layout.pie()
      .value(function(d) { return d.count; })
      .sort(null);

    var createChart=function(svg,outerRadius,innerRadius,fillFunction,className, dataset){

      var arc=d3.svg.arc()
              .innerRadius(innerRadius)
              .outerRadius(outerRadius);

      var path= svg
              .selectAll('.'+className)
              .data(pie(dataset))
              .enter()
              .append("path")
              .attr("class", className)
              .attr("d", arc)
              .attr("fill", fillFunction)
              .attr('transform', 'rotate(-135)');;

        path.transition()
                .duration(1000)
                .attrTween('d', function(d) {
                    var interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, d);
                    return function(t) {
                        return arc(interpolate(t));
                    };
                });


          var chart={ path:path, arc:arc };

          return chart;
      };

      var mainChart1 = createChart(svg, function (d) { return innerRadius + d.data.size}, innerRadius, color[0], 'path1', dataset1);
      var shadowChart1 = createChart(svg, function (d) { return innerRadius + d.data.size - 20}, innerRadius - 20, function(d,i){
        var c=d3.hsl(color[0]);
        return d3.hsl((c.h+5), (c.s -.07), (c.l -.15));
      }, 'path2', dataset1);

      var mainChart2 = createChart(svg, function (d) { return innerRadius + d.data.size - 20}, innerRadius - 20, color[1], 'path3', dataset2);
      var shadowChart2 = createChart(svg, function (d) { return innerRadius + d.data.size - 35}, innerRadius - 35, function(d,i){
        var c=d3.hsl(color[1]);
        return d3.hsl((c.h+5), (c.s -.07), (c.l -.15));
      }, 'path4', dataset2);

  }

  private d3NftChart(data: any) {

    var d3 = (window as any).d3;
    var id = "#nftchart";
    var sum = 0;

    Object.keys(data).forEach(k => sum += data[k]);
    console.log(sum);

    if(sum.toString() == "0") {
      data = {a : 10};
    }

    d3.select(id).selectAll("*").remove();
    d3.select(`${id}-legend`).selectAll("*").remove();
    d3.select(id).attr("style", "text-align: left");
    d3.select(`${id}-legend`).attr("style", "text-align: left");
    var color = [ "#48A9A6", "#8247E5", "#8C8C8C", "#003399" ];

    // var width = d3.select(id).getBoundingClientRect().width;
    // var height = d3.select(id).getBoundingClientRect().width;

    var width = (window as any).innerWidth;
    var height = (window as any).innerWidth;
    var donutWidth = 75;
    var radius = (Math.min(width, height) / 2) - 20;

    var svg = d3.select(id)
    .append("svg")
      .attr("width", width)
      .attr("height", height)
    .append("g")
      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var pie = d3.layout.pie()
      .value(function(d) { return d.value; })
      .sort(null);

    var data_ready = pie(d3.entries(data));

    svg.selectAll('svg')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', d3.svg.arc()
      .innerRadius(radius - donutWidth).outerRadius(radius))
      // .innerRadius(innerRadius)
      //   .outerRadius(function (d) {
      //     return innerRadius + d.data.size;
      //   });
      .attr('fill', function(d, i){ return(color[i]) })
      .style("opacity", 1)

    svg.append("text")
      .attr("text-anchor", "middle")
      .attr("x", "0")
      .attr("y", "-0.5em")
      .attr("font-size", "50px")
      .attr("fill", (this._ThemeSwitcher.getCurrentTheme() == 'dark' ? "white" : "black" )  )
      .text(sum.toString())

    svg.append("text")
      .attr("text-anchor", "middle")
      .attr("x", "0")
      .attr("y", "-0.4em")
      .attr("font-size", "16px")
      .attr("fill", (this._ThemeSwitcher.getCurrentTheme() == 'dark' ? "white" : "black" ) )
      .text("NFTs")

    var topEM = 2.5;

    if(sum.toString() != "0") {
      data_ready.forEach((dd, ii) => {
        svg.append("text")
        .attr("text-anchor", "middle")
        .attr("x", "0")
        .attr("y", "-0.2em")
        .attr("font-size", "12px")
        .append("tspan")
        .attr("x", "0")
        .attr("y", (topEM + (1.2 * ii)).toString() + "em")
        .attr("fill", color[ii])
        .text(`${dd.value} ${dd.data.key}`);
      })
    }

  }

  async processDecentralandCollectible(obb: any) {
    var collectibleNameResponse = await this._NftService.obtainDecentralandCollectibleDetail(obb.nft.contractAddress);
    var collectibleName = '';
    if(collectibleNameResponse.data.length > 0) {
      collectibleName = collectibleNameResponse.data[0].name;
    }
    var ss = <DecentralandCollectible>{
      name: obb.nft.name,
      image: obb.nft.image,
      owner: obb.nft.owner,
      collectibleName: collectibleName,
      rarity: obb.nft.data.wearable.rarity,
      url: obb.nft.url,
      createdAt: obb.nft.createdAt
    }
    return ss;
  }


  processPoapNft(ob: any) {
    var s = <PoapNFT>{
      event_id: ob.event.id,
      event_fancy_id: ob.event.fancy_id,
      event_name: ob.event.name,
      event_event_url: ob.event.event_url,
      event_image_url: ob.event.image_url,
      event_country: ob.event.country,
      event_city: ob.event.city,
      event_description: ob.event.description,
      event_year: ob.event.year,
      event_start_date: ob.event.start_date,
      event_end_date: ob.event.end_date,
      event_expiry_date: ob.event.expiry_date,
      event_supply: ob.event.supply,
      tokenId: ob.tokenId,
      owner: ob.owner,
      chain: ob.chain,
      created: ob.created,
    }
    console.log(s);
    return s;
  }

  processAlchemyNft(ob: OwnedNft) {
    var s = <AlchemyNfts>{
      is_spam: !!ob.contract.isSpam, // contract: NftContractForNft isSpam;
      balance: ob.balance,
      token_id: ob.tokenId, // tokenId: string
      name: !!ob.name ? ob.name : (!!ob.contract.openSeaMetadata ? ob.contract.openSeaMetadata.collectionName : null ),
      description: !!ob.description ? ob.description : (!!ob.contract.openSeaMetadata ? ob.contract.openSeaMetadata.description : null ),
      image_uri: (
        !!ob.collection ?
          (ob.collection.name == "ENS: Ethereum Name Service" ?
            "https://i.seadn.io/gae/0cOqWoYA7xL9CkUjGlxsjreSYBdrUBE0c6EO1COG4XE8UeP-Z30ckqUNiL872zHQHQU5MUNMNhfDpyXIP17hRSC5HQ?auto=format&dpr=1&w=256" :
            (!!ob.image.originalUrl ? ob.image.originalUrl : null))
            : (!!ob.image.originalUrl ? ob.image.originalUrl : null)
          ),
      token_uri: ob.tokenUri, // use this for permalink
      token_standard: ob.tokenType.toString(), // NtokenType: NftTokenType;
      collectionName: !!ob.collection ? ob.collection.name : "", //  collection: BaseNftCollection;
      contract: ob.contract.address, // contract: NftContractForNft address: string;
      created_at: ob.mint.timestamp, // mint?: NftMint;
      updated_at: ob.timeLastUpdated, // timeLastUpdated: string;
      attributes: ob.raw.metadata.attributes // NftRawMetadata metadata: Record<string, any>;
    }
    console.log(s);
    return s;
  }

  processDisplayElements2(ob: any) {

    var s = <NftDisplayElements2>{
      attributes: !!ob.attributes ? ob.attributes : [],
      identifier: !!ob.identifier ? ob.identifier : null,
      collection: !!ob.collection ? ob.collection : null,
      contract: !!ob.contract ? ob.contract : null,
      token_standard: !!ob.token_standard ? ob.token_standard : null,
      name: !!ob.name ? ob.name : null,
      description: !!ob.description ? ob.description : null,
      image_url: (ob.collection == "ens" ? "https://i.seadn.io/gae/0cOqWoYA7xL9CkUjGlxsjreSYBdrUBE0c6EO1COG4XE8UeP-Z30ckqUNiL872zHQHQU5MUNMNhfDpyXIP17hRSC5HQ?auto=format&dpr=1&w=256" : (!!ob.image_url ? ob.image_url : null)),
      metadata_url: !!ob.metadata_url ? ob.metadata_url : null,
      created_at: !!ob.created_at ? ob.created_at : null,
      updated_at: !!ob.updated_at ? ob.updated_at : null,
      is_disabled: !!ob.is_disabled ? ob.is_disabled : null,
      is_nsfw: !!ob.is_nsfw ? ob.is_nsfw : null,
      permalink: !!ob.permalink ? ob.permalink : null
    }
    console.log(s);
    return s;

  }

  processDisplayElements(ob: any) {
    var s = <NftDisplayElements>{
      id: !!ob.id ? ob.id : null,
      contract_address: !!ob.asset_contract ? (!!ob.asset_contract.address ? ob.asset_contract.address : null )  : null,
      created_date: !!ob.asset_contract.created_date ? ob.asset_contract.created_date : null,
      name: !!ob.name ? ob.name : null,
      owner: !!ob.creator.owner ? ob.creator.owner : (!!ob.creator.user ? ob.creator.user.username : null ),
      description: !!ob.description ? ob.description : null,
      external_link: !!ob.external_link ? ob.external_link : null,
      image_url: !!ob.image_url ? ob.image_url : null,
      permalink: !!ob.permalink ? ob.permalink : null,
      banner_image_url: !!ob.collection.banner_image_url ? ob.collection.banner_image_url : null,
      username: !!ob.creator.user ? ob.creator.user.username : (!!ob.owner.user ? ob.owner.user.username : null) ,
      profile_img_url: !!ob.creator.profile_img_url ? ob.creator.profile_img_url : null,
      owner_address: !!ob.owner ? (!!ob.owner.address ? ob.owner.address : null) : null,
      creator_address: !!ob.creator ? (!!ob.creator.address ? ob.creator.address : null) : null,
    }
    console.log(s);
    return s;
  }

  showSessionDetail(c: WalletConnect) {
    console.log(c);
    var index = this._WalletConnectProvider.activeSessions.findIndex(k => k['backupUri'] == c['backupUri']);
    console.log(index);
    var detailData = this._WalletConnectProvider.activeSessions[index];

    this._ModalController.create({
      component: WcSessionComponent,
      componentProps: {
        data: detailData
      }
    }).then(modal => {
      modal.onDidDismiss().then(async (data) => {
        console.log(data);
        if(!data.data.value) {
          try {
            var connector = detailData;
            this.presentToast(`${this._Translate.instant('WALLETCONNECT.session-ended-message-1')} ${connector.peerMeta.name} ${this._Translate.instant('WALLETCONNECT.session-ended-message-2')}`);
            connector.killSession();
            if(this.address) {
              this.activeSessions = this._WalletConnectProvider.activeSessions.filter(ss => ss.accounts.includes(this.address));
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(this.address) > -1) > -1);
            } else {
              this.activeSessions = this._WalletConnectProvider.activeSessions;
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2;
            }
          } catch(e) {
            this.presentToast(this._Translate.instant('WALLET.endProcessError'));
          }
        }
      })
      modal.present();
    })
  }

  showSessionDetail2(wcc2: any) {
    // alert(JSON.stringify(wcc2));

    this._ModalController.create({
      component: Wc2SessionComponent,
      componentProps: {
        data: wcc2
      }
    }).then(modal => {
      modal.onDidDismiss().then(async (data) => {
        console.log(data);
        if(!data.data.value) {
          try {
            console.log("closed");
            console.log(wcc2);
            console.log(this._WalletConnectProvider.activeSessions2);
            const projectId = await this._Vault.getSecret(VaultSecretKeys.WALLET_CONNECT_PROJECT_ID);
            const core = new Core({ projectId });
            const web3wallet = await Web3Wallet.init({ core, metadata: CLIENT_META });
            this._WalletConnectProvider.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(this.address) > -1) != -1);
            try {
              await web3wallet.disconnectSession({ topic: wcc2.topic, reason: getSdkError("USER_DISCONNECTED") }).catch(e => {
                this.presentToast(this._Translate.instant('WALLET.endProcessError'));
              });
              console.info("wcc2.pairingTopic", wcc2.pairingTopic);
              console.info("Array.from", Array.from(this._WalletConnectProvider.activeSessions2, w => w.pairingTopic));
              this._WalletConnectProvider.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.pairingTopic.indexOf(wcc2.pairingTopic) == -1);
              this.presentToast(this._Translate.instant('WALLETCONNECT_V2.disconnection_successful'));
            } catch(e) {
              console.log(e);
            }
            console.log("🚀 ~ file: nft.page.ts:675 ~ NftPage ~ modal.onDidDismiss ~ this.activeSessions2:", this._WalletConnectProvider.activeSessions2)
            await this._SecureStorageService.setValue(SecureStorageKey.walletConnect2Sessions, JSON.stringify(this._WalletConnectProvider.activeSessions2));
          } catch(e) {
            console.log(e);
            this.presentToast(this._Translate.instant('WALLET.endProcessError'));
          }
        }
      })
      modal.present();
    })
  }


  private d3Chart(dataset: any) {

    var d3 = (window as any).d3;
    var id = "#chart";

    console.table(dataset);

    d3.select(id).selectAll("*").remove();
    d3.select(`${id}-legend`).selectAll("*").remove();

    if(this.loyaltyPoints < STAGE_1_MAX) {
      dataset.push({ count: STAGE_1_MAX - this.loyaltyPoints, size: 0 })
    }

    var width = (window as any).innerWidth;
    var height = (window as any).innerWidth;
    var donutWidth = 75;
    var innerRadius = (Math.min(width, height) / 2) - donutWidth;

    // var color = d3.scale.category20b();
    var color = [
      '#54BF7B',
      '#FF9C00',
      '#878787',
      'white'];

    // console.log(color);

    var svg = d3.select(id)
    .append('svg')
    .attr('width', width)
    .attr('height', height)
    .append('g')
    .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

    // console.log(svg);

    // var arc = d3.svg.arc()
    //   .innerRadius(innerRadius)
    //   .outerRadius(function (d) {
    //     return innerRadius + d.data.size;
    //   });

    // // console.log(arc);

    var pie = d3.layout.pie()
      .value(function(d) { return d.count; })
      .sort(null);

    var createChart=function(svg,outerRadius,innerRadius,fillFunction,className){

      var arc=d3.svg.arc()
              .innerRadius(innerRadius)
              .outerRadius(outerRadius);

      var path= svg
              .selectAll('.'+className)
              .data(pie(dataset))
              .enter()
              .append("path")
              .attr("class", className)
              .attr("d", arc)
              .attr("fill", fillFunction)
              .attr('transform', 'rotate(-135)');;



        path.transition()
                .duration(1000)
                .attrTween('d', function(d) {
                    var interpolate = d3.interpolate({startAngle: 0, endAngle: 0}, d);
                    return function(t) {
                        return arc(interpolate(t));
                    };
                });


      var chart={ path:path, arc:arc };

      return chart;
  };

  var mainChart = createChart(svg, function (d) { return innerRadius + d.data.size}, innerRadius, function(d, i){ return color[i] }, 'path1');
  var shadowChart = createChart(svg, function (d) { return innerRadius + d.data.size - 20}, innerRadius - 20, function(d,i){
    var c=d3.hsl(color[i]);
    return d3.hsl((c.h+5), (c.s -.07), (c.l -.15));
  }, 'path2');


  }

  private presentToast(message: string) {
    this._ToastController.create({
      message, position: 'top', duration: 2000
    }).then(toast => toast.present())
  }

  showNFTDetail(userNft: AlchemyNfts) {
    this._ModalController.create({
      component: NftDetailComponent,
      componentProps: {
        data: userNft,
        source: (this.firecloud.assessmentContract.toLowerCase() == userNft.contract.toLowerCase()) ? 'assessment' : 'openSea',
        address: this.trackAddress
      }
    }).then(modal => {
      modal.onDidDismiss().then((data) => {

      })
      modal.present();
    })
  }

  showPoapNFTDetail(poap: PoapNFT) {
    console.log(poap);
    this._ModalController.create({
      component: NftDetailComponent,
      componentProps: {
        data: poap,
        source: 'poap'
      }
    }).then(modal => {
      modal.onDidDismiss().then((data) => {

      })
      modal.present();
    })
  }

  showCollectibleDetail(wear: DecentralandCollectible) {
    console.log(wear);
    this._ModalController.create({
      component: NftDetailComponent,
      componentProps: {
        data: wear,
        source: 'decentralandCollectible'
      }
    }).then(modal => {
      modal.onDidDismiss().then((data) => {

      })
      modal.present();
    })
  }

  walletConnectInit() {

    this._ActionSheetController.create({
      mode: 'md',
      header: this._Translate.instant('SIGNSTEPSIX.selectGenderPlaceholder'),
      buttons: [
        {
          text: this._Translate.instant('CHAT.scan-qr-code'), icon: 'qr-code', handler: () => {
          if(this.address) {
            this._NftService.scanWalletConnect(null, this.address).then(() => {
              console.log("this._WalletConnectProvider.activeSessions", this._WalletConnectProvider.activeSessions);
              this.activeSessions = this._WalletConnectProvider.activeSessions.filter(ss => ss.accounts.includes(this.address));
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(this.address) > -1) > -1);
            })
          } else {
            this._NftService.scanWalletConnect(null).then(() => {
              console.log("this._WalletConnectProvider.activeSessions", this._WalletConnectProvider.activeSessions);
              this.activeSessions = this._WalletConnectProvider.activeSessions;
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2;
            })
          }

        } },
        { text: this._Translate.instant('SIGNATURE.enter-wc-string'), icon: 'at', handler: () => { this.initiateWCviaString() } },
        { text: this._Translate.instant('GENERAL.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('Cancel') } }
      ]
    }).then(sheet => {
      sheet.present();
    });

  }

  initiateWCviaString() {
    this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant('SIGNATURE.enter-wc-string'),
      inputs: [{ type: 'text', placeholder: 'wc:...', name: 'wc' }],
      buttons: [{ text: this._Translate.instant('BUTTON.CANCEL') , role: 'cancel', handler: () => {}}, {
        text: this._Translate.instant('WALLET.connect'),
        handler: (data) => {
          var url = data.wc;
          if(url.substr(0,3) != 'wc:') {
            this.presentToast(this._Translate.instant('SIGNATURE.invalid-string'));
            return;
          }
          if(this.address) {
            this._WalletConnectProvider.init(url, this.address).then(() => {
              console.log("this._WalletConnectProvider.activeSessions", this._WalletConnectProvider.activeSessions);
              this.activeSessions = this._WalletConnectProvider.activeSessions.filter(ss => ss.accounts.includes(this.address));
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2.filter(ss => ss.namespaces['eip155']['accounts'].findIndex(sss => sss.indexOf(this.address) > -1) > -1);
            })
          } else {
            this._WalletConnectProvider.processDeeplink(url, false).then(() => {
              this.activeSessions = this._WalletConnectProvider.activeSessions;
              this.activeSessions2 = this._WalletConnectProvider.activeSessions2;
            })
          }

        }
      }]
    }).then(alerti => alerti.present())
  }

  goBack() {
    if (this.timestampSubscription) {
      if (!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }
    this._NavController.navigateBack('/dashboard/payments');
  }

  displayPictureOptions() {
    this._ImageSelectionService.showChangePicture().then(photo => {
      this.profilePictureSrc = !!photo ? photo : '../../assets/job-user.svg'
    });
  }

  redeemTokens() {
    this._PopoverController.create({
        component: DidPopoverComponent,
        componentProps: {
          data: this._Translate.instant('NFT.redeem-tokens-content'),
          heading: this._Translate.instant('NFT.redeem-tokens-heading'),
        },
        cssClass: 'did-popover',
        showBackdrop: true,
        translucent: true
    }).then((popover) => {
        popover.present();
        setTimeout(() => {
            popover.dismiss();
        }, 10000)
    })
  }

}