import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SafariViewController } from '@awesome-cordova-plugins/safari-view-controller/ngx';
import { MerchantDetailsPage } from './merchant-details.page';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
// import { TooltipsModule } from 'ionic-tooltips';
import { DidPopoverModule } from './did-popover/did-popover.module';
import { QRCodeModule } from 'angularx-qrcode';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';

const routes: Routes = [
  {
    path: '',
    component: MerchantDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    // TooltipsModule,
    DidPopoverModule,
    QRCodeModule,
    // MarketplaceCertificateModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [MerchantDetailsPage],
  providers: [SafariViewController, EmailComposer]
})
export class MerchantDetailsPageModule {}
