import { MerchantDetails } from '../core/providers/marketplace/marketplace-model';

interface MerchantPage3HistoryItem {
    from: Date;
    to: Date;
    object: string;
    place: string;
}

export const videoLabellingExample: MerchantDetails = {
    productBackgroundImage: '',
    merchantLogo: '',
    id: -1,
    displayToggle: true,
    dateModified: '1234567',
    productCategories: [],
    productName: 'Whatever',
    productPage1CtaColorcode: '12313',
    productPage2CtaColorcode: '123131',
    merchantName: 'Video Labelling with Jobgrader',
    merchantClaim: 'Earn by labelling objects within a given video',
    merchantCity: '',
    merchantCountry: 'Worldwide',
    productPage1Headline: 'ShareMe stellt sich vor',
    productPage1BodyText: `<p>Are you looking for a video labelling job? Then you’ve come to the right place.</p><p>Video labelling is the process of adding meaningful information to raw video, such as identifying objects, actions, people or events. Video labelling can be used for a variety of purposes, including</p>
    <ul>
      <li>Education and training</li>
      <li>Computer Vision and Artificial Intelligence</li>
      <li>Video analysis and understanding</li>
      <li>Video search and retrieval</li>
    </ul>
    <p>You will earn THXC tokens.</p>`,
    productPage1CtaText: 'Apply for a job',
    // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
    productPage1CtaTargetURL: 'https://jobgrader.app/partner',
    productPage2Headline: 'ShareMe und helix id Vorteile',
    productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
          <p>Hier findest du alles um deinen Account bei
            ShareMe zu aktivieren.</p>
          <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
          <ul>
            <li>Führerschein verifizieren</li>
            <li>Altersbestätigung verifizieren</li>
            <li>Unsere AGBs bestätigen</li>
          </ul>
          <p><u>ShareMe in deiner Umgebung</u></p>
          <ul>
            <li>Alle deutschen Großstädte</li>
            <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
          </ul>
          <p><u>ShareMe Preisgestaltung</u></p>
          <ul>
            <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
            <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
            <li>Kostenlose Registrierung für helix id Nutzer</li>
            <li>Keine monatlichen Kosten</li>
          </ul>`,
    productPage2CtaText: 'Ich will dabei sein',
    // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
    productPage2CtaTargetURL: 'https://jobgrader.app/partner',
    productPage3Headline: 'Buchungen',
    productHistory: [

    ]
};

export const abTestingExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -7,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'A/B-Testing with Jobgrader',
  merchantClaim: 'Earn by comparing',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you searching for micro jobs in A/B-Testing? Then this is the exact spot for you!</p>
  <p>A/B-Testing compares for example web page or app versions for improved user engagement, conversions, and more. That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Optimising User Experience</li>
    <li>Enhancing Conversion Rates</li>
    <li>Data-Driven Decision Making</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const audioLabellingExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -6,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Audio Labelling with Jobgrader',
  merchantClaim: 'Earn by labelling audio clips',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for audio labelling jobs? Then you are at the best place!</p>
  <p>Audio Labelling involves tasks like tagging and categorising audio clips.That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Training AI and Machine Learning Models</li>
    <li>Improving Voice Recognition</li>
    <li>Enhancing Music Recommendations</li>
    <li>Organising Audio Libraries</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const fortuneExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -3,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Prediction with Jobgrader',
  merchantClaim: 'Forecast and earn',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for prediction jobs? Then you're precisely where you need to be!</p>
  <p>Prediction tasks involve sharing foresight, intuition, or analysis on various topics.That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Informed Decision-Making</li>
    <li>Risk Management</li>
    <li>Innovation and Planning</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const formsExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -4,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Market Research with Jobgrader',
  merchantClaim: 'Earn by surveying',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for market research jobs? Then you've found the right one!</p>
  <p>Market research tasks involve creating, distributing, and collecting data through surveys.  That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Consumer Insights</li>
    <li>Product Development</li>
    <li>Marketing Effectiveness</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const statisticsExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -5,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Statistical Evaluation with Jobgrader',
  merchantClaim: 'Earn by analysing',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for statistical evaluation jobs? Congrats, you're in the right place!</p>
  <p>Statistical evaluation tasks involve analysing data, creating meaningful charts, and informing decision-making. That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Data-Driven Decision-Making</li>
    <li>Quality Control</li>
    <li>Performance Optimization</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const captchaExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -8,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Captcha with Jobgrader',
  merchantClaim: 'Earn by solving Captchas',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for captcha jobs? Then you've discovered the ideal spot!</p>
  <p>Captcha tasks categorise pictures to specific definitions. That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Training AI and Machine Learning Models</li>
    <li>User Data Protection</li>
    <li>Enhancing User Experience</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: 'ShareMe und helix id Vorteile',
  productPage2BodyText: ` <p>Mobilität neu erleben - Freiheit erfahren.</p>
        <p>Hier findest du alles um deinen Account bei
          ShareMe zu aktivieren.</p>
        <p><u>In 3 einfachen Schritten zum Erfolg.</u></p>
        <ul>
          <li>Führerschein verifizieren</li>
          <li>Altersbestätigung verifizieren</li>
          <li>Unsere AGBs bestätigen</li>
        </ul>
        <p><u>ShareMe in deiner Umgebung</u></p>
        <ul>
          <li>Alle deutschen Großstädte</li>
          <li>Freie Fahrten mit dem ÖPNV zu deinem Wunsch-Fahrzeug</li>
        </ul>
        <p><u>ShareMe Preisgestaltung</u></p>
        <ul>
          <li>Exklusiver Rabatt für helix id Nutzer 20%</li>
          <li>Transparente Preisgestaltung ab 9 ct./Minute</li>
          <li>Kostenlose Registrierung für helix id Nutzer</li>
          <li>Keine monatlichen Kosten</li>
        </ul>`,
  productPage2CtaText: 'Ich will dabei sein',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: 'https://jobgrader.app/partner',
  productPage3Headline: 'Buchungen',
  productHistory: [

  ]
};

export const priceGuessingExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -2,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: 'Price Guessing with Jobgrader',
  merchantClaim: 'Earn by estimating',
  merchantCity: '',
  merchantCountry: 'Worldwide',
  productPage1Headline: 'ShareMe stellt sich vor',
  productPage1BodyText: `<p>Are you looking for price guessing jobs? Then you've chosen the best place!</p>
  <p>Price guessing tasks evaluate items, services, or experiences. That's why the results you deliver in order processing are so important:</p>
  <ul>
    <li>Pricing Strategy</li>
    <li>Market Competitiveness</li>
    <li>Demand Forecasting</li>
  </ul>
  <p>You will earn THXC tokens.</p>`,
  productPage1CtaText: 'Apply for a job',
  // productPage1CtaTargetURL: 'https://chief-digital-officers.com/de/mobility-blockchain-platform/',
  productPage1CtaTargetURL: 'https://jobgrader.app/partner',
  productPage2Headline: '',
  productPage2BodyText: ``,
  productPage2CtaText: '',
  // productPage2CtaTargetURL: 'https://blockchain-helix.com/de/',
  productPage2CtaTargetURL: '',
  productPage3Headline: '',
  productHistory: [

  ]
};

export const quizExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -2,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: '',
  merchantClaim: '',
  merchantCity: '',
  merchantCountry: '',
  productPage1Headline: '',
  productPage1BodyText: '',
  productPage1CtaText: '',
  productPage1CtaTargetURL: '',
  productPage2Headline: '',
  productPage2BodyText: ``,
  productPage2CtaText: '',
  productPage2CtaTargetURL: '',
  productPage3Headline: ''
};

export const aiTrainingExample: MerchantDetails = {
  productBackgroundImage: '',
  merchantLogo: '',
  id: -2,
  displayToggle: true,
  dateModified: '1234567',
  productCategories: [],
  productName: 'Whatever',
  productPage1CtaColorcode: '12313',
  productPage2CtaColorcode: '123131',
  merchantName: '',
  merchantClaim: '',
  merchantCity: '',
  merchantCountry: '',
  productPage1Headline: '',
  productPage1BodyText: '',
  productPage1CtaText: '',
  productPage1CtaTargetURL: '',
  productPage2Headline: '',
  productPage2BodyText: ``,
  productPage2CtaText: '',
  productPage2CtaTargetURL: '',
  productPage3Headline: ''
};