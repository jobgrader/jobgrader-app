import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { EventsList, EventsService } from '../core/providers/events/events.service';

@Component({
  selector: 'app-veriff-thanks',
  templateUrl: './veriff-thanks.page.html',
  styleUrls: ['./veriff-thanks.page.scss'],
})
export class VeriffThanksPage implements OnInit {

  veriffSessionId: string;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private _ModalController: ModalController,
    private _EventService: EventsService,
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  private async componentInit() {
  }

  public navigateToHome() {
    this._EventService.publish(EventsList.dataReceivedByPushNotification);
    this.nav.navigateBack('/dashboard/tab-home').then(() => {
      this._ModalController.getTop().then(modal => {
        if (modal) {
          modal.dismiss();
        }
      });
    });
  }

  public navigateToProfile() {
    this._EventService.publish(EventsList.dataReceivedByPushNotification);
    this.nav.navigateBack('/dashboard/personal-details').then(() => {
      this._ModalController.getTop().then(modal => {
        if (modal) {
          modal.dismiss();
        }
      });
    });
  }

  public navigateToKYC() {
    this._EventService.publish(EventsList.dataReceivedByPushNotification);
    this.nav.navigateBack('/kyc').then(() => {
      this._ModalController.getTop().then(modal => {
        if (modal) {
          modal.dismiss();
        }
      });
    });
  }

}
