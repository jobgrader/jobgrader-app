import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FortunePage } from './fortune.page';
import { SwiperJobPage } from './swiper/swiper-job.page';
import { RatingJobPage } from './rating/rating-job.page';
import { ABJobPage } from './ab/ab-job.page';
import { QuizJobPage } from './quiz/quiz-job.page';
import { AiTrainingJobPage } from './ai-training/ai-training-job.page';
import { OCRJobPage } from './ocr/ocr-job.page';
import { AudioLabellingPage } from './audio-labelling/audio-labelling.page';


const routes: Routes = [
  {
    path: '',
    component: FortunePage
  },
  { path: 'swiper/:id', component: SwiperJobPage },
  { path: 'rating/:id', component: RatingJobPage },
  { path: 'ab/:id', component: ABJobPage },
  { path: 'quiz/:id', component: QuizJobPage },
  { path: 'ai-training/:id', component: AiTrainingJobPage },
  { path: 'ocr/:id', component: OCRJobPage },
  { path: 'audio-labelling/:id', component: AudioLabellingPage },
  {
    path: 'human/:id',
    loadChildren: () =>
    import('./human/human.module').then(
      (m) => m.HumanPageModule
    )
  },
  {
    path: 'video-recording/:id',
    loadChildren: () => import('./../video-recording/video-recording.module').then( m => m.VideoRecordingPageModule)
  },
  {
    path: 'hcaptcha/:id',
    loadChildren: () => import('./hcaptcha/hcaptcha-job.module').then( m => m.HCaptchaJobModule)
  },
  {
    path: 'details/:id',
    loadChildren: () => import('./details/job-details.module').then( m => m.JobDetailsModule)
  },
  {
    path: 'howto/:id',
    loadChildren: () => import('./howto/job-howto.module').then( m => m.JobHowToModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FortunePageRoutingModule {}
