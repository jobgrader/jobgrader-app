import { HttpEvent, HttpEventType } from '@angular/common/http';
import { AfterViewInit, Component, effect, inject } from '@angular/core';
import { VoiceRecorder, RecordingData, GenericResponse } from '@independo/capacitor-voice-recorder';
import WaveSurfer from 'wavesurfer.js';
import { BaseJobComponent } from '../base-job/base-job.component';
import { LoadingController, ToastController } from '@ionic/angular';
import { Subscription, timer } from 'rxjs';
import { TranslateProviderService } from '@services/translate/translate-provider.service';
import { format, addSeconds } from 'date-fns';
// import * as ffmpeg from 'fluent-ffmpeg';

@Component({
  selector: 'app-audio-labelling',
  templateUrl: './audio-labelling.page.html',
  styleUrls: ['./audio-labelling.page.scss'],
})
export class AudioLabellingPage extends BaseJobComponent implements AfterViewInit {

  private _Translate = inject(TranslateProviderService);
  private _ToastController = inject(ToastController);
  private _LoadingController = inject(LoadingController);

  private solutions: { url: string }[] = [];
  private mediaUpload: { base64: string, mimeType: string, assignmentId: string };
  public timer = 0;
  private timerSubscription: Subscription;

  type = 'audio_labelling';
  isRecording = false;
  isPlaying = false;
  playbackData: HTMLAudioElement;
  text: string;
  progress = 0;
  isMinimumLength = false;
  loading: HTMLIonLoadingElement;

  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('AudioLabellingPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.text = null
      } else {
        this.text = jobs[0]?.text;
      }
    });
  }


  async ngAfterViewInit() {

    const canDeviceVoiceRecord: GenericResponse = await VoiceRecorder.canDeviceVoiceRecord();
    console.log("AudioLabellingPage#canDeviceVoiceRecord: ", canDeviceVoiceRecord.value);

    const hasAudioRecordingPermission: GenericResponse = await VoiceRecorder.hasAudioRecordingPermission();
    console.log("AudioLabellingPage#hasAudioRecordingPermission: ", hasAudioRecordingPermission.value);

    if (!hasAudioRecordingPermission.value) {
      const requestAudioRecordingPermission: GenericResponse = await VoiceRecorder.requestAudioRecordingPermission();
      console.log("AudioLabellingPage#requestAudioRecordingPermission: ", requestAudioRecordingPermission.value);
    }

    this.loading = await this._LoadingController.create({
      message: this._Translate.instant('PLEASE_WAIT')
    });
  }

  ionViewWillLeave() {
    this.stopRecording(true);
  }

  formatTime(seconds) {
    const date = addSeconds(new Date(0), seconds);
    return format(date, 'mm:ss');
  }

  async startRecording() {

    this.playbackData = null;
    this.isRecording = true;
    this.mediaUpload = undefined;

    const startRecording: GenericResponse = await VoiceRecorder.startRecording();
    // console.log("AudioLabellingPage#startRecording; startRecording.value:", startRecording.value);

    // this.job.manifest.annotations.audio.min = 5;

    //  we us the progress bar just when max value is present
    if (this.job.manifest.annotations?.audio?.max) {

      // if there is no min value, we set the minimum length to true, so the progress bar
      // has always the green valid color
      if (!this.job.manifest.annotations?.audio?.min) {
        this.isMinimumLength = true;
      }

      // console.log('AudioLabellingPage#startRecording; before timer', new Date());

      this.timer = 0;
      this.timerSubscription = timer(1000, 1000)
        .subscribe(() => {
          this.timer += 1;
          // console.log('AudioLabellingPage#startRecording; time', new Date());

          this.progress = this.timer / Number(this.job.manifest.annotations.audio.max);

          if (this.timer === Number(this.job.manifest.annotations.audio.min)) {
            this.isMinimumLength = true;
          }

          if (this.timer === Number(this.job.manifest.annotations.audio.max)) {
            this.stopRecording();
          }
        });
    }

  }

  async stopRecording(exit = false) {

    this.isRecording = false;
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
    this.progress = 0;
    this.isMinimumLength = false;

    let stopRecording: RecordingData;
    const status = await VoiceRecorder.getCurrentStatus();
    if (status.status != 'NONE') {
      stopRecording = await VoiceRecorder.stopRecording();
      console.log("AudioLabellingPage#stopRecording: ", stopRecording.value);
    }

    if (exit) {
      return;
    }

    if (this.job.manifest.annotations?.audio?.min && this.timer < this.job.manifest.annotations.audio.min) {
      this.displayVideoTooShort();
      return;
    }

    const base64Sound = stopRecording.value.recordDataBase64;
    const mimeType = stopRecording.value.mimeType;

    this.playbackData = new Audio(`data:${mimeType};base64,${base64Sound}`)
    this.playbackData.onplay = () => { this.isPlaying = true };
    this.playbackData.onpause = () => { this.isPlaying = false };

    this.generateWaveform();

    this.mediaUpload =
    {
      base64: base64Sound,
      mimeType: mimeType,
      assignmentId: this.assignmentId,
    }
  }

  playRecording() {
    // console.log("AudioLabellingPage#playRecording; this.playbackData.paused:", this.playbackData.paused);

    this.playbackData.play();

  }

  pausePlaying() {
    this.playbackData.pause();
  }

  submit() {
    this.humanService.createPresignedUrl(this.mediaUpload.assignmentId, null, this.mediaUpload.mimeType)
      .subscribe({
        next: async (res: any) => {
          // console.log('AudioLabellingPage#submit; createPresignedUrl res:', res);

          if (res.url) {
            const url = res.url.split('?')[0];
            // console.log('AudioLabellingPage#submit; url:', url);

            await this.loading.present();

            this.humanService.uploadMedia(res.url, this.mediaUpload.base64)
              .subscribe({
                next: async (event: HttpEvent<any>) => {
                  console.log('AudioLabellingPage#submit; uploadMedia event:', event);

                  switch (event.type) {
                    case HttpEventType.Sent:
                      console.log('AudioLabellingPage#submit; uploadMedia Sent:', this.mediaUpload.assignmentId, this.mediaUpload.mimeType);
                      break;
                    case HttpEventType.UploadProgress:
                      // compute and show the % done:
                      const percentDone = event.total ? Math.round(100 * event.loaded / event.total) : 0;

                      this.loading.message = this._Translate.instant('AUDIO_UPLOAD_PROGRESS', { percentDone: percentDone.toString() });

                      console.log(`AudioLabellingPage#submit; uploadMedia UploadProgress: is ${percentDone}% uploaded.`);
                      break;
                    case HttpEventType.Response:
                      console.log(`AudioLabellingPage#submit; uploadMedia Response: upload done!`, event);

                      await this.loading.dismiss();

                      this.solutions.push({ url: url });
                      console.log('AudioLabellingPage#submit; this.solutions:', this.solutions);

                      this.solve(JSON.stringify(this.solutions));

                      break;
                    default:
                      console.log(`AudioLabellingPage#submit; uploadMedia Response: surprising upload event: ${event.type}.`);
                  }
                },
                error: (err: any) => {
                  console.log('AudioLabellingPage#submit; uploadMedia error:', err);

                  this.displayAudioUploadError();
                }
              });
          }

        },
        error: (err: any) => {
          console.log('AudioLabellingPage#submit; uploadMedia error:', err);

          this.displayAudioUploadError();
        }
      });
  }

  // async pauseRecording() {

  //   this.isPlaying = false;

  //   const pauseRecording: GenericResponse = await VoiceRecorder.pauseRecording();
  //   console.log("AudioLabellingPage#pauseRecording: ", pauseRecording.value);

  // }

  // async resumeRecording() {

  //   const resumeRecording: GenericResponse = await VoiceRecorder.resumeRecording();
  //   console.log("AudioLabellingPage#resumeRecording: ", resumeRecording.value);

  // }

  private generateWaveform() {
    const waveform = document.getElementById("waveform");

    if (waveform) {
      waveform.innerHTML = ``;

      WaveSurfer.create({
        container: '#waveform',
        waveColor: '#54BF7B',
        progressColor: '#FF9C00',
        media: this.playbackData,
      });
    }
  }

  private async displayVideoTooShort() {
    const toast = await this._ToastController.create({
      message: this._Translate.instant('FORTUNE.AUDIO_LABELLING.AUDIO_TOO_SHORT'),
      duration: 2000,
      position: 'top'
    });

    await toast.present();
  }

  private async displayAudioUploadError() {
    const toast = await this._ToastController.create({
      message: this._Translate.instant('FORTUNE.AUDIO_LABELLING.AUDIO_UPLOAD_ERROR'),
      duration: 2000,
      position: 'top'
    });

    await toast.present();
  }

  // async convertAudioBase64(base64Data) {
  //   const buffer = Buffer.from(base64Data, 'base64');

  //   // Pipe the decoded audio data directly to ffmpeg for conversion
  //   const ffmpegProcess = ffmpeg()
  //     .input(buffer) // Use buffer as input instead of a file
  //     .audioCodec('libmp3lame') // Set the audio codec to libmp3lame (MP3)
  //     .format('pipe'); // Output format set to pipe for streaming

  //   let base64Output = '';

  //   ffmpegProcess.on('data', (chunk) => {
  //     // Append the received data chunks to the base64 output string
  //     base64Output += chunk.toString('base64');
  //   });

  //   ffmpegProcess.on('end', () => {
  //     console.log('Conversion completed! Base64 MP3 data:', base64Output);
  //     return base64Output;
  //   });

  //   ffmpegProcess.on('error', (err) => console.error('Error:', err));

  //   ffmpegProcess.run();
  // }

}
