import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';


// custom imports
import { NavigateToJobService } from '@services/navigate-to-job';
import { JobDetailsRoutingModule } from './job-details-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { JobDetailsPage } from './job-details.page';


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    JobDetailsRoutingModule,
  ],
  providers: [
    NavigateToJobService
  ],
  declarations: [
    JobDetailsPage,
  ]
})
export class JobDetailsModule {}
