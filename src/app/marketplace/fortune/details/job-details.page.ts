import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';


// custom imports
import { NavigateToJobService } from '@services/navigate-to-job';
import { HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { JobDetail } from 'src/app/core/providers/human/JobDetail';


@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.page.html',
  styleUrls: ['./job-details.page.scss'],
})
export class JobDetailsPage implements OnInit {

  private workerAddress: string;
  private escrowAddress: string;

  private language: string;

  job: JobDetail;
  from: string;
  private demo: boolean;
  private dataBase64: string;


  constructor(
    private route: ActivatedRoute,
    private navController: NavController,
    private secureStorageService: SecureStorageService,
    private humanService: HumanService,
    private navigateToJobService: NavigateToJobService,
  ) { }

  async ngOnInit() {
    this.workerAddress = await this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    this.escrowAddress = this.route.snapshot.paramMap.get('id');
    this.from = this.route.snapshot.queryParams['from'];
    this.demo = this.route.snapshot.queryParams['demo'];
    this.dataBase64 = this.route.snapshot.queryParams['data'];

    if (this.demo && this.dataBase64) {
      const job = JSON.parse(Buffer.from(this.dataBase64, 'base64').toString());
      if (!job.manifest.annotations) {
        job.manifest.annotations = {
          payout: 1,
          payoutUnit: 'THXC',
          workTime: 30,
          workTimeUnit: 'minutes',
          workTimeLimit: 1,
          workTimeLimitUnit: 'hour',
          payoutDuration: 2,
          payoutDurationUnit: 'days',
          regions: ['EU'],
          age: 18,
          gender: 'm',
          targetGroup: 0,
          personalData: 0,
          publication: 1,
          scope: 0,
          information: 'This is the information blabla<br>blabla and<br>more blabla'
        }
      }

      this.job = job;
      return;
    }

    this.humanService.getJobDetails(80002, this.escrowAddress)
          .subscribe({
            next: (job: JobDetail) => {
              // console.log('JobDetailsPage#ngOnInit job:', job);

              if (!job.manifest.annotations) {

                job.manifest.annotations = {
                  payout: 1,
                  payoutUnit: 'THXC',
                  workTime: 30,
                  workTimeUnit: 'minutes',
                  workTimeLimit: 1,
                  workTimeLimitUnit: 'hour',
                  payoutDuration: 2,
                  payoutDurationUnit: 'days',
                  regions: ['EU'],
                  age: 18,
                  gender: 'm',
                  targetGroup: 0,
                  personalData: 0,
                  publication: 1,
                  scope: 0,
                  information: 'This is the information blabla<br>blabla and<br>more blabla'
                }
              }

              this.job = job;
            },
            error: (err: any) => {
              this.job = null;
            }
          });
  }

  goBack() {
    const queryParams: Params = {};
    queryParams.type = this.from;

    if (this.demo && this.dataBase64) {
      queryParams.demo = this.demo;
      queryParams.data = this.dataBase64;
    }

    this.navController.navigateBack(`/jobs`, { queryParams });

  }

  goToHowTo() {
    const queryParams: Params = {};
    queryParams.from = this.from;

    if (this.demo && this.dataBase64) {
      queryParams.demo = this.demo;
      queryParams.data = this.dataBase64;
    }

    this.navController.navigateForward(`/jobs/howto/${this.escrowAddress}`, { queryParams });
  }

  async goToJob() {
    this.navigateToJobService.goToJob(this.job, this.demo, this.dataBase64);
  }
}
