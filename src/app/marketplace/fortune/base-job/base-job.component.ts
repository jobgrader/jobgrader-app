import { Component, OnInit, inject, signal } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { AlertController, NavController, ToastController } from "@ionic/angular";
import * as Sentry from '@sentry/angular-ivy';
import { FirestoreCloudFunctionsService } from "@services/firestore-cloud-functions/firestore-cloud-functions.service";
import { TranslateProviderService } from "@services/translate/translate-provider.service";
import { Subject, finalize, takeUntil, timer } from "rxjs";


// custom imports
import { JobDetail } from "src/app/core/providers/human/JobDetail";
import { HumanService } from "src/app/core/providers/human/human.service";
import { SecureStorageKey } from "src/app/core/providers/secure-storage/secure-storage-key.enum";
import { SecureStorageService } from "src/app/core/providers/secure-storage/secure-storage.service";
import { UserPhotoServiceAkita } from "src/app/core/providers/state/user-photo/user-photo.service";


export @Component({
  template: ''
})
abstract class BaseJobComponent implements OnInit {

  private destroy$ = new Subject();


  private route = inject(ActivatedRoute);
  private userPhotoServiceAkita = inject(UserPhotoServiceAkita);
  private secureStorageService = inject(SecureStorageService);
  protected humanService = inject(HumanService);
  private navController = inject(NavController);
  private firebase = inject(FirestoreCloudFunctionsService)
  private alertService = inject(AlertController);
  private translateService = inject(TranslateProviderService);
  private toastController = inject(ToastController);


  protected job: JobDetail;
  protected workerAddress: string;
  protected escrowAddress: string;
  protected assignmentId: string;
  protected language: string;
  protected type: string;
  protected deviceId: string;

  protected demo: boolean;
  protected dataBase64: string;
  protected goBackAfterDone = true;

  userImage;
  jobs = signal<any>([]);

  trusted = null;
  solutionsCorrect = null;

  async ngOnInit() {
    this.userImage = this.userPhotoServiceAkita.getPhoto();
    this.workerAddress = await this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    this.escrowAddress = this.route.snapshot.paramMap.get('id');
    this.assignmentId = this.route.snapshot.queryParams['assignmentId'];
    this.language = await this.secureStorageService.getValue(SecureStorageKey.language ,false) || 'en';
    this.deviceId = await this.secureStorageService.getValue(SecureStorageKey.deviceInfo, false);
    this.demo = this.route.snapshot.queryParams['demo'];
    this.dataBase64 = this.route.snapshot.queryParams['data'];

    // console.log('BaseJobComponent#ngOnInit this.demo:', this.demo);
    // console.log('BaseJobComponent#ngOnInit this.dataBase64:', this.dataBase64);

    if (this.demo && this.dataBase64) {
      const job = JSON.parse(Buffer.from(this.dataBase64, 'base64').toString());
      this.jobs = job.manifest.data;
      return;
    }

    this.humanService.getJobDetails(80002, this.escrowAddress)
          .subscribe({

            next: (job: JobDetail) => {
              // console.log('BaseJobComponent#ngOnInit job:', job);

              this.job = job;

              this.humanService.downloadJson(job.manifest.dataUrl)
                    .subscribe({
                      next: (tasks: any[] | { type: string, data: [] }) => {
                        // console.log('BaseJobComponent#ngOnInit tasks:', tasks);

                        this.jobs.set(tasks);
                      },
                      error: (err: any) => {
                        this.jobs = null;
                      }
                    });
            },
            error: (err: any) => {
              this.jobs = null;
            }
          });
  }

  protected goBack() {
    this.destroy$.next(0);

    const queryParams: Params = {};
    queryParams.type = this.type;

    if (this.demo && this.dataBase64) {
      queryParams.demo = this.demo;
      queryParams.data = this.dataBase64;
    }

    this.navController.navigateBack(`/jobs`, { queryParams });
  }


  protected solve(solution: string) {
    this.humanService.solve(this.assignmentId, solution)
          .pipe(
            finalize(async () => {
              // console.log('BaseJobComponent#solve; finalize');

              await this.manageSolutionAndVc(solution);

              this.displayJobDoneMessage();

              timer(500)
                .pipe(
                  takeUntil(this.destroy$),
                )
                .subscribe({
                  next: () => {
                    if (this.goBackAfterDone) {
                      this.goBack();
                    }
                  }
                });

            })
          )
          .subscribe({
            next: (value: any) => {
              console.log('BaseJobComponent#solve; success:', value);
            },
            error: (err: any) => {
              console.log('BaseJobComponent#solve; error:', err);
            }
          })
  }

private manageSolutionAndVc(solution: string): Promise<void> {
  return new Promise((resolve) => {
    if (!this.demo && this.job.manifest.annotations?.certificates?.length > 0) {
      this.firebase.checkJobSolution(this.escrowAddress, solution, this.workerAddress, this.job.manifest.annotations.certificates, this.deviceId).then(res => {
        if(res.success) {
          this.trusted = res.trusted;
          this.solutionsCorrect = res.solutionsCorrect;

          resolve();

        } else {
          this.trusted = false; // false
          this.solutionsCorrect = false; // false

          resolve();
        }
      }).catch(err => {
        this.trusted = null;
        this.solutionsCorrect = null;

        console.error('BaseJobComponent#manageSolutionAndVc; err:', err);

                // we send the error the sentry and resolve the promise anyway
        Sentry.captureException(err);
        resolve();
      })
    } else {
      resolve();
    }
  })
}

  presentAlert(message: string) {
    this.alertService.create({
      message,
      buttons: [{
        text: "Ok",
        handler: () => {

        }
      }]
    }).then(a => {
      a.present();
    })
  }

  private async displayJobDoneMessage() {
    // console.log('BaseJobComponent#displayJobDoneMessage');

    const toast = await this.toastController.create({
      message: this.translateService.instant('FORTUNE.MESSAGES.JOB_DONE'),
      duration: 2000,
      position: 'top'
    });

    await toast.present();
  }
}