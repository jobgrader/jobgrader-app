import { Component, ElementRef, effect, QueryList, Renderer2, ViewChildren, inject, signal } from '@angular/core';
import { AnimationController } from '@ionic/angular';
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-fortune-ab-job',
  templateUrl: './ab-job.page.html',
  styleUrls: ['./ab-job.page.scss'],
})
export class ABJobPage extends BaseJobComponent {

  private animationCtrl = inject(AnimationController);
  private renderer = inject(Renderer2);

  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;

  private solutions: { a: string, b: string, s: 'a' | 'b' }[] = [];

  type = 'ab';
  allDone = false;
  descriptionA: string;
  descriptionB: string;

  jobPairs: { a: number, b: number }[];


  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('ABJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.jobPairs = [];
        this.descriptionA = null
        this.descriptionB = null
      } else {
        this.jobPairs = this.createPairs(jobs.filter((job) => job.id <= 4).flatMap((job) => job.id ));
        console.log('ABJobPage#ngOnInit this.jobPairs:', this.jobPairs);

        this.descriptionA = this.jobs()[this.jobPairs[0].a].description;
        this.descriptionB = this.jobs()[this.jobPairs[0].b].description;
      }
    });
  }



  select(index: number, s: 'a' | 'b') {
    // console.log('ABJobPage#select; index', index);
    // console.log('ABJobPage#select; s', s);

    // get the clicked card, which is .card-a or card-b of the shown cards on top
    let card = this.cards.toArray()[index].nativeElement?.querySelector('.card-' + s);
    // console.log('ABJobPage#select; card', card);

    const animation = this.animationCtrl
      .create()
      .addElement(card)
      .duration(200)
      .direction('alternate')
      .fromTo('borderColor', (s === 'a' ? 'var(--ion-color-secondary)' : 'var(--ion-color-primary)'), '#ffffff');

    animation.onFinish(() => {

      this.renderer.addClass(this.cards.toArray()[index].nativeElement, 'hidden');

      const a = this.jobs()[this.jobPairs[index].a].id;
      const b = this.jobs()[this.jobPairs[index].b].id;
      this.solutions.push({ a, b, s });

      // console.log('ABJobPage#select; this.solutions', this.solutions);

      if (index === this.jobPairs.length-1) {
        this.allDone = true;
        this.solve(JSON.stringify(this.solutions));
      } else {
        this.descriptionA = this.jobs()[this.jobPairs[index+1].a].description;
        this.descriptionB = this.jobs()[this.jobPairs[index+1].b].description;
      }
    });

    animation.play();
  }

  private createPairs(ids: any[]): {a: number, b: number}[]  {
    let pairs: { a: number, b: number }[] = [];
    for (let i = 0; i < ids.length; i++) {
      for (let k = i+1; k < ids.length; k++) {
        pairs.push({ a: i, b: k})
      }
    }

    return pairs;
  }
}
