import { Component, ElementRef, QueryList, Renderer2, ViewChildren, effect, inject } from '@angular/core';


// custom imports
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-fortune-rating-job',
  templateUrl: './rating-job.page.html',
  styleUrls: ['./rating-job.page.scss'],
})
export class RatingJobPage extends BaseJobComponent {

  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;

  private renderer = inject(Renderer2);

  private solutions: number[] = [];

  type = 'rating';
  allDone = false;
  description: string;


  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('RatingJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.description = null
      } else {
        this.description = jobs[0]?.description;
      }
    });
  }


  rate(value: number, index: number) {
    // console.log('RateJobPage#rate; value', value);
    // console.log('RateJobPage#rate; index', index);

    this.jobs()[index].rate = value;

    setTimeout(() => {
      this.renderer.addClass(this.cards.toArray()[index].nativeElement, 'hidden');
      this.solutions.push(value);

      if (index === this.jobs().length-1) {
        this.allDone = true;
        this.solve(JSON.stringify(this.solutions));
      } else {
        this.description = this.jobs()[index+1]?.description;
      }
    }, 200);
  }
}
