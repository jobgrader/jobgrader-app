import { Component, ElementRef, OnInit, QueryList, Renderer2, ViewChild, ViewChildren, effect, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonContent, IonInput, NavController } from '@ionic/angular';
import { Subject, finalize, takeUntil, timer } from 'rxjs';


// custom imports
import { HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-ocr-job-job',
  templateUrl: './ocr-job.page.html',
  styleUrls: ['./ocr-job.page.scss'],
})
export class OCRJobPage extends BaseJobComponent implements OnInit {

  @ViewChild(IonContent) content: IonContent;
  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;

  private renderer = inject(Renderer2);

  private solutions: { type: string; value: string }[] = [];

  type = 'aiTraining';
  scan = '';
  allDone = false;

  typeChecked = false;
  valueChecked = false;
  typeEditable = false;
  valueEditable = false;

  isModalOpen = false;


  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('OCRJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.solutions = [];
        this.scan = null;
      } else {
        this.solutions = [];

        jobs.forEach((job) => {
          this.solutions.push({ type: job.type, value: job.value});
        })

        this.scan = jobs[0].scan;
      }
    });
  }


  reset(index: number) {
    // console.log('OCRJobPage#reset; index', index);

    this.resetDatatype(index);
    this.resetValue(index);
  }

  private resetDatatype(index: number) {
    // console.log('OCRJobPage#resetDatatype; index', index);

    this.solutions[index].type = this.jobs()[index].type;
    this.typeEditable = false;
    this.typeChecked = false;
  }

  private resetValue(index: number) {
    // console.log('OCRJobPage#resetValue; index', index);

    this.solutions[index].value = this.jobs()[index].value;
    this.valueEditable = false;
    this.valueChecked = false;
  }

  async editDatatype(el: IonInput) {
    // console.log('OCRJobPage#editDatatype; el', el);

    this.typeChecked = false;
    this.typeEditable = true;
    await el.setFocus();
  }

  async editValue(el: IonInput) {
    // console.log('OCRJobPage#editValue; el', el);

    this.valueChecked = false;
    this.valueEditable = true;
    await el.setFocus();
  }

  submitDatatype() {
    this.typeEditable = false;
    this.typeChecked = true;
  }

  submitValue() {
    this.valueEditable = false;
    this.valueChecked = true;
  }

  submit(index: number) {
    // console.log('OCRJobPage#submit; index', index);

    setTimeout(() => {
      this.renderer.addClass(this.cards.toArray()[index].nativeElement, 'hidden');
      this.content.scrollToTop(100);

      if (index === this.jobs().length-1) {
        this.allDone = true;

        // console.log('OCRJobPage#submit; solutions', this.solutions);
        this.solve(JSON.stringify(this.solutions));
      } else {
        this.scan = this.jobs()[index+1]?.scan;
        this.typeChecked = false;
        this.valueChecked = false;
        this.typeEditable = false;
        this.valueEditable = false;
      }
    }, 200);
  }

  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
  }

}
