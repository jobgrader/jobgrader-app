import { Component, ElementRef, QueryList, Renderer2, ViewChildren, effect, inject } from '@angular/core';


// custom imports
// import { DidVcService } from 'src/app/core/providers/did-vc/did-vc.service';
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-fortune-quiz-job',
  templateUrl: './quiz-job.page.html',
  styleUrls: ['./quiz-job.page.scss'],
})
export class QuizJobPage extends BaseJobComponent {

  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;

  private renderer = inject(Renderer2);
  // private didVcService = inject(DidVcService);

  private solutions: number[] = [];

  type = 'quiz';
  allDone = false;
  description: string;


  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('QuizJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.description = null
      } else {
        this.description = jobs[0]?.description;
      }
    });
  }


  select(selectedAnswer: number) {
    // console.log('QuizJobPage#select; selectedAnswer', selectedAnswer);

    setTimeout(() => {
      this.renderer.addClass(this.cards.toArray()[this.solutions.length].nativeElement, 'hidden');
      this.solutions.push(selectedAnswer);

      if (this.solutions.length === this.jobs().length) {
        this.goBackAfterDone = false;
        this.allDone = true;
        this.solve(JSON.stringify(this.solutions));
      }
    }, 200);
  }
}
