import { Component, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { finalize, forkJoin, Subject, takeUntil } from 'rxjs';
import { HumanService, PriceGuessJob } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';
import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';
import { FirestoreCloudFunctionsService } from '@services/firestore-cloud-functions/firestore-cloud-functions.service';


@Component({
  selector: 'app-fortune',
  templateUrl: './fortune.page.html',
  styleUrls: ['./fortune.page.scss'],
})
export class FortunePage implements AfterViewInit {

  address: string;
  reloadJobs = false;

  jobs: PriceGuessJob[] = [];

  inactiveStatus = {};

  private type: string[];
  private from: string;
  demo: boolean;
  dataBase64: string;
  label: string;
  private destroy$ = new Subject<void>();

  constructor(
    private activatedRoute: ActivatedRoute,
    private human: HumanService,
    private navController: NavController,
    private toastController: ToastController,
    private secureStorageService: SecureStorageService,
    private userPhotoServiceAkita: UserPhotoServiceAkita,
    private apiProviderService: ApiProviderService,
    public theme: ThemeSwitcherService,
    private firecloud: FirestoreCloudFunctionsService
  ) {
    this.from = this.activatedRoute.snapshot.queryParams['type'];
    this.demo = this.activatedRoute.snapshot.queryParams['demo'];
    this.dataBase64 = this.activatedRoute.snapshot.queryParams['data'];
    // console.log('FortunePage#constructor; this.from', this.from);

    switch (this.from) {
      case 'price_guess':
        this.type = ['PRICE_GUESS_SWIPE'];
        this.label = 'PRICE_GUESS';
        break;
      case 'ab':
        this.type = ['FORTUNE_AB'];
        this.label = 'AB';
        break;
      case 'rating':
        this.type = ['FORTUNE_RATING'];
        this.label = 'RATING';
        break;
      case 'quiz':
        this.type = ['FORTUNE_QUIZ'];
        this.label = 'QUIZ';
        break;
      case 'aiTraining':
        this.type = ['FORTUNE_PROMPT_TRAINING', 'JOBGRADER_OCR'];
        this.label = 'AI_TRAINING';
        break;
      case 'audio_labelling':
        this.type = ['JOBGRADER_AUDIO'];
        this.label = 'AUDIO_LABELLING';
        break;
      case 'video_labelling':
        this.type = ['JOBGRADER_FACE_AI', 'JOBGRADER_VIDEO'];
        this.label = 'VIDEO_LABELLING';
        break;
      case 'hcaptcha':
      default:
        this.from = 'hcaptcha';
        this.type = ['JOBGRADER_HCAPTCHA'];
        this.label = 'HCAPTCHA';
    }

    // console.log('FortunePage#constructor; this.type', this.type);
    // console.log('FortunePage#constructor; label', this.label);
  }

  async ngAfterViewInit() {
    this.fetchJobs();
  }

  ionViewWillLeave() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  async fetchJobs() {
    this.destroy$.next();
    this.address = await this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);

    this.reloadJobs = true;
    this.jobs = [];


    if (this.demo && this.dataBase64) {
      this.jobs.push(JSON.parse(Buffer.from(this.dataBase64, 'base64').toString()));
      this.reloadJobs = false;
      return;
    }

    if (!this.address) {
      this.reloadJobs = false;
      return;
    }

    const onlyUnique = (value, index, array) => {
      return array.indexOf(value) === index;
    }


    // do the returnAssesmentsNfts promise and the http observable in parallel for performance reasons
    forkJoin(
      this.from == 'quiz' ?
        [
          // this promise and can not be interrupted, so everything what is done in the promise
          // will be executed anyway
          // TODO: promise can not be interrupted, look if we can change it to an observable
          this.firecloud.returnAssessmentEpnNfts(),
          this.human.listJobs(80002, this.address, this.type)
        ] :
        [
          new Promise(resolve => resolve([])),
          this.human.listJobs(80002, this.address, this.type)
        ]
    )
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: ([epnNfts, jobs]: [any[], any]) => {
          // console.log('FortunePage#fetchJobs; epnNfts:', epnNfts);
          // console.info('FortunePage#fetchJobs; jobs:', jobs);

          const epnNftIds = Array.from(epnNfts, n =>
            n.attributes?.find(nn => nn.trait_type == 'id').value.toLowerCase()).filter(onlyUnique);

          if (jobs?.results?.length > 0) {
            jobs.results.forEach((data: { escrowAddress: string }) => {

              this.human.getJobDetails(80002, data.escrowAddress)
                .pipe(
                  takeUntil(this.destroy$),
                  finalize(
                    () => {
                      // after first job is fetched we stop the loading skeleton
                      // if we have for example 100 jobs it will take to much time to show
                      // first job if we need to fetch all of them
                      this.reloadJobs = false;
                    }
                  )
                )
                .subscribe({
                  next: (job: any) => {

                    if (this.from == 'quiz') {
                      Object.assign(this.inactiveStatus, {
                        [job.escrowAddress.toLowerCase()]: job.manifest.annotations.certificates.every(c => epnNftIds.includes(c))
                      });
                    }

                    this.jobs.push(job);
                  },
                  error: (err: any) => {
                  }
                });
            })
          } else {
            this.reloadJobs = false;
          }
        },
        error: (err: any) => {
          this.jobs = [];
          this.reloadJobs = false;
        }
      });

  }

  returnInactiveStatus(job: any) {
    return !!this.inactiveStatus[job.escrowAddress.toLowerCase()];
  }

  presentToast(message: string) {
    this.toastController.create({
      message,
      position: 'top',
      duration: 2000
    }).then((toast) => {
      toast.present();
    })
  }

  goHome() {
    if (!this.demo) {
      this.navController.navigateBack('/dashboard/tab-home');
    }
  }

  openJob(job: PriceGuessJob) {
    // console.log('FortunePage#openJob; job', job);
    // console.log('FortunePage#openJob; this.demo', this.demo);
    // console.log('FortunePage#openJob; this.dataBase64', this.dataBase64);

    const queryParams: Params = {};
    queryParams.from = this.from;

    if (this.demo && this.dataBase64) {
      queryParams.demo = this.demo;
      queryParams.data = this.dataBase64;
    }

    this.navController.navigateForward(`/jobs/details/${job.escrowAddress}`, { queryParams });
  }

  goToWalletGeneration() {
    this.navController.navigateForward(`/wallet-generation`);
  }
}
