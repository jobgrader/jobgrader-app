import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { NavController } from '@ionic/angular';
import * as H from '@vladmandic/human';
import { Subject } from 'rxjs';
import { UserPhotoServiceAkita } from '../../../core/providers/state/user-photo/user-photo.service';
import { BaseJobComponent } from '../base-job/base-job.component';


// custom imports


@Component({
  selector: 'app-human-job',
  templateUrl: './human-job.page.html',
  styleUrls: ['./human-job.page.scss'],
})
export class HumanJobPage extends BaseJobComponent implements AfterViewInit, OnInit {

  @ViewChild('canvas', { read: ElementRef }) canvasElementRef: ElementRef;
  @ViewChild('video', { read: ElementRef }) videoElementRef: ElementRef;
  @ViewChild('log', { read: ElementRef }) logElementRef: ElementRef;
  @ViewChild('fps', { read: ElementRef }) fpsElementRef: ElementRef;
  @ViewChild('perf', { read: ElementRef }) perfElementRef: ElementRef;


  allDone = false;
  type = 'video_labelling';

  // private width = 1920; // used by webcam config as well as human maximum resultion // can be anything, but resolutions higher than 4k will disable internal optimizations

  private humanConfig: Partial<H.Config> = { // user configuration for human, used to fine-tune behavior
    debug: true,
    backend: 'webgl',
    // cacheSensitivity: 0,
    // cacheModels: false,
    // warmup: 'none',
    // modelBasePath: '../../models',
    modelBasePath: 'https://vladmandic.github.io/human-models/models/',
    filter: { enabled: true, equalization: false, flip: false },
    face: { enabled: true, detector: { rotation: false }, mesh: { enabled: true }, attention: { enabled: false }, iris: { enabled: true }, description: { enabled: true }, emotion: { enabled: true }, antispoof: { enabled: true }, liveness: { enabled: true } },
    body: { enabled: false },
    hand: { enabled: false },
    object: { enabled: false },
    segmentation: { enabled: false },
    gesture: { enabled: true },
  };

  private human = new H.Human(this.humanConfig);

  private dom = {
    canvas: null,
    video: null,
    log: null,
    fps: null,
    perf: null,
  };

  private timestamp = { detect: 0, draw: 0, tensors: 0, start: 0 }; // holds information used to calculate performance and possible memory leaks
  private fps = { detectFPS: 0, drawFPS: 0, frames: 0, averageMs: 0 }; // holds calculated fps information for both detect and screen refresh

  private enabled = true;


  async ngAfterViewInit(): Promise<void> {
    console.log('HumanJobPage#ngAfterViewInit; start');

    this.dom.canvas = this.canvasElementRef.nativeElement;
    this.dom.video = this.videoElementRef.nativeElement;
    this.dom.log = this.logElementRef.nativeElement;
    this.dom.fps = this.fpsElementRef.nativeElement;
    this.dom.perf = this.perfElementRef.nativeElement;

    this.log('human version:', this.human.version, '| tfjs version:', this.human.tf.version['tfjs-core']);
    this.log('platform:', this.human.env.platform, '| agent:', this.human.env.agent);
    this.status('loading...');
    await this.human.load(); // preload all models
    this.log('backend:', this.human.tf.getBackend(), '| available:', this.human.env.backends);
    this.log('models stats:', this.human.models.stats());
    this.log('models loaded:', this.human.models.loaded());
    this.log('environment', this.human.env);
    this.status('initializing...');


    console.log('HumanJobPage#ngAfterViewInit; before this.human.warmup()');
    await this.human.warmup(); // warmup function to initialize backend for future faster detection
    console.log('HumanJobPage#ngAfterViewInit; before webCam()');
    await this.webCam(); // start webcam
    console.log('HumanJobPage#ngAfterViewInit; before detectionLoop()');
    await this.detectionLoop(); // start detection loop
    console.log('HumanJobPage#ngAfterViewInit; before drawLoop()');
    await this.drawLoop(); // start draw loop
    console.log('HumanJobPage#ngAfterViewInit; end');
  }

  ionViewWillLeave() {
    //  somehow we need to clean up the human library and the use of the webcam
    this.human.webcam.stop();
    this.human.reset();
    // set enabled to defauls so the requestAnimationFrame will stop
    this.enabled = false;
  }

  private log(...msg) { // helper method to output messages
    this.dom.log.innerText += msg.join(' ') + '\n';
    console.log(...msg); // eslint-disable-line no-console
  };

  private status(msg) {
    this.dom.fps.innerText = msg; // print status element
  }

  private perf(msg) {
    this.dom.perf.innerText = 'tensors:' + this.human.tf.memory().numTensors.toString() + ' | performance: ' + JSON.stringify(msg).replace(/"|{|}/g, '').replace(/,/g, ' | '); // print performance element
  }

  private async webCam() {
    const devices = await this.human.webcam.enumerate();
    const id = devices[0].deviceId; // use first available video source
    // const width = this.width;
    const width = this.human.webcam.width;
    const webcamStatus = await this.human.webcam.start({ element: this.dom.video, crop: false, width, id }); // use human webcam helper methods and associate webcam stream with a dom element
    this.log(webcamStatus);
    this.dom.canvas.width = this.human.webcam.width;
    this.dom.canvas.height = this.human.webcam.height;
    this.dom.canvas.onclick = async () => { // pause when clicked on screen and resume on next click
      if (this.human.webcam.paused) await this.human.webcam.play();
      else this.human.webcam.pause();
    };
    console.log('xxxxxxxxxxxxxxxx');
    const ctx = this.dom.canvas.getContext("2d");
    const match = /(?<value>\d+\.?\d*)/;
    const newSize = parseFloat(ctx.font.match(match).groups.value + 5);
    ctx.font = ctx.font.replace(match, newSize);
    console.log(ctx);
    console.log(this.dom.canvas.getContext("2d").font);
    console.log('xxxxxxxxxxxxxxxx');
  }

  private async detectionLoop() { // main detection loop
    if (this.enabled) {
      if (!this.dom.video.paused) {
        if (this.timestamp.start === 0) this.timestamp.start = this.human.now();
        // log('profiling data:', await this.human.profile(this.dom.video));
        await this.human.detect(this.dom.video); // actual detection; were not capturing output in a local variable as it can also be reached via this.human.result
        const tensors = this.human.tf.memory().numTensors; // check current tensor usage for memory leaks
        if (tensors - this.timestamp.tensors !== 0) this.log('allocated tensors:', tensors - this.timestamp.tensors); // printed on start and each time there is a tensor leak
        this.timestamp.tensors = tensors;
        this.fps.detectFPS = Math.round(1000 * 1000 / (this.human.now() - this.timestamp.detect)) / 1000;
        this.fps.frames++;
        this.fps.averageMs = Math.round(1000 * (this.human.now() - this.timestamp.start) / this.fps.frames) / 1000;
        if (this.fps.frames % 100 === 0 && !this.dom.video.paused) this.log('performance', { ...this.fps, tensors: this.timestamp.tensors });
      }
      this.timestamp.detect = this.human.now();
      requestAnimationFrame(() => this.detectionLoop()); // start new frame immediately
    }
  }

  private async drawLoop() { // main screen refresh loop
    if (this.enabled) {
      if (!this.dom.video.paused) {
        const interpolated = this.human.next(this.human.result); // smoothen result using last-known results
        const processed = await this.human.image(this.dom.video); // get current video frame, but enhanced with this.human.filters
        this.human.draw.canvas(processed.canvas as HTMLCanvasElement, this.dom.canvas);

        const opt: Partial<H.DrawOptions> = { bodyLabels: `person confidence [score] and ${this.human.result?.body?.[0]?.keypoints.length} keypoints` };
        await this.human.draw.all(this.dom.canvas, interpolated, opt); // draw labels, boxes, lines, etc.
        this.perf(interpolated.performance); // write performance data
      }
      const now = this.human.now();
      this.fps.drawFPS = Math.round(1000 * 1000 / (now - this.timestamp.draw)) / 1000;
      this.timestamp.draw = now;
      this.status(this.dom.video.paused ? 'paused' : `fps: ${this.fps.detectFPS.toFixed(1).padStart(5, ' ')} detect | ${this.fps.drawFPS.toFixed(1).padStart(5, ' ')} draw`); // write status
      setTimeout(() => this.drawLoop(), 30); // use to slow down refresh from max refresh rate to target of 30 fps
    }
  }


}
