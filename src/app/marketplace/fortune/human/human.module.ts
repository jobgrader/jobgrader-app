import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HumanPageRoutingModule } from './human-routing.module';

import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { HumanJobPage } from './human-job.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    HumanPageRoutingModule,
  ],
  declarations: [
    HumanJobPage,
  ]
})
export class HumanPageModule {}
