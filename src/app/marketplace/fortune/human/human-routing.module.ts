import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// custom imports
import { HumanJobPage } from './human-job.page';


const routes: Routes = [
  {
    path: '',
    component: HumanJobPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HumanPageRoutingModule {}
