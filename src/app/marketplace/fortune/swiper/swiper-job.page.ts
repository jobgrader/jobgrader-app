import { Component, ElementRef, NgZone, OnInit, QueryList, Renderer2, ViewChild, ViewChildren, effect, inject } from '@angular/core';
import { GestureController, Platform } from '@ionic/angular';
import { Subject, finalize, interval, takeUntil, timer } from 'rxjs';


// custom imports
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-fortune-swiper-job',
  templateUrl: './swiper-job.page.html',
  styleUrls: ['./swiper-job.page.scss'],
})
export class SwiperJobPage extends BaseJobComponent implements OnInit {

  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;
  @ViewChild('up', { read: ElementRef }) up: ElementRef;
  @ViewChild('down', { read: ElementRef }) down: ElementRef;
  @ViewChild('heart', { read: ElementRef }) heart: ElementRef;


  private gestureCtrl = inject(GestureController);
  private renderer = inject(Renderer2);
  private zone = inject(NgZone);
  private plt = inject(Platform);


  private solutions: number[] = [];


  // startX: number = 0; // starting point of the swipe
  // endX: number = 0; //ending point of the swipe



  type = 'price_guess';
  allDone = false;
  description: string;

  private lastOnStart: number = 0;
  private DOUBLE_CLICK_THRESHOLD: number = 500;

  // constructor(
  //   private route: ActivatedRoute,
  //   private _NavController: NavController,
  //   private userPhotoServiceAkita: UserPhotoServiceAkita,
  //   private humanSevice: HumanService,
  //   private gestureCtrl: GestureController,
  //   private plt: Platform,
  //   private renderer: Renderer2,
  //   private zone: NgZone,
  //   private _SecureStorage: SecureStorageService,
  //   private humanService: HumanService
  // ) { }
  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('RatingJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.description = null
      } else {
        this.description = jobs[0]?.description;

        setTimeout(
          () => {
            this.setGestureForCards();

            // this.solutions.push(0);
            // this.solutions.push(1);
            // this.solutions.push(1);
            // this.solutions.push(0);

            // setTimeout(
            //     () => {
            //       console.log('SEND SOLUTION');

            //       this.solve(JSON.stringify(this.solutions));
            //       this.solutions = [];

            //     }, 2000
            //   );

          }, 200
        );
      }
    });
  }


  private setGestureForCards() {
    const cardArray = this.cards.toArray();

    for (let i  = 0; i < cardArray.length; i++) {
      const card = cardArray[i];

      const gesture = this.gestureCtrl.create({
        el: card.nativeElement,
        gestureName: 'fortune-swipe-tap',
        disableScroll: true,
        canStart: (ev) => {
          const now = Date.now();

          if (Math.abs(now - this.lastOnStart) <= this.DOUBLE_CLICK_THRESHOLD) {

            // this.renderer.setStyle(this.heart.nativeElement, 'transition', '.5s ease in');
            this.renderer.setStyle(this.heart.nativeElement, 'opacity', 1);

            this.renderer.addClass(card.nativeElement, 'hidden');

            // this.renderer.setStyle(card.nativeElement, 'transition', '.5s ease out');
            // this.renderer.setStyle(card.nativeElement, 'transform', `translateX(${+this.plt.width()*2}px) rotate(${ev.deltaX / 2}deg)`);


            setTimeout(() => {
              this.renderer.setStyle(this.heart.nativeElement, 'opacity', 0);
              this.solutions.push(0);
              this.removeJob();
            }, 500);

            this.lastOnStart = 0;
          } else {
            this.lastOnStart = now;
          }
        },
        onStart: (ev) => {
        },
        onMove: (ev) => {

          this.renderer.setStyle(card.nativeElement, 'transform', `translateX(${ev.deltaX}px) rotate(${ev.deltaX / 10}deg)`);

          if (ev.deltaX < 0) {

            this.renderer.setStyle(this.down.nativeElement, 'opacity', String(-ev.deltaX / 100));
            this.renderer.addClass(card.nativeElement, 'down');
            this.renderer.removeClass(card.nativeElement, 'up');

          } else {

            this.renderer.setStyle(this.up.nativeElement, 'opacity', String(ev.deltaX / 100));
            this.renderer.addClass(card.nativeElement, 'up');
            this.renderer.removeClass(card.nativeElement, 'down');

          }

        },
        onEnd: (ev) => {
          this.renderer.setStyle(card.nativeElement, 'transition', '.5s ease out');

          if (ev.deltaX > 150) {

            this.renderer.setStyle(card.nativeElement, 'transform', `translateX(${+this.plt.width()*2}px) rotate(${ev.deltaX / 2}deg)`);
            this.solutions.push(1);

            this.removeJob();

          } else if (ev.deltaX < -150) {

            this.renderer.setStyle(card.nativeElement, 'transform', `translateX(-${+this.plt.width()*2}px) rotate(${ev.deltaX / 2}deg)`);
            this.solutions.push(-1);

            this.removeJob();

          } else {

            this.renderer.removeStyle(card.nativeElement, 'transform');
            this.renderer.removeClass(card.nativeElement, 'up');
            this.renderer.removeClass(card.nativeElement, 'down');

          }

          setTimeout(() => {
            if (this.down) this.renderer.setStyle(this.down.nativeElement, 'opacity', '0');
            if (this.up) this.renderer.setStyle(this.up.nativeElement, 'opacity', '0');
          }, 100);

          this.lastOnStart = 0;
        }
      });

      gesture.enable(true);
    }
  }

  private removeJob() {
    setTimeout(() => {
      this.zone.run(() => {
        this.jobs().splice(0, 1);

        if (this.jobs().length === 0) {
          this.allDone = true;
          this.solve(JSON.stringify(this.solutions));
        } else {
          this.description = this.jobs()[0]?.description;
        }
      })
    }, 500);
  }
}
