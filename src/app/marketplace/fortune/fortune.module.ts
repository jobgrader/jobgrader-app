import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FortunePageRoutingModule } from './fortune-routing.module';

import { FortunePage } from './fortune.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { SwiperJobPage } from './swiper/swiper-job.page';
import { RatingJobPage } from './rating/rating-job.page';
import { ABJobPage } from './ab/ab-job.page';
import { QuizJobPage } from './quiz/quiz-job.page';
import { AiTrainingJobPage } from './ai-training/ai-training-job.page';
import { OCRJobPage } from './ocr/ocr-job.page';
import { AudioLabellingPage } from './audio-labelling/audio-labelling.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    FortunePageRoutingModule
  ],
  declarations: [
    FortunePage,
    SwiperJobPage,
    RatingJobPage,
    ABJobPage,
    QuizJobPage,
    AiTrainingJobPage,
    OCRJobPage,
    AudioLabellingPage,
  ]
})
export class FortunePageModule {}
