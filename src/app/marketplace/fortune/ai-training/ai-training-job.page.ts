import { Component, ElementRef, QueryList, Renderer2, ViewChild, ViewChildren, effect, inject } from '@angular/core';
import { Clipboard } from "@capacitor/clipboard";
import { TextToSpeech } from '@capacitor-community/text-to-speech';
import { IonContent, ToastController } from '@ionic/angular';


// custom imports
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { BaseJobComponent } from '../base-job/base-job.component';


@Component({
  selector: 'app-fortune-ai-training-job',
  templateUrl: './ai-training-job.page.html',
  styleUrls: ['./ai-training-job.page.scss'],
})
export class AiTrainingJobPage extends BaseJobComponent {

  @ViewChild(IonContent) content: IonContent;
  @ViewChildren('card', { read: ElementRef }) cards: QueryList<ElementRef>;
  @ViewChild('popover') popover: any;

  private renderer = inject(Renderer2);
  private _ToastController = inject(ToastController);
  private _Translate = inject(TranslateProviderService);

  private solutions: { selectedAnswer: number; results: { weight: number }[] }[] = [];
  private solutions2: { attributes: any }[] = [];

  type = 'aiTraining';
  allDone = false;
  description: string;

  index = 0;
  speakIndex = -1;
  upAndDown: [];

  isPopoverOpen = false;
  message = '';

  attributes = {
    helpfulness: 0,
    correctness: 0,
    coherence: 0,
    complexity: 0,
    verbosity: 0,
  }

  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('RatingJobPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {

        this.solutions = [];
        this.solutions2 = [];

      } else {

        if (!jobs.data) {
          jobs.forEach((job) => {
            let results = [];
            job.results.forEach((result) => {
              results.push({ weight: 0 });
            })
            this.solutions.push({ selectedAnswer: null, results });
          });
        }

      }
    });
  }

  async ionViewWillLeave() {
    // assure that the text-to-speech is stopped before when we leave the page
    await this.stop();
  }


  async select(promptIndex: number, selectedAnswer: number) {
    // console.log('AiTrainingJobPage#select; selectedAnswer', selectedAnswer);

    // assure that the text-to-speech is stopped before when we go to the next card
    // or when we upload the soltuion
    await this.stop();

    setTimeout(() => {
      // this.renderer.addClass(this.cards.toArray()[promptIndex].nativeElement, 'hidden');
      this.index++;
      this.content.scrollToTop(100);
      this.solutions[promptIndex].selectedAnswer = selectedAnswer;

      if (promptIndex === this.jobs().length - 1) {
        this.allDone = true;
        this.solve(JSON.stringify(this.solutions));
      }
    }, 200);
  }

  up(promptIndex: number, resultIndex: number) {
    // console.log('AiTrainingJobPage#up; promptIndex:', promptIndex);
    // console.log('AiTrainingJobPage#up; resultIndex:', resultIndex);

    this.solutions[promptIndex].results[resultIndex].weight = 1;
  }

  down(promptIndex: number, resultIndex: number) {
    // console.log('AiTrainingJobPage#down; promptIndex:', promptIndex);
    // console.log('AiTrainingJobPage#down; resultIndex:', resultIndex);

    this.solutions[promptIndex].results[resultIndex].weight = -1;
  }

  copy(text: string) {
    // console.log('AiTrainingJobPage#copy; text:', text);

    Clipboard.write({ string: text }).then(() => {
      this.presentToast(
        this._Translate.instant("FORTUNE.AI_TRAINING.COPY_TO_CLIPBOARD")
      );
    })
  }

  async speak(text, index) {
    // console.log('AiTrainingJobPage#speak; text:', text);
    // console.log('AiTrainingJobPage#speak; index:', index);

    // we cleanup the text from html tags
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = text;
    text = tempDiv.textContent || tempDiv.innerText || '';

    await this.stop();

    // IMPORTANT:
    await new Promise((resolve) => setTimeout(resolve, 500));

    this.speakIndex = index;

    TextToSpeech.speak(
      {
        text: text,
        lang: 'en-US',
        rate: 1.0,
        pitch: 1.0,
        volume: 1.0,
        category: 'playback',
      }
    )
      .then(
        (value) => {
          // console.log('AiTrainingJobPage#speak; finish value:', value);
        }
      )
      .catch(
        (error) => {
          // console.log('AiTrainingJobPage#speak; error:', error);
        }
      )
      .finally(
        () => {
          // console.log('AiTrainingJobPage#speak; finally');
          this.speakIndex = -1;
        }
      );
  };

  async stop() {
    // IMPORTANT: when plugin is in use and we try to stop it before run another speach, the plugin runs
    // always in a interrupted exeception, for that reason we catch it here so that the speak() method
    // does not break unexpected
    try {
      // it looks  like in certain situations the finally block for the  TextToSpeech.speak() method is not recorgnized
      // for that reason we need to gurantee that the speakIndex is reseted here
      this.speakIndex = -1;

      await TextToSpeech.stop();

    } catch (err) {
      // console.error(err);
    }
  };

  reset() {
    this.attributes = {
      helpfulness: 0,
      correctness: 0,
      coherence: 0,
      complexity: 0,
      verbosity: 0,
    }
  }

  async submit(promptIndex: number) {
    // console.log('AiTrainingJobPage#promptIndex; promptIndex:', promptIndex);

    // assure that the text-to-speech is stopped before when we go to the next card
    // or when we upload the soltuion
    await this.stop();

    if (this.attributes.helpfulness === 0 ||
      this.attributes.correctness === 0 ||
      this.attributes.coherence === 0 ||
      this.attributes.complexity === 0 ||
      this.attributes.verbosity === 0
    ) {
      this.presentToast(this._Translate.instant("FORTUNE.AI_TRAINING.ERRORS.ATTRBIUTES_ZERO"));
      return;
    }

    setTimeout(() => {
      // this.renderer.addClass(this.cards.toArray()[promptIndex].nativeElement, 'hidden');
      this.index++;
      this.content.scrollToTop(100);
      this.solutions2.push({ attributes: this.attributes });
      this.reset();

      if (promptIndex === this.jobs().data.length - 1) {
        // console.log('AiTrainingJobPage#submit; this.solutions2:', this.solutions2);

        this.allDone = true;
        this.solve(JSON.stringify(this.solutions2));
      }
    }, 200);
  }

  presentPopover(e: Event, text: string) {
    this.message = this._Translate.instant(text)
    this.popover.event = e;
    this.isPopoverOpen = true;

    // automatically hide the popover after 3 seconds
    setTimeout(() => {
      this.isPopoverOpen = false;
      this.message = '';
    }, 3000);
  }


  private presentToast(message: string) {
    this._ToastController
      .create({
        duration: 2000,
        position: "top",
        message,
      })
      .then((toast) => toast.present());
  }
}
