import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// custom imports
import { JobHowToPage } from './job-howto.page';


const routes: Routes = [
  {
    path: '',
    component: JobHowToPage
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class JobHowToRoutingModule {}
