import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';


// custom imports
import { NavigateToJobService } from '@services/navigate-to-job';
import { HumanService } from 'src/app/core/providers/human/human.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { JobDetail } from 'src/app/core/providers/human/JobDetail';


@Component({
  selector: 'app-job-howto',
  templateUrl: './job-howto.page.html',
  styleUrls: ['./job-howto.page.scss'],
})
export class JobHowToPage implements OnInit {

  private workerAddress: string;
  private escrowAddress: string;
  private from: string;
  private demo: boolean;
  private dataBase64: string;

  private language: string;

  job: JobDetail;


  constructor(
    private route: ActivatedRoute,
    private navController: NavController,
    private secureStorageService: SecureStorageService,
    private humanService: HumanService,
    private navigateToJobService: NavigateToJobService,
  ) { }

  async ngOnInit() {
    this.workerAddress = await this.secureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    this.escrowAddress = this.route.snapshot.paramMap.get('id');
    this.from = this.route.snapshot.queryParams['from'];
    this.demo = this.route.snapshot.queryParams['demo'];
    this.dataBase64 = this.route.snapshot.queryParams['data'];

    // console.log('JobHowToPage#ngOnInit this.demo:', this.demo);
    // console.log('JobHowToPage#ngOnInit this.dataBase64:', this.dataBase64);

    if (this.demo && this.dataBase64) {
      this.job = JSON.parse(Buffer.from(this.dataBase64, 'base64').toString());
      return;
    }

    this.humanService.getJobDetails(80002, this.escrowAddress)
          .subscribe({

            next: (job: JobDetail) => {
              // console.log('JobHowToPage#ngOnInit job:', job);

              this.job = job;

            },
            error: (err: any) => {
              this.job = null;
            }
          });
  }

  goBack() {
    let navigateBack = `/jobs/details/${this.escrowAddress}`;

    const queryParams: Params = {};
    queryParams.from = this.from;

    if (this.demo && this.dataBase64) {
      queryParams.demo = this.demo;
      queryParams.data = this.dataBase64;
    }

    this.navController.navigateBack(navigateBack, { queryParams });
  }

  async goToJob() {
    this.navigateToJobService.goToJob(this.job, this.demo, this.dataBase64);
  }
}
