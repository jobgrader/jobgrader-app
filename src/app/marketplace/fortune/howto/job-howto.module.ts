import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';


// custom imports
import { NavigateToJobService } from '@services/navigate-to-job';
import { JobHowToRoutingModule } from './job-howto-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { JobHowToPage } from './job-howto.page';


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    JobHowToRoutingModule,
  ],
  providers: [
    NavigateToJobService
  ],
  declarations: [
    JobHowToPage,
  ]
})
export class JobHowToModule {}
