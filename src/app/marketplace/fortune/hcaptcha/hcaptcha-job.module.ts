import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HCapchtaJobRoutingModule } from './hcaptcha-job-routing.module';

import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { HCaptchaJobPage } from './hcaptcha-job.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    HCapchtaJobRoutingModule,
  ],
  declarations: [
    HCaptchaJobPage,
  ]
})
export class HCaptchaJobModule {}
