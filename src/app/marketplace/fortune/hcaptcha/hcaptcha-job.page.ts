import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { NavController } from '@ionic/angular';
import * as Sentry from '@sentry/angular-ivy';
import { Subject, Subscription, finalize, takeUntil, timer } from 'rxjs';


// custom imports
import { environment } from 'src/environments/environment';
import { ThemeSwitcherService } from 'src/app/core/providers/theme/theme-switcher.service';
import { BaseJobComponent } from '../base-job/base-job.component';

declare var hcaptcha: any;

@Component({
  selector: 'app-hcaptcha',
  templateUrl: './hcaptcha-job.page.html',
  styleUrls: ['./hcaptcha-job.page.scss'],
})
export class HCaptchaJobPage extends BaseJobComponent implements OnInit {

  private themeService = inject(ThemeSwitcherService);

  scan = '';
  allDone = false;
  hCaptchaShowing = false;
  jobReloading = false;
  type = 'hcaptcha';
  goBackAfterDone = false;


  private captchaId;
  private _consoleError;
  private timestampSubscription: Subscription;
  private siteKey = environment.hcaptcha.siteKey;
  // private secret = environment.hcaptcha.secret;


  ionViewWillEnter() {
    this.hCaptchaShowing = false;
    this.jobReloading = false;

    this._consoleError = (window as any).console.error;

    (window as any).console.error = (error) => { this.onError(error); };
  }

  ionViewDidEnter() {
    // once view is loaded wait 500ms and auto load first hcaptcha job
    setTimeout(() => { this.initCaptcha(); }, 500);
  }

  ionViewWillLeave() {
    if (this._consoleError) {
      (window as any).console.error = this._consoleError;
      this._consoleError = null;
    }

    if (this.timestampSubscription) {
      if (!this.timestampSubscription.closed) {
        this.timestampSubscription.unsubscribe();
      }
    }

    if (this.captchaId) {
      hcaptcha.reset(this.captchaId);
      this.captchaId = null;
    }
    document.querySelector('body > div:last-of-type')?.remove();
  }


  initCaptcha() {

    this.hCaptchaShowing = true;

    if (!this.captchaId) {
      this.captchaId = hcaptcha.render('h-captcha', {
        sitekey: this.siteKey,
        theme: this.themeService.getCurrentTheme(),
        hl: this.language,
        'error-callback': (error) => this.onError(error),
      });
    }

    this.showCaptcha();
  }

  private showCaptcha() {
    const captcha = hcaptcha.execute(this.captchaId, { async: true });

    if (captcha) {
      captcha
        .then((data) => {
          console.log('HCaptchaJobPage#showCaptcha; succes:', data);

          if (data?.success === false) {
            // this sends the error even to Sentry Log
            console.error('HCaptchaJobPage#showCaptcha; success === false', data);
          }

          this.solve(JSON.stringify({ resonse: data.repsonse }));

          this.jobReloading = true;
          this.showCaptcha();
        })
        .catch((error) => {
          // mburger TODO: maybe check which type of error and show a message if needed
          // 'challenge-closed' for example doesnt need any info but
          // 'challenge-error' and others should show an error
          // 'challenge-expired' should maybe reopen the captcha
          // have a look at the other errors here https://docs.hcaptcha.com/configuration
          console.log('HCaptchaJobPage#showCaptcha; error:', error);

          this.hCaptchaShowing = false;
          this.jobReloading = false;
        });
    }
  }

  private onError(error) {
    if (this._consoleError) {
      this._consoleError("HcaptchaPage#onError; error:", error);
    } else {
      console.error("HcaptchaPage#onError; error:", error);
    }

    if(environment.production) {
      // log error to sentry
      try {
          Sentry.captureException(error.originalError || error);
      } catch (e) {
          // do nothing
      }
    }
  }

}
