import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


// custom imports
import { HCaptchaJobPage } from './hcaptcha-job.page';


const routes: Routes = [
  {
    path: '',
    component: HCaptchaJobPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HCapchtaJobRoutingModule {}
