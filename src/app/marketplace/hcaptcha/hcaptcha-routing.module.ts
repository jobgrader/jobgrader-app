import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HcaptchaPage } from './hcaptcha.page';

const routes: Routes = [
  {
    path: '',
    component: HcaptchaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HcaptchaPageRoutingModule {}
