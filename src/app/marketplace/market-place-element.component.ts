import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-market-place-element',
  templateUrl: './market-place-element.component.html',
  styleUrls: ['./market-place-element.component.scss'],
})
export class MarketPlaceElementComponent implements OnInit {
  /** The path to the image of the element */
  @Input() elementSrc: string;
  /** The name of the element */
  @Input() elementName: string;
  constructor(
      private navController: NavController
  ) { }

  public ngOnInit(): void {}

}
