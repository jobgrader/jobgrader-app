import { Injectable } from '@angular/core';
import { Capacitor } from '@capacitor/core';

@Injectable({
  providedIn: 'root'
})
export class PwaVideoRecordingService {

  private stream: MediaStream;
  private recorder: MediaRecorder;
  private chunks: Blob[] = [];
  private mimeType: { type: string, extension: string };

  constructor() {
    const supportedMimeTypes = this.getSupportedMimeTypes();
    if (supportedMimeTypes?.length > 0) {
      this.mimeType = supportedMimeTypes[0];
    }
   }

  public async startRecording(): Promise<void> {
    try {

      this.stream = await navigator.mediaDevices.getUserMedia({
        video: {
          echoCancellation: true,
        }, audio: {
          echoCancellation: true
        }
      });

      const container = document.getElementById("videoContainer");
      let videoElement = document.getElementById("video") as HTMLVideoElement;

      if (!videoElement) {
        videoElement = document.createElement('video') as HTMLVideoElement;

        videoElement.height = container.getBoundingClientRect().height;
        videoElement.width = container.getBoundingClientRect().width;
        videoElement.setAttribute("id", "video");

        container.appendChild(videoElement);
      }

      if (Capacitor.getPlatform() === 'ios') {
        videoElement.setAttribute("playsinline", "playsinline");
      }

      videoElement.muted = true;

      try {
        videoElement.srcObject = this.stream;

        await videoElement.play();

        this.chunks = [];
        this.recorder = new MediaRecorder(this.stream,
          {
          mimeType: this.mimeType.type
          }
        )

        this.recorder.ondataavailable = (e) => {
          if (e.data?.size > 0) {
            this.chunks.push(e.data);
          }
        };

        // IMPORTANT: safari might not emit ondataavailable unless you provide a timeslice
        // this force the MediaRecord to release data periodically, 0.5 second in this case
        this.recorder.start(500);

      } catch (error) {
        console.error('Failed to start video stream:', error);
      }

    } catch (error) {
      console.error('Error accessing camera:', error);
      throw error;
    }
  }

  public async stopRecording(): Promise<{blob: Blob, extension: string}> {
    return new Promise(resolve => {
      const tracks = this.stream?.getTracks();
      tracks?.forEach(track => track.stop());

      if (this.recorder) {
        // this.recorder.ondataavailable = (data) => {
        //   this.recorder.stop();
        //   resolve(data.data);
        // }
        this.recorder.onstop = (e) => {
          const blob = new Blob(this.chunks, { type: this.chunks[0].type });
          this.chunks = [];
          resolve({ blob: blob, extension: this.mimeType.extension });
        };
      }

    })
  }

  private getSupportedMimeTypes() {
    const mimeTypes = [
      { type: 'video/webm;codecs=vp9', extension: 'webm' },      // Best size/quality for web => FULL HD 10sec = 2.4 MB;
      { type: 'video/mp4;codecs=h264', extension: 'mp4' },        // Great balance, widely supported => FULL HD 10sec = ???
      { type: 'video/mp4;codecs=vp9', extension: 'mp4' },         // VP9 in MP4, newer, less common => FULL HD 10sec = 2.5 MB; HD 10sec =
      { type: 'video/webm;codecs=vp8', extension: 'webm' },       // Good size/quality ratio, lower than VP9 => FULL HD 10sec = 4.3 MB
      { type: 'video/mp4;codecs=avc1.42E01E', extension: 'mp4' }, // H.264, standard and efficient => FULL HD 10sec = 3.99 MB
      { type: 'video/x-matroska;codecs=avc1,opus', extension: 'mkv' }, // Matroska with AVC1,opus => FULL HD 10sec = 3.95 MB
      // { type: 'audio/webm;codecs=opus', extension: 'webm' },      // Opus audio, good compression
      // { type: 'audio/ogg;codecs=opus', extension: 'ogg' },        // Ogg Opus audio, good for audio files
      { type: 'video/ogg;codecs=theora', extension: 'ogv' },      // Older, larger files, less efficient => FULL HD 10sec =
      { type: 'video/mp4;codecs=vp8', extension: 'mp4' },         // VP8 in MP4, less efficient than VP9 => FULL HD 10sec =
    ];

    const supportedTypes = mimeTypes.filter(({ type }) => MediaRecorder.isTypeSupported(type));

    console.log('PWAVideoRecordingService#getSupportedMimeTypesX; supportedTypes:', supportedTypes);
    return supportedTypes;
  }
}
