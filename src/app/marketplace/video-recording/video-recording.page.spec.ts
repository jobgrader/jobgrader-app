import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VideoRecordingPage } from './video-recording.page';

describe('VideoRecordingPage', () => {
  let component: VideoRecordingPage;
  let fixture: ComponentFixture<VideoRecordingPage>;

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoRecordingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
