import { HttpEvent, HttpEventType } from '@angular/common/http';
import { AfterViewInit, Component, NgZone, effect, inject } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Filesystem, Directory, Encoding } from '@capacitor/filesystem';


// custom imports
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { PwaVideoRecordingService } from './pwa-video-recording.service';
import { BaseJobComponent } from '../fortune/base-job/base-job.component';
import { Subscription, timer } from 'rxjs';
import { format, addSeconds } from 'date-fns';

@Component({
  selector: 'app-video-recording',
  templateUrl: './video-recording.page.html',
  styleUrls: ['./video-recording.page.scss'],
})
export class VideoRecordingPage extends BaseJobComponent implements AfterViewInit {

  private _Translate = inject(TranslateProviderService);
  private _ToastController = inject(ToastController);
  private _NgZone = inject(NgZone);
  private _PwaVideoRecording = inject(PwaVideoRecordingService);
  private _LoadingController = inject(LoadingController);

  type = 'video_labelling';
  isRecording = false;
  isPlaying = false;
  activateButtons = false;
  videoUrl: string;
  progress = 0;
  isMinimumLength = false;
  text: string;

  // private config: VideoRecorderPreviewFrame = null;
  private solutions: { url: string }[] = [];
  private mediaUpload: { base64: string, mimeType?: string, extension?: string, assignmentId: string };
  public timer = 0;
  private timerSubscription: Subscription;
  loading: HTMLIonLoadingElement;


  constructor() {
    super();

    effect(() => {
      const jobs = this.jobs();
      // console.log('VideoRecordinPage#constructor; jobs:', jobs);

      if (jobs.length === 0) {
        this.text = null;
      } else {
        this.text = jobs[0]?.text;
      }
    });
  }

  async ngAfterViewInit() {

    this.loading = await this._LoadingController.create({
      message: this._Translate.instant('PLEASE_WAIT')
    });

    this._NgZone.run(() => {
      setTimeout(() => {
        // const videoContainer = document.getElementById("videoContainer").getBoundingClientRect();

        // this.config = {
        //   id: 'videoContainer',
        //   stackPosition: 'front', // 'front' overlays your app', 'back' places behind your app.
        //   width: videoContainer.width,
        //   height: videoContainer.height,
        //   x: videoContainer.x, //15,
        //   y: videoContainer.y,// 140,
        //   borderRadius: 0
        // };

        this.activateButtons = true;

      }, 1000);
    })

  }

  ionViewWillLeave() {
    this.stopRecording(true);
  }

  formatTime(seconds) {
    const date = addSeconds(new Date(0), seconds);
    return format(date, 'mm:ss');
  }

  onScroll(event) {
    // if (Capacitor.isNativePlatform() && Capacitor.getPlatform() === 'ios') {
    //   const videoContainer = document.getElementById("videoContainer").getBoundingClientRect();
    //   VideoRecorder.editPreviewFrameConfig({
    //     id: 'videoContainer',
    //     stackPosition: 'front', // 'front' overlays your app', 'back' places behind your app.
    //     width: videoContainer.width,
    //     height: videoContainer.height,
    //     x: videoContainer.x, //15,
    //     y: videoContainer.y,// 140,
    //     borderRadius: 0
    //   });
    // }
  }

  async startRecording() {

    this.isRecording = true;
    this.videoUrl = null;

    // if (Capacitor.isNativePlatform() && Capacitor.getPlatform() === 'ios') {
    //   await VideoRecorder.initialize({
    //     camera: VideoRecorderCamera.FRONT, // Can use BACK
    //     previewFrames: [this.config]
    //   });

    //   await VideoRecorder.startRecording();
    // } else {
    await this._PwaVideoRecording.startRecording();
    // }

    // this.job.manifest.annotations.video.min = 5;

    //  we us the progress bar just when max value is present
    if (this.job.manifest.annotations?.video?.max) {

      // if there is no min value, we set the minimum length to true, so the progress bar
      // has always the green valid color
      if (!this.job.manifest.annotations?.video?.min) {
        this.isMinimumLength = true;
      }

      // console.log('VideoRecordinPage#startRecording; before timer', new Date());

      this.timer = 0;
      this.timerSubscription = timer(1000, 1000)
        .subscribe(() => {
          this.timer += 1;
          // console.log('VideoRecordinPage#startRecording; time', new Date());

          this.progress = this.timer / Number(this.job.manifest.annotations.video.max);

          if (this.timer === Number(this.job.manifest.annotations.video.min)) {
            this.isMinimumLength = true;
          }

          if (this.timer === Number(this.job.manifest.annotations.video.max)) {
            this.stopRecording();
          }
        });
    }
  }

  async stopRecording(exit = false) {

    this.isRecording = false;
    if (this.timerSubscription && !this.timerSubscription.closed) {
      this.timerSubscription.unsubscribe();
    }
    this.progress = 0;
    this.isMinimumLength = false;

    const videoContainer = document.getElementById('videoContainer');
    const existingVideos = document.getElementsByTagName("video");

    for (let i = 0; i < existingVideos.length; i++) {
      existingVideos[i].remove();
    }

    // if (Capacitor.isNativePlatform() && Capacitor.getPlatform() === 'ios') {

    //   const recording = await VideoRecorder.stopRecording();

    //   if (recording) {
    //     this.videoUrl = recording.videoUrl;
    //     console.log('Recorded video URL:', this.videoUrl);

    //   } else {
    //     console.warn('Recording failed.');
    //   }

    //   await VideoRecorder.destroy();

    //   this.mediaUpload =
    //   {
    //     base64: await this.videoUrlToBase64(this.videoUrl),
    //     extension: this.videoUrl.split('.').pop()?.toLowerCase(),
    //     assignmentId: this.assignmentId,
    //   }

    // } else {

    const result = await this._PwaVideoRecording.stopRecording();
    this.videoUrl = URL.createObjectURL(result.blob);

    this.mediaUpload =
    {
      base64: await this.blobToBase64(result.blob, result.blob.type),
      extension: result.extension,
      assignmentId: this.assignmentId,
    }
    // }

    if (exit) {
      return;
    }

    if (this.job.manifest.annotations?.video?.min && this.timer < this.job.manifest.annotations.video.min) {
      this.videoUrl = null;
      this.displayVideoTooShort();
      return;
    }


    const video: HTMLVideoElement = document.createElement("video");

    video.controls = true;

    const boundingClient = videoContainer.getBoundingClientRect();
    video.width = boundingClient.width;
    video.height = boundingClient.height;

    video.src = this.videoUrl;

    // video.autoplay = true;
    video.setAttribute("id", "video");
    videoContainer.appendChild(video);

  }

  async playRecording() {

    const existingVideo: HTMLVideoElement = <HTMLVideoElement>document.getElementById("video");

    this.isPlaying = true;

    if (existingVideo) {
      existingVideo.onpause = () => {
        this.isPlaying = false;
      }
      existingVideo.onabort = () => {
        this.isPlaying = false;
      }
      existingVideo.onended = () => {
        this.isPlaying = false;
      }
      existingVideo.muted = false;
      existingVideo.play();
    } else {
      const videoContainer = document.getElementById('videoContainer');
      const video: HTMLVideoElement = document.createElement("video");
      video.setAttribute("id", "video");

      video.muted = false;
      video.src = this.videoUrl;

      const boundingClient = videoContainer.getBoundingClientRect();
      video.width = boundingClient.width;
      video.height = boundingClient.height;

      videoContainer.appendChild(video);

      video.play();
    }

  }

  pausePlaying() {

    const video: HTMLVideoElement = <HTMLVideoElement>document.getElementById("video");

    if (video) {
      video.pause();
      this.isPlaying = false;
    }

  }

  async convertUrlToBlob() {

    return await fetch(this.videoUrl).then(r => r.blob());

  }

  displayToastAndBack() {
    this._ToastController.create({
      message: this._Translate.instant('FORTUNE.VIDEO_LABELLING.SUCCESS'),
      duration: 2000,
      position: 'top'
    }).then(toast => {
      toast.present();
      this.goBack();
    })
  }

  submit() {
    this.humanService.createPresignedUrl(this.mediaUpload.assignmentId, this.mediaUpload.extension, this.mediaUpload.mimeType)
      .subscribe({
        next: async (res: any) => {
          // console.log('VideoRecordinPage#submit; createPresignedUrl res:', res);

          if (res.url) {
            const url = res.url.split('?')[0];
            // console.log('VideoRecordinPage#submit; url:', url);

            await this.loading.present();

            this.humanService.uploadMedia(res.url, this.mediaUpload.base64)
              .subscribe({
                next: async (event: HttpEvent<any>) => {
                  console.log('VideoRecordinPage#submit; uploadMedia event:', event);

                  switch (event.type) {
                    case HttpEventType.Sent:
                      console.log('VideoRecordinPage#submit; uploadMedia Sent:', this.mediaUpload.assignmentId, this.mediaUpload.extension, this.mediaUpload.mimeType);
                      break;
                    case HttpEventType.UploadProgress:
                      // compute and show the % done:
                      const percentDone = event.total ? Math.round(100 * event.loaded / event.total) : 0;

                      this.loading.message = this._Translate.instant('VIDEO_UPLOAD_PROGRESS', { percentDone: percentDone.toString() });

                      console.log(`VideoRecordinPage#submit; uploadMedia UploadProgress: is ${percentDone}% uploaded.`);
                      break;
                    case HttpEventType.Response:
                      console.log(`VideoRecordinPage#submit; uploadMedia Response: upload done!`, event);

                      await this.loading.dismiss();

                      this.solutions.push({ url: url });
                      console.log('VideoRecordinPage#submit; this.solutions:', this.solutions);

                      this.solve(JSON.stringify(this.solutions));

                      break;
                    default:
                      console.log(`VideoRecordinPage#submit; uploadMedia Response: surprising upload event: ${event.type}.`);
                  }
                },
                error: (err: any) => {
                  console.log('VideoRecordinPage#submit; uploadMedia error:', err);

                  this.displayVideoUploadError();
                }
              });
          }

        },
        error: (err: any) => {
          console.log('VideoRecordinPage#submit; uploadMedia error:', err);

          this.displayVideoUploadError();
        }
      });
  }

  // helper function to convert Blob to Base64 string
  private blobToBase64(blob: Blob, mimeType: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        const dataUrlPrefix = `data:${mimeType};base64,`;
        const base64WithDataUrlPrefix = <string>reader.result;
        const base64 = base64WithDataUrlPrefix.replace(dataUrlPrefix, '');
        resolve(base64);
      };
      reader.onerror = reject;
      reader.readAsDataURL(blob);
    });
  }

  private async videoUrlToBase64(videoUrl: string): Promise<string> {
    // Read the video file using Filesystem plugin
    const fileData = await Filesystem.readFile({
      path: videoUrl.replace(`${window.origin}/_capacitor_file_/private`, "file://"),
    });

    // fileData.data is the base64 string of the video
    return <string>fileData.data;
  }

  private async displayVideoTooShort() {
    const toast = await this._ToastController.create({
      message: this._Translate.instant('FORTUNE.VIDEO_LABELLING.VIDEO_TOO_SHORT'),
      duration: 2000,
      position: 'top'
    });

    await toast.present();
  }

  private async displayVideoUploadError() {
    const toast = await this._ToastController.create({
      message: this._Translate.instant('FORTUNE.VIDEO_LABELLING.VIDEO_UPLOAD_ERROR'),
      duration: 2000,
      position: 'top'
    });

    await toast.present();
  }
}
