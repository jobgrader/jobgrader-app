import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoRecordingPageRoutingModule } from './video-recording-routing.module';

import { VideoRecordingPage } from './video-recording.page';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JobHeaderModule,
    TranslateModule,
    VideoRecordingPageRoutingModule
  ],
  declarations: [VideoRecordingPage]
})
export class VideoRecordingPageModule {}
