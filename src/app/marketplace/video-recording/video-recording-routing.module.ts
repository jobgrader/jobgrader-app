import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoRecordingPage } from './video-recording.page';

const routes: Routes = [
  {
    path: '',
    component: VideoRecordingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideoRecordingPageRoutingModule {}
