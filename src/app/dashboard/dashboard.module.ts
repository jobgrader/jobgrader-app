import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DashboardPage } from "./dashboard.page";
import { SharedModule } from "../shared/shared.module";
import { AuthenticationGuard } from "../core/guards/authentication/authentication-guard.service";

const routes: Routes = [
  {
    path: "",
    component: DashboardPage,
    // canActivate: [AuthenticationGuard],
    children: [
      {
        path: "crypto-activities",
        loadChildren: () =>
          import("../crypto-activities/crypto-activities.module").then(
            (m) => m.CryptoActivitiesPageModule
          ),
      },
      // {
      //   path: "kyc",
      //   loadChildren: () =>
      //     import("../kyc/kyc.module").then((m) => m.KycPageModule),
      // },

      {
        path: "legal-documents",
        loadChildren: () =>
          import("../legal-documents/legal-documents.module").then(
            (m) => m.LegalDocumentsPageModule
          ),
      },

      {
        path: "marketplace/:key",
        loadChildren: () =>
          import("../marketplace/marketplace.module").then(
            (m) => m.MarketplacePageModule
          ),
      },
      {
        path: 'hcaptcha',
        loadChildren: () =>
        import('../marketplace/hcaptcha/hcaptcha.module').then(
          (m) => m.HcaptchaPageModule
        )
      },
      {
        path: "nft",
        loadChildren: () =>
          import("../nft/nft.module").then((m) => m.NftPageModule),
      },

      {
        path: "personal-details",
        loadChildren: () =>
          import("../personal-details/personal-details.module").then(
            (m) => m.PersonalDetailsPageModule
          ),
      },
      {
        path: "requests",
        loadChildren: () =>
          import("../requests/requests.module").then(
            (m) => m.RequestsPageModule
          ),
      },

      {
        path: 'certificates-page',
        loadChildren: () =>
          import('../certificates-page/certificates-page.module').then(
            (m) => m.CertificatesPagePageModule
          )
      },

      {
        path: "tab-home",
        // canActivate: [AuthenticationGuard],
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab-home/tab-home.module").then(
                (m) => m.TabHomePageModule
              ),
          },
        ],
      },
      // {
      //   path: "tab-contact",
      //   // canActivate: [AuthenticationGuard],
      //   children: [
      //     {
      //       path: "",
      //       loadChildren: () =>
      //         import("../contact/contact-list/contact-list.module").then(
      //           (m) => m.ContactListPageModule
      //         ),
      //     },
      //   ],
      // },
      {
        path: "tab-profile",
        // canActivate: [AuthenticationGuard],
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab-profile/tab-profile.module").then(
                (m) => m.TabProfilePageModule
              ),
          },
        ],
      },
      {
        path: "tab-settings",
        // canActivate: [AuthenticationGuard],
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../tab-settings/tab-settings.module").then(
                (m) => m.TabSettingsPageModule
              ),
          },
        ],
      },
      // {
      //   path: "tab-qrcode",
      //   // canActivate: [AuthenticationGuard],
      //   children: [
      //     {
      //       path: "",
      //       loadChildren: () =>
      //         import("../contact/contact-own/contact-own.module").then(
      //           (m) => m.ContactOwnPageModule
      //         ),
      //     },
      //   ],
      // },
      {
        path: "payments",
        // canActivate: [AuthenticationGuard],
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../payments/payments.module").then(
                (m) => m.PaymentsPageModule
              ),
          },
          {
            path: "crypto-account-page",
            loadChildren: () =>
              import(
                "../payments/crypto-account-page/crypto-account-page.module"
              ).then((m) => m.CryptoAccountPagePageModule),
          },
          {
            path: "network-account-details",
            loadChildren: () =>
              import(
                "../payments/network-account-details/network-account-details.module"
              ).then((m) => m.NetworkAccountDetailsPageModule),
          },
          {
            path: "wallet-settings",
            loadChildren: () =>
              import("../payments/wallet-settings/wallet-settings.module").then(
                (m) => m.WalletSettingsPageModule
              ),
          },
        ],
      },

      {
        path: "",
        redirectTo: "/dashboard/tab-home",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  declarations: [DashboardPage],
})
export class DashboardPageModule {}
