import { Component, OnInit, ViewChild } from '@angular/core';
import { ThemeSwitcherService } from '../core/providers/theme/theme-switcher.service';
import { TranslateProviderService } from '../core/providers/translate/translate-provider.service';
import { NavController, IonSlides } from '@ionic/angular';

interface AltWalkthroughContent {
  image: string;
  header: string;
  content: string;
}

@Component({
  selector: 'app-alt-walkthrough',
  templateUrl: './alt-walkthrough.page.html',
  styleUrls: ['./alt-walkthrough.page.scss'],
})
export class AltWalkthroughPage implements OnInit {
  @ViewChild(IonSlides) public slider: IonSlides;

  public slides: Array<AltWalkthroughContent> = [];
  // private destroy$ = new Subject();
  public activeIndex: number = 0;

  public sliderOptions = {
    initialSlide: 0,
    speed: 500
  };

  constructor(
    private _ThemeSwitcherService: ThemeSwitcherService,
    private _TranslateProviderService: TranslateProviderService,
    private _NavController: NavController
  ) { }

  async ngOnInit() {
    var theme = this._ThemeSwitcherService.getCurrentTheme();
    var lang = await this._TranslateProviderService.getLangFromStorage();
    console.log(`theme: ${theme}`);
    console.log(`lang: ${lang}`);
    var folder = `${lang}-${theme}`;
    [1,2,3,4,5,6,7].forEach(n => {
      this.slides.push({
        image: `../../assets/alt-walkthrough/${folder}/${n}.png`,
        header: this._TranslateProviderService.instant(`ALTWALKTHROUGH.header.${n}`),
        content: this._TranslateProviderService.instant(`ALTWALKTHROUGH.content.${n}`)
      })
    })
  }

  leftAction() {
    this._NavController.back();
  }

  rightAction() {
    this._NavController.back();
  }

  slideDidChange() {
    this.slider.getActiveIndex().then(index => {
        this.activeIndex = index;
        console.log(this.activeIndex);
    });
  }

  // public ngOnDestroy(): void {
  //   this.destroy$.next();
  // }

}
