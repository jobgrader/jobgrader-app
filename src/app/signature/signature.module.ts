import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { SignaturePage } from './signature.page';
import { AuthenticationGuard } from '../core/guards/authentication/authentication-guard.service';
import { UserProviderResolver } from '../core/resolvers/user/user-provider.resolver';
import { ClaimDeeplinkProviderResolver } from '../core/resolvers/claim-deeplink/claim-deeplink-provider.resolver';
import {
  VerificationDeeplinkAvailabilityProviderGuard
} from '../core/guards/verification-deeplink-availability/verification-deeplink-availability-provider.guard';
import {
  SignatureDeeplinkAvailabilityProviderGuard
} from '../core/guards/signature-deeplink-availability/signature-deeplink-availability-provider.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthenticationGuard,
      VerificationDeeplinkAvailabilityProviderGuard,
      SignatureDeeplinkAvailabilityProviderGuard
    ],
    component: SignaturePage,
    resolve: {
      user: UserProviderResolver,
      claimedDeeplinkPayload: ClaimDeeplinkProviderResolver
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [SignaturePage]
})
export class SignaturePageModule {}
