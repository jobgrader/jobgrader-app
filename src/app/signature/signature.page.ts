import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { User } from '../core/models/User';
import { DeeplinkProviderService } from '../core/providers/deeplink/deeplink-provider.service';
import { NavController } from '@ionic/angular';

const SIGNATURE_REJECT_STATUS = 3;
const SIGNATURE_ACCEPT_STATUS = 4;

@Component({
  selector: 'app-signature',
  templateUrl: './signature.page.html',
  styleUrls: ['./signature.page.scss'],
})
export class SignaturePage implements OnInit {

  user: User = {};
  private claimedDeeplinkPayload: any;
  private signatureData: any;
  private ngOnInitExecuted: any;

  constructor(
    private nav: NavController,
    private activatedRoute: ActivatedRoute,
    private deeplinkProviderService: DeeplinkProviderService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  goBack() {
    this.nav.navigateBack('/dashboard');
  }

  async reject() {
    const { signedDateTime, expiryDateTime } = this.getSignatureSignedExpiryDates();
    this.signatureData =
    this.deeplinkProviderService.temporarySignaturedDeeplinkResponse(
      this.claimedDeeplinkPayload.accountId,
      signedDateTime,
      expiryDateTime,
      SIGNATURE_REJECT_STATUS
    );

    const link = document.createElement('a');
    document.body.appendChild(link);
    link.setAttribute('style', 'display: none');
    link.href = `daimler-demo://daimler-demo.com/sign?string=${this.deeplinkProviderService.encodePayload(
      this.signatureData
    )}`;
    link.click();
    link.remove();
  }

  async signature() {
    const { signedDateTime, expiryDateTime } = this.getSignatureSignedExpiryDates();
    this.signatureData =
    this.deeplinkProviderService.temporarySignaturedDeeplinkResponse(
      this.claimedDeeplinkPayload.accountId,
      signedDateTime,
      expiryDateTime,
      SIGNATURE_ACCEPT_STATUS
    );

    const link = document.createElement('a');
    document.body.appendChild(link);
    link.setAttribute('style', 'display: none');
    link.href = `daimler-demo://daimler-demo.com/sign?string=${this.deeplinkProviderService.encodePayload(
      this.signatureData
    )}`;
    link.click();
    link.remove();
  }

  private getSignatureSignedExpiryDates() {
    const signedDateTime = new Date().toUTCString();
    const expiryDateTime = new Date(+new Date() + 3 * 60 * 60 * 1000).toUTCString();

    return { signedDateTime, expiryDateTime };
  }

  private async componentInit() {
    const { user, claimedDeeplinkPayload } = this.activatedRoute.snapshot.data;
    this.user = user;
    this.claimedDeeplinkPayload = claimedDeeplinkPayload;
    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }

}
