import { Component, Input, OnInit } from '@angular/core';
import { ApiProviderService } from 'src/app/core/providers/api/api-provider.service';
import { AlertController, ModalController, Platform, ToastController } from '@ionic/angular';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';
import { ethers, Wallet } from 'ethers';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import * as CryptoJS from 'crypto-js';
import { UDIDNonce } from 'src/app/core/providers/device/udid.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { KeyExportComponent } from 'src/app/sign-up/components/step-5/key-export/key-export.component';
import { LockService } from 'src/app/lock-screen/lock.service';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';
import { File } from '@awesome-cordova-plugins/file/ngx';
import { ActivityKeys, UserActivitiesService } from 'src/app/core/providers/user-activities/user-activities.service';
import { eth } from 'web3';
import * as Identicon from "identicon.js";

import { generateMnemonic } from 'bip39';

declare var window: any;

@Component({
  selector: 'app-import-userkey',
  templateUrl: './import-userkey.component.html',
  styleUrls: ['./import-userkey.component.scss'],
})
export class ImportUserkeyComponent implements OnInit {
  @Input() initial: boolean;
  @Input() address?: string;

  private existingUserMnemonic;
  private existingImportedWallets;
  private existingUserAddress;

  constructor(
    private _ModalController: ModalController,
    private _ApiProviderService: ApiProviderService,
    private _Platform: Platform,
    private _SecureStorageService: SecureStorageService,
    private _ToastController: ToastController,
    private _AlertController: AlertController,
    private _Translate: TranslateProviderService,
    private _LockService: LockService,
    private _CryptoProviderService: CryptoProviderService,
    private _UserActivity: UserActivitiesService,
    private _Chooser: Chooser,
    private _File: File
  ) { }

  async ngOnInit() {
    console.log(this.initial);
    this.existingUserMnemonic = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletMnemonic, false);
    this.existingUserAddress = await this._SecureStorageService.getValue(SecureStorageKey.web3WalletPublicKey, false);
    var existingImportedWalletsString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
    this.existingImportedWallets = !!existingImportedWalletsString ? JSON.parse(existingImportedWalletsString) : [];
  }

  async importFile() {

    var presentAlert = async (wallet: any, isMnemonic: boolean) => {

      const alerty = await this._AlertController.create({
        mode: 'ios',
        header: this._Translate.instant('IMPORTKEY.alert-1-header'),
        inputs: [{ type: 'text', placeholder: '...', name: 'name'}],
        buttons: [{ text: this._Translate.instant('BUTTON.CANCEL'), role: 'cancel', handler: () => {} },{
          text: this._Translate.instant('GENERAL.ok'),
          handler: async (data) => {

            if(!data.name || data.name.trim() == "") {
              this.presentToast(this._Translate.instant('IMPORTKEY.name-empty-error'));
              return;
            }
            if(Array.from(this.existingImportedWallets, k => (k as any).heading).includes(data.name)) {
              this.presentToast(this._Translate.instant('IMPORTKEY.name-default-error'));
              return;
            }
            if(data.name.trim().length > 20) {
              this.presentToast(this._Translate.instant('IMPORTKEY.name-length-error'));
              return;
            }

            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];

            this.existingImportedWallets.push({
              heading: data.name,
              publicKey: wallet.address,
              mnemonic: !!isMnemonic ? wallet.mnemonic.phrase : null,
              privateKey: wallet.privateKey,
              creationDate: +new Date(),
              color
            })

            await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(this.existingImportedWallets));
            await this._UserActivity.updateActivity(ActivityKeys.WALLET_IMPORTED, { address: this._ApiProviderService.obtainDisplayableAddress(wallet.address) });

            this.close(false);

          }
        }]
      })

      await alerty.present();
    }

    if(this._Platform.is('hybrid')) {

      this._LockService.removeResume();

      this._Chooser.getFile({ mimeTypes: "application/json" }).then(async (file) => {
        console.log(file);
        if(!file) {
          this.presentToast(this._Translate.instant('IMPORTKEY.nofile'));
          return;
        }

        var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();

        if(!!(file as any).dataURI) {
          var data = (file as any).dataURI.toString().replace("data:application/json;base64,","");
          var d = window.atob(data);
        } else {
          try {
            data = await this._File.readAsArrayBuffer(file.path, file.name);
            d = new TextDecoder().decode(data);
          } catch(e) {
            data = await this._File.readAsArrayBuffer((file as any).uri.replace(file.name, ""), file.name);
            d = new TextDecoder().decode(data);
          }
        }

        try {
          var d1 = CryptoJS.AES.decrypt(JSON.parse(d), UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
        } catch(e) {
          d1 = CryptoJS.AES.decrypt(d, UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
        }

        try {
          var d3 = JSON.parse(d1);
        } catch(e) {
          d3 = d1;
        }

        try {
          var d2 = CryptoJS.AES.decrypt(d3, backupSymmetricKey).toString(CryptoJS.enc.Utf8);
          console.log(d2);
        } catch(e) {
          d2 = d3;
        }

        try {
          var d4 = JSON.parse(d2);
        } catch(e) {
          d4 = d2;
        }

        const isMnemonic = (d4.split(" ").length > 1);
        console.log("isMnemonic", isMnemonic);

        if(isMnemonic && !ethers.utils.isValidMnemonic(d4)) {
          this.presentToast(this._Translate.instant('WALLET.invalidMnemonic'));
          return;
        }


        if((this.initial && isMnemonic && (this.existingUserMnemonic == d4)) || (!this.initial && ((this.existingUserMnemonic == d4) || this.existingImportedWallets.includes(d4)))) {
          this.presentToast(this._Translate.instant('IMPORTKEY.wallet-imported'));
          return;
        }

        try {

          if(isMnemonic) {
            var wallet: any = ethers.Wallet.fromMnemonic(d4);
            } else {
            wallet = eth.accounts.privateKeyToAccount(d4);
            }

         if(this.initial) {

          this._ApiProviderService.saveUserWeb3WalletAddress(wallet.address).then(async () => {
            if(!!wallet.mnemonic){
              if(!!wallet.mnemonic.phrase) {
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, wallet.mnemonic.phrase);
              }
            }
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address);
            this._LockService.addResume();
            this.close(true);
          }).catch(e => {
            console.log(e);
            this.presentToast(this._Translate.instant('IMPORTKEY.invalid-mnemonic'));
            this._LockService.addResume();
            return;
          });


        } else {

          await presentAlert(wallet, isMnemonic);
          this._LockService.addResume();

        }

        } catch(e) {
          this.presentToast(this._Translate.instant('IMPORTKEY.invalid-file'));
          this._LockService.addResume();
          console.log(e);
        }

     }).catch(e => {
       console.log(e);
     })
    } else {
      var a = document.createElement("input");
      a.type = "file";
      a.style.display = "none";
      // onclick event handler
      a.onchange = ((fileData) => {
        console.log(a.files);
        console.log(fileData);
        var reader = new FileReader();
        reader.onload = async (e) => {
         var t = e.target.result;

         if(t.toString().indexOf("data:application/json;base64,") == -1) {
           this.presentToast(this._Translate.instant('IMPORTKEY.invalid-format'));
           return;
         }

         var tt = window.atob(t.toString().replace("data:application/json;base64,",""));

         var backupSymmetricKey = await this._CryptoProviderService.returnBackupSymmetricKey();

         try {
          var d1 = CryptoJS.AES.decrypt(JSON.parse(tt), UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
        } catch(e) {
          d1 = CryptoJS.AES.decrypt(tt, UDIDNonce.userKey).toString(CryptoJS.enc.Utf8);
        }

         try {
           var d3 = JSON.parse(d1);
         } catch(e) {
           d3 = d1;
         }

         try {
           var d2 = CryptoJS.AES.decrypt(d3, backupSymmetricKey).toString(CryptoJS.enc.Utf8);
           console.log(d2);
         } catch(e) {
           d2 = d3;
         }

         try {
           var d4 = JSON.parse(d2);
         } catch(e) {
           d4 = d2;
         }

         const isMnemonic = (d4.split(" ").length > 1);

         if(isMnemonic && !ethers.utils.isValidMnemonic(d4)) {
          this.presentToast(this._Translate.instant('WALLET.invalidMnemonic'));
          return;
        }


         if((this.initial && isMnemonic && (this.existingUserMnemonic == d4)) || (!this.initial && (this.existingUserMnemonic == d4 || this.existingImportedWallets.includes(d4)))) {
            this.presentToast(this._Translate.instant('IMPORTKEY.wallet-imported'));
            return;
         }

         if(isMnemonic) {
          var wallet: any = ethers.Wallet.fromMnemonic(d4);
        } else {
          wallet = eth.accounts.privateKeyToAccount(d4);
        }

         if(this.initial) {

          this._ApiProviderService.saveUserWeb3WalletAddress(wallet.address).then(async () => {
            if(!!wallet.mnemonic) {
              if(!!wallet.mnemonic.phrase) {
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, wallet.mnemonic.phrase);
              }
            }
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
            await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address);

            this.close(true);
          }).catch(e => {
            console.log(e);
            this.presentToast(this._Translate.instant('IMPORTKEY.invalid-mnemonic'));
            return;
          });

         } else {

          await presentAlert(wallet, isMnemonic);

         }

        }

        reader.readAsDataURL(a.files[0]);

      })
      a.click();
      a.remove();

    }


  }

  async importPrivate() {

    const alerty = await this._AlertController.create({
      header: this._Translate.instant('IMPORTKEY.alert-2-header'),
      inputs: [
        { type: 'text', name: 'name', placeholder: 'Wallet name' },
        { type: 'textarea', name: 'privateKey', placeholder: '...' },
      ],
      buttons: [{
        text: this._Translate.instant('GENERAL.ok'),
        handler: async (data) => {
          if(data.privateKey == '') {
            this.presentToast(this._Translate.instant('IMPORTKEY.invalid-privateKey'));
            await this.importPrivate();
            return;
          }

          if(data.name == '') {
            this.presentToast(this._Translate.instant('WALLET.uniqueName'));
            await this.importPrivate();
            return;
          }

          try {
            const privateKey = data.privateKey.startsWith("0x") ? data.privateKey : `0x${data.privateKey}`;
            const wallet = eth.accounts.privateKeyToAccount(privateKey);

            /*
            Wallet:
            {
              "address": "0xb8CE9ab6943e0eCED004cDe8e3bBed6568B2Fa01",
              "privateKey": "0x348ce564d427a3311b6536bbcff9390d69395b06ed6c486954e971d960fe8709"
            }
            */

            /*
            Sign:
            {
              "message": "Ansik",
              "messageHash": "0xf6340d25da0f0432bda68d26458fab0dad42dc90eb3dd32dbe4832ffdababb43",
              "v": "0x1c",
              "r": "0xc9e4ad1aed409c62052bccd6c12429e4c03d858a40c152cb7e3240dce4221709",
              "s": "0x13123bf1b172d701bd2fa6902ef03a95d41da3246639f2c09f88d227c80b7fbc",
              "signature": "0xc9e4ad1aed409c62052bccd6c12429e4c03d858a40c152cb7e3240dce422170913123bf1b172d701bd2fa6902ef03a95d41da3246639f2c09f88d227c80b7fbc1c"
            }
            */

            /*
            Encrypt:
            {
              "version": 3,
              "id": "149dfee0-a272-4726-83f3-c9d04c2ea91d",
              "address": "b8ce9ab6943e0eced004cde8e3bbed6568b2fa01",
              "crypto": {
                  "ciphertext": "4db8c4bb54cd116fe06c80f0459695037b60c92a5394e7b42a843d60fff7fc63",
                  "cipherparams": {
                      "iv": "97974d1e9210de307c991e7c983446f4"
                  },
                  "cipher": "aes-128-ctr",
                  "kdf": "scrypt",
                  "kdfparams": {
                      "n": 8192,
                      "r": 8,
                      "p": 1,
                      "dklen": 32,
                      "salt": "0e6e39f7861472ad18937e2aadfc3667ac6d04131ce5e100ef15f02690dc8e08"
                  },
                  "mac": "b75b63523bd2f7c3825831370cbd323e3e8c074d7977c04c52a98f371d4e7cfb"
              }
            }
          */
            var importedWalletsWithoutMnemonicString = await this._SecureStorageService.getValue(SecureStorageKey.importedWallets, false);
            var importedWalletsWithoutMnemonic = !!importedWalletsWithoutMnemonicString ? JSON.parse(importedWalletsWithoutMnemonicString) : [];
            var check = importedWalletsWithoutMnemonic.find(im => im.publicKey == wallet.address);

            if(!!check) {
              this.presentToast(this._Translate.instant('IMPORTKEY.wallet-imported'));
              return;
            }

            var colorschemes = [ 'lime', 'orange', 'purple', 'red', 'yellow', 'lightblue', 'cyan' ];
            var random = Math.floor(Math.random() * colorschemes.length);
            var color = colorschemes[random];

            importedWalletsWithoutMnemonic.push({
              primary: false,
              heading: data.name,
              publicKey: wallet.address,
              mnemonic: null,
              privateKey: wallet.privateKey,
              icon: `data:image/png;base64,${new Identicon(
                wallet.address,
                420
              ).toString()}`,
              creationDate: +new Date(),
              color
            })

            console.log("importedWalletsWithoutMnemonic", importedWalletsWithoutMnemonic);

            await this._SecureStorageService.setValue(SecureStorageKey.importedWallets, JSON.stringify(importedWalletsWithoutMnemonic));

            this.close(false);
          } catch(e) {
            console.log(e);
            this.presentToast(this._Translate.instant('IMPORTKEY.invalid-privateKey'));
          }

        }
      },{
        text: this._Translate.instant('BUTTON.CANCEL'),
        role: 'cancel',
        handler: () => {
          // this.close(false);
        }
      }]
    })

    await alerty.present();

}


  async importText() {

    if(this.initial) {
      const alerty = await this._AlertController.create({
        mode: 'ios',
        header: this._Translate.instant('IMPORTKEY.alert-2-header'),
        inputs: [
          { type: 'textarea', name: 'mnemonic', placeholder: '...' }
        ],
        buttons: [{
          text: 'Ok',
          handler: async (data) => {
            if(data.mnemonic == '') {
              this.presentToast(this._Translate.instant('IMPORTKEY.invalid-mnemonic'));
              await this.importText();
              return;
            }


            if((this.initial && (this.existingUserMnemonic == data.mnemonic)) || (!this.initial && ((this.existingUserMnemonic == data.mnemonic) || this.existingImportedWallets.includes(data.mnemonic)))) {
              this.presentToast(this._Translate.instant('IMPORTKEY.wallet-imported'));
              return;
            }

            try {
              var wallet = ethers.Wallet.fromMnemonic(data.mnemonic);

              this._ApiProviderService.saveUserWeb3WalletAddress(wallet.address).then(async () => {
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, wallet.mnemonic.phrase);
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
                await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, wallet.address);
                await this._UserActivity.updateActivity(ActivityKeys.WALLET_CREATED, { address: this._ApiProviderService.obtainDisplayableAddress(wallet.address) });
                this.close(true);
              }).catch(e => {
                console.log(e);
                this.presentToast(this._Translate.instant('IMPORTKEY.invalid-keypair'));
                return;
              })

            } catch(e) {
              this.presentToast(this._Translate.instant('IMPORTKEY.invalid-mnemonic'));
              // await this.importText();
              console.log(e);
            }

          }
        },{
          text: this._Translate.instant('BUTTON.CANCEL'),
          role: 'cancel',
          handler: () => {}
        }]
      })

      await alerty.present();
    } else {
      this.close(true);
    }

  }

  async generateWallet() {
    const alerto = await this._AlertController.create({
      mode: 'ios',
      header: this._Translate.instant('IMPORTKEY.alert-3-header'),
      message: this._Translate.instant('IMPORTKEY.alert-3-message'),
      buttons: [{
        text: this._Translate.instant('IMPORTKEY.alert-3-button-1'),
        role: 'cancel',
        handler: () => {}
      }, {
        text: this._Translate.instant('IMPORTKEY.alert-3-button-2'),
        handler: () => {
          const userMnemonic = generateMnemonic();
          const wallet = ethers.Wallet.fromMnemonic(userMnemonic);

          var newUserPublicKey = wallet.address;
          if(newUserPublicKey) {
            this._ApiProviderService.saveUserWeb3WalletAddress(newUserPublicKey).then(() => {
              this._ModalController.create({
                component: KeyExportComponent,
                componentProps: {
                  mnemonic: userMnemonic
                }
              }).then(modal => {
                modal.onDidDismiss().then(async () => {
                  await this._SecureStorageService.setValue(SecureStorageKey.web3WalletMnemonic, userMnemonic);
                  await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPublicKey, newUserPublicKey);
                  await this._SecureStorageService.setValue(SecureStorageKey.web3WalletPrivateKey, wallet.privateKey);
                  await this._UserActivity.updateActivity(ActivityKeys.WALLET_CREATED, { address: this._ApiProviderService.obtainDisplayableAddress(wallet.address) });
                  this.close(true);
                })
                modal.present();
              })
            }).catch(e => {
              console.log(e);
              this.presentToast(this._Translate.instant('IMPORTKEY.error-saving-new-address'));
            });
          } else {
            this.presentToast(this._Translate.instant('IMPORTKEY.something-wrong'));
          }

        }
      }]
    })
    await alerto.present();
  }

  presentToast(message: string) {
    this._ToastController.create({
      position: 'top',
      duration: 2000,
      message
    }).then(toast => toast.present())
  }

  close(value: boolean) {
    this._ModalController.dismiss({value});
  }

}
