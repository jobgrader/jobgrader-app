import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../shared/shared.module';
import { ForgotUsernamePage } from './forgot-username.page';


const routes: Routes = [
  {
    path: '',
    component: ForgotUsernamePage,
    children:
    [
      {
        path: 'step-1',
        loadChildren: () => import('../forgot-username/components/step-1/step-1.module').then(m => m.ForgotUsernameStepOnePageModule)
      },
      {
        path: 'step-2',
        loadChildren: () => import('../forgot-username/components/step-2/step-2.module').then(m => m.ForgotUsernameStepTwoPageModule)
      },
      {
        path: '',
        redirectTo: 'step-1',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [ForgotUsernamePage]
})
export class ForgotUsernamePageModule {}

