import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { of, SubscriptionLike } from 'rxjs';
import { delay } from 'rxjs/operators';
import { UtilityService } from '../../../core/providers/utillity/utility.service';
import { ApiProviderService, ValidationResponse } from '../../../core/providers/api/api-provider.service';
import { TranslateProviderService } from '../../../core/providers/translate/translate-provider.service';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { PasswordStrengthInfo } from 'src/app/core/providers/user';
import { CryptoProviderService } from 'src/app/core/providers/crypto/crypto-provider.service';

@Component({
  selector: 'app-forgot-username-step-1',
  templateUrl: './step-1.page.html',
  styleUrls: ['./step-1.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ForgotUsernameStepOnePage implements OnInit, OnDestroy {
  @ViewChild(IonContent) content: IonContent;
  forgotUsernameForm: UntypedFormGroup;
  notValidForm = false;
  processing = false;
  private ngOnInitExecuted: any;
  private additionalStyleSheetId: string;
  public passwordStrengthInfo: PasswordStrengthInfo = {
    password1Type: "password"
  }

  constructor(
    private nav: NavController,
    private cdr: ChangeDetectorRef,
    private utilityService: UtilityService,
    private elemRef: ElementRef,
    private api: ApiProviderService,
    private translateProviderService: TranslateProviderService,
    private toastController: ToastController,
    private _NetworkService: NetworkService,
    private _CryptoProviderService: CryptoProviderService
  ) {
  }

  async ionViewWillEnter() {
    this.content.scrollToTop();
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  /** @inheritDoc */
  async ngOnInit() {
    this.ngOnInitExecuted = true;
    const tagName = this.elemRef.nativeElement.tagName.toLowerCase();
    const styles: string =
        this.utilityService.calculateWhiteCircleCssParams(`${tagName} .page-content:after`);
    UtilityService.addStyleToHead(styles, this.additionalStyleSheetId = `${tagName}CircleStyle`);
    await this.componentInit();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    UtilityService.removeStyleFromHead(this.additionalStyleSheetId);
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      position: 'top',
    });
    toast.present();
  }

  async nextStep() {
    this.forgotUsernameForm.markAsDirty();
    this.processing = true;

    if (!this.forgotUsernameForm.valid) {
      return false;
    }

    if (!this._NetworkService.checkConnection()) {
      this._NetworkService.addOfflineFooter('ion-footer');
      return;
    } else {
      this._NetworkService.removeOfflineFooter('ion-footer');
    }
    var encryptedPw = await this._CryptoProviderService.encryptPasswordAsBase64(this.forgotUsernameForm.value.password);
    // console.log(encryptedPw);

    // var checkEmail = await this.api.checkEmail(this.forgotUsernameForm.value.email);

    // // console.log(checkEmail);

    // if(!checkEmail) {
    //   await this.presentToast(this.translateProviderService.instant('FORGOTUSERNAMESTEPONE.mailNotExist'));
    //   return;
    // }

    try {
      const response: ValidationResponse = await this.api.forgotUsername(this.forgotUsernameForm.value.email, encryptedPw);
      this.processing = false;
      console.info(response);
      if(!(response as ValidationResponse).errorFound) {
        this.nav.navigateForward('/forgot-username/step-2');
      } else {
        const error = response.errors[0];
        if(error.errorCode == 1011) {
          await this.presentToast(this.translateProviderService.instant('FORGOTUSERNAME.INCORRECT_PASSWORD'));
        } else if (error.errorCode == 1015) {
          await this.presentToast(this.translateProviderService.instant('FORGOTUSERNAME.INCORRECT_EMAIL'));
        } else {
          await this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));
        }
      }
    } catch (error) {
      console.log(error);
      await this.presentToast(this.translateProviderService.instant('LOADER.somethingWrong'));

    }

  }

  async backToLogin() {
    this.nav.navigateBack('/login?from=forgot-username');
  }

  prevStep() {
    this.nav.navigateBack('/login?from=resetpassword');
  }

  private async componentInit() {
    this.forgotUsernameForm = new UntypedFormGroup({
      email: new UntypedFormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/)
      ]),
      password: new UntypedFormControl('', [
        Validators.required,
        this.validatePassword
        // Validators.pattern('^[A-Za-z0-9!?@#$%^&*)(+=._-]+$')
      ])
    });

    const componentInitTimeout: SubscriptionLike = of(true).pipe(delay(1)).subscribe(() => {
      this.detectChanges();
      componentInitTimeout.unsubscribe();
    });
  }

  togglePasswordVisibility() {
    this.passwordStrengthInfo.password1Type = (this.passwordStrengthInfo.password1Type == 'text') ? 'password' : 'text';
  }

  private validatePassword(formControl: UntypedFormControl) {
    const password = formControl.value;
    const hasEmptySpaces = password.match(/[\s]/);
    const hasBetweenMinAndMaxCharacters = password.match(/^.{8,25}$/);
    const hasBlockLetters = password.match(/[A-Z]/);
    const hasSmallLetter = password.match(/[a-z]/);
    const hasEitherUpperAndOrLowerCase = (hasBlockLetters !== null || hasSmallLetter !== null);
    const hasAccentedCharacters = password.match(/[\u00C0-\u024F\u1E00-\u1EFF]/);

    const isValid = (
      hasEmptySpaces === null &&
      hasBetweenMinAndMaxCharacters !== null &&
      hasEitherUpperAndOrLowerCase !== null &&
      hasAccentedCharacters == null
    );

    return (
      isValid
      ? null : {
        validatePassword: {
          valid: false
        }
      }
    );
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }
}
