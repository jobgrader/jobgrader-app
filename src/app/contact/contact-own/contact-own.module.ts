import { QRCodeModule } from 'angularx-qrcode';
import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { ContactOwnPage } from './contact-own.page';
import { AuthenticationGuard } from 'src/app/core/guards/authentication/authentication-guard.service';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: ContactOwnPage
  }
];

@NgModule({
  imports: [
    QRCodeModule,
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ContactOwnPage],
})
export class ContactOwnPageModule {}
