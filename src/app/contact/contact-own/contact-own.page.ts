import { ApiProviderService } from '../../core/providers/api/api-provider.service';
import { UserProviderService } from '../../core/providers/user/user-provider.service';
import { AuthenticationProviderService } from '../../core/providers/authentication/authentication-provider.service';
import { ContactService } from '../shared/contact.service';
import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { interval, Subject, Subscription } from 'rxjs';
import { AppConfig } from '../../app.config';
import { TranslateProviderService } from '../../core/providers/translate/translate-provider.service';
import { ToastController, ActionSheetController } from '@ionic/angular';
import { takeUntil } from 'rxjs/operators';
import { AppStateService } from '../../core/providers/app-state/app-state.service';
import { UtilityService } from '../../core/providers/utillity/utility.service';
import { SecureStorageService } from '../../core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from '../../core/providers/secure-storage/secure-storage-key.enum';
import { BarcodeService } from '../../core/providers/barcode/barcode.service';
import { ChatService } from 'src/app/core/providers/chat/chat.service';
import { KycService } from 'src/app/kyc/services/kyc.service';
import { EventsService } from 'src/app/core/providers/events/events.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { ImageSelectionService } from 'src/app/shared/components/image-selection/image-selection.service';
import { UserPhotoServiceAkita } from '../../core/providers/state/user-photo/user-photo.service';

const BARCODE_SCANNER_OPTIONS = {
  preferFrontCamera: false, // iOS and Android
  showFlipCameraButton: true, // iOS and Android
  showTorchButton: true, // iOS and Android
  torchOn: false, // Android, launch with the torch switched on (if available)
  saveHistory: false, // Android, save scan history (default false)
  prompt: 'Place a QR Code inside the scan area', // Android
  resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
  formats: 'QR_CODE', // default: all but PDF_417 and RSS_EXPANDED
  orientation: 'portrait', // Android only (portrait|landscape), default unset so it rotates with the device
  disableAnimations: false, // iOS
  disableSuccessBeep: false // iOS and Android
};

@Component({
  selector: 'app-contact-own',
  templateUrl: './contact-own.page.html',
  styleUrls: ['./contact-own.page.scss']
})
export class ContactOwnPage implements AfterViewInit, OnDestroy {

  private destroy$ = new Subject();

  private readonly appConfig: any = AppConfig;
  @ViewChild('profileImg') public profileImg: ElementRef;

  public timeout = 1;
  public qrCodeValue: string;

  public qrCodeIsHidden = false;
  public showDiscalimer = false;
  public disclaimerIcon: string;

  public qrCodeCaptionGeneration: string;
  public qrCodeCaptionCountdown: string;
  private timerSubscription: Subscription;

  public placeholderImage: string;

  public id: string;
  public displayPicture: string;
  public displayHelixUsername: string;
  public chatPublicKey: string;

  public userAllowedToUseChat: boolean;

  constructor(
      private contactService: ContactService,
      private userService: UserProviderService,
      private securestorage: SecureStorageService,
      private _TranslateProviderService: TranslateProviderService,
      private apiProviderService: ApiProviderService,
      private appStateService: AppStateService,
      public _ChatService: ChatService,
      public _KycService: KycService,
      private _ToastController: ToastController,
      public _EventsService: EventsService,
      private _ActionSheetController: ActionSheetController,
      public _BarcodeService: BarcodeService,
      private _NetworkService: NetworkService,
      private _ImageSelectionService: ImageSelectionService,
      private _AuthenticationProviderService: AuthenticationProviderService,
      private userPhotoServiceAkita: UserPhotoServiceAkita,
  ) {
  }

  ionViewWillEnter() {
    const photo = this.userPhotoServiceAkita.getPhoto();
    if(photo) {
      this.displayPicture = photo
      this.setImageOnLoad();
    } else {
      // if(this.appStateService.isAuthorized) {
        this.placeholderImage = '../../../assets/job-user.svg';
      // } else {
      //   this.placeholderImage = '../../../assets/U-unAuthorized.svg';
      // }
    }

    this._KycService.isUserAllowedToUseChatMarketplace().then(r => {
      this.userAllowedToUseChat = r;
      this._TranslateProviderService.getLangFromStorage().then(lang => {
        this.disclaimerIcon = (lang == 'de') ?  '../../../assets/QR_Alert_de.svg' : '../../../assets/QR_Alert_en.svg';
        this.showDiscalimer = !this.appStateService.isAuthorized || !this.userAllowedToUseChat;
      })
    })
    if(!this._NetworkService.checkConnection()){
      this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }
    this._AuthenticationProviderService.displayLoginOptionIfNecessary();
  }

  public ngAfterViewInit(): void {
    this.startQrTimer();
  }

  /** @inheritDoc */
  public ngOnDestroy(): void {
    this.destroy$.next(0);
    this.destroy$.complete();
  }

  /** Callback for opening the scanner */
  public openScanner(): void {
    if(!this.userAllowedToUseChat) {
      this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed')).then(() => {})
    } else {
      this._BarcodeService.scanContact();
    }
  }



  private setImageOnLoad(): void {
    UtilityService.setImageOnLoad(this.profileImg.nativeElement, async () => {
      this.stopQrTimer();
      await this.startQrTimer();
    });
  }

  private stopQrTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  public async contactAdditionOptions() {
    if(this.appStateService.isAuthorized) {
      const alert = await this._ActionSheetController.create({
        mode: 'md',
        header: this._TranslateProviderService.instant('CHAT.contact-addition-options'),
        buttons: [
          { text: this._TranslateProviderService.instant('CHAT.scan-qr-code'), icon: 'qr-code', handler: () => { this.openScanner() } },
          { text: this._TranslateProviderService.instant('CHAT.add-did-contact'), icon: 'at', handler: () => { this.addContactViaUsername() } },
          { text: this._TranslateProviderService.instant('GENERAL.cancel'), icon: 'close', role: 'cancel', handler: () => { console.log('Cancel') } }
        ]
      });
      await alert.present();
    } else {
      await this.apiProviderService.noUserLoggedInAlert();
    }
}

public addContactViaUsername(): void {
  this._KycService.isUserAllowedToUseChatMarketplace().then(s => {
    if(!s) {
      this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed')).then(() => {})
    } else {
      this._ChatService.showSearchUserByUsername();
    }
  })
}

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 3000, position: 'top' });
    await toast.present();
  }

  private async startQrTimer(): Promise<void> {
    const user = await this.userService.getUser();
    this.id = user.userid;

    this.displayHelixUsername = await this.userService.getUsername();

    this.userAllowedToUseChat = await this._KycService.isUserAllowedToUseChatMarketplace();
    if(!this.userAllowedToUseChat) {
      return await this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed'));
    }

    let chatUserData_ = await this.securestorage.getValue(SecureStorageKey.chatUserData, false);
    var chatUsername = chatUserData_ ? JSON.parse(chatUserData_).username : '';

    this.chatPublicKey = await this.securestorage.getValue(SecureStorageKey.chatPublicKey, false);
    const INTERVAL = 1000; // ms

    let countdown = 0;

    let validity: number;
    let timeout: number;
    let encryptedContactIdAsString: string;

    const encryptedQRObject = {
      id: this.id,
      helixUsername: this.displayHelixUsername,
      chatPublicKey: this.chatPublicKey,
      chatUsername: chatUsername,
      hasImage: (this.displayPicture != null)
    };

    // console.log(encryptedQRObject);
    const encryptedQRString = JSON.stringify(encryptedQRObject);
    const data = await this.contactService.getQRCode(encryptedQRString, new Date(), 'contact');
    // console.log('---' + new Date().toTimeString());
    // console.log(`---encryptedContactIdAsString: ${data.encryptedContactIdAsString}, validity: ${data.validity}`);

    validity = data.validity;
    timeout = data.timeout;
    encryptedContactIdAsString = data.encryptedContactIdAsString;

    var url = `${encryptedContactIdAsString}`;
    this.qrCodeValue = url;

    const timer = interval(INTERVAL);
    this.timerSubscription = timer.pipe(takeUntil(this.destroy$)).subscribe(() => {
      countdown++;

      // console.log(`countdown: ${countdown}, validity: ${validity}`);

      if (countdown === validity - timeout || countdown === validity - timeout + 1) {
        this.qrCodeIsHidden = true;
        this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.new-generation");
        return;
      }

      if (countdown === validity) {
        this.contactService.getQRCode(encryptedQRString, new Date(), 'contact').then(data => {
          validity = data.validity;
          var url = `${data.encryptedContactIdAsString}`;
          this.qrCodeValue = url;
          this.qrCodeIsHidden = false;
        });

        countdown = 0;
      }

      this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.expiry-notice.part1")+ `${validity - timeout - countdown}s` + this._TranslateProviderService.instant("QRSCAN.expiry-notice.part2");
    });
  }



  async displayPictureOptions() {
    if(this.appStateService.isAuthorized) {
      // console.log('Should work!')
      this._ImageSelectionService.showChangePicture().then(photo => {
        this.displayPicture = !!photo ? photo : this.placeholderImage;
      });
    } else {
      await this.apiProviderService.noUserLoggedInAlert();
    }
  }
}
