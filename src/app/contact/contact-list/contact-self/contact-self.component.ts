import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';
import { UserProviderService } from 'src/app/core/providers/user/user-provider.service';
import { KycService } from 'src/app/kyc/services/kyc.service';
import { ContactService } from '../../shared/contact.service';
import { Subscription, interval } from 'rxjs';
import { AppStateService } from 'src/app/core/providers/app-state/app-state.service';
import { NetworkService } from 'src/app/core/providers/network/network-service';
import { AuthenticationProviderService } from 'src/app/core/providers/authentication/authentication-provider.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contact-self',
  templateUrl: './contact-self.component.html',
  styleUrls: ['./contact-self.component.scss'],
})
export class ContactSelfComponent implements OnDestroy, OnInit {

  // private destroy$ = new Subject();

  public timeout = 1;
  public qrCodeValue: string;

  public qrCodeIsHidden = false;
  public showDiscalimer = false;
  public disclaimerIcon: string;

  public qrCodeCaptionGeneration: string;
  public qrCodeCaptionCountdown: string;
  private timerSubscription: Subscription;

  public id: string;
  public displayPicture: string;
  public displayHelixUsername: string;
  public chatPublicKey: string;

  public userAllowedToUseChat: boolean;

  constructor(
    private _ModalController: ModalController,
    private _UserService: UserProviderService,
    private _TranslateProviderService: TranslateProviderService,
    private _KycService: KycService,
    private _SecureStorageService: SecureStorageService,
    private _ContactService: ContactService,
    private _AppStateService: AppStateService,
    private _NetworkService: NetworkService,
    private _AuthenticationProviderService: AuthenticationProviderService,
    private _ToastController: ToastController,
    private _Router: Router,
    private _ActivatedRoute: ActivatedRoute
  ) {
   }

  close() {
    this._ModalController.dismiss();
  }

  public ngOnDestroy(): void {
    // this.destroy$.next(0);
    // this.destroy$.complete();

    this.timerSubscription.unsubscribe();
  }

  public ionViewWillEnterCustom(): void {
    this._KycService.isUserAllowedToUseChatMarketplace().then(r => {
      // this.userAllowedToUseChat = true;
      this.userAllowedToUseChat = r;
      console.log(this.userAllowedToUseChat);
      this._TranslateProviderService.getLangFromStorage().then(lang => {
        this.disclaimerIcon = (lang == 'de') ?  '../../../../assets/QR_Alert_de.svg' : '../../../../assets/QR_Alert_en.svg';
        // this.showDiscalimer = false;
        this.showDiscalimer = !this._AppStateService.isAuthorized || !this.userAllowedToUseChat;
        console.log(this.showDiscalimer);
      })
    })
    if(!this._NetworkService.checkConnection()){
      this._NetworkService.addOfflineFooter('ion-footer');
    } else {
        this._NetworkService.removeOfflineFooter('ion-footer');
    }
    this._AuthenticationProviderService.displayLoginOptionIfNecessary();
  }

  async presentToast(message: string) {
    var toast = await this._ToastController.create({ message, duration: 3000, position: 'bottom' });
    await toast.present();
  }

  public ngOnInit(): void {
    this.ionViewWillEnterCustom();
    this.startQrTimer();
  }

  private stopQrTimer(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }

  private async startQrTimer(): Promise<void> {
    const user = await this._UserService.getUser();
    this.id = user.userid;

    this.displayHelixUsername = await this._UserService.getUsername();

    this.userAllowedToUseChat = await this._KycService.isUserAllowedToUseChatMarketplace();
    if(!this.userAllowedToUseChat) {
      return await this.presentToast(this._TranslateProviderService.instant('CHAT.userNotAllowed'));
    }

    let chatUserData_ = await this._SecureStorageService.getValue(SecureStorageKey.chatUserData, false);
    var chatUsername = chatUserData_ ? JSON.parse(chatUserData_).username : '';

    this.chatPublicKey = await this._SecureStorageService.getValue(SecureStorageKey.chatPublicKey, false);
    const INTERVAL = 1000; // ms

    let countdown = 0;

    let validity: number;
    let timeout: number;
    let encryptedContactIdAsString: string;

    const encryptedQRObject = {
      id: this.id,
      helixUsername: this.displayHelixUsername,
      chatPublicKey: this.chatPublicKey,
      chatUsername: chatUsername,
      hasImage: (this.displayPicture != null)
    };

    const encryptedQRString = JSON.stringify(encryptedQRObject);
    const data = await this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact');

    validity = data.validity;
    timeout = data.timeout;
    encryptedContactIdAsString = data.encryptedContactIdAsString;

    var url = `${encryptedContactIdAsString}`;
    this.qrCodeValue = url;

    const timer = interval(INTERVAL);
    this.timerSubscription = timer.subscribe(() => {
      countdown++;

      if (countdown === validity - timeout || countdown === validity - timeout + 1) {
        this.qrCodeIsHidden = true;
        this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.new-generation");
        return;
      }

      if (countdown === validity) {
        this._ContactService.getQRCode(encryptedQRString, new Date(), 'contact').then(data => {
          validity = data.validity;
          var url = `${data.encryptedContactIdAsString}`;
          this.qrCodeValue = url;
          this.qrCodeIsHidden = false;
        });

        countdown = 0;
      }

      this.qrCodeCaptionCountdown = this._TranslateProviderService.instant("QRSCAN.expiry-notice.part1")+ `${validity - timeout - countdown}s` + this._TranslateProviderService.instant("QRSCAN.expiry-notice.part2");
    });
  }

}
