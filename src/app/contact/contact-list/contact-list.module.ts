import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { ContactListPage } from './contact-list.page';
import { AuthenticationGuard } from 'src/app/core/guards/authentication/authentication-guard.service';
import { ContactSelfModule } from './contact-self/contact-self.module';
import { JobHeaderModule } from 'src/app/job-header/job-header.module';

const routes: Routes = [
  {
    path: '',
    // canActivate: [AuthenticationGuard],
    component: ContactListPage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContactSelfModule,
    SharedModule,
    JobHeaderModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [ContactListPage]
})
export class ContactListPageModule {}
