import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActionSheetController, NavController } from '@ionic/angular';
import { ChatService } from 'src/app/core/providers/chat/chat.service';
import { DomSanitizer } from '@angular/platform-browser';
import { SecureStorageService } from 'src/app/core/providers/secure-storage/secure-storage.service';
import { SecureStorageKey } from 'src/app/core/providers/secure-storage/secure-storage-key.enum';
import { environment } from 'src/environments/environment';
import { TranslateProviderService } from 'src/app/core/providers/translate/translate-provider.service';

@Component({
  selector: 'app-tab-contact',
  templateUrl: './contact-detail.page.html',
  styleUrls: ['./contact-detail.page.scss']
})
export class ContactDetailPage implements OnInit {

  private ngOnInitExecuted = false;
  public powerUserMode: boolean = false;

  constructor(
    private nav: NavController,
    private actionSheetController: ActionSheetController,
    public _ChatService: ChatService,
    private _SecureStorageService: SecureStorageService,
    public _DomSanitizer: DomSanitizer,
    private translate: TranslateProviderService,
    private cdr: ChangeDetectorRef
  ) {
  }

  async ngOnInit() {
    this.ngOnInitExecuted = true;
    await this.componentInit();
  }

  private async componentInit() {
    if (!this._ChatService.chatThreadToView) {
      return this.goBack();
    }
    var powermode = await this._SecureStorageService.getValue(SecureStorageKey.powerUserMode, false);
    this.powerUserMode = (powermode == 'true');
    setTimeout(() => this.detectChanges());
  }

  async ionViewWillEnter() {
    if (!this.ngOnInitExecuted) {
      await this.componentInit();
    }
  }

  async ionViewDidLeave() {
    this.ngOnInitExecuted = false;
  }

  showKeyPairs() {
    var ob = this._ChatService.threadChatKeyPairs[this._ChatService.stripDNS(this._ChatService.chatThreadToView.userWrapper.chatUserName)];
    if(!ob) {
      ob = this._ChatService.threadChatKeyPairs[this._ChatService.stripDNS(this._ChatService.chatThreadToView.userWrapper.chatUserName) + environment.chatUsernameSuffix + '@' + environment.chatServerUrl];
    }
    // console.log(this._ChatService.threadChatKeyPairs);
    // console.log(this._ChatService.chatThreadToView.userWrapper.chatUserName);
    alert(JSON.stringify(ob));
  }

  edit() {
    this.presentActionSheet();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      mode: 'md',
      header: this.translate.instant('CHAT.edit-contact'),
      buttons: [
        {
          text: this.translate.instant('PERSONALDETAILS.delete'),
          role: 'destructive',
          icon: 'trash',
          handler: () => {
            this._ChatService.deleteFriend(this._ChatService.chatThreadToView.userWrapper.chatUserName);
            this.nav.navigateBack('/dashboard/tab-contact');
          }
        }
      ]
    });
    await actionSheet.present();
  }

  chat() {
    this._ChatService.navigateToChat();
  }

  goBack() {
    this._ChatService.chatThreadToView = null;
    this._ChatService.receiveMessageCallback = null;
    this.nav.navigateBack('/dashboard/tab-contact');
  }

  private detectChanges() {
    if (!this.cdr['destroyed']) {
      this.cdr.detectChanges();
    }
  }
}
