import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule,
    CommonModule,
    IonicModule,
    TranslateModule.forChild(),
  ],
})

export class ContactModule {}
