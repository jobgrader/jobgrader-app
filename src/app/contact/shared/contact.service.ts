import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor() {
  }

  public decryptContactId(encryptedString: string): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const floorSeconds = this.floorSeconds(new Date().getSeconds());
        const timestamp = new Date().setSeconds(floorSeconds, 0);
        const timestampString = timestamp.toString();
        const decrypted = CryptoJS.AES.decrypt(encryptedString, timestampString).toString(CryptoJS.enc.Utf8);
        if (decrypted.length) {
          return resolve(decrypted);
        }
        throw Error(`Invalid timestamp`);
      } catch (err) {
        reject(`Error decrypting contactId: ${err}`);
      }
    });
  }

  public getQRCode(userObjectString: string, timestamp: Date, path: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var floorSeconds = this.floorSeconds(new Date().getSeconds());
      var encryptTimestamp = new Date().setSeconds(floorSeconds, 0);
      var timestampString = encryptTimestamp.toString();
      var encryptedContactIdAsString = `${window.location.origin}/${path}?string=${encodeURIComponent(CryptoJS.AES.encrypt(userObjectString, timestampString).toString())}`;
      var validity = this.calculateValidityInSeconds(timestamp);
      var timeout = 2;

      // console.log('--- getQRCode result: -------------------------------------');
      // console.log('--- Time:' + new Date().toTimeString());
      // console.log(`--- EncryptedContactAsString: ${encryptedContactIdAsString}`);
      // console.log(`--- Validity: ${validity}`);
      // console.log(`--- Timeout: ${timeout}`);
      // console.log('--- getQRCode: --------------------------------------------');
      
      resolve({ encryptedContactIdAsString, validity, timeout });
    });
  }

  private floorSeconds(s: number = new Date().getSeconds()): number {
    return Math.floor(s / 30) * 30;
  }

  private calculateValidityInSeconds(timestamp: Date = new Date()): number {
    const seconds = timestamp.getSeconds();

    return seconds >= 30 ? 60 - seconds : 30 - seconds;
  }

  public base58Stringify(data: any) {
    return CryptoJS.enc.Base64.stringify(data);
  }

  public base58SParse(data: string) {
    return CryptoJS.enc.Base64.parse(data);
  }
}
