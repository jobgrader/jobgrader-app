export const environment = {
    production: true,
    consoleLog: {
      log: false,
      debug: false,
      warn: true,
      error: true,
      info: true
    },
    helixEnv: "",
    icloud: "",
    apiUrl: "",
    chatServerUrl: "",
    chatUsernameSuffix: "",
    marketplace: "",
    blue: "",
    adminBasicAuth: "",
    alchemy: {
      api_key: "",
      https: "",
      websocket: ""
    },
    authadaiOS: "",
    authadaAndroid: "",
    helixTicketShop: "",
    did: {
      veriff: "",
      verifeye: "",
      authada: "",
      ondato: ""
    },
    verificationProd: true, // Set it to false when testing is done
    sentry: {
      url: "",
    },
    firebase: {
      apiKey: "",
      authDomain: "",
      projectId: "",
      storageBucket: "",
      messagingSenderId: "",
      appId: "",
      measurementId: "",
      vapidKey: ""
    },
    hidExchange: {
      url: "",
    },
    linkedin: {
      client_id: "",
      client_secret: ""
    },
    discord: {
      client_id: "",
      target_id: "",
      api_base_url: "",
      oauth_base_url: "",
      redirect_uri: ""
    },
    telegram: {
      bot: {
        uri: "",
        token: ""
      },
      redirectUri: ""
    },
    googleAuth: {
      scopes: [],
      serverClientId: "",
      clientId: "",
      iosClientId: "",
      androidClientId: "",
      forceCodeForRefreshToken: true
    },
    socials: {
      telegram: "",
      linkedin: "",
      discord: "",
      x: ""
    },
    filePrefix: {
      userkey: "",
      privateKey: ""
    },
    emails: {
      feedback: ""
    },
    hcaptcha: {
      siteKey: "",
      secret: ""
    },
    firestore: {
      publicKey: "",
      endpoints: {
        generateReferralCode: "",
        fetchReferralPrograms: "",
        obtainUserGroups: "",
        obtainUserData: "",
        manageUserEntry: "",
        checkReferralCodeValidity: "",
        returnPoints: "",
        fetchSocialMediaTasks: "",
        fetchSocialUserStatus: "",
        updateSocialUserStatus: "",
        fetchSocialAccounts: "",
        updateSocialAccounts: "",
        updateFirebaseToken: "",
        obtainDiscordCode: "",
        jobgraderInviteUrl: "",
        checkJobSolution: "",
        storeAssessmentVcs: "",
        obtainAssessmentVcs: "",
        fetchAssessmentContract: "",
        fetchEpnNfts: "",
        thxcUsd: "",
        subscribeMailChimp: "",
        telegramWebhook: "",
        walletconnectregistertopic: "",
        walletconnectderegistertopic: ""
      }
    }
  };
