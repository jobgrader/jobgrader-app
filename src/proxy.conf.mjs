export default {
    "/proxy-test": {
        "target": "https://testserver.jobgrader.app",
        "secure": false,
        "pathRewrite": {
          "^/proxy-test": ""
        },
        "logLevel": "debug",
        "headers": { origin: 'https://testserver.jobgrader.app', host: 'testserver.jobgrader.app'}
    },
    "/proxy-prod": {
      "target": "https://appserver.jobgrader.app",
      "secure": false,
      "pathRewrite": {
        "^/proxy-prod": ""
      },
      "logLevel": "debug",
      "headers": { origin: 'https://appserver.jobgrader.app', host: 'appserver.jobgrader.app'}
  }
}